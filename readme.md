# Notes

|                                                            |                                            |                                                |
|:----                                                       |:-----                                      |:-----                                          |
| **blockchain**                                             |                                            |                                                |
| [classical cipher](blockchain/classical_cipher.md)         | [block cipher](blockchain/block_cipher.md) | [stream cipher](blockchain/stream_cipher.md)   |
| [enigma](blockchain/enigma-simulator.md)                   | [collisions](blockchain/collisions.md)     | [asymmetric](blockchain/asymmetric.md)         |
| [classical cipher](blockchain/classical_cipher.md)         | [cryptopals](blockchain/cryptopals.md)     | [ethereum](blockchain/ethereum.md)             |
| [blockchain](/blockchain/blockchain.md)                    | [BTC](blockchain/btc.md)                   | [ECDSA](blockchain/ecdsa.md)                   |
| **dev**                                                    |                                            |                                                |
| [flask](dev/flask/flask.md)                                | [javascript](dev/javascript.md)            | [frontend](dev/frontend/frontend.md)           |
| [vue](dev/vuejs.md)                                        | [dev](dev/dev.md)                          | [docker](dev/docker.md)                        |
| [git](dev/git.md)                                          | [search engine](dev/search.md)             | [system design](dev/system_design.md)          |
| [docker](dev/docker.md)                                    | [ansible](dev/dev.md)                      | [kubernetes](dev/k8s)                          |
| **scripts**                                                |                                            |                                                |
| [scrape](scripts/scrape.md)                                | [grep](scripts/grep.md)                    | [selenium](scripts/selenium.md)                |
| [beautifulsoup](scripts/beautifulsoup.md)                  | [regex](scripts/regex.md)                  | [python](scripts/python.md)                    |
| **data science**                                           |                                            |                                                |
| [pandas](dev/ai/pandas.md)                                 | [feature engineering](dev/ai/features.md)  | [compuer vision](dev/ai/vision.md)             |
| [numpy](dev/ai/numpy.md)                                   | [models](dev/ai/math.md)                   | [nlp](dev/ai/nlp/nlp.md)                       |
| [data visualization](dev/ai/visualization.md)              |                                            |                                                |
| **configs**                                                |                                            |                                                |
| [arch linux](tools/configs/arch_linux.md)                  | [crontab](tools/configs/crontab.md)        | [vim](dev/vim.md)                              |
| [org-mode](tools/configs/Emacs.org)                        | [git](dev/git.md)                          | [nginx](tools/configs/nginx.md)                |
| [onion](sec/tor/onion.md)                                  | [swarm](tools/configs/swarm.md)            | [tmux](tools/configs/tmux.md)                  |
| **lists**                                                  |                                            |                                                |
| [cheatsheets](random/lists/cheatsheets.md)                 | [cli servers](random/lists/cli_servers.md) | [labs](random/lists/labs.md)                   |
| [list of ctfs](random/lists/list_of_ctfs.md)               | [payloads](random/lists/payloads.md)       | [sans25](random/lists/sans25.md)               |
| [syscalls](random/syscall.md)                              |                                            |                                                |
| **security**                                               |                                            |                                                |
| [binary](sec/binary/binary.md)                             | [crypto](sec/ctf/crypto.md)                | [capture the flag](sec/ctf/ctftime.md)         |
| [osint](sec/osint/osint.md)                                | [recon](sec/recon/recon.md)                | [scan](sec/scan/scan.md)                       |
| [web](sec/web/web.md)                                      | [windows](sec/windows/windows.md)          | [testing](dev/testing.md)                      |
| [redteam](sec/redteam.md)                                  |                                            |                                                |
| **random**                                                 |                                            |                                                |
| [computer](random/computer.md)                             | [coding](random/learn_2_code/coding.md)    | [abacus to ai](random/abacus_to_ai.md)         |
| [recursion](random/recursion.md)                           | [SICP](random/sicp.md)                     | [algorithms](random/algorithms.md)             |
| [functional programming](random/functional_programming.md) | [compiler](random/compiler.md)             | [transistor to browser](random/trans2web.md)   |
| [feeds](random/feeds.md)                                   | [hashtable](random/hashtable.md)           | [TCP](random/tcp.md)                           |
