# data structures

** Abstract data types
```
Container, List, Tuple, Multimap (example Associative array),
Set, Multiset (bag), Stack, Queue (example Priority queue),
Double-ended queue, Graph (example Tree, Heap)
```

** Arrays
```
Array, Bit array, Bit field, Bitboard, Bitmap,
Circular buffer, Control table, Image, Dope vector,
Dynamic array, Gap buffer, Hashed array tree, Lookup table,
Matrix, Parallel array, Sorted array, Sparse matrix,
Iliffe vector, Variable-length array,
```

** Lists
```
Doubly linked list, Array list, Linked list, Association list,
Self-organizing list, Skip list, Unrolled linked list, VList,
Conc-tree list, Xor linked list, Zipper,
Doubly connected edge list also known as half-edge,
Difference list, Free list
```

** Hash Tables
```
Bloom filter, Count-Min sketch, Distributed hash table,
Double hashing, Dynamic perfect hash table, Hash array mapped trie,
Hash list, Hash table, Hash tree, Hash trie, Koorde,
Prefix hash tree, Rolling hash, MinHash, Quotient filter, Ctrie
```

** Heaps
```
Heap, Binary heap, B-heap, Weak heap, Binomial heap Fibonacci heap,
AF-heap, Leonardo Heap, 2-3 heap, Soft heap, Pairing heap,
Leftist heap, Treap, Beap, Skew heap, Ternary heap, D-ary heap, Brodal queue
```

** Trees
```
binary, heaps, space partitioning etc.
Binary Tree, Binary Search Tree, Red-Black Tree,
B-tree, Weight-balanced Tree
```

** Graphs
```
Adjacency list, Adjacency matrix, Graph-structured stack,
Scene graph, Decision tree, Binary decision diagram,
Zero-suppressed decision diagram, And-inverter graph,
Directed graph, Directed acyclic graph,
Propositional directed acyclic graph, Multigraph, Hypergraph
```
-----------

rope, gap buffer, piece table

### resources
- https://github.com/SamirPaul1/DSAlgo
- https://blog.bytebytego.com/p/ep-43-8-data-structures-that-power
- http://opendatastructures.org
