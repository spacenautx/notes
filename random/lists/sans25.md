| Rank | ID      | Name                                                                                       | Score |
|------|---------|--------------------------------------------------------------------------------------------|-------|
| [01] | CWE-119 | Improper Restriction of Operations within the Bounds of a Memory Buffer                    | 75.56 |
| [02] | CWE-79  | Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting')       | 45.69 |
| [03] | CWE-20  | Improper Input Validation                                                                  | 43.61 |
| [04] | CWE-200 | Information Exposure                                                                       | 32.12 |
| [05] | CWE-125 | Out-of-bounds Read                                                                         | 26.53 |
| [06] | CWE-89  | Improper Neutralization of Special Elements used in an SQL Command ('SQL Injection')       | 24.54 |
| [07] | CWE-416 | Use After Free                                                                             | 17.94 |
| [08] | CWE-190 | Integer Overflow or Wraparound                                                             | 17.35 |
| [09] | CWE-352 | Cross-Site Request Forgery (CSRF)                                                          | 15.54 |
| [10] | CWE-22  | Improper Limitation of a Pathname to a Restricted Directory ('Path Traversal')             | 14.10 |
| [11] | CWE-78  | Improper Neutralization of Special Elements used in an OS Command ('OS Command Injection') | 11.47 |
| [12] | CWE-787 | Out-of-bounds Write                                                                        | 11.08 |
| [13] | CWE-287 | Improper Authentication                                                                    | 10.78 |
| [14] | CWE-476 | NULL Pointer Dereference                                                                   |  9.74 |
| [15] | CWE-732 | Incorrect Permission Assignment for Critical Resource                                      |  6.33 |
| [16] | CWE-434 | Unrestricted Upload of File with Dangerous Type                                            |  5.50 |
| [17] | CWE-611 | Improper Restriction of XML External Entity Reference                                      |  5.48 |
| [18] | CWE-94  | Improper Control of Generation of Code ('Code Injection')                                  |  5.36 |
| [19] | CWE-798 | Use of Hard-coded Credentials                                                              |  5.12 |
| [20] | CWE-400 | Uncontrolled Resource Consumption                                                          |  5.04 |
| [21] | CWE-772 | Missing Release of Resource after Effective Lifetime                                       |  5.04 |
| [22] | CWE-426 | Untrusted Search Path                                                                      |  4.40 |
| [23] | CWE-502 | Deserialization of Untrusted Data                                                          |  4.30 |
| [24] | CWE-269 | Improper Privilege Management                                                              |  4.23 |
| [25] | CWE-295 | Improper Certificate Validation                                                            |  4.06 |
