# gitlinks

- https://github.com/Ignitetechnologies/Web-Application-Cheatsheet/blob/master/README.md
- https://github.com/OlivierLaflamme/Cheatsheet-God
- https://github.com/dennyzhang/cheatsheet-kubernetes-A4
- https://github.com/inforion/idapython-cheatsheet
- https://github.com/mleblebici
- https://github.com/rstacruz/cheatsheets
- https://github.com/security-cheatsheet
- https://github.com/EbookFoundation/free-programming-books/blob/master/more/free-programming-cheatsheets.md

# others

- [BashPitFalls](http://mywiki.wooledge.org/BashPitfalls)
- [shelldorado](http://shelldorado.com/shelltips/beginner.html)
- [ss64](http://ss64.com/nt/)
- [cheat-sheats](http://www.cheat-sheets.org/)
- [infosecwriters](http://www.infosecwriters.com/Papers/nessusNMAPcheatSheet.pdf)
- [nixtutor](http://www.nixtutor.com/linux/all-the-best-linux-cheat-sheets/)
- [robvanderwoude](http://www.robvanderwoude.com/ntadmincommands.php)
- [john the ripper](https://countuponsecurity.files.wordpress.com/2015/06/jtr-cheatsheetimg.png)
- [danielmiessler](https://danielmiessler.com/study/tcpdump/)
- [devdocs](https://devdocs.io/)
- [devhints](https://devhints.io/)
- [IPv4_subnetting_reference](https://en.wikipedia.org/wiki/IPv4_subnetting_reference)
- [nmap](https://hakin9.org/nmap-cheat-sheet/)
- [lzone.de](https://lzone.de/cheat-sheet/)
- [portswigger XSS](https://portswigger.net/web-security/cross-site-scripting/cheat-sheet)
- [rstforums](https://rstforums.com/forum/22324-hacking-tools-windows.rst)
- [service name port numbers](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml)
- [OWASP](https://www.owasp.org/index.php/Cheat_Sheets)
- [SANS netcat](https://www.sans.org/security-resources/sec560/netcat_cheat_sheet_v1.pdf)
