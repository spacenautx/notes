# payloads

- xss
- https://github.com/Proviesec/xss-payload-list
- https://gbhackers.com/top-500-important-xss-cheat-sheet/
- pwd
- ssi
- lfi
- directories
- shellcodes
- dorks
- payloads

- https://digitalinvestigator.blogspot.com/2022/06/wordlists-for-penetration-testing.html
- https://github.com/sh377c0d3/Payloads
- https://github.com/Bo0oM/fuzz.txt
- https://wordlists.assetnote.io
- https://github.com/minimaxir/big-list-of-naughty-strings
- https://github.com/dsignr/disallowed-usernames/blob/master/disallowed%20usernames.csv
- https://github.com/danielmiessler/SecLists/blob/master/Fuzzing/Polyglots/SQLi_Polyglots.txt

- https://github.com/fuzzdb-project/fuzzdb/tree/master/attack/sql-injection

- https://github.com/tennc/fuzzdb/tree/master/dict/BURP-PayLoad
- https://github.com/tennc/fuzzdb/tree/master/attack-payloads/sql-injection

- https://github.com/1N3/IntruderPayloads
- https://github.com/1N3/IntruderPayloads/blob/master/burpattack_sqli_authbypass
- https://github.com/1N3/IntruderPayloads/blob/master/burpattack_sqli_error_based
- https://github.com/1N3/IntruderPayloads/blob/master/burpattack_sqli_time_based
- https://github.com/trietptm/SQL-Injection-Payloads

- https://github.com/payloadbox

- https://github.com/foospidy/payloads
- https://github.com/foospidy/payloads/tree/master/other/sqli
- https://github.com/foospidy/payloads/tree/master/owasp/fuzzing_code_database/sqli
- https://github.com/foospidy/payloads/blob/master/owasp/jbrofuzz/sqli.txt
- https://github.com/client9/libinjection/blob/master/data/sqli-misc.txt
- https://github.com/TeamMentor/TM_Temp_Scripts
- https://github.com/arvinddoraiswamy/mywebappscripts/tree/master/FuzzLists
- https://github.com/owtf/owtf/blob/develop/dictionaries/attack_vectors/web/sqli/basic.txt
- https://rmccurdy.com/scripts/sql.txt

- https://github.com/danTaler/detectionString
- https://github.com/NickSanzotta/BurpIntruder

- https://github.com/quantumvm/JavaReverseTCPShell
- https://github.com/TBGSecurity/splunk_shells
- https://github.com/praetorian-inc/pyshell
- https://github.com/danielmiessler/RobotsDisallowed
- https://github.com/EgeBalci/ARCANUS
- https://github.com/nccgroup/Winpayloads
- https://github.com/epinna/weevely3
- https://github.com/foospidy/payloads
- https://github.com/EgeBalci/HERCULES
- https://github.com/4w4k3/Insanity-Framework
- https://github.com/gabemarshall/Brosec
- https://github.com/khr0x40sh/MacroShop
- https://github.com/nccgroup/demiguise
- https://github.com/Mr-Un1k0d3r/ClickOnceGenerator
- https://github.com/swisskyrepo/PayloadsAllTheThings
- https://github.com/TheKingOfDuck/fuzzDicts

- default
- https://github.com/ihebski/DefaultCreds-cheat-sheet
- https://github.com/assetnote/commonspeak2-wordlists
- https://github.com/random-robbie/bruteforce-lists
- https://wordlists.assetnote.io
- https://github.com/mishrasunny174/WordLists
- https://github.com/kaimi-io/web-fuzz-wordlists
- https://github.com/ghostlulzhacks/wordlist
- https://github.com/ZephrFish/Wordlists
- https://github.com/danielmiessler%5D/RobotsDisallowed
- https://github.com/swisskyrepo/PayloadsAllTheThings
- https://github.com/1N3/IntruderPayloads
- https://github.com/cujanovic
- https://github.com/lavalamp-/password-lists
- https://github.com/arnaudsoullie/ics-default-passwords
- https://github.com/jeanphorn/wordlist
- https://github.com/j3ers3/PassList
- https://github.com/nyxxxie/awesome-default-passwords
- https://github.com/six2dez/OneListForAll
- https://github.com/c4b3rw0lf/wordlist-collection
- https://github.com/jeanphorn/wordlist
- https://github.com/gmelodie/awesome-wordlists
- https://github.com/Screetsec/Wordlist-Dracos
- https://github.com/fuzzdb-project/fuzzdb
- https://github.com/danielmiessler/SecLists
- https://github.com/xmendez/wfuzz
- https://github.com/minimaxir/big-list-of-naughty-strings
- https://github.com/7hang/Fuzz_dic
- https://github.com/emadshanab/Content-Discovery-Web-Dir-Bruteforce-wordlists-Collection
