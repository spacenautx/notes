### quine
- [https://github.com/mame/quine-relay](https://github.com/mame/quine-relay)
- [https://github.com/aztek/awesome-self-reference](https://github.com/aztek/awesome-self-reference)
- [https://github.com/corkami/collisions/tree/master](https://github.com/corkami/collisions/tree/master)
- [https://github.com/makenowjust/quine](https://github.com/makenowjust/quine)
- [https://oisinmoran.com/quinetweet](https://oisinmoran.com/quinetweet)
- [https://code-poetry.com](https://code-poetry.com)
- [https://cs.lmu.edu/~ray/notes/quineprograms/](https://cs.lmu.edu/~ray/notes/quineprograms/)
- [http://www.iwriteiam.nl/SigProgC.html](http://www.iwriteiam.nl/SigProgC.html)
- [https://stackoverflow.com/questions/1995113/strangest-language-feature](https://stackoverflow.com/questions/1995113/strangest-language-feature)
- [https://twitter.com/David3141593/status/1573218394358386688](https://twitter.com/David3141593/status/1573218394358386688)
- [https://aem1k.com/world/](https://aem1k.com/world/)

### codegolf
- [codegolf js](https://gist.github.com/xem/206db44adbdd09bac424)

### creepy code
- [CreepyCodeCollection](https://github.com/MinhasKamal/CreepyCodeCollection)
- [pdf with its own md5](https://github.com/zhuzilin/pdf-with-its-own-md5)
