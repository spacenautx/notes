- https://jvns.ca/blog/2014/08/12/what-happens-if-you-write-a-tcp-stack-in-python/
 
 
###### *Let's Code a TCP/IP Stack*

## Part 1: Ethernet & ARP


Writing your own TCP/IP stack may seem like a daunting task. Indeed, TCP has accumulated many specifications over its lifetime of more than thirty years. The core specification, however, is seemingly compact1 - the important parts being TCP header parsing, the state machine, congestion control and retransmission timeout computation.
The most common layer 2 and layer 3 protocols, Ethernet and IP respectively, pale in comparison to TCP’s complexity. In this blog series, we will implement a minimal userspace TCP/IP stack for Linux.
The purpose of these posts and the resulting software is purely educational - to learn network and system programming at a deeper level.
Contents

- TUN/TAP devices
- Ethernet Frame Format
- Ethernet Frame Parsing
- Address Resolution Protocol
- Address Resolution Algorithm
- Conclusion
- Sources

## TUN/TAP devices

To intercept low-level network traffic from the Linux kernel, we will use a Linux TAP device. In short, a TUN/TAP device is often used by networking userspace applications to manipulate L3/L2 traffic, respectively. A popular example is tunneling, where a packet is wrapped inside the payload of another packet.
The advantage of TUN/TAP devices is that they’re easy to set up in a userspace program and they are already being used in a multitude of programs, such as OpenVPN.
As we want to build the networking stack from the layer 2 up, we need a TAP device. We instantiate it like so:

```c
/*
 * Taken from Kernel Documentation/networking/tuntap.txt
 */
int tun_alloc(char *dev)
{
    struct ifreq ifr;
    int fd, err;

    if( (fd = open("/dev/net/tap", O_RDWR)) < 0 ) {
        print_error("Cannot open TUN/TAP dev");
        exit(1);
    }

    CLEAR(ifr);

    /* Flags: IFF_TUN   - TUN device (no Ethernet headers)
     *        IFF_TAP   - TAP device
     *
     *        IFF_NO_PI - Do not provide packet information
     */
    ifr.ifr_flags = IFF_TAP | IFF_NO_PI;
    if( *dev ) {
        strncpy(ifr.ifr_name, dev, IFNAMSIZ);
    }

    if( (err = ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0 ){
        print_error("ERR: Could not ioctl tun: %s\n", strerror(errno));
        close(fd);
        return err;
    }

    strcpy(dev, ifr.ifr_name);
    return fd;
}
```

After this, the returned file descriptor fd can be used to read and write data to the virtual device’s ethernet buffer.
The flag IFF_NO_PI is crucial here, otherwise we end up with unnecessary packet information prepended to the Ethernet frame. You can actually take a look at the kernel’s source code of the tun-device driver and verify this yourself.

** Ethernet Frame Format

The multitude of different Ethernet networking technologies are the backbone of connecting computers in Local Area Networks (LANs). As with all physical technology, the Ethernet standard has greatly evolved from its first version2, published by Digital Equipment Corporation, Intel and Xerox in 1980.
The first version of Ethernet was slow in today’s standards - about 10Mb/s and it utilized half-duplex communication, meaning that you either sent or received data, but not at the same time. This is why a Media Access Control (MAC) protocol had to be incorporated to organize the data flow. Even to this day, Carrier Sense, Multiple Access with Collision Detection (CSMA/CD) is required as the MAC method if running an Ethernet interface in half-duplex mode.
The invention of the 100BASE-T Ethernet standard used twisted-pair wiring to enable full-duplex communication and higher throughput speeds. Additionally, the simultaneous increase in popularity of Ethernet switches made CSMA/CD largely obsolete.
The different Ethernet standards are maintained by the IEEE 802.33 working group.
Next, we’ll take a look at the Ethernet Frame header. It can be declared as a C struct followingly:

```c
#include <linux/if_ether.h>

struct eth_hdr
{
    unsigned char dmac[6];
    unsigned char smac[6];
    uint16_t ethertype;
    unsigned char payload[];
} __attribute__((packed));
```

The dmac and smac are pretty self-explanatory fields. They contain the MAC addresses of the communicating parties (destination and source, respectively).
The overloaded field, ethertype, is a 2-octet field, that depending on its value, either indicates the length or the type of the payload. Specifically, if the field’s value is greater or equal to 1536, the field contains the type of the payload (e.g. IPv4, ARP). If the value is less than that, it contains the length of the payload.
After the type field, there is a possibility of several different tags for the Ethernet frame. These tags can be used to describe the Virtual LAN (VLAN) or the Quality of Service (QoS) type of the frame. Ethernet frame tags are excluded from our implementation, so the corresponding field also does not show up in our protocol declaration.
The field payload contains a pointer to the Ethernet frame’s payload. In our case, this will contain an ARP or IPv4 packet. If the payload length is smaller than the minimum required 48 bytes (without tags), pad bytes are appended to the end of the payload to meet the requirement.
We also include the if_ether.h Linux header to provide a mapping between ethertypes and their hexadecimal values.
Lastly, the Ethernet Frame Format also includes the Frame Check Sequence field in the end, which is used with Cyclic Redundancy Check (CRC) to check the integrity of the frame. We will omit the handling of this field in our implementation.

** Ethernet Frame Parsing

The attribute packed in a struct’s declaration is an implementation detail - It is used to instruct the GNU C compiler not to optimize the struct memory layout for data alignment with padding bytes4. The use of this attribute stems purely out of the way we are “parsing” the protocol buffer, which is just a type cast over the data buffer with the proper protocol struct:

```c
struct eth_hdr *hdr = (struct eth_hdr *) buf;

A portable, albeit slightly more laborious approach, would be to serialize the protocol data manually. This way, the compiler is free to add padding bytes to conform better to different processor’s data alignment requirements.

The overall scenario for parsing and handling incoming Ethernet frames is straightforward:

if (tun_read(buf, BUFLEN) < 0) {
    print_error("ERR: Read from tun_fd: %s\n", strerror(errno));
}

struct eth_hdr *hdr = init_eth_hdr(buf);

handle_frame(&netdev, hdr);
```

The handle_frame function just looks at the ethertype field of the Ethernet header, and decides its next action based upon the value.

** Address Resolution Protocol
The Address Resolution Protocol (ARP) is used for dynamically mapping a 48-bit Ethernet address (MAC address) to a protocol address (e.g. IPv4 address). The key here is that with ARP, multitude of different L3 protocols can be used: Not just IPv4, but other protocols like CHAOS, which declares 16-bit protocol addresses.
The usual case is that you know the IP address of some service in your LAN, but to establish actual communications, also the hardware address (MAC) needs to be known. Hence, ARP is used to broadcast and query the network, asking the owner of the IP address to report its hardware address.
The ARP packet format is relatively straightforward:

```c
struct arp_hdr
{
    uint16_t hwtype;
    uint16_t protype;
    unsigned char hwsize;
    unsigned char prosize;
    uint16_t opcode;
    unsigned char data[];
} __attribute__((packed));
```

The ARP header (arp_hdr) contains the 2-octet hwtype, which determines the link layer type used. This is Ethernet in our case, and the actual value is 0x0001.
The 2-octet protype field indicates the protocol type. In our case, this is IPv4, which is communicated with the value 0x0800.
The hwsize and prosize fields are both 1-octet in size, and they contain the sizes of the hardware and protocol fields, respectively. In our case, these would be 6 bytes for MAC addresses, and 4 bytes for IP addresses.
The 2-octet field opcode declares the type of the ARP message. It can be ARP request (1), ARP reply (2), RARP request (3) or RARP reply (4).
The data field contains the actual payload of the ARP message, and in our case, this will contain IPv4 specific information:

```
struct arp_ipv4
{
    unsigned char smac[6];
    uint32_t sip;
    unsigned char dmac[6];
    uint32_t dip;
} __attribute__((packed));
```

The fields are pretty self explanatory. smac and dmac contain the 6-byte MAC addresses of the sender and receiver, respectively. sip and dip contain the sender’s and receiver’s IP addresses, respectively.
** Address Resolution Algorithm

The original specification depicts this simple algorithm for address resolution:
```

?Do I have the hardware type in ar$hrd?
Yes: (almost definitely)
    [optionally check the hardware length ar$hln]
?Do I speak the protocol in ar$pro?
Yes:
    [optionally check the protocol length ar$pln]
    Merge_flag := false
    If the pair <protocol type, sender protocol address> is
    already in my translation table, update the sender
    hardware address field of the entry with the new
    information in the packet and set Merge_flag to true.
?Am I the target protocol address?
Yes:
    If Merge_flag is false, add the triplet <protocol type,
    sender protocol address, sender hardware address> to
    the translation table.
?Is the opcode ares_op$REQUEST?  (NOW look at the opcode!!)
Yes:
    Swap hardware and protocol fields, putting the local
    hardware and protocol addresses in the sender fields.
    Set the ar$op field to ares_op$REPLY
    Send the packet to the (new) target hardware address on
    the same hardware on which the request was received.
```

Namely, the translation table is used to store the results of ARP, so that hosts can just look up whether they already have the entry in their cache. This avoids spamming the network for redundant ARP requests.
The algorithm is implemented in arp.c.
Finally, the ultimate test for an ARP implementation is to see whether it replies to ARP requests correctly:

```bash
[user@localhost lvl-ip]$ arping -I tap0 10.0.0.4
ARPING 10.0.0.4 from 192.168.1.32 tap0
Unicast reply from 10.0.0.4 [00:0C:29:6D:50:25]  3.170ms
Unicast reply from 10.0.0.4 [00:0C:29:6D:50:25]  13.309ms

[user@localhost lvl-ip]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.0.0.4                 ether   00:0c:29:6d:50:25   C                     tap0
```

The kernel’s networking stack recognized the ARP reply from our custom networking stack, and consequently populated its ARP cache with the entry of our virtual network device. Success!

*  Conclusion

The minimal implementation of Ethernet Frame handling and ARP is relatively easy and can be done in a few lines of code. On the contrary, the reward-factor is quite high, since you get to populate a Linux host’s ARP cache with your own make-belief Ethernet device!
The source code for the project can be found at GitHub.
In the next post, we’ll continue the implementation with ICMP echo & reply (ping) and IPv4 packet parsing.
If you liked this post, you can share it with your followers and follow me on Twitter!
Kudos to Xiaochen Wang, whose similar implementation proved invaluable for me in getting up to speed with C network programming and protocol handling. I find his source code5 easy to understand and some of my design choices were straight-out copied from his implementation.

*  Sources
    - https://tools.ietf.org/html/rfc7414 ↩
    - http://ethernethistory.typepad.com/papers/EthernetSpec.pdf ↩
    - https://en.wikipedia.org/wiki/IEEE_802.3 ↩
    - https://gcc.gnu.org/onlinedocs/gcc/Common-Type-Attributes.html#Common-Type-Attributes ↩
    - https://github.com/chobits/tapip ↩

## Part 2: IPv4 & ICMPv4

This time in our userspace TCP/IP stack we will implement a minimum viable IP layer and test it with ICMP echo requests (also known as pings).
We will take a look at the formats of IPv4 and ICMPv4 and describe how to check them for integrity. Some features, such as IP fragmentation, are left as an exercise for the reader.
For our networking stack IPv4 was chosen over IPv6 since it is still the default network protocol for the Internet. However, this is changing fast1 and our networking stack can be extended with IPv6 in the future.

=Contents=

- Internet Protocol version 4
    + Header Format
    + Internet Checksum
- Internet Control Message Protocol version 4
    + Header Format
    + Messages and their processing
- Testing the implementation
- Conclusion
- Sources

#### Internet Protocol version 4

The next layer (L3)2 in our implementation, after Ethernet frames, handles the delivery of data to a destination. Namely, the Internet Protocol (IP) was invented to provide a base for transport protocols such as TCP and UDP. It is connectionless, meaning that unlike TCP, all of the datagrams are handled independently of each other in the networking stack. This also means that IP datagrams may arrive out-of-order.3
Furthermore, IP does not guarantee successful delivery. This is a conscious choice taken by the protocol designers, since IP is meant to provide a base for protocols that likewise do not guarantee delivery. UDP is one such protocol.
If reliability between the communicating parties is required, a protocol such as TCP is used on top of IP. In that case, the higher level protocol is responsible for detecting missing data and making sure all of it is delivered.

*** Header Format

The IPv4 header is typically 20 octets in length. The header can contain trailing options, but they are omitted from our implementation. The meaning of the fields is relatively straightforward and can be described as a C struct:

```c
struct iphdr {
    uint8_t version : 4;
    uint8_t ihl : 4;
    uint8_t tos;
    uint16_t len;
    uint16_t id;
    uint16_t flags : 3;
    uint16_t frag_offset : 13;
    uint8_t ttl;
    uint8_t proto;
    uint16_t csum;
    uint32_t saddr;
    uint32_t daddr;
} __attribute__((packed));
```

The 4-bit version field indicates the format of the Internet header. In our case, the value will be 4 for IPv4.
The Internet Header Length field ihl is likewise 4 bits in length and indicates the number of 32-bit words in the IP header. Because the field is 4 bits in size, it can only hold a maximum value of 15. Thus the maximum length of an IP header is 60 octets (15 times 32 divided by eight).
The type of service field tos originates from the first IP specification4. It has been divided into smaller fields in later specifications, but for simplicity’s sake, we will treat the field as defined in the original specification. The field communicates the quality of service intended for the IP datagram.
The total length field len communicates the length of the whole IP datagram. As it is a 16-bit field, the maximum length is then 65535 bytes. Large IP datagrams are subject to fragmentation, in which they are split into smaller datagrams in order to satisfy the Maximum Transmission Unit (MTU) of different communication interfaces.
The id field is used to index the datagram and is ultimately used for reassembly of fragmented IP datagrams. The field’s value is simply a counter that is incremented by the sending party. In turn, the receiving side knows how to order the incoming fragments.
The flags field defines various control flags of the datagram. In specific, the sender can specify whether the datagram is allowed to be fragmented, whether it is the last fragment or that there’s more fragments incoming.
The fragment offset field, frag_offset, indicates the position of the fragment in a datagram. Naturally, the first datagram has this index set to 0.
The ttl or time to live is a common attribute that is used to count down the datagram’s lifetime. It is usually set to 64 by the original sender, and every receiver decrements this counter by one. When it hits zero, the datagram is to be discarded and possibly an ICMP message is replied back to indicate an error.
The proto field provides the datagram an inherent ability to carry other protocols in its payload. The field usually contains values such as 16 (UDP) or 6 (TCP), and is simply used to communicate the type of the actual data to the receiver.
The header checksum field, csum, is used to verify the integrity of the IP header. The algorithm for it is relatively simple, and will be explained further down in this tutorial.
Finally, the saddr and daddr fields indicate the source and destination addresses of the datagram, respectively. Even though the fields are 32-bit in length and thus provide a pool of approximately 4.5 billion addresses, the address range is going to be exhausted in the near-future5. The IPv6 protocol extends this length to 128 bits and as a result, future-proofs the address range of the Internet Protocol, perhaps permanently.

*** Internet Checksum
The Internet checksum field is used to check the integrity of an IP datagram. Calculating the checksum is relatively simple and is defined in the original specification4:
The checksum field is the 16 bit one’s complement of the one’s complement sum of all 16 bit words in the header. For purposes of computing the checksum, the value of the checksum field is zero.
The actual6 code for the algorithm is as follows:

```c
uint16_t checksum(void *addr, int count)
{
    /* Compute Internet Checksum for "count" bytes
     *         beginning at location "addr".
     * Taken from https://tools.ietf.org/html/rfc1071
     */

    register uint32_t sum = 0;
    uint16_t * ptr = addr;

    while( count > 1 )  {
        /*  This is the inner loop */
        sum += * ptr++;
        count -= 2;
    }

    /*  Add left-over byte, if any */
    if( count > 0 )
        sum += * (uint8_t *) ptr;

    /*  Fold 32-bit sum to 16 bits */
    while (sum>>16)
        sum = (sum & 0xffff) + (sum >> 16);

    return ~sum;
}
```

Take the example IP header 45 00 00 54 41 e0 40 00 40 01 00 00 0a 00 00 04 0a 00 00 05:

    Adding the fields together yields the two’s complement sum 01 1b 3e.
    Then, to convert it to one’s complement, the carry-over bits are added to the first 16-bits: 1b 3e + 01 = 1b 3f.
    Finally, the one’s complement of the sum is taken, resulting to the checksum value e4c0.

The IP header becomes 45 00 00 54 41 e0 40 00 40 01 e4 c0 0a 00 00 04 0a 00 00 05.

The checksum can be verified by applying the algorithm again and if the result is 0, the data is most likely good.
** Internet Control Message Protocol version 4

As the Internet Protocol lacks mechanisms for reliability, some way of informing communicating parties of possible error scenarios is required. As a result, the Internet Control Message Protocol (ICMP)7 is used for diagnostic measures in the network. An example of this is the case where a gateway is not reachable - the network stack that recognizes this sends an ICMP “Gateway Unreachable” message back to the origin.
*** Header Format

The ICMP header resides in the payload of the corresponding IP packet. The structure of the ICMPv4 header is as follows:

```c
struct icmp_v4 {
    uint8_t type;
    uint8_t code;
    uint16_t csum;
    uint8_t data[];
} __attribute__((packed));
```

Here, the type field communicates the purpose of the message. 42 different8 values are reserved for the type field, but only about 8 are commonly used. In our implementation, the types 0 (Echo Reply), 3 (Destination Unreachable) and 8 (Echo request) are used.

The code field further describes the meaning of the message. For example, when the type is 3 (Destination Unreachable), the code-field implies the reason. A common error is when a packet cannot be routed to a network: the originating host then most likely receives an ICMP message with the type 3 and code 0 (Net Unreachable).

The csum field is the same checksum field as in the IPv4 header, and the same algorithm can be used to calculate it. In ICMPv4 however, the checksum is end-to-end, meaning that also the payload is included when calculating the checksum.
*** Messages and their processing

The actual ICMP payload consists of query/informational messages and error messages. First, we’ll look at the Echo Request/Reply messages, commonly referred to as “pinging” in networking:

```c
struct icmp_v4_echo {
    uint16_t id;
    uint16_t seq;
    uint8_t data[];
} __attribute__((packed));
```

The message format is compact. The field id is set by the sending host to determine to which process the echo reply is intended. For example, the process id can be set in to this field.
The field seq is the sequence number of the echo and it is simply a number starting from zero and incremented by one whenever a new echo request is formed. This is used to detect if echo messages disappear or are reordered while in transit.
The data field is optional, but often contains information like the timestamp of the echo. This can then be used to estimate the round-trip time between hosts.
Perhaps the most common ICMPv4 error message, Destination Unreachable, has the following format:

```c
struct icmp_v4_dst_unreachable {
    uint8_t unused;
    uint8_t len;
    uint16_t var;
    uint8_t data[];
} __attribute__((packed));
```

The first octet is unused. Then, the len field indicates the length of the original datagram, in 4-octet units for IPv4. The value of the 2-octet field var depends on the ICMP code.
Finally, as much as possible of the original IP packet that caused the Destination Unreachable state is placed into the data field.

** Testing the implementation
From a shell, we can verify that our userspace networking stack responds to ICMP echo requests:

```bash
[user@localhost ~]$ ping -c3 10.0.0.4
PING 10.0.0.4 (10.0.0.4) 56(84) bytes of data.
64 bytes from 10.0.0.4: icmp_seq=1 ttl=64 time=0.191 ms
64 bytes from 10.0.0.4: icmp_seq=2 ttl=64 time=0.200 ms
64 bytes from 10.0.0.4: icmp_seq=3 ttl=64 time=0.150 ms

--- 10.0.0.4 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 1999ms
rtt min/avg/max/mdev = 0.150/0.180/0.200/0.024 ms
```

* Conclusion

A minimum viable networking stack that handles Ethernet frames, ARP and IP can be created relatively easily. However, the original specifications have been extended with many new ones. In this post, we skimmed over IP features such as options, fragmentation and the header DCN and DS fields.
Furthermore, IPv6 is crucial for the future of the Internet. It is not yet ubiquitous but being a newer protocol than IPv4, it definitely is something that should be implemented in our networking stack.
The source code for this blog post can be found at GitHub.
In the next blog post we will advance to the transport layer (L4) and start implementing the notorious Transmission Control Protocol (TCP). Namely, TCP is a connection-oriented protocol and ensures reliability between both communicating sides. These aspects obviously bring about more complexity, and being an old protocol, TCP has its dark corners.
If you liked this post, you can share it with your followers and follow me on Twitter!

* Sources
- https://en.wikipedia.org/wiki/IPv6_deployment ↩
- https://en.wikipedia.org/wiki/OSI_model ↩
- https://en.wikipedia.org/wiki/TCP/IP_Illustrated#Volume_1:_The_Protocols ↩
- http://tools.ietf.org/html/rfc791 ↩ ↩2
- https://en.wikipedia.org/wiki/IPv4_address_exhaustion ↩
- https://tools.ietf.org/html/rfc1071 ↩
- https://www.ietf.org/rfc/rfc792.txt ↩
- http://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml ↩

* =Part 3:= TCP Basics & Handshake


Now that our userspace TCP/IP stack has minimal implementations for Ethernet and IPv4, it is time to look into the dreaded Transmission Control Protocol (TCP).
Operating on the fourth OSI networking layer1, transport, TCP is responsible for repairing erroneous connections and faults in packet delivery. Indeed, TCP is the workhorse of the Internet, providing reliable communications in virtually all computer networking today.
TCP is not exactly a new protocol - the first specification came out in 19742. A lot of has changed since then and TCP has acquired many extensions and corrections3.
This post will delve into the basic theory behind TCP and attempts to give motivation for its design. Furthermore, we will look into the TCP header and discuss establishing a connection (TCP handshaking). As the last step, we’ll demonstrate the first functionality of TCP in our networking stack.

=Contents=
- Reliability mechanisms
- TCP Basics
- TCP Header Format
- TCP Handshake
- TCP Options
- Testing the TCP Handshake
- Conclusion
- Sources

** Reliability mechanisms

The problem of sending data reliably may seem superficial, but its actual implementation is involved. Mainly, several questions arise regarding error-repair in a datagram-style network:

    How long should the sender wait for an acknowledgement from the receiver?
    What if the receiver cannot process data as fast as it is sent?
    What if the network in between (a router, for example) cannot process data as fast as it is sent?

In all of the scenarios, the underlying dangers of packet-switched networks apply - an acknowledgement from the receiver can be corrupted or even lost in transmit, which leaves the sender in a tricky situation.
To combat these problems, several mechanisms can be used. Perhaps the most common is the sliding window technique, where both parties keep an account of the transmitted data. The window data is considered to be sequential (like a slice of an array) and that window “slides” forward as data is processed (and acknowledged) by both sides:

```
                 Left window edge             Right window edge
                       |                             |
                       |                             |
          ---------------------------------------------------------
          ...|    3    |    4    |    5    |    6    |    7    |...
          ---------------------------------------------------------
                  ^     ^                            ^    ^
                  |      \                          /     |
                  |       \                        /      |
             Sent and           Window size: 3         Cannot be
             ACKed                                     sent yet

```

The convenient property of using this kind of a sliding window is that it also alleviates the problem of flow control. Flow control is required, when the receiver cannot process data as fast it is sent. In this scenario, the size of the sliding window would be negotiated to be lower, resulting in throttled output from the sender.
Congestion control, on the other hand, helps the networking stacks in between the sender and receiver to not get congested. There are two general methods for this: in the explicit version, the protocol has a field for specifically informing the sender about the congestion status. In the implicit version, the sender tries to guess when the network is congested and should throttle its output. Overall, congestion control is a complex, recurrent networking problem with accompanying research still being done to this day.4

** TCP Basics

The underlying mechanisms in TCP are much more involved than in other protocols like UDP and IP. Namely, TCP is a connection-oriented protocol, which means that an unicast communication channel is established between exactly two sides as a first step. This connection is being actively taken care of by both sides: Establishing the connection (Handshaking), informing the other party of the data’s state and possible problems.
The other important property of TCP is that it is a streaming protocol. Unlike UDP, TCP does not guarantee applications steady “chunks” of data as they are sent and received. Rather, the TCP implementation has to buffer the data and when packets get lost, reordered or corrupted, TCP has to wait and organize the the data in the buffer. Only when the data is deemed intact, TCP can hand the data over to the application’s socket.
As TCP operates on data as a stream, the “chunks” from the stream have to be converted into packets that IP can carry. This is called packetization, where the TCP header contains the sequence number of the current index in the stream. This also has the convenient property that the stream can be broken into many variable-sized segments and TCP then knows how to repacketize them.
Similarly to IP, TCP also checks the message for integrity. This is achieved through the same checksum algorithm as in IP, but with added details. Mainly, the checksum is end-to-end, meaning that both the header and the data are included to the checksumming. In addition, a pseudo-header built from the IP header is included.
If a TCP implementation receives corrupted segments, it discards them and does not notify the sender. This error is corrected by a timer set by the sender, which can be used to retransmit a segment if it was never acknowledged by the receiver.
TCP is also a full-duplex system, meaning that traffic can flow simultaneously in both direction. This means that the communicating sides have to keep the sequencing of data to both directions in memory. In more depth, TCP preserves its traffic footprint by including an acknowledgement for the opposite traffic in the segments it sends.
In essence, the sequencing of the data stream is the main principle of TCP. The problem of keeping it synchronized, however, is not a simple one.

** TCP Header Format

Next, we’ll define the message header and describe its fields. The TCP header is seemingly simple, but contains a lot of information about the communication state.
The TCP header is 20 octets in size5:

```

        0                            15                              31
       -----------------------------------------------------------------
       |          source port          |       destination port        |
       -----------------------------------------------------------------
       |                        sequence number                        |
       -----------------------------------------------------------------
       |                     acknowledgment number                     |
       -----------------------------------------------------------------
       |  HL   | rsvd  |C|E|U|A|P|R|S|F|        window size            |
       -----------------------------------------------------------------
       |         TCP checksum          |       urgent pointer          |
       -----------------------------------------------------------------
```

The Source Port and Destination Port fields are used to establish multiple connections from and to hosts. Namely, the Berkeley sockets are the prevalent interface for applications to bind to the TCP networking stack. Through ports, the networking stack knows where to direct the traffic to. As the fields are 16 bits in size, the port values range from 0 to 65535.
Since every byte in the stream is numbered, the Sequence Number represents the TCP segment’s window index. When handshaking, this contains the Initial Sequence Number (ISN).
The Acknowledgment Number contains the window’s index of the next byte the sender expects to receive. After the handshake, the ACK field must always be populated.
The Header Length (HL) field presents the length of the header in 32-bit words.
Next, several flags are presented. The first 4 bits (rsvd) are not used.
    - Congestion Window Reduced (C) is used for informing that the sender reduced its sending rate.
    - ECN Echo (E) informs that the sender received a congestion notification.
    - Urgent Pointer (U) indicates that the segment contains prioritized data.
    - ACK (A) field is used to communicate the state of the TCP handshake. It stays on for the remainder of the connection.
    - PSH (P) is used to indicate that the receiver should “push” the data to the application as soon as possible.
    - RST (R) resets the TCP connection.
    - - SYN (S) is used to synchronize sequence numbers in the initial handshake.
    - FIN (F) indicates that the sender has finished sending data.

The Window Size field is used to advertise the window size. In other words, this is the number of bytes the receiver is willing to accept. Since it is a 16-bit field, the maximum window size is 65,535 bytes.
The TCP Checksum field is used to verify the integrity of the TCP segment. The algorithm is the same as for the Internet Protocol, but the input segment also contains the TCP data and also a pseudo-header from the IP datagram.
The Urgent Pointer is used when the U-flag is set. The pointer indicates the position of the urgent data in the stream.
After the header, several options can be provided. An example of these options is the Maximum Segment Size (MSS), where the sender informs the other side of the maximum size of the segments.
After the possible options, the actual data follows. The data, however, is not required. For example, the handshake is accomplished with only TCP headers.

** TCP Handshake

A TCP connection usually undergoes the following phases: connection setup (handshaking), data transfer and closing of the connection. The following diagrams depicts the usual handshaking routine of TCP:

```
          TCP A                                                TCP B
    1.  CLOSED                                               LISTEN
    2.  SYN-SENT    --> <SEQ=100><CTL=SYN>               --> SYN-RECEIVED
    3.  ESTABLISHED <-- <SEQ=300><ACK=101><CTL=SYN,ACK>  <-- SYN-RECEIVED
    4.  ESTABLISHED --> <SEQ=101><ACK=301><CTL=ACK>       --> ESTABLISHED
    5.  ESTABLISHED --> <SEQ=101><ACK=301><CTL=ACK><DATA> --> ESTABLISHED
```

    Host A’s socket is in a closed state, which means it is not accepting connections. On the contrary, host B’s socket that is bound to a specific port is listening for new connections.
    Host A intends to initiate a connection with host B. Thus, A crafts a TCP segment that has its SYN flag set and also the Sequence field populated with a value (100).
    Host B responds with a TCP segment that has its SYN and ACK fields set, and acknowledges A’s Sequence number by adding 1 to it (ACK=101). Likewise, B generates a sequence number (300).
    The 3-way handshake is finished by an ACK from the originator (A) of the connection request. The Acknowledgement field reflects the sequence number the host next expects to receive from the other side.
    Data starts to flow, mainly because both sides have acknowledged each other’s segment numbers.

This is the common scenario of establishing a TCP connection. However, several questions arise:

    - How is the initial Sequence number chosen?
    - What if both sides request a connection for each other at the same time?
    - What if segments are delayed for some time or indefinitely?

The Initial Sequence Number (ISN) is chosen independently by both communication parties at first contact. As it is a crucial part of identifying the connection, it has to be chosen so that it is most likely unique and not easily guessable. Indeed, the TCP Sequence Number Attack6 is the situation where an attacker can replicate a TCP connection and effectively feed data to the target, impersonating as a trusted host.
The original specification suggests that the ISN is chosen by a counter that increments every 4 microseconds. This, however, can be guessed by an attacker. In reality, modern networking stacks generate the ISN by more complicated methods.
The situation where both endpoints receive a connection request (SYN) from each other is called a Simultaneous Open. This is solved by an extra message exchange in the TCP handshake: Both sides send an ACK (without knowing that the other side has done it as well), and both sides SYN-ACK the requests. After this, data transfer commences.
Lastly, the TCP implementation has to have a timer for knowing when to give up on establishing a connection. Attempts are made to re-establish the connection, usually with an exponential backoff, but once the maximum retries or the time threshold is met, the connection is deemed to be non-existant.

** TCP Options

The last field in the TCP header segment is reserved for possible TCP options. The original specification provided three options, but later specifications have added many more. Next, we’ll look at the most common options.
The Maximum Segment Size (MSS) option informs the maximum TCP segment size the TCP implementation is willing to receive. The typical value for this is 1460 bytes in IPv4.
The Selective Acknowledgment (SACK) option optimizes the scenario when many packets are lost in transmit and the receiver’s window of data is filled with “holes”. To remedy the resulting degraded throughput, a TCP implementation can inform the sender of the specific packets it did not receive with SACK. Thus, the sender receives information about the state of the data in a more straight-forward manner than with the accumulating acknowledment scheme.
The Window Scale option increases the limited 16-bit window size. Namely, if both sides include this option in their handshake segments, the window size is multiplied with this scale. Having bigger window sizes are mainly important for bulk data transfer.
The Timestamps option allows the sender to place a timestamp into the TCP segment, which then can be used to calculate the RTT for each ACK segment. This information can then be used to calculate the TCP retransmission timeout.

** Testing the TCP Handshake

Now that we have the TCP handshake routine mocked and it effectively listens for every port, let’s test it:

```

[user@localhost ~]$ nmap -Pn 10.0.0.4 -p 1337

Starting Nmap 7.00 ( https://nmap.org ) at 2016-05-08 19:02 EEST
Nmap scan report for 10.0.0.4
Host is up (0.00041s latency).
PORT     STATE SERVICE
1337/tcp open  waste

Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds

```

Because of the fact that nmap does a SYN-scan (it only waits for the SYN-ACK to decide if the target’s port is open), it is easy to fool it to think we have an application listening on the port by just returning a SYN-ACK TCP segment.

** Conclusion
The minimum viable TCP handshake routine can be done relatively effortless by just picking a Sequence number, setting the SYN-ACK flags and calculating the checksum for the resulting TCP segment.
Next time, we’ll look into the most important responsibility of TCP: Reliable data transfer. Managing the window of the stream is essential for transmitting data with TCP and the logic for it can get somewhat complex.
Furthermore, providing applications a way to bind to the TCP implementation is done with sockets. Thus, we’ll look into the Berkeley Socket API and see if we can mock it for applications, enabling them to use our custom TCP implementation.
The source code for the project is hosted at GitHub.
If you liked this post, you can share it with your followers and follow me on Twitter!

** Sources

- https://en.wikipedia.org/wiki/OSI_model ↩
- https://tools.ietf.org/html/rfc675 ↩
- https://tools.ietf.org/html/rfc7414 ↩
- https://en.wikipedia.org/wiki/TCP/IP_Illustrated#Volume_1:_The_Protocols ↩
- http://www.tcpdump.org/tcpdump_man.html ↩
- http://www.ietf.org/rfc/rfc1948.txt ↩

* =Part 4:= TCP Data Flow & Socket API

Previously, we introduced ourselves to the TCP header and how a connection is established between two parties.
In this post, we will look into TCP data communication and how it is managed.
Additionally, we will provide an interface from the networking stack that applications can use for network communication. This Socket API is then utilized by our example application to send a simple HTTP request to a website.

=Contents=

- Transmission Control Block
- TCP Data Communication
- TCP Connection Termination
- Socket API
- Testing our Socket API
- Conclusion
- Sources

** Transmission Control Block

It is beneficial to start the discussion on TCP data management by defining the variables that record the data flow state.
In short, the TCP has to keep track of the sequences of data it has sent and received acknowledgments for. To achieve this, a data structure called the Transmission Control Block (TCB) is initialized for every opened connection.
The variables for the outgoing (sending) side are:

```
    Send Sequence Variables

      SND.UNA - send unacknowledged
      SND.NXT - send next
      SND.WND - send window
      SND.UP  - send urgent pointer
      SND.WL1 - segment sequence number used for last window update
      SND.WL2 - segment acknowledgment number used for last window update
      ISS     - initial send sequence number

```

In turn, the following data is recorded for the receiving side:

```
    Receive Sequence Variables

      RCV.NXT - receive next
      RCV.WND - receive window
      RCV.UP  - receive urgent pointer
      IRS     - initial receive sequence number
```

Additionally, helper variables of the current segment being processed are defined followingly:

```
    Current Segment Variables

      SEG.SEQ - segment sequence number
      SEG.ACK - segment acknowledgment number
      SEG.LEN - segment length
      SEG.WND - segment window
      SEG.UP  - segment urgent pointer
      SEG.PRC - segment precedence value
```

Together, these variables constitute most of the TCP control logic for a given connection.

** TCP Data Communication

Once a connection is established, explicit handling of the data flow starts. Three variables from the TCB are important for basic tracking of the state:

```
    SND.NXT - The sender will track the next sequence number to use in SND.NXT.
    RCV.NXT - The receiver records the next sequence number to expect in RCV.NXT.
    SND.UNA - The sender will record the oldest unacknowledged sequence number in SND.UNA.
```

Given a sufficient time period when TCP is managing the data communication and no transmit occurs, all these three variables will be equal.
For example, when A decides to send a segment with data to B, the following happens:

```
    TCP A sends a segment and advances SND.NXT in its own records (TCB).

    TCB B receives the segment and acknowledges it by advancing RCV.NXT and sends an ACK.

    TCB A receives the ACK and advances SND.UNA.
```

The amount by which the variables are advanced is the length of the data in the segment.
This is the basis for TCP control logic over the transmit of data. Let’s see how this looks like with tcpdump(1), a popular utility for capturing network traffic:

```
[user@localhost level-ip]$ sudo tcpdump -i any port 8000 -nt
IP 10.0.0.4.12000 > 10.0.0.5.8000: Flags [S], seq 1525252, win 29200, length 0
IP 10.0.0.5.8000 > 10.0.0.4.12000: Flags [S.], seq 825056904, ack 1525253, win 29200, options [mss 1460], length 0
IP 10.0.0.4.12000 > 10.0.0.5.8000: Flags [.], ack 1, win 29200, length 0
```

The address 10.0.0.4 (host A) initiates a connection from port 12000 to host 10.0.0.5 (host B) listening on port 8000.
After the three-way handshake, the connection is established and their internal TCP socket state is set to ESTABLISHED. Initial sequence numbers are 1525252 for A, and 825056904 for B.

```
IP 10.0.0.4.12000 > 10.0.0.5.8000: Flags [P.], seq 1:18, ack 1, win 29200, length 17
IP 10.0.0.5.8000 > 10.0.0.4.12000: Flags [.], ack 18, win 29200, length 0
```

Host A sends a segment with 17 bytes of data, which host B acknowledges with an ACK segment. Relative sequence numbers are shown by default with tcpdump to ease readability. Thus, ack 18 is actually 1525253 + 17.
Internally, the TCP of the receiving host (B) has advanced RCV.NXT with the number 17.

```
IP 10.0.0.4.12000 > 10.0.0.5.8000: Flags [.], ack 1, win 29200, length 0
IP 10.0.0.5.8000 > 10.0.0.4.12000: Flags [P.], seq 1:18, ack 18, win 29200, length 17
IP 10.0.0.4.12000 > 10.0.0.5.8000: Flags [.], ack 18, win 29200, length 0
IP 10.0.0.5.8000 > 10.0.0.4.12000: Flags [P.], seq 18:156, ack 18, win 29200, length 138
IP 10.0.0.4.12000 > 10.0.0.5.8000: Flags [.], ack 156, win 29200, length 0
IP 10.0.0.5.8000 > 10.0.0.4.12000: Flags [P.], seq 156:374, ack 18, win 29200, length 218
IP 10.0.0.4.12000 > 10.0.0.5.8000: Flags [.], ack 374, win 29200, length 0
```

The interplay of sending data and acknowledging it continues. As can be seen, the segments with length 0 only have the ACK flag set, but the acknowledgement sequence numbers are precisely increment based on the previously received segment’s length.

```
IP 10.0.0.5.8000 > 10.0.0.4.12000: Flags [F.], seq 374, ack 18, win 29200, length 0
IP 10.0.0.4.12000 > 10.0.0.5.8000: Flags [.], ack 375, win 29200, length 0
```

Host B informs host A that it has no more data to send by generating a FIN segment. In turn, host A acknowledges this.

To finish the connection, host A also has to signal that it has no more data to send.

** TCP Connection Termination

Closing a TCP connection is likewise an involved operation, and can be forcibly terminated (RST) or finished with a mutual agreement (FIN).
The basic scenario is as follows:

```
The active closer sends a FIN segment.
The passive closer acknowledges this by sending an ACK segment.
The passive closer starts its own close operation (when it has no more data to send) and effectively becomes an active closer.
Once both sides have sent a FIN to each other and they have acknowledged them to both directions, the connection closes.
```

Evidently, the closing of a TCP connection requires four segments, in contrast to the three segments of the TCP connection establishment (three-way handshake).
Additionally, TCP is a bi-directional protocol, so it is possible to have the other end announce it has no more data to send, but stay online for incoming data. This is called TCP Half-close.
The unreliable nature of packet-switched networks introduce additional complexity to the connection termination - FIN segments can disappear or never intentionally be sent, which leaves the connection in an awkward state. For example, in Linux the kernel parameter tcp_fin_timeout controls how many seconds TCP waits for a final FIN packet, before forcibly closing the connection. This is a violation of the specification, but is needed for Denial of Service (DoS) prevention.1
Aborting a connection involves a segment with the RST flag set. Resets can occur because of many reasons, but some usual ones are:

    - Connection request to a nonexistent port or interface
    - The other TCP has crashed and ends up in a out-of-sync connection state
    - Attempts to disturb existing connections2

Thus, the happy path of TCP data transmission never involves a RST segment.

** Socket API

To be able to utilize the networking stack, some kind of an interface has to be provided for applications. The BSD Socket API is the most famous one and it originates from the 4.2BSD UNIX release from 1983.3 The Socket API in Linux is compatible to the BSD Socket API.4
A socket is reserved from the networking stack by calling socket(2), passing the type of the socket and protocol as parameters. Common values are AF_INET for the type and SOCK_STREAM as domain. This will default to a TCP-over-IPv4 socket.
After succesfully reserving a TCP socket from the networking stack, it will be connected to a remote endpoint. This is where connect(2) is used and calling it will launch the TCP handshake.
From that point on, we can just write(2) and read(2) data from our socket.
The networking stack will handle queueing, retransmission, error-checking and reassembly of the data in the TCP stream. For the application, the inner acting of TCP is mostly opaque. The only thing the application can rely on is that the TCP has acknowledged the responsibility of sending and receiving the stream of data, and that it will inform the application of unexpected behavior through the socket API.
As an example, let’s look at the system calls that a simple invocation of curl(1) does:

```c
strace -esocket,connect,sendto,recvfrom,close curl -s example.com > /dev/null
socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) = 3
connect(3, {sa_family=AF_INET, sin_port=htons(80), sin_addr=inet_addr("93.184.216.34")}, 16) = -1 EINPROGRESS (Operation now in progress)
sendto(3, "GET / HTTP/1.1\r\nHost: example.co"..., 75, MSG_NOSIGNAL, NULL, 0) = 75
recvfrom(3, "HTTP/1.1 200 OK\r\nCache-Control: "..., 16384, 0, NULL, NULL) = 1448
close(3)                                = 0
+++ exited with 0 +++
```

We observe the Socket API calls with strace(1), a tool for tracing system calls and signals. The steps are:

    - A socket is opened with socket, and the type is specified as IPv4/TCP.
    - connect launches the TCP handshake. Destination address and port are passed to the function.
    - After the connection is successfully established, sendto(3) is used to write data to the socket - in this case, the usual HTTP GET request.
    - From that point on, curl eventually reads the incoming data with recvfrom.

The astute reader may have noticed that no read or write system calls were placed. This is because the actual socket API does not contain those functions, but normal I/O operations can also be used. From man socket(7):4
In addition, the standard I/O operations like write(2), writev(2), sendfile(2), read(2), and readv(2) can be used to read and write data.
In the end, the Socket API contains multiple functions for just writing and reading data. This is complicated by the I/O family of functions, which can also be used to manipulate the socket’s file descriptor.

** Testing our Socket API
Now that our networking stack provides a socket interface, we can write applications against it.
The popular tool curl is used to transmit data with a given protocol. We can replicate the HTTP GET behavior of curl by writing a minimal implementation:

```bash
./lvl-ip curl example.com 80
```

```html
...
<!doctype html>
<html>
<head>
    <title>Example Domain</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>

<body>
<div>
    <h1>Example Domain</h1>
    <p>This domain is established to be used for illustrative examples in documents. You may use this
    domain in examples without prior coordination or asking for permission.</p>
    <p><a href="http://www.iana.org/domains/example">More information...</a></p>
</div>
</body>
</html>
```

In the end, sending a HTTP GET request exercises the underlying networking stack only minimally.

* Conclusion
We have now essentially implemented a rudimentary TCP with simple data management and provided an interface applications can use.
However, TCP data communication is not a simple problem. The packets can get corrupted, reordered or lost in transit. Furthermore, the data transmit can congest arbitrary elements in the network.
For this, the TCP data communication needs to include more sophisticated logic. In the next post, we will look into TCP Window Management and TCP Retransmission Timeout mechanisms, to better cope with more challenging settings.
The source code for the project is hosted at GitHub.
If you liked this post, you can share it with your followers and follow me on Twitter!

* Sources
- https://linux.die.net/man/7/tcp ↩
- https://en.wikipedia.org/wiki/TCP_reset_attack ↩
- http://www.kohala.com/start/tcpipiv2.html ↩
- http://man7.org/linux/man-pages/man7/socket.7.html ↩ ↩2

* =Part 5:= TCP Retransmission


At this point we have a TCP/IP stack that is able to communicate to other hosts in the Internet. The implementation so far has been fairly straight-forward, but missing a major feature: Reliability.

Namely, our TCP does not guarantee the integrity of the data stream it presents to applications. Even establishing the connection can fail if the handshake packets are lost in transit.

Introducing reliability and control is our next main focus in creating a TCP/IP stack from scratch.
Contents

    Automatic Repeat Request
    TCP Retransmission
    Karn’s Algorithm
    Managing the RTO timer
    Requesting retransmission
    Trying it out
    Conclusion
    Sources

## Automatic Repeat Request

The basis for many reliable protocols is the concept of Automatic Repeat reQuest (ARQ)1.
In ARQ, a receiver sends acknowledgments for data it has received, and the sender retransmits data it never received acknowledgements for.
As we have discussed, TCP keeps the sequence numbers of transmitted data in memory and responds with acknowledgments. The transmitted data is put into a retransmission queue and timers associated with the data are started. If no acknowledgment for the sequence of data is received before the timer runs out, a retransmission occurs.
As can be seen, TCP builds its reliability on the principles of ARQ. However, detailed implementations of ARQ are involved. Simple questions like “how long should the sender wait for an acknowledgement?” are tricky to answer, especially when maximum performance is desired. TCP extensions like Selective Acknowledgments (SACK)2 alleviate efficiency problems by acknowledging out-of-order data and avoiding unnecessary round-trips.

## TCP Retransmission

Retransmissions in TCP are described in the original specification3 as:
When the TCP transmits a segment containing data, it puts a copy on a retransmission queue and starts a timer; when the acknowledgment for that data is received, the segment is deleted from the queue. If the acknowledgment is not received before the timer runs out, the segment is retransmitted.
However, the original formula for calculating the retransmission timeout was deemed inadequate for different network environments. The current “standard method”4 was described by Jacobson5 and the latest codified specification can be found from RFC62986.
The basic algorithm is relatively straightforward. For a given TCP sender, state variables are defined for calculating the timeout:

- srtt is smoothed round-trip time for averaging the round-trip time (RTT) of a segment
- rttvar holds the round-trip time variation
- rto eventually holds the retransmission timeout, e.g. in milliseconds

In short, srtt acts as a low-pass filter for consecutive RTTs. Due to possible large variation in the RTT, rttvar is used to detect those changes and prevent them from skewing the averaging function. Additionally, a clock granularity of G seconds is assumed.
As described in RFC62986, the steps for computation are as follows:
```

    Before the first RTT measurement:

    rto = 1000ms

    On the first RTT measurement R:

    srtt = R
    rttvar = R/2
    rto = srtt + max(G, 4*rttvar)

    On subsequent measurements:

    alpha = 0.125
    beta = 0.25
    rttvar = (1 - beta) * rttvar + beta * abs(srtt - r)
    srtt = (1 - alpha) * srtt + alpha * r
    rto = srtt + max(g, 4*rttvar)

```
After computing rto, if it is less than 1 second, round it up to 1 second. A maximum amount can be provided but it has to be at least 60 seconds
The clock granularity of TCP implementations has traditionally been estimated to be fairly high, ranging from 500ms to 1 second. Modern systems like Linux, however, use a clock granularity of 1 millisecond4.
One thing to note is that the RTO is suggested to always be at least 1 second. This is to guard against spurious retransmissions, i.e. when a segment is retransmitted too soon, causing congestion in the network. In practice, many implementations go for sub-second rounding: Linux uses 200 milliseconds.

## Karn’s Algorithm

One mandatory algorithm that prevents the RTT measurement from giving false results is Karn’s Algorithm7. It simply states that the RTT samples should not be taken for retransmitted packets.
In other words, the TCP sender keeps track whether the segment it sent was a retransmission and skips the RTT routine for those acknowledgments. This makes sense, since otherwise the sender could not distinguish acknowledgements between the original and retransmitted segment.
When utilizing the timestamp TCP option, however, the RTT can be measured for every ACK segment. We will deal with the TCP Timestamp option in a separate blog post.

## Managing the RTO timer
Managing the retransmission timer is relatively straightforward. RFC6298 recommends the following algorithm:

- When sending a data segment and the RTO timer is not running, activate it with the timeout value of rto
- When all outstanding data segments have been acknowledged, turn off the RTO timer
- When an ACK is received for new data, restart the RTO timer with the value of rto

And when the RTO timer expires:

- Retransmit the earliest unacknowledged segment
- Back off the RTO timer with a factor of 2, i.e. (rto = rto * 2)
- Start the RTO timer

Additionally, when the backing off of the RTO value occurs and a subsequent measurement is successfully made, the RTO value can shrink drastically. The TCP implementation may clear srtt and rttvar when backing off and waiting for an acknowledgment6.

** Requesting retransmission

A TCP is usually not only relying on the TCP sender’s timers to fix lost packets. The receiving side can also inform the sender that segments need to be retransmitted.
Duplicate acknowledgment is an algorithm where out-of-sequence segments are acknowledged, but by the sequence number of the latest in-order segment. After three duplicate acknowledgments, the TCP sender should realize that it needs to retransmit the segment that was advertised by the duplicate acknowledgments.
Furthermore, Selective Acknowledgment (SACK) is a more sophisticated version of the duplicate acknowledgment. It is a TCP option where the receiver is able to encode the received sequences into its acknowledgements. Then the sender immediately notices any lost segments and resends them. We will discuss the SACK TCP option in a later blog post.

## Trying it out

Now that we’ve gone through the concepts and general algorithms, let’s see how TCP retransmissions look like on the wire.

Let’s change the firewall rules to drop packets after the connection is established and try to fetch the front page of HN:

```bash
iptables -I FORWARD --in-interface tap0 -m conntrack --ctstate ESTABLISHED -j DROP

./tools/level-ip curl news.ycombinator.com
curl: (56) Recv failure: Connection timed out
```

Observing the connection traffic we see that the HTTP GET is retransmitted with roughly a doubling interval:
```

[user@localhost ~]$ sudo tcpdump -i tap0 host 10.0.0.4 -n -ttttt
00:00:00.000000 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [S], seq 3975419138, win 44477, options [mss 1460], length 0
00:00:00.004318 IP 104.20.44.44.80 > 10.0.0.4.41733: Flags [S.], seq 4164704437, ack 3975419139, win 29200, options [mss 1460], length 0
00:00:00.004534 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [.], ack 1, win 44477, length 0
00:00:00.011039 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [P.], seq 1:85, ack 1, win 44477, length 84: HTTP: GET / HTTP/1.1
00:00:01.094237 IP 104.20.44.44.80 > 10.0.0.4.41733: Flags [S.], seq 4164704437, ack 3975419139, win 29200, options [mss 1460], length 0
00:00:01.094479 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [.], ack 1, win 44477, length 0
00:00:01.210787 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [P.], seq 1:85, ack 1, win 44477, length 84: HTTP: GET / HTTP/1.1
00:00:03.607225 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [P.], seq 1:85, ack 1, win 44477, length 84: HTTP: GET / HTTP/1.1
00:00:08.399056 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [P.], seq 1:85, ack 1, win 44477, length 84: HTTP: GET / HTTP/1.1
00:00:18.002415 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [P.], seq 1:85, ack 1, win 44477, length 84: HTTP: GET / HTTP/1.1
00:00:37.289491 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [P.], seq 1:85, ack 1, win 44477, length 84: HTTP: GET / HTTP/1.1
00:01:15.656151 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [P.], seq 1:85, ack 1, win 44477, length 84: HTTP: GET / HTTP/1.1
00:02:32.590664 IP 10.0.0.4.41733 > 104.20.44.44.80: Flags [P.], seq 1:85, ack 1, win 44477, length 84: HTTP: GET / HTTP/1.1
```

Verifying the retransmission back-off and the receiver going silent is easy, but what about the scenario when retransmissions are triggered only for some segments? For optimal performance, the RTO algorithm needs to ‘bounce back’ when the connection is detected as healthy.

Let’s set the firewall rule to only block the 6th packet for 6000 bytes:
```bash
$ iptables -I FORWARD --in-interface tap0 -m connbytes --connbytes 6 --connbytes-dir original --connbytes-mode packets -m quota --quota 6000 -j DROP

```
Now, if we try to send some data, our TCP has to recognize the communication blackout and when it ends. Let’s send 6009 bytes:
```bash
./tools/level-ip curl -X POST http://httpbin.org/post -d "payload=$(perl -e "print 'lorem ipsum ' x500")"

```

Let’s step through the connection phases, see when retransmissions are triggered and how the RTO value changes. Below is a modified tcpdump output with inline comments about our TCP socket’s internal state:

```
00.000000 10.0.0.4.49951 > httpbin.org.80: [S], seq 1, options [mss 1460]
00.120709 httpbin.org.80 > 10.0.0.4.49951: [S.], seq 1, ack 1, options [mss 8961]
00.120951 10.0.0.4.49951 > httpbin.org.80: [.], ack 1

- Connection established, TCP RTO value of 10.0.0.4:49951 is 1000 milliseconds.

00.122686 10.0.0.4.49951 > httpbin.org.80: [P.], seq 1:174, ack 1: POST /post
00.242564 httpbin.org.80 > 10.0.0.4.49951: [.], ack 174
01.141287 10.0.0.4.49951 > httpbin.org.80: [.], seq 174:1634, ack 1: HTTP
01.141386 10.0.0.4.49951 > httpbin.org.80: [.], seq 1634:3094, ack 1: HTTP
01.141460 10.0.0.4.49951 > httpbin.org.80: [.], seq 3094:4554, ack 1: HTTP
01.263301 httpbin.org.80 > 10.0.0.4.49951: [.], ack 1634
01.265995 httpbin.org.80 > 10.0.0.4.49951: [.], ack 3094

- So far so good, the remote host has acked our HTTP POST
  and the start of our payload. RTO value is 336ms.

01.526797 10.0.0.4.49951 > httpbin.org.80: [.], seq 3094:4554, ack 1: HTTP
02.259425 10.0.0.4.49951 > httpbin.org.80: [.], seq 3094:4554, ack 1: HTTP
03.735553 10.0.0.4.49951 > httpbin.org.80: [.], seq 3094:4554, ack 1: HTTP

- The communication blackout caused by our iptables rule has started.
  Our TCP has to retransmit the segment multiple times.
  The RTO value of the socket keeps increasing:
    01.526797: 618ms
    02.259425: 1236ms
    03.735553: 2472ms

06.692867 10.0.0.4.49951 > httpbin.org.80: [.], seq 3094:4554, ack 1: HTTP
06.819115 httpbin.org.80 > 10.0.0.4.49951: [.], ack 4554

- Finally the remote host responds. Our RTO value has increased to 4944ms.
  Karn\'s Algorithm takes effect here: The new RTO value cannot be measured
  with the retransmitted segment, so we skip it.

06.819356 10.0.0.4.49951 > httpbin.org.80: [.], seq 4554:6014, ack 1: HTTP
06.819442 10.0.0.4.49951 > httpbin.org.80: [P.], seq 6014:6182, ack 1: HTTP
06.948678 httpbin.org.80 > 10.0.0.4.49951: [.], ack 6014
06.948917 httpbin.org.80 > 10.0.0.4.49951: [.], ack 6182

- Now we get ACKs on the first try and the network is relatively healthy again.
  Karn\'s Algorithm allows us to measure the new RTO:
    06.948678: 309ms
    06.948917: 309ms

06.948942 httpbin.org.80 > 10.0.0.4.49951: [P.], seq 1:26, ack 6182: HTTP 100 Continue
06.949014 httpbin.org.80 > 10.0.0.4.49951: [.], seq 26:1486, ack 6182: HTTP/1.1 200 OK
06.949145 10.0.0.4.49951 > httpbin.org.80: [.], ack 26
06.949816 httpbin.org.80 > 10.0.0.4.49951: [.], seq 1486:2946, ack 6182: HTTP
06.949894 httpbin.org.80 > 10.0.0.4.49951: [.], seq 2946:4406, ack 6182: HTTP
06.950029 10.0.0.4.49951 > httpbin.org.80: [.], ack 2946
06.950030 httpbin.org.80 > 10.0.0.4.49951: [.], seq 4406:5866, ack 6182: HTTP
06.950161 httpbin.org.80 > 10.0.0.4.49951: [P.], seq 5866:6829, ack 6182: HTTP
06.950287 10.0.0.4.49951 > httpbin.org.80: [.], ack 5866
06.950435 10.0.0.4.49951 > httpbin.org.80: [.], ack 6829
06.958155 10.0.0.4.49951 > httpbin.org.80: [F.], seq 6182, ack 6829
07.082998 httpbin.org.80 > 10.0.0.4.49951: [F.], seq 6829, ack 6183
07.083253 10.0.0.4.49951 > httpbin.org.80: [.], ack 6830

- The data communication and connection is finished.
  No significant changes to the RTO measurement occur.
```

** Conclusion
Retransmissions in TCP are an essential part of a robust implementation. A TCP has to survive and be performant in changing network environments, where for example the latencies can suddenly spike or the network path is blocked for a moment.
Next time, we will take a look at TCP Congestion Control for achieving maximum performance without degrading the network’s health.
I’d be happy if you try the project out and give feedback. See Getting Started on how to use it with cURL and Firefox!
If you liked this post, you can share it with your followers and follow me on Twitter!

## Sources
- https://en.wikipedia.org/wiki/Automatic_repeat_request ↩
- https://tools.ietf.org/html/rfc2018 ↩
- https://www.ietf.org/rfc/rfc793.txt ↩
- https://en.wikipedia.org/wiki/TCP/IP_Illustrated#Volume_1:_The_Protocols ↩ ↩2
- http://ee.lbl.gov/papers/congavoid.pdf ↩
- https://tools.ietf.org/html/rfc6298 ↩ ↩2 ↩3
- https://en.wikipedia.org/wiki/Karn%27s_Algorithm ↩
- [link](http://www.saminiir.com/lets-code-tcp-ip-stack-1-ethernet-arp/)
----------


## network layers
## TCP/IP

```
    +--------------------+-------------------+----------------------------------+
    |      OSI MODEL     |    TCP/IP MODEL   |       TCP/IP Protocol Suite      |
    +--------------------+-------------------+------+---+---+---+---+---+-------+
    |  Application Layer |                   |      |   |   |   |   |   |       |
    |                    |                   |      |   | T |   |   |   |       |
    +--------------------+                   |   H  | S | E | F | D | R |   S   |
    | Presentation Layer | Application Layer |   T  | M | L | T | N | I |   N   |
    |                    |                   |   T  | T | N | P | S | P |   M   |
    +--------------------+                   |   P  | P | E |   |   |   |   P   |
    |      Session Layer |                   |      |   | T |   |   |   |       |
    |                    |                   |      |   |   |   |   |   |       |
    +--------------------+-------------------+------+---+---+---+---+---+-------+
    |                                                                           |
    +--------------------+-------------------+------------------+---------------+
    |   Transport Layer  |  Transport Layer  |        TCP       |      UDP      |
    +--------------------+-------------------+------------------+---------------+
    |                                                                           |
    +--------------------+-------------------+------------------+-------+-------+
    |                    |                   |                  |  IGMP |  ICMP |
    |    Network Layer   |   Internet Layer  +------+     IP    +-------+-------+
    |                    |                   |  ARP |                           |
    +--------------------+-------------------+------+---------------------------+
    |                                                                           |
    +--------------------+-------------------+----------+-------+-------+-------+
    |   Data Link Layer  |      Network      |          |       |       |       |
    +--------------------+      Access       | Ethernet | Token |  ATM  | Frame |
    |   Physical Layer   |       Layer       |          |  Ring |       | Relay |
    +--------------------+-------------------+----------+-------+-------+-------+
```


| Physical Layer                                     |
|----------------------------------------------------|
| 1-Wire                                             |
| ARINC 818 Avionics Digital Video Bus               |
| Bluetooth physical layer                           |
| CAN bus (controller area network) physical layer   |
| DSL                                                |
| EIA RS-232 also: EIA-422, EIA-423, RS-449, RS-485  |
| Etherloop                                          |
| Ethernet physical layer 10BASE-T, 10BASE2,         |
| 10BASE5, 100BASE-TX, 100BASE-FX, 100BASE-T,        |
| 1000BASE-T, 1000BASE-SX and others                 |
| GSM air interface physical layer                   |
| G.hn/G.9960 physical layer                         |
| I²C, I²S                                           |
| IEEE 1394 interface                                |
| ISDN                                               |
| IRDA physical layer                                |
| ITU                                                |
| Mobile Industry Processor Interface physical layer |
| OTN Optical Transport Network                      |
| SPI                                                |
| SMB                                                |
| Telephone network modems                           |
| USB physical layer                                 |


| Data Layer                                                 |
|------------------------------------------------------------|
| ARP - Address Resolution Protocol                          |
| ARCnet                                                     |
| ATM                                                        |
| CDP - Cisco Discovery Protocol                             |
| CAN - Controller Area Network                              |
| Econet                                                     |
| Ethernet                                                   |
| EAPS - Ethernet Automatic Protection Switching             |
| FDDI - Fiber Distributed Data Interface                    |
| Frame Relay                                                |
| HDLC - High-Level Data Link Control                        |
| IEEE 802.2 - provides LLC functions to IEEE 802 MAC layers |
| IEEE 802.11 - wireless LAN                                 |
| I²C                                                        |
| LattisNet                                                  |
| LAPD - Link Access Procedures, D channel                   |
| LLDP - Link Layer Discovery Protocol                       |
| LocalTalk                                                  |
| MIL-STD-1553                                               |
| MPLS - Multiprotocol Label Switching                       |
| NDP - Nortel Discovery Protocol                            |
| SDN - OpenFlow                                             |
| PPP - Point-to-Point Protocol                              |
| Profibus                                                   |
| SpaceWire                                                  |
| SLIP - Serial Line Internet Protocol (obsolete)            |
| SMLT - Split multi-link trunking                           |
| IEEE 802.1aq                                               |
| Spanning Tree Protocol                                     |
| StarLan                                                    |
| Token ring                                                 |
| UDLD - Unidirectional Link Detection                       |
| UNI/O                                                      |
| 1-Wire                                                     |

| Network Layer |                                            |
|---------------|--------------------------------------------|
| DDP           | Datagram Delivery Protocol                 |
| DVMRP         | Distance Vector Multicast Routing Protocol |
| ICMP          | Internet Control Message Protocol          |
| IGMP          | Internet Group Management Protocol         |
| IPsec         | Internet Protocol Security                 |
| IPv4/IPv6     | Internet Protocol                          |
| IPX           | Internetwork Packet Exchange               |
| PIM-DM        | Protocol Independent Multicast Dense Mode  |
| PIM-SM        | Protocol Independent Multicast Sparse Mode |
| RSMLT         | Routing Information Protocol               |
| SPB           | Shortest Path Bridging                     |

| Transport Layer |                                      |
|-----------------|--------------------------------------|
| ATP             | AppleTalk Transaction Protocol       |
| CUDP            | Cyclic UDP                           |
| DCCP            | Datagram Congestion Control Protocol |
| FCP             | Datagram Congestion Control Protocol |
| IL              | Fibre Channel Protocol               |
| MPTCP           | IL Protocol                          |
| RDP             | Multipath TCP                        |
| RUDP            | Reliable User Datagram Protocol      |
| SCTP            | Stream Control Transmission Protocol |
| SPX             | Sequenced Packet Exchange            |
| SST             | Structured Stream Transport          |
| TCP             | Transmission Control Protocol        |
| UDP             | User Datagram Protocol               |
| UDP-Lite        | User Datagram Protocol               |
| µTP             | Micro Transport Protocol             |

| Session Layer |                                                    |
|---------------|----------------------------------------------------|
| ADSP          | AppleTalk Data Stream Protocol                     |
| ASP           | AppleTalk Session Protocol                         |
| H.245         | Call Control Protocol for Multimedia Communication |
| ISO-SP        | OSI session-layer protocol (X.225, ISO 8327)       |
| iSNS          | Internet Storage Name Service                      |
| L2F           | Layer 2 Forwarding Protocol                        |
| L2TP          | Layer 2 Tunneling Protocol                         |
| NetBIOS       | Network Basic Input Output System                  |
| PAP           | Password Authentication Protocol                   |
| PPTP          | Point-to-Point Tunneling Protocol                  |
| RPC           | Remote Procedure Call Protocol                     |
| RTCP          | Real-time Transport Control Protocol               |
| SMPP          | Short Message Peer-to-Peer                         |
| SCP           | Session Control Protocol                           |
| SOCKS         | the SOCKS internet protocol, see Internet socket   |
| ZIP           | Zone Information Protocol                          |
| SDP           | Sockets Direct Protocol                            |

| Presentation Layer |                                            |
|--------------------|--------------------------------------------|
| CASE               | common application service element         |
| ACSE               | Association Control Service Element        |
| ROSE               | Remote Operation Service Element           |
| CCR                | Commitment Concurrency and Recovery        |
| RTSE               | Reliable Transfer Service Element          |
| SASE               | specific application service element       |
| FTAM               | File Transfer, Access and Manager          |
| VT                 | Virtual Terminal                           |
| MOTIS              | Message Oriented Text Interchange Standard |
| CMIP               | Common Management Information Protocol     |
| JTM                | Job Transfer and Manipulation              |
| MMS                | Manufacturing Messaging Service            |
| RDA                | Remote Database Access                     |
| DTP                | Distributed Transaction Processing         |

| Application Layer  |                                                |
|--------------------|------------------------------------------------|
| 9P                 | Plan 9 - Bell Labs distributed                 |
|                    | file system protocol                           |
| AFP                | Apple Filing Protocol                          |
| APPC               | Advanced Program-to-Program Communication      |
| AMQP               | Advanced Message Queuing Protocol              |
| Atom               | Publishing Protocol                            |
| BEEP               | Block Extensible Exchange Protocol             |
| Bitcoin            | Digital currency                               |
| BitTorrent         | peer-to-peer file sharing                      |
| CFDP               | Coherent File Distribution Protocol            |
| CoAP               | Constrained Application Protocol               |
| DDS                | Data Distribution Service                      |
| DeviceNet          | automation industry protocol                   |
| eDonkey            | classic file sharing protocol                  |
| ENRP               | Endpoint Handlespace Redundancy Protocol       |
| FastTrack          | filesharing, known from KaZaa and more         |
| Finger             | User Information Protocol                      |
| Freenet            | censorship resistant p2p network               |
| FTAM               | File Transfer Access and Management            |
| Gopher             | Gopher protocol                                |
| HL7                | Health Level Seven                             |
| HTTP               | HyperText Transfer Protocol                    |
| H.323              | Packet-Based Multimedia Communications System  |
| IRCP               | Internet Relay Chat Protocol                   |
| Kademlia           | p2p hashtables                                 |
| LDAP               | Lightweight Directory Access Protocol          |
| LPD                | Line Printer Daemon Protocol                   |
| MIME               | (S-MIME) Multipurpose Internet                 |
|                    | Mail Extensions and Secure MIME                |
| Modbus             | Serial communications protocol                 |
| MQTT               | Protocol MQ Telemetry Transport                |
| Netconf            | Network Configuration Protocol                 |
| NFS                | Network File System                            |
| NIS                | Network Information Service                    |
| NNTP               | Network News Transfer Protocol                 |
| NTCIP              | National Transportation Communications         |
|                    | for Intelligent Transportation System Protocol |
| NTP                | Network Time Protocol                          |
| OSCAR              | AOL Instant Messenger Protocol                 |
| PNRP               | Peer Name Resolution Protocol                  |
| RDP                | Remote Desktop Protocol                        |
| RELP               | Reliable Event Logging Protocol                |
| RIP                | Routing Information Protocol                   |
| Rlogin             | Remote Login in UNIX Systems                   |
| RPC                | Remote Procedure Call                          |
| RTMP               | Real Time Messaging Protocol                   |
| RTP                | Real-time Transport Protocol                   |
| RTPS               | Real Time Publish Subscribe                    |
| RTSP               | Real Time Streaming Protocol                   |
| SAP                | Session Announcement Protocol                  |
| SDP                | Session Description Protocol                   |
| SIP                | Session Initiation Protocol                    |
| SLP                | Service Location Protocol                      |
| SMB                | Server Message Block                           |
| SMTP               | Simple Mail Transfer Protocol                  |
| SNTP               | Simple Network Time Protocol                   |
| SSH                | Secure Shell                                   |
| SSMS               | Secure SMS Messaging Protocol                  |
| TCAP               | Transaction Capabilities Application Part      |
| TDS                | Tabular Data Stream                            |
| Tor                | anonymity network                              |
| TSP                | Time Stamp Protocol                            |
| VTP                | Virtual Terminal Protocol                      |
| Whois (and RWhois) | Remote Directory Access Protocol               |
| WebDAV             | Web Distributed Authoring and Versioning       |
| X.400              | Message Handling Service Protocol              |
| X.500              | Directory Access Protocol (DAP)                |
| XMPP               | Extensible Messaging and Presence Protocol     |

### resources
- https://blog.cloudflare.com/the-quantum-state-of-a-tcp-port/
