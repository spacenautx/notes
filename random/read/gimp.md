# gimp

##  Tools

### Menus
1.1. The Image Menu Bar
1.2. Context Menus
1.3. Tear-off menus
1.4. Tab menus

### The "File" Menu
2.1. Overview
2.2. New…
2.3. Create
2.4. Open…
2.5. Open as Layers…
2.6. Open Location…
2.7. Open Recent
2.8. Save
2.9. Save as…
2.10. Save a Copy…
2.11. Revert
2.12. Export…
2.13. Export As…
2.14. Create Template…
2.15. Print
2.16. Send by Email
2.17. Copy Image Location
2.18. Show in File Manager
2.19. Close
2.20. Close all
2.21. Quit

### The "Edit" Menu
3.1. “Edit” Menu Entries
3.2. Undo
3.3. Redo
3.4. Undo History
3.5. Cut
3.6. Copy
3.7. Copy Visible
3.8. Paste
3.9. Paste Into Selection
3.10. Paste In Place
3.11. Paste Into Selection In Place
3.12. Paste as
3.13. Buffer
3.14. Clear
3.15. Fill with FG Color
3.16. Fill with BG Color
3.17. Fill with Pattern
3.18. Fill Selection Outline
3.19. Fill Path
3.20. Stroke Selection
3.21. Stroke Path
3.22. The “Preferences” Command
3.23. Keyboard Shortcuts
3.24. Modules
3.25. Units

### The "Select" Menu
4.1. Introduction to the “Select” Menu
4.2. Select All
4.3. None
4.4. Invert
4.5. Float
4.6. By Color
4.7. From Path
4.8. Selection Editor
4.9. Feather
4.10. Sharpen
4.11. Shrink
4.12. Grow
4.13. Border
4.14. Remove Holes
4.15. Distort
4.16. Rounded Rectangle
4.17. Toggle QuickMask
4.18. Save to Channel
4.19. To Path

### The "View" Menu
5.1. Introduction to the “View” Menu
5.2. New View
5.3. Show All
5.4. Dot for Dot
5.5. Zoom
5.6. Flip and Rotate (0°)
5.7. Shrink Wrap
5.8. Full Screen
5.9. Navigation Window
5.10. Display Filters
5.11. Color Management
5.12. Show Selection
5.13. Show Layer Boundary
5.14. Show Guides
5.15. Show Grid
5.16. Show Sample Points
5.17. Snap to Guides
5.18. Snap to Grid
5.19. Snap to Canvas
5.20. Snap to Active Path
5.21. Padding Color
5.22. Show Menubar
5.23. Show Rulers
5.24. Show Scrollbars
5.25. Show Statusbar

### The "Image" Menu
6.1. Overview
6.2. Duplicate
6.3. Mode
6.4. RGB mode
6.5. Grayscale mode
6.6. Indexed mode
6.7. Precision
6.8. Color Management
6.9. Enable Color Management
6.10. Assign Color Profile
6.11. Convert to Color Profile
6.12. Discard Color Profile
6.13. Save Color Profile to File
6.14. Transform
6.15. Flip Horizontally; Flip Vertically
6.16. Rotation
6.17. Canvas Size
6.18. Fit Canvas to Layers
6.19. Fit Canvas to Selection
6.20. Print Size
6.21. Scale Image
6.22. Crop Image
6.23. Slice Using Guides
6.24. Zealous Crop
6.25. Merge Visible Layers
6.26. Flatten Image
6.27. Align Visible Layers…
6.28. Guides
6.29. New Guide
6.30. New Guide (by Percent)
6.31. New Guides from Selection
6.32. Remove all guides
6.33. Configure Grid…
6.34. Image Properties
6.35. Metadata
6.36. Metadata Viewer
6.37. Metadata Editor

### The "Layer" Menu
7.1. Introduction to the “Layer” Menu
7.2. New Layer
7.3. New Layer Group
7.4. New From Visible
7.5. Duplicate layer
7.6. Anchor layer
7.7. Merge Down
7.8. Delete Layer
7.9. The Text Commands of the Layer Menu
7.10. Discard Text Information
7.11. “Stack” Submenu
7.12. Select Previous Layer
7.13. Select Next Layer
7.14. Select Top Layer
7.15. Select Bottom Layer
7.16. Raise Layer
7.17. Lower Layer
7.18. Layer to Top
7.19. Layer to Bottom
7.20. The “Reverse Layer Order” command
7.21. The “Mask” Submenu
7.22. Add Layer Mask
7.23. Apply Layer Mask
7.24. Delete Layer Mask
7.25. Show Layer Mask
7.26. Edit Layer Mask
7.27. Disable Layer Mask
7.28. Mask to Selection
7.29. Add Layer Mask to Selection
7.30. Subtract Layer Mask from Selection
7.31. Intersect Layer Mask with Selection
7.32. The “Transparency” Submenu of the “Layer” menu
7.33. Add Alpha Channel
7.34. Remove Alpha Channel
7.35. Color to Alpha
7.36. Semi-flatten
7.37. Threshold Alpha
7.38. Alpha to Selection
7.39. Add Alpha channel to Selection
7.40. Subtract from Selection
7.41. Intersect Alpha channel with Selection
7.42. The “Transform” Submenu
7.43. Flip Horizontally
7.44. Flip Vertically
7.45. Rotate 90° clockwise
7.46. Rotate 90° counter-clockwise
7.47. Rotate 180°
7.48. Arbitrary Rotation
7.49. Offset
7.50. Layer Boundary Size
7.51. Layer to Image Size
7.52. Scale Layer
7.53. Crop Layer

### The "Colors" Menu
8.1. Introduction to the “Colors” Menu
8.2. Color Balance
8.3. Color Temperature
8.4. Hue Chroma
8.5. Hue-Saturation
8.6. Saturation
8.7. Exposure
8.8. Shadows-Highlights
8.9. Brightness-Contrast
8.10. Levels
8.11. Curves
8.12. Invert
8.13. Linear Invert
8.14. Value Invert
8.15. The “Auto” Submenu
8.16. Equalize
8.17. White Balance
8.18. Stretch Contrast
8.19. Stretch HSV
8.20. Color Enhance
8.21. Color Enhance (legacy)
8.22. The “Components” Submenu
8.23. Channel Mixer
8.24. Compose
8.25. Extract Component
8.26. Mono Mixer
8.27. Decompose
8.28. Recompose
8.29. Color to Gray
8.30. Desaturate
8.31. Sepia
8.32. The “Map” Submenu
8.33. Rearrange Colormap
8.34. Set Colormap
8.35. Alien Map
8.36. Color Exchange
8.37. Rotate Colors
8.38. Gradient Map
8.39. Palette Map
8.40. Sample Colorize
8.41. Fattal et al. 2002
8.42. Mantiuk 2006
8.43. Reinhard 2005
8.44. Stress
8.45. Retinex
8.46. The “Info” Submenu
8.47. Histogram
8.48. Border Average
8.49. Colorcube Analysis
8.50. Export Histogram
8.51. Smooth Palette
8.52. Threshold
8.53. Colorize
8.54. Posterize
8.55. Color to Alpha…
8.56. Dither
8.57. RGB Clip
8.58. Hot…

### The "Tools" Menu
9.1. Introduction to the “Tools” Menu

### The "Filters" Menu
10.1. Introduction to the “Filters” Menu
10.2. Repeat Last
10.3. Re-show Last
10.4. Reset All Filters
10.5. The “Python-Fu” Submenu
10.6. The “Script-Fu” Submenu

### "Windows" Menu

### The “Help” Menu
12.1. Introduction to the “Help” Menu
12.2. Help
12.3. Context Help
12.4. Tip of the Day
12.5. About
12.6. Plug-In Browser
12.7. The Procedure Browser
12.8. GIMP online

###1. The Toolbox
1.1. Introduction
1.2. Tool Icons
1.3. Color and Indicator Area
1.4. Tool Options

### Selection Tools
2.1. Common Features
2.2. Rectangle Selection
2.3. Ellipse Selection
2.4. Free Selection (Lasso)
2.5. Fuzzy selection (Magic wand)
2.6. Select By Color
2.7. Intelligent Scissors
2.8. Foreground Select

### Paint Tools
3.1. Common Features
3.2. Dynamics
3.3. Brush Tools (Pencil, Paintbrush, Airbrush)
3.4. Bucket Fill
3.5. Gradient
3.6. Pencil
3.7. Paintbrush
3.8. MyPaint Brush
3.9. Eraser
3.10. Airbrush
3.11. Ink
3.12. Clone
3.13. Heal
3.14. Perspective Clone
3.15. Blur/Sharpen
3.16. Smudge
3.17. Dodge/Burn

### Transform Tools
4.1. Common Features
4.2. Align
4.3. Move
4.4. Crop
4.5. Rotate
4.6. Scale
4.7. Shear
4.8. Perspective
4.9. 3D Transform
4.10. Unified Transform
4.11. Handle Transform
4.12. Flip
4.13. The Cage Tool
4.14. Warp Transform

### Other
5.1. Overview
5.2. Paths
5.3. Color Picker
5.4. Zoom
5.5. Measure
5.6. Text
5.7. GEGL Operation

### Dialogs
1. Dialog Introduction

### Image Structure Related Dialogs
2.1. Layers Dialog
2.2. Channels Dialog
2.3. Paths Dialog
2.4. Colormap Dialog
2.5. Histogram dialog
2.6. Navigation Dialog
2.7. Undo History Dialog

### Image-content Related Dialogs
3.1. FG/BG Color Dialog
3.2. Brushes Dialog
3.3. Patterns Dialog
3.4. Gradients Dialog
3.5. Palettes Dialog
3.6. Tagging
3.7. Fonts Dialog

### Image Management Related Dialogs
4.1. Buffers Dialog
4.2. Images Dialog
4.3. Document History Dialog
4.4. Templates Dialog

### Misc. Dialogs
5.1. Tool Presets Dialog
5.2. Tool Preset Editor
5.3. Device Status Dialog
5.4. Error Console
5.5. Dashboard
5.6. Save File
5.7. Export File
5.8. Sample Points Dialog
5.9. Pointer Dialog
5.10. Symmetry Painting dialog

### Filters

1. Introduction
2. Common Features
### Blur Filters
3.1. Introduction
3.2. Gaussian Blur
3.3. Mean Curvature Blur
3.4. Median Blur
3.5. Pixelize
3.6. Selective Gaussian Blur
3.7. Circular Motion Blur
3.8. Linear Motion Blur
3.9. Zoom Motion Blur
3.10. Tileable Blur

### Enhance Filters
4.1. Introduction
4.2. Antialias
4.3. Deinterlace
4.4. High Pass
4.5. Noise Reduction
4.6. Red Eye Removal
4.7. Symmetric Nearest Neighbor
4.8. Sharpen (Unsharp Mask)
4.9. Despeckle
4.10. Destripe
4.11. NL Filter
4.12. Wavelet Decompose

### 5. Distort Filters
5.1. Introduction
5.2. Apply Lens
5.3. Emboss
5.4. Engrave
5.5. Lens Distortion
5.6. Mosaic
5.7. Polar Coordinates
5.8. Ripple
5.9. Shift
5.10. Spherize
5.11. Value Propagate
5.12. Video
5.13. Waves
5.14. Whirl and Pinch
5.15. Wind
5.16. Curve Bend
5.17. Emboss (legacy)
5.18. Newsprint
5.19. Page Curl

### Light and Shadow Filters
6.1. Introduction
6.2. Supernova
6.3. Lens Flare
6.4. Gradient Flare
6.5. Lighting Effects
6.6. Sparkle
6.7. Drop Shadow
6.8. Long Shadow
6.9. Vignette
6.10. Drop Shadow (legacy)
6.11. Perspective
6.12. Xach-Effect

### Noise Filters
7.1. Introduction
7.2. CIE lch Noise
7.3. HSV Noise
7.4. Hurl
7.5. Pick
7.6. RGB Noise
7.7. Slur
7.8. Spread

### Edge-Detect Filters
8.1. Introduction
8.2. Difference of Gaussians
8.3. Edge
8.4. Laplace
8.5. Neon
8.6. Sobel
8.7. Image Gradient

### Generic Filters
9.1. Introduction
9.2. Convolution Matrix
9.3. Distance Map
9.4. GEGL graph
9.5. Normal Map
9.6. Dilate
9.7. Erode

### Combine Filters
10.1. Introduction
10.2. Depth Merge
10.3. Filmstrip

### Artistic Filters
11.1. Introduction
11.2. Apply Canvas
11.3. Cartoon
11.4. Cubism
11.5. Glass Tile
11.6. Oilify
11.7. Photocopy
11.8. Simple Linear Iterative Clustering (SLIC)
11.9. Softglow
11.10. Waterpixels
11.11. Cartoon (legacy)
11.12. Clothify
11.13. GIMPressionist
11.14. Oilify (legacy)
11.15. Predator
11.16. Softglow (legacy)
11.17. Van Gogh (LIC)
11.18. Weave

### Decor Filters
12.1. Introduction
12.2. Add Bevel
12.3. Add Border
12.4. Coffee Stain
12.5. Fog
12.6. Fuzzy Border
12.7. Old Photo
12.8. Round Corners
12.9. Slide
12.10. Stencil Carve
12.11. Stencil Chrome

### Map Filters
13.1. Introduction
13.2. Bump Map
13.3. Displace
13.4. Fractal Trace
13.5. Illusion
13.6. Little Planet
13.7. Panorama Projection
13.8. Recursive Transform
13.9. Paper Tile
13.10. Tile Seamless
13.11. Fractal Trace (legacy)
13.12. Map Object
13.13. Tile

### Rendering Filters
14.1. Introduction
14.2. Flame
14.3. Fractal Explorer
14.4. IFS Fractal
14.5. Cell Noise
14.6. Perlin Noise
14.7. Plasma
14.8. Simplex Noise
14.9. Solid Noise
14.10. Difference Clouds
14.11. Bayer Matrix
14.12. Checkerboard
14.13. Diffraction Patterns
14.14. Grid
14.15. Maze
14.16. Sinus
14.17. Checkerboard (legacy)
14.18. CML Explorer
14.19. Grid (legacy)
14.20. Jigsaw
14.21. Qbist
14.22. Circuit
14.23. Gfig
14.24. Lava
14.25. Line Nova
14.26. Sphere Designer
14.27. Spyrogimp

### Web Filters
15.1. Introduction
15.2. ImageMap
15.3. Semi-Flatten
15.4. Slice

### Animation Filters
16.1. Introduction
16.2. Blend
16.3. Burn-In
16.4. Rippling
16.5. Spinning Globe
16.6. Waves
16.7. Optimize
16.8. Playback


- udemy
```text
- process RAW files for GIMP
- what a Layer Mask is and how to use them in GIMP
- tips and techniques for editing images and creating graphics from a pro photographer (and graphic designer)
- what a Layer is and how to use them in GIMP
- configure the GIMP interface for increased productivity
- the different file types you can use in GIMP as well as GIMP's proprietary file format
- setup and use the History panel for streamlining your workflow
- save 1 hour for every 8 hours worked
- save files for the web, print, Photoshop and more
- what Blending Modes are and how to use them in GIMP
- about the 7 different Blending Mode groups and how to use them in GIMP
- all about the 4 main tools in GIMP; Selections, Paint, Transform and Color Tools
- use brushes in GIMP
- install pre-made brushes, from other artists, in GIMP
- create your own brushes in GIMP
- use Palettes & Presets to streamline your workflow
- the core differences between RAW and JPG files.
- how to create Color Tool Presets to streamline your workflow
- Color Balance Tool for editing your images
- Colorize Tool for editing your images
- Hue Saturation Tool for editing your images
- Brightness Contrast Tool for editing your images
- Levels Tool for editing your images
- Utilize the Histogram to streamline your editing workflow
- Curves Tool for editing your images
- Threshold Tool for editing your images
- Posterize & Desaturate Tool for editing your images
- the difference between the Heal & Clone Tool
- Heal Tool for editing your images
- Clone Tool for editing your images
- Dodge & Burn Tool for editing your images
- the importance of the Selection Tools and how to use
- Free Select Tool
- Foreground Select Tool
- Fuzzy Select Tool
- Select by Color Tool
- Quick Mask Tool
- Path Tool
- Text Tool
- Patterns Tool
- Filter Tools
- create basic shapes in GIMP
- add photos to shapes
- personal editing workflow for processing images
- add text along a path
- add an image to text
- create text out of grass
- add a reflection to your text
- make a superhero "pop-out" of an iMac screen
- add light to an image
- add smoke to an image
- All 38 layer modes (blend modes) for creating image effects.
- Important design and image editing concepts like Layers and Selection Areas
- Make image adjustments using powerful built-in tools and filters.
- Touch up photos to remove imperfections or background objects.
- Enhance photos for a better finished product.
- Master the Paths Tool
- Add a vignette around photos for a more dramatic effect.
- The Dodge and Burn Tools for a "Vogue" look.
- Crop or resize images with minimal quality loss, including cropping images into a circle shape.
- Export your photos to file types supported by GIMP.
- Create image compositions that combine graphic design, images, and filters.
- Edit black and white photos using recommended tools and methods.
- Edit low-light photos
- Remove objects from photos and perform other photo manipulations
- Repair old and damaged photographs
```
