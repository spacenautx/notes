# algebra

### Linear Algebra
- https://github.com/kenjihiranabe/The-Art-of-Linear-Algebra
- https://pabloinsente.github.io/intro-linear-algebra

### Vectors

- definition
- scalars
- addition
- scalar multiplication
- inner product(dot product)
- vector projection
- cosine similarity
- orthogonal vectors
- normal and orthonormal vectors
- vector norm
- vector space
- linear combination
- linear span
- linear independence
- basis vectors
