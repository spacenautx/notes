# math

- https://www.theoremoftheday.org
- https://maths-with-python.readthedocs.io/en/latest/
- https://github.com/drvinceknight/Python-Mathematics-Handbook
- https://github.com/jrjohansson/scientific-python-lectures
- https://github.com/rossant/awesome-math/blob/master/README.md
- https://doingmathwithpython.github.io/pages/about.html
- https://thepalindrome.substack.com/p/how-to-measure-the-angle-between
--------
- calculus
- probability
- algebra
- regression
- matrix
- trigonometry
- geometry
-------
- [prime](https://www.quantamagazine.org/the-map-of-mathematics-20200213/)
- euler
-------
- 17equations
- https://x.com/DataScienceDojo/status/1704554805618950416?s=20
- https://twitter.com/ExploreCosmos_/status/1740057444745883945K
-------
- fourier
- https://stevejtrettel.site/code/2021/fourier-series/
- https://blog.revolutionanalytics.com/2014/01/the-fourier-transform-explained-in-one-sentence.html
- https://www.youtube.com/watch?v=spUNpyF58BY
- https://www.reddit.com/r/Python/comments/dk85ue/easy_fourier_transform_implementing_3blue1browns/

-------
- rocketscience
- flight
- mechanics
-------
- 91divoc
- https://www.quantamagazine.org/quantum-mischief-rewrites-the-laws-of-cause-and-effect-20210311/
- https://www.reddit.com/r/ReverseEngineering/comments/smf4u/reverser_wanting_to_develop_mathematically/
----
### Pi 
- https://mathworld.wolfram.com/PiDigits.html
### self learned

| Books                                                                      | Authors                             |
|----------------------------------------------------------------------------|-------------------------------------|
| Number Theory                                                              | Andrej Dujella                      |
| Naive Set Theory                                                           | Paul R. Halmos                      |
| Book of Proof                                                              | Richard Hammack                     |
| Introduction to Topology                                                   | Bert Mendelson                      |
| Concepts of Modern Mathematics                                             | Ian Stewart                         |
| An Introduction to Mathematical                                            | Reasoning Peter J. Eccles           |
| What Is Mathematics?                                                       | Richard Courant                     |
| The Cauchy-Schwarz Master Class                                            | J. Michael Steele                   |
| Principles of Mathematical Analysis                                        | Walter Rudin                        |
| Ordinary Differential Equations                                            | Morris Tenenbaum                    |
| Number Theory George                                                       | E. Andrews                          |
| No Bullshit Guide to Math and Physics                                      | Ivan Savov                          |
| No Bullshit Guide to Linear Algebra                                        | Ivan Savov                          |
| Mathematics for the Nonmathematician                                       | Morris Kline                        |
| Introduction to Linear Algebra                                             | Gilbert Strang                      |
| Introduction to Analysis Arthur Mattuck Elementary Number Theory           | Gareth A.  Jones                    |
| Coding the Matrix- Linear Algebra through Applications to Computer Science | Philip N. Klein                     |
| Calculus                                                                   | Michael Spivak                      |
| Calculus                                                                   | Tom M. Apostol                      |
| Abstract Algebra                                                           | David S. Dummit                     |
| A Book of Abstract Algebra                                                 | Charles C Pinter                    |
| Prime Numbers and the Riemann Hypothesis                                   | Barry Mazur                         |
| Mathematics and Its History                                                | John Stillwell                      |
| Topology                                                                   | James Munkres                       |
| The Art of Problem Solving Vol 2                                           | Richard Rusczyk                     |
| Introduction to Probability, Statistics, and Random Processes              | Hossein Pishro-Nik                  |
| The Art of Problem Solving, Vol. 1- The Basics                             | Sandor Lehoczky and Richard Rusczyk |
| Linear Algebra Done Right                                                  | Sheldon Axler                       |

### visualization
- https://kyndinfo.notion.site/Interpolation-and-Animation-44d00edd89bc41d686260d6bfd6a01d9

### extra
- https://web.archive.org/web/20190114142918/https://mathscitech.org/articles/zero-to-zero-power
- https://twitter.com/PhysInHistory/status/1740403565846573436
