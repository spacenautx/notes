# blender

```text
Preview 01:52
How to Watch & Download Videos in 1080p 01:23
Coming Soon to the Encyclopedia 00:33
Navigating the Course (Important!) 02:19
User Interface 04:09
Customizing the Layout 04:45
Workspaces 02:44
General Editors 06:08
Animation Editors 06:10
Scripting Editors 02:19
Data Editors 02:10
User Preferences and Suggested Changes 11:04
Saving and Loading 04:43
File Backups and Recovery 04:52
Importing and Exporting 01:46
Navigating Through 3D Space 04:51
The Axis and Grid 03:38
Units of Measurement 04:54
Viewport Shading 23:00
Viewport Options 10:40
Different Work Modes Overview 11:05
How much do you know about Blender so far? 4 questions
Introduction to Working with Blender 00:56
Selection Tools and Active Selection 04:15
Basic Tools 09:21
Object Origins 03:30
Global Axis Vs. Local Axis 03:00
The 3D Cursor 03:30
Transformation Orientations 05:50
Pivot Point Options 03:54
Duplication and Linked Duplicates 05:43
Object Parenting 06:43
Snapping Tools 19:49
Origin and Parent Transformations 03:39
Smooth , Flat, and Auto Smooth Shading 02:41
Collections and View Layers 13:26
Scenes 06:39
Appending and Linking 05:28
Setting Up Background Images 10:03
More Selection Tools 05:02
Measuring Tool 02:35
Annotations Tool 08:03
Quick Favorites Menu 01:22
Project - Your First 3D Scene - Making a Toy Train 38:30
Are you ready to work with Blender? 6 questions
Introduction to Mesh Modeling 01:18
Mesh Anatomy and Common Terminology 04:22
Everything's a Triangle 02:09
Project - Your First 3D Model - Low Poly Axe 21:12
What are Normals? 11:49
Selection Tools and Selection Modes 08:39
Transform Tools and Basic Tools 09:37
Deleting, Dissolving, and Collapsing 06:25
Separating and Joining 03:55
Extruding 05:48
Creating Faces and Edges 05:41
Face Tools - Part 1 15:17
Face Tools - Part 2 09:48
Edge Tools - Part 1 16:30
Edge Tools - Part 2 08:25
Splitting and Ripping 04:56
Knife, Knife Project, and Bisect 09:25
Vertex Tools 14:34
Mirroring Tools 09:42
Convex Hull, Spin, and Spin Duplicate 07:52
More Selection Tools 11:10
Subsurf Modeling - Part 1 - Introduction 04:20
Subsurf Modeling - Part 2 - Rules and Tools 11:14
Project - Subsurf Modeling a Coffee Mug 15:25
Vertex Groups 06:09
Mesh Analysis and Measurements 02:44
Cleaning Up Your Meshes 09:28
Editing Normals 12:49
Do you have what it takes to be Blender's next top modeler? 8 questions
Curve Editing Tools 11:06
Curve Properties 16:26
Taper, Bevel, Follow Path and Active Spline Panels 11:10
Not too many curveballs here, promise! 5 questions
Introduction to Surface Modeling 04:31
Surface Editing Tools 06:44
Surfaces Properties 08:23
Surface to say this Quiz will make you think! 5 questions
Introduction to Drivers 01:23
Project - The Basic Drivers Workflow 12:15
The Drivers Editor 11:38
Editing F-Curves 12:30
Scripted Expressions 07:09
Combining Multiple Input Variables 04:39
Input Variables - Transform Channel 07:27
Input Variables - Rotational Difference 03:03
Input Variables - Distance 01:31
Input Variables - Single Property 10:33
Project - Light Switch 10:22
Project - Boxing Ring Dynamic Camera 07:12
Project - RGB Sliders 03:53
Project - Muscle Flexing 12:05
Introduction to Constraints 01:28
Working with Constraints 13:41
Preparing Constraints Demo Scene 03:46
Transformation Constraints - Copy Location 05:01
Transformation Constraints - Copy Rotation 01:19
Transformation Constraints - Copy Scale 02:18
Transformation Constraints - Copy Transform 01:35
Transformation Constraints - Limit Distance 04:38
Transformation Constraints - Limit Location 06:46
Transformation Constraints - Limit Rotation 01:04
Transformation Constraints - Limit Scale 01:28
Transformation Constraints - Maintain Volume 02:06
Transformation Constraints - Transformation 05:38
Tracking Constraints - Clamp To 05:08
Tracking Constraints - Damped Track 03:05
Tracking Constraints - Locked Track 02:36
Tracking Constraints - Stretch To 04:32
Tracking Constraints - Track To 04:58
Relationship Constraints - Action 08:13
Relationship Constraints - Child Of 03:33
Relationship Constraints - Floor 03:20
Relationship Constraints - Follow Path 05:20
Relationship Constraints - Pivot 03:36
Relationship Constraints - Shrink Wrap 10:23
You don't need to constrain yourself with this Quiz! 8 questions
Modifier Basics 11:59
Modify - Data Transfer 10:19
Modify - Mesh Cache and Mesh Sequence Cache 06:45
Modify - Normal Edit 04:56
Modify - Weighted Normals 05:10
Modify - UV Project 05:44
Modify - UV Warp 04:03
Modify - Vertex Weight Edit 06:58
Modify - Vertex Weight Mix 07:02
Modify - Vertex Weight Proximity 06:08
Generate - Array 09:59
Generate - Bevel 17:29
Generate - Boolean 05:51
Generate - Build 04:31
Generate - Decimate 06:28
Generate - Edge Split 03:31
Generate - Mask 02:42
Generate - Mirror 10:17
Generate - Multiresolution 08:39
Generate - Remesh 05:44
Generate - Screw 06:58
Generate - Skin 07:03
Generate - Solidify 09:42
Generate - Subdivision Surface 05:25
Generate - Adaptive Subdivision Surface 06:49
Generate - Triangulate 03:41
Generate - Weld 01:45
Preview 05:03
Deform - Cast 04:37
Deform - Curve 06:14
Deform - Displace 05:16
Deform - Hook 09:11
Deform - Laplacian Deform 05:08
Deform - Lattice 05:06
Deform - Mesh Deform 17:46
Deform - Shrinkwrap 13:00
Deform - Simple Deform 06:13
Deform - Smooth 01:07
Deform - Smooth Corrective 03:57
Deform - Smooth Laplacian 04:57
Deform - Surface Deform 02:29
Deform - Warp 04:38
Deform - Wave 05:17
Project - Acid Burn Effect 06:01
Project - Customizable Archi-Viz Assets 08:59
Project - Metal Grate 03:50
Project - Object Instancing Arrays 02:59
Project - Fitting Wipers on Windshields 06:01
You've Modified your knowledge, now you can test it! 9 questions
Introduction to Blender's Armatures 04:47
Armature Object Properties - Part 1 - Skeleton 04:51
Armature Object Properties - Part 2 - Display and Bone Groups 07:54
Armature Object Properties - Part 3 - Pose Library 04:44
Armature Bone Properties - Part 1 - Transform, B-Bones, and Relations 09:30
Armature Bone Properties - Part 2 - Deform, Envelopes, and Viewport Display 06:43
Knowing how to Rig will get you the gig! Try this Skeleton Armature Quiz. 5 questions
Deforming a Character - Part 1 - Creating a Character Skeleton 16:53
Deforming a Character - Part 2 - Binding the Skeleton to the Character 06:03
Deforming a Character - Part 3 - Tweaking the Deformation 06:19
Deforming a Character - Part 4 - Weight Painting and Completing the Deformation 09:10
This Quiz will really show your Character! 4 questions
Deforming a Character - Part 1 - Creating a Character Skeleton 13:29
Deforming a Character - Part 2 - Parenting to the Armature & Automatic Skinning 05:20
Deforming a Character - Part 3 - Skinning the Character's Head 11:07
Deforming a Character - Part 4 - Individual Vertex Weights and Weight Painting 12:03
Deforming a Character - Part 5 - Completing the Character Skinning 08:11
Deforming a Character - Part 6 - Creating a Mesh Deform Cage 13:43
Deforming a Character - Part 7 - Deforming with the Mesh Deform Modifier 12:22
Deforming a Character - Part 8 - Deforming the Hood and Cape 12:35
Head and Neck Control Rig Method - Part 1 - Hinge Mechanism 06:55
Head and Neck Control Rig Method - Part 2 - Animation Control Bones 14:09
Head and Neck Control Rig Method - Part 3 - Custom Control Shapes 13:32
Shoulder and Arm Control Rig Method - Part 1 - Shoulders and FK Arm Preparation 12:25
Shoulder and Arm Control Rig Method - Part 2 - FK Arm Mechanism 06:35
Shoulder and Arm Control Rig Method - Part 3 - FK Control Shapes 10:21
Shoulder and Arm Control Rig Method - Part 4 - IK Arm Mechanism 11:04
Shoulder and Arm Control Rig Method - Part 5 - IK Control Shapes and Elbow Pole 10:15
Shoulder and Arm Control Rig Method - Part 6 - IK Elbow Pole Viz 04:52
Hand Control Rig Method - Part 1 - Adding Hand and Finger Joints 08:41
Hand Control Rig Method - Part 2 - Joining Fingers to Body and Skinning 11:45
Hand Control Rig Method - Part 3 - Finger Curl Rig 10:00
Hand Control Rig Method - Part 4 - Completing Fingers and Thumb Rig 10:44
Hand Control Rig Method - Part 5 - Palm Rig and Control Shapes 14:55
Leg Control Rig Method - Part 1 - FK Leg Rig 13:33
Leg Control Rig Method - Part 2 - IK Leg Rig 15:54
Spine Control Rig Method - Part 1 - Inverse Hip Rig 04:04
Spine Control Rig Method - Part 2 - Spine Rig 04:07
Spine Control Rig Method - Part 3 - Spine Hinge Mechanism 04:24
Spine Control Rig Method - Part 4 - Custom Bone Shapes 07:25
10/10 People who take this Quiz are ready to Rig professionally! 8 questions
Animation Keyframes 10:59
Advanced Animation Keying Sets 06:48
Animation in The Timeline - Part 1 - Navigation & Tools 08:15
Animation in the Timeline - Part 2 - The Playback Menu 08:09
Animation in The Dope Sheet Editor - Introduction & The Action Editor 03:37
Animation in The Dope Sheet Editor - The Dope Sheet & Editor Window 07:26
The Shape Key Editor 07:41
Animation in The Graph Editor - Part 1 06:42
Animation in The Graph Editor - Part 2 17:34
The Non-Linear Animation (NLA) Editor 17:41
FK & IK (Forward & Inverse Kinematics) 15:34
The Curious Case of Animating Rotations 18:16
Rotation Calculations: Euler Gimbal Method 07:42
This Quiz will get you quite Animated! 11 questions
Blender's Material Properties 09:55
Project - Your Very First Shaders 08:20
Node Wrangler (Enable This Add-on!) 15:00
Input Nodes - Ambient Occlusion 04:09
Input Nodes - Attribute 01:50
Input Nodes - Bevel 02:11
Input Nodes - Camera Data 04:07
Input Nodes - Fresnel 04:20
Input Nodes - Geometry 15:56
Input Nodes - Hair Info 11:14
Input Nodes - Layer Weight 03:01
Input Nodes - Light Path 22:21
Input Nodes - Object Info 12:17
Input Nodes - Particle Info 11:34
Input Nodes - RGB 00:45
Input Nodes - Tangent 01:52
Input Nodes - UV Map 01:17
Input Nodes - Value 00:58
Input Nodes - Vertex Colors 01:35
Input Nodes - Volume Info 03:39
Input Nodes - Wireframe 01:24
Output Nodes - Material, World, and Light Output Nodes 03:53
Shader Nodes - Add Shader 02:50
Shader Nodes - Anisotropic 16:42
Shader Nodes - Diffuse 04:08
Shader Nodes - Emission 14:25
Shader Nodes - Glass 15:36
Shader Nodes - Glossy 02:36
Shader Nodes - Hair 07:32
Shader Nodes - Holdout 01:04
Shader Nodes - Mix Shader 20:51
Preview 28:35
Shader Nodes - Principled Hair 07:35
Shader Nodes - Volumetric Shaders - Part 1 - Cycles 17:22
Shader Nodes - Volumetric Shaders - Part 2 - Eevee 20:19
Shader Nodes - Refraction 03:46
Shader Nodes - Subsurface Scattering 32:17
Shader Nodes - Toon 11:46
Shader Nodes - Translucent 26:03
Shader Nodes - Transparent 05:55
Shader Nodes - Velvet 02:06
Shader Nodes - Volume Absorption and Scatter 10:09
Texture Nodes - Brick 04:19
Texture Nodes - Checker 00:53
Texture Nodes - Environment 03:15
Texture Nodes - Gradient 02:24
Texture Nodes - IES 01:56
Texture Nodes - Image 11:15
Texture Nodes - Magic 01:52
Texture Nodes - Musgrave 08:52
Texture Nodes - Noise 03:06
Texture Nodes - Point Density 05:23
Texture Nodes - Sky 02:42
Texture Nodes - Voronoi 19:41
Texture Nodes - Wave 01:51
Texture Nodes - White Noise 03:35
Color Nodes - Bright Contrast 10:40
Color Nodes - Gamma 05:25
Color Nodes - Hue Saturation 05:48
Color Nodes - Invert 01:27
Color Nodes - Light Falloff 10:06
Color Nodes - MixRGB 11:22
Color Nodes - RGB Curves 09:42
Vector Nodes - Bump 02:44
Vector Nodes - Displacement 06:51
Vector Nodes - Mapping 06:25
Vector Nodes - Normal 03:40
Vector Nodes - Normal Map 03:27
Vector Nodes - Vector Curves 02:41
Vector Nodes - Vector Displacement 05:05
Vector Nodes - Vector Transform 04:19
Converter Nodes - Blackbody 04:30
Converter Nodes - Clamp 04:32
Converter Nodes - ColorRamp 08:47
Converter Nodes - Combine/Separate HSV 03:34
Converter Nodes - Combine/Separate RGB 08:01
Converter Nodes - Combine/Separate XYZ 07:42
Converter Nodes - RGB to BW (Black and White) 10:30
Converter Nodes - Map Range 03:19
Converter Nodes - Math 05:18
Converter Nodes - Shader to RGB - Eevee Exclusive Shader Node 05:13
Converter Nodes - Vector Math 22:12
Converter Nodes - Wavelength 11:57
Node Groups 08:45
Script Node 03:28
Layout Nodes 06:10
Project - Micropolygon Displacement 07:18
Project - Object Proximity Shading 03:31
Preview 26:03
Project - Procedural Carbon Fiber - Part 2 - Anisotrophy in Cycles 06:22
No need to throw Shade, take this Quiz! 10 questions
Introduction to UV Mapping 02:48
UV Editor and UV Mapping Basics 05:51
Project - Full UV Unwrapping Workflow 11:42
UV Mapping - Seams and Unwrapping 14:29
UV Mapping - Smart UV Project 05:29
UV Mapping - Lightmap Pack 05:04
UV Mapping - Follow Active Quads 04:02
UV Mapping - Projections and Reset 08:48
UV Pinning 03:21
Live Unwrap 03:04
Exporting a UV Layout 02:12
Mesh Editing and UV Correction 04:30
Selection - UV Sync Selection 02:25
Selection - Sticky Selection 01:56
Welding, Aligning, and Straightening 03:43
UV Stitching 01:12
UV Stretching 04:07
Sculpting UVs 02:55
Island Options 03:31
Multiple Objects in One UV Layout 02:20
Multiple UV Maps 05:11
Tips and Tricks 03:27
Project - UDIMs in Blender 18:20
Project - Unwrapping an Axe 04:59
Let's see if you can wrap your head around unwrapping! 4 questions
Camera Properties - Lens Properties 06:20
Camera Navigation - Active Camera View 02:27
Camera Switching 03:16
Camera Navigation - Camera Transforms 04:38
Camera Properties - Depth Of Field 05:32
Camera Properties - Safe Areas, Background Image, and Viewport Display 04:37
Give this Camera Quiz a Shot! 6 questions
Introduction to Cycles 01:38
Rendering Basics 04:28
Render Engine Settings 02:46
Lighting in Cycles - Point Light 03:53
Lighting in Cycles - Sun Light 02:09
Lighting in Cycles - Spot Light 04:20
Lighting in Cycles - Area Lights and Portals 05:44
Lighting in Cycles - Emission Shaders 04:07
Lighting in Cycles - HDRIs 03:55
World Settings and Ambient Occlusion 04:14
Sampling - Path Tracing 03:42
Sampling - Branched Path Tracing 05:53
Sampling - Advanced 02:17
Light Paths - Max Bounces 06:37
Light Paths - Clamping 04:06
Light Paths - Caustics 02:26
Render Properties - Volumes 02:58
Render Properties - Hair 04:45
Render Properties - Simplify 07:10
Render Properties - Motion Blur 09:37
Render Properties - Film 08:53
Render Properties - Performance 07:35
Render Properties - Color Management 03:18
Object Settings - Shadow Catcher 01:30
Object Settings - Visibility Options 03:22
This Quiz will Render you fit to be a 3D artist! 6 questions
Introduction to the Eevee Render Engine 01:48
Switching Between Eevee and Cycles 03:40
Rendering Basics 04:28
Transparency in Eevee 14:02
Lighting in Eevee - Point Light 11:53
Lighting in Eevee - Sun Light 07:25
Lighting in Eevee - Spot Light 03:38
Lighting in Eevee - Area Light 06:26
World Shader in Eevee 12:30
Light Probes in Eevee - Reflection Plane 11:19
Light Probes in Eevee - Reflection Cubemap 07:21
Light Probes in Eevee - Irradiance Volume 13:22
Object Volumetrics in Eevee 17:26
Screen Space Effects - Color Management 14:00
Screen Space Effects - Sampling 02:22
Screen Space Effects - Ambient Occlusion (GTAO) 01:06
Screen Space Effects - Bloom 04:50
Screen Space Effects - Depth Of Field 01:51
Screen Space Effects - Sub-Surface Scattering 04:10
Screen Space Effects - Screen Space Reflection 07:18
Screen Space Effects - Volumetrics 09:18
Screen Space Effects - Shadows 05:03
Screen Space Effects - Indirect Lighting 04:52
Screen Space Effects - Film 04:12
Screen Space Effects - Simplify 05:24
Screen Space Effects - Hair 04:47
The Eevee section contained all the answers to this Quiz... Gotta Catch 'Em All! 7 questions
Introduction 01:28
Modeling - Platform and Walls 13:05
Camera Setup 02:34
Modeling - TV 08:49
Modeling - Entertainment Center 12:39
Modeling - Couch 13:59
Modeling - Area Rug 04:48
Modeling - Side Tables 08:09
Modeling - Moulding 05:37
Modeling - Window Cutout 05:19
Modeling - Windows 10:36
Modeling - Curtain Rod 05:11
Modeling - Curtains 13:06
Modeling - Power Outlets 06:31
Modeling - Wall Shelf 03:59
Modeling - Books 10:15
Modeling - Power Cables 08:45
Modeling - Potted Plants 21:00
Modeling - Table Lamp 05:25
Modeling - String Lights 20:51
Setting Up Cycles 01:09
Lighting - Point Lights 05:21
Shaders - Basic Shaders - Part 1 07:56
Shaders - Basic Shaders - Part 2 07:28
Shaders - Hardwood Floors 06:03
Shaders - Drywall 04:38
Shaders - Wood 04:42
Shaders - Brick Wall 09:34
Shaders - Area Rug 04:07
Shaders - Plants 08:00
Shaders - Books 04:28
Shaders - Glass 02:54
Lighting - Moon Light 03:03
Shaders - Curtains and Lamp Shade 03:26
Lighting - String Lights 01:07
Final Touches 04:36
Final Render and Compositing 07:25
```
