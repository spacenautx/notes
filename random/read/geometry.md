#+title: geometry
#+TAGS: math
#+SETUPFILE: setup/theme-readtheorg-local.setup
#+OPTIONS: ^:nil

* Axioms
* Points
* Lines
* Planes
* Angles
* Curves
* Surfaces
* Manifolds
* Length, area, and volume
* Metrics and measures
* Congruence and similarity
* Compass and straightedge constructions
* Dimension
* Symmetry
