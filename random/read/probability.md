#+title: probability
#+TAGS: math
#+SETUPFILE: setup/theme-readtheorg-local.setup
#+OPTIONS: ^:nil

http://colah.github.io/posts/2015-09-Visual-Information/
- Basic rules and axioms
- events
- sample space
- frequentist approach
- dependent and independent events
- conditional probability
- Random variables- continuous and discrete
- expectation
- variance,
- distributions- joint and conditional
- Bayes’ Theorem
- MAP
- MLE
- Popular distributions- binomial
- bernoulli
- poisson
- exponential
- gaussian
- Conjugate priors
