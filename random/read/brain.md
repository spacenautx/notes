#+title: brain

* interaction
* [[file:memory_org.org][memory]]
* evolution
* learn
* [[file:focus.org][focus]]
* [[file:critical_thinking.org][Critical Thinking]]
* [[file:zen.org][zen]]
* fruit fly brain
- https://github.com/neurokernel/neurokernel
* [[file:periodic_table.org][periodic_table]]
* biopython
* resources
- https://github.com/asoplata/open-computational-neuroscience-resources
- https://github.com/analyticalmonk/awesome-neuroscience
- https://github.com/brainglobe/brainrender
- https://github.com/brainflow-dev/brainflow
- https://github.com/EtienneCmb/visbrain
