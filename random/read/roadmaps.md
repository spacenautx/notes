# roadmaps


- [bug bounty](https://github.com/1ndianl33t/Bug-Bounty-Roadmaps)
- [hacker](https://github.com/sundowndev/hacker-roadmap)

- [AI](https://github.com/AMAI-GmbH/AI-Expert-Roadmap)
- [data engineer](https://github.com/datastacktv/data-engineer-roadmap )
- [data scientist](https://github.com/MrMimic/data-scientist-roadmap)
- [nlp](https://github.com/graykode/nlp-roadmap)

- [android](https://github.com/MindorksOpenSource/android-developer-roadmap)
- [developer](https://github.com/kamranahmedse/developer-roadmap)
- [ui-ux](https://github.com/togiberlin/ui-ux-designer-roadmap)
- [game developer](https://github.com/utilForever/game-developer-roadmap)
