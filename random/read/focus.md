# focus


### prefrontal cortex
ventral medial prefrontal cortex
dorsolateral prefrontal cortex
the ability to make healthier long- term decisions and engage in
self-discipline comes more easily to some people than others as a
result of the activity and structure of their prefrontal cortex

The phrase “exercise your self-control” is an accurate way of thinking
about your ability to be disciplined in the face of temptation, as that
ability can be built up if you consistently exercise it by making healthier
choices, and it can also be stripped down by constantly giving in to
unhealthy pleasures.

### ventral striatum
the ventral striatum—a brain region linked to
addictions—in their mid-life when they were trying to exercise self-
control while enticed to make unhealthy choices. They were also
uniformly more successful by almost all conventional means. These
biological differences may have started out small, but were significant in
later years.

### focus
Focus is one of the main pillars of self-discipline; a person who lacks the
ability to focus is almost certainly one who will also lack discipline.
Focus itself is dependent on something that neuroscientists call executive
functions.
- working memory
- impulse control
 
### cognitive flexibility and adaptability
You can see why they are aptly named the executive functions.
Your brain has to be able to set and pursue goals, prioritize activities,
filter distractions, and control unhelpful inhibitions.

Together, these functions have been shown to take place in several brain
regions including
##### the dorsolateral prefrontal cortex,
##### the anterior cingulate cortex,
##### and the orbitofrontal cortex,
##### supplementary motor area and
##### the cingulate motor zones.
Just like with self-discipline and willpower, the association with specific brain
structures means you can target and improve them specifically.

It’s clearly desirable to have more blood flow to those regions of the
### amygdala
fight or flight
alarm system


On top of that, the scans showed that the
gray matter in the
prefrontal cortex had become noticeably denser. The gray matter growth wasn’t
isolated to just the prefrontal cortex. The brain structure located behind
the frontal lobe—
the anterior cingulate cortex—
also became denser with meditation practice.
This brain area has been associated with functions having to do with self-regulation,
such as monitoring attention conflicts and allowing for greater cognitive flexibility.
In other words, meditation can both reduce the feelings and emotions that make us
lose self-control and increase our ability to manage those feelings by physically
improving the brain structures responsible for them.

* basal ganglia
* reptilian brain
* motivation
* box breathing
* urge surfing
* environment
* dopamine
* social circle
