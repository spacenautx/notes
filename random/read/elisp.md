# elisp


## Introduction

- Emacs Lisp fundamentals
- Everything about functions and variables
- Manipulating Emacs
- Working with the system
- Creating and using extensibility points
- Writing macros
- Publishing to MELPA


If you know Lisp well, don't worry if I skip concepts!

## What is Lisp?

- That weird language with Lots of Irritating Superfluous Parentheses
- A language and environment based on the idea of interactivity
- The syntax enables new language constructs to be defined
- The ultimate hacker language!

## Emacs Lisp

- A dialect of Lisp for configuring and extending Emacs
- Much of Emacs' built-in functionality is written with it!
- Has core features geared toward discoverability and extensibility
- APIs and data types for many types special to Emacs (buffers, windows, etc)


## Lisp Syntax

The beauty of Lisp comes from the simplicity of its syntax!

Lisp syntax is primarily composed of lists, symbols, and values.

```lisp
  (defun the-meaning-of-life (answer)
    (message "The answer is %s" answer))

  ;; Newlines and whitespace can be added anywhere in lists
  (list 1 2 3
        4 5 6
        7 8 9)
```

The code can also be treated as data!
You shouldn't be afraid of editing code with parentheses, there are packages for that :)

Values (or "Objects")
Any value, or object, has a type.  It also has a textual representation that may or may not be "readable"!

## Lisp Types

- Strings
- Numbers (both integer and float)
- Symbols
- Cons cells
- Arrays and Vectors
- ... and more

https://www.gnu.org/software/emacs/manual/html_node/elisp/Programming-Types.html#Programming-Types

## Emacs Types

There are many types specific to Emacs as well, most don't have a code representation:

- Buffers
- Windows
- Frames
- Threads
- Keymaps
- ...

Things we do with these types can affect the Emacs interface:

```lisp

  ;; Get the previous buffer and switch to it
  (switch-to-buffer (other-buffer))

```

https://www.gnu.org/software/emacs/manual/html_node/elisp/Editing-Types.html#Editing-Types

* Forms and Evaluation

A "form" is any lisp object that can be evaluated.

## How evaluation works

Evaluation works differently for:

- Lists
- Symbols
- All other object types

Some are [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Self_002dEvaluating-Forms.html#Self_002dEvaluating-Forms][self-evaluating]], meaning that they return their own value:

```lisp

;; Primitives are usually self-evaluating
42

"Hello!"

[1 2 (+ 1 2)]

;; Not self-evaluating!
buffer-file-name

;; Evaluates a function!
(+ 300 11)

(300 100)

;; Some representations can't be evaluated!
#<buffer Emacs-Lisp-01.org>

```

## The "Environment"

Everything is evaluated in terms of Emacs Lisp's global environment!

Pros: you can change anything in the environment as you go
Cons: your environment might get "dirty" over time in your Emacs session

```lisp

  ;; Set the initial value
  (setq efs/our-nice-variable "Hello System Crafters!")

  ;; Change it to something else (even a different type!)
  (setq efs/our-nice-variable 1337)

```

## Expressions

Lisp is an expression-based language, almost all forms return a value.

```lisp

  ;; A very useful function...
  (defun add-42 (num)
    (+ num 42))

  ;; It returns the result
  (add-42 58)

  ;; Using the result in another call
  (* (add-42 58) 100)

```

* Symbols

A symbol is also a type of object, but it's not self-evaluating!

Symbols can contain alphanumeric characters plus many others:

```lisp

  # Possible symbol characters
  - + = * / _ ~ ! @ $ % ^ & : < > { } ?

```

This gives you the ability to ascribe meanings to symbols based on the characters they contain.  Some examples:

- =bui-keyword->symbol= - Convert from one type to another
- =efs/some-name= - Define a "namespace" for the symbol
- =*pcache-repositories*= - Indicates a global variable (not common in Emacs Lisp)
- =string== - Check if something is equal to something else

When a symbol is evaluated, it returns the variable value associated with that binding:

```lisp

  ;; The example we saw before
  buffer-file-name

```

Function names can't be evaluated like this though:

```lisp

  get-file-buffer

```

We will discuss this point more in a future episode.

https://www.gnu.org/software/emacs/manual/html_node/elisp/Symbol-Type.html#Symbol-Type

* Infix vs Prefix

Lisp expressions use "prefix" notation:

```lisp

(+ 300 (- 12 1))

```

Why is this useful?  Because it puts all functions and operators at the same level of importance, even the ones you define!

* Exercise

Open up the =*scratch*= buffer and experiment with writing simple expressions.  Use =C-x C-e= (=eval-last-sexp=) at the end of each expression to evaluate them.

Here are some you can try:

```lisp

  42

  (* 42 10)

  (concat "Hello " "Emacs!")

  ;; Simple list
  '(1 2 3)

  ;; Another way to create a list
  (list 1 2 3)

  ;; Get the second list item
  (car (cdr '(1 2 3)))

  ;; A vector
  [1 2 3]

```

Also, go take a look at your Emacs configuration and see what things you can recognize about it now!


* =Types, Conditionals, and Loops in Emacs Lisp=
* What will we cover?

We'll get a sense for the basic data types of the language and how we can use them!

- True and false
- Numbers
- Characters
- Sequences
- Strings
- Lists
- Arrays
- Combining expressions with logic operators
- Conditional logic
- Loops

* Follow along with IELM!

You can open up an interactive Emacs Lisp REPL in Emacs by running =M-x ielm.=

I'll be using the following snippet for evaluating code in the REPL:

```lisp

  (defun efs/ielm-send-line-or-region ()
    (interactive)
    (unless (use-region-p)
      (forward-line 0)
      (set-mark-command nil)
      (forward-line 1))
    (backward-char 1)
    (let ((text (buffer-substring-no-properties (region-beginning)
                                                (region-end))))
      (with-current-buffer "*ielm*"
        (insert text)
        (ielm-send-input))

      (deactivate-mark)))

  (defun efs/show-ielm ()
    (interactive)
    (select-window (split-window-vertically -10))
    (ielm)
    (text-scale-set 1))

  (define-key org-mode-map (kbd "C-c C-e") 'efs/ielm-send-line-or-region)
  (define-key org-mode-map (kbd "C-c E") 'efs/show-ielm)

```

* True and false

Normally a language would have a "boolean" type for expressing "true" and "false".

In Emacs Lisp, we have =t= and =nil= which serve the same purpose.

They are symbols!

```lisp

  (type-of t)   ;; symbol
  (type-of nil) ;; symbol

```

These symbols are used to provide "yes" and "no" answers for expressions in the language.

There are many "predicates" for the different types which return =t= or =nil=, we will cover them when talking about each type.

* Equality

One of the most basic operations you would do on any type is check whether two values are the same!

There are a few ways to do that in Emacs Lisp:

- =eq= - Are the two parameters the same object?
- =eql= - Are the two parameters same object or same number?
- =equal= - Are the two parameters equivalent?
- Type-specific equality predicates

```lisp

  (setq test-val '(1 2 3))

  (eq 1 1)                  ;; t
  (eq 3.1 3.1)              ;; nil
  (eq "thing" "thing")      ;; nil
  (eq '(1 2 3) '(1 2 3))    ;; nil
  (eq test-val test-val)    ;; t

  (eql 1 1)                 ;; t
  (eql 3.1 3.1)             ;; t
  (eql "thing" "thing")     ;; nil
  (eql '(1 2 3) '(1 2 3))   ;; nil
  (eql test-val test-val)   ;; t

  (equal 1 1)               ;; t
  (equal 3.1 3.1)           ;; t
  (equal "thing" "thing")   ;; t
  (equal '(1 2 3) '(1 2 3)) ;; t
  (equal test-val test-val) ;; t

```

A general rule is that you should use =equal= for most equality checks or use a type-specific equality check.

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Equality-Predicates.html#Equality-Predicates][Emacs Lisp Manual: Equality Predicates]]

* Numbers

There are two main types of numbers in Emacs Lisp:

- Integers - Whole numbers
- Floating point numbers - Numbers with a decimal

```lisp

  1
  3.14159

  -1
  -3.14159

  1.
  1.0

  -0

```

## Operations

You can perform mathematical operations on these numbers:

```lisp

  (+ 5 5)  ;; 10
  (- 5 5)  ;; 0
  (* 5 5)  ;; 25
  (/ 5 5)  ;; 1

  ;; Nesting arithmetic!
  (* (+ 3 2)
     (- 10 5))  ;; 25

  (% 11 5)      ;; 1 - integer remainder
  (mod 11.1 5)  ;; 1.099 - float remainder

  (1+ 5)   ;; 6
  (1- 5)   ;; 4

```

You can also convert between integers and floats:

- =truncate= - Rounds float to integer by moving toward zero
- =round= - Rounds to the nearest integer
- =floor= - Rounds float to integer by subtracting
- =ceiling= - Round up to the next integer

```lisp

  (truncate 1.2)   ;; 1
  (truncate -1.2)  ;; -1

  (floor 1.2)      ;; 1
  (floor -1.2)     ;; -2

  (ceiling 1.2)    ;; 2
  (ceiling 1.0)    ;; 1

  (round 1.5)      ;; 2
  (round 1.4)      ;; 1

```

See also:

- [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Rounding-Operations.html#Rounding-Operations][Floating point rounding operations]]
- [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Bitwise-Operations.html#Bitwise-Operations][Bitwise operations]]
- [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Math-Functions.html#Math-Functions][Standard mathematical functions]]

## Predicates

These predicates will help you identify the number types in code:

```lisp

  (integerp 1)     ;; t
  (integerp 1.1)   ;; nil
  (integerp "one") ;; nil

  (floatp 1)       ;; nil
  (floatp 1.1)     ;; t
  (floatp "one")   ;; nil

  (numberp 1)      ;; t
  (numberp 1.1)    ;; t
  (numberp "one")  ;; nil

  (zerop 1)        ;; nil
  (zerop 0)        ;; t
  (zerop 0.0)      ;; t

```

## Comparisons

You can compare two numeric values (even integers against floats):

```lisp

  (= 1 1)     ;; t
  (= 1 1.0)   ;; t
  (= 1 1 1
     1 1 1)   ;; t

  (< 1 2)     ;; t
  (> 1 2)     ;; nil
  (> 1 1)     ;; nil
  (> 1.2 1)   ;; nil

  (>= 1 1)     ;; t
  (<= -1 -1.0) ;; t

  (max 1 5 2 7)  ;; 7
  (min -1 3 2 4) ;; -1

```

* Characters

Characters are really just integers that are interpreted as characters:

```lisp

  ?A   ;; 65
  ?a   ;; 97

  ?\n  ;; 10
  ?\t  ;; 9

  ;; Unicode
  ?\N{U+E0}    ;; 224
  ?\u00e0      ;; 224
  ?\U000000E0  ;; 224
  ?\N{LATIN SMALL LETTER A WITH GRAVE} ;; 224

  ;; Control and meta char syntax
  ?\C-c        ;; 3
  (kbd "C-c")  ;; "^C"
  ?\M-x        ;; 134217848
  (kbd "M-x")  ;; [134217848]

```

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Character-Type.html#Character-Type][Emacs Lisp Manual: Character Type]]

## Comparisons

```lisp

  (char-equal ?A ?A)
  (char-equal ?A 65)
  (char-equal ?A ?a)

  case-fold-search
  (setq case-fold-search nil)
  (setq case-fold-search t)

```

* Sequences

In Emacs Lisp, strings, lists, and arrays are all considered sequences

```lisp

  (sequencep "Sequence?")     ;; t
  (sequencep "")              ;; t

  (sequencep '(1 2 3))        ;; t
  (sequencep '())             ;; t

  (sequencep [1 2 3])         ;; t
  (sequencep [])              ;; t

  (sequencep 22)              ;; nil
  (sequencep ?A)              ;; nil

  ;; What do you expect?
  (sequencep nil)

```

You can get the length of any sequence with =length=:

```lisp

  (length "Hello!")    ;; 6
  (length '(1 2 3))    ;; 3
  (length [5 4 3 2])   ;; 4
  (length nil)         ;; 0

```

You can get an element of any sequence at a zero-based index with =elt=:

```lisp

  (elt "Hello!" 1)   ;; ?e
  (elt "Hello!" -1)  ;; error -out of range

  (elt '(3 2 1) 2)  ;; 1
  (elt '(3 2 1) 3)  ;; nil - out of range
  (elt '(3 2 1) -1)  ;; 3
  (elt '(3 2 1) -2)  ;; 3
  (elt '(3 2 1) -6)  ;; 3 - seems to always return first element

  (elt [1 2 3 4] 2)   ;; 3
  (elt [1 2 3 4] 5)   ;; error - out of range
  (elt [1 2 3 4] -1)  ;; error - out of range

```

* Strings

Strings are arrays of characters:

```lisp

  "Hello!"

  "Hello \
   System Crafters!"

  "Hello \\ System Crafters!"

  (make-string 5 ?!)            ;; !!!!!
  (string ?H ?e ?l ?l ?o ?!)    ;; "Hello!"

```

## Predicates

```lisp

  (stringp "Test!")           ;; t
  (stringp 1)                 ;; nil
  (stringp nil)               ;; nil

  (string-or-null-p "Test")   ;; t
  (string-or-null-p nil)      ;; t

  (char-or-string-p ?A)       ;; t
  (char-or-string-p 65)       ;; t
  (char-or-string-p "A")      ;; t

  (arrayp "Array?")           ;; t
  (sequencep "Sequence?")     ;; t
  (listp "List?")             ;; nil

```

## Comparisons

You can compare strings for equivalence or for sorting:

- =string== or =string-equal=
- =string<= or =string-lessp=
- =string>= or =string-greaterp=

```lisp

  (string= "Hello" "Hello")    ;; t
  (string= "HELLO" "Hello")    ;; nil

  (string<  "Hello" "Hello")    ;; nil
  (string<  "Mello" "Yello")    ;; t
  (string<  "Hell"  "Hello")    ;; t

  (string>  "Hello" "Hello")    ;; nil
  (string>  "Mello" "Yello")    ;; nil
  (string>  "Hell"  "Hello")    ;; nil

```

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Text-Comparison.html#Text-Comparison][Emacs Lisp Manual: Text Comparison]]

## Operations

```lisp

  (substring "Hello!" 0 4)    ;; Hell
  (substring "Hello!" 1)      ;; ello!

  (concat "Hello " "System" " " "Crafters" "!")
  (concat)

  (split-string "Hello System Crafters!")
  (split-string "Hello System Crafters!" "s")
  (split-string "Hello System Crafters!" "S")

  (split-string "Hello System Crafters!" "[ !]")
  (split-string "Hello System Crafters!" "[ !]" t)

  ;; Default splitting pattern is [ \f\t\n\r\v]+

  (setq case-fold-search nil)
  (setq case-fold-search t)

```

## Formatting

You can create a string from existing values using =format=:

```lisp

  (format "Hello %d %s!" 100 "System Crafters")
  (format "Here's a list: %s" '(1 2 3))

```

There are many more format specifications, mainly for number representations, consult the manual for more info:

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Formatting-Strings.html#Formatting-Strings][Emacs Lisp Manual: Formatting Strings]]

## Writing messages

As you've already seen, you can write messages to the echo area (minibuffer) and =*Messages*= buffer using the =message= function:

```lisp

  (message "This is %d" 5)

```

It uses the same formatting specifications as =format!=

* Lists

The list is possibly the most useful data type in Emacs Lisp.

## Cons Cells

Lists are built out of something called "cons cells".  They enable you to chain together list elements using the "cons" container.

You can think of a "cons" like a pair or "tuple" with values that can be accessed with =car= and =cdr=:

- =car= - Get the first value in the cons
- =cdr= - Get the second value in the cons

```lisp

  (cons 1 2)  ;; '(1 . 2)
  '(1 . 2)    ;; '(1 . 2)

  (car '(1 . 2))  ;; 1
  (cdr '(1 . 2))  ;; 2

  (setq some-cons '(1 . 2))

  (setcar some-cons 3)
  some-cons              ;; '(3 . 2)

  (setcdr some-cons 4)
  some-cons              ;; '(3 . 4)

```

## Building lists from cons

There are two ways to build a list from cons cells:

```lisp

  (cons 1 (cons 2 (cons 3 (cons 4 nil))))
  (cons 1 '(2 3 4))

  (cons '(1 2 3) '(4 5 6))

  (append '(1 2 3) 4)
  (append '(1 2 3) '(4))

```

## Predicates

```lisp

  (listp '(1 2 3))
  (listp 1)

  (listp nil)       ;; t
  (cons 1 nil)
  (append '(1) nil)

  (listp (cons 1 2))
  (listp (cons 1 (cons 2 (cons 3 (cons 4 nil)))))
  (consp (cons 1 (cons 2 (cons 3 (cons 4 nil)))))

```

## Alists

Association lists (or "alists") are lists containing cons pairs for the purpose of storing named values:

```lisp

  (setq some-alist '((one . 1)
                     (two . 2)
                     (three . 3)))

  (alist-get 'one   some-alist)  ;; 1
  (alist-get 'two   some-alist)  ;; 2
  (alist-get 'three some-alist)  ;; 3
  (alist-get 'four  some-alist)  ;; nil

  (assq 'one   some-alist)  ;; '(one . 1)
  (rassq 1     some-alist)  ;; '(one . 1)

  ;; There is no alist-set!
  (setf (alist-get 'one some-alist) 5)
  (alist-get 'one some-alist)  ;; 5

```

## Plists

A property list (or "plist") is another way to do key/value pairs with a flat list:

```lisp

  (plist-get '(one 1 two 2) 'one)
  (plist-get '(one 1 two 2) 'two)

  (plist-put '(one 1 two 2) 'three 3)

```

* Arrays

Arrays are sequences of values that are arranged contiguously in memory.  They are much faster to access!

The most obvious form of array is a "vector", a list with square brackets.  Strings are also arrays!

We know how to access elements in arrays, but you can set them with =aset=:

```lisp

  (setq some-array [1 2 3 4])
  (aset some-array 1 5)
  some-array

  (setq some-string "Hello!")
  (aset some-string 0 ?M)
  some-string

```

We can set all values in an array using =fillarray=

```lisp

  (setq some-array [1 2 3])
  (fillarray some-array 6)
  some-array

```

* Logic Expressions

Logic expressions allow you to combine expressions using logical operators (=and=, =or=)

You can think of this as operations on the "truthiness" or "falsiness" of expressions!

## What is true?

When evaluating expressions, everything except the value =nil= and the empty list ='()= is considered =t=!

```lisp

  (if t        'true 'false)  ;; true
  (if 5        'true 'false)  ;; true
  (if "Emacs"  'true 'false)  ;; true
  (if ""       'true 'false)  ;; true
  (if nil      'true 'false)  ;; false
  (if '()      'true 'false)  ;; false

```

## Logic operators

Emacs provides the following logic operators:

- =not= - Inverts the truth value of the argument
- =and= - Returns the last value if all expressions are truthy
- =or= - Returns the first value that is truthy (short-circuits)
- =xor= - Returns the first value that is truthy (doesn't short-circuit)

```lisp

  (not t)    ;; nil
  (not 3)    ;; nil
  (not nil)  ;; t

  (and t t t t 'foo)   ;; 'foo
  (and t t t 'foo t)   ;; 't
  (and 1 2 3 4 5)      ;; 5
  (and nil 'something) ;; nil

  (or  nil 'something) ;; 'something
  (or  nil 'something t) ;; 'something
  (or (- 3 3) (+ 2 0)) ;; 0

```

* Conditional expressions

## The =if= expression

As we saw before, the =if= expression evaluates an expression and based on the result, picks one of two "branches" to evaluate next.

The "true" branch is a single expression, the "false" branch can be multiple expressions:

```lisp

  (if t 5
    ;; You can add an arbitrary number of forms in the "false" branch
    (message "Doing some extra stuff here")
    (+ 2 2))

```

You can use =progn= to enable multiple expressions in the "true" branch:

```lisp

  (if t
    (progn
      (message "Hey, it's true!")
      5)
    ;; You can add an arbitrary number of forms in the "false" branch
    (message "Doing some extra stuff here")
    (+ 2 2))

```

Since this is an expression, it returns the value of the last form evaluated inside of it:

```lisp

  (if t 5
    (message "Doing some extra stuff here")
    (+ 2 2))

  (if nil 5
    (message "Doing some extra stuff here")
    (+ 2 2))

```

You can use =if= expressions inline when setting variables:

```lisp

  (setq tab-width (if (string-equal (format-time-string "%A")
                                    "Monday")
                      3
                      2))
```

## The =when= and =unless= expressions

These expressions are useful for evaluating forms when a particular condition is true or false:

- =when= - Evaluate the following forms when the expression evaluates to =t=
- =unless= - Evaluate the following forms when the expression evaluates to =nil=

```lisp

  (when (> 2 1) 'foo)    ;; 'foo
  (unless (> 2 1) 'foo)  ;; nil

  (when (> 1 2) 'foo)    ;; nil
  (unless (> 1 2) 'foo)  ;; 'foo

```

Both of these expressions can contain multiple forms and return the result of the last form:

```lisp

  (when (> 2 1)
    (message "Hey, it's true!")
    (- 5 2)
    (+ 2 2)) ;; 4

  (unless (> 1 2)
    (message "Hey, it's true!")
    (- 5 2)
    (+ 2 2)) ;; 4

```

## The =cond= expression

The =cond= expression enables you to concisely list multiple conditions to check with resulting forms to execute:

```lisp

  (setq a 1)
  (setq a 2)
  (setq a -1)

  (cond ((eql a 1) "Equal to 1")
        ((> a 1)   "Greater than 1")
        (t         "Something else!"))

```

## The =pcase= expression

This one is powerful!  We will cover it in a future episode.

* Loops

There are 4 ways to loop in Emacs Lisp:

## while

Loops until the condition expression returns false:

```lisp

  (setq my-loop-counter 0)

  (while (< my-loop-counter 5)
    (message "I'm looping! %d" my-loop-counter)
    (setq my-loop-counter (1+ my-loop-counter)))

```

## dotimes

```lisp

  (dotimes (count 5)
    (message "I'm looping more easily! %d" count))

```

## dolist

Loops for each item in a list:

```lisp

  (dolist (item '("one" "two" "three" "four" "five"))
    (message "Item %s" item))

```

## Recursion

Can be fun and interesting, but not safe for a loop that will have many cycles:

```lisp

  (defun efs/recursion-test (counter limit)
    (when (< counter limit)
      (message "I'm looping via recursion! %d" counter)
      (efs/recursion-test (1+ counter) limit)))

  (efs/recursion-test 0 5)

```

* What's next?

- Dive into functions!
- Shorter side videos on =pcase=, regular expressions



* =Defining Functions and Commands=
* What will we cover?

- Basic function definitions
- Function parameter types
- Documenting functions
- Invoking functions with =funcall= and =apply=
- Interactive functions
- Example code!

* What is a function?

- A reusable piece of code
- Can accept inputs via parameters
- Usually returns a result
- Often has a name, but can be anonymous!
- Can be called by any other code or function

* Defining a function

You've probably seen this before, but let's break it down:

```lisp

  (defun do-some-math (x y)
    (* (+ x 20)
       (- y 10)))

  (do-some-math 100 50)

```

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Defining-Functions.html][Emacs Lisp Manual: Defining Functions]]

* Function arguments

Functions can have different types of arguments:

- "Optional" arguments: Arguments that are not required to be provided
- "Rest" arguments: One variable to contain an arbitrary amount of remaining parameters

They can both be used in the same function definition!

```lisp
  ;; If Y or Z are not provided, use the value 1 in their place
  (defun multiply-maybe (x &optional y z)
    (* x
       (or y 1)
       (or z 1)))

  (multiply-maybe 5)              ;; 5
  (multiply-maybe 5 2)            ;; 10
  (multiply-maybe 5 2 10)         ;; 100
  (multiply-maybe 5 nil 10)       ;; 50
  (multiply-maybe 5 2 10 7)       ;; error

  ;; Multiply any non-nil operands
  (defun multiply-many (x &rest operands)
    (dolist (operand operands)
      (when operand
        (setq x (* x operand))))
    x)

  (multiply-many 5)               ;; 5
  (multiply-many 5 2)             ;; 10
  (multiply-many 5 2 10)          ;; 100
  (multiply-many 5 nil 10)        ;; 50
  (multiply-many 5 2 10 7)        ;; 700

  ;; Multiply any non-nil operands with an optional operand
  (defun multiply-two-or-many (x &optional y &rest operands)
    (setq x (* x (or y 1)))
    (dolist (operand operands)
      (when operand
        (setq x (* x operand))))
    x)

  (multiply-two-or-many 5)        ;; 5
  (multiply-two-or-many 5 2)      ;; 10
  (multiply-two-or-many 5 2 10)   ;; 100
  (multiply-two-or-many 5 nil 10) ;; 50
  (multiply-two-or-many 5 2 10 7) ;; 700

```

* Documenting functions

The first form in the function body can be a string which describes the function!

```lisp

  (defun do-some-math (x y)
    "Multiplies the result of math expressions on the arguments X and Y."
    (* (+ x 20)
       (- y 10)))

```

The convention is to capitalize the names of all parameters to the function.

Use =describe-function= to look at =alist-get= as an example!

When you need to write a longer documentation string, you can use newlines inside of the string to wrap it.  Don't indent the following lines, though!

Let's wrap this documentation string:

```lisp

  (defun do-some-math (x y)
    "Multiplies the result of math expressions on the arguments X
and Y. This documentation string is long so it wraps onto the
next line. Keep in mind that you should not indent the following
lines or the documentation string will look bad!"
    (* (+ x 20)
       (- y 10)))

```

You can use ~M-q~ inside of the documentation to wrap multi-line documentation strings!

* Functions without names

Sometimes you need to pass a function to another function (or to a hook) but you don't want to define a named function for it.

Use a lambda function!

```lisp

  (lambda (x y)
    (+ 100 x y))

  ;; You can call a lambda function directly
  ((lambda (x y)
     (+ 100 x y))
   10 20)

```

Why "lambda"?  It comes from a mathematical system called [[https://en.wikipedia.org/wiki/Lambda_calculus][lambda calculus]] where the Greek lambda (λ) was used to denote a function definition.

* Invoking functions

You can store a lambda function or named function reference in a variable:

```lisp

  ;; The usual way
  (+ 2 2)

  ;; Calling it by symbol
  (funcall '+ 2 2)

  ;; Define a function that accepts a function
  (defun gimmie-function (fun x)
    (message "Function: %s -- Result: %d"
             fun
             (funcall fun x)))

  ;; Store a lambda in a variable
  (setq function-in-variable (lambda (arg) (+ arg 1)))

  ;; Define an equivalent function
  (defun named-version (arg)
    (+ arg 1))

  ;; Invoke lambda from parameter
  (gimmie-function (lambda (arg) (+ arg 1)) 5)

  ;; Invoke lambda stored in variable (same as above)
  (gimmie-function function-in-variable 5)

  ;; Invoke function by passing symbol
  (gimmie-function 'named-version 5)

```

Maybe you have a list of values that you want to pass to a function?  Use =apply= instead!

```lisp

  (apply '+ '(2 2))
  (funcall '+ 2 2)

  ;; Even works with &optional and &rest parameters
  (apply 'multiply-many '(1 2 3 4 5))
  (apply 'multiply-two-or-many '(1 2 3 4 5))

```

* Defining commands

Interactive functions are meant to be used directly by a user in Emacs!

In Emacs terminology, an interactive function is considered to be a "command."

They provide a few benefits over normal functions

- They show up in =M-x= command list
- Can be used in key bindings
- Can have parameters sent via prefix arguments, ~C-u~

When you write your own package, your user-facing functions should be defined as commands (unless you are writing a programming library!)

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Defining-Commands.html][Emacs Lisp Manual: Defining Commands]]

## Defining an interactive function

The form =(interactive)= needs to be the first one in the function definition!

```lisp

  (defun my-first-command ()
    (interactive)
    (message "Hey, it worked!"))

```

Invoke it using =M-x=!

The description will now be different in =describe-function=.

## Interactive parameters

The =interactive= form accepts parameters that tells Emacs what to do when the command is executed interactively (either via ~M-x~ or when used via key binding).  Some examples:

*General arguments*
- =N= - Prompt for numbers or use a [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Prefix-Command-Arguments.html][numeric prefix argument]]
- =p= - Use numeric prefix without prompting (only prefix arguments)
- =M= - Prompt for a string
- =i= - Skip an "irrelevant" argument

*Files, directories, and buffers*
- =F= - Prompt for a file, providing completions
- =D= - Prompt for a directory, providing completions
- =b= - Prompt for a buffer, providing completions

*Functions, commands, and variables*
- =C= - Prompt for a command name
- =a= - Prompt for a function name
- =v= - Prompt for a custom variable name

We won't go through every possibility, check the documentation for more:

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Interactive-Codes.html#Interactive-Codes][Emacs Manual: Interactive codes]]

## Examples

Try to bind ~C-c z~ to =do-some-math= which we defined earlier:

```lisp

  (global-set-key (kbd "C-c z") 'do-some-math)

```

Let's run it!

It tells us that =commandp= returns false for this function, it's not a command!

```lisp

  (defun do-some-math (x y)
    "Multiplies the result of math expressions on the two arguments"
    (interactive)
    (* (+ x 20)
       (- y 10)))

```

Run it again!

Now it complains about not having arguments for =x= and =y=.  Let's fix it!

```lisp

  (defun do-some-math (x y)
    "Multiplies the result of math expressions on the two arguments"
    (interactive "N")
    (* (+ x 20)
       (- y 10)))

```

It needs to prompt for both parameters!

```lisp

  (defun do-some-math (x y)
    "Multiplies the result of math expressions on the two arguments"
    (interactive "N\nN")
    (* (+ x 20)
       (- y 10)))

```

Improve the prompts by adding a descriptive string after each:

```lisp

  (defun do-some-math (x y)
    "Multiplies the result of math expressions on the two arguments"
    (interactive "NPlease enter a value for x: \nNy: ")
    (* (+ x 20)
       (- y 10)))

```

Need to write out the result!

```lisp

  (defun do-some-math (x y)
    "Multiplies the result of math expressions on the arguments X and Y."
    (interactive "Nx: \nNy: ")
    (message "The result is: %d"
             (* (+ x 20)
                (- y 10))))
```

Try it with numeric prefix argument:

Let's look at a couple other examples:

```lisp

  (defun ask-favorite-fruit (fruit-name)
    (interactive "MEnter your favorite fruit: ")
    (message "Wrong, %s is disgusting!" fruit-name))

  (defun backup-directory (dir-path)
    (interactive "DSelect a path to back up: ")
    (message "Oops, I deleted %s" dir-path))

  (defun run-a-command (command)
    (interactive "CPick a command: ")
    (message "Run %s yourself!" command))

```

* A real example!

Let's define the project we'll be following for the rest of the series:

- A package for managing your dotfiles!
- Handles tangling org-mode files containing most of your configuration
- Can also initialize and manage your dotfiles repository

Today we'll define a command that automatically tangles the =.org= files in your dotfiles folder.

## Finished code

```lisp

  (setq dotfiles-folder "~/Projects/Code/emacs-from-scratch")
  (setq dotfiles-org-files '("Emacs.org" "Desktop.org"))

  (defun dotfiles-tangle-org-file (&optional org-file)
    "Tangles a single .org file relative to the path in
dotfiles-folder.  If no file is specified, tangle the current
file if it is an org-mode buffer inside of dotfiles-folder."
    (interactive)
   ;; Suppress prompts and messages
    (let ((org-confirm-babel-evaluate nil)
          (message-log-max nil)
          (inhibit-message t))
      (org-babel-tangle-file (expand-file-name org-file dotfiles-folder))))

  (defun dotfiles-tangle-org-files ()
    "Tangles all of the .org files in the paths specified by the variable dotfiles-folder"
    (interactive)
    (dolist (org-file dotfiles-org-files)
      (dotfiles-tangle-org-file org-file))
    (message "Dotfiles are up to date!"))

```


* =Defining Variables and Scopes=
* What will we cover?

- The many ways to set the value of a variable
- Defining variables
- Creating buffer-local variables
- Understanding variable scopes
- Creating variable scopes with =let=
- Defining and setting customization variables

We'll briefly apply what we've learned to our project!

* Review: what is a variable?

A variable is an association (binding) between a name (more specifically a symbol) and a value.

- ='tab-width= -> =4=

In Emacs there are many ways to define these bindings and change the values that they are associated with.

There are also a few ways to change how the value for a particular variable is resolved!

* Setting variables

You're probably familiar with this, a large part Emacs configuration is setting variables!

We usually do this with =setq=:

```lisp

(setq tab-width 4)

```

But what is this really doing?

```lisp

(set 'tab-width 4)

(set 'tab-width (- 4 2))

```

The variable does not have to exist or be pre-defined!

```lisp

(set 'i-dont-exist 5)

i-dont-exist

```

=setq= is just a convenience function for setting variable bindings.  It removes the need for quoting the symbol name!

```lisp

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse 't)
(setq scroll-step 1)
(setq use-dialog-box nil)

```

It also allows you to set multiple variables in one expression:

```lisp

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))
      mouse-wheel-progressive-speed nil
      mouse-wheel-follow-mouse 't
      scroll-step 1
      use-dialog-box nil)

```

* Defining variables

Setting global variables is easy, but what if you want to document the purpose of a variable?

This is what =defvar= is for.  It basically allows you to create a variable binding and assign a documentation string to it:

```lisp

(setq am-i-documented "no")

(defvar am-i-documented "yes"
    "I will relieve my own concern by documenting myself")

```

Why didn't =am-i-documented= show up as "yes"?  =defvar= only applies the default value if the binding doesn't already exist!

This is actually useful: packages can define their variables with =defvar= and you can set values for them *before* the package gets loaded!  Your settings will not be overridden by the default value.

If you want the default value to be immediately applied while writing your code, use =eval-defun= (~C-M-x~)

In the end, you would use =defvar= when you want to define and document a variable in your configuration or in a package.  In most other cases, plain =setq= is sufficient.

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Defining-Variables.html#Defining-Variables][Emacs Lisp Manual: Defining Variables]]

* Buffer local variables

You can set the value of a variable for the current buffer only using =setq-local=.  Any code that runs in that buffer will receive the buffer-local value instead of the global value!

This is the first example where we see how the value of a variable can be different depending on where it is accessed.

```lisp

  (setq-local tab-width 4)

```

Why do this?  There are many settings that should only be set per buffer, like editor settings for different programming languages and customization variables for major modes.

If the variable isn't already buffer-local, =setq-local= will make it so, but only for the current buffer!

```lisp

  ;; some-value doesn't exist yet!
  (setq some-value 2)

  ;; Make it buffer-local
  (setq-local some-value 4)

  ;; Using setq now will only set the buffer-local binding!
  (setq some-value 5)

  ;; A variable may only exist in a particular buffer!
  (setq-local only-buffer-local "maybe?")

```

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Buffer_002dLocal-Variables.html#Buffer_002dLocal-Variables][Emacs Lisp Manual: Buffer Local Variables]]

## Making a variable local for all buffers

You can make any variable local for all future buffers with the =make-variable-buffer-local= function:

```lisp

  (setq not-local-yet t)
  (make-variable-buffer-local 'not-local-yet)

```

If you are writing an Emacs Lisp package and want to provide a buffer-local variable, this is the way to do it!

```lisp

  ;; Defining a variable with defvar and then making it buffer local
  (defvar new-buffer-local-var 311)
  (make-variable-buffer-local 'new-buffer-local-var)

```

## Setting default values

You might also want to set the default value for a buffer-local variable with =setq-default=:

```lisp

  (setq-default not-local-yet nil)

  (setq-default tab-width 2
                evil-shift-width 2)

  ;; BEWARE!  Unexpected results using buffer-local variables:
  (setq-default evil-shift-width tab-width)

  ;; This will create a variable that doesn't exist
  (setq-default will-i-be-created t)

```

Keep in mind that =setq-default= *does not* set the value in the current buffer, only future buffers!

* Defining variable scopes

## What is a "scope"?

It's a region of your code where a variable is bound to a particular value (or not).

More specifically, the value of =x= can be different depending on where in your code you try to access it!

There are two different models for variable scope in Emacs Lisp, we will discuss this later.

## Global scope

So far, we've been using variables that are defined in the "global" scope, meaning that they are visible to any other code loaded in Emacs.  A buffer-local variable can be thought of as a global variable for a particular buffer.

Global variables are great for two things:

- Storing configuration values that are used by modes and commands
- Storing information that needs to be accessed by future invocations of a piece of code

## Defining a local scope with =let=

Sometimes you just need to define a variable temporarily without "polluting" the global scope.  For example:

```lisp

  (setq x 0)

  (defun do-the-loop ()
    (interactive)
    (message "Starting the loop from %d" x)
    (while (< x 5)
      (message "Loop index: %d" x)
      (incf x))
    (message "Done!"))

  (do-the-loop)

```

But what if we run the function again?

We can use =let= to define =x= inside of =do-the-loop=:

```lisp

  (defun do-the-loop ()
    (interactive)
    (let ((x 0))
      (message "Starting the loop from %d" x)
      (while (< x 5)
        (message "Loop index: %d" x)
        (incf x))
      (message "Done!")))

  (do-the-loop)

```

=x= is bound inside of the scope contained within the =let= expression!

However, what happened to the =x= that we defined globally?

```lisp

  (defun do-the-loop ()
    (interactive)
    (message "The global value of x is %d" x)
    (let ((x 0))
      (message "Starting the loop from %d" x)
      (while (< x 5)
        (message "Loop index: %d" x)
        (incf x))
      (message "Done!")))

```

The =x= defined in the =let= overrides the global =x=!  Now when you set the value of =x=, you are only setting the value of the local =x= binding.

*NOTE*: In the examples above, I am using =let= inside of a function definition, but it can be used anywhere!  We'll see this in the next section.

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Variable-Scoping.html#Variable-Scoping][Emacs Lisp Manual: Variable Scoping]]

## Defining multiple bindings with =let= and =let*=

Once you start writing code that isn't so trivial, you'll find that you need to initialize a few temporary variables in a function to precalculate some results before running the real function body.

The =let= expression enables you to bind multiple variables in the local scope:

```lisp

  (let ((y 5)
        (z 10))
    (* y z))

```

However, what if you want to refer to =y= in the expression that gets assigned to =z=?

```lisp

  (let ((y 5)
        (z (+ y 5)))
    (* y z))

```

=let*= allows you to use previous variables you've bound in subsequent binding expressions:

```lisp

  (let* ((y 5)
         (z (+ y 5)))
    (* y z))

```

The difference between =let= and =let*= is that =let*= actually expands to something more like this:

```lisp

  (let ((y 5))
    (let ((z (+ y 5)))
      (* y z)))

```

Side note: there are a couple of useful macros called =if-let= and =when-let=, we will cover them in another video about helpful Emacs Lisp functions!

* Understanding "dynamic" scope

Emacs Lisp uses something called "dynamic scope" by default.  This means that the value that is associated with a variable may change depending on where an expression gets evaluated.

It's easier to understand this by looking at an example:

```lisp

  (setq x 5)

  ;; x is considered a "free" variable
  (defun do-some-math (y)
    (+ x y))

  (do-some-math 10)     ;; 15

  (let ((x 15))
    (do-some-math 10))  ;; 25

  (do-some-math 10)
```

The value of =x= is resolved from a different scope based on where =do-some-math= gets executed!

This can actually be useful for customizing the behavior for functions from other packages.  We've seen this before!

```lisp

  (defun dotfiles-tangle-org-file (&optional org-file)
    "Tangles a single .org file relative to the path in
dotfiles-folder.  If no file is specified, tangle the current
file if it is an org-mode buffer inside of dotfiles-folder."
    (interactive)
   ;; Suppress prompts and messages
    (let ((org-confirm-babel-evaluate nil)
          (message-log-max nil)
          (inhibit-message t))
      (org-babel-tangle-file (expand-file-name org-file dotfiles-folder))))

```

We didn't actually change the global value of any of these variables!

The other scoping model in Emacs is called "lexical scoping".  We will cover this and contrast the differences with dynamic scoping in another video.

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Variable-Scoping.html#Variable-Scoping][Emacs Lisp Manual: Variable Scoping]]

* Defining customization variables

Customizable variables are used to define user-facing settings for customizing the behavior of Emacs and packages.

The primary difference between They show up in the customization UI (users can set them without code)

We'll only cover them briefly today because they are a core part of Emacs.  I'll make another video to cover custom variables and the customization interface in depth.

## Using =defcustom=

The =defcustom= function allows you to define a customizable variable:

```lisp

  (defcustom my-custom-variable 42
    "A variable that you can customize")

```

=defcustom= takes some additional parameters after the documentation string:

- =:type= - The expected value type
- =:group= - The symbol that identifies the "group" this variable belongs to (defined with =defgroup=)
- =:options= - The list of possible values this variable can hold
- =:set= - A function that will be invoked when this variable is customized
- =:get= - A function that will be invoked when this variable is resolved
- =:initialize= - A function to be used to initialize the variable when it gets defined
- =:local= - When =t=, automatically marks the variable as buffer-local

There are a few more properties that I didn't mention but you can find them in the manual:

[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Variable-Definitions.html][Emacs Lisp Manual: Defining Customization Variables]]
[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Group-Definitions.html][Emacs Lisp Manual: Defining Customization Groups]]

* Setting customizable variables (correctly)

Some variables are defined to be customized and could have behavior that executes when they are changed.

The important thing to know is that =setq= does not trigger this behavior!

Use =customize-set-variable= to set these variables correctly in code:

```lisp

  (customize-set-variable 'tab-width 2)
  (customize-set-variable 'org-directory "~/Notes)

```

If you're using =use-package= (which I recommend), you can use the =:custom= section:

```lisp

  (use-package emacs
    :custom
    (tab-width 2))

  (use-package org
    :custom
    (org-directory "~/Notes"))

```

- How do I know that a variable is customizable?

The easiest way is to use =describe-variable= (bound to ~C-h v~) to check the documentation.  If the variable is customizable it should say:

```lisp

  "You can customize this variable"

```

*NOTE:* The [[https://github.com/Wilfred/helpful][Helpful]] package gives a lot more useful information!

You can also use =custom-variable-p= on the variable's symbol (eval with ~M-:~)

```lisp

  (custom-variable-p 'tab-width)
  (custom-variable-p 'org-directory)
  (custom-variable-p 'org--file-cache)

```

* Continuing the project

We've covered a lot today so we'll keep the example short this time!

We're going convert a couple of the variables from last time into customizable variables using =defcustom=:

```lisp

  (defcustom dotfiles-folder "~/.dotfiles"
    "The folder where dotfiles and org-mode configuration files are stored."
    :type 'string
    :group 'dotfiles)

  (defcustom dotfiles-org-files '()
    "The list of org-mode files under the `dotfiles-folder' which
  contain configuration files that should be tangled"
    :type '(list string)
    :group 'dotfiles)

  (defun dotfiles-tangle-org-file (&optional org-file)
    "Tangles a single .org file relative to the path in
  dotfiles-folder.  If no file is specified, tangle the current
  file if it is an org-mode buffer inside of dotfiles-folder."
    (interactive)
   ;; Suppress prompts and messages
    (let ((org-confirm-babel-evaluate nil)
          (message-log-max nil)
          (inhibit-message t))
      (org-babel-tangle-file (expand-file-name org-file dotfiles-folder))))

  (defun dotfiles-tangle-org-files ()
    "Tangles all of the .org files in the paths specified by the variable dotfiles-folder"
    (interactive)
    (dolist (org-file dotfiles-org-files)
      (dotfiles-tangle-org-file org-file))
    (message "Dotfiles are up to date!"))

```

* What's next?

In the next episode we will start discussing the most important extensibility points in Emacs:

- Major and minor modes
- Hooks

* resources
https://github.com/p3r7/awesome-elisp
https://github.com/redguardtoo/mastering-emacs-in-one-year-guide/blob/master/guide-en.org
https://github.com/jtmoulia/elisp-koans
https://www.gnu.org/software/emacs/manual/html_mono/eintr.html#Why
https://github.com/caiorss/Emacs-Elisp-Programming/blob/master/Elisp_Programming.org
https://github.com/chrisdone/elisp-guide
https://github.com/caiorss/Emacs-Elisp-Programming
https://github.com/bbatsov/emacs-lisp-style-guide
