# calculus

 
The derivative of a function is one of the basic concepts of mathematics.
Together with the integral, derivative occupies a central place in calculus.

## differential
 The process of finding the derivative is called differentiation.

- The constant rule: This is simple. f (x) = 5 is a horizontal line with a slope
  of zero, and thus its derivative is also zero.

- The power rule: To repeat, bring the power in front, then reduce the power by
  That’s all there is to it. The power rule works for any power: a positive,
  a negative, or a fraction. Make sure you remember how to do the last function.
  It’s the simplest function, yet the easiest problem to miss. By the way, do
  you see how finding this last derivative follows the power rule? (Hint: x to
  the zero power equals one). You can differentiate radical functions by
  rewriting them as power functions and then using the power rule.

- The constant multiple rule: What if the function you’re differentiating begins
  with a coefficient? Makes no difference. A coefficient has no effect on the
  process of differentiation. You just ignore it and differentiate according to
  the appropriate rule. The coefficient stays where it is until the final step
  when you simplify your answer by multiplying by the coefficient. Don’t forget
  that ð (~3.14) and e (~2.72) are numbers, not variables, so they behave like
  ordinary numbers. Constants in problems, like c and k, also behave like
  ordinary numbers. So, for example,

- The sum rule: When you want the derivative of a sum of terms, take the
  derivative of each term separately.

- The difference rule: If you have a difference (that’s subtraction) instead of
  a sum, it makes no difference. You still differentiate each term separately.
  The addition and subtraction signs are unaffected by the differentiation.
 
[derivative](https://www.math24.net/definition-derivative/)

## integral
The inverse operation for differentiation is called integration.

## Functions
chain rule, partial derivatives, Functions and Their Graphs, Limits of
Functions, Definition and Properties of the Derivative, Table of First Order
Derivatives, Table of Higher Order Derivatives, Applications of the Derivative,
Properties of Differentials, Multivariable Functions, Basic Differential
Operators, Indefinite Integral, Integrals of Rational Functions, Integrals of
Irrational Functions, Integrals of Trigonometric Functions, Integrals of
Hyperbolic Functions, Integrals of Exponential and Logarithmic Functions,
Reduction Formulas for Integrals, Definite Integral, Improper Integral, Double
Integral, Triple Integral, Line Integral, Surface Integral, Some Finite Series,
Infinite Series, Alternating Series, Power Series, Taylor and Maclaurin Series,
Binomial Series


## Gradient

concept, intuition, properties,
partial derivative
directional derivative
Vector and matrix calculus
how to find derivative of {scalar-valued, vector-valued} function wrt a {scalar,
vector} -> four combinations- Jacobian


+ Eigenvalues & eigenvectors
concept, intuition, significance, how to find Principle component analysis
concept, properties, applications Singular value decomposition concept,
properties, applications

+ Gradient algorithms
local/global maxima and minima, saddle point, convex functions, gradient descent algorithms- batch,
mini-batch, stochastic, their performance comparison

+ Miscellaneous
Information theory- entropy, cross-entropy, KL divergence, mutual information
Markov Chain- definition, transition matrix, stationarity
