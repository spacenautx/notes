- https://github.com/charlax/engineering-management
- https://github.com/ryanburgess/engineer-manager
- https://github.com/kuchin/awesome-cto
- https://github.com/mateusz-brainhub/awesome-cto-resources
- https://github.com/vicoyeh/pointers-for-software-engineers
- https://github.com/ksindi/managers-playbook
- https://github.com/LappleApple/awesome-leading-and-managing
- https://github.com/engineering-management/awesome-engineering-management
- https://github.com/alan-turing-institute/the-turing-way

-----
* sell
- https://blog.zsec.uk/ltr101-selling-yourself/ 

### startup
- https://github.com/dakshshah96/awesome-startup-credits
- https://github.com/mmccaff/PlacesToPostYourStartup
- https://github.com/KrishMunot/awesome-startup
- https://github.com/trekhleb/promote-your-next-startup
- https://github.com/trekhleb/promote-your-next-startup
- https://github.com/kuchin/awesome-cto
- https://github.com/draftdev/startup-marketing-checklist
- https://github.com/mmccaff/PlacesToPostYourStartup
- https://github.com/KrishMunot/awesome-startup

### finanace
- https://github.com/emilepetrone/financial_lessons

### consulting
- https://github.com/sdg-1/consulting-handbook
