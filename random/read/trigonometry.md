#+title: trigonometry
#+OPTIONS: ^:nil num:nil
#+STARTUP: latexpreview
#+EXPORT_FILE_NAME: /home/untitled/ctftime/backend/templates/wiki/trig

* =sine=
- sin \theta is the projection to "y" direction
- $sin \Theta = \frac{opposite}{hypotenuse}$
- how much vertical / how much total
- sin $\Theta \equiv$ "chopping function in y-direction if length = 1"
-----
* =cosine=
- cosine $\Theta$ is the projection to "x" direction
- $cos \Theta = \frac{adjacent}{hypotenuse}$
- how much horizontal / how much total
- cosin \Theta \equiv "chopping function in x-direction if length = 1"
-----
* =tangent=
-----
* =secant=
-----
* =cosecant=
-----
* =cotangent=
-----

A numbered display equation:

\begin{equation}
\frac{\partial u}{\partial t}-\alpha\nabla^2u=0\tag{1}
\end{equation}

A second numbered equation:

\begin{equation}
E=MC^2\tag{2}
\end{equation}


\begin{equation}                        % arbitrary environments,
$x=\sqrt{b}$                              % even tables, figures

If $a^2=b$ and \( b=2 \), then the solution must be
either $$ a=+\sqrt{2} $$ or \[ a=-\sqrt{2} \].

$\theta$

- https://twitter.com/Rainmaker1973/status/1686426275861188620
