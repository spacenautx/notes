# interview

- https://github.com/Advanced-Frontend/Daily-Interview-Question
- https://github.com/MaximAbramchuck/awesome-interview-questions
- https://github.com/andreis/interview
- https://github.com/jwasham/coding-interview-university
- https://github.com/viraptor/reverse-interview
- https://github.com/sudheerj/javascript-interview-questions
- https://github.com/lydiahallie/javascript-questions
- https://github.com/trimstray/test-your-sysadmin-skills
- https://jordanpotti.com/2016/12/28/what-to-know-for-your-first-infosec-interview/
- https://rkoutnik.com/articles/Questions-to-ask-your-interviewer.html
- https://github.com/petermekhaeil/salary-negotiating

# resume

- https://twitter.com/hasantoxr/status/1729378033654767749
- https://github.com/WebPraktikos/universal-resume
- https://github.com/billryan/resume
- https://github.com/jankapunkt/latexcv
- https://github.com/mitchelloharawild/vitae
- https://github.com/posquit0/Awesome-CV
- https://github.com/rzashakeri/beautify-github-profile
- https://github.com/salomonelli/best-resume-ever
- https://github.com/sproogen/modern-resume-theme
- https://resumake.io/generator/templates
- https://resume.io/
- https://twitter.com/DThompsonDev/status/1637801149460885507
- https://www.kickresume.com/en/ai-resume-writer/


# remote
- https://github.com/poteto/hiring-without-whiteboards
- https://github.com/jaegeral/companies-hiring-security-remote
- https://github.com/acacess/awesome-immigration
- https://github.com/cbovis/awesome-digital-nomads
- https://github.com/engineerapart/TheRemoteFreelancer
- https://github.com/florinpop17/app-ideas
- https://github.com/georgemandis/remote-working-list
- https://github.com/hugo53/awesome-RemoteWork
- https://github.com/lukasz-madon/awesome-remote-job
- https://github.com/novoda/spikes
- https://github.com/remote-jp/remote-in-japan/blob/master/README.en.md
- https://github.com/remoteintech/remote-jobs
- https://github.com/zenika-open-source/awesome-remote-work
- https://github.com/Nithur-M/work-from-anywhere
- https://nomadlist.com/
- https://remotive.io/remote-jobs
- https://stayinghome.club/
- https://weworkremotely.com/
- https://www.remotepython.com/

01. Hubspot Talent
02. Working nomads
03. Virtual vocations
04. Just Remote
05. FlexJobs
06. JobsPresso
07. Skip The Drive
08. Remote co
09. WellFound
10. Authentic Jobs
11. PowerToFly
12. Upwork
13. Fiverr
- Aerolab : http://aerolab.co
- Aerostrat : http://aerostratsoftware.com
- AgFlow : http://agflow.com
- Aha! : http://aha.io
- Algorithmia : http://algorithmia.com
- ALICE : http://aliceplatform.com
- Alight Solutions : http://alight.com
- 10up : http://10up.com
- 15Five : http://15five.com
- 17hats : http://17hats.com
- 1Password : http://1password.com
- Acquia : http://acquia.com
- Adzuna : http://adzuna.co.uk
- AEStudio : http://ae.studio
- BetaPeak : http://betapeak.com
- BetterUp : http://betterup.com
- Beyond Company : http://beyondcompany.com.br
- BeyondPricing : http://beyondpricing.com
- Big Cartel : http://bigcartel.com
- Bill : http://bill.com
- Bit Zesty : http://bitzesty.com
- CartStack : http://cartstack.com
- Casumo : http://casumo.com
- Celsius : http://celsius.network
- ChainLink Labs : http://chainlinklabs.com
- Chargify : http://chargify.com
- charity: water : http://charitywater.org
- ChatGen : http://ChatGen.AI
- DataStax : http://datastax.com
- Datica : http://datica.com
- DealDash : http://dealdash.com
- Delighted : http://delighted.com
- Designcode : http://designcode.io
- Dev Spotlight : http://devspotlight.com
- Devsquad : http://devsquad.com
- EPAM : http://epam.com
- Epic Games : http://epicgames.com/site/en-US/car…
- Epilocal : http://epilocal.com
- Episource : http://episource.com
- Equal Experts Portugal : http://equalexperts.com/contact-us/lis…
- Ergeon : http://ergeon.com
- Estately : http://estately.com
- Fireball Labs : http://fireballlabs.com
- FivexL : http://fivexl.io
- Flexera : http://flexera.com
- FlightAware : http://flightaware.com
- Flip : http://flip.id
- Flowing : http://flowing.it
- http://Fly.io : http://fly.io
- Gaggle : http://gaggle.net
- Geckoboard : http://geckoboard.com
- GEO Jobe : http://geo-jobe.com
- Hack Reactor Remote : http://hackreactor.com/remote/
- Hanno : http://hanno.co
- Hanzo Archives : http://hanzo.co
- Happy Cog : http://happycog.com
- iFit : http://ifit.com
- Igalia : http://igalia.com
- Incsub : http://incsub.com
- Jackson River : http://jacksonriver.com
- JupiterOne : http://jupiterone.com/careers/
- Kaggle : http://kaggle.com
- kea : http://kea.ai
- Labelbox : http://labelbox.com
- LeadIQ : http://leadiq.com
- Manifold : http://manifold.co
- Maxicus : http://maxicus.com
- Nagarro : http://nagarro.com/en
- Netguru : http://netguru.com
- Skillcrush : http://skillcrush.com
- SmartCash : http://smartcash.cc
- TED : http://ted.com
- Udacity : http://udacity.com
- VMware : http://vmware.com/in.html
- Wells Fargo : http://wellsfargo.com
- Wolfram : http://wolfram.com
- Xapo : http://xapo.com/en/
- YAZIO : http://yazio.com/en/jobs
- Zup : http://zup.com.br


# others
- https://www.classcentral.com/report/free-certificates/
- https://github.com/paralax/awesome-cybersecurity-internships

