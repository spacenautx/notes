### research
- https://github.com/pliang279/awesome-phd-advice
- http://karpathy.github.io/2016/09/07/phd/
- https://www.openbookpublishers.com/books/10.11647/obp.0235
- https://www.cantorsparadise.com/the-shortest-physics-paper-ever-published-41f79291ceb4
- https://twitter.com/dr_asadnaveed/status/1752325049908957517



Paperpal: Writing assistant and grammar corrector.
Elicit: Helps you find relevant papers.
BioRender: Great to make medical figures. You'll never go back to making figures with PowerPoint again! 
Zotero: Open source reference/ citations manager. It lets you automatically download PDFs and take notes as well.
Researchgate: When you don't have access to an article, you can ask the author on Researchgate to share it with you. 
Dropbox: Never loose your files again! Dropbox can automatically upload and backup your files.
Pomodoro: Focus on work using 25 or 30 minute time blocks.
Sci-hub: Find a paper when your library's access fails you. 
Canva: Easy-to-use graphic designer. Design your conference presentation.
Quillbot or WordTune: rewriter/ paraphraser
Scrivener: A good tool to build your overall dissertation which you will later improve. 
Taskade: Working on multiple projects? Make Gantt diagrams to track progress and stay productive.
LaTex: Software for typesetting documents. It is used as a document markup language.
Turnitin: Plaigiarism checker 
