# arXiv

## Required arXiv Submission Checklist

### 1.) Are you an arXiv registered author?
Required registration information:
▹ Email address — should be current; an academic or research institution email is highly recommended
▹ Username — should be unique; cannot be changed!
▹ Password — must be 8 characters, 1 non-letter
Personal information
▹ Name
▹ Affiliation
▹ Archive(s) most likely to submit to
All members must comply with arXiv’s Code of Conduct

### 2.) Are your email address and affiliation up to date?
Update your user information here.

### 3.) Are you the original author of the article?
Are you a proxy that has been pre-approved by arXiv?
Special permission must be granted for third party or proxy submission of articles. Learn more and request permission here.

### 4.) Are you the sole author of the article?
Have all co-authors consented to submission to arXiv?
All co-authors must consent to submission of an article to arXiv. Learn more here.

### 5.) Does your article display “scholarly interest, originality, novelty, and/or significance”?
Articles that do not include substantial research, or that are too similar to other recent submissions may be declined. Read more about the arXiv moderation guidelines here.

### 6.) Have you or do you plan on submitting your article to a journal or other outlet that prohibits distribution of your article on arXiv?
When you submit, you grant arXiv an irrevocable license to include your article in their repository. arXiv will not remove an article to comply with a journal policy at any time.
Read more in the Instructions for Submission and Submission Terms and Agreement.

### 7.) Have you selected a license for your article?
arXiv requires irrevocable permission from authors to share your article. This permission is granted through one of their available licenses.

### 8.) Do you know the Archive and Subject Class where you plan to submit?
Review the categories within each of the major archives on arXiv and select the best fit for your article.

### 9.) Have you been endorsed to submit to your selected subject category?
Have you submitted to that Archive and Subject Class previously?
Endorsement is required for first-time submissions to a category to ensure that arXiv contributors are members of the scientific community. Endorsement is not a form of peer-review!
Some arXiv authors are given automatic endorsement through their academic or research institutions.
If you are unsure if you are endorsed, you can start the submission process here. Fill out the information on the first page and click Continue. If you need endorsement, a request for endorsement message will appear at the top of the page. You will not be able to move on in the submission process until you are endorsed.
To receive endorsement, follow the instructions here.

### 10.) Is your entire article (text, figures, tables, etc) all included in one PDF file?
When you export your article as a PDF from Curvenote, everything is included in one file and we’ve taken care of arXiv’s PDF formatting requirements.
Ancillary files are not currently supported with PDF submissions to arXiv.
If you are submitting multiple versions of your article in multiple languages, both versions should be combined into a single PDF file.

### 11.) If you have a copyright statement in your PDF, does it comply with arXiv’s redistribution license?
Copyright statements are only allowed if they do not contradict the redistribution license agreement necessary for submission and acceptance of your article.

### 12.) Does your PDF file name meet the format requirements?
Only contains these characters: a-z A-Z 0–9 _ + — . , =
File names on arXiv are case sensitive. Any submissions with file names that contain other characters besides those listed above will be rejected.

### 13.) Does your title meet the format requirements for the web submission form field?
Not all uppercase letters
No unicode characters
Only some MathJax
TeX macros expanded
Spelled correctly
Proper use of TeX accent commands
References to other articles on arXiv should be formatted as
arXiv:arch-ive/YYMMNNN or arXiv:YYMM.NNNN(N)

### 14.) Does your author list meet the format requirements for the web submission form field?
All author names are included in the list (no et al.)
Author names or initials are in one of the following formats — last name is your family name
First Last or First Middle Last or F. Last or F. M. Last
Multiple last names entered in order First Last Last
Proper use of TeX accent commands
No names with “,” or “and”
No honorifics (Dr., Professor)
No degree suffixes (MD, PhD)
Other suffixes entered as First Last IV or First Last Jr
Author affiliation(s) follow the author name in parentheses, in either of the following formats:
Author One (Affiliation1), Author Two (Affiliation1), Author Three (Affiliation2)
Author One (1), Author Two (1 and 2), Author Three (2)
((1) Affiliation1, (2) Affiliation2)
Author names are separated with a comma or “and”:
First Last, First Last, First Last
First Last, First Last and First Last
If you are submitting as a named collaboration, one of the following formats should be used.
Article written by a collaboration which is comprised of the following authors:
Collaboration: Author One, Author Two, Author Three
Article written by the following authors on behalf of the collaboration:
Author One, Author Two, Author Three for the Collaboration
Article written by the following authors who are members of the collaboration:
Author One, Author Two, Author Three (the Collaboration)

### 15.) Does your abstract meet the format requirements for the web submission form field?
Less than 1,920 characters — when you attempt to export your PDF from Curvenote, you will be notified if your abstract is too long.
Does not include “abstract”
Only some TeX commands with MathJax
TeX macros expanded
No TeX formatting commands (\it \,)
Proper use of TeX accent commands
For new paragraphs, all carriage returns must be followed by white space (indent). Other carriage returns will be removed.
References to other articles on arXiv should use the following format
arXiv:arch-ive/YYMMNNN or arXiv:YYMM.NNNN(N)
When submitting an article in a language other than English or multiple languages, an English version of the abstract is required and should be submitted in the following format:
English abstract
Other language abstract

### 16.) If your article has a report number from your institution, have you included it in the web submission form field?
Nothing else should be included in this field.

### 17.) If your article has already appeared in a journal or proceedings, does the full bibliographic reference meet the format requirements for the web submission form field?
References should include the volume number, year, and page number/range.
Multiple references should be separated by a semicolon and space
No URLs
Only submissions that have previously appeared in journals or proceedings can be included in this field. You can add a journal reference to your arXiv article after submission and once it appears elsewhere.

### 18.) If your article has already appeared in a journal or proceedings, does the DOI meet the format requirements for the web submission form field?
Only DOI(s) should be included in this field. Example: 10.1016/j.cageo.2015.09.015

### Multiple DOIs should be separated by a space
Only submissions that have previously appeared in journals or proceedings can be included in this field. You can add a DOI to your arXiv article after submission and once it appears elsewhere.

### 19.) If you are submitting to the Math archive, do you have your mathematical classification code and does it meet the format requirements for the web submission form field?
Find your classification code according to the Mathematics Subject Classification here.
Format your classification code such that:
▹ “Primary” and “Secondary” are in parentheses
▹ Multiple classification keys are separated with commas
▹ If there is only a primary classification, “Primary” keyword is optional

### 20.) If you are submitting to the Computer Science archive, do you have your classification code and does it meet the following format requirements for the web submission form field?
Find your classification code according to the ACM Computing Classification System here.
Format your classification code such that:
Multiple classifications are separated by a semicolon and space

### 21.) Have you carefully checked the title, abstract, and processed files for errors?
If you find an error after submission and before the public announcement you can click “Unsubmit” from the Actions column on your user page. You can then make changes and resubmit your article.
If you find an error after the public announcement of your article, DO NOT make a new submission with the corrections. The original article should be replaced following the instructions here.
Optional arXiv Submission Recommendations
In addition to the necessary components, there are a few recommended components for arXiv article submission. We have included the recommendations and common use cases for these components below.
Comments
Entered into the comments field in the web submission form.
Cannot be edited without creating a new version of your article.
No copyright statements of license information should be included here.
Optional but recommended comments include the following:
Number of pages
Number of figures
“Submitted to” or “Accepted to” with journal and/or conference proceedings information
URL(s) for related files or information — we recommend including a link to your original Curvenote article.
Check that any periods or text following the URL are separated from the URL for correct interpretation.
On the archive, the URL will be shown as a link with the text “this http URL”, and as such the surrounding text should be written accordingly.
If other than English, note the language of the main text or that there are multiple versions in different languages.
References to other articles on arXiv should use the following formats
arXiv:arch-ive/YYMMNNN or arXiv:YYMM.NNNN(N)
Author roles — such as “Appendix” by or “Editor”
If this is a replacement version — comments on the extent of the changes and reasons for replacement.
Proper use of TeX accent commands
Large image file sizes
Including large figures in a PDF submission can slow the display of your article from some readers. arXiv recommends to keep document sizes down with efficient figures.
As arXiv continues to support and to further open-access publication, our mission at Curvenote is to help scientists, researchers, and engineers accelerate scientific discovery through communication. As a scientific writing tool focused on collaboration, we understand that science can only move forward when we share our ideas and work together.
Curvenote strives to support authors in every step of their research workflows, from note-taking, drafting, collaboration, reproducibility, and publication. You can learn more about how Curvenote can help in our interview with Dr. Lindsey Heagy, assistant professor at the University of British Columbia, Research Workflows. To learn more about Curvenote’s dedicated export feature and integration of arXiv’s formatting template, check out Steve Purves’ blog How to export to PDF & LaTeX using Curvenote.
We hope that this arXiv submission checklist helps speed you through the process, and share your research even earlier!

### resource
https://towardsdatascience.com/a-checklist-for-submitting-your-research-to-arxiv-64f31b4127d2
