# economics


- https://aeturrell.github.io/coding-for-economists/intro.html

- https://github.com/goabstract/Marketing-for-Engineers
- https://github.com/antontarasenko/awesome-economics
- https://github.com/salesforce/ai-economist
- https://bigthink.com/culture-religion/kahnemans-mind-clarifying-systems/#link_time=1464976243

* Open Expenses

- A curated list of businesses publicly sharing their expenses.

[Across Customer support from Slack channels.](https://web.archive.org/web/20200313180716/https://acrossapp.com/blog/how-a-2-person-startup-already-uses-28-other-tools)
[Baremetrics Revenue analytics and dunning tools for subscription businesses.](https://twitter.com/Shpigford/status/1070017669888425984)
[Buffer Social media management software for brands.](https://open.buffer.com/transparent-pricing-buffer/)
[Buttondown Simple newsletter service.](https://www.notion.so/Running-Costs-f29729ded5494272947f656440967cbf)
[Candy Japan Japanese candy subscription service.](https://www.candyjapan.com/behind-the-scenes/running-costs-for-candy-japan)
[Cronitor Cron job, background task, and uptime monitoring.](https://blog.cronitor.io/the-aws-spend-of-a-saas-side-business-30bd5dbd91b)
[Cushion Time tracking, invoicing, and work forecasting for freelancers.](https://cushionapp.com/running-costs)
[Hanno Agency specialized in digital healthcare.](https://hanno.co/blog/investing-in-tools-no-brainer/)
[Hexadecimal No-nonsense website monitoring and status page service.](https://tryhexadecimal.com/running-costs)
[Hexicon Strategy word game (mobile app).](https://www.indiehackers.com/post/it-cost-us-2933-40-to-develop-our-mobile-game-as-a-side-project-heres-what-we-spent-it-on-fb9e33536d)
[Leave Me Alone Email subscription manager.](https://leavemealone.app/open/)
[Pinboard Bookmarking for introverts.](https://twitter.com/Pinboard/status/494238943894700032)
[Pixelic Design collaboration software for product teams.](https://blog.pixelic.io/startup-tool-stack/)
[ReferralHero Referral marketing software.](https://manuel.friger.io/blog/referralhero-expenses)
[Simple Analytics Simple, privacy-friendly analytics.](https://simpleanalytics.com/open)
[Sitesauce Convert dynamic websites into static ones.](https://sitesauce.app/open)

