## Step 1: Choose a concept you want to learn about.  What topic are you curious
about?  Once you identify a topic, take out a blank sheet of paper. Write out
everything you know about the subject you want to understand as if you were
teaching it to a child.  As you learn more about the topic, add it to your
sheet. Often people find it helpful to use a different color pen so you can see
your learning grow.  Once you think you understand the topic, move on to step 2.


## Step 2: Explain it to a 12-year-old Now that you think you understand a topic
reasonably well, explain it to a 12-year-old.  Use your sheet as a reference and
try to remove any jargon or complexity. Only use simple words. Only use words a
child would understand.


## Step 3: Reflect, Refine, and Simplify Only when you can explain the subject
in simple terms do you understand it.  Simple is beautiful.  Review your notes
to make sure you didn?t mistakenly borrow any jargon or gloss over anything
complicated.  Read it out loud as if to a child. If the explanation isn?t simple
enough or sounds confusing, that?s a good indication that you need to reflect
and refine.  Go back to the source material, reviewing the parts you don?t quite
understand yet.  Repeat until you have a simple explanation.


## Step 4: Organize and Review To test your understanding in the real world, run
it by someone else. How effective was your explanation? What questions did they
ask? What parts did they get confused about?  When you?re happy with your
understanding, take the page you created with a simple explanation and put it
into a binder. Following this technique for everything you learn gives you a
binder full of learning that you can review a couple of times a year.
