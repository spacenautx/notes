# programming

- [advent of code](https://advenofcode.com)
- [rosetta](http://rosettacode.org/wiki/Category:Programming_Tasks)
- [project-euler](https://www.project-euler.net)
- [recursion](https://github.com/asweigart/recursion_examples)
- [100 code](100_code.md)
- [100 days frontend](100_day_project.md)
- [100 day project](100_day_project.md)
- [50 projects 50 days](https://github.com/bradtraversy/50projects50days)
- [interactive coding challeges](https://github.com/donnemartin/interactive-coding-challenges)
- [how to be a programmer](https://github.com/braydie/HowToBeAProgrammer)
- [tech handbook](https://github.com/yangshun/tech-interview-handbook)
- [professional programming](https://github.com/charlax/professional-programming)
- [lightoj](https://lightoj.com/problems/category)
- [pytudes](https://github.com/norvig/pytudes)
- https://github.com/Anon-Artist/100-days-of-OffSec
- https://codingchallenges.fyi/challenges/intro
