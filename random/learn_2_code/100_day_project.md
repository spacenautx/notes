# 100 day project

-------

## 100 days of frontend coding [0/16]
#### Days 01-08: HTML [0/9]

- [ ] Take the Basic HTML and HTML5 section on freeCodeCamp.
- [ ] HTML page structure
- [ ] HTML elements
- [ ] Nesting HTML elements
- [ ] Semantic markup
- [ ] Links / multiple pages
- [ ] Images
- [ ] Audio/video media
- [ ] Forms and form elements
- [ ] Create a multi-page website! (See Project Ideas if you need some inspiration).

#### Days 09-16: CSS [0/9]

- [ ] Take the Basic CSS, CSS flexbox, and CSS grid sections on freeCodeCamp.
- [ ] In-line CSS
- [ ] style tags
- [ ] External CSS with <link>
- [ ] Styling elements
- [ ] Selectors
- [ ] Floats, clearing floats
- [ ] Layouts (grid, flexbox)
- [ ] Fonts, custom fonts
- [ ] Style the HTML page(s) you made when learning HTML!

#### Days 17-24: JavaScript Basics [0/9]

- [ ] Take the Basic JavaScript and ES6 sections on freeCodeCamp.
- [ ] Too many language fundamentals to list here!
- [ ] script tag and placement
- [ ] Accessing HTML elements
- [ ] The event loop, call stack, and event queue
- [ ] Prototypal Inheritance
- [ ] Reference vs. value
- [ ] Add some dynamic elements or logic to your HTML/CSS page(s) developed earlier!

#### Days 25-27: jQuery [0/6]

- [ ] Take the jQuery section on freeCodeCamp.
- [ ] Document ready
- [ ] Selectors
- [ ] Toggle classes
- [ ] Animation
- [ ] Add or move HTML elements
- [ ] Add jQuery to your site!

#### Days 28-33: Responsive Web Design [0/5]

- [ ] Take the Responsive Web Design Principles section on freeCodeCamp.
- [ ] Media queries, breakpoints
- [ ] Responsive images
- [ ] Make your website responsive!
- [ ] Use Chrome DevTools to view your site on different devices/viewports.

#### Days 34-36: Accessibility [0/4]

- [ ] Take the Applied Accessibility section on freeCodeCamp.
- [ ] Read some content on The A11Y Project
- [ ] Review their checklist
- [ ] Update your site(s) for accessibility based on this checklist

#### Days 37-39: Git [0/13]

- [ ] Git Tutorial for Beginners (Video)
- [ ] Install git
- [ ] Set up a github account
- [ ] Learn the most-used git commands:
- [ ] init
- [ ] clone
- [ ] add
- [ ] commit
- [ ] push
- [ ] pull
- [ ] merge
- [ ] rebase
- [ ] Add your existing projects to github!

#### Days 40-44: Node and NPM [0/4]

- [ ] Research node and how it is different than the browser
- [ ] Install node (npm comes with it)
- [ ] Create a simple javascript file and run it with node
- [ ] Use NPM to manage any dependencies in your existing project(s) (e.g., jQuery!)

#### Days 45-50: Sass [0/9]

- [ ] Install Sass globally with npm!
- [ ] Sass Crash Course Video
- [ ] Follow the Learn Sass tutorial and/or freeCodeCamp Sass tutorial.
- [ ] Update your existing site to generate your CSS using Sass!
- [ ] Days 51-54: Bootstrap
- [ ] Learn what Bootstrap is and why you would want to use it
- [ ] Bootstrap 4 Crash Course (Video)
- [ ] Complete the Bootstrap section on freeCodeCamp
- [ ] Refactor your site using bootstrap!

#### Days 55-66: BEM [0/15]

- [ ] Read the BEM introduction
- [ ] Why I Use BEM (Video)
- [ ] Create a simple webpage using BEM conventions.
- [ ] Days 57-61: Gulp
- [ ] Install gulp with npm
- [ ] Follow the gulp for beginners tutorial on CSS-Tricks
- [ ] In your website, set up gulp to:
- [ ] Compile Sass for you
- [ ] Put the generated CSS file in build subdirectory
- [ ] Move your web pages to the build directory
- [ ] Inject a reference to your generated CSS file into your web pages
- [ ] Days 62-65: Webpack
- [ ] Read webpack concepts
- [ ] What is Webpack, How does it work? (Video)
- [ ] This webpack tutorial

#### Days 66-68: ESLint [0/3]

- [ ] Install eslint using npm
- [ ] How to Setup VS Code + Prettier + ESLint (Video)
- [ ] Lint your JavaScript

#### Days 68-83: React [0/4]

- [ ] Take the React tutorial
- [ ] Learn React with Mosh
- [ ] Refactor your website as a React app!
- [ ] Note: create-react-app is a convenient tool to scaffold new React projects.

#### Days 84-89: Redux [0/7]

- [ ] This Redux video tutorial
- [ ] This Redux video series by Dan Abramov, creator of Redux
- [ ] Take note of the Redux three principles
- [ ] Single source of truth
- [ ] State is read-only
- [ ] Changes are made with pure functions
- [ ] Add Redux state management to your app!

#### Days 90-94: Jest [0/3]

- [ ] Learn Jest basics
- [ ] If you used create-react-app, Jest is already configured.
- [ ] Add tests to your application!

#### Days 95-97: TypeScript [0/3]

- [ ] Learn TypeScript in 5 minutes
- [ ] Learn TypeScript in 50 minutes (Video)
- [ ] Optionally create a React app with TypeScript

#### Days 98-100: NextJS [0/3]

- [ ] Next.js Getting Started
- [ ] Next.js Crash Course (Video)
- [ ] Create a Next.js app or migrate your existing app to Next.js

* [100 days of code frontend](https://github.com/nas5w/100-days-of-code-frontend)
