- https://github.com/vossenjp/bashcookbook-examples/
- https://github.com/vossenjp/bashidioms-examples
- Cybersecurity Ops with bash

# grep
- searches for pattern in files and prints each line that matches the input pattern

```grep -<options> <pattern> <filenames>```

- grep can be used to search in a single file or in multiple files

## 12.1. Useful ```grep``` options

option  | description
---     | ---
-i      | ignore case
-n      | display line numbers along with lines
-v      | display lines that do not match the pattern
-c      | count the number of matching lines
-r      | search in all files under each directory
-l      | diplay the filename of the file which has the matching pattern
-o      | print only the matched string. The whole line with the matched string is not printed
-I      | ignore binary files
-A\<n>  | include n lines after match
-B\<n>  | include n lines before match
-C\<n>  | include n lines before and after the match

### 12.1.1. Examples
- lets say a demo file(demo.txt) has following content

> THIS IS UPPER CASE LINE<br>
> this is lower case line<br>
> This is regular line<br>
> This line is also regular<br>
> Line number four<br>
> Line #5<br>
> last line

- Search for a string in a file<br>
  ```grep "this" demo.txt```

- Search for a string in a file, without matching case<br>
  ```grep -i "this" demo.txt```

- Search for a string in a file and get both the line number and output<br>
  ```grep -n "this" demo.txt```

- Get the number of lines matching the searched string<br>
  ```grep -c "this" demo.txt```

- Get the filename in which the searched string is found<br>
  ```grep -l "this" demo.txt```

- Get 2 lines after the matching line<br>
  ```grep -A2 "This" demo.txt```

- Get 2 line before the matching line<br>
  ```grep -B2 "This" demo.txt```

- Get 2 lines before and after the matching line<br>
  ```grep -C2 "This" demo.txt```

- Search recursively in all files in subdirectories<br>
  ```grep -inrI 'some text' /path/to/dir```

## 12.2. Regular expression in grep
- a regular expression is a sequence of characters that specifies the search pattern in text.
- different characters have special meaning in regular expressions

character | description
---       | ---
[abc]     | matches any one of the characters in the square brackets
[a-d]     | matches any one of the characters in the range specified in the square brackets
^start    | matches the pattern only if the pattern is at the start of the line
end$      | matches the pattern only if the pattern is at the end of the line
[^abc]    | matches any one character that is NOT present in the square brackets
[^a-d]    | matches any one character that is NOT present in the range
.         | matches any one character
\*        | mathces 0 or more occurences of the preceding character
.*        | matches zero or more of any character

## 12.3. Examples

- Match any one character<br>
  ```grep "[Tt]his" demo.txt```

- Search for line starting with the search pattern<br>
  ```grep "^last" demo.txt```

- Search for line ending with the search patter<br>
  ```grep "regular$" demo.txt```

- Search for line with a character in specified range<br>
  ```grep "[0-9]" demo.txt```

- Search for line without a character in specified range<br>
  ```grep "[^0-9]" demo.txt```

- Search for a line where the middle characters are not known<br>
  ```grep "line.*regular" demo.txt```

**Note:** grep can have regular expressions in the search pattern part, and can have wildcards in the files to search section.
```


### bash server
- https://dev.to/leandronsp/building-a-web-server-in-bash-part-i-sockets-2n8b

### tricks
- https://github.com/hackerschoice/thc-tips-tricks-hacks-cheat-sheet
- http://www.pixelbeat.org/docs/unix-parallel-tools.html
- https://github.com/trinib/Linux-Bash-Commands
- https://xtremepentest.gumroad.com/l/bash-scripting-cheatsheet


### Resources
- https://github.com/vastutsav/command-line-quick-reference/blob/main/README.md

- https://github.com/jlevy/the-art-of-command-line
- https://github.com/denysdovhan/bash-handbook
- https://github.com/Idnan/bash-guide
- http://conqueringthecommandline.com/book/frontmatter
- https://github.com/onceupon/Bash-Oneliner
- https://github.com/dylanaraps/pure-bash-bible
- https://tldp.org/LDP/abs/html/

### sed-awk
- https://github.com/james-s-tayler/grep-sed-awk-magic
- https://github.com/imrannissar/awk-and-sed
- https://github.com/codenameyau/sed-awk-cheatsheet
- https://posts.specterops.io/fawk-yeah-advanced-sed-and-awk-usage-parsing-for-pentesters-3-e5727e11a8ad

