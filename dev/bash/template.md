```bash

#!/bin/bash

set -o nounset
set -o errexit

if ! <possible failing command> ; then
    echo "failure ignored"
fi

ExtractBashComments() {
    egrep "^#"
} 
cat myscript.sh | ExtractBashComments | wc 
comments=$(ExtractBashComments < myscript.sh)

SumLines() {  # iterating over stdin - similar to awk      
    local sum=0
    local line=””
    while read line ; do
        sum=$((${sum} + ${line}))
    done
    echo ${sum}
} 
SumLines < data_one_number_per_line.txt 
log() {  # classic logger
   local prefix="[$(date +%Y/%m/%d\ %H:%M:%S)]: "
   echo "${prefix} $@" >&2
} 
log "INFO" "a message"

```

### scripts 
- https://github.com/HarshCasper/Rotten-Scripts
- https://github.com/avinashkranjan/Amazing-Python-Scripts
- https://github.com/mahmoud/awesome-python-applications

#### resources
- http://robertmuth.blogspot.com/2012/08/better-bash-scripting-in-15-minutes.html
- [advanced](https://tldp.org/LDP/abs/html/)
- [error handling](https://www.jtrocinski.com/posts/Bash-Error_handling_in_scripts.html)
- https://hacklido.com/blog/172-bash-for-hackers-learn-the-art-of-bash-scripting
- https://xtremepentest.gumroad.com/l/bash-scripting-cheatsheet
- https://www.networkworld.com/article/3688357/bash-scripting-tips-that-can-save-time-on-linux.amp.html
- https://www.freecodecamp.org/news/bash-scripting-tutorial-linux-shell-script-and-command-line-for-beginners/
- https://www.codelivly.com/bash-scripting-tips-that-can-save-time-on-linux/
- https://www.codelivly.com/bash-scripting-tutorial/
- https://github.com/bobbyiliev/introduction-to-bash-scripting
- https://github.com/niieani/bash-oo-framework
- https://www.networkworld.com/article/3692812/verifying-bash-script-arguments.html
- https://github.com/juan131/bash-libraries
- https://github.com/aks/bash-lib
- https://github.com/pirate/bash-utils
