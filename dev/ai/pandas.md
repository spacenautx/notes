- [explore](#initial analysis)
- [cleaning](#cleaning)
- [filtering and sorting](#filtering and sorting)
- [grouping](#grouping)
- [Apply](#Apply)
- [merge](#merge)
- [stats](#stats)
- [charts](#charts)
- [resources](#resources)

# pandas

```python
# from a CSV file
df = pd.read_csv(filename)
# from a delimited text file (like TSV)
df = pd.read_table(filename)
# from an Excel file
df = pd.read_excel(filename)
# reads from a SQL table/database
df = pd.read_sql(query, connection_object)
# reads from a JSON formatted string, URL or file.
df = pd.read_json(json_string)
# parses an html URL, string or file and extracts tables to a list of dataframes
df = pd.read_html(url)
# takes the contents of your clipboard and passes it to read_table()
df = pd.read_clipboard()
# from a dict, keys for columns names, values for data as lists
df = pd.DataFrame(dict)
# json list into df
df = pd.DataFrame.from_records(data)
```

## initial analysis

```python
df.info()

# first 10 entries
df.head(10)

# last 10 entries
df.tail(10)

# number of rows in the dataset
df.shape[0]

# number of columns in the dataset
df.shape[1]

# name of all columns
df.columns

# name of 5th column
df.columns[4]

# dataset indexed
df.index

# describe
df.describe()
df.describe(include = "all")
df.column_name.describe()

df.column_name.unique()

# non null observations in the given axis
df.count()
# Returns the correlation between columns in a DataFrame
df.corr()
# Returns the highest value in each column
df.max()
# Returns the lowest value in each column
df.min()
# Returns the median of each column
df.median()
# Returns the standard deviation of each column
df.std()

# mean
df.points.mean()

# median
df.points.median()

df.column_name.value_counts()
df
```

import pandas_profiling as pp
pp.ProfileReport(df) #to display the report

## cleaning

```python
# dropna drops missing values (think of na as "not available")
df = df.dropna(axis=0)
```

```python
# csv file
df = pd.read_csv('data.csv')

# from json
df = pd.DataFrame(data=raw_data)
df = pd.DataFrame(raw_data, columns = raw_data.keys()) # name each column if needed


# most numbers
c = df.groupby('column_name')
c = c.sum()
c.sort_values(['column_name'], ascending = False)

# Convert the type of the column Year to datetime64
crime.Year = pd.to_datetime(crime.Year, format='%Y')
crime = crime.set_index('Year', drop = True) # Set Year as the index of the dataframe

# Is there any duplicate dates?
apple = apple.set_index('Date')
apple.index.is_unique
# Make the first entry the oldest date.
apple.sort_index(ascending = True).head()
# difference in days between the first day and the oldest
(apple.index.max() - apple.index.min()).days
# How many months in the data we have?
apple_months = apple.resample('BM').mean()
len(apple_months.index)

# Group the year by decades and sum the values
# Pay attention to the Population column number, summing this column is a mistake
# To learn more about .resample (https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.resample.html)
# To learn more about Offset Aliases (http://pandas.pydata.org/pandas-docs/stable/timeseries.html#offset-aliases)
crimes = crime.resample('10AS').sum() # Uses resample to sum each decade
population = crime['Population'].resample('10AS').max() # Uses resample to get the max value only for the "Population" column
crimes['Population'] = population # Updating the "Population" column

# most dangerous decade to live in the US?
crime.idxmax(0) # apparently the 90s was a pretty dangerous time in the US

# Delete the Total column
del crime['Total']

# total
df.column_name.sum()

# item into float
df.column_name.dtype

# rename
# Change the name of the columns to bedrs, bathrs, price_sqr_meter
housemkt.rename(columns = {0: 'bedrs', 1: 'bathrs', 2: 'price_sqr_meter'}, inplace=True)
housemkt.head()
countries.rename({'capital': 'capital_city', 'language': 'most_spoken_language'}, axis='columns')
countries.columns = ['country', 'capital_city', 'continent', 'most_spoken_language']

# Reverse row order
# To reverse the row order, we make use of the loc operator. This works in the following way:
df.loc[::-1]
df.loc[::-1].reset_index(drop=True)

# reverse column order
df.loc[:, ::-1]

# reindex the DataFrame so it goes from 0 to 299
bigcolumn.reset_index(drop=True, inplace=True)

# lambda function and change the type of item
dollarizer = lambda x: float(x[1:-1])
df.item_price = df.item_price.apply(dollarizer)

capitalizer = lambda x: x.capitalize()
stud_alcoh['Mjob'].apply(capitalizer)
stud_alcoh['Fjob'].apply(capitalizer)
stud_alcoh['Mjob'] = stud_alcoh['Mjob'].apply(capitalizer)
stud_alcoh['Fjob'] = stud_alcoh['Fjob'].apply(capitalizer)

# revenue for the period in the dataset
revenue = (df['quantity'] * df['item_price']).sum()

# get unique team names
df.home_team_name.nunique()

# number of team_name occurences
df.home_team_name.value_counts().count()

# most frequent
df.column_name.value_counts().head(1).index[0]

# least occurrence
df.column_name.value_count()

# mean
round(df.total_goal_count.mean(), 2)

# avg goals per game
(df['total_goal_count'] / df.shape[0]).sum().mean()

```

## filtering and sorting

```python

# set column_name as the index of the dataframe
army.set_index('column_name', inplace=True)

# clean the column_name column and transform it in a float
prices = [float(value[1 : -1]) for value in df.column_name]

# reassign the column with the cleaned prices
df.column_name = prices

# delete the duplicates in item_name and quantity
df_filtered = df.drop_duplicates(['item_name','quantity','choice_description'])

# df_filtered
# select only the products with quantity equals to 1
df_one_prod = df_filtered[df_filtered.quantity == 1]
df_one_prod

df_one_prod[df_one_prod['column_name']>10].column_name.nunique()
df_one_prod[df_one_prod['column_name']>10]
#df.query('price_per_item > 10').df.nunique()

df.query("Quantity == 95 and UnitPrice == 182")
df.query("Quantity == 95 & UnitPrice == 182")
df.query("Quantity == 95 or UnitPrice == 182")
df.query("not (Quantity == 95)")
df.query("Quantity != 95")
df.query("Status == 'Not Shipped'")
df.query("Shipping_Cost*2 < 50")
df.query("Quantity**2 + Shipping_Cost**2 < 500")
df.query("sqrt(UnitPrice) > 15")
df.query("sqrt(UnitPrice) < Shipping_Cost/2")
df.query("OrderDate.dt.month == 8")
df.query("OrderDate.dt.month == 8 and OrderDate.dt.year == 2021 and OrderDate.dt.day >=15")
df.query("OrderDate >= '2021-08-15' and OrderDate <= '2021-08-31'")
df.query("OrderDate >= '2021-08-15' and OrderDate <= '2021-08-31' and Status == 'Delivered'")
df["OrderDate"] = pd.to_datetime(df["OrderDate"], format="%Y-%m-%d")


# delete the duplicates in item_name and quantity
df_filtered = df.drop_duplicates(['item_name','quantity'])

# select only the products with quantity equals to 1
df_one_prod = df_filtered[df_filtered.quantity == 1]

# select only the item_name and item_price columns
price_per_item = df_one_prod[['item_name', 'item_price']]

# sort the values from the most to less expensive
price_per_item.sort_values(by = "item_price", ascending = False)

# Sort by the name of the item
df.item_name.sort_values()
# OR
df.sort_values(by = "item_name")

# most expensive item
df.sort_values(by = "item_price", ascending = False).head(1)

# How many times was a Veggie Salad Bowl ordered?
df_salad = df[df.item_name == "Veggie Salad Bowl"]
len(df_salad)

# How many times did someone order more than one Canned Soda?
df_drink_steak_bowl = df[(df.item_name == "Canned Soda") & (df.quantity > 1)]
len(df_drink_steak_bowl)

# View only the columns Team, Yellow Cards and Red Cards and assign them to a dataframe called discipline
# filter only giving the column names
discipline = euro12[['Team', 'Yellow Cards', 'Red Cards']]

# Sort the teams by Red Cards, then to Yellow Cards
discipline.sort_values(['Red Cards', 'Yellow Cards'], ascending = False)

# Calculate the mean Yellow Cards given per Team
round(discipline['Yellow Cards'].mean())

# Filter teams that scored more than 6 goals
euro12[euro12.Goals > 6]
army[army["deaths"] > 50] # Select rows where df.deaths is greater than 50
army[(army["deaths"] > 500) | (army["deaths"] < 50)] # Select rows where df.deaths is greater than 500 or less than 50
army[army["regiment"] != "Dragoons"]    # Select all the regiments not named "Dragoons"

# Select the teams that start with G
euro12[euro12.Team.str.startswith('G')]

# use .iloc to slices via the position of the passed integers
euro12.iloc[: , 0:7] # : means all, 0:7 means from 0 to 7
euro12.iloc[: , :-3] # use negative to exclude the last 3 columns
army.iloc[2:7, 2:6]  # rows 3 to 7 and the columns 3 to 6
army.iloc[4:, :]     # every row after the fourth row and all columns


# .loc is another way to slice, using the labels of the columns and indexes
euro12.loc[euro12.Team.isin(['England', 'Italy', 'Russia']), ['Team','Shooting Accuracy']]
army.loc[["Arizona"]].iloc[:, 2] # Select the third cell in the row named Arizona
army.loc[["Maine", "Alaska"], ["deaths", "size", "deserters"]] # Select the 'deaths', 'size' and 'deserters' columns from Maine and Alaska
army.loc[["Texas", "Arizona"], :] # Select the rows called Texas and Arizona
army.loc[:, ["deaths"]].iloc[2] # Select the third cell down in the column named deaths
```

## grouping

```python

# Which continent drinks more beer on average?
drinks.groupby('continent').beer_servings.mean()
# For each continent print the statistics for wine consumption.
drinks.groupby('continent').wine_servings.describe()

# For each combination of occupation and gender, calculate the mean age
users.groupby(['occupation', 'gender']).age.mean()
drinks.groupby('continent').mean() # Print the mean alcohol consumption per continent for every column
users.groupby('occupation').age.mean() # mean age per occupation

#Discover the Male ratio per occupation and sort it from the most to the least
def gender_to_numeric(x): # create a function
    if x == 'M':
        return 1
    if x == 'F':
        return 0
users['gender_n'] = users['gender'].apply(gender_to_numeric) # apply the function to the gender column and create a new column
a = users.groupby('occupation').gender_n.sum() / users.occupation.value_counts() * 100
a.sort_values(ascending = False) # sort to the most male

# Print the median alcohol consumption per continent for every column
drinks.groupby('continent').median()

# For each occupation, calculate the minimum and maximum ages
users.groupby('occupation').age.agg(['min', 'max'])

# Print the mean, min and max values for spirit consumption.
drinks.groupby('continent').spirit_servings.agg(['mean', 'min', 'max'])  # This time output a DataFrame


# For each occupation present the percentage of women and men
gender_ocup = users.groupby(['occupation', 'gender']).agg({'gender': 'count'}) # create a data frame and apply count to gender
occup_count = users.groupby(['occupation']).agg('count') # create a DataFrame and apply count for each occupation
occup_gender = gender_ocup.div(occup_count, level = "occupation") * 100 # divide the gender_ocup per the occup_count and multiply per 100
occup_gender.loc[: , 'gender'] # present all rows from the 'gender column'


# Present the mean preTestScores grouped by regiment and company without heirarchical indexing
regiment.groupby(['regiment', 'company']).preTestScore.mean().unstack()

# number of observations in each regiment and company
regiment.groupby(['company', 'regiment']).size()


# Iterate over a group and print the name and the whole data from the regiment
for name, group in regiment.groupby('regiment'): # Group the dataframe by regiment, and for each regiment,
    print(name) # print the name of the regiment
    print(group) # print the data of that regiment
```

## Apply

```python

#Create a function called majority that returns a boolean value to a new column called legal_drinker
def majority(x):
    if x > 17:
        return True
    else:
        return False
stud_alcoh['legal_drinker'] = stud_alcoh['age'].apply(majority)
stud_alcoh.head()

# Multiply every number of the dataset by 10.
def times10(x):
    if type(x) is int:
        return 10 * x
    return x
stud_alcoh.applymap(times10).head(10)
```

## merge

```python
cars = cars1.append(cars2)

data1 = pd.DataFrame(raw_data_1, columns = ['subject_id', 'first_name', 'last_name'])
data2 = pd.DataFrame(raw_data_2, columns = ['subject_id', 'first_name', 'last_name'])
data3 = pd.DataFrame(raw_data_3, columns = ['subject_id','test_id'])
# Join the two dataframes along rows and assign all_data
all_data = pd.concat([data1, data2])
# Join the two dataframes along columns and assing to all_data_col
all_data_col = pd.concat([data1, data2], axis = 1)
# Merge all_data and data3 along the subject_id value
pd.merge(all_data, data3, on='subject_id')
# Merge only the data that has the same 'subject_id' on both data1 and data2
pd.merge(data1, data2, on='subject_id', how='inner')
# Merge all values in data1 and data2, with matching records from both sides where available.
pd.merge(data1, data2, on='subject_id', how='outer')

```

## stats

```python
 #Group the dataset by name and assign to names
del baby_names["Year"] # you don't want to sum the Year column, so you delete it

names = baby_names.groupby("Name").sum() # group the data
names.head() # print the first 5 observations
names.sort_values("Count", ascending = 0).head() # sort it from the biggest value to the smallest one

# What is the name with most occurrences?
names.Count.idxmax()
# OR
names[names.Count == names.Count.max()]

# How many different names have the least occurrences?
len(names[names.Count == names.Count.min()])

# What is the median name occurrence?
names[names.Count == names.Count.median()]

# What is the standard deviation of names?
names.Count.std()

# Compute how many values are missing for each location over the entire record.
# They should be ignored in all calculations below.
# "Number of non-missing values for each location: "
data.isnull().sum()

# Compute how many non-missing values there are in total.
#number of columns minus the number of missing values for each location
data.shape[0] - data.isnull().sum()
#or
data.notnull().sum()

# Downsample the record to a yearly frequency for each location.
data.groupby(data.index.to_period('A')).mean()
# Downsample the record to a monthly frequency for each location.
data.groupby(data.index.to_period('M')).mean()
# Downsample the record to a weekly frequency for each location.
data.groupby(data.index.to_period('W')).mean()

#Calculate the min, max and mean windspeeds and standard deviations of the windspeeds
# across all locations for each week
# (assume that the first week starts on January 2 1961) for the first 52 weeks.
# resample data to 'W' week and use the functions
weekly = data.resample('W').agg(['min','max','mean','std'])
# slice it for the first 52 weeks and locations
weekly.loc[weekly.index[1:53], "RPT":"MAL"] .head(10)
```

## charts

```python

# set seaborn style to white
sns.set_style("white")

# Plot the total_bill column histogram
# create histogram
ttbill = sns.distplot(tips.total_bill);

# set lables and titles
ttbill.set(xlabel = 'Value', ylabel = 'Frequency', title = "Total Bill")

# take out the right and upper borders
sns.despine()

sns.jointplot(x ="total_bill", y ="tip", data = tips)

# Create one image with the relationship of total_bill, tip and size.
# Hint: It is just one function.
sns.pairplot(tips)

# Present the relationship between days and total_bill value
sns.stripplot(x = "day", y = "total_bill", data = tips, jitter = True);

# Create a scatter plot with the day as the y-axis and tip as the x-axis, differ the dots by sex
sns.stripplot(x = "tip", y = "day", hue = "sex", data = tips, jitter = True);

# Create a box plot presenting the total_bill per day differetiation the time (Dinner or Lunch)
sns.boxplot(x = "day", y = "total_bill", hue = "time", data = tips);

# Create two histograms of the tip value based for Dinner and Lunch. They must be side by side.
# better seaborn style
sns.set(style = "ticks")

# creates FacetGrid
g = sns.FacetGrid(tips, col = "time")
g.map(plt.hist, "tip");


# Create two scatterplots graphs, one for Male and another for Female,
# presenting the total_bill value and tip relationship, differing by smoker or no smoker
# They must be side by side.
g = sns.FacetGrid(tips, col = "sex", hue = "smoker")
g.map(plt.scatter, "total_bill", "tip", alpha =.7)

g.add_legend()
```
# write output
```python
# write
with pd.ExcelWriter("path_to_file.xlsx") as writer:
    df.to_excel(writer, sheet_name="Sheet1")

# append
with pd.ExcelWriter("path_to_file.xlsx", mode="a", engine="openpyxl") as writer:
    df.to_excel(writer, sheet_name="Sheet1")
```

## resources

- https://www.w3resource.com/python-exercises/
- https://www.machinelearningplus.com/python/101-pandas-exercises-python/
- https://www.dataschool.io/python-pandas-tips-and-tricks/
- https://github.com/justmarkham/pycon-2019-tutorial/blob/master/tutorial.ipynb
- [large dataset](https://towardsdatascience.com/how-to-handle-large-datasets-in-python-1f077a7e7ecf)
- https://pandastutor.com/
- https://www.gormanalysis.com/blog/python-pandas-for-your-grandpa/
- https://www.analyticsvidhya.com/blog/2022/07/step-by-step-exploratory-data-analysis-eda-using-python/
