| Google Dataset           | Search	Super broad, varying quality                              |
| Kaggle	               | More limited, but lots of context and community                  |
| KDNuggets	               | Specific for AI, ML, data science                                |
| Government websites	   | Wide variety, resources to learn                                 |
| Pudding.cool	           | Pop culture, essays                                              |
| 538	                   | Sports, politics, clean data                                     |
| Tidy Tuesdays	           | Messy data, great community                                      |
| GitHub	               | Huge amount of searchable data with commentary, variable quality |
| Buzzfeed	               | Pop culture, essays, rigorous science                            |
| Awesome Public Datasets  | Wide variety, only datasets, no commentary                       |

### power analysis
- https://twitter.com/selcukorkmaz/status/1688287551893704704
