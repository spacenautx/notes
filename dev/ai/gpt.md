### learn
- https://jalammar.github.io/how-gpt3-works-visualizations-animations/
- https://jaykmody.com/blog/gpt-from-scratch/ 

### math
- https://mem.ai/p/t1VxYX2lrfcGgTSmjERk


### llm
- https://gist.github.com/veekaybee/be375ab33085102f9027853128dc5f0e
- https://github.com/WooooDyy/LLM-Agent-Paper-List

- https://github.com/f/awesome-chatgpt-prompts
- https://www.youtube.com/watch?v=kCc8FmEb1nY
- https://github.com/nomic-ai/gpt4all
- https://github.com/joonspk-research/generative_agents

### prompt
- https://learnprompting.org/docs/intermediate/zero_shot_cot
- https://github.com/dair-ai/Prompt-Engineering-Guide

### llm
- https://threadreaderapp.com/thread/1659608476455256078.html
- https://roadmap.sh/guides/introduction-to-llms
- https://pub.towardsai.net/from-novice-to-expert-a-comprehensive-step-by-step-study-plan-for-mastering-llms-dc9feb60ecc4
- https://github.com/mlabonne/llm-course
- https://finbarr.ca/how-is-llama-cpp-possible/

### llama3 from scratch
- https://github.com/naklecha/llama3-from-scratch

### cookbook
- https://github.com/openai/openai-cookbook/tree/main

### security
- https://github.com/fr0gger/Awesome-GPT-Agents
