    Capturing patterns from data is called fitting or training the model.
    The data used to fit the model is called the training data.

    The steps to building and using a model are:

- Define: What type of model will it be? A decision tree? Some other type of model? Some other parameters of the model type are specified too.
- Fit: Capture patterns from provided data. This is the heart of modeling.
- underfit
- overfit
- Predict: Just what it sounds like
- Evaluate: Determine how accurate the model's predictions are.

-  Define model. Specify a number for random_state to ensure same results each run. You use any number, and model quality won't depend meaningfully on exactly what value you choose.
model = DecisionTreeRegressor(random_state=1)



# models

- linear regression
- logistic regression
- support vector machine
- naive bayes
- k-nearest neighbours
- decision tree
- random forest
- adaboost
- gradient boost
- xgboost
- lightgbm
- catboost

- k-means clustering
- hierarchial clustering
- dbscan clustering

- principal component analysis

# 30 models in 2 seconds

```python
import lazypredict
from lazypredict.Supervised import LazyClassifier
# Finally, let's run the models and see how it goes.

clf = LazyClassifier(verbose=0,ignore_warnings=True)
models, predictions = clf.fit(X_train, X_test, y_train, y_test)
models
# Keep in mind that the results obtained with lazy predict SHOULD NOT be considered final models
```

# model validation
- gradio
- train_test_split
- k-fold cross validation
- Leave-one-out Cross-Validation
- Stratified K-Fold Cross-Validation
- Mean Absolute Error (also called MAE)
- Leave-one-group-out Cross-Validation
- Nested Cross-Validation
- Time-series Cross-Validation
- Wilcoxon signed-rank test
- McNemar’s test
- 5x2CV paired t-test
- 5x2CV combined F test

error = actual − predicted
- sklearn = train_test_split
