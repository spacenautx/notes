### resources

- [deep dive into deep learning](http://d2l.ai/chapter_appendix-mathematics-for-deep-learning/index.html)
- [linear algebra series](https://www.youtube.com/watch?v=kjBOesZCoqc&list=PL0-GT3co4r2y2YErbmuJw2L5tW4Ew2O5B)
- [linear regression](https://threadreaderapp.com/thread/1714619920955346995.html)
- [calculus series](https://www.youtube.com/watch?v=WUvTyaaNkzM)
- [deep learning series](https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi&index=2)
- [statistics](https://www.statlearning.com)
- [statistics concepts](https://kanger.dev/basic-statistics-for-data-science-concepts-guide/)
- [harvard probability](https://www.youtube.com/watch?v=KbB0FjPg0mw)
- [self learner](https://www.neilwithdata.com/mathematics-self-learner)
- [ema](https://github.com/stakewithus/notes/blob/main/notebook/ema.ipynb)
- https://twitter.com/pendulum_bot/status/1636071602335907841
- [matrix multiplication](https://twitter.com/TivadarDanka/status/1635262943616172032)
- [p-value](https://statmodeling.stat.columbia.edu/2023/04/14/4-different-meanings-of-p-value-and-how-my-thinking-has-changed/)

- [linear algebra](https://github.com/weijie-chen/Linear-Algebra-With-Python)

- [cards](https://franknielsen.github.io/Cards/index.html)
- [high dimensional probability](https://www.math.uci.edu/~rvershyn/teaching/hdp/hdp.html)
