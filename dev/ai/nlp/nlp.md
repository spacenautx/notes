### preprocessing
- stemming
- lemmatization = combining similar words
- stemming
- removing stopwords
- tagging

### semantics
- classification

- vector
- ngram

### resources
- https://alvinntnu.github.io/NTNU_ENC2045_LECTURES/intro.html
- https://realpython.com/python-speech-recognition/
- https://github.com/cgpotts/cs224u/
- https://stanford-cs324.github.io/winter2022/
