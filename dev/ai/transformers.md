- https://pytorch.org/tutorials/beginner/deep_learning_60min_blitz.html
- https://peterbloem.nl/blog/transformers

- https://betterprogramming.pub/a-guide-on-the-encoder-decoder-model-and-the-attention-mechanism-401c836e2cdb
- https://towardsdatascience.com/attention-is-all-you-need-discovering-the-transformer-paper-73e5ff5e0634
- https://arxiv.org/pdf/1801.10198.pdf
- [Attention Is All You Need](https://arxiv.org/abs/1706.03762)
- https://github.com/valeman/Transformers_Are_What_You_Dont_Need
 
 
- https://twitter.com/omarsar0/status/1756350236744663519

- https://www.linkedin.com/posts/tom-yeh_deeplearning-transformer-neuralnetwork-activity-7151207905055100928-qsXV/?utm_source=share&utm_medium=member_desktop

- https://twitter.com/Saboo_Shubham_/status/1764142216987955369
