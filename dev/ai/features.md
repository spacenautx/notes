### resources
- https://github.com/ashishpatel26/Amazing-Feature-Engineering/blob/master/A%20Short%20Guide%20for%20Feature%20Engineering%20and%20Feature%20Selection.md

The columns that are inputted into our model (and later used to make predictions) are called "features."

Sometimes, you will use all columns except the target as features. Other times you'll be better off with fewer features.
- By convention, this data is called X.

Selecting The Prediction Target
- By convention, the prediction target is called y

# most feature selection methods can be divided into three major buckets
## Basic Selection Methods

Removing Constant Features
Removing Quasi-Constant Features
Removing Duplicated Features

## Correlation Feature Selection
Removing Correlated Features
Basic Selection Methods + Correlation - Pipeline

## - Filter based: We specify some metric and based on that filter features. An example of such a metric could be correlation/chi-square.
Filter Methods: Univariate Statistical Methods
Mutual Information
Chi-square distribution
Anova
Basic Selection Methods + Statistical Methods - Pipeline
Filter Methods: Other Methods and Metrics
Univariate roc-auc, mse, etc
Method used in a KDD competition - 2009

## - Wrapper-based: Wrapper methods consider the selection of a set of features as a search problem. Example: Recursive Feature Elimination
Wrapper Methods
Step Forward Feature Selection
Step Backward Feature Selection
Exhaustive Feature Selection


## Embedded: Embedded methods use algorithms that have built-in feature selection methods. For instance, Lasso and RF have their own feature selection methods.
Embedded Methods: Linear Model Coefficients
Logistic Regression Coefficients
Linear Regression Coefficients
Effect of Regularization on Coefficients
Basic Selection Methods + Correlation + Embedded - Pipeline
Embedded Methods: Lasso
Lasso
Basic Selection Methods + Correlation + Lasso - Pipeline
Embedded Methods: Tree Importance
Random Forest derived Feature Importance
Tree importance + Recursive Feature Elimination
Basic Selection Methods + Correlation + Tree importance - Pipeline


## Hybrid Feature Selection Methods
Feature Shuffling
Recursive Feature Elimination
Recursive Feature Addition




# featuretools


# FEATURE SELECTION

## Filter methods
* Wrapper methods
* Embedded methodsFILTER METHODS
* Statistical measures
Basics
* Fisher score
* Constant
* Univariate methods
* Quasi-constant
* Mutual information
* Duplicated
* Correlation
* BONUS: ALTERNATIVE FILTER METHODS
* Alternative non-mainstream
* methods learnt from Data
* Science competitions
* WRAPPER METHODS
* Step forward selection
* Step backward selection
* Exhaustive search
* BONUS: ALTERNATIVE WRAPPER METHODS
* Feature shufflingEMBEDDED METHODS
* LASSO
* Decision tree derived importance
* Regression coefficientsBONUS: HYBRID METHODS
* Recursive feature
* elimination
----------------------------------------------------------------------------------------------------
* Feature selection is the process of selecting a subset of relevant features
* (variables, predictors) for use in machine learning  model building.WHY SHOULD WE SELECT FEATURES?
* Simple models are easier to interpret
* Shorter training times
* Enhanced generalisation by reducing overfitting
* Easier to implement by software developers
* Reduced risk of data errors during model use
* Variable redundancy
* Bad learning behaviour in high dimensional spacesFEATURE SELECTION: PROCEDURE
* A feature selection algorithm can be seen as the combination of a search technique for proposing new feature subsets, along with an evaluation measure which scores the different feature subsets.
* Computationally expensive
* Different feature subsets render optimal performance for different machine learning algorithms
----------------------------------------------------------------------------------------------------

## FILTER METHODS
* Rely on the characteristics of the data (feature characteristics)
* Do not use machine learning algorithms
* Model agnostic
* Tend to be less computationally expensive
* Usually give lower prediction performance than a wrapper methods
* Are very well suited for a quick screen and removal of irrelevant featuresWRAPPER METHODS
* Use predictive machine learning models to score the feature subset
* Train a new model on each feature subset
* Tend to be very computationally expensive
* Usually provide the best performing feature subset for a given machine learning algorithm
* They may not produce the best feature combination for a different machine learning modelEMBEDDED METHODS
* Perform feature selection as part of the model construction process
* Consider the interaction between features and models
* They are less computationally expensive than wrapper methods, because they fit the machine learning model only once.FEATURE SELECTION: METHODS
* Filter Wrapper Embedded
* Variance * Forward  * LASSO
* selection
* Correlation * Tree
* Backward  importance
* Univariate
* Exhaustive
* search
----------------------------------------------------------------------------------------------------
* FILTER METHODS
* Select variables independently of the machine learning algorithm
* Rely only on the characteristics of the data (of the variables)
* Model agnostic
* Fast computationFILTER METHODS | UNIVARIATE
* Two step procedure:
* Rank features according to a certain criteria
* Each feature is ranked independently of the feature space
* Select the highest ranking features
* May select redundant variables because they do not consider the relationships between features

# FILTER METHODS | RANKING CRITERIA Feature scores on various statistical tests:
* Chi-square | Fisher Score
* Univariate parametric tests (anova)
* Mutual information
* Variance
* Constant features
* Quasi-constant featuresFILTER METHODS | MULTIVARIATE
* Handle redundant feature
* Duplicated features
* Correlated features
* Simple yet powerful methods to quickly remove irrelevant and redundant features
* First step in feature selection proceduresFILTER METHODS
* Quick dataset screening for irrelevant features
* Quick removal of redundant features
* Constant features
* Duplicated features
* Correlated features
----------------------------------------------------------------------------------------------------
## WRAPPER METHODS
* Evaluate the features in the light of a specific machine learning algorithm
* Evaluate subsets of variables
 Detect interactions between variables
 Find the optimal feature subset for the desired classifierWRAPPER METHODS | PROCEDURE
* Search for a subset of features
* Build a machine learning model on the selected feature subset
* Evaluate model performance
* Repeat
 How to search for the subset of features?
 How to stop the search?WRAPPER METHODS | SEARCH
* Forward feature selection
 Adds 1 feature at a time
* Backward feature elimination
 Removes 1 feature at a time
* Exhaustive feature search
 Searches across all possible feature combinationsWRAPPER METHODS | SEARCH
* Greedy algorithms
* Aim to find the best possible combinations
* Computationally expensive
* Often impracticable (Exhaustive search)WRAPPER METHODS | STOPPING CRITERIA
* Performance increase
* Performance decrease
* Predefined number of features is reached
 Stopping criteria are somewhat arbitrary
 To be defined by userWRAPPER METHODS | SUMMARY
* Better predictive accuracy than filter methods
* Best performing feature subset for the predefined classifier
* Computationally expensive
* Stopping criteria is relatively arbitrary
----------------------------------------------------------------------------------------------------
## EMBEDDED METHODS
* Perform feature selection during the modelling algorithm's execution.
* These methods are thus embedded in the algorithm either as its normal or extended functionality.
 Faster than wrapper methods
 More accurate than filter methods
 Detect interactions between variables
 Find the feature subset for the algorithm being trainedEMBEDDED METHODS | PROCEDURE
* Train a machine learning algorithm
* Derive the feature importance
* Remove non-important featuresEMBEDDED METHODS
Embedded
Methods
Tree  Regression
LASSO
importance coefficientsEMBEDDED METHODS | SUMMARY
* Better predictive accuracy than filter methods
* Faster than wrapper methods
* Render generally good feature subsets for the used algorithm
* Constrained to the limitations of the algorithm
----------------------------------------------------------------------------------------------------
## BASIC METHODSFILTER METHODS - BASIC METHODS
* Constant features
* Quasi-constant features
* Duplicated features
 Duplicated features may arise after one hot encoding of categorical
variablesFILTER METHODS - BASIC METHODS
A combination of boosting and bagging for KDD Cup 2009 – Fast Scoring on a Large Database.

----------------------------------------------------------------------------------------------------
## CORRELATIONFILTER METHODS - CORRELATION
* Correlation is a measure of the linear relationship of 2 or more variables
* Through correlation, we can predict one variable from the other
* Good variables are highly correlated with the target
* Correlated predictor variables provide redundant information
* Variables should be correlated with the target but uncorrelated among
themselvesCORRELATION FEATURE SELECTION
The central hypothesis is that good feature sets contain features that are highly
correlated with the class, yet uncorrelated with each other
M. Hall 1999, Correlation-based Feature Selection for Machine Learning, PhD ThesisCORRELATION AND MACHINE LEARNING
* Correlated features do not necessarily affect model accuracy per se.
* High dimensionality does
* If 2 features are highly correlated, the second one will add little
information over the previous one: removing it helps reduce dimension
* Correlation affects model interpretability: linear models
* Different classifiers show different sensitivity to correlationFILTER METHODS - CORRELATION
Pearson’s correlation coefficient:
𝑆𝑢𝑚 𝑋1 − 𝑋1𝑚𝑒𝑎𝑛 × 𝑋2 − 𝑋2𝑚𝑒𝑎𝑛 × 𝑋𝑛 − 𝑋𝑛_𝑚𝑒𝑎𝑛
𝑉𝑎𝑟𝑋1 × 𝑉𝑎𝑟𝑋2 × 𝑉𝑎𝑟𝑋𝑛
Pearson’s coefficient values vary between -1 and 1:
1 is highly correlated: the more of variable x1, the more of x2
-1 is highly anti-correlated: the more of variable x1, the less of x2

----------------------------------------------------------------------------------------------------
## STATISTICAL AND RANKING PROCEDURES
Statistical –
ranking methods
Information  Univariate roc-
Fisher Score Univariate tests
Gain auc / rmseSTATISTICS AND RANKING METHODS
Two steps:
Rank features based
Select features with
on certain criteria /
highest rankings
metric
Pros and Cons
 Fast
 Does not contemplate feature redundancySTATISTICS AND RANKING METHODS
Variable Target
X1 Y
Evaluate if the variable is important to discriminate the targetMUTUAL INFORMATION
* Measures the mutual dependence of 2 variables
* Determines how similar the joint distribution p(X,Y) is to the products of
individual distributions p(X)p(Y)
* If X and Y are independent, their MI is zero
* If X is deterministic of Y, the MI is the uncertainty in X.MUTUAL INFORMATION
𝑃(𝑥𝑖, 𝑦𝑗)
  𝑃 𝑥𝑖, 𝑦𝑗 × log
𝑃 𝑥𝑖 𝑃(𝑦𝑗)
𝑖,𝑦FISHER SCORE
* Measures the dependence of 2 variables
* Suited for categorical variables.
* Target should be binary
* Variable values should be non-negative, and typically Boolean, frequencies,
or counts.
* It compares observed distribution of class among the different labels
against the expected one, would there be no labelsFISHER SCORE
Male Female Total Row
Survived = 1 2 9 11
Survived = 0 10 3 13
Total column 12 12 24
Male Female Total Row
Survived = 1 0.17 0.75 0.46
Survived = 0 0.38 0.25 0.54
Total column 1 1 1
# FISHER SCORE UNIVARIATE TESTS
* Measures the dependence of 2 variables  ANOVA
* Suited for continuous variables
* Requires a binary target
* Sklearn extends the test to continuous targets with a correlation trick
* Assumes linear relationship between variable and target
* Assumes variables are normally distributed
* Sensitive to the sample sizeUNIVARIATE ROC-AUC / RMSE
* Measures the dependence of 2 variables  using machine learning
* Suited for all types of variables
* Makes no assumption on the distribution of the variablesUNIVARIATE ROC-AUC / RMSE
* Builds decision tree  Ranks the features  Selects the features with using a single variable  according to the model  the highest machine and the target roc-auc or rmse learning metrics roc-auc = 0.5 means random
----------------------------------------------------------------------------------------------------
# GREEDY ALGORITHMS WRAPPER METHODS
* Greedy search algorithms
* Utilise a specific classifier to select the optimal set of features
* Sequential feature selection algorithms add or remove one feature
at the time based on the classifier performance until a feature
subset of the desired size k is reached, or any other desired
criteria is metWRAPPER METHODS
* Step Forward feature selection
Step
algorithms begin from no feature and
Forward
add one at a time
Step
* Step backwards feature selection
Backward
begins from all the features and
removes one feature at a time
Exhaustive
* Exhaustive feature selection tries all
possible feature combinationsSTEP FORWARD FEATURE SELECTION
Chooses the
Evaluates all one that
Evaluates all  subsets of 2  Evaluates  Repeats until provides subsets of 1  features (the  algorithm  criteria is best feature first selected  performance met algorithm and another)
performanceSTEP BACKWARD FEATURE SELECTION
Removes  Removes  Removes
1feature and  second  third feature  Repeats until Evaluates all evaluates  feature and  and  criteria is features first algorithm  measures  evaluates  met
performance performance performanceEXHAUSTIVE FEATURE SELECTION
* Makes all possible feature combinations from 1 to n = total features.
* And selects the one that provides best performance.GREEDY SEQUENTIAL ALGORITHMS
* Extremely computationally expensive
* Often not feasible due to number of features in dataset
* Feature space optimised for a specific algorithm
* Should provide the highest performance
----------------------------------------------------------------------------------------------------
# LASSO REGULARISATION
Regularization consists in adding a penalty on the different parameters of the model to reduce the freedom of the model. Hence, the model will be less likely to fit the noise of the training data and will improve the generalization abilities of the model. For linear models there are in general 3 types of regularisation:
* The L1 regularization (also called Lasso)
* The L2 regularization (also called Ridge)
* The L1/L2 regularization (also called Elastic net)REGULARISATION: LASSO
1
2 1
×   𝑦 − 𝑦𝑝𝑟𝑒𝑑 + ⋋   ∅
2𝑚
m = number of observations
Y = observed output
Ypred = predicted output ∅ 𝑋 + ∅ 𝑋 + … + ∅ 𝑋
1 1 2 2 𝑛 𝑛
 is the regularisation parameter
L1 / Lasso will shrink some parameters to zero, therefore allowing for
feature elimination.REGULARISATION: RIDGE
1
2 2
×   𝑦 − 𝑦𝑝𝑟𝑒𝑑 + ⋋   ∅
2𝑚
m = number of observations
Y = observed output
Ypred = predicted output ∅ 𝑋 + ∅ 𝑋 + … + ∅ 𝑋
1 1 2 2 𝑛1
 is the regularisation parameter
For l2 / Ridge, as the penalisation increases, the coefficients approach but
do not equal zero, hence no variable is ever excludedLASSO VS RIDGE
https://www.r-bloggers.com/machine-learning-explained-regularization/LASSO VS RIDGE
Least angle and ℓ1 penalized regression: A review
Least angle and ℓ1 penalized regression: A review. Hesterberg et al. Statistics Surveys, 2008EMBEDDED METHODS: LASSO
By fitting a linear or logistic regression with a Lasso regularisation, we can then evaluate the coefficients of the different variables, and remove those variables which coefficients are zero.
----------------------------------------------------------------------------------------------------
# REGRESSION COEFFICIENTS
𝑦 = β + β 𝑋 + β 𝑋 + … + β 𝑋
0 1 1 1 1 𝑛 𝑛
The coefficients of the predictors (the bs) are directly proportional to how
much that feature contributes to the final value of y
Coefficients in linear models depend on a few assumptionsREGRESSION COEFFICIENTS
* Linear relationship between predictor (X) and outcome (Y)
* Xs are independent
* Xs are not correlated to each other (no-multicollinearity)
* Xs are normally distributed
In addition, for direct coefficient comparison Xs should be in the same scaleCHANGE IN COEFFICIENTS WITH REGULARISATION
Least angle and ℓ1 penalized regression: A review
Least angle and ℓ1 penalized regression: A review. Hesterberg et al. Statistics Surveys, 2008
----------------------------------------------------------------------------------------------------
# EMBEDDED METHODS
# FEATURE SELECTION BY TREE DERIVED VARIABLE IMPORTANCEDECISION TREES
# Decision trees
* Most popular machine learning algorithms
* Highly accurate
* Good generalisation (low overfitting)
* InterpretabilityDECISION TREE IMPORTANCE
Highest impurity (all classes are Age mixed) >30?
Impurity decreases as age is Gender  Fare > an important factor in the = male? 100?
decision
Impurity decreases
0.2 0.6 0.6 0.8
furtherRANDOM FOREST IMPORTANCE
* Random Forests consist of several hundreds of individual decision trees
* The impurity decrease for each feature is averaged across trees
Limitations
* Correlated features show equal or similar importance
* Correlated features importance is lower than the real importance, determined when tree is built in absence of correlated counterparts
* Highly cardinal variables show greater importance (trees are biased to this type of variables)RANDOM FOREST IMPORTANCE
* Build a random forest
* Determine feature importance
* Select the features with highest importance
* There is a scikit-learn implementation for thisRANDOM FOREST IMPORTANCE
Recursive feature elimination
* Build random forests
* Calculate feature importance
* Remove least important feature
* Repeat till a condition is met
* If the feature removed is correlated to another feature in the dataset, by removing the correlated feature, the true importance of the other feature will be revealed
* its importance will increase
GRADIENT BOOSTED TREES FEATURE IMPORTANCE
* Feature importance calculated in the same way
* Biased to highly cardinal features
* Importance is susceptible to correlated features
* Interpretability of feature importance is not so straightforward:
* Later trees fit to the errors of the first trees, therefore feature importance is not necessarily proportional on the influence of the feature on the outcome, rather on the mistakes of the previous trees.
* Averaging across trees may not add much information on true relation between feature and target
----------------------------------------------------------------------------------------------------


### resources
- https://github.com/krishnaik06/5-Days-Live-EDA-and-Feature-Engineering
- https://towardsdatascience.com/6-types-of-feature-importance-any-data-scientist-should-master-1bfd566f21c9
- https://towardsdatascience.com/non-linear-correlation-matrix-the-much-needed-technique-which-nobody-talks-about-132bc02ce632
- https://github.com/ashishpatel26/Amazing-Feature-Engineering
