- Bias is like a neuron's if statement - it allows for a neuron to stay inactive (return an output of zero) unless the input is strong (high) enough. 
- Formally we'd say that bias allows to regulate neuron's activation threshold.
 
 
### relu 
- https://twitter.com/Sumanth_077/status/1690736726288027648
 
 ### resources
- https://machinelearningmastery.com/rectified-linear-activation-function-for-deep-learning-neural-networks
- https://threadreaderapp.com/thread/1643963032207495169.html
- https://www.johndcook.com/blog/2023/07/01/activation-functions/
