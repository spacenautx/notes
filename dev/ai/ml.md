- https://github.com/lukas/ml-class
- https://github.com/stas00/ml-engineering
- https://github.com/ashishpatel26/500-AI-Machine-learning-Deep-learning-Computer-vision-NLP-Projects-with-code
- https://github.com/eriklindernoren/ML-From-Scratch
- https://github.com/eugeneyan/applied-ml
- https://github.com/lazyprogrammer/machine_learning_examples

### projects
- https://github.com/NirantK/awesome-project-ideas
 
10 machine learning techniques for trading:

- Natural Language Processing
- Support Vector Machines
- Reinforcement Learning
- Clustering Algorithms
- Deep Learning (RNNs)
- Anomaly Detection
- Gradient Boosting
- Neural Networks
- Random Forests
- Decision Trees
