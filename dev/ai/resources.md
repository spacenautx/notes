### resources
- https://github.com/alexeygrigorev/data-science-interviews
- https://github.com/tatsu-lab/stanford_alpaca
- https://crfm.stanford.edu/2023/03/13/alpaca.html
- https://github.com/merveenoyan/my_notes
- https://horace.io/brrr_intro.html
- https://github.com/dair-ai/ML-Papers-Explained

### learn
- https://madewithml.com/courses/mlops/
- https://stanford-cs329s.github.io/2021/index.html#overview
- https://github.com/DataTalksClub/mlops-zoomcamp
- https://github.com/jiep/offensive-ai-compilation
- https://atcold.github.io/pytorch-Deep-Learning/
- https://fullstackdeeplearning.com/spring2021/
- https://github.com/towardsai/tutorials
- https://github.com/patchy631/machine-learning

### data poisoning
- https://github.com/penghui-yang/awesome-data-poisoning-and-backdoor-attacks
