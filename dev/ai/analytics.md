Foundations of Data Science
by Google

Data applications and workflow
PACE (Plan, Analyze, Construct, Execute) 
project workflow and how to organize a data project. 

1. Speak the language of your audience
3. Be the connection to the data
4. Let your visualizations help tell the story 
5. Build positive professional relationships
6. Identify assumptions about the data
7. Identify limitations in the data 


Reading: Elements of successful communication

=============================================================================
Go Beyond the Numbers: Translate Data into Insights
by Google

- Use Python tools to examine raw data structure and format
- Select relevant Python libraries to clean raw data
- Demonstrate how to transform categorical data into numerical data with Python
- Utilize input validation skills to validate a dataset with Python
- Identify techniques for creating accessible data visualizations with Tableau
- Determine decisions about missing data and outliers 
- Structure and organize data by manipulating date strings
 
| 1. Discovering | You check out the overall shape, size, and content of the dataset. You find it is short on data.        |
| 2. Joining     | You add more data.                                                                                      |
| 3. Validating  | You perform a quick check that the new data doesn't have mistakes or misspellings.                      |
| 4. Structuring | You structure the data in different time periods and segments to understand trends.                     |
| 5. Validating  | You do another quick check to ensure the new columns you've made in structuring are correctly designed. |
| 6. Cleaning    | You check for outliers, missing data, and needs for conversions or transformations,                     |
| 7. Validating  | After cleaning, you double check the changes you made are correct and accurate,                         |
| 8. Presenting  | You share your dataset with a peer.                                                                     |
 

=============================================================================
The Power of Statistics
by Google

Concepts such as descriptive and inferential statistics, probability, sampling, confidence intervals, and hypothesis testing, Python for statistical analysis and practice communicating your findings 


- Describe the use of statistics in data science 
- Use descriptive statistics to summarize and explore data
- Calculate probability using basic rules
- Model data with probability distributions
- Describe the applications of different sampling methods 
- Calculate sampling distributions 
- Construct and interpret confidence intervals
- Conduct hypothesis tests

Introduction to statistics
identify the difference between descriptive and inferential statistics. 
Video: Statistics in action: A/B testing
Video: Descriptive statistics versus inferential statistics
Discussion Prompt: Share your initial thoughts on statistics

Measures of central tendency: The mean, the median, and the mode
Measures of central tendency such as the mean, median, and mode let you describe the center of the dataset using a single value. knowing the center of dataset helps you quickly understand its basic structure and determine the next steps in the analysis. 

Measures of dispersion: Range, variance, and standard deviation
Use standard deviation to measure variation in many types of data like ad revenues, stock prices, employee salaries, and more. 
Measures of dispersion like the standard deviation, variance, and range let you quickly identify the variation in your data values, and get a better understanding of the basic structure of your data. 

Measures of position: Percentiles and quartiles
Five number summary 
The minimum
The first quartile (Q1)
The median, or second quartile (Q2)
The third quartile (Q3)
The maximum 


-------------------------------------------------------------------------------------
Module 2
# Probability

# Foundational concepts: Random experiment, outcome, event 

Probability for single events. 
For example, if you’re dealing with two events, you can label one event A and the other event B. 
The probability of event A is written as P(A). 
The probability of event B is written as P(B).
For any event A, 0 ≤ P(A) ≤ 1. In other words, the probability of any event A is always between 0 and 1. 
If P(A) > P(B), then event A has a higher chance of occurring than event B.
If P(A) = P(B), then event A and event B are equally likely to occur.

# Mutually exclusive events 
Two events are mutually exclusive if they cannot occur at the same time. 
For example, you can’t be on the Earth and on the moon at the same time, or be sitting down and standing up at the same time. 
Or, take two classic examples of probability theory. If you toss a coin, you cannot get heads and tails at the same time. If you roll a die, you cannot get a 2 and a 4 at the same time. 

# Independent events 
Two events are independent if the occurrence of one event does not change the probability of the other event. This means that one event does not affect the outcome of the other event. 
For example, watching a movie in the morning does not affect the weather in the afternoon. Listening to music on the radio does not affect the delivery of your new refrigerator. These events are separate and independent. 
Or, take two consecutive coin tosses or two consecutive die rolls. Getting heads on the first toss does not affect the outcome of the second toss. For any given coin toss, the probability of any outcome is always 1 out of 2, or 50%. Getting a 2 on the first roll does not affect the outcome of the second roll. For any given die roll, the probability of any outcome is always 1 out of 6, or 16.7%.

# Conditional probability

# Dependent events  
Two events are dependent if the occurrence of one event changes the probability of the other event. This means that the first event affects the outcome of the second event. 
For instance, if you want to get a good grade on an exam, you first need to study the course material. Getting a good grade depends on studying. If you want to eat at a popular restaurant without waiting for a table, you have to arrive early. Avoiding a wait depends on arriving early.  In each instance, you can say that the second event is dependent on, or conditional on, the first event. 


Bayes’ theorem to describe more complex events. 

Posterior and prior probability 
In Bayesian statistics, prior probability refers to the probability of an event before new data is collected. Posterior probability is the updated probability of an event based on new data. 

Bayes’s theorem lets you calculate posterior probability by updating the prior probability based on your data. 

For example, let’s say a medical condition is related to age. You can use Bayes’s theorem to more accurately determine the probability that a person has the condition based on age. The prior probability would be the probability of a person having the condition. The posterior, or updated, probability would be the probability of a person having the condition if they are in a certain age group.

The theorem
Let’s examine the theorem itself.

Bayes’s theorem states that for any two events A and B, the probability of A given B equals the probability of A multiplied by the probability of B given A divided by the probability of B. 

In the theorem, prior probability is the probability of event A. Posterior probability, or what you’re trying to calculate, is the probability of event A given event B. 

* P(A): Prior probability

* P(A|B): Posterior probability 

Sometimes, statisticians and data professionals use the term “likelihood” to refer to the probability of event B given event A, and the term “evidence” to refer to the probability of event B. 

* P(B|A): Likelihood

* P(B): Evidence 

Using these terms, you can restate Bayes’s theorem as:

* Posterior = Likelihood * Prior / Evidence
 
Bayes's theorem is the foundation for the field of Bayesian statistics, also known as Bayesian inference, which is a powerful method for analyzing and interpreting data in modern data analytics.


# Discrete probability distributions
binomial, Poisson, and normal distribution can help better understand the structure of data.

# Uniform 
The uniform distribution describes events whose outcomes are all equally likely, or have equal probability. 
For example, rolling a die can result in six outcomes: 1, 2, 3, 4, 5, or 6. The probability of each outcome is the same: 1 out of 6, or about 16.7%. 

# Binomial
The binomial distribution models the probability of events with only two possible outcomes: success or failure. These outcomes are mutually exclusive and cannot occur at the same time. 
This definition assumes the following: 
Each event is independent, or does not affect the probability of the others.
Each event has the same probability of success. 

Remember that success and failure are labels used for convenience. For example, if you toss a coin, there are only two possible outcomes: heads or tails. You could choose to label either heads or tails as a successful outcome based on the needs of your analysis.

The binomial distribution represents a type of random event called a binomial experiment. A binomial experiment has the following attributes:
* The experiment consists of a number of repeated trials.
* Each trial has only two possible outcomes.
* The probability of success is the same for each trial.
* And, each trial is independent.

An example of a binomial experiment is tossing a coin 10 times in a row. This is a binomial experiment because it has the following features:
* The experiment consists of 10 repeated trials, or coin tosses.
* Each trial has only two possible outcomes: heads or tails.
* Each trial has the same probability of success. If you define success as heads, then the probability of success for each toss is the same: 50%.
* Each trial is independent. The outcome of one coin toss does not affect the outcome of any other coin toss.

Binomial distribution can be used to model the probability that:
* A new medication generates side effects
* A credit card transaction is fraudulent 
* A stock price rises in value 

In machine learning, the binomial distribution is often used to classify data. 
For example, train an algorithm to recognize whether a digital image is or is not a specific type of animal, like a cat or a dog. 


Bernoulli 
The Bernoulli distribution is similar to the binomial distribution as it also models events that have only two possible outcomes (success or failure). The only difference is that the Bernoulli distribution refers to only a single trial of an experiment, while the binomial refers to repeated trials. A classic example of a Bernoulli trial is a single coin toss. 

Poisson 
The Poisson distribution models the probability that a certain number of events will occur during a specific time period. 
Examples, such as a distance, area, or volume. In this course, we focus on time. 
The Poisson distribution represents a type of random experiment called a Poisson experiment. 

* The number of events in the experiment can be counted.
* The mean number of events that occur during a specific time period is known.
* Each event is independent. 

For example, imagine you have an online website where you post content. Your website averages two views per hour. You want to determine the probability that your website will receive a certain number of views in a given hour.

This is a Poisson experiment because:
* The number of events in the experiment can be counted. You can count the number of views.
* The mean number of events that occur during a specific time period is known. There is an average of two views per hour.
* Each outcome is independent. The probability of one person viewing your website does not affect the probability of another person viewing your website.

Use the Poisson distribution to model data such as the number of: 
* Calls per hour for a customer service call center
* Customers per day at a shop 
* Thunderstorms per month in a city
* Financial transactions per second at a bank

# Probability Density and Probability 
A probability function is a mathematical function that provides probabilities for the possible outcomes of a random variable. 
There are two types of probability functions: 
* Probability Mass Functions (PMFs) represent discrete random variables
* Probability Density Functions (PDFs) represent continuous random variables      


# Continuous probability distributions
Objective versus subjective probability
Video: The principles of probability
Reading: Fundamental concepts of probability
Video: The basic rules of probability and events
Reading: The probability of multiple events



# The normal distribution
All normal distributions have the following features: 

* The shape is a bell curve
* The mean is located at the center of the curve
* The curve is symmetrical on both sides of the mean
* The total area under the curve equals 1

Reading: Model data with the normal distribution
Video: Standardize data using z-scores


-------------------------------------------------------------------------------------
Module 3
# Sampling


# Introduction to sampling
The relationship between sample and population
 
A population might be the set of:  
* All students at a university
* All the cell phones ever manufactured by a company
* All the forests on Earth 

Samples drawn from the above populations might be: 
* The math majors at the university
* The cell phones manufactured by the company in the last week
* The forests in Canada


# The sampling process
The stages of the sampling process
1. Identify the target population
  - The target population is the complete set of elements that you’re interested in knowing more about
2. Select the sampling frame
  - Generally your sampling frame is the accessible part of your target population, but sometimes it will include elements apart from this set. 
3. Choose the sampling method
  - probability sampling and non-probability sampling.  
4. Determine the sample size 
  - Sample size helps determine the precision of the predictions 
5. Collect the sample data
  -  Collect your sample data, which is the final step in the sampling process. 


# The central limit theorem - Infer population parameters with the central limit theorem
The central limit theorem states that the sampling distribution of the mean approaches a normal distribution as the sample size increases. In other words, as your sample size increases, your sampling distribution assumes the shape of a bell curve.

# Conditions
In order to apply the central limit theorem, the following conditions must be met:

Randomization: Your sample data must be the result of random selection. Random selection means that every member in the population has an equal chance of being chosen for the sample.

Independence: Your sample values must be independent of each other. Independence means that the value of one observation does not affect the value of another observation. Typically, if you know that the individuals or items in your dataset were selected randomly, you can also assume independence.

10%: To help ensure that the condition of independence is met, your sample size should be no larger than 10% of the total population when the sample is drawn without replacement (which is usually the case). 

Note: In general, you can sample with or without replacement. When a population element can be selected only one time, you are sampling without replacement. When a population element can be selected more than one time, you are sampling with replacement. You’ll learn more about this topic later on in the course. 

Sample size: The sample size needs to be sufficiently large.

There is no exact rule for how large a sample size needs to be in order for the central limit theorem to apply. The answer depends on the following factors: 

  - Requirements for precision. The larger the sample size, the more closely your sampling distribution will resemble a normal distribution, and the more precise your estimate of the population mean will be. 
  - The shape of the population. If your population distribution is roughly bell-shaped and already resembles a normal distribution, the sampling distribution of the sample mean will be close to a normal distribution even with a small sample size. 

In general, many statisticians and data professionals consider a sample size of 30 to be sufficient when the population distribution is roughly bell-shaped, or approximately normal. However,  if the original population is not normal—for example, if it’s extremely skewed or has lots of outliers—data professionals often prefer the sample size to be a bit larger. Exploratory data analysis can help you determine how large of a sample is necessary for a given dataset. 


The sampling distribution of the proportion
* Use sampling distributions to estimate population proportions. In statistics, a population proportion refers to the percentage of individuals or elements in a population that share a certain characteristic. Proportions measure percentages or parts of a whole.

The sampling distribution of the mean
* A sampling distribution is a probability distribution of a sample statistic.


-------------------------------------------------------------------------------------
Module 4
# Confidence intervals

Use confidence intervals to describe the uncertainty of their estimates. 
Construct and interpret confidence intervals — and how to avoid some common misinterpretations.

# Correct interpretation  
Example: mean weight 
Let’s explore an example to get a better understanding of how to interpret a confidence interval. Imagine you want to estimate the mean weight of a population of 10,000 penguins. Instead of weighing every single penguin, you select a sample of 100 penguins. The mean weight of your sample is 30 pounds. Based on your sample data, you construct a 95% confidence interval between 28 pounds and 32 pounds. 

95 CI [28, 32]

Interpret the confidence interval
Earlier, you learned that the confidence level expresses the uncertainty of the estimation process. Let’s discuss what 95% confidence means from a more technical perspective.

Technically, 95% confidence means that if you take repeated random samples from a population, and construct a confidence interval for each sample using the same method, you can expect that 95% of these intervals will capture the population mean. You can also expect that 5% of the total will not capture the population mean. 

The confidence level refers to the long-term success rate of the method, or the estimation process based on random sampling. 

For the purpose of our example, let’s imagine that the mean weight of all 10,000 penguins is 31 pounds, although you wouldn’t know this unless you actually weighed every penguin. So, you take a sample of the population.

Imagine you take 20 random samples of 100 penguins each from the penguin population, and calculate a 95% confidence interval for each sample. You can expect that approximately 19 of the 20 intervals, or 95% of the total, will contain the actual population mean weight of 31 pounds. One such interval will be the range of values between 28 pounds and 32 pounds. 

Diagram has 20 vertical bars that represent confidence intervals. 19 intervals include the population mean, 1 does not.
In practice, data professionals usually select one random sample and generate one confidence interval, which may or may not contain the actual population mean. This is because repeated random sampling is often difficult, expensive, and time-consuming. Confidence intervals give data professionals a way to quantify the uncertainty due to random sampling.

# Incorrect interpretations
Now that you have a better understanding of how to properly interpret a confidence interval, let’s review some common misinterpretations and how to avoid them. 

# Misinterpretation 1: 
95% refers to the probability that the population mean falls within the constructed interval 
One incorrect statement that is often made about a confidence interval at a 95% level of confidence is that there is a 95% probability that the population mean falls within the constructed interval. 

In our example, this would mean that there’s a 95% chance that the mean weight of the penguin population falls in the interval between 28 pounds and 32 pounds. 

This is incorrect. The population mean is a constant. 

Like any population parameter, the population mean is a constant, not a random variable. While the value of the sample mean varies from sample to sample, the value of the population mean does not change. The probability that a constant falls within any given range of values is always 0% or 100%.  It either falls within the range of values, or it doesn’t. 

For example, any given random sample of 100 penguins may have a different mean weight: 32.8 pounds, 27.3 pounds, 29.6 pounds, and so on. You can use a sampling distribution to assign a specific probability to each of your sample means because these are random variables. However, the population mean weight is considered a constant. In our example, if you weigh all 10,000 penguins, you’ll find that the population mean is 31 pounds. This value is fixed, and does not vary from sample to sample.

| Sample Mean (100 penguins) | Population Mean (10,000 penguins) |
| 32.8 lbs                   | 31 lbs                            |
| 27.3 lbs                   | 31 lbs                            |
| 29.6 lbs                   | 31 lbs                            |

So, it’s not strictly correct to say there is a 95% chance that your confidence interval captures the population mean because this implies that the population mean is variable. Intervals change from sample to sample, but the value of the population mean you’re trying to capture does not.

What you can say is that if you take repeated random samples from the population, and construct a confidence interval for each sample using the same method, you can expect 95% of your intervals to capture the population mean. 

Pro tip: Remember that a 95% confidence level refers to the success rate of the estimation process. 

# Misinterpretation 2: 
95% refers to the percentage of data values that fall within the interval 
Another common mistake is to interpret a 95% confidence interval as saying that 95% of all of the data values in the population fall within the interval. This is not necessarily true. A 95% confidence interval shows a range of values that likely includes the actual population mean. This is not the same as a range that contains 95% of the data values in the population. 

For example, your 95% confidence interval for the mean penguin weight is between 28 pounds and 32 pounds. It may not be accurate to say that 95% of all weight values fall within this interval. It’s possible that over 5% of the penguin weights in the population are outside this interval—either less than 28 pounds or greater than 32 pounds. 

Table displays weight values for 8 penguins. 4 weights fall outside the confidence interval between 28 and 32 pounds.

# Misinterpretation 3: 
95% refers to the percentage of sample means that fall within the interval 
A third common misinterpretation is that a 95% confidence interval implies that 95% of all possible sample means fall within the range of the interval. This is not necessarily true. For example, your 95% confidence interval for mean penguin weight is between 28 pounds and 32 pounds. Imagine you take repeated samples of 100 penguins and calculate the mean weight for each sample. It’s possible that over 5% of your sample means will be less than 28 pounds or greater than 32 pounds.

3 small circles and 1 large circle represent 3 samples of 100 penguins drawn from a population of 10,000. 

Key takeaways
Knowing how to correctly interpret confidence intervals will give you a better understanding of your estimate, and help you share useful and accurate information with stakeholders. You may need to explain the common misinterpretations too, and why they're incorrect. You don't want your stakeholders to base their decisions on a misinterpretation. Understanding how to effectively communicate your results to stakeholders is a key part of your job as a data professional.  



# Construct a confidence interval 

# Large sample: Z-scores
For large sample sizes, you use z-scores to calculate the margin of error, just like you did earlier to estimate mean battery life for cell phones. This is because of the central limit theorem: for large sample sizes, the sample mean is approximately normally distributed. For a standard normal distribution, also called a z-distribution, you use z-scores to make calculations about your data. 

# Small sample: T-scores 
For small sample sizes, you need to use a different distribution, called the t-distribution. Statistically speaking, this is because there is more uncertainty involved in estimating the standard error for small sample sizes. You don't need to worry about the technical details, which are beyond the scope of this course. For now, just know that if you're working with a small sample size, and your data is approximately normally distributed, you should use the t-distribution rather than the standard normal distribution. For a t-distribution, you use t-scores to make calculations about your data. 


-------------------------------------------------------------------------------------
Module 5
# Introduction to hypothesis testing

Determine if the results of a test or experiment are statistically significant or due to chance. 
The basic steps for any hypothesis test 

* Statistical hypotheses
1. State the null hypothesis and the alternative hypothesis.
  - In statistics, the null hypothesis is often abbreviated as H sub zero (H0). 
  - When written in mathematical terms, the null hypothesis always includes an equality symbol (usually =, but sometimes ≤ or ≥). 
  - Null hypotheses often include phrases such as “no effect,” “no difference,” “no relationship,” or “no change.”

2. Choose a significance level.
  - The significance level, or alpha (α), is the threshold at which you will consider a result statistically significant. The significance level is also the probability of rejecting the null hypothesis when it is true.
  - Typically, data professionals set the significance level at 0.05, or 5%. That means results at least as extreme as yours only have a 5% chance (or less) of occurring when the null hypothesis is true. 
  - Note: 5% is a conventional choice, and not a magical number. It’s based on tradition in statistical research and education. Other common choices are 1% and 10%. You can adjust the significance level to meet the specific requirements of your analysis. A lower significance level means an effect has to be larger to be considered statistically significant.
  - Pro tip: As a best practice, you should set a significance level before you begin your test. Otherwise, you might end up in a situation where you are manipulating the results to suit your convenience.

3. Find the p-value.
P-value refers to the probability of observing results as or more extreme than those observed when the null hypothesis is true. 
Your p-value helps you determine whether a result is statistically significant. A low p-value indicates high statistical significance, while a high p-value indicates low or no statistical significance.
Every hypothesis test features:
  - A test statistic that indicates how closely your data match the null hypothesis. For a z-test, your test statistic is a z-score; for a t-test, it’s a t-score. 
  - A corresponding p-value that tells you the probability of obtaining a result at least as extreme as the observed result if the null hypothesis is true.
  - As a data professional, you’ll almost always calculate p-value on your computer, using a programming language like Python or other statistical software. In this example, you’re conducting a z-test, so your test statistic is a z-score of 2.53. Based on this test statistic, you calculate a p-value of 0.0057, or 0.57%. 


5. Reject or fail to reject the null hypothesis.
In a hypothesis test, you compare your p-value to your significance level to decide whether your results are statistically significant. 
There are two main rules for drawing a conclusion about a hypothesis test: 
  - If your p-value is less than your significance level, you reject the null hypothesis.
  - If your p-value is greater than your significance level, you fail to reject the null hypothesis.  


* Alternative hypothesis 
  1. The alternative hypothesis has the following characteristics:
  2. In statistics, the alternative hypothesis is often abbreviated as H sub a (Ha). 
  3. When written in mathematical terms, the alternative hypothesis always includes an inequality symbol (usually ≠, but sometimes < or >). 
  4. Alternative hypotheses often include phrases such as “an effect,” “a difference,” “a relationship,” or “a change.”


How hypothesis testing can help you draw meaningful conclusions about data.
Understand the concept of statistical significance to effectively conduct a hypothesis test and interpret the results. Insights based on statistically significant results can help stakeholders make more informed business decisions. 


# Type I and type II errors
Errors in statistical decision-making
When you decide to reject or fail to reject the null hypothesis, there are four possible outcomes–two represent correct choices, and two represent errors. You can: 

1. Reject the null hypothesis when it’s actually true (Type I error)
2. Reject the null hypothesis when it’s actually false (Correct)
3. Fail to reject the null hypothesis when it’s actually true (Correct) 
4. Fail to reject the null hypothesis when it’s actually false (Type II error)

* One-sample test for means
A one-sample test determines whether or not a population parameter, like a mean or proportion, is equal to a specific value. 

* Two-sample tests: Means
A two-sample test determines whether or not two population parameters, such as two means or two proportions, are equal to each other. 

# One-tailed 
- A one-tailed test results when the alternative hypothesis states that the actual value of a population parameter is either less than or greater than the value in the null hypothesis. 
- A one-tailed test may be either left-tailed or right-tailed. A left-tailed test results when the alternative hypothesis states that the actual value of the parameter is less than the value in the null hypothesis. A right-tailed test results when the alternative hypothesis states that the actual value of the parameter is greater than the value in the null hypothesis. 

# Two-tailed tests
- A two-tailed test results when the alternative hypothesis states that the actual value of the parameter does not equal the value in the null hypothesis.
- For example, imagine a test in which the null hypothesis states that the mean weight of a penguin population equals 30 lbs. 
- In a left-tailed test, the alternative hypothesis might state that the mean weight of the penguin population is less than ("<") 30 lbs. 
- In a right-tailed test, the alternative hypothesis might state that the mean weight of the penguin population is greater than (">") 30 lbs. 
- In a two-tailed test, the alternative hypothesis might state that the mean weight of the penguin population is not equal (“≠”) to 30 lbs.

Two-sample tests: Proportions

# A/B testing
- Average revenue per user: How much revenue does a user generate for a website?
- Average session duration: How long does a user remain on a website?
- Click rate: If a user is shown an ad, does the user click on it?
- Conversion rate: If a user is shown an ad, will that user convert into a customer?


Experimental Design
xperimental design refers to planning an experiment in order to collect data to answer your research question. 

Researchers conduct experiments in many fields: medicine, physics, psychology, manufacturing, marketing, and more. The typical purpose of an experiment is to discover a cause-and-effect relationship between variables. For example, a data professional might design an experiment to discover whether:
- A new medicine leads to faster recovery time 
- A new website design increases product sales
- A new fertilizer increases crop growth 
- A new training program improves athletic performance


There are at least three key steps in designing an experiment: 
- Define your variables 
1. The independent variable refers to the cause you’re interested in investigating. A researcher changes or controls the independent variable to determine how it affects the dependent variable. “Independent” means it’s not influenced by other variables in the experiment.
2. The dependent variable refers to the effect you’re interested in measuring. “Dependent” means its value is influenced by the independent variable. 
- Formulate your hypothesis 
1. hypothesis states the relationship between your independent and dependent variables and predicts the outcome of your experiment. 
- Assign test subjects to treatment and control groups 


=============================================================================
Regression Analysis: Simplify Complex Data Relationships
by Google

Use regression analysis to discover the relationships between different variables in a dataset
Identify key factors that affect business performance. 
You'll learn about different methods of data modeling and how to use them to approach business problems. 
You’ll also explore methods such as linear regression, analysis of variance (ANOVA), and logistic regression.  

- Explore the use of predictive models to describe variable relationships, with an emphasis on correlation
- Determine how multiple regression builds upon simple linear regression at every step of the modeling process
- Run and interpret one-way and two-way ANOVA tests
- Construct different types of logistic regressions including binomial, multinomial, ordinal, and Poisson log-linear regression models

Reading: Helpful resources and tips
Video: Tiffany: Gain actionable insights with regression models
Video: PACE in regression analysis

Video: Mathematical linear regression
Video: Introduction to logistic regression

-------------------------------------------------------------------------------------
Module 2
Simple linear regression

Ordinary least squares estimation
Explore ordinary least squares

Correlation and the intuition behind simple linear regression
Correlation is a measurement of the way two variables move together. If there is a strong correlation between the variables, then knowing one will be very helpful to predict the other. However, if there is a weak correlation between two variables, then knowing the value of one will not tell you much about the value of the other. In the context of linear regression, correlation refers to linear correlation: as one variable changes, so does the other at a constant rate.

continuous variable can be summarized using some basic numbers. Two of these summary statistics are:
  - Average: A measurement of central tendency (mean, median, or mode)
  - Standard deviation: A measurement of spread


Make linear regression assumptions
https://www.coursera.org/learn/regression-analysis-simplify-complex-data-relationships/supplement/RPXgA/correlation-and-the-intuition-behind-simple-linear-regression

# The four main assumptions of simple linear regression
1. Linearity: Each predictor variable (Xi) is linearly related to the outcome variable (Y).
- In order to assess whether or not there is a linear relationship between the independent and dependent variables, it is easiest to create a scatterplot of the dataset. The independent variable would be on the x-axis, and the dependent variable would be on the y-axis. There are a number of different Python functions that you can use to read in the data and to create a scatterplot. Some packages used for data visualizations include Matplotlib, seaborn, and Plotly. Testing the linearity assumption should occur before the model is built.
2. Normality: The errors are normally distributed.*
- The normality assumption focuses on the errors, which can be estimated by the residuals, or the difference between the observed values in the data and the values predicted by the regression model. For that reason, the normality assumption can only be confirmed after a model is built and predicted values are calculated. Once the model has been built, you can either create a QQ-plot to check that the residuals are normally distributed, or create a histogram of the residuals. Whether the assumption is met is up to some level of interpretation.
3. Independent Observations: Each observation in the dataset is independent.
- Like the normality assumption, the homoscedasticity assumption concerns the residuals of a model, so it can only be evaluated after a regression model has already been constructed. A scatterplot of the fitted values (i.e., the model’s predicted Y values) versus the residuals can help determine whether the homoscedasticity assumption is violated.
4. Homoscedasticity: The variance of the errors is constant or similar across the model.*
- Define a different outcome variable.
If you are interested in understanding how a city’s population correlates with the number of restaurants in a city, you know that some cities are much more populous than others. You can then redefine the outcome variable as the ratio of population to restaurants.
- Transform the Y variable.
As with the above assumptions, sometimes taking the logarithm or transforming the Y variable in another way can potentially fix inconsistencies with the homoscedasticity assumption.


* "errors" and "residuals" in connection with regression. You may see this in other online resources and materials throughout your time as a data professional. In actuality, there is a difference:
* Residuals are the difference between the predicted and observed values. You can calculate residuals after you build a regression model by subtracting the predicted values from the observed values.
* Errors are the natural noise assumed to be in the model.
* Residuals are used to estimate errors when checking the normality and homoscedasticity assumptions of linear regression.


Evaluate uncertainty in regression analysis
Interpret measures of uncertainty in regression
- Regression analysis utilizes estimation techniques, so there is always uncertainty around the predictions.
- We can measure uncertainty using confidence intervals, p-values, and confidence bands.
- For every coefficient estimate, we are testing the hypothesis that the coefficient equals 0.
 
Video: Model evaluation metrics
Reading: Evaluation metrics for simple linear regression
Discussion Prompt: Report regression results
Video: Interpret and present linear regression results
Reading: Correlation versus causation: Interpret regression results
Ungraded Lab: Exemplar: Evaluate simple linear regression


-------------------------------------------------------------------------------------
Module 3
Multiple linear regression
key topics in machine learning: selection, overfitting, and the bias-variance tradeoff.

Reading: Multiple linear regression scenarios
Video: Represent categorical variables
Video: Make assumptions with multiple linear regressions
Reading: Multiple linear regression assumptions and multicollinearity
Ungraded Plugin: Identify: Multiple regression assumptions

Video: Interpret multiple regression coefficients

Video: The problem with overfitting
Reading: Underfitting and overfitting
Video: Top variable selection methods
Video: Regularization: Lasso, Ridge, and Elastic Net regression
Discussion Prompt: Multicollinearity and model minimalism
Ungraded Lab: Activity: Perform multiple linear regression


-------------------------------------------------------------------------------------
Module 4
Advanced hypothesis testing

Conduct two kinds of Chi-squared tests, as well as one-way and two-way ANOVA tests.

Video: Hypothesis testing with chi-squared
Reading: Chi-squared tests: Goodness of fit versus independence

Video: Introduction to the analysis of variance
Reading: More about ANOVA
Ungraded Lab: Annotated follow-along guide: Explore one-way vs. two-way ANOVA tests with Python
Video: Explore one-way vs. two-way ANOVA tests with Python
Video: ANOVA post hoc tests with Python
Video: Ignacio: Discovery at every stage of your career
Ungraded Lab: Exemplar: Hypothesis testing with Python

Video: ANCOVA: Analysis of covariance
Video: More dependent variables: MANOVA and MANCOVA


-------------------------------------------------------------------------------------
Module 5
Logistic regression

Binomial logistic regression, a type of regression analysis that classifies data into two categories. 
Build a binomial logistic regression model and how data professionals use this type of model to gain insights from their data.

Video: Find the best logistic regression model for your data
Video: Construct a logistic regression model with Python
Video: Evaluate a binomial logistic regression model
Video: Key metrics to assess logistic regression results
Reading: Common logistic regression metrics in Python
Video: Interpret the results of a logistic regression
Reading: Interpret logistic regression models
Ungraded Lab: Exemplar: Perform logistic regression
Video: Answer questions with regression models
Reading: Prediction with different types of regression


-------------------------------------------------------------------------------------
Module 6
Course 5 end-of-course project

Video: Leah: Strategies for sharing models and modeling techniques


=============================================================================
The Nuts and Bolts of Machine Learning
by Google


machine learning uses algorithms and statistics to teach computer systems to discover patterns in data.
use machine learning to help analyze data, solve complex problems, and make accurate predictions. 
two main types of machine learning: supervised and unsupervised.
specific models such as Naive Bayes, decision tree, random forest, and more.  

-Apply feature engineering techniques using Python
-Construct a Naive Bayes model
-Describe how unsupervised learning differs from supervised learning
-Code a K-means algorithm in Python 
-Evaluate and optimize the results of K-means model
-Explore decision tree models, how they work, and their advantages over other types of supervised machine learning
-Characterize bagging in machine learning, specifically for random forest models 
-Distinguish boosting in machine learning, specifically for XGBoost models 
-Explain tuning model parameters and how they affect performance and evaluation metrics

four main types of machine learning: supervised, unsupervised, reinforcement, and deep learning.

Reading: Helpful resources and tips
Video: Susheela: Delight people with data
Video: The main types of machine learning
Video: Determine when features are infinite
Video: Categorical features and classification models
Ungraded Plugin: Identify: Machine learning solutions
Video: Guide user interest with recommendation systems
Reading: Case study: The Woobles: The power of recommendation systems to drive sales

Video: Equity and fairness in machine learning
Video: Build ethical models

Reading: Reference guide: Python for machine learning
Video: More about Python packages
Ungraded Plugin: Categorize: Data science tools
Reading: Python libraries and packages

Video: Resources to answer programming questions
Reading: Find solutions online
Video: Your machine learning team
Video: Samantha: Connect to the data professional community


-------------------------------------------------------------------------------------
Module 2
Workflow for building complex models


Video: PACE in machine learning
Reading: More about planning a machine learning project
Video: Ganesh: Overcome challenges and learn from your mistakes
Video: Analyze data for a machine learning model
Video: Introduction to feature engineering
Reading: Explore feature engineering
Video: Solve issues that come with imbalanced datasets
Reading: More about imbalanced datasets
Ungraded Lab: Annotated follow-along guide: Feature engineering with Python
Video: Feature engineering and class balancing
Ungraded Lab: Exemplar: Perform feature engineering

Video: Introduction to Naive Bayes
Reading: Naive Bayes classifiers
Ungraded Lab: Annotated follow-along guide: Construct a Naive Bayes model with Python
Video: Construct a Naive Bayes model with Python
Video: Key evaluation metrics for classification models
Reading: More about evaluation metrics for classification models
Ungraded Lab: Exemplar: Build a Naive Bayes model


-------------------------------------------------------------------------------------
Module 3
Unsupervised learning techniques

supervised and unsupervised techniques and the benefits and uses of each approach. 
apply two unsupervised machine learning models: clustering and K-means.

Video: Introduction to K-means
Reading: More about K-means
Ungraded Lab: Annotated follow-along guide: Use K-means for color compression with Python
Video: Use K-means for color compression with Python
Reading: Clustering beyond K-means

Video: Key metrics for representing K-means clustering
Video: Inertia and silhouette coefficient metrics
Reading: More about inertia and silhouette coefficient metrics
Ungraded Lab: Annotated follow-along resource: Apply inertia and silhouette score with Python
Ungraded Lab: Exemplar: Build a K-means model


-------------------------------------------------------------------------------------
Module 4
Tree-based modeling

Video: Tree-based modeling
Ungraded Plugin: Identify: Parts of the decision tree
Reading: Explore decision trees
Video: Build a decision tree with Python

Video: Tune a decision tree
Reading: Hyperparameter tuning
Video: Verify performance using validation
Reading: More about validation and cross-validation
Ungraded Lab: Annotated follow-along guide: Tune and validate decision trees
Video: Tune and validate decision trees with Python
Ungraded Lab: Exemplar: Build a decision tree

Video: Bootstrap aggregation
Reading: Bagging: How it works and why to use it
Video: Explore a random forest
Reading: More about random forests
Video: Tuning a random forest
Ungraded Lab: Annotated follow-along guide: Build and cross-validate a random forest model
Video: Build and cross-validate a random forest model with Python
Video: Build and validate a random forest model using a validation data set
Reading: Reference guide: Random forest tuning
Reading: Reference guide: Validation and cross-validation
Ungraded Lab: Exemplar: Build a random forest model
Reading: Case Study: Machine learning model unearths resourcing insights for Booz Allen Hamilton

Video: Introduction to boosting: AdaBoost
Video: Gradient boosting machines
Reading: More about gradient boosting
Video: Tune a GBM model
Reading: Reference guide: XGBoost tuning
Ungraded Lab: Annotated follow-along guide: Build an XGBoost model with Python
Video: Build an XGBoost model with Python
Ungraded Lab: Exemplar: Build an XGBoost model


=============================================================================
Google Advanced Data Analytics Capstone
by Google


Capstone Project
Data-focused career resources


# tools
- https://github.com/Wilson-ZheLin/Streamline-Analyst/tree/main
