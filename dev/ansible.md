# ansible
----

## archiecture and design
### file module
- ansible all -m file -a 'path=/tmp/test state=touch'
- ansible all -m file -a 'path=/tmp/test_modules.txt state=touch mode=600'

### ansible colors
- red failure
- yellow succes, with changes
- green success, no changes

### idempotence

### copy module
- ansible all -m copy -a 'src=/tmp/x dest=/tmp/x'
- ansible all -m copy -a 'remote_src=yes src=/tmp/testing dest=/tmp/y'

### command module
- ansible all -a 'hostname' -o
- ansible all -m fetch -a 'src=/tmp/test_modules.txt dest=/tmp/'

### ansible-doc
 ansible-doc file

## playbooks
- https://docs.ansible.com/ansible/2.4/playbooks_keywords.html
### variables
- variables that will apply to the play
- ansible-playbook motd_playbook.yaml -e 'motd="Testing the motd playbook\n"'
- magic variables

### tasks
- delegation
- specific tasks for execution on specific targets

### handlers
- notify key from a task at the end

### facts
- filter sepcific facts
- creation of custom facts
    `sudo mkdir -p /etc/ansible/facts.d`
- execution of custom facts
    `ansible linux -m file -a 'path=/etc/ansible/facts.d/getdate1.fact state=absent'`
- custom facts in environments without super user access

### templating
- jinja2

### creating and executing
- configure out hosts to target the linux group

## executables
## inventories
### dynamic inventories
### register & when


## modules
- set_fact
- pause
- prompt
- wait_for
- assemble
- add_host
- group_by
- fetch


## custom module


## async
- polling
- async job identifiers
- status handling
- seriel execution
- batch execution
- alternative strategies to facilitate parallel execution

## plugins
- action
- cache
- callback
- connection
- strategy
- shell
- test
- vars
- lookup plugin
- filter plugin

### setup module
- ansible centos1 -m setup

## vault
- encrypt / decrypt variables and files
- re-encrypting data
- multiple vaults

## structuring
- includes
- imports
- tags

### roles
- role structure
- ansible-galaxy
- move existing project to a role
- role execution
- parameters
- dependencies

## versioning

## best practices

## cloud


## troubleshooting
- ssh
- syntax
- step
- start at
- log path
- verbosity

## resources
- https://github.com/warhorse/warhorse
- https://github.com/geerlingguy/ansible-for-devops
