# javascript

### example
- https://github.com/kormyen/memex

### learn
- https://github.blog/2018-08-09-create-a-13kb-javascript-game-in-30-days-with-js13kgames/
- https://github.com/microsoft/Web-Dev-For-Beginners
- https://github.com/verekia/js-stack-from-scratch
- https://github.com/MostlyAdequate/mostly-adequate-guide
- https://github.com/getify/Functional-Light-JS
- https://javascript30.com/
- https://betterprogramming.pub/10-javascript-promise-challenges-before-you-start-an-interview-c9af8d4144ec
- https://github.com/thejsway/thejsway
- https://github.com/js-org/js.org
- https://github.com/braziljs/js-the-right-way
- https://github.com/ryanmcdermott/clean-code-javascript
- https://github.com/getify/You-Dont-Know-JS
- https://github.com/h5bp/Front-end-Developer-Interview-Questions
- https://github.com/mbeaudru/modern-js-cheatsheet
- https://github.com/leonardomso/33-js-concepts
- https://github.com/azu/promises-book
- https://github.com/lydiahallie/javascript-questions/

### frameworks
-  https://github.com/justin-schroeder/arrow-js     

# DOM
- http://domenlightenment.com

## 33 concepts

1. **[Call Stack](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#1-call-stack)**
2. **[Primitive Types](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#2-primitive-types)**
3. **[Value Types and Reference Types](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#3-value-types-and-reference-types)**
4. **[Implicit, Explicit, Nominal, Structuring and Duck Typing](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#4-implicit-explicit-nominal-structuring-and-duck-typing)**
5. **[== vs === vs typeof](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#5--vs--vs-typeof)**
6. **[Function Scope, Block Scope and Lexical Scope](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#6-function-scope-block-scope-and-lexical-scope)**
7. **[Expression vs Statement](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#7-expression-vs-statement)**
8. **[IIFE, Modules and Namespaces](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#8-iife-modules-and-namespaces)**
9. **[Message Queue and Event Loop](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#9-message-queue-and-event-loop)**
10. **[setTimeout, setInterval and requestAnimationFrame](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#10-settimeout-setinterval-and-requestanimationframe)**
11. **[JavaScript Engines](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#11-javascript-engines)**
12. **[Bitwise Operators, Type Arrays and Array Buffers](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#12-bitwise-operators-type-arrays-and-array-buffers)**
13. **[DOM and Layout Trees](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#13-dom-and-layout-trees)**
14. **[Factories and Classes](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#14-factories-and-classes)**
15. **[this, call, apply and bind](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#15-this-call-apply-and-bind)**
16. **[new, Constructor, instanceof and Instances](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#16-new-constructor-instanceof-and-instances)**
17. **[Prototype Inheritance and Prototype Chain](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#17-prototype-inheritance-and-prototype-chain)**
18. **[Object.create and Object.assign](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#18-objectcreate-and-objectassign)**
19. **[map, reduce, filter](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#19-map-reduce-filter)**
20. **[Pure Functions, Side Effects, State Mutation and Event Propagation](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#20-pure-functions-side-effects-state-mutation-and-event-propagation)**
21. **[Closures](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#21-closures)**
22. **[High Order Functions](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#22-high-order-functions)**
23. **[Recursion](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#23-recursion)**
24. **[Collections and Generators](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#24-collections-and-generators)**
25. **[Promises](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#25-promises)**
26. **[async/await](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#26-asyncawait)**
27. **[Data Structures](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#27-data-structures)**
28. **[Expensive Operation and Big O Notation](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#28-expensive-operation-and-big-o-notation)**
29. **[Algorithms](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#29-algorithms)**
30. **[Inheritance, Polymorphism and Code Reuse](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#30-inheritance-polymorphism-and-code-reuse)**
31. **[Design Patterns](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#31-design-patterns)**
32. **[Partial Applications, Currying, Compose and Pipe](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#32-partial-applications-currying-compose-and-pipe)**
33. **[Clean Code](https://github.com/leonardomso/33-js-concepts/blob/master/README.md#33-clean-code)**

## snippets
------
#### Get value
- Given an object or array — the function will return the value at specified path, otherwise null.

```javascript
const getValue = (obj, path) => path
    .replace(/\[([^[\]]*)]/g, '.$1.')
    .split('.')
    .filter(prop => prop !== '')
    .reduce((prev, next) => (
        prev instanceof Object ? prev[next] : undefined
    ), obj);

getValue({ a: { b: c: 'd' } }, 'a.b.c'); // = d
getValue({ a: { b: c: [1, 2] } }, 'a.b.c[1]'); // = 2
```
------
#### Clamp
- Ensure a value is within a specified range, otherwise “clamp” to the closest of the minimum and maximum value.
```javascript
const clamp = (min, max, value) => {
  if (min > max) throw new Error('min cannot be greater than max');
  return value < min
    ? min
    : value > max
      ? max
      : value;
  }
}

clamp(0, 10, -5); // = 0
clamp(0, 10, 20); // = 10
```
------
#### Sleep
- Wait the specified duration in milliseconds before performing the next operation.
```javascript
const sleep = async (duration) => (
  new Promise(resolve =>
    setTimeout(resolve, duration)
  )
);

await sleep(1000); // waits 1 sec
```
------
#### Group by
- Group and index related items in an object according to the keying-function.
```javascript
const groupBy = (fn, list) => (
  list.reduce((prev, next) => ({
    ...prev,
    [fn(next)]: [...(prev[fn(next)] || []), next]
  }), {})
);

groupBy(vehicle => vehicle.make, [
  { make: 'tesla', model: '3' },
  { make: 'tesla', model: 'y' },
  { make: 'ford', model: 'mach-e' },
]);

// {
//   tesla: [ { make: 'tesla', ... }, { make: 'tesla', ... } ],
//   ford: [ { make: 'ford', ... } ],
// }
```
------
#### Collect By
- Create sub-lists containing related items according to the keying-function.
```javascript
import groupBy from './groupBy';

const collectBy = (fn, list) =>
  Object.values(groupBy(fn, list));

collectBy(vehicle => vehicle.make, [
  { make: 'tesla', model: '3' },
  { make: 'tesla', model: 'y' },
  { make: 'ford', model: 'mach-e' },
]);

// [
//   [ { make: 'tesla', ... }, { make: 'tesla', ... } ],
//   [ { make: 'ford', ... } ],
// ]
```
------
#### Head
- Get the first element of a list. This function is useful for writing clean and readable code.
```javascript
const head = list => list[0];

head([1, 2, 3]); // = 1
head([]); // = undefined
```
------
#### Tail
- Get all but the first element of a list. This function is useful for writing clean and readable code.
```javascript
const tail = list => list.slice(1);

tail([1, 2, 3]); // = [2, 3]
tail([]); // = []
```
------
#### Flatten
- Create a flat list by pulling all items from nested sub-lists recursively.
```javascript
const flatten = list => list.reduce((prev, next) => ([
  ...prev,
  ...(Array.isArray(next) ? flatten(next) : [next])
]), []);

flatten([[1, 2, [3, 4], 5, [6, [7, 8]]]]); // = [1, 2, 3, 4, 5, 6, 7, 8]
```
------

#### Intersection By
- Find all values that are present in both lists as defined by a keying-function.

```javascript
const intersectionBy = (fn, listA, listB) => {
  const b = new Set(listB.map(fn));
  return listA.filter(val => b.has(fn(val)));
};

intersectionBy(v => v, [1, 2, 3], [2, 3, 4]); // = [2, 3]
intersectionBy(v => v, [{ a: 1 }, { a: 2 }], [{ a: 2}, { a: 3 }, { a: 4 }]); // = [{ a: 2 }];
````
------

#### Index By
- Index each element in a list by a value determined by the keying-function.
```javascript
const indexBy = (fn, list) =>
  list.reduce((prev, next) => ({
    ...prev,
    [fn(next)]: next
  }, {}));

indexBy(val => val.a, [{ a: 1 }, { a: 2 }, { a: 3 }]);
// = { 1: { a: 1 }, 2: { a:2 }, 3: { a: 3 } }
```
------
#### Difference By
- Find all items in the first list that are not present in the second list — determined by the keying-function.
```javascript
import indexBy from './indexBy';

const differenceBy = (fn, listA, listB) => {
  const bIndex = indexBy(fn, listb);
  return listA.filter(val => !bIndex[fn(val)]);
});

differenceBy(val => val, [1,2,3], [3,4,5]); // = [1,2]
differenceBy(
  vehicle => vehicle.make,
  [{ make: 'tesla' }, { make: 'ford' }, { make: 'gm' }],
  [{ make: 'tesla' }, { make: 'bmw' }, { make: 'audi' }],
); // = [{ make: 'ford' }, { make: 'gm' }]
```
------
#### Recover With
- Return the default value if the given function throws an Error.
```javascript
const recoverWith = async (defaultValue, fn, ...args) => {
  try {
    const result = await fn(...args);
    return result;
  } catch (_e) {
    return defaultValue;
  }
}

recoverWith('A', val => val, 'B'); // = B
recoverWith('A', () => { throw new Error() }); // = 'A'
```
------
#### Distance
- Calculate the Euclidean distance between two points p1 & p2.
```javascript
const distance = ([x0, y0], [x1, y1]) => (
  Math.hypot(x1 - x0, y1 - y0)
);

distance([0, 1], [5, 4]); // = 5.8309518948453
```
------
#### Drop While
- Drops elements from the list, beginning at the first element, until some predicate is met.
```javascript
const dropWhile = (pred, list) => {
  let index = 0;
  list.every(elem => {
    index++;
    return pred(elem);
  });
  return list.slice(index-1);
}

dropWhile(val => (val < 5), [1,2,3,4,5,6,7]); // = [5,6,7]
```
------
#### Sum By
- Calculate the sum of all elements in a list given some function that produce the individual value of each element.
```javascript
const sumBy = (fn, list) =>
  list.reduce((prev, next) => prev + fn(next), 0);

sumBy(product => product.price, [
  { name: 'pizza', price: 10 },
  { name: 'pepsi', price: 5 },
  { name: 'salad', price: 5 },
]); // = 20
```
------
#### Ascending
- Creates a ascending comparator-function given a valuating function.
```javascript
const ascending = (fn) => (a, b) => {
  const valA = fn(a);
  const valB = fn(b);
  return valA < valB ? -1 : valA > valB ? 1 : 0;
}

const byPrice = ascending(val => val.price);
[{ price: 300 }, { price: 100 }, { price: 200 }].sort(byPrice);
// = [{ price: 100 }, { price: 200 }, { price: 300 }]
```
------
#### Descending
- Creates a descending comparator-function given a valuating function.
```javascript
const descending = (fn) => (a, b) => {
  const valA = fn(b);
  const valB = fn(a);
  return valA < valB ? -1 : valA > valB ? 1 : 0;
}

const byPrice = descending(val => val.price);
[{ price: 300 }, { price: 100 }, { price: 200 }].sort(byPrice);
// = [{ price: 300 }, { price: 200 }, { price: 100 }]
```
------
#### Find Key
- Find the first key within an index that satisfies the given predicate.
```javascript
const findKey = (predicate, index) => Object
  .keys(index)
  .find(key => predicate(index[key], key, index));

findKey(
  car => !car.available,
  {
    tesla: { available: true },
    ford: { available: false },
    gm: { available: true }
  },
); // = "ford"
```
------
#### Bifurcate By
- Split the values of a given list into two lists, one containing values the predicate function evaluates to truthy, the other list containing the falsy.
```javascript
const bifurcateBy = (predicate, list) =>
  list.reduce((acc, val, i) => (
    acc[predicate(val, i) ? 0 : 1].push(val), acc),
    [[], []]
  );

bifurcateBy(val => val > 0, [-1, 2, -3, 4]);
// = [[2, 4], [-1, -3]]
```
------
#### Pipe
- Perform left-to-right function composition. All extra arguments will be passed to the first function in the list, thus can have any arity. The result will be passed on the second, and the result of the second will be passed to third,… and so on until all functions have been processed.
```javascript
const pipe = (functions, ...args) => (
  functions.reduce(
    (prev, next) => Array.isArray(prev) ? next(...prev) : next(prev),
    args
  )
);
pipe([Math.abs, Math.floor, val => -val], 4.20); // = -4
pipe([(a, b) => a - b, Math.abs], 5, 10); // = 5
```
------



promise
mounted
iife
lifecycle

iife
callbacks
arrow functions
data.map(el => .........);
event loop
stream
Promise
get then reject catch
async await next()
exports module
express
middleware
next()
mongoose
validator
env
config

handling errors
logs
user
password reset
rate limiting
helmet
express-mongo-sanitizer
xss
parameter pollution


```javascript
// regex
// create regex
var regex = /.+/;
var regex = new RegExp('.+');
// create regex with flags
var regex = new RegExp('[0-9]+', 'g');
var regex = /[0-9]+/g;
// check if string matches regex
var success = string.match(regex);
// find match
var myArray = regex.exec('[input]');
// find index of first match
string.search(regex);
// regex replace
string.replace(regex, '[replace]');
// watch out: literal string replace
string.replace('find literal string', '[replacement]');
// regex syntax
// available flags
i // case insensitive
g // general search (all matches)
m // multiline mode
s // dot '.' matches '\n'
u // unicode support
y // sticky (exact position match)
// escape sequences
\\ // backslash
\. // literal dot '.'
\t // tab
\r // carriage return
\n // newline
\u[code] // unicode code point in hex
// meta characters
. // any character
^ // begin of input
$ // end of input
\b // word boundary
\B // not a word boundary
\d // digit
\D // not a digit
\s // whitespace
\S // not whitespace
\w // word
\W // not a word character
// repetition
? // optional character, greedy
?? // optional character, lazy
 * // zero or more characters, greedy
*? // zero or more characters, lazy
+ // one or more characters, greedy
+? // one or more characters, lazy
{N} // exactly N characters
{N,} // N or more characters, greedy
{N,}? // N or more characters, lazy
{N,M} // N to M characters, greedy
{N,M}? // N to M characters, lazy
// examples // all input
/.+/
// everything up until the last slash in string
/.*[/]/

```

```javascript
// arrays // initialize empty array
var a = [];
// initialize array with values
var a = [0, 1, 2];
// functions
// defining a function
function foo() {
  alert("foo");
}
```

* objects
```javascript
// define object
var person = {
    variableA: "A",
    variableB: "B",
};
```

```javascript
// object with method
var person = {
    variableA: "A",
    nethodName : function() {
        return "hello " + this.variableA;
    }
};
```

* control flow
```javascript
iterate array
for(i=0; i<parent.length; i++) {
    Console.log(parent[i]);
}
```

```javascript
// dom // lookup in dom using css selector
var e = document.querySelectorAll("[selector]");

// change window title
document.title = "New Title";

// go to url
window.location.href = "http://www.example.com";

// get full url of current page
window.location.href;

// get domain name only
window.location.hostname; //returns the domain name of the web host

// get path only
window.location.pathname;
// get protocol only (http|https)
window.location.protocol;

// Threads // invoke with a delay
setTimeout(
  function () {
    alert("foo");
  },
  [millis]
);
```

* delay event until user stops typing
```javascript
function delay(millis, callback) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, millis);
  };
}
```

* example usage
```javascript
document.getElementById("foo").onkeypress(
  delay(500, function (e) {
    alert("foo");
  })
);
```

### projects
- https://github.com/codemistic/Web-Development

* documentation
https://www.w3schools.com/jsref/jsref_obj_regexp.asp
https://javascript.info/regular-expressions


* key codes for events
https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
http://rmhh.co.uk/ascii.html
http://keycode.info/
