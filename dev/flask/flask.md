# flask

```python
@app.route('/')
def home():
    """Landing page."""
    nav = [
        {'name': 'Home', 'url': 'https://example.com/1'},
        {'name': 'About', 'url': 'https://example.com/2'},
        {'name': 'Pics', 'url': 'https://example.com/3'}
    ]
    return render_template(
        'home.html',
        nav=nav,
        title="Jinja Demo Site",
        description="Smarter page templates with Flask & Jinja."
    )
```


```python
from flask import Flask, render_template

app = Flask(__name__)


@app.route("/")
@app.route("/home")
def home():
    return render_template("index.html")


@app.route("/user/<name>")
def user(name):
    return render_template("user.html", name=name)


if __name__ == "__main__":
    app.run(debug=True, host="192.168.1.42", port=5555)
```

# flask-mail

``` python
```

# flask-error

```python
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500
```

# password-reset
# flask-models
# flask-views

```python
from sqlalchemy import func

def sidebar_data():
  recent = Post.query.order_by(
    Post.publish_date.desc()
  ).limit(5).all()
  top_tags = db.session.query(
    Tag, func.count(tags.c.post_id).label('total')
  ).join(
    tags
  ).group_by(Tag).order_by('total DESC').limit(5).all()
  return recent, top_tags
```
# blueprint

# flask-templates


# flask-config

```python
python
basedir = os.path.abspath(os.path.dirname(__file__))
```

```python
class Config(object):
    SECRET_KEY = '736670cb10a600b695a55839ca3a5aa54a7d7356cdef815d2ad6e19a2031182b'
    RECAPTCHA_PUBLIC_KEY = "6LdKkQQTAAAAAEH0GFj7NLg5tGicaoOus7G9Q5Uw"
    RECAPTCHA_PRIVATE_KEY = '6LdKkQQTAAAAAMYroksPTJ7pWhobYb88fTAcxcYn'
    POSTS_PER_PAGE = 10

    pass
```

```python
class ProdConfig(Config):
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DB_URI', '')
    pass
```

```python
class DevConfig(Config):
    DEBUG = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'database.db')
    CACHE_TYPE = 'simple'
    DEBUG = True
    pass
```

```python
class TestConfig(Config):
    db_file = tempfile.NamedTemporaryFile()

    DEBUG = True
    DEBUG_TB_ENABLED = False

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + db_file.name
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CACHE_TYPE = 'null'
    WTF_CSRF_ENABLED = False
    pass
```
# WTF-forms

```python
# csrf
import os
os.urandom(24)
'\xa8\xcc\xeaP+\xb3\xe8|\xad\xdb\xea\xd0\xd4\xe8\xac\xee\xfaW\x072@O3'

from flask_wtf import FlaskForm as Form
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired, Length
...
class CommentForm(Form):
  name = StringField(
    'Name',
    validators=[DataRequired(), Length(max=255)]
  )
  text = TextAreaField(u'Comment', validators=[DataRequired()])

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
class NameForm(FlaskForm):
    name = StringField('What is your name?', validators=[DataRequired()])
    submit = SubmitField('Submit')
```
** auth
*** login
*** openID & oauth
*** roles
** flask-sqlalchemy
** flask-pagination
** jinja2


### resourcs
- http://opentechschool.github.io/python-flask/
