# base_html

Extended by all the other templates
```jinja2
<html>
<head>
    {% block head %}
    <title>{% block title %}{% endblock %} - My Application</title>
    {% endblock %}
</head>

<body>
    {% block body %}
    {% endblock %}
</body>
</html>
```

# tips

```text
footer.html: Included by base.html
head.html: Included by base.html
messages.html: Included by base.html
navbar.html: Included by base.html
rightbody.html: Included by base.html
home.html: Rendered by the home Flask endpoint function
post.html: Rendered by the post Flask endpoint function
tag.html: Rendered by the posts_by_tag endpoint function
user.html: Rendered by the posts_by_user endpoint function
```

<h1>{{ post.title }}</h1>

{{ your_dict['key'] }}
{{ your_list[0] }}

{% raw %}
<script id="template" type="text/x-handlebars-template">
  <h1>{{title}}</h1>
  <div class="body">
    {{body}}
  </div>
</script>
{% endraw %}


{{ variable | filter_name(*args) }}

{{ variable | filter_name }}

{% filter filter_name %}
  A bunch of text
{% endfilter %}

{{ "<h1>Post Title</h1>" | safe }}

{{ {'key': False, 'key2': None, 'key3': 45} | tojson }}



{%if user.is_logged_in() %}
  <a href='/logout'>Logout</a>
{% else %}
  <a href='/login'>Login</a>
{% endif %}

{% for post in posts %}
  <div>
    <h1>{{ post.title }}</h1>
    <p>{{ post.text | safe }}</p>

{% for post in posts if post.text %}
  <div>
    <h1>{{ post.title }}</h1>
    <p>{{ post.text | safe }}</p>
  </div>
{% endfor %}

{% macro input(name, label, value='', type='text') %}
  <div class="form-group">
    <label for"{{ name }}">{{ label }}</label>
    <input type="{{ type }}" name="{{ name }}"
      value="{{ value | escape }}" class="form-control">
  </div>
{% endmacro %}


{{ config.SQLALCHEMY_DATABASE_URI }}

{{ request.url }}

{{ session.new }}

{{ url_for('home') }}


{{ url_for('post', post_id=1) }}

{% with messages = get_flashed_messages(with_categories=true) %}
    {% if messages %}
        {% for category, message in messages %}
        <div class="alert alert-{{ category }} alert-dismissible"
       role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-
   label="Close"><span aria-hidden="true">&times;</span></button>
            {{ message }}
        </div>
        {% endfor %}
    {% endif %}
{% endwith %}


{% include 'navbar.html' %}
<div class="container">
    <div class="row row-lg-4">
        <div class="col">
            {% block head %}
            {% include 'head.html' %}
            {% endblock %}
        </div>
    </div>
    {% include 'messages.html' %}
    {% block body %}
    <div class="row">
        <div class="col-lg-9">
            {% block leftbody %}
            {% endblock %}
        </div>
        <div class="col-lg-3 rounded">
            {% block rightbody %}
            {% include 'rightbody.html' %}
            {% endblock %}
        </div>
    </div>
    {% endblock %}
    {% include 'footer.html' %}


{% extends "base.html" %}
{% import 'macros.html' as macros %}
{% block title %}Home{% endblock %}
{% block leftbody %}
{{ macros.render_posts(posts) }}
{{ macros.render_pagination(posts, 'home') }}
{% endblock %}


{% macro render_posts(posts, pagination=True) %}
...
{% for post in _posts %}
<div >
    <h1>
        <a class="text-dark" href="{{ url_for('post', post_id=post.id)
}}">{{ post.title }}</a>
    </h1>
</div>
<div class="row">
    <div class="col">
        {{ post.text | truncate(500) | safe }}
        <a href="{{ url_for('post', post_id=post.id) }}">Read More</a>

{% extends "base.html" %}
{% import 'macros.html' as macros %}
{% block title %}{{ tag.title }}{% endblock %}
{% block leftbody %}
<div class="row">
    <div class="col bg-light">
        <h1 class="text-center">Posts With Tag {{ tag.title }}</h1>
    </div>
</div>
{{ macros.render_posts(posts, pagination=False) }}
{% endblock %}




#+END_SRC

