- https://github.com/igorbenav/FastAPI-boilerplate
- https://www.permit.io/blog/implement-authorization-in-fastapi?utm_source=reddit&utm_medium=referral&utm_campaign=comm
- https://github.com/zhanymkanov/fastapi-best-practices
- https://mergeboard.com/blog/6-multitenancy-fastapi-sqlalchemy-postgresql/
- https://github.com/PacktPublishing/Python-for-Software-Engineering-Bootcamp-/tree/main/PythonTutorialFastAPI-master
- https://github.com/htbrandao/fastemplate
- https://github.com/petrgazarov/FastAPI-app
- https://github.com/rednafi/fastapi-nano
- https://github.com/fastapi-users/fastapi-users


- [Path parameters](#Path parameters)
- [File upload](#File upload)
- [Response parameter](#Response parameter)
- [Request body](#Request body)
- [Pydantic](#Pydantic)
- [Dependendy injection](#Dependendy injection)

# install
```bash
pip install fastapi 'uvicorn[standard]'
```
# server
```bash
uvicorn project.app:app
```

# Path parameters
```python
from fastapi import FastAPI
from pydantic import BaseModel


class Post(BaseModel):
    title: str
    nb_views: int


class PublicPost(BaseModel):
    title: str


app = FastAPI()


# Dummy database
posts = {
    1: Post(title="Hello", nb_views=100),
}


@app.get("/posts/{id}", response_model=PublicPost)
async def get_post(id: int):
    return posts[id]
```

# File upload
```python
from typing import List

from fastapi import FastAPI, File, UploadFile

app = FastAPI()


@app.post("/files")
async def upload_multiple_files(files: List[UploadFile] = File(...)):
    return [
        {"file_name": file.filename, "content_type": file.content_type}
        for file in files
    ]
```


# Response parameter
```python
from fastapi import FastAPI, Response, status
from pydantic import BaseModel


class Post(BaseModel):
    title: str


app = FastAPI()

# Dummy database
posts = {
    1: Post(title="Hello"),
}


@app.put("/posts/{id}")
async def update_or_create_post(id: int, post: Post, response: Response):
    if id not in posts:
        response.status_code = status.HTTP_201_CREATED
    posts[id] = post
    return posts[id]
```


# Request body
```python
from fastapi import FastAPI
from pydantic import BaseModel


class User(BaseModel):
    name: str
    age: int


class Company(BaseModel):
    name: str


app = FastAPI()


@app.post("/users")
async def create_user(user: User, company: Company):
    return {"user": user, "company": company}
```


# Pydantic standard
```python
from datetime import date
from enum import Enum
from typing import List

from pydantic import BaseModel, ValidationError


class Gender(str, Enum):
    MALE = "MALE"
    FEMALE = "FEMALE"
    NON_BINARY = "NON_BINARY"


class Address(BaseModel):
    street_address: str
    postal_code: str
    city: str
    country: str


class Person(BaseModel):
    first_name: str
    last_name: str
    gender: Gender
    birthdate: date
    interests: List[str]
    address: Address


# Invalid address
try:
    Person(
        first_name="John",
        last_name="Doe",
        gender=Gender.MALE,
        birthdate="1991-01-01",
        interests=["travel", "sports"],
        address={
            "street_address": "12 Squirell Street",
            "postal_code": "424242",
            "city": "Woodtown",
            # Missing country
        },
    )
except ValidationError as e:
    print(str(e))

# Valid
person = Person(
    first_name="John",
    last_name="Doe",
    gender=Gender.MALE,
    birthdate="1991-01-01",
    interests=["travel", "sports"],
    address={
        "street_address": "12 Squirell Street",
        "postal_code": "424242",
        "city": "Woodtown",
        "country": "US",
    },
)
print(person)
```

# Dependendy injection
```python
from typing import Tuple

from fastapi import FastAPI, Depends, Query

app = FastAPI()


class Pagination:
    def __init__(self, maximum_limit: int = 100):
        self.maximum_limit = maximum_limit

    async def skip_limit(
        self,
        skip: int = Query(0, ge=0),
        limit: int = Query(10, ge=0),
    ) -> Tuple[int, int]:
        capped_limit = min(self.maximum_limit, limit)
        return (skip, capped_limit)

    async def page_size(
        self,
        page: int = Query(1, ge=1),
        size: int = Query(10, ge=0),
    ) -> Tuple[int, int]:
        capped_size = min(self.maximum_limit, size)
        return (page, capped_size)


pagination = Pagination(maximum_limit=50)


@app.get("/items")
async def list_items(p: Tuple[int, int] = Depends(pagination.skip_limit)):
    skip, limit = p
    return {"skip": skip, "limit": limit}


@app.get("/things")
async def list_things(p: Tuple[int, int] = Depends(pagination.page_size)):
    page, size = p
    return {"page": page, "size": size}

```

### templates
- https://github.com/zhanymkanov/fastapi_production_template
- https://towardsdatascience.com/build-a-back-end-with-postgresql-fastapi-and-docker-7ebfe59e4f06
- https://github.com/alexk1919/fastapi-motor-mongo-template
