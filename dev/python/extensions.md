# resources

- https://setuptools.pypa.io/en/latest/userguide/ext_modules.html
- https://www.pythonsheets.com/notes/python-c-extensions.html
- https://book.pythontips.com/en/latest/python_c_extension.html#python-c-api
- https://realpython.com/build-python-c-extension-module/
- https://docs.python.org/3/extending/extending.html
