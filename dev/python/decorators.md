# decorators

### validate inputs

```python

def validate_inputs(func):
    def validate(*args):

        variables_value_map = dict(zip(func.__code__.co_varnames, args))

        for key, value in func.__annotations__.items():
            assert isinstance(variables_value_map[key], value), 'Invalid Attribute'
        return func(*args)

    return validate

@validate_inputs
def val(a:str, 
        b: (int,type(None)),
        c: list,
        d: dict,
        e: type(None),
        f: (float,list),
        g: int
        ):
    pass


@validate_inputs
def some_other_method(inp:int):
    pass

some_other_method(5)
```

### timer
```python
import functools
import time

def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer

@timer
def waste_some_time(num_times):
    for _ in range(num_times):
        sum([i**2 for i in range(10000)])

waste_some_time(10000)
```

### debug
```python
import functools

def debug(func):
    """Print the function signature and return value"""
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]                      # 1
        kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
        signature = ", ".join(args_repr + kwargs_repr)           # 3
        print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")           # 4
        return value
    return wrapper_debug

@debug
def make_greeting(name, age=None):
    if age is None:
        return f"Howdy {name}!"
    else:
        return f"Whoa {name}! {age} already, you are growing up!"

make_greeting("Richard", age=112)
```

### plugins
```python
import random
PLUGINS = dict()

def register(func):
    """Register a function as a plug-in"""
    PLUGINS[func.__name__] = func
    return func

@register
def say_hello(name):
    return f"Hello {name}"

@register
def be_awesome(name):
    return f"Yo {name}, together we are the awesomest!"

def randomly_greet(name):
    greeter, greeter_func = random.choice(list(PLUGINS.items()))
    print(f"Using {greeter!r}")
    return greeter_func(name)

randomly_greet("Alice")
```
### exit
```python
import atexit

@atexit.register
def goodbye():
    print("Bye bye!")

print("Hello Yang!")
```

### singledispatch
```python
from functools import singledispatch

@singledispatch
def fun(arg):
    print("Called with a single argument")

@fun.register(int)
def _(arg):
    print("Called with an integer")

@fun.register(list)
def _(arg):
    print("Called with a list")

fun(1)  # Prints "Called with an integer"
fun([1, 2, 3])  # Prints "Called with a list"
```

### resources

- https://gist.github.com/Zearin/2f40b7b9cfc51132851a
- https://gist.github.com/mminer/34d4746fa82b75182ee7

