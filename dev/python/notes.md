# # profiler

```python
import cProfile

prof = cProfile.Profile()
prof.enable()

main()

prof.disable()
prof.print_stats()
```

### timer

```python
t1_start = perf_counter()
from time import perf_counter
main()
t1_stop = perf_counter()

t1 = f'T1: {round((t1_stop-t1_start)*10000, 3)}'

```

### poetry
```bash
poetry init
poetry add progressbar2
poetry install
poetry add 'progressbar2=3.1.0'
poetry update
poetry run pip
poetry shell
pipenv install progressbar2
pipenv install
pipenv update
```


### user input
```python
while (line := input('Please enter a line: ')) != '':
    print(line)
```

### read file
```python
if (fh := open("readme.md", "r")) != None:
    text = fh.read()
```

### follow file
- monitor logs

#### advanced
- https://twitter.com/treyhunner/status/1729149330706087947
