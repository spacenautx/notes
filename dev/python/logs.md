# resources

- https://docs.python.org/3/howto/logging-cookbook.html
- https://logbook.readthedocs.io
- http://plumberjack.blogspot.com/2010/09/improved-queuehandler-queuelistener.html
- https://eliot.readthedocs.io/en/stable/
- https://opentelemetry.io
- https://www.honeycomb.io
- https://lightstep.com
- sentry
- https://github.com/jaegertracing/jaeger/
- https://pyroscope.io
- https://prodfiler.com 
- https://guicommits.com/how-to-log-in-python-like-a-pro/

- https://github.com/driscollis/pythonlogging
