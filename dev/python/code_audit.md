| Tool        | Description                                                                  |
|:------------|:-----------------------------------------------------------------------------|
| Pylint      | Checks for errors, tries to enforce a coding standard, looks for code smells |
| PyFlakes    | Analyzes programs and detects various errors                                 |
| pycodestyle | Checks against some of the style conventions in PEP 8                        |
| pydocstyle  | Checks compliance with Python docstring conventions                          |
| Bandit      | Analyzes code to find common security issues                                 |
| MyPy        | Checks for optionally-enforced static types                                  |
| Pylama      |                                                                              |
| Flake8      |                                                                              |
| Mccabe      | Checks McCabe complexity                                                     |
| Radon       | Analyzes code for various metrics (lines of code, complexity, and so on)     |
| Black       | Formats Python code without compromise                                       |
| Isort       | Formats imports by sorting alphabetically and separating into sections       |
| Yapf        | Python formatter.                                                            |

### tests

| Pylint      | Logical & Stylistic | Checks for errors, tries to enforce a coding standard, looks for code smells |
| PyFlakes    | Logical             | Analyzes programs and detects various errors                                 |
| Bandit      | Logical             | Analyzes code to find common security issues                                 |
| MyPy        | Logical             | Checks for optionally-enforced static types                                  |
| pycodestyle | Stylistic           | Checks against some of the style conventions in PEP 8                        |
| pydocstyle  | Stylistic           | Checks compliance with Python docstring conventions                          |

code analysis and formatting tools:

| Mccabe | Analytical | Checks McCabe complexity                                                 |
| Radon  | Analytical | Analyzes code for various metrics (lines of code, complexity, and so on) |
| Black  | Formatter  | Formats Python code without compromise                                   |
| Isort  | Formatter  | Formats imports by sorting alphabetically and separating into sections   |

| flake8 | runs pycodestyle, pyflakes, maccabe |
