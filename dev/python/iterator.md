# iterator

```python
class AdvancedCountIterator:
    def __init__(self, start=0, step=1, stop=None):
        self.i = start
        self.start = start
        self.step = step
        self.stop = stop

    def __iter__(self):
        return self

    def __next__(self):
        if self.stop is not None and self.i >= self.stop:
            raise StopIteration

        value = self.i
        self.i += self.step
        return value

    def __len__(self):
        return int((self.stop - self.start) // self.step)

    def __contains__(self, key):
        # To check `if 123 in count`.
        # Note that this does not look at `step`!
        return self.start < key < self.stop

    def __repr__(self):
        return (
            f'{self.__class__.__name__}(start={self.start}, '
            f'step={self.step}, stop={self.stop})')

    def __getitem__(self, slice_):
        return itertools.islice(self, slice_.start,
                                slice_.stop, slice_.step)

# count = AdvancedCountIterator(start=2.5, step=0.5, stop=5)
```
