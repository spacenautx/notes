# k8s

# Command reference
## Minikube

```bash
minikube start
# starts minikube. If this hangs (wait 15 minutes)
```

```bash
minikube stop
# stops the minikube virtual machine. This may be necessary to do if you have an error when starting
```

```bash
minikube delete
# do this to completely wipe away the minikube image. Useful if minikube is refusing to start and all else fails. Also you can delete all files in <home>/.minikube and <home>/.kube
```

```bash
minikube docker-env
# find out the required environment variables to connect to the docker daemon running in minikube.
```

```bash
minikube ip
# find out the ip address of minikube. Needed for browser access.
```

## Kubectl

```bash
kubectl get all
# list all objects that you’ve created. Pods at first, later, ReplicaSets, Deployments and Services
```

```bash
kubectl apply –f <yaml file>
# either creates or updates resources depending on the contents of the yaml file
```

```bash
kubectl apply –f .
# apply all yaml files found in the current directory
```

```bash
kubectl describe pod <name of pod>
# gives full information about the specified pod
```

```bash
kubectl exec –it <pod name> <command>
# execute the specified command in the pod’s container.  Doesn’t work well in Cygwin.
```

```bash
kubectl get (pod | po | service | svc | rs | replicaset | deployment | deploy)
# get all pods or services. replicasets and deployments.
```

```bash
kubectl get po --show-labels
# get all pods and their labels
```

```bash
kubectl get po --show-labels -l {name}={value}
# get all pods matching the specified name:value pair
```

```bash
kubectl delete po <pod name>
# delete the named pod. Can also delete svc, rs, deploy
```

```bash
kubectl delete po --all
# delete all pods (also svc, rs, deploy)
```

## Deployment Management

```bash
kubectl rollout status deploy <name of deployment>
# get the status of the named deployment
```

```bash
kubectl rollout history deploy <name of deployment>
# get the previous versions of the deployment
```

```bash
kubectl rollout undo deploy <name of deployment>
# optionally -- to-revision=<revision number>
# recommended: this is used only in stressful emergency situations! Your YAML will now be out of date with the live deployment!
```

# Pod

# Services

### resources
- https://towardsdatascience.com/learn-kubernetes-the-easy-way-d1cfa460c013
