# BaseButton

```vue
<template>
  <button class="button">
    <slot/>
  </button>
</template>
```
