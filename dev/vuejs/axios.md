# axios

- GET, POST PUT, DELETE
- add authentication to each request
- set timeouts if requests take too long
- configure defaults for every request
- intercept requests to create middleware
- handle error and cancel requests properly
- properly serialize and deserialize requests & responses

### refactor API call into service layer
