# checklist


- [ ] move events data to parent (Home.vue)
- [ ] parent creates EventCard components
- [ ] parent feeds each EventCard its own event
- [ ] parent displays EventCards in a Flexbox container
