# composition API

### setup()
Executes before
- components
- props
- data
- methods
- computed properties
- lifecycle methods
- no access to "this"

optional arguments
1. props
2. context

- watchEffect
