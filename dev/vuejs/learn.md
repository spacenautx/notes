# VueJS

### vuex state management
#### store
- [state](state.md)
- [mutations](mutations.md)
    - action calls state
    - UPPER_CASE
    - update/mutate vuex state
- [actions](actions.md)
    - component calls action
    - check conditions / call api before mutation
    - dispatch
    - notification
- [getters](getters.md)
    - mapState
    - state calls getters
- modules

### components
- [components](component.md)

### requirements
```bash
npm install -g @vue/cli
vue create frontvue
```
- front page


### project structure

- project/
    - package.json
    - public/
    - src/
        - App.vue
        - main.js
        - assets/
            - logo.png
            - favicon.ico
            - index.html
            - styles/
                - style.css
        - components/
            - About.vue
            - Account.vue
            - [BaseButton.vue](BaseButton.md)
            - BaseCheckBox.vue
            - BaseInput.vue
            - BaseRadio.vue
            - BaseRadioGroup.vue
            - BaseSelect.vue
            - Footer.vue
            - FormContact.vue
            - [FormLogin.vue](component.md#login form)
            - FormPasswordReset.vue
            - FormRegistration.vue
            - FormUpload.vue
            - FormProfile.vue
            - Gallery.vue
            - Home.vue
            - List.vue
            - Lists.vue
            - NavBar.vue
            - Notification.vue
            - LeftSideBar.vue
            - RightSideBar.vue
            - Pagination.vue
            - PrivacyPolicy.vue
            - Products.vue
            - Product.vue
            - Profile.vue
            - Cart.vue
            - Orders.vue
            - Settings.vue
            - Table.vue
            - TermsAndConditions.vue
        - services/
            - apiClient.ts
            - statusCode.ts
            - auth.ts
            - http.ts
        - router/
            - index.js
        - store/
            - user.js
            - notification.js
        - utils/
        - views/
            - About.vue
            - Home.vue



### main.js

```javascript
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

const app = createApp(App)
app.use(router)
app.mount('#app')
```

### App.vue

```vue
<template>
  <div id="nav">
    <router-link to="/">Home</router-link>
    <router-link to="/about">About</router-link>
  </div>
  </router-view>
</template>
```


### router/index.js

```javascript
import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // lazy-loaded route
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
```

### request/statusCode.ts

```javascript
export const showStatus = ( status ?: number ) => {
    let message = "";

    switch  (status) {

        case 400:
            message = "Request Error (400)";
        break;

        case 401:
            message = "Unauthorized, please log in again (401)";
        break;

        case 403:
            message = "Access Denied (403)";
        break;

        case 404:
            message = "Request error (404)";
        break;

        case 408:
            message = "Request timed out (408)";
        break;

        case 500:
            message = "Server Error (500)";
        break;

        case 501:
            message = "Service not implemented (501)";
        break;

        case 502:
            message = "Network error (502)";
        break;

        case 503:
            message = "Service unavailable (503)";
        break;

        case 504:
            message = "Network timed out (504)";
        break;

        case 505:
            message = "HTTP version not supported (505)";
        break;

        default:
            message = `Error connecting (${status})!`;
    }

    return `${message}, please check the network or contact the administrator! `;
};
```


### resources
- https://github.com/webfansplz/vuejs-challenges
- https://vuejsexamples.com
- https://github.com/vuejs/awesome-vue
