# vue forms

- src/
    - components/
        - BaseInput.vue
        - BaseSelect.vue
    - views/
        - SimpleForm.vue

- add label prop
- make it v-model capable
- make the options reactive


### validate
* vee-validate

- validate fields and forms
- handle errors
- key API concepts
- YUP
- best practices
