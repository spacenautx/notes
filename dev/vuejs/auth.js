const TokenKey = "Authorization$://"; //authorization code

/* Get getItem */
export function getLocal(key ?: string) {
    return localStorage.getItem(key ? key : TokenKey) as any;
}

/* set setItem */
export function setLocal(key : string | undefined , params : any) {
    return localStorage.setItem(key ? key : TokenKey , params);
}

/* remove removeItem */
export function removeLocal(key ?: string) {
    return localStorage.removeItem(key ? key : TokenKey );
}

/* Clear all items */
export function clearLocal() {
    return localStorage.clear();
}
