# vuejs


| Name       | Purpose                                                                         |
| :----      | :----                                                                           |
| main.js    | app entry point, which loads and initializes Vue along with the root component  |
| App.vue    | Root component, starting point from which all other components will be rendered |
| components | where UI components are stored                                                  |
| router.js  | where URLS are defined and mapped to components                                 |
| views      | where UI components that are tied to the router are stored                      |
| assets     | where static assets, like images and fonts, are stored                          |


* components
* directives
button @click
v-model
v-if
v-else
v-for
** template
** script
** export default
*** props
[
'name',
'phone',
'email'
]

*** slot
*** data () {
    return {
        vueLink: "vuejs.org"
        }
    }
*** watch
**** counter
*** computed
*** methods
some functions () {
        return something;
    }
**** created
*** name
*** events
*** emit
** mounted
** style
* vue-router
** setup
** register
** render
** router-link
** styling
** navigation
** route-params
** watchers
** nested-routes
** catch-all
** scroll-behaviour
* vuex
** store
*** namedspace
*** state
*** mutations
*** actions
*** getters
*** mapGetterS
* config
* dist
* html-page
** directives
*** v-html
*** v-bind:value
**** :value
*** v-bind:syle
*** v-bind:class
*** v-on:click
**** @click
*** v-model (two-way binding)
**** v-on:input + v-bind:value
*** v-on:input
*** v-on:submit
*** v-once
*** v-if v-else
*** v-show
*** v-for
*** v-slot
*** v-to

* vue-cli
