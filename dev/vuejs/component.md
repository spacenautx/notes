# single file component

* anatomy for a view component
-----

1. template gives the skeletal structure
```vue
<template
    <p>{{ learning... }} </p>
</template>
 ```

2. script gives the functionality/brain (can be typescript)
```javascript
<script>
export default {
    data() {
        return {
            total: 0
            }
    }
}
</script>
```

3. style gives the looks (can be scss)
```css
<style scoped>
p {
    font-size: 14px;
}
</style>
```

#### login form
```javascript
```

### lifecycle hooks (options api)
- beforeCreate
- created
- beforeMount
- mounted
- beforeUpdate
- updated
- beforeUnmount
- unmounted
- errorCaptured
- renderTracked
- renderTriggered
