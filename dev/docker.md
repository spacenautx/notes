# docker

## quick

```bash
docker build . *  # creates image from Dockerfile.
docker system prune -a *  # remove any stopped containers and all unused images
docker images -a       *  # show all images
docker run -it --cpuset-cpus=4 -m 300M bionic:bash *  # run with cpu and mem defined
docker system df *  # docker storage space used
docker image ls -f dangling=true *  # show dangling images
docker image rm $(docker image ls -f dangling=true -q) *
docker container ls -aq *  # show all containers
# remove all stopped containers
docker container rm -f $(docker container ls -aq) *
```

| instructions| explanation                                                                                                                            |
|:---         |:---                                                                                                                                    |
| FROM        | Sets the Base Image for subsequent instructions.                                                                                       |
| MAINTAINER  | (deprecated - use LABEL instead) Set the Author field of the generated images.                                                         |
| RUN         | execute any commands in a new layer on top of the current image and commit the results.                                                |
| CMD         | provide defaults for an executing container.                                                                                           |
| EXPOSE      | informs Docker that the container listens on the specified network ports at runtime. does not make ports accessible.                   |
| ENV         | sets environment variable.                                                                                                             |
| ADD         | copies new files, directories or remote file to container. Invalidates caches. Avoid ADD and use COPY instead.                         |
| COPY        | copies new files or directories to container. copies as root . --chown=<user>:<group> ownership to another user/group. (Same for ADD.) |
| ENTRYPOINT  | configures a container that will run as an executable.                                                                                 |
| VOLUME      | creates a mount point for externally mounted volumes or other containers.                                                              |
| USER        | sets the user name for following RUN / CMD / ENTRYPOINT commands.                                                                      |
| WORKDIR     | sets the working directory.                                                                                                            |
| ARG         | defines a build-time variable.                                                                                                         |
| ONBUILD     | adds a trigger instruction when the image is used as the base for another build.                                                       |
| STOPSIGNAL  | sets the system call signal that will be sent to the container to exit.                                                                |
| LABEL       | apply key/value metadata to your images, containers, or daemons.                                                                       |
| SHELL       | override default shell is used by docker to run commands.                                                                              |
| HEALTHCHECK | tells docker how to test a container to check that it is still working.                                                                |

## kali docker

```bash

docker pull kalilinux/kali-linux-docker
docker run -t -i kalilinux/kali-linux-docker /bin/bash

#  inside the docker:
apt-get update -y
apt dist-upgrade
apt autoremove
apt clean

apt-get update && apt-get install metasploit-framework vim git tmux zsh openssh-server

update-rc.d -f ssh remove
update-rc.d -f ssh defaults
rm /etc/ssh/ssh_host_*
dpkg-reconfigure openssh-server

#  Restart the SSH server
service ssh restart

#  Auto start SSH
update-rc.d -f ssh enable 2 3 4 5
docker start admiring_lehmann && docker exec -it admiring_lehmann service ssh restart

#  storage location
/usr/lib/systemd/system/docker.service --> ExecStart=/usr/bin/dockerd -g /home/untitled/containers -H fd://
```


## kali

```bash
docker version
docker info
docker pull kalilinux/kali-linux-docker
docker run -t -i kalilinux/kali-linux-docker /bin/bash
#  The -t option invokes a tty terminal (basically a reverse shell)
#  to the image. The /bin/bash launches the bash terminal environment.

#  Create an image from a Dockerfile.
docker build
docker build [options] . -t "app/container_name"    *  name

docker run
docker run [options] IMAGE
#  see `docker create` for options
#  Run a command in an image.

#  Manage containers
docker create
docker create [options] IMAGE
    -a, --attach               *  attach stdout/err
-i, --interactive          *  attach stdin (interactive)
    -t, --tty                  *  pseudo-tty
    --name NAME            *  name your image
    -p, --publish 5000:5000    *  port map
    --expose 5432          *  expose a port to linked containers
    -P, --publish-all          *  publish all ports
    --link container:alias *  linking
    -v, --volume `pwd`:/app    *  mount (absolute paths needed)
    -e, --env NAME=hello       *  env vars
#  Example
    docker create --name app_redis_1 \
        --expose 6379 \
        redis:3.0.2
#  Create a container from an image.

        docker exec
        docker exec [options] CONTAINER COMMAND
        -d, --detach        *  run in background
        -i, --interactive   *  stdin
        -t, --tty           *  interactive

#  Example
        docker exec app_web_1 tail logs/development.log
        docker exec -t -i app_web_1 rails c

#  Run commands in a container.

#  Start/stop a container.
#  docker start
        docker start [options] CONTAINER
        -a, --attach        *  attach stdout/err
        -i, --interactive   *  attach stdin
        docker stop [options] CONTAINER

#  Manage containers using ps/kill.
        docker ps
        docker ps
        docker ps -a
        docker kill $ID

#  Images
        docker images
        docker images
# REPOSITORY   TAG        ID
        ubuntu       12.10      b750fe78269d
        me/myapp     latest     7b2431a8d968
        docker images -a   *  also show intermediate

#  Manages images.
#  docker rmi
        docker rmi b750fe78269d
#  Deletes images.
```

## best practices
- Before we get into the best practices for using Docker, here’s a quick overview of the vocabulary you should know:
- Layer: a set of read-only files or commands that describe how to set up the underlying system beneath the container.
- Layers are built on top of each other, and each one represents a change to the filesystem.
- Image: an immutable layer that forms the base of the container.
- Container: an instance of the image that can be executed as an independent application.
- The container has a mutable layer that lies on top of the image and that is separate from the underlying layers.
- Registry: a storage and content delivery system used for distributing Docker images.
- Repository: a collection of related Docker images, often different versions of the same application.
- Try to keep your images as small as possible.
- Don’t include libraries and dependencies unless they’re an absolute requirement for the application to run.
- If your application needs to be scalable, consider using Docker Swarm, a tool for managing a cluster of nodes as a single virtual system.

```bash
#  Developing with Docker Containers:
docker create [image] *  Create a new container from a particular image.
docker login *  Log into the Docker Hub repository.
docker pull [image] *  Pull an image from the Docker Hub repository.
docker push [username/image] *  Push an image to the Docker Hub repository.
docker search [term] *  Search the Docker Hub repository for a particular term.
docker tag [source] [target] *  Create a target tag or alias that refers to a source image.
#  Running Docker Containers
docker start [container] *  Start a particular container.
docker stop [container] *  Stop a particular container.
docker exec -ti [container] [command]: *  Run a shell command inside a particular container.
docker run -ti — image [image] [container] [command]:
#  Create and start a container at the same time, and then run a command inside it.
docker run -ti — rm — image [image] [container] [command]:
#  Create and start a container at the same time, run a command inside it, and then remove the container after executing the command.
docker pause [container]: *  Pause all processes running within a particular container.
#  Using Docker Utilities:
docker history [image]: *  Display the history of a particular image.
docker images *  List all of the images that are currently stored on the system.
docker inspect [object] *  Display low-level information about a particular Docker object.
docker ps *  List all of the containers that are currently running.
docker version *  Display the version of Docker that is currently installed on the system.
#  Cleaning Up Your Docker Environment:
docker kill [container] *  Kill a particular container.
docker kill $(docker ps -q) *  Kill all containers that are currently running.
docker rm [container] *  Delete a particular container that is not currently running.
docker rm $(docker ps -a -q) *  Delete all containers that are not currently running.
```

```bash
# Replace latest with a pinned version tag from https://hub.docker.com/_/alpine
#
# We suggest using the major.minor tag, not major.minor.patch.
FROM alpine:latest

# Non-root user for security purposes.
#
# UIDs below 10,000 are a security risk, as a container breakout could result
# in the container being ran as a more privileged user on the host kernel with
# the same UID.
#
# Static GID/UID is also useful for chown'ing files outside the container where
# such a user does not exist.
RUN addgroup -g 10001 -S nonroot && adduser -u 10000 -S -G nonroot -h /home/nonroot nonroot

# Install packages here with `apk add --no-cache`, copy your binary
# into /sbin/, etc.

# Tini allows us to avoid several Docker edge cases, see https://github.com/krallin/tini.
RUN apk add --no-cache tini
ENTRYPOINT ["/sbin/tini", "--", "myapp"]
# Replace "myapp" above with your binary

# bind-tools is needed for DNS resolution to work in *some* Docker networks, but not all.
# This applies to nslookup, Go binaries, etc. If you want your Docker image to work even
# in more obscure Docker environments, use this.
RUN apk add --no-cache bind-tools

# Use the non-root user to run our application
USER nonroot

# Default arguments for your app (remove if you have none):
CMD ["--foo", "1", "--bar=2"]
```

## commands

```bash
#  Installation
#  Linux
curl -sSL https://get.docker.com/ | sh
#  Mac
#  Use this link to download the dmg.
#  https://download.docker.com/mac/stable/Docker.dmg
#  Windows
#  Use the msi installer:
#  https://download.docker.com/win/stable/InstallDocker.msi
#  Docker Registries & Repositories
#  Login to a Registry
docker login
docker login localhost:8080
#  Logout from a Registry
docker logout
docker logout localhost:8080
#  Searching an Image
docker search nginx
docker search nginx --stars=3 --no-trunc busybox
#  Pulling an Image
docker pull nginx
docker pull eon01/nginx localhost:5000/myadmin/nginx
#  Pushing an Image
docker push eon01/nginx
docker push eon01/nginx localhost:5000/myadmin/nginx
#  Running Containers
#  Creating a Container
docker create -t -i eon01/infinite --name infinite
#  Running a Container
docker run -it --name infinite -d eon01/infinite
#  Renaming a Container
docker rename infinite infinity
#  Removing a Container
docker rm infinite
#  Updating a Container
docker update --cpu-shares 512 -m 300M infinite
#  Starting & Stopping Containers
#  Starting
docker start nginx
#  Stopping
docker stop nginx
#  Restarting
docker restart nginx
#  Pausing
docker pause nginx
#  Unpausing
docker unpause nginx
#  Blocking a Container
docker wait nginx
#  Sending a SIGKILL
docker kill nginx
#  Connecting to an Existing Container
docker attach nginx
#  Getting Information about Containers
#  Running Containers
docker ps
docker ps -a
#  Container Logs
docker logs infinite
#  Inspecting Containers
docker inspect infinite
#  Containers Events
docker events infinite
#  Public Ports
docker port infinite
#  Running Processes
docker top infinite
#  Container Resource Usage
docker stats infinite
#  Inspecting Changes to Files or Directories on a Container’s Filesystem
docker diff infinite
#  Manipulating Images
#  Listing Images
docker images
#  Building Images
docker build .
docker build github.com/creack/docker-firefox
docker build - < Dockerfile
docker build - < context.tar.gz
docker build -t eon/infinite .
docker build -f myOtherDockerfile .
curl example.com/remote/Dockerfile | docker build -f - .
#  Removing an Image
docker rmi nginx
#  Loading a Tarred Repository from a File or the Standard Input Stream
docker load < ubuntu.tar.gz
docker load --input ubuntu.tar
#  Save an Image to a Tar Archive
docker save busybox > ubuntu.tar
#  Showing the History of an Image
docker history
#  Creating an Image Fron a Container
docker commit nginx
#  Tagging an Image
docker tag nginx eon01/nginx
#  Pushing an Image
docker push eon01/nginx
#  Networking
#  Creating Networks
docker network create -d overlay MyOverlayNetwork
docker network create -d bridge MyBridgeNetwork
docker network create -d overlay \
    --subnet=192.168.0.0/16 \
    --subnet=192.170.0.0/16 \
    --gateway=192.168.0.100 \
    --gateway=192.170.0.100 \
    --ip-range=192.168.1.0/24 \
    --aux-address="my-router=192.168.1.5" --aux-address="my-switch=192.168.1.6" \
    --aux-address="my-printer=192.170.1.5" --aux-address="my-nas=192.170.1.6" \
    MyOverlayNetwork
#  Removing a Network
docker network rm MyOverlayNetwork
#  Listing Networks
docker network ls
#  Getting Information About a Network
docker network inspect MyOverlayNetwork
#  Connecting a Running Container to a Network
docker network connect MyOverlayNetwork nginx
#  Connecting a Container to a Network When it Starts
docker run -it -d --network=MyOverlayNetwork nginx
#  Disconnecting a Container from a Network
docker network disconnect MyOverlayNetwork nginx
#  Cleaning Docker
#  Removing a Running Container
docker rm nginx
#  Removing a Container and its Volume
docker rm -v nginx
#  Removing all Exited Containers
docker rm $(docker ps -a -f status=exited -q)
#  Removing All Stopped Containers
docker rm `docker ps -a -q`
#  Removing a Docker Image
docker rmi nginx
#  Removing Dangling Images
docker rmi $(docker images -f dangling=true -q)
#  Removing all Images
docker rmi $(docker images -a -q)
#  Stopping & Removing all Containers
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
#  Removing Dangling Volumes
docker volume rm $(docker volume ls -f dangling=true -q)
#  Docker Swarm
#  Installing Docker Swarm
curl -ssl https://get.docker.com | bash
#  Initializing the Swarm
docker swarm init --advertise-addr 192.168.10.1
#  Getting a Worker to Join the Swarm
docker swarm join-token worker
#  Getting a Manager to Join the Swarm
docker swarm join-token manager
#  Listing Services
docker service ls
#  Listing nodes
docker node ls
#  Creating a Service
docker service create --name vote -p 8080:80 instavote/vote
#  Listing Swarm Tasks
docker service ps
Scaling a Service
docker service scale vote=3
#  Updating a Service
docker service update --image instavote/vote:movies vote
docker service update --force --update-parallelism 1 --update-delay 30s nginx
docker service update --update-parallelism 5--update-delay 2s --image instavote/vote:indent vote
docker service update --limit-cpu 2 nginx
docker service update --replicas=5 nginx
```

## Dockerfile

```bash

#  NOTE: this example is taken from the default Dockerfile for the official nginx Docker Hub Repo
#  https://hub.docker.com/_/nginx/
#  NOTE: This file is slightly different than the video, because nginx versions have been updated
#        to match the latest standards from docker hub... but it's doing the same thing as the video
#        describes
FROM debian:stretch-slim
#  all images must have a FROM
#  usually from a minimal Linux distribution like debian or (even better) alpine
#  if you truly want to start with an empty container, use FROM scratch

ENV NGINX_VERSION 1.13.6-1~stretch
ENV NJS_VERSION   1.13.6.0.1.14-1~stretch

#  optional environment variable that's used in later lines and set as envvar when container is running

RUN apt-get update \
&& apt-get install --no-install-recommends --no-install-suggests -y gnupg1 \
&& \
NGINX_GPGKEY=573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62; \
found=''; \
for server in \
    ha.pool.sks-keyservers.net \
    hkp://keyserver.ubuntu.com:80 \
    hkp://p80.pool.sks-keyservers.net:80 \
    pgp.mit.edu \
; do \
echo "Fetching GPG key $NGINX_GPGKEY from $server"; \
apt-key adv --keyserver "$server" --keyserver-options timeout=10 --recv-keys "$NGINX_GPGKEY" && found=yes && break; \
done; \
test -z "$found" && echo >&2 "error: failed to fetch GPG key $NGINX_GPGKEY" && exit 1; \
apt-get remove --purge -y gnupg1 && apt-get -y --purge autoremove && rm -rf /var/lib/apt/lists/* \
&& echo "deb http://nginx.org/packages/mainline/debian/ stretch nginx" >> /etc/apt/sources.list \
&& apt-get update \
&& apt-get install --no-install-recommends --no-install-suggests -y \
nginx=${NGINX_VERSION} \
nginx-module-xslt=${NGINX_VERSION} \
nginx-module-geoip=${NGINX_VERSION} \
nginx-module-image-filter=${NGINX_VERSION} \
nginx-module-njs=${NJS_VERSION} \
gettext-base \
&& rm -rf /var/lib/apt/lists/*

#  optional commands to run at shell inside container at build time
#  this one adds package repo for nginx from nginx.org and installs it

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
&& ln -sf /dev/stderr /var/log/nginx/error.log
#  forward request and error logs to docker log collector

EXPOSE 80 443
#  expose these ports on the docker virtual network
#  you still need to use -p or -P to open/forward these ports on host

CMD ["nginx", "-g", "daemon off;"]
#  required: run this command when container is launched
#  only one CMD allowed, so if there are multiple, last one wins
```

## compose yaml

```yaml
version: "3.3"
services:
proxy:
image: nginx:1.13.6
configs:
    - source: proxy
target: /etc/nginx/nginx.conf
ports:
    - 8000:8000
networks:
    - front
deploy:
restart_policy:
condition: on-failure
api:
image: lucj/api
networks:
    - front
deploy:
restart_policy:
condition: on-failure
configs:
proxy:
external: true
networks:
front:
```

---------

## resources
- [dockerfiles](https://github.com/jessfraz/dockerfiles)
- [cheatsheet](https://github.com/wsargent/docker-cheat-sheet)
- [docker-mastery](https://github.com/BretFisher/udemy-docker-mastery)
- [official images](https://github.com/docker-library/official-images/tree/master/library)
- [documentations](https://docs.docker.com/)
- [docs](https://docker-curriculum.com)
- [recipes](https://github.com/vimagick/dockerfiles)
- [Best practices](https://github.com/hexops/dockerfile)
- [buildkit](https://github.com/moby/buildkit)
- [Docker Labs](https://github.com/collabnix/dockerlabs)
- [labs](https://github.com/docker/labs)
- [awesome docker](https://github.com/veggiemonk/awesome-docker)
- [optimize](https://github.com/wagoodman/dive)
- [labtainers](https://github.com/mfthomps/Labtainers)
- [cheat-sheet](https://github.com/wsargent/docker-cheat-sheet)
- [pentest](https://blog.ropnop.com/docker-for-pentesters/)
- [linter](https://github.com/goodwithtech/dockle)
- [scan](https://github.com/aquasecurity/trivy)
- [security](https://infosecwriteups.com/advanced-docker-security-2ef31ac7547f)
- [slim](https://blog.codacy.com/five-ways-to-slim-your-docker-images/)
- [python container](https://github.com/PacktPublishing/Python-in-Containers)
