# dev

## editors
-----
- [vim](../tools/configs/vim.md)

## APIs
------
- [public apis](https://github.com/n0shake/Public-APIs)
- [public APIs](https://github.com/public-apis/public-apis)
- https://github.com/toddmotto/public-apis
- [json-server](https://github.com/typicode/json-server)

## dbs
------
- sqlite3
- mongodb
- postgresql

## caching
- redis
- badger

## load balancers
- traefik

## servers
------
- [frp](https://github.com/fatedier/frp)
- [nginx](../tools/configs/nginx.md)

## tests
-----
- testing

## CI/CD
-

## production
-----
- [going to production](https://github.com/mtdvio/going-to-production)

## benchmark
------
- [efficiency](https://sites.google.com/view/energy-efficiency-languages/home)
- [benchmarks](https://github.com/kostya/benchmarks)
- [hyperfine](https://github.com/sharkdp/hyperfine)
- [awesome http benchmark](https://github.com/denji/awesome-http-benchmark)
- [realworld](https://github.com/gothinkster/realworld)
- [load testing](https://github.com/wg/wrk)
- [webframeworks](https://github.com/the-benchmarker/web-frameworks)
- [performance](https://github.com/thedaviddias/Front-End-Performance-Checklist)
- https://programming-language-benchmarks.vercel.app/rust-vs-go
- https://github.com/hatoo/oha

## others
-----
- https://github.com/Coder-World04/Complete-System-Design
- [system design primer](https://github.com/donnemartin/system-design-primer)
- https://github.com/mhadidg/software-architecture-books
- [low level design primer](https://github.com/prasadgujar/low-level-design-primer)
- https://github.com/mehdihadeli/awesome-software-architecture
- https://github.com/puncsky/system-design-and-architecture
- https://github.com/robinstickel/awesome-design-principles
- [sdp qs](https://github.com/arpitbbhayani/system-design-questions)
- [system design resources](https://github.com/InterviewReady/system-design-resources)
- [seniority](https://roadmap.sh/guides/levels-of-seniority)
- [side project kit](https://github.com/maxprilutskiy/side-project-kit)
- [stack on a budget](https://github.com/255kb/stack-on-a-budget)
- [promote](https://github.com/trekhleb/promote-your-next-startup)
- [site reliability engineering](https://github.com/dastergon/awesome-sre)
