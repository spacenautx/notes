# CSS

### best practices
- https://gist.github.com/MWins/6c07745a7c55ee60b131
- https://en.bem.info/methodology/quick-start/

## grid

### sass
- https://blog.logrocket.com/understanding-css-grid-by-building-your-own-grid/

### glitch
- https://twitter.com/NanouuSymeon/status/1695796835380613151

### resources
- https://github.com/AllThingsSmitty/css-protips
- https://lhammer.cn/You-need-to-know-css/#/
- [complete guide to grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
- [skelton-ux](https://www.freecodecamp.org/news/how-to-build-skeleton-screens-using-css-for-better-user-experience/)
- https://github.com/vasanthk/css-refresher-notes
- https://github.com/adamschwartz/magic-of-css
- https://github.com/micromata/awesome-css-learning
- https://twitter.com/NanouuSymeon/status/1685500463477927936
- https://github.com/shadcn-ui/ui
