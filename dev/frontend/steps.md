- [ ] grid layout
- [ ] responsiveness
- [ ] colors
- [ ] fonts
- [ ] icons
- [ ] navbar
- [ ] side panels
- [ ] footer
- [ ] contact form
- [ ] registration page
- [ ] login form
- [ ] password reset form
- [ ] profile form
- [ ] admin settings
- [ ] cards
- [ ] accordion
- [ ] search
- [ ] tables
- [ ] gallery
- [ ] file uploads
