- https://medium.com/codex/terraform-best-practices-how-to-structure-your-terraform-projects-b5b050eab554
- https://github.com/antonbabenko/terraform-best-practices

# Section1 Readme.md
### destroy.md
### first-ec2.md
### github.md
### provider-versioning.md
### resp01.md
---

# Section2 Read, Generate, Modify Configuration Readme.md
### approach-to-variable-assignment.md
### conditional.md
### counte-parameter.md
### data-sources.md
### data-types.md
### debugging.md
### dynamic-block.md
### functions.md
### graph.md
### large-infra.md
### load-order.md
### local-values.md
### plan-to-file.md
### settings.md
### splat-expression.md
### state-management.md

*  state-management.tf
```bash
provider "aws" {
  region     = "us-west-2"
  access_key = "YOUR-ACCESS-KEY"
  secret_key = "YOUR-SECRET-KEY"
}

resource "aws_instance" "myec2" {
  ami           = "ami-082b5a644766e0e6f"
  instance_type = "t2.micro"
}

resource "aws_iam_user" "lb" {
  name = "loadbalancer"
  path = "/system/"
}

terraform {
  backend "s3" {
    bucket = "kplabs-remote-backends"
    key    = "demo.tfstate"
    region = "us-east-1"
    access_key = "YOUR-ACCESS-KEY"
    secret_key = "YOUR-SECRET-KEY"
  }
}
```

* Commands used for State Management
```bash
terraform state list
terraform state mv aws_instance.webapp aws_instance.myec2
terraform state pull
terraform state rm aws_instance.myec2
```

### taint.md
### terraform-format.md

### terraform-providers.md

* Provider Documentation
- https://www.terraform.io/docs/providers/index.html

* aws.tf
```bash
provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}
```

* wavefront.tf
```bash
provider "wavefront" {
   address = "spaceape.wavefront.com"
 }
```

* Downloading the Wavefront provider plugin
```bash
wget https://github.com/spaceapegames/terraform-provider-wavefront/releases/download/v2.1.1/terraform-provi
der-wavefront_v2.1.1_darwin_amd64
```

* Creating Plugin Directory and moving provider plugin
```bash
mkdir ~/terraform.d/plugins
mv terraform-provider-wavefront_v2.1.1_darwin_amd64 terraform-provider-wavefront_v2.1.1
mv terraform-provider-wavefront_v2.1.1 ~/.terraform.d/plugins/
```

### terraform-validate.md
### terraform-variables.md
### variable-assignment.md

---

3 - Terraform Provisioners/Readme.md

3 - Terraform Provisioners/failure-behavior.md
3 - Terraform Provisioners/provisioner-types.md
---

4 - Terraform Modules & Workspaces/Readme.md

4 - Terraform Modules & Workspaces/kplabs-workspace.md
4 - Terraform Modules & Workspaces/terraform-registry.md
---

5 - Remote State Management/Readme.md

5 - Remote State Management/demofile.md
5 - Remote State Management/git-integration.md
5 - Remote State Management/state-management.md
5 - Remote State Management/tf-gitignore.md
5 - Remote State Management/tf-import.md
---

6 - Security Primer/Readme.md

6 - Security Primer/credentials.md
6 - Security Primer/multiple-providers.md
6 - Security Primer/tfstate-git.md
---

7 - Terraform Cloud & Enterprise Capabilities/Readme.md

7 - Terraform Cloud & Enterprise Capabilities/remote-backend.md
---
