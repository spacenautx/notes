```bash
// Configure the Google Cloud provider
provider "google" {
 credentials = file("CREDENTIALS_FILE.json")
 project     = "project-name"
 region      = "us-central1-a"
}
```

```bash
terraform init
```
