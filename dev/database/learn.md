- https://taksec.github.io/google-dorks-bug-bounty/
- https://github.com/ivanceras/sakila/tree/master/sqlite-sakila-db
- https://datamastery.gitlab.io/exercises/sakila-queries.html
- https://pgexercises.com/
- [normalization](https://www.databasestar.com/database-normalization/)
- [index](https://use-the-index-luke.com/)
- https://github.com/pingcap/talent-plan
- https://mode.com/sql-tutorial/
- https://sqlzoo.net/wiki/SQL_Tutorial

### tools
- http://sqlfiddle.com/
- https://architecturenotes.co/things-you-should-know-about-databases/
