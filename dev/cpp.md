# C

- [ecosystem.html](https://hackingcpp.com/cpp/tools/ecosystem.html)
- [cdescent](https://www.chiark.greenend.org.uk/~sgtatham/cdescent)
- [Learning-C](https://github.com/h0mbre/Learning-C)
- [Learn-C-By-Creating-A-Rootkit](https://h0mbre.github.io/Learn-C-By-Creating-A-Rootkit)
- [c-code-style](https://github.com/MaJerle/c-code-style)
- [howto-c](https://matt.sh/howto-c)
- [notes.html](http://cs.yale.edu/homes/aspnes/classes/223/notes.html)
- [Creating-Your-Very-Own-x64-PE-Packer-Protector-fro](https://www.codeproject.com/Articles/5317556/Creating-Your-Very-Own-x64-PE-Packer-Protector-fro)
- [efficient-programming-with-components](https://github.com/justinmeiners/efficient-programming-with-components)
- [efficient-programming-with-components](https://www.jmeiners.com/efficient-programming-with-components)
- [learn-c](https://www.learn-c.org)
- [reference](https://en.cppreference.com/w/c)
- [awesome-coding-standards](https://github.com/abougouffa/awesome-coding-standards)
- [CPP-Crash-Course](https://github.com/rougier/CPP-Crash-Course)
- [top-50-cpp-project-ideas-for-beginners-advanced](https://www.geeksforgeeks.org/top-50-cpp-project-ideas-for-beginners-advanced)
- [42_CheatSheet](https://github.com/agavrel/42_CheatSheet)
- https://jorengarenar.github.io/blog/less-known-c
- https://abstractexpr.com/2023/06/29/structures-in-c-from-basics-to-memory-alignment/

### make
- https://makefiletutorial.com/
