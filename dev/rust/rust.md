# rust

### backups
- https://github.com/tbillington/kondo

### hacking rust
- https://github.com/mytechnotalent/Hacking-Rust

### database
- https://github.com/pingcap/talent-plan

#### Primitives
Rust provides access to a wide variety of primitives. A sample includes:

#### Scalar Types
- signed integers: i8, i16, i32, i64, i128 and isize (pointer size)
- unsigned integers: u8, u16, u32, u64, u128 and usize (pointer size)
- floating point: f32, f64
- char Unicode scalar values like 'a', 'α' and '∞' (4 bytes each)
- bool either true or false
- and the unit type (), whose only possible value is an empty tuple: ()
Despite the value of a unit type being a tuple, it is not considered a compound type because it does not contain multiple values.

#### generics
- https://www.jernesto.com/articles/who_needs_haskell.html

#### Compound Types
- arrays like [1, 2, 3]
- tuples like (1, true)
Variables can always be type annotated. Numbers may additionally be annotated via a suffix or by default. Integers default to i32 and floats to f64. Note that Rust can also infer types from context.

### projects
- https://github.com/skyzh/mini-lsm
- https://github.com/vectordotdev/vector
- https://fathy.fr/carbonyl
- https://github.com/not-yet-awesome-rust/not-yet-awesome-rust


### solana
- https://github.com/solana-labs/solana/wiki/Learning-Rust

### learn
- https://learning-rust.github.io/docs/overview/

### solid-state register allocator
- https://www.mattkeeter.com/blog/2022-10-04-ssra/

### functions
- https://steveklabnik.com/writing/too-many-words-about-rusts-function-syntax
### macros
- https://geeklaunch.io/blog/fathomable-rust-macros/
- https://blog.logrocket.com/macros-in-rust-a-tutorial-with-examples/

### methods
--

### struct
--

### enum
--

### traits
- https://github.com/pretzelhammer/rust-blog/blob/master/posts/tour-of-rusts-standard-library-traits.md

### generics
--

### lifetime
- https://github.com/pretzelhammer/rust-blog/blob/master/posts/common-rust-lifetime-misconceptions.md
- https://doc.rust-lang.org/nomicon/meet-safe-and-unsafe.html

### atomics & locks
- https://marabos.nl/atomics/
- https://eventhelix.com/rust/

### concurrency & parallelism
- https://rust-lang-nursery.github.io/rust-cookbook/concurrency/parallel.html

### lua interpreter
- https://wubingzheng.github.io/build-lua-in-rust/en/

### functional programming
- https://serokell.io/blog/rust-for-haskellers
- https://www.jernesto.com/articles/who_needs_haskell.html

### modular errors
- https://sabrinajewson.org/blog/errors

### event system
- https://lunatic.solutions/
- https://github.com/lunatic-solutions/lunatic
- https://www.notapenguin.blog/posts/rust-event-systems/

### tracing
- https://burgers.io/custom-logging-in-rust-using-tracing-part-2

### plugins
- https://adventures.michaelfbryan.com/posts/plugins-in-rust/
- https://nullderef.com/blog/plugin-impl/

### notation
- https://web.mit.edu/rust-lang_v1.25/arch/amd64_ubuntu1404/share/doc/rust/html/reference/notation.html

### webassembly
- https://rustwasm.github.io/docs/book/introduction.html
- https://rustwasm.github.io/docs/wasm-pack/introduction.html
- https://rustwasm.github.io/docs/wasm-bindgen/introduction.html
- https://yew.rs/community/awesome

### performance
- https://jbecker.dev/research/on-writing-performant-rust
- https://nnethercote.github.io/perf-book/io.html

### load testing
- https://github.com/fcsonline/drill

### debug
- https://research.checkpoint.com/2023/rust-binary-analysis-feature-by-feature/

### reduce executable size
- https://lifthrasiir.github.io/rustlog/why-is-a-rust-executable-large.html

### best practices
- https://practice.rs/compound-types/tuple.html
- https://www.lurklurk.org/effective-rust/newtype.html
- https://rust-lang.github.io/api-guidelines/documentation.html

### resources
- https://github.com/Ghvstcode/Rust-Tcp
- https://github.com/tandasat/Hypervisor-101-in-Rust
- https://github.com/skerkour/black-hat-rust
- https://stackoverflow.blog/2022/07/14/how-rust-manages-memory-using-ownership-and-borrowing/
- https://hackaday.com/2022/07/10/grok-rust-in-a-flash/
- https://dev.to/deepu105/rust-easy-modern-cross-platform-command-line-tools-to-supercharge-your-terminal-4dd3
- dioxuslabs.com
- https://steveklabnik.com/writing
- https://github.com/trickster0/OffensiveRust
- https://github.com/pretzelhammer/rust-blog
- https://geeklaunch.io/blog/nothing-in-rust/
- https://thetechtrailblazer.blog/2023/03/20/writing-a-compiler-and-a-virtual-machine-in-rust/
- https://areweasyncyet.rs/
- https://cheats.rs/

### async 
- https://bertptrs.nl/2023/04/27/how-does-async-rust-work.html

# learn
- https://github.com/steadylearner/Rust-Full-Stack
- https://github.com/richardanaya/tour_of_rust
- https://github.com/stevedonovan/gentle-intro/tree/master/src
- https://learnxinyminutes.com/docs/rust/
- https://google.github.io/comprehensive-rust/
- https://github.com/rust-lang/rustlings
- https://github.com/ctjhoa/rust-learning
- https://github.com/sunface/rust-by-practice
- https://github.com/lpxxn/rust-design-pattern
- https://rust-unofficial.github.io/patterns/
- https://rauljordan.com/rust-concepts-i-wish-i-learned-earlier/
- https://github.com/kyclark/command-line-rust
- https://github.com/cognitive-engineering-lab/aquascope
- https://github.com/tweedegolf/101-rs
- https://rust-book.cs.brown.edu/ch04-01-what-is-ownership.html
- https://gist.github.com/noxasaxon/7bf5ebf930e281529161e51cd221cf8a
- https://rust-unofficial.github.io/too-many-lists/
- https://github.com/brson/rust-anthology/blob/master/master-list.md
- https://mmstick.github.io/gtkrs-tutorials/
- https://github.com/LukeMathWalker/zero-to-production
- https://without.boats/blog/patterns-and-abstractions/
- https://github.com/EmilHernvall/dnsguide
- https://github.com/alexpusch/rust-magic-patterns

### tools 
- https://github.com/svenstaro/miniserve
- https://brightprogrammer.netlify.app/post/reverse-engineering-rustlang-binaries-0x1-empty-program/
- https://github.com/asg017/sqlite-regex
- https://github.com/TaKO8Ki/awesome-alternatives-in-rust
- [tiling window manager](https://sminez.github.io/penrose/getting-started.html)
- https://tavianator.com/2023/irregex.html
