- Rust generics let you write a function or data type with multiple possible types, akin to an actor playing various roles without needing a body double each time.
-  generics in functions are just an additional parameter list enclosed by <> where instead of the arguments being used in-code, we can use them in the function definition. 
- Structs and Enums in Rust can also be generic
- The implementation of methods or traits can be generic

### resources
- https://www.worldofbs.com/rust-generics/
