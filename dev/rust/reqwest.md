```rust
//main.rs
use reqwest::Error;

async fn get_request() -> Result<(), Error> {
    let response = reqwest::get("https://www.fruityvice.com/api/fruit/apple").await?;
    println!("Status: {}", response.status());

    let body = response.text().await?;
    println!("Body:\n{}", body);

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    get_request().await?;
    Ok(())
}
```
