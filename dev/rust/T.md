The <T> syntax

the type parameter between open and close angle brackets after the name
We use the <T> syntax when declaring generic types. 
T is known as the type parameter, and it’s written between the angle brackets. 
T represents any data type, which means there is no way to know what T is at runtime
- we can’t pattern match on it, 
- can’t do any type-specific operations, 
- and can’t have different code paths for different T’s.

You can use any letter to signify a type parameter, but it’s common to use T.
