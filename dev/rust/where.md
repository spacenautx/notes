```rust 
fn notify<T, U>(t: T, u: U) -> i32
    where T: Display + Clone,
          U: Clone + Debug
{
    // your code here
}
```
In the function above, we've enforced that T must implement both Display and Clone, 
while U must implement Clone and Debug. 
The where clause makes it clear what our generics need to do, 
and it's like reading a well-organized shopping list rather than a jumbled receipt.
