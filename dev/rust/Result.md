### Result<T, E>

Result enum represents either success (Ok) or failure (Err). 
Sometimes it is important to express why an operation failed. 
In that case we must use Result enum.

Syntax:
```rust 
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

Ok(value): Indicates that the operation succeeded. Value is of type T.
Err(why): Indicates that the operation failed. Why is of type E.

Example:

```rust 
fn main() {
    let result = find_max_value(5, 5);
    match result  
        Ok(value) => println!("Maximum value is : {}", value),
        Err(error) => println!("Error Message : {}", error)
    }
}

fn find_max_value(num1: 32, num2: k2) -> Result<i32, String> {
    match num1 > num2 {
        true => Ok(num1),
        false => Err("Equal numbers".to_string())
    }
}
```
