### Option<T>

Option enum represents an optional value. 
Above all, every option has Some and contains a value, or None and does not contain a value.

Syntax:

```rust
enum Option<T> {
    Some(T),
    None,
}
```

Example:

```rust
fn main() {
    let result = find_max_value(5,0);
    match result{
        Some(_) => println!("Num1 is maximum"),
        None => println!("Num2 is maximum")
    }
}

fn find_max_value<T: PartialOrd + std::ops::Rem<Output = T>>(num1:T,num2:T) -> Option<T> {
    match num1 > num2 {
        true => Some(num1),
        false => None
    }
}
```
