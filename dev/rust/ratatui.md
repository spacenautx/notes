| 1. | state / app | i.e. model      |
| 2. | ui          | i.e. view       |
| 3. | main loop   | i.e. controller |


```bash
tree
.
├── build.rs
└── src
   ├── action.rs
   ├── components
   │  ├── app.rs
   │  └── mod.rs
   ├── config.rs
   ├── main.rs
   ├── runner.rs
   ├── tui.rs
   └── utils.rs
```
