```rust
fn fibonacci(n: u8) -> u64 {
    match n {
        0 => panic!("Zero is not a right argument to fibonacci()!"),
        1 | 2 => 1,
        _ => fibonacci(n - 1) + fibonacci(n - 2),
    }
}
```
