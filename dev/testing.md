# testing 

- https://github.com/ddosify/ddosify
- https://github.com/codesenberg/bombardier
- https://github.com/tsenart/vegeta
- https://github.com/locustio/locust
- https://github.com/atinfo/awesome-test-automation/blob/master/python-test-automation.md
- https://github.com/abhivaikar/howtheytest
- https://github.com/shekyan/slowhttptest
- https://devopsdirective.com/posts/2020/03/load-testing-f1-micro/

- [nginx tuning](https://gist.github.com/denji/8359866)

### tools
- https://github.com/mozilla/http-observatory
- https://github.com/GoogleChrome/lighthouse
