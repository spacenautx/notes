```bash
vim myAwesomeCA.cnf # <---This is filename, not part of the file
```
```bash
[ req ]
distinguished_name  = req_distinguished_name
x509_extensions     = root_ca

[ req_distinguished_name ]
countryName             = Country Name (2 letter code)
countryName_min         = 2
countryName_max         = 2
stateOrProvinceName     = State or Province Name (full name)
localityName            = Locality Name (eg, city)
0.organizationName      = Organization Name (eg, company)
organizationalUnitName  = Organizational Unit Name (eg, section)
commonName              = Common Name (eg, fully qualified host name)
commonName_max          = 64
emailAddress            = Email Address
emailAddress_max        = 64

[ root_ca ]
basicConstraints            = critical, CA:true
```
```bash
vim myAwesomeServer.ext # <---This is file name, not part of the file
```
```bash
subjectAltName = @alt_names
extendedKeyUsage = serverAuth

[alt_names]
IP.1 = 192.168.101.150
IP.2 = 192.168.100.150
```
```bash
openssl req -x509 -newkey rsa:2048 -out myAwesomeCA.cer -outform PEM -keyout myAwesomeCA.pvk -days 10000 -verbose -config myAwesomeCA.cnf -nodes -sha256 -subj "/CN=MyCompany CA"
openssl req -newkey rsa:2048 -keyout myAwesomeServer.pvk -out myAwesomeServer.req -subj /CN=localhost -sha256 -nodes
openssl x509 -req -CA myAwesomeCA.cer -CAkey myAwesomeCA.pvk -in myAwesomeServer.req -out myAwesomeServer.cer -days 10000 -extfile myAwesomeServer.ext -sha256 -set_serial 0x1111
# You can check the validity of the certificates using the command
openssl s_server -accept 15000 -cert myAwesomeServer.cer -key myAwesomeServer.pvk -CAfile myAwesomeCA.cer -WWW
```

```bash
cd /etc/nginx/sites-enabled/
vim default
```
```bash
# Nginx Config
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        listen 443 ssl;
        root /var/www/html;
index index.html index.htm index.nginx-debian.html;
server_name _;
        ssl_certificate /home/myAwesomeServer.cer; #path to cert
        ssl_certificate_key /home/myAwesomeServer.pvk; #path to key
        location / {
                # try_files $uri $uri/ =404;
                proxy_pass https://10.128.0.31:943/; #openvpn-as
        }
}
```
```bash
service nginx restart
```

For Chrome : Open settings -> Manage certificates -> Authorities (tab)
and click on import, open the myAwesomeCA.cer file and trust it,
Now just open the server IP in browser with HTTPS.
- https://your_server_ip
