```golang
go version
go mod init myapp
go build -o eliza main.go
go run main.go
```

```go
package declaration
import

fmt.Println("here")

// comment
/*
 comment
*/

var output, reminder string

var matches = []string{
    "test",
    "check"
}

var reflections = map[string]string {
    "am": "are",
    "was": "were"
}

var responses = [][]strings {
    {},
    {}
}
```

```golang
func Intro() string {
    return "test"

func Response(userInput string) string {

}
```

```golang
for {
}

for i := 0; i < len(x); i++ {

}
```


----

## resources
- https://github.com/inancgumus/learngo
- https://github.com/quii/learn-go-with-tests
- https://asecuritysite.com/golang/
- https://www.elttam.com/blog/golang-codereview/
- https://github.com/loov/lensm
- https://github.com/answerdev/answer
- https://github.com/stormsinbrewing/The-Golang-Guide
