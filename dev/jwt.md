# jwt


JWT is used for user authentication and is passed between the user and the server. The full definition of the acronym is JSON Web
Token. The way they work is to encode the user identity and sign it digitally, making it an unforgeable token that identifies the user,
and the application can later control access for the user based on their identity.

A JWT is a string composed of the header, payload, and signature. Those three parts are separated by a
.. Here is an example:

eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NjQ5ODI5OTcs
Im5iZiI6MTU2NDk4Mjk5NywianRpIjoiMGIzOTVlODQtNjFjMy00NjM3LTkwMzYtZjgyZDgy
YTllNzc5IiwiZXhwIjoxNTY0OTgzODk3LCJpZGVudGl0eSI6MywiZnJlc2giOmZhbHNlLCJ
0eXBlIjoiYWNjZXNzIn0.t6F3cnAmbUXY_PwLnnBkKD3Z6aJNvIDQ6khMJWj9xZM

The header of the JWT contains the encryption type, "alg": "HS256", and the encryption algorithm, "typ": "JWT".
We can see this clearly if we base64 decode the header string:

>>> import base64
>>> header = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9'
>>> base64.b64decode(header)

b'{"typ":"JWT","alg":"HS256"}'

The content of the payload part is arbitrary. It can be anything the developer likes. We can put in it the user ID, nickname, and so on.
When the application server receives this token, it can base64 decode it and obtain the information inside. One important thing to note
is that this information is not encrypted, therefore it is not recommended to store credit card details or passwords here:

>>> import base64
>>> payload = 'eyJpYXQiOjE1NjQ5ODI5OTcsIm5iZiI6MTU2NDk4Mjk5NywianRpI
joiMGIzOTVlODQtNjFjMy00NjM3LTkwMzYtZjgyZDgyYTllNzc5IiwiZXhwIjoxNTY0
OTgzODk3LCJpZGVudGl0eSI6MywiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0'
>>> base64.b64decode(payload + '==')

b'{"iat":1564982997,"nbf":1564982997,"jti":"0b395e84-61c3-4637-9036-f82d82a9e779","exp":1564983897,"identity":3,"fresh":false,"type":"access"}'

The secret part here is a signature created by the HS256 algorithm.
The algorithm is encrypting the encoded header and payload data with a secret key that is known by the application server only.
While anyone can modify the JWT content, that would result in a different signature, thus the data integrity is protected.

We can make use of the free service at https://jwt.io/ to have a better view of the structure and content in the JWT token:
Figure 4.1: The JWT website

With a simple structure, header.payload.secret, we have a JWT, which will be used in this project for user authentication. Based on
the user's identity, we can then apply access controls or other kinds of logic.

#### Flask-JWT-Extended
Flask-JWT-Extended is a user authentication package that provides the create_access_token function for making new access JWTs. It
also provides the jwt_required decorator for protecting the API endpoints (for checking whether users have logged in). Also, the
get_jwt_identity() function is provided to get the identity of a JWT in a protected endpoint. This allows us to know who the
authenticated users are. This is an extremely useful package for user authentication.

Before we dive into the coming exercise, let's first discuss two very important key configurations that we will be using. They are as follows:

#### SECRET_KEY
This is the key for encrypting the message and generating the signature. We recommend that you use a complex string.

#### JWT_ERROR_MESSAGE_KEY
This is the key for the error message whenever there is an error. The default value is msg, but we are setting that to the message here.

We will work on the user login function together in the next exercise. You will learn how user login works and how we can tell who the authenticated user is.


### refresh tokens
For the sake of security, we often set an expiration time for our tokens (flask-jwt-extended defaults that to 15 minutes).
Because a token will expire, we need a function to refresh it without users putting in their credentials again.
Flask-JWT-Extended provides refresh-token-related functions.
A refresh token is a long-lived token that can be used to generate new access tokens.
Please don't mix up refresh tokens and access tokens.
A refresh token can only be used to obtain a new access token; it cannot be used as an access token to access restricted endpoints.
For example, endpoints that have the jwt_required() or jwt_optional() decorators need an access token.

Here's a brief explanation of the refresh-token-related functions in Flask-JWT-Extended:
create_access_token: This function creates a new access token.
create_refresh_token: This function creates a refresh token.
jwt_refresh_token_required: This is a decorator specifying that the refresh token is required.
get_jwt_identity: This function gets the user that holds the current access token.

You will learn more about these functions in the next exercise. We will also add a fresh attribute to our token. This fresh attribute will
only be set to True when users get the token by putting in their credentials. When they simply refresh the token, they will get a token
with fresh = false. The reason for a refresh token is that we would like to avoid users having to put their credentials in again and
again. However, for some critical functions, for example, changing passwords, we will still require them to have a fresh token.


### logout
We will first declare a black_list to store all the logged-out access tokens.
Later, when the user wants to visit the access-controlled API endpoints, we will first check whether the access token is still valid using the blacklist.

- https://systemweakness.com/hacking-jwt-d29f39e202d5
- https://github.com/ticarpi/jwt_tool
- https://github.com/ticarpi/jwt_tool/wiki
- https://cendyne.dev/posts/2022-08-07-how-google-played-with-bad-cryptography.html
- https://book.hacktricks.xyz/pentesting-web/hacking-jwt-json-web-tokens
- https://www.pingidentity.com/en/resources/blog/post/jwt-security-nobody-talks-about.html
- https://developer.pingidentity.com/en/tools-for-devs/jwt-decoder.html
- https://www.f5.com/labs/articles/threat-intelligence/jwt--a-how-not-to-guide
- https://github.com/jwtk/jjwt/issues/86
- https://zerodayhacker.com/hacking-a-jwt-json-web-token-part-1/
- https://pentestmag.com/appsec-tales-viii-jwt/
- https://juba-notes.notion.site/JWT-attacks-4f62b2b641a84032bc624f8e8432345d#a90e9d01154041baa3fd39a6635c420c
