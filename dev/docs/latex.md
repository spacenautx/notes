# latex


- https://www.logicmatters.net/latex-for-logicians/
- https://castel.dev/post/lecture-notes-1/
- https://ejmastnak.github.io/tutorials/vim-latex/intro.html
- https://latex-tutorial.com/tutorials/
- https://tobi.oetiker.ch/lshort/lshort.pdf
- https://latex.knobs-dials.com/
- https://github.com/luong-komorebi/Begin-Latex-in-minutes
- https://goalkicker.com/LaTeXBook/
- https://github.com/SirCharlieMars/dotfiles/tree/master/latex_template
- https://github.com/lervag/vimtex 
- https://github.com/typst/typst



### editors
- https://github.com/TeXworks/texworks
- https://github.com/texstudio-org/texstudio
-
