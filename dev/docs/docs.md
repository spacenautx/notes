### Documentation-Driven Development

The philosophy behind Documentation-Driven Development is a simple: **from the
perspective of a user, if a feature is not documented, then it doesn't exist,
and if a feature is documented incorrectly, then it's broken.**

- Document the feature *first*. Figure out how you're going to describe the
  feature to users; if it's not documented, it doesn't exist. Documentation is
  the best way to define a feature in a user's eyes.
- Whenever possible, documentation should be reviewed by users (community or
  Spark Elite) before any development begins.
- Once documentation has been written, development should commence, and
  test-driven development is preferred.
- Unit tests should be written that test the features as described by the
  documentation. If the functionality ever comes out of alignment with the
  documentation, tests should fail.
- When a feature is being modified, it should be modified documentation-first.
- When documentation is modified, so should be the tests.
- Documentation and software should both be versioned, and versions should
  match, so someone working with old versions of software should be able to find
  the proper documentation.

So, the preferred order of operations for new features:
- Write documentation
- Get feedback on documentation
- Test-driven development (where tests align with documentation)
- Push features to staging
- Functional testing on staging, as necessary
- Deliver feature
- Publish documentation
- Increment versions

------

#### how to
- http://www.cs.cmu.edu/~mleone/how-to.html
- https://www.cse.iitb.ac.in/~ranade/communicationskills.html


#### resources
- https://documentation.divio.com/explanation/
- https://www.julian.com/guide/write/intro
- https://github.com/matheusfelipeog/beautiful-docs
- https://github.com/matheusfelipeog/beautiful-docs

-----

#### tools
- https://rust-lang.github.io/mdBook/
- https://github.com/charmbracelet/vhs
- https://github.com/Aloxaf/silicon
- https://github.com/carbon-app/carbon
