### BLOCKCHAIN EXPLORERS:
1. [BITCOIN (BTC)](https://www.blockchain.com/)
2. [BITCOIN (BTC)](https://btc.com/)
3. [ETHERIUM (ETH)](https://etherscan.io/)
4. [RIPPLE (XRP)](https://xrpcharts.ripple.com/#/graph)
5. [BITCOIN CASH (BCH)](https://explorer.bitcoin.com/bch)
6. [LITECOIN (LTC)](https://litecoinblockexplorer.net/)
7. [STELLAR (XLM)](https://steexp.com/)
8. [DASH (DASH)](https://explorer.dash.org/insight/)
9. [ZCASH (ZEC)](https://explorer.zcha.in/)
10. [MONERO (XMR)](https://localmonero.co/blocks)
----

### UNIVERSAL BLOCKCHAIN EXPLORERS:
1. [BLOCKCHAIR](https://blockchair.com/ru/)
2. [TOKENVIEV](https://tokenview.com/ru/)
3. [BLOCKCHAIN.COM](https://www.blockchain.com/ru/explorer)
4. [BLOCK EXPLORER](https://blockexplorer.com/)
5. [BITAPS](https://bitaps.com/)
6. [BLOCKCYPHER](https://live.blockcypher.com/)
----

### OTHER SERVICES:
1. [MONITORING THE ACTIVITY OF A CRYPTO WALLET](https://cryptocurrencyalerting.com/wallet-watch.html)
2. [MONITORING THE ACTIVITY OF A CRYPTO WALLET](https://www.cryptotxalert.com/)
3. [CHECKING THE COMMUNICATION BETWEEN CRYPTO WALLETS](https://learnmeabitcoin.com/tools/path/)
4. [SOURCE COMPILATION FROM BLOCKSHERLOCK](https://www.blocksherlock.com/home/blockchain-explorers)
5. [BRUTEFORCE WALLET'S PASSWORD](https://github.com/gurnec/btcrecover)
6. [MY DEMO SERVICE](http://89-108-64-202.cloudvps.regruhosting.ru/register)
7. [CRYPTO WALLET SCORING](https://bitrankverified.com/)
----

### USEFUL WEBINARS:
1. [Fundamentals of Cryptocurrency Research](https://youtu.be/_rke7phTpLQ)
2. [GraphSense Developer Webinar Series](https://www.youtube.com/channel/UCNvAmbkaLUkijhealvR1zTQ)
3. [Cryptocurrency analysis with Maltego and BitQuery](https://youtu.be/cCl7g4sll6I)
4. [Tracking Cryptocurrency Transactions in Maltego](https://youtu.be/A7XhEvAgYz4)
5. [Cryptocurrency Investigation - Blockchain basics](https://www.youtube.com/watch?v=rz9s2k-VHz0)
----

### USEFUL ARTICLES:
1. [Methods for deanonymizing bitcoin users](https://www.cryptolux.org/images/a/a1/Ccsfp614s-biryukovATS.pdf)
2. [Deanonymization of bitcoin P2P clients](https://ispranproceedings.elpub.ru/jour/article/view/455/239)
3. [Bitcoin User Privacy Assessment](https://eprint.iacr.org/2012/596.pdf)
4. [Privacy Risks in Web Payments Using Cryptocurrencies](https://arxiv.org/pdf/1708.04748.pdf)
5. [A Fistful of Bitcoins: Characteristics of Payments Among Anonymous](https://www.researchgate.net/publication/262357109_A_fistful_of_bitcoins_characterizing_payments_among_men_with_no_names)
6. [Bitcoin Address Clustering Method Based on Multiple Heuristic Conditions](https://www.researchgate.net/publication/351019556_Bitcoin_Address_Clustering_Method_Based_on_Multiple_Heuristic_Conditions)
7. [Bitcoin Transaction Graph Analysis](https://www.researchgate.net/publication/271855021_Bitcoin_Transaction_Graph_Analysis)
8. [Great reflection on Bitcoin. Crypto anonymity. User deanonymization methods.](https://telegra.ph/Bolshaya-refleksiya-na-temu-Bitcoin-Anonimnost-kripty-Metody-deanonimizacii-polzovatelej-02-13)
9. [The Best Blockchain Analysis Tools and How They Work](https://bitnovosti.com/2021/01/16/luchshie-instrumenty-dlya-blokchejn-analiza-i-kak-oni-rabotayut/)
10. [Knowing Your Coin Privacy (KYCP)](https://medium.com/samourai-wallet/knowing-your-coin-privacy-using-kycp-org-7b3b4385d8b)
----

### VISUALIZATION OF CRYPTOCURRENCY TRANSACTIONS:
1. [CRYSTAL (BTC Free)](https://explorer.crystalblockchain.com/)
2. [ETHTECTIVE (ETH Free)](https://ethtective.com/)
3. [MALTEGO (external software)](https://www.maltego.com/downloads/)
4. [BLOCKPATH](https://blockpath.com/)
5. [OXT (after registration)](https://oxt.me/)
6. [SICP (after registration)](http://sicp.ueba.su/)
7. [GRAPHSENSE (after registration)](https://graphsense.info/)
8. [CRYPTO HOUND](https://c-hound.ai/)
9. [ORBIT (open source)](https://github.com/s0md3v/Orbit)
----

### IDENTIFICATION OF CRYPTO WALLETS:
1. [CRYSTAL (5 requests per day)](https://explorer.crystalblockchain.com/)
2. [WALLET EXPLORER](https://www.walletexplorer.com/)
3. [BITINFOCHARTS](https://bitinfocharts.com/)
4. [OXT (after registration)](https://oxt.me/)
----

### CRYPTO WALLETS ABUSE:
1. [BITCOIN ABUSE](https://www.bitcoinabuse.com/)
2. [BITCOIN WHOS WHO](https://bitcoinwhoswho.com/)
3. [CHECK BITCOIN ADDRESS](https://checkbitcoinaddress.com/)
4. [SCAM ALERT](https://scam-alert.io/)
5. [Badbitcoin.org](https://badbitcoin.org/)
6. [BitcoinAIS.com](https://bitcoinais.com/)
7. [CRYPTSCAM](https://cryptscam.com/ru)
8. [INTELLIGENCE X](https://intelx.io/tools?tab=bitcoin)
----

### GOOGLE DORKS:
1. [Clearing search results at the request of a crypto wallet](https://www.google.com/search?q=3QzYvaRFY6bakFBW4YBRrzmwzTnfZcaA6E+-block&rlz=1C5CHFA_enRU937RU938&oq=3QzYvaRFY6bakFBW4YBRrzmwzTnfZcaA6E+-block&aqs=chrome..69i57.15017j0j15&sourceid=chrome&ie=UTF-8)
2. [Search for a crypto wallet on a specific site](https://www.google.com/search?q=site%3Abitcointalk.org+3QzYvaRFY6bakFBW4YBRrzmwzTnfZcaA6E&newwindow=1&rlz=1C5CHFA_enRU937RU938&sxsrf=AOaemvLLvktRcH1X8q3WGsgSPCqhpXhuow%3A1638900831518&ei=X6SvYY-CH7H5qwHW-4jYCw&ved=0ahUKEwjPrYmvpdL0AhWx_CoKHdY9ArsQ4dUDCA8&uact=5&oq=site%3Abitcointalk.org+3QzYvaRFY6bakFBW4YBRrzmwzTnfZcaA6E&gs_lcp=Cgdnd3Mtd2l6EANKBAhBGABKBAhGGABQAFgAYK8HaABwAngAgAFFiAFFkgEBMZgBAKABAqABAcABAQ&sclient=gws-wiz)
3. [Search for lists of identified wallets](https://www.google.com/search?q=site%3Ahttps%3A%2F%2Fdocs.google.com%2Fspreadsheets+Bounty+intext%3A%22%40gmail.com%22&oq=site%3Ahttps%3A%2F%2Fdocs.google.com%2Fspreadsheets+Bounty+intext%3A%22%40gmail.com%22&aqs=chrome.0.69i59j69i58.968j0j9&sourceid=chrome&ie=UTF-8)
----

### BLOCK ANALYSIS:
1. [KYCP](https://kycp.org/#/)
2. [BLOCKSTREAM](https://blockstream.info/)
----

