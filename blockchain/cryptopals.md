# cryptopals

# basics
+ 1. Convert hex to base64 (https://cendyne.dev/posts/2022-01-23-base64.html)
+ 2. Fixed XOR
+ 3. Single-byte XOR cipher
+ 4. Detect single-character XOR
+ 5. Implement repeating-key XOR
+ 6. Break repeating-key XOR
+ 7. AES in ECB mode
+ 8. Detect AES in ECB mode
-----
# block
+ 11. Implement PKCS#7 padding
+ 12. Implement CBC mode
+ 13. An ECB/CBC detection oracle
+ 14. Byte-at-a-time ECB decryption (Simple)
+ 15. ECB cut-and-paste
+ 16. Byte-at-a-time ECB decryption (Harder)
+ 17. PKCS#7 padding validation
+ 18. CBC bitflipping attacks
-----
# block & stream
+ 19. The CBC padding oracle
+ 20. Implement CTR, the stream cipher mode
+ 21. Break fixed-nonce CTR mode using substitutions
+ 22. Break fixed-nonce CTR statistically
+ 23. Implement the MT19937 Mersenne Twister RNG
+ 24. Crack an MT19937 seed
+ 25. Clone an MT19937 RNG from its output
+ 26. Create the MT19937 stream cipher and break it
-----
# stream & randomness
+ 27. Break "random access read/write" AES CTR
+ 28. CTR bitflipping
+ 29. Recover the key from CBC with IVKey
+ 30. Implement a SHA-1 keyed MAC
+ 31. Break a SHA-1 keyed MAC using length extension
+ 32. Break an MD4 keyed MAC using length extension
+ 33. Implement and break HMAC-SHA1 with an artificial timing leak
+ 34. Break HMAC-SHA1 with a slightly less artificial timing leak
-----
# diffie hellman
+ 35. Implement Diffie-Hellman
+ 36. Implement a MITM key-fixing attack on Diffie-Hellman with parameter injection
+ 37. Implement DH with negotiated groups, and break with malicious "g" parameters
+ 38. Implement Secure Remote Password (SRP)
+ 39. Break SRP with a zero key
+ 40. Offline dictionary attack on simplified SRP
+ 41. Implement RSA
+ 42. Implement an E3 RSA Broadcast attack
-----
# rsa and dsa
+ 43. Implement unpadded message recovery oracle
+ 44. Bleichenbacher's e3 RSA Attack
+ 45. DSA key recovery from nonce
+ 46. DSA nonce recovery from repeated nonce
+ 47. DSA parameter tampering
+ 48. RSA parity oracle
+ 49. Bleichenbacher's PKCS 1.5 Padding Oracle (Simple Case)
+ 50. Bleichenbacher's PKCS 1.5 Padding Oracle (Complete Case)
-----
# hashes
+ 51. CBC-MAC Message Forgery
+ 52. Hashing with CBC-MAC
+ 53. Compression Ratio Side-Channel Attacks
+ 54. Iterated Hash Function Multicollisions
+ 55. Kelsey and Schneier's Expandable Messages
+ 56. Kelsey and Kohno's Nostradamus Attack
+ 57. MD4 Collisions
+ 58. RC4 Single-Byte Biases
-----
# abstract algebra
+ 59. Diffie-Hellman Revisited: Small Subgroup Confinement
+ 60. Pollard's Method for Catching Kangaroos
+ 61. Elliptic Curve Diffie-Hellman and Invalid-Curve Attacks
+ 62. Single-Coordinate Ladders and Insecure Twists
+ 63. Duplicate-Signature Key Selection in ECDSA (and RSA)
+ 64. Key-Recovery Attacks on ECDSA with Biased Nonces
-----



### resources
- https://pthree.org/2011/04/06/convert-text-to-base-64-by-hand/
- https://accu.org/index.php/journals/1915
- https://en.wikipedia.org/wiki/Binary_number#Counting_in_binary
- https://cryptopals.com/
- https://cypher.codes/writing/cryptopals-challenge-set-2
- http://manansingh.github.io/The-CryptoGame/thecryptogame_files/c13-des-block.html
- https://www.mtholyoke.edu/courses/quenell/s2003/ma139/js/count.html
- http://pages.mtu.edu/~shene/NSF-4/Tutorial/VIG/Vig-IOC.html
- http://www.crypto-it.net/eng/theory/index-of-coincidence.html
- https://www.mygeocachingprofile.com/codebreaker.vigenerecipher.aspx
- http://practicalcryptography.com/cryptanalysis/stochastic-searching/cryptanalysis-vigenere-cipher/
- https://github.com/bberrynz/RandomPythonScripts/blob/master/xorcrack.py
- http://manansingh.github.io/The-CryptoGame/thecryptogame_files/ARTICLES/The%20DES%20Algorithm%20Illustrated.html
- http://www.cs.trincoll.edu/~crypto/historical/vigenere.html
- https://inventwithpython.com/vigenereHacker.py
- https://github.com/adeptex/rsatool
- https://github.com/ashutosh1206/Cryptography-Python
- https://github.com/FabioBaroni/awesome-exploit-development/blob/master/README.md
- https://www.electronics-tutorials.ws/binary/bin_2.html
- https://en.wikipedia.org/wiki/Category:Cryptographic_attacks
- https://asecuritysite.com/subjects/chapter81
- http://manansingh.github.io/Cryptolab-Offline/
- https://github.com/ashutosh1206/Crypton/tree/master/Block-Cipher
- https://github.com/sonickun/cryptools/blob/master/test.py
- https://github.com/reschly/cryptopals/blob/master/prob29.py
- https://github.com/ctz/cryptopals
- https://id0-rsa.pub/
- https://github.com/iN3O/cryptopals
- https://github.com/p4-team/crypto-commons
- http://practicalcryptography.com/ciphers/
- http://mslc.ctf.su/wp/tag/rsa/
- http://www.simonsingh.net/The_Black_Chamber/chamberguide.html
- http://manansingh.github.io/The-CryptoGame/thecryptogame_files/index.html
- https://github.com/ValarDragon/CTF-Crypto
- https://inventwithpython.com/hacking/chapter21.html
