# classical cipher

* encodings
### ASCII code
```shell
# Source text: The quick brown fox jumps over the lazy dog

84 104 101 32 113 117 105 99 107 32 98 114 111 119 110 32 102 111 120 32 106 117 109 112 115 32 111 118 101 114 32 116 104 101 32 108 97 122 121 32 100 111 103
```

### Base64/32/16
```
# Source text: The quick brown fox jumps over the lazy dog

b16 - 54686520717569636B2062726F776E20666F78206A756D7073206F76657220746865206C617A7920646F67
b32 - KRUGKIDROVUWG2ZAMJZG653OEBTG66BANJ2W24DTEBXXMZLSEB2GQZJANRQXU6JAMRXWO===
b64 - VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZw==
```

### Shellcode
```shell
# Source text: The quick brown fox jumps over the lazy dog

\x54\x68\x65\x7f\x71\x75\x69\x63\x6b\x7f\x62\x72\x6f\x77\x6e\x7f\x66\x6f\x78\x7f\x6a\x75\x6d\ X70\x73\x7f\x6f\x76\x65\x72\x7f\x74\x68\x65\x7f\x6c\x61\x7a\x79\x7f\x64\x6f\x67
```
### Quoted-printable
```shell
=E6= 95 =8 F =E6=8D=B7=E7=9A= 84 =E6=A3= 95 =E8= 89 =B2=E7=8B= 90 =E7=8B=B8=E8=B7 =B3=E8
=BF= 87 =E4=BA= 86 =E6= 87 = 92 =E6= 83 =B0=E7=9A= 84 =E7=8B= 97
```
### XXencode
```
Source text: The quick brown fox jumps over the lazy dog

After encoding: hJ4VZ653pOKBf647mPrRi64NjS0-eRKpkQm-jRaJm65FcNG-gMLdt64FjNkc+
```
### UUencode
```
Source text: The quick brown fox jumps over the lazy dog

After encoding: M5&AE('%U:6-K(&)R;W=N(&9O>"!J=6UP<R!O=F5R('1H92!L87IY(&1O9PH*
```

### URL
```
Source text: The quick brown fox jumps over the lazy dog

After encoding:

#!shell
%54%68%65%20%71%75%69%63%6b%20%62%72%6f%77%6e%20%66%6f%78%20%6a%75%6d% 70%73%20%6f%76%65%72%20%74%68%65%20%6c%61%7a%79%20%64%6f%67
```

### Unicode
```
Source text: The

&#x [Hex]: &#x0054;&#x0068;&#x0065;

 [Decimal]: &#00084;&#00104;&#00101;

\U [Hex]: \U0054\U0068\U0065

\U+ [Hex]: \U+0054\U+0068\U+0065
```

### Escape/Unescape
```
Source text: The

After encoding: %u0054%u0068%u0065
```

### HTML entity
```

```

### strike code
```
#!shell
  1   2   3   4   5
1 ABC/KDE
 2  F   GHIJ
 3 LMNOP
 4 QRS   T   U
 5 VWXYZ
```

### Morse Code
```
Source text: THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG

After encoding:

#!shell
- .... . / --.- ..- .. -.-. -.- / -... .-. --- .-- -. / ..-. --- -..- / .--- ..- -- .--. ... / --- ...- . .-. / - .... . / .-.. .- --. .-.-- / -.. --- --.
```

### Coded story
```

```

= Various text encryptions =
```

```

====  Replace encryption ====

### Atbash Cipher
```
ABCDEFGHIJKLMNOPQRSTUVWXYZ
ZYXWVUTSRQPONMLKJIHGFEDCBA
Clear text: the quick brown fox jumps over the lazy dog

Ciphertext: gsv jfrxp yildm ulc qfnkh levi gsv ozab wlt
```

### Caesar
```
Clear text: The quick brown fox jumps over the lazy dog

Offset: 1

Ciphertext: Uif rvjdl cspxo gpy kvnqt pwfs uif mbaz eph
```

### ROT5/13/18/47
```
rot13.

Clear text: the quick brown fox jumps over the lazy dog

Ciphertext: gur dhvpx oebja sbk whzcf bire gur ynml qbt
```

### Substitution
```
Clear text: the quick brown fox jumps over the lazy dog

Ciphertext: cei jvaql hkdtf udz yvoxr dsik cei npbw gdm
```

### vigenere
```
Ciphertext:

#!shell
pmpafxaikkitprdsikcplifhwceigixkirradfeirdgkipgigudkcekiigpwrpucikceiginasikwduearrxiiqepcceindgmieinpwdfprduppcedoikiqiasafmfddfipfgmdafmfdteiki
Decryption:


(ps: score value is smaller and more accurate)

Key: PHQGIUMEAVLNOFDXBKRCZSTJWY

Clear text:

#!shell
AGAINPIERREWASOVERTAKENBYTHEDEPRESSIONHESODREADEDFORTHREEDAYSAFTERTHEDELIVERYOFHISSPEECHATTHELODGEHELAYONASOFAATHOMERECEIVINGNOONEANDGOINGNOWHERE
```

### bacon
```

```

### Hill
```
#!shell
XUKEXWSLZJUAXUNKIGWFSOZRAWURORKXAOSLHROBXBTKCMUWDVPTFBLMKEFVWMUXTVTWUIDDJVZKBRMCWOIWYDXMLUFPVSHAGSVWUFWORCWUIDUJCNVTTBERTUNOJUZHVTWKORSVRZSVVFSQXOCMUWPYTRLGBMCYPOJCLRIYTVFCCMUWUFPOXCNMCIWMSKPXEDLYIQKDJWIWCJUMVRCJUMVRKXWURKPSEEIWZVXULEIOETOOFWKBIUXPXUGOWLFPWUSCH
```

### Pigpen password
```

```

### Polybius Square
```
Clear text: THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
Ciphertext: 442315 4145241325 1242345233 213453 2445323543 442315 31115554 143422
```

### Chardonnay password (Zigzag encryption)
```

```

### Playfair
```
#! Python
>> > Import from pycipher Playfair
 >> > Playfair ( 'CULTREABDFGHIKMNOPQSVWXYZ' ) .encipher ( 'THE QUICK BROWN FOX JUMPS the OVER THE LAZY DOG' )
 'UKDNLHTGFLWUSEPWHLISNPCGCRGAUBVZAQIV'
>> > Playfair ( 'CULTREABDFGHIKMNOPQSVWXYZ' ) .decipher ( ' UKDNLHTGFLWUSEPWHLISNPCGCRGAUBVZAQIV' )
 'THEQUICKBROWNFOXIUMPSOVERTHELAZYDOGX'
```

### Vigenère
```
#!python
>> >from pycipher import Vigenere
 >> >Vigenere( 'CULTURE' ).encipher( 'THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG' )
 'VBPJOZGMVCHQEJQRUNGGWQPPKNYINUKRXFK'
>> >Vigenere( 'CULTURE' ).decipher( ' VBPJOZGMVCHQEJQRUNGGWQPPKNYINUKRXFK' )
 'THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG'
```

### AutoKey
```
#!python
 >>>from pycipher import Autokey
 >>>Autokey( 'CULTURE').encipher( 'THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG')
 'VBPJOZGDIVEQVHYYAIICXCSNLFWWZVDPWVK'
 >>>Autokey( 'CULTURE').decipher( ' VBPJOZGDIVEQVHYYAIICXCSNLFWWZVDPWVK')
 'THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG'
```

### Beaufort
```
#!python
 >>>from pycipher import Beaufort
 >>>Beaufort( 'CULTURE').encipher( 'THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG')
 'JNHDAJCSTUFYEZOXCZICMOZHCBKARUMVRDY'
 >>>Beaufort( 'CULTURE').decipher( ' JNHDAJCSTUFYEZOXCZICMOZHCBKARUMVRDY')
 'THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG'
```

### Running Key
```

```

### Porta password
```
Clear text: THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
Ciphertext: FRW HKQRY YMFMF UAA OLWHD ALWI JPT ZXHC NGV
```

### Homophonic
```
Clear text: THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
Ciphertext (one of them): 6CZ KOVST XJ0MA EQY IOGL4 0W1J UC7 P9NB F0H
```

### Affine
```

```

### Baconian
```
#!shell
 A = aaaaa I/J = abaaa R = baaaa

B = aaaab K = abaab S = baaab

C = aaaba L = ababa T = baaba

D = aaabb M = ababb U/V = baabb

E = aabaa N = abbaa W = babaa

F = aabab O = abbab X = babab

G = aabba P = abbba Y = babba

H = aabbb Q = abbbb Z = babbb
Clear text: T H E F O X

Ciphertext: baaba aabbb aabaa aabab abbab babab
```

### ADFGX and ADFGVX passwords (ADFG/VX Cipher)
```
#!python
 >>>from pycipher import ADFGVX
 >>>a = ADFGVX( 'ph0qg64mea1yl2nofdxkr3cvs5zw7bj9uti8', 'HOWAREU')
 >>>a.encipher( 'THE QUICK BROWN FOX')
 'DXXFAFGFFXGGGFGXDVGDVGFAVFVAFVGG'
 >>>a.decipher( ' DXXFAFGFFXGGGFGXDVGDVGFAVFVAFVGG')
 'THEQUICKBROWNFOX'
```

### Double password (Bifid Cipher)
```
#!shell
5121542133 5435452521 3523311521 34
```

### Trifid
```
#!shell
231313313231132312232223223221312331331132113222132
NOONWGBXXLGHHWSKW
```

### Four-Square
```
Clear text: THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG

Ciphertext: ESZWQAFHGTDKWHRKUENYQOLMQTUNWMBPTGHQ
```

### Checkerboard
```
   QUICK
  --------------
B |KNI/JGH
R |PQRST
O | OYZUA
W |MXWVB
N |LFEDC
Replaced by a dense array:

#Clear text : THEQUICKBROWNFOX
#Ciphertext : RK BK RU OC OC BI NK BQ WK RI OQ WI BU NU OQ WU
```

### Crossddle Checkerboard
```

```

### Fractionated Morse
```
Clear text: THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
Key: MORSECODE
(Similar) Morse Code:
#! shell
 -x... .x .xx-- .-x. .-x. .x- .- .x- .-xx-.. .x .- .x---x .--x - .xx. .- .x---x-. .-xx .---x. .- --x .-- .x.. .xx---x.. .-x .x .- .xx-x... .x .xx .-. .x .-x--. .x- .--xx-. .x---x--.
```

### Bazeries
```
Clear text: THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG

Random numbers: 2333

###  Clear text matrix:

#!shell
A   FLQV
BGMRW
CHNSX
DI/JO   TY
EKPUZ
Example key matrix:

#!shell
 TWOHU
SANDR
EI/JYBC
FGKLM
PQVXZ
Clear text grouping:

#!shell
2 3 3 3 2 3 3 3 2 3 3 3
TH EQU ICK BRO WN FOX JUM PSO VE RTH ELA ZYD OG
Group plaintext reverse order:

#!shell
 HT UQE KCI ORB WN XOF MUJ OSP EV EHT ALE DYZ GO
Replace with key matrix:

#!shell
 IL XHP QEG KDS YR CKW NXG KBV PU ILD TOP FMZ AK
```

### Digrafid
```
The first square key: digrafid

The second square key: cipher

Close table:

#!shell
 1 2 3 4 5 6 7 8 9
DIGRA FDBC 1 2 3
EHJLMNOPQ 4 5 6
S TUVWXYZ # 7 8 9
                  Cfs 1
                  Igt 2
                  Pju 3
                  Hkv 4
                  Elw 5
                  Rmx 6
                  Any 7
                  Boz 8
                  Dq # 9
```

### Grandpré
```
#!shell
Clear text: THEQUICKBROWN   FO
Ciphertext: 84 27 82 41 51 66 31 36 15 71 67 73 52 34 67
```

### Beale
```

```

### Keyboard
```

```

==== Transposition encryption ====
### Curve Cipher
```

```

### Transposition
```

```

### Rail-fence
```

```

==== mechanical password  ====
### enigma
```

```

### = Code obfuscation encryption =
```

```

### Asp confusion
```

```

### Php obfuscated
```

```

### Css/js obfuscated
```

```

### VBScript.Encode
```

```

### Ppencode
```

```

### Rrencode
```

```

### Jjencode/aaencode
```

```

### JSfuck
```

```

### Jother
```

```

### Brainfuck
```shell

+++++ +++++ [->++ +++++ +++ < ] > ++++ .---. +++++ ++..+ ++ . < + +++++ +++++
[ - > ++ +++++ ++++ < ]> +++ ++++. < ++++ +++[ - > ---- --- < ] > --. < +++++ ++ [ - >
----- - < ] > ----- -----. <
```


### Classical ciphers

- [Alberti cipher](https://en.wikipedia.org/wiki/Alberti_cipher)
    | [dCode](http://www.dcode.fr/alberti-cipher)
- ✔ **[Atbash cipher](https://en.wikipedia.org/wiki/Atbash)**
    | [dCode](http://www.dcode.fr/atbash-mirror-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/atbash-cipher/)
    | [Rumkin.com](http://rumkin.com/tools/cipher/atbash.php)
- AMSCO cipher
    | [dCode](http://www.dcodme.fr/amsco-cipher)
- [Autokey cipher](https://en.wikipedia.org/wiki/Autokey_cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/autokey/)
- [Baconian cipher](https://en.wikipedia.org/wiki/Bacon%27s_cipher)
    | [dCode](http://www.dcode.fr/bacon-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/baconian/)
    | [Rumkin.com](http://rumkin.com/tools/cipher/baconian.php)
- [Jefferson disk / Bazeries cylinder](https://en.wikipedia.org/wiki/Jefferson_disk),
  [M-94](https://en.wikipedia.org/wiki/M-94)
    | [dCode](http://www.dcode.fr/bazeries-cipher)
- [Scytale cipher](https://en.wikipedia.org/wiki/Scytale)
    | [dCode](http://www.dcode.fr/scytale-cipher)
- [Monoaplhabetic substitution cipher (simple substitution)](https://en.wikipedia.org/wiki/Substitution_cipher#Simple_substitution)
    | [dCode](http://www.dcode.fr/monoalphabetic-substitution)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/simple-substitution/)
    | [Rumkin.com](http://rumkin.com/tools/cipher/substitution.php)
    - ✔ **[Affine cipher](https://en.wikipedia.org/wiki/Affine_cipher)**
        | [dCode](http://www.dcode.fr/affine-cipher)
        | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/affine/)
        | [Rumkin.com](http://rumkin.com/tools/cipher/affine.php)
    - ✔ **Caesar cipher**
        | [dCode](http://www.dcode.fr/caesar-cipher)
        | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/caesar/)
        | [Rumkin.com](http://rumkin.com/tools/cipher/caesar.php)
    - Keyboard Coordinates
        | [dCode](http://www.dcode.fr/keyboard-coordinates)
    - Keyboard Shift cipher
        | [dCode](http://www.dcode.fr/keyboard-shift-cipher)
    - ✔ **Keyed caesar cipher**
        | [Rumkin.com](http://rumkin.com/tools/cipher/caesar-keyed.php)
    - ✔ **Letter-to-number**
        | [dCode](http://www.dcode.fr/letter-number-cipher)
        | [Rumkin.com](http://rumkin.com/tools/cipher/numbers.php)
    - ✔ **[Morse Code](https://en.wikipedia.org/wiki/Morse_code)**
        | [dCode](http://www.dcode.fr/morse-code)
        | [Rumkin.com](http://rumkin.com/tools/cipher/morse.php)
        - Wabun Code
            | [dCode](http://www.dcode.fr/wabun-code)
    - Music Notes
        | [dCode](http://www.dcode.fr/music-notes)
    - ✔ **ROT cipher**
        | [dCode](http://www.dcode.fr/rot-cipher)
    - ✔ **ROT-5**
        | [dCode](http://www.dcode.fr/rot5-cipher)
        | [Web Utils](http://www.webutils.pl/ROTencode)
    - ✔ **ROT-13**
        | [dCode](http://www.dcode.fr/rot-13-cipher)
        | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/rot13/)
        | [Rumkin.com](http://rumkin.com/tools/cipher/rot13.php)
        | [Web Utils](http://www.webutils.pl/ROTencode)
    - ✔ **ROT-18**
        | [Web Utils](http://www.webutils.pl/ROTencode)
    - ✔ **ROT-47**
        | [dCode](http://www.dcode.fr/rot-47-cipher)
        | [Web Utils](http://www.webutils.pl/ROTencode)
    - Shift cipher
        | [dCode](http://www.dcode.fr/shift-cipher)
    - Trifid cipher
        | [dCode](http://www.dcode.fr/trifide-cipher)
        | [Practical Cryptograpgy](http://www.practicalcryptography.com/ciphers/classical-era/trifid/)
- [Homophonic Substitution cipher](https://en.wikipedia.org/wiki/Substitution_cipher#Homophonic_substitution)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/homophonic-substitution/)
    - [Beale ciphers](https://en.wikipedia.org/wiki/Beale_ciphers)
    - [Book cipher](https://en.wikipedia.org/wiki/Book_cipher)
        | [dCode](http://www.dcode.fr/book-cipher)
    - [Straddling checkerboard cipher](https://en.wikipedia.org/wiki/Straddling_checkerboard)
        | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/straddle-checkerboard/)
    - Trithemius Ave Maria
        | [dCode](http://www.dcode.fr/trithemius-ave-maria)

- Alphabetical Ranks Added
    | [dCode](http://www.dcode.fr/alphabetical-ranks-added)
- Bellaso cipher
    | [dCode](http://www.dcode.fr/bellaso-cipher)
- Bifid cipher
    | [dCode](http://www.dcode.fr/bifid-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/bifid/)
    | [Rumkin.com](http://rumkin.com/tools/cipher/bifid.php)
- Caesar Box cipher
    | [dCode](http://www.dcode.fr/caesar-box-cipher)
- Cardan Grille
    | [dCode](http://www.dcode.fr/cardan-grille)
- Chaocipher
    | [dCode](http://www.dcode.fr/chao-cipher)
- Chinese Code
    | [dCode](http://www.dcode.fr/chinese-code)
- Codes and Nomenclators cipher
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/codes-and-nomenclators/)
- Columnar Transposition
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/columnar-transposition/)
    | [Rumkin.com](http://rumkin.com/tools/cipher/coltrans.php)
    - Double Transposition
        | [Rumkin.com](http://rumkin.com/tools/cipher/coltrans-double.php)
        | [dCode](http://www.dcode.fr/double-transposition-cipher)
    - Übchi
        | [Rumkin.com](http://rumkin.com/tools/cipher/ubchi.php)
- Consonants/Vowels Rank cipher
    | [dCode](http://www.dcode.fr/consonants-vowels-rank-cipher)
- Deranged Alphabet cipher
    | [dCode](http://www.dcode.fr/deranged-alphabet-generator)
- Four-Square cipher
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/four-square/)
- ✔ **[Fractionated Morse cipher](https://en.wikipedia.org/wiki/Transposition_cipher#Fractionation)**
    | [dCode](http://www.dcode.fr/fractionated-morse)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/fractionated-morse/)
- Gold Bug cipher
    | [dCode](http://www.dcode.fr/gold-bug-poe)
- Hill cipher
    | [dCode](http://www.dcode.fr/hill-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/hill/)
- Ideograms cipher
    | [dCode](http://www.dcode.fr/ideograms)
- LSPK90 Clockwise
    | [dCode](http://www.dcode.fr/lspk90-cw-leet-speak-90-degrees-clockwise)
- Mexican Army cipher wheel
    | [dCode](http://www.dcode.fr/mexican-army-cipher-wheel)
- Modulo cipher
    | [dCode](http://www.dcode.fr/modulo-cipher)
- Navajo Code
    | [dCode](http://www.dcode.fr/navajo-code)
- ✔ **One Time Pad**
    | [Rumkin.com](http://rumkin.com/tools/cipher/otp.php)
- Playfair cipher
    | [dCode](http://www.dcode.fr/playfair-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/playfair/)
    | [Rumkin.com](http://rumkin.com/tools/cipher/playfair.php)
    - Two Square cipher
        [dCode](http://www.dcode.fr/two-square-cipher)
- ✔ **[Polybius square](https://en.wikipedia.org/wiki/Polybius_square)**
    | [dCode](http://www.dcode.fr/polybius-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/polybius-square/)
    - [ADFGVX cipher](https://en.wikipedia.org/wiki/ADFGVX_cipher)
        | [dCode](http://www.dcode.fr/adfgvx-cipher)
        | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/adfgvx/)
    - [ADFGX cipher](https://en.wikipedia.org/wiki/ADFGVX_cipher#Operation_of_ADFGX)
        | [dCode](http://www.dcode.fr/adfgx-cipher)
        | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/adfgx/)
    - [Nihilist cipher](https://en.wikipedia.org/wiki/Nihilist_cipher)
        | [dCode](http://www.dcode.fr/nihilist-cipher)
- Porta cipher
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/porta/)
- ✔ **Rail-fence (ZigZag) cipher**
    | [dCode](http://www.dcode.fr/rail-fence-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/rail-fence/)
    | [Rumkin.com](http://rumkin.com/tools/cipher/railfence.php)
    - Redefence cipher
        | [dCode](http://www.dcode.fr/redefence-cipher)
- Rotate
    | [Rumkin.com](http://rumkin.com/tools/cipher/rotate.php)
- Running Key cipher
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/running-key/)
- Skip
    | [Rumkin.com](http://rumkin.com/tools/cipher/skip.php)
- ✔ **[URL encode](https://en.wikipedia.org/wiki/Percent-encoding)**
    | [Web Utils](http://www.webutils.pl/URLencode)
- Vigenére cipher
    | [dCode](http://www.dcode.fr/vigenere-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/vigenere-gronsfeld-and-autokey/)
    | [Rumkin.com](http://rumkin.com/tools/cipher/vigenere.php)
    - Beaufort cipher
        | [dCode](http://www.dcode.fr/beaufort-cipher)
        | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/classical-era/beaufort/)
        - Variant Beaufort cipher
            [dCode](http://www.dcode.fr/variant-beaufort-cipher)
    - Gronsfeld cipher
        | [dCode](http://www.dcode.fr/gronsfeld-cipher)
        | [Practical Cryptograhy](http://www.practicalcryptography.com/ciphers/classical-era/vigenere-gronsfeld-and-autokey/)
        | [Rumkin.com](http://rumkin.com/tools/cipher/gronsfeld.php)
    - Keyed Vigenére cipher
        | [Rumkin.com](http://rumkin.com/tools/cipher/vigenere-keyed.php)
    - Multiplication Vigenére cipher
        | [dCode](http://www.dcode.fr/multiplication-vigenere-cipher)
    - ✔ **[Trithemius cipher](https://en.wikipedia.org/wiki/Tabula_recta#Trithemius_cipher)**
        | [dCode](http://www.dcode.fr/trithemius-cipher)
    - Vernam cipher
        | [dCode](http://www.dcode.fr/vernam-cipher)
    - [Vigenére autokey cipher (autoclave cipher)](https://en.wikipedia.org/wiki/Autokey_cipher)
        | [dCode](http://www.dcode.fr/autoclave-cipher)
        | [Rumkin.com](http://rumkin.com/tools/cipher/vigenere-autokey.php)
- XOR cipher
    | [dCode](http://www.dcode.fr/xor-cipher)

### Mechanical ciphers

- Enigma cipher
    | [dCode](http://www.dcode.fr/enigma-machine-cipher)
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/mechanical-era/enigma/)
- Lorenz cipher
    | [Practical Cryptography](http://www.practicalcryptography.com/ciphers/mechanical-era/lorenz/)
- [M-209](https://en.wikipedia.org/wiki/M-209)
- [SIGABA](https://en.wikipedia.org/wiki/SIGABA)

### Binary-to-text encoding

- [Ascii85](https://en.wikipedia.org/wiki/Ascii85)
    | [dCode](http://www.dcode.fr/ascii-85-encoding)
    | [Web Utils](http://www.webutils.pl/Ascii85)
- [Base16](https://en.wikipedia.org/wiki/Base64)
- [Base32](https://en.wikipedia.org/wiki/Base32)
- [Base58](https://en.wikipedia.org/wiki/Base58)
- ✔ **[Base64](https://en.wikipedia.org/wiki/Base64)**
    | [dCode](http://www.dcode.fr/base-64-coding)
    | [Rumkin.com](http://rumkin.com/tools/cipher/base64.php)
    | [Wikibooks](https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64)
- [Base91](https://en.wikipedia.org/wiki/Binary-to-text_encoding#Encoding_standards)
    | [dCode](http://www.dcode.fr/base-91-encoding)
- [UUEncode](https://en.wikipedia.org/wiki/Uuencoding)
    | [dCode](http://www.dcode.fr/uu-encoding)
    | [Web Utils](http://www.webutils.pl/UUencode)
- [XXEncode](https://en.wikipedia.org/wiki/Xxencoding)
    | [Web Utils](http://www.webutils.pl/XXencode)

### Bases

- Base 2 <-> Base 10
    | [dCode](http://www.dcode.fr/binary-code)
- Base 26 <-> Base 10
    | [dCode](http://www.dcode.fr/base-26-cipher)

### Barcodes

- [Code 39](https://en.wikipedia.org/wiki/Code_39)
    | [dCode](http://www.dcode.fr/barcode-39)
- [Code 128](https://en.wikipedia.org/wiki/Code_128)
    | [dCode](http://www.dcode.fr/barcode-128)

### Symmetric key encryption

- [RC2](https://en.wikipedia.org/wiki/RC2)
- ✔ **[RC4](https://en.wikipedia.org/wiki/RC4)**
    - ✔ **[RC4A](https://en.wikipedia.org/wiki/RC4#RC4A)**
    - [VMPC (Variably Modified Permutation Composition)](https://en.wikipedia.org/wiki/Variably_Modified_Permutation_Composition)
    - [RC4+](https://en.wikipedia.org/wiki/RC4#RC4.2B)
    - [Spritz](https://en.wikipedia.org/wiki/RC4#Spritz)
- [RC5](https://en.wikipedia.org/wiki/RC5)
- [RC6](https://en.wikipedia.org/wiki/RC6)

### Public key

- [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem))

### Hash functions

- [HAVAL](https://en.wikipedia.org/wiki/HAVAL)
    | [Web Utils](http://www.webutils.pl/Haval-Hash-Calculator)
- [MD2](https://en.wikipedia.org/wiki/MD2_(cryptography))
- [MD4](https://en.wikipedia.org/wiki/MD4)
- [MD5](https://en.wikipedia.org/wiki/MD5)
    | [dCode](http://www.dcode.fr/md5-hash)
    | [Web Utils](http://www.webutils.pl/MD5_Calculator)
- [MD6](https://en.wikipedia.org/wiki/MD6)
- [RIPEMD](https://en.wikipedia.org/wiki/RIPEMD)
    | [Web Utils](http://www.webutils.pl/RIPEMD_Calculator)
- [SHA-0](https://en.wikipedia.org/wiki/SHA-1#SHA-0)
- [SHA-1](https://en.wikipedia.org/wiki/SHA-1)
    | [dCode](http://www.dcode.fr/sha1-hash)
    | [Web Utils](http://www.webutils.pl/SHA1_Calculator)
- [SHA-2](https://en.wikipedia.org/wiki/SHA-2)
- [SHA-3](https://en.wikipedia.org/wiki/SHA-3)
- [Tiger](https://en.wikipedia.org/wiki/Tiger_(cryptography))
    | [Web Utils](http://www.webutils.pl/Tiger-Hash-Calculator)

### Graphical

- Acéré cipher
    | [dCode](http://www.dcode.fr/acere-cipher)
- Music Sheet cipher
    | [dCode](http://www.dcode.fr/music-sheet-cipher)
- Pigpen cipher
    | [dCode](http://www.dcode.fr/pigpen-cipher)
- Templars cipher
    | [dCode](http://www.dcode.fr/templars-cipher)

### Algoriothms

- Benford's Law
    | [dCode](http://www.dcode.fr/benford-law)
- Frequency Analysis
    | [dCode](http://www.dcode.fr/frequency-analysis)
    | [Rumkin.com](http://rumkin.com/tools/cipher/frequency.php)
- Index of Coincidence
    | [dCode](http://www.dcode.fr/index-coincidence)
- Middle squares method for pseurorandom number generation

### Utilities

- Characters Type
    | [dCode](http://www.dcode.fr/characters-type)
- Charset Converter
    | [Web Utils](http://www.webutils.pl/Charset-Converter)
- Crypt
    | [Web Utils](http://www.webutils.pl/Crypt-Hash-Calculator)
- Crypto Solver
    | [Rumkin.com](http://rumkin.com/tools/cipher/cryptogram-solver.php)
- Cryptogram Assistant
    | [Rumkin.com](http://rumkin.com/tools/cipher/cryptogram.php)
- Isogram Generator
    | [dCode](http://www.dcode.fr/isogram)
- Text Manipulator
    | [Rumkin.com](http://rumkin.com/tools/cipher/manipulate.php)
- Word Desubstitution
    | [dCode](http://www.dcode.fr/word-desubstitution)

### Lists

- [dCode](http://www.dcode.fr/tools-list#cryptography)
- [Practical Cryptography](http://www.practicalcryptography.com/ciphers/)
- [Rumkin.com](http://rumkin.com/tools/cipher/)
- [Web Utils](http://www.webutils.pl/)
- [Binary-to-text encoding on Wikipedia](https://en.wikipedia.org/wiki/Binary-to-text_encoding)

### Libraries

- [Caesar Salad](https://github.com/schnittstabil/caesar-salad) (JavaScript)
- [cipher-machines](https://github.com/keltia/cipher-machines) (Go)
- [CryptoJS](https://code.google.com/archive/p/crypto-js/) (JavaScript)

### resources
- https://www.tuicool.com/articles/2E3INnm
- https://www.cnblogs.com/ssooking/p/6559935.html
