# arch linux

- grep
```bash
#Extract emails from file
grep -E -o "\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}\b" file.txt

#Extract valid IP addresses
grep -E -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" file.txt

#Extract passwords
grep -i "pwd\|passw" file.txt

#Extract users
grep -i "user\|invalid\|authentication\|login" file.txt

## Extract hashes
#Extract md5 hashes ({32}), sha1 ({40}), sha256({64}), sha512({128})
egrep -oE '(^|[^a-fA-F0-9])[a-fA-F0-9]{32}([^a-fA-F0-9]|$)' *.txt | egrep -o '[a-fA-F0-9]{32}' > md5-hashes.txt
#Extract valid MySQL-Old hashes
grep -e "[0-7][0-9a-f]{7}[0-7][0-9a-f]{7}" *.txt > mysql-old-hashes.txt
#Extract blowfish hashes
grep -e "$2a\$\08\$(.){75}" *.txt > blowfish-hashes.txt
#Extract Joomla hashes
egrep -o "([0-9a-zA-Z]{32}):(w{16,32})" *.txt > joomla.txt
#Extract VBulletin hashes
egrep -o "([0-9a-zA-Z]{32}):(S{3,32})" *.txt > vbulletin.txt
#Extraxt phpBB3-MD5
egrep -o '$H$S{31}' *.txt > phpBB3-md5.txt
#Extract Wordpress-MD5
egrep -o '$P$S{31}' *.txt > wordpress-md5.txt
#Extract Drupal 7
egrep -o '$S$S{52}' *.txt > drupal-7.txt
#Extract old Unix-md5
egrep -o '$1$w{8}S{22}' *.txt > md5-unix-old.txt
#Extract md5-apr1
egrep -o '$apr1$w{8}S{22}' *.txt > md5-apr1.txt
#Extract sha512crypt, SHA512(Unix)
egrep -o '$6$w{8}S{86}' *.txt > sha512crypt.txt

#Extract e-mails from text files
grep -E -o "\b[a-zA-Z0-9.#?$*_-]+@[a-zA-Z0-9.#?$*_-]+.[a-zA-Z0-9.-]+\b" *.txt > e-mails.txt

#Extract HTTP URLs from text files
grep http | grep -shoP 'http.*?[" >]' *.txt > http-urls.txt
#For extracting HTTPS, FTP and other URL format use
grep -E '(((https|ftp|gopher)|mailto)[.:][^ >"	]*|www.[-a-z0-9.]+)[^ .,;	>">):]' *.txt > urls.txt
#Note: if grep returns "Binary file (standard input) matches" use the following approaches # tr '[\000-\011\013-\037177-377]' '.' < *.log | grep -E "Your_Regex" OR # cat -v *.log | egrep -o "Your_Regex"

#Extract Floating point numbers
grep -E -o "^[-+]?[0-9]*.?[0-9]+([eE][-+]?[0-9]+)?$" *.txt > floats.txt

## Extract credit card data
#Visa
grep -E -o "4[0-9]{3}[ -]?[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{4}" *.txt > visa.txt
#MasterCard
grep -E -o "5[0-9]{3}[ -]?[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{4}" *.txt > mastercard.txt
#American Express
grep -E -o "\b3[47][0-9]{13}\b" *.txt > american-express.txt
#Diners Club
grep -E -o "\b3(?:0[0-5]|[68][0-9])[0-9]{11}\b" *.txt > diners.txt
#Discover
grep -E -o "6011[ -]?[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{4}" *.txt > discover.txt
#JCB
grep -E -o "\b(?:2131|1800|35d{3})d{11}\b" *.txt > jcb.txt
#AMEX
grep -E -o "3[47][0-9]{2}[ -]?[0-9]{6}[ -]?[0-9]{5}" *.txt > amex.txt

## Extract IDs
#Extract Social Security Number (SSN)
grep -E -o "[0-9]{3}[ -]?[0-9]{2}[ -]?[0-9]{4}" *.txt > ssn.txt
#Extract Indiana Driver License Number
grep -E -o "[0-9]{4}[ -]?[0-9]{2}[ -]?[0-9]{4}" *.txt > indiana-dln.txt
#Extract US Passport Cards
grep -E -o "C0[0-9]{7}" *.txt > us-pass-card.txt
#Extract US Passport Number
grep -E -o "[23][0-9]{8}" *.txt > us-pass-num.txt
#Extract US Phone Numberss
grep -Po 'd{3}[s-_]?d{3}[s-_]?d{4}' *.txt > us-phones.txt
#Extract ISBN Numbers
egrep -a -o "\bISBN(?:-1[03])?:? (?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]\b" *.txt > isbn.txt

```
- objdump
- logs
- gnupg
- audit
- tcpdump
- ssh
```
- https://book.hacktricks.xyz/tunneling-and-port-forwarding
- http://pentestmonkey.net/cheat-sheet/ssh-cheat-sheet
- https://arniealmighty.wordpress.com/2009/08/04/the-ssh-tunnelling-cheatsheat/
- https://cheatsheet.dennyzhang.com/cheatsheet-ssh-a4
- https://computingforgeeks.com/ssh-cheatsheet-for-sysadmins/
- https://help.ubuntu.com/community/SSH/OpenSSH/PortForwarding
- https://lzone.de/cheat-sheet/SSH
- https://medium.com/maverislabs/proxyjump-the-ssh-option-you-probably-never-heard-of-2d7e41d43464
- https://neverendingsecurity.wordpress.com/2015/04/07/ssh-cheatsheet/
- https://tournasdimitrios1.wordpress.com/2011/01/23/the-ultimate-ssh-tricks-manual/
- https://www.everythingcli.org/ssh-tunnelling-for-fun-and-profit-autossh/
- https://posts.specterops.io/offensive-security-guide-to-ssh-tunnels-and-proxies-b525cbd4d4c6
```
- bash
```
https://github.com/LeCoupa/awesome-cheatsheets/blob/master/languages/bash.sh
```

- https://github.com/jaburns/ngincat
```

# On the receiving end running,
nc -l -p 1234 > out.file
# will begin listening on port 1234.

# On the sending end running,
nc -w 3 [destination] 1234 < out.file
# will connect to the receiver and begin sending file.

# For faster transfers if both sender and receiver has some basic *nix tools installed, you can compress the file during sending process,
# On the receiving end,
nc -l -p 1234 | uncompress -c | tar xvfp -

# On the sending end,
tar cfp - /some/dir | compress -c | nc -w 3 [destination] 1234

# A much cooler but less useful use of netcat is, it can transfer an image of the whole hard drive over the wire using a command called dd.
# On the sender end run,
dd if=/dev/hda3 | gzip -9 | nc -l 3333

# On the receiver end,
nc [destination] 3333 | pv -b > hdImage.img.gz

```
* openssl
- https://www.freecodecamp.org/news/openssl-command-cheatsheet-b441be1e8c4a/
* shadow

```
*** 1. Username : It is your login name.
*** 2. Password : It is your encrypted password. The password should be minimum 8-12 characters long including special characters, digits, lower case alphabetic and more. Usually password format is set to $id$salt$hashed, The $id is the algorithm used On GNU/Linux as follows:
**** $1$ is MD5
**** $2a$ is Blowfish
**** $2y$ is Blowfish
**** $5$ is SHA-256
**** $6$ is SHA-512
*** 3. Last password change (lastchanged) : Days since Jan 1, 1970 that password was last changed
*** 4. Minimum : The minimum number of days required between password changes i.e. the number of days left before the user is allowed to change his/her password
*** 5. Maximum : The maximum number of days the password is valid (after that user is forced to change his/her password)
*** 6. Warn : The number of days before password is to expire that user is warned that his/her password must be changed
*** 7. Inactive : The number of days after password expires that account is disabled
*** 8. Expire : days since Jan 1, 1970 that account is disabled i.e. an absolute date specifying when the login may no longer be used.
```
* passwd
```
| 1 | Username       | It is used when user logs in. It should be between 1 and 32 characters in length.                                                                      |
| 2 | Password       | An x character indicates that encrypted password is stored in /etc/shadow file.                                                                        |
| 3 | User ID (UID)  | UID 0 is root, UIDs 1-99 are for other predefined accounts. UID 100-999 are for administrative and system accounts/groups.                             |
| 4 | Group ID (GID) | The primary group ID (stored in /etc/group file)                                                                                                       |
| 5 | User ID Info   | The comment field. It allow you to add extra information about the users such as user’s full name, phone number etc. This field use by finger command. |
| 6 | Home directory | The absolute path to the directory the user will be in when they log in. If this directory does not exists then users directory becomes /              |
| 7 | Command/shell  | The absolute path of a command or shell (/bin/bash). Typically, this is a shell. Please note that it does not have to be a shell.                      |
```
- images

** webp to png
```
dwebp file.webp -o file.png
```
- audio
- mpd
- ncmpcpp
- video
- blender
- common
```
aliases
arch_install
audit
awk_sed
base64
bless
blueman
calcurse
cheat.sh
cli_servers
compton
cfdisk
feh
ffmpeg
gimp
glibc
gnupg
gparted
grep
grub
head
hexdump
i3wm
imagemagick
iptables
libreoffice
ltrace
ncmpcpp
newsboat
qutebrowser
rainbowstream
ranger
rofi
weechat
youtube-dl
zathura
taskwarrior
znc
tmux
tor
vpn
```
- maintenance
```
journalctl --vacuum-size=500M
paccache -rvk 1
inxi -Fxz
ps aux, ps axjf, ps -au phonexicum, ps aux --sort pmem
df -hT, du -hd 1, fdisk -l, free -h
ulimit - get and set user limits in linux
netstat, htop, top, dstat, free, vmstat, ncdu, iftop, hethogs
lsblk, lscpu, lshw, lsus, lspci, lsusb
lsof -nPi - list opened files - very flexible utility, can be used for network analylsis

# SEToolkit (v3.5.1 - 2013) - a collection of scripts for performance analysis and gives advice on performance improvement (it has been a standard in system performance monitoring for the Solaris platform over the last 10 years)
# inotify or man fanotify (can block actions) - Linux kernel subsystem that acts to extend filesystems to notice changes to the filesystem, and report those changes to applications.


# To get a list of last installed packages, you can run:
grep -i installed /var/log/pacman.log
# To get a list of last upgraded packages, you can run:
grep -i upgraded /var/log/pacman.log
# To get a list of last installed or upgraded packages, you can run:
grep -iE 'installed|upgraded' /var/log/pacman.log
#ist of top processes ordered by RAM and CPU
ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head
# disk usage
du -h --max-depth=1 | sort -hr | head -n 15 2>&1 < /dev/null
du -hs * | sort -rh | head -5
find -type f -exec du -Sh {} + | sort -rh | head -n 5

```

```
ssh
monit
mount
netcat
netstat
network
nohup
objdump
openssl
pacman
permissions
regex
rsync
```
- audit
```
logs
strace
strings
sudo
watch

```
