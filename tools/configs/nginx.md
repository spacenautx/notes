# nginx

## set up

```bash
  directive --> server_name domain.com;
  context  (scope) http(main), server, events, location, types
  location blocks
    prferential prefix '^~ /'
    prefix match       '  /'
    exact match        '= /'
    regex match        '~ /' case sensitive
    regex match        '~* /' case insensitive
  virtualhost
  variables
    $host $uri $args
  if condition
  rewrites --> takes more processing
  and
  redirects
  try files
  named locations
  logs
    access
    error
  inheritance
    standard directive
       root, try
    array directive
       access logs
    action directive
       redirect, return

  php-fm fastcgi
  worker_process, events

  buffers & timeouts

  dynamic modules

  headers & expires
  compressed responses
  fastcgi cache
  http2
  ssl
  server push

  security
    TLS
    ssl_protocols
    ssl_ciphers
    sshl_dhparam
    HSTS
    ssl_cache
  rate-limiting, traffic shaping

  server_tokens off #hide version
  add_header X-Frame-Options "SAMEORIGIN" # prevent clickjacking
  add_header X-XSS-Protection "1; mode=block" #prevent XSS


  let's encrypt with certbot


  reverse proxy & load balancing
       upstream

  geoIP
```

## config

```bash
user       www www;  ## Default: nobody
worker_processes  5;  ## Default: 1
error_log  logs/error.log;
pid        logs/nginx.pid;
worker_rlimit_nofile 8192;

events {
  worker_connections  4096;  ## Default: 1024
}

http {
  include    conf/mime.types;
  include    /etc/nginx/proxy.conf;
  include    /etc/nginx/fastcgi.conf;
  index    index.html index.htm index.php;

  default_type application/octet-stream;
  log_format   main '$remote_addr - $remote_user [$time_local]  $status '
    '"$request" $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';
  access_log   logs/access.log  main;
  sendfile     on;
  tcp_nopush   on;
  server_names_hash_bucket_size 128; # this seems to be required for some vhosts

  server { # php/fastcgi
    listen       80;
    server_name  domain1.com www.domain1.com;
    access_log   logs/domain1.access.log  main;
    root         html;

    location ~ \.php$ {
      fastcgi_pass   127.0.0.1:1025;
    }
  }

  server { # simple reverse-proxy
    listen       80;
    server_name  domain2.com www.domain2.com;
    access_log   logs/domain2.access.log  main;

    # serve static files
    location ~ ^/(images|javascript|js|css|flash|media|static)/  {
      root    /var/www/virtual/big.server.com/htdocs;
      expires 30d;
    }

    # pass requests for dynamic content to rails/turbogears/zope, et al
    location / {
      proxy_pass      http://127.0.0.1:8080;
    }
  }

  upstream big_server_com {
    server 127.0.0.3:8000 weight=5;
    server 127.0.0.3:8001 weight=5;
    server 192.168.0.1:8000;
    server 192.168.0.1:8001;
  }

  server { # simple load balancing
    listen          80;
    server_name     big.server.com;
    access_log      logs/big.server.access.log main;

    location / {
      proxy_pass      http://big_server_com;
    }
  }
}
```


| Official                                                                                 |
|------------------------------------------------------------------------------------------|
| [beginner's](http://nginx.org/en/docs/beginners_guide.html)                              |
| [server_names](http://nginx.org/en/docs/http/server_names.html)                          |
| [examples](https://www.nginx.com/resources/wiki/start/topics/examples/full/)             |
| [pre-canned](https://www.nginx.com/resources/wiki/start/#pre-canned-configurations)      |
| [pitfalls](https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/) |


| Resources                                                                                            |
|------------------------------------------------------------------------------------------------------|
| [misconfigurations](https://blog.detectify.com/2020/11/10/common-nginx-misconfigurations/)           |
| [nginx admins handbook](https://github.com/trimstray/nginx-admins-handbook)                          |
| [server configs](https://github.com/h5bp/server-configs-nginx)                                       |
| [nginxconfig.io](https://github.com/digitalocean/nginxconfig.io)                                     |
| [gui configure](https://www.digitalocean.com/community/tools/nginx)                                  |
| [nginx-ui](https://github.com/schenkd/nginx-ui)                                                      |
| [ngxtop](https://github.com/lebinh/ngxtop)                                                           |
| [nginx resources](https://github.com/fcambus/nginx-resources)                                        |
| [nginx-tutorials](https://github.com/openresty/nginx-tutorials)                                      |
| [gixy](https://github.com/yandex/gixy)                                                               |
| [nginxpwner](https://github.com/stark0de/nginxpwner)                                                 |
| [tuning](https://github.com/denji/nginx-tuning)                                                      |
| [3rd party](https://github.com/agile6v/awesome-nginx)                                                |
| [arch-linux](https://wiki.archlinux.org/title/nginx)                                                 |
| [reverse-proxy](https://github.com/linuxserver/reverse-proxy-confs)                                  |
| [25 security practices](https://www.cyberciti.biz/tips/linux-unix-bsd-nginx-webserver-security.html) |
| [uwsgi](https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html)                                      |
| [virtual hosts](https://gist.github.com/soheilhy/8b94347ff8336d971ad0)                               |
