# tmux


``` bash
set-option -g default-shell /usr/bin/zsh
set -g history-file ~/.tmux_history
set -g mouse on
set-option -g status-interval 1
#set -g history-limit 50000
set-option -g history-limit 50000
set-option -g status-justify "left"
#set -g status-justify centre
set -g status-left-length 75
set -g status-right-length 175
#set -g status-left-length 85
#set -g default-terminal "xterm-256color"
set -g default-terminal "tmux-256color"
#set-option -ga terminal-overrides ",xterm*:Tc"
set -sg escape-time 0
#set-option -ga terminal-overrides ",xterm-terminator:Tc"
#set-option -ga terminal-overrides ",*256*:Tc"
#keybind
unbind C-b
set -g prefix C-a
bind C-a send-prefix
bind - split-window -v -c "#{pane_current_path}"
bind \\ split-window -h -c "#{pane_current_path}"

#maximize & restore panes
# Pane resizing {{{
bind-key -r Z resize-pane -Z
bind-key -r H resize-pane -L "5"
bind-key -r L resize-pane -R "5"
bind-key -r J resize-pane -D "5"
bind-key -r K resize-pane -U "5"
# }}}
bind z run tmux-url-select.pl
set-option -g allow-rename off
#set -g visual-activity on

## Navigate panes like Vim.
setw -g mode-keys vi
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind-key -r C-l select-window -t :+
bind-key -r C-h select-window -t :-
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'V' send -X select-line
bind-key -T copy-mode-vi 'r' send -X rectangle-toggle
bind-key -T copy-mode-vi 'y' send -X copy-pipe-and-cancel "xclip -in -selection clipboard"
bind-key P run "xsel -o | tmux load-buffer - ; tmux paste-buffer"
setw -g xterm-keys on

# Smart pane switching with awareness of Vim splits.
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
bind-key -n 'C-\' if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
bind-key -T copy-mode-vi C-h select-pane -L
bind-key -T copy-mode-vi C-j select-pane -D
bind-key -T copy-mode-vi C-k select-pane -U
bind-key -T copy-mode-vi C-l select-pane -R
bind-key -T copy-mode-vi 'C-\' select-pane -l

### set some pretty colors
#set -g mode-style pane-active-border-fg colour235 #base01
#set -g pane-active-border-style fg=red

set -g pane-border-style fg=green
set -g base-index 0
setw -g pane-base-index 0
#set -g status-bg default #set for transparent background
set-option -g set-titles on
#set-option -g set-titles-string "#W"

set-option -g set-titles-string '#T #{pane_current_path}' # window number,program name,active (or not)
#set-option -g set-titles-string " #W #F #T #{pane_current_command}"
#  modes
setw -g clock-mode-colour colour5
#setw -g mode-attr bold
#setw -g mode-fg colour255
#setw -g mode-bg colour33
#set -g mode-style fg=yellow,bg=colour45,blink,bold
set -g mode-style fg=black,bg=colour45,bold

# PANES
set -g pane-border-style bg=default
set -g pane-border-style fg=colour236
set -g pane-active-border-style bg=default
set -g pane-active-border-style fg=colour237
set -g display-panes-colour black
set -g display-panes-active-colour brightblack

# statusbar
#set -g status-position bottom
#set -g status-justify left
set -g status-bg default
set -g status-fg colour137
#set -g status-attr dim
#set -g status-left ''

set-window-option -g window-status-style bg=default
setw -g window-status-current-style fg=colour1
setw -g window-status-current-style bg=default
#setw -g window-status-current-bg colour236
setw -g window-status-current-style bold
set -g status-left '#[fg=colour60]'
setw -g window-status-current-format '#[fg=colour46]#I: #[fg=colour226]#W#[fg=colour226]#F'
setw -g window-status-format '#I: #[fg=colour60]#[fg=colour60]#W#[fg=colour60]#F'
#set -g status-left '#[fg=colour33] #{s|/home/untitled|  |:pane_current_path} '
set -g status-right '#{?client_prefix,#[fg=colour51]prefix-ON,}  #[fg=colour202] #(tail -1 ~/.zsh_history | cut -d ";" -f2- | cut -f 1 -d " ") #[fg=colour33] #{s|/home/untitled|  |:pane_current_path} #[fg=colour109]#(~/tools/histor.sh) #[fg=colour63]#S:#I.#P #[fg=colour46]%d %b %Y#[fg=colour33] %l:%M %p #(date -u | awk '{print $4}')'

#setw -g window-status-bell-style bold
#setw -g window-status-bell-style fg=colour255
#setw -g window-status-bell-style bg=colour1

#+----------+
#+ Messages +
#+---------+
set -g message-style fg=colour196
set -g message-style bg=colour236
set -g message-command-style fg=cyan
set -g message-command-style bg=colour237


set-option -g status-position bottom
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'JindrichPilar/tmux-timekeeper'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-sidebar'
set -g @resurrect-strategy-vim 'session'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-net-speed'

run '~/.tmux/plugins/tpm/tpm'
```


### resources
#-- https://www.hamvocke.com/blog/remote-pair-programming-with-tmux/
#-- https://www.howtoforge.com/sharing-terminal-sessions-with-tmux-and-screen#sharing-between-two-different-accounts-with-tmux
