- [Import](#Import)
- [Collection](#Collection)
- [Import](#Import)
- [Create](#Creating a Database)
- [Import](#Import)
- [quick](#quick)

# Import
```python
import pymongo
from pymongo import MongoClient
```

# Connection
```python
HOST = "192.168.1.16"
PORT = 27017
myclient = MongoClient(HOST, PORT)

CON_STR = "mongodb+srv://USERNAME:PASSWORD@cluster1.abcde.mongodb.net/?retryWrites=true&w=majority"
myclient = MonogoClient(CON_STR)
```

# Creating a Database
*Important:* In MongoDB, a database is not created until it gets content!
``` python
print(myclient.list_database_names())

dblist = myclient.list_database_names()
if "mydatabase" in dblist:
    print("The database exists.")
# connect to database
mydb = myclient["database_name"]
```

# Creating a Collection
*Important:* In MongoDB, a collection is not created until it gets content!
```python
print(mydb.list_collection_names())

collist = mydb.list_collection_names()
if "customers" in collist:
    print("The collection exists.")
mycol = mydb["customers"]
```

# Insert Into Collection
```python
mydict = { "name": "John", "address": "Highway 37" }
x = mycol.insert_one(mydict)
```

# Return the _id Field
```python
mydict = { "name": "John", "address": "Highway 37" }

x = mycol.insert_one(mydict)

mydict = { "name": "Peter", "address": "Lowstreet 27" }

x = mycol.insert_one(mydict)

print(x.inserted_id)
```

# Insert Multiple Documents
```python
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabase"]
mycol = mydb["customers"]

mylist = [
  { "name": "Amy", "address": "Apple st 652"},
  { "name": "Hannah", "address": "Mountain 21"},
  { "name": "Michael", "address": "Valley 345"},
  { "name": "Sandy", "address": "Ocean blvd 2"},
  { "name": "Betty", "address": "Green Grass 1"},
  { "name": "Richard", "address": "Sky st 331"},
  { "name": "Susan", "address": "One way 98"},
  { "name": "Vicky", "address": "Yellow Garden 2"},
  { "name": "Ben", "address": "Park Lane 38"},
  { "name": "William", "address": "Central st 954"},
  { "name": "Chuck", "address": "Main Road 989"},
  { "name": "Viola", "address": "Sideway 1633"}
]

x = mycol.insert_many(mylist)

# print list of the _id values of the inserted documents:
print(x.inserted_ids)
```

# Insert Multiple Documents, with Specified IDs
```python
mylist = [
  { "_id": 1, "name": "John", "address": "Highway 37"},
  { "_id": 2, "name": "Peter", "address": "Lowstreet 27"},
  { "_id": 3, "name": "Amy", "address": "Apple st 652"},
  { "_id": 4, "name": "Hannah", "address": "Mountain 21"},
  { "_id": 5, "name": "Michael", "address": "Valley 345"},
  { "_id": 6, "name": "Sandy", "address": "Ocean blvd 2"},
  { "_id": 7, "name": "Betty", "address": "Green Grass 1"},
  { "_id": 8, "name": "Richard", "address": "Sky st 331"},
  { "_id": 9, "name": "Susan", "address": "One way 98"},
  { "_id": 10, "name": "Vicky", "address": "Yellow Garden 2"},
  { "_id": 11, "name": "Ben", "address": "Park Lane 38"},
  { "_id": 12, "name": "William", "address": "Central st 954"},
  { "_id": 13, "name": "Chuck", "address": "Main Road 989"},
  { "_id": 14, "name": "Viola", "address": "Sideway 1633"}
]

x = mycol.insert_many(mylist)
print(x.inserted_ids)
```

# Find One
```python
# Find the first document in the customers collection:
x = mycol.find_one()
print(x)
```

# Find All
```python
# Return all documents in the "customers" collection, and print each document:
for x in mycol.find():
    print(x)
```

# Return Only Some Fields
```python
# Return only the names and addresses, not the _ids:
for x in mycol.find({},{ "_id": 0, "name": 1, "address": 1 }):
    print(x)
```

#
```python
# This example will exclude "address" from the result:
for x in mycol.find({},{ "address": 0 }):
    print(x)
```

#
```python
# You get an error if you specify both 0 and 1 values in the same object (except if one of the fields is the _id field):
for x in mycol.find({},{ "name": 1, "address": 0 }):
    print(x)
```

# Filter the Result
```python
myquery = { "address": "Park Lane 38" }

mydoc = mycol.find(myquery)

for x in mydoc:
    print(x)
```

# Advanced Query
```python
myquery = { "address": { "$gt": "S" } }

mydoc = mycol.find(myquery)

for x in mydoc:
  print(x)
```

# Filter With Regular Expressions
```python
myquery = { "address": { "$regex": "^S" } }

mydoc = mycol.find(myquery)

for x in mydoc:
  print(x)
```

# Sort the Result
```python
mydoc = mycol.find().sort("name")

for x in mydoc:
  print(x)
```

# Sort Descending
```python
mydoc = mycol.find().sort("name", -1)

for x in mydoc:
  print(x)
```

# Delete Document
```python
myquery = { "address": "Mountain 21" }

mycol.delete_one(myquery)
```

# Delete Many Documents
```python
myquery = { "address": {"$regex": "^S"} }

x = mycol.delete_many(myquery)

print(x.deleted_count, " documents deleted.")
```

# Delete All Documents in a Collection
```python
x = mycol.delete_many({})

print(x.deleted_count, " documents deleted.")
```

# Delete Collection
``` python
mycol.drop()
```

# Update Collection
```python
myquery = { "address": "Valley 345" }
newvalues = { "$set": { "address": "Canyon 123" } }

mycol.update_one(myquery, newvalues)

# print "customers" after the update:
for x in mycol.find():
  print(x)
```

# Update Many
```python
myquery = { "address": { "$regex": "^S" } }
newvalues = { "$set": { "name": "Minnie" } }

x = mycol.update_many(myquery, newvalues)

print(x.modified_count, "documents updated.")
```

# Limit the result to only return 5 documents
```python
myresult = mycol.find().limit(5)

# print the result:
for x in myresult:
  print(x)
```

# quick
```python
from pymongo import MongoClient

HOST = "192.168.1.16"
PORT = 27017
myclient = MongoClient(HOST, PORT)
mydb = myclient["database_name"]
mycol = mydb["customers"]
for x in mycol.find():
    print(x)
```
