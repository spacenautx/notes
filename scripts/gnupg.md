# gnupg

## On Debian/Ubuntu based distros
```bash
sudo apt-get install gpg
```

## List public keys you have stored (ours and other people's keys)
```bash
gpg --list-keys
```

## List private keys (generally only our own)
```bash
gpg --list-secret-keys
```

## CREATE A NEW PRIVATE KEY
```bash
gpg --gen-key
```

## EXPORT A PRIVATE KEY

```bash
# Find the ID of your key first
# The ID is the hexadecimal number
gpg --list-secret-keys

# This is your private key keep it secret!
# Replace XXXXXXXX with your hexadecimal key ID
gpg --export-secret-keys --armor XXXXXXXX > ./my-priv-gpg-key.asc

# Omitting the --armor flag will give you binary output instead of ASCII
# which would result in a slightly smaller file but the ASCII
# formatted (armored) can be printed physically, is human readable,
# and transfered digitally easier.
# Both formats can be imported back in to GPG later
```

## DELETE A KEY
```bash
# Private keys
# Use the ID listed with --list-secret-keys
gpg --delete-secret-keys XXXXXXXX

# Public keys
# Use the ID listed with --list-keys
gpg --delete-keys XXXXXXXX
```

## IMPORT A KEY
If you need to import a key you can use the following command. This is useful if
you are on a new computer or a fresh install and you need to import your key
from a backup. You can import a public or a private key this way. Typically the
only time you will be importing a private key is when restoring a backup of your
own private key. The most common case for importing a public key is to store
someone else's public key in order to send them a private message or to verify a
signature of theirs.
```bash
# This works the same for binary or ASCII (armored) versions of keys
# This is also the same for private and public keys
gpg --import ./my-priv-gpg-key.asc

# You can also directly import a key from a server
# For example, import the DevDungeon/NanoDano public GPG key from MIT
gpg --keyserver pgp.mit.edu  --recv C104CDF0EDA54C82
```

## GENERATE REVOCATION KEY
```
gpg --output my_revocation_certificate.asc --gen-revoke <key>
```

## PUSH YOUR PUBLIC KEY TO KEY SERVER
```bash
# You may want or need to publish your public key somewhere where everyone can
# find it. For example, to push code to the Maven central repository, you must
# sign your code with a GPG key that has a public signature somewhere like the MIT
# public key server. You can push your public key using the --send-keys flag.

# There are many public key servers out there, not just MIT
# Replace XXXXXXXX with your key id from --list-keys
gpg --keyserver hkp://pgp.mit.edu --send-keys XXXXXXXX
```

## ENCRYPT WITH A PASSPHRASE (SYMMETRIC)
```bash
gpg --symmetric message.txt
# Prompts you for a passphrase
# Creates message.txt.gpg (binary)

gpg --armor --symmetric message.txt
# Same, but ASCII format output instead of binary
# Creates message.txt.asc (ASCII)

## Specify the encryption algorithm
gpg --symmetric --cipher-algo AES256

## Get the list of cipher algorithms
gpg --version
# E.g. 3DES, BLOWFISH, AES256, TWOFISH

## Specify output file
gpg --output message.txt.gpg --symmeteric message.txt

## Encrypt and sign (all in the single output file)
gpg --sign --symmetric message.txt
```

## ENCRYPT FOR A SINGLE RECIPIENT (ASYMMETRIC)
You can encrypt a message for a single specific recipient. You do this by
encrypting asymmetrically with your private key and the recipients public key.
By doing this, only the recipient's private key will decrypt the message. You
will need the recipient's public key in order to do this. They can share their
public key with you directly, or you can search public key servers.

## Import NanoDano's public GPG key
```bash
gpg --keyserver pgp.mit.edu --search-keys user@hostname.com

# It will print a list of matching results.
# Enter the number next to the one you want to import
# After it's imported you can verify the public key is stored with
gpg --list-keys

# This will prompt and ask the recipient's email address
# and you will have to enter the ID or email
gpg --encrypt message.txt

# or specify the recipient as the -r argument
gpg  --recipient nanodano@devdungeon.com --encrypt message.txt
# Encrypted output will be in message.txt.gpg

# Encrypt and store in ASCII format (message.txt.asc)
# Both binary and ASCII versions decrypt the same
gpg  --armor --recipient nanodano@devdungeon.com --encrypt message.txt
```

## Encrypt and sign at the same time
```
gpg --encrypt --sign --recipient nanodano@devdungeon.com message.txt
```

## Specify output file
```
gpg --output message.txt.gpg
```

## DECRYPT A MESSAGE
```bash
# You can decrypt messages with the -d or --decrypt option. It will
# automatically determine if it is symmetrically encrypted with something like AES
# and it needs a passphrase, or if it was encrypted asymmetrically with something
# like RSA and it needs to look for a private key. The recipient will be detected
# automatically and GPG will search locally to see if the private key is stored.
# If it is, it will ask for the passphrase and then print out the decrypted
# message to the console. If it is a symmetric encryption it will prompt you for
# the passphrase, and then print the message.

# Decrypt and print message to screen
# Will automatically verify signature if there was one
# Will automatically detect if symmetric or asymmetric
# Will automatically use the appropriate private key if available
# Will automatically prompt for passphrase if symmetric
gpg -d message.txt.gpg

# Decrypt and put output in decrypted.txt
gpg --decrypt message.txt.gpg > decrypted.txt
```


## SIGN A MESSAGE
```bash

# It is not used to encrypt and secure the information and guarantees no
# confidentiality. It only proves integity and authenticity of the message. You
# can create a binary signature or a plain-text signature.

gpg --clearsign message.txt
# Outputs message.txt.asc in plain text, suitable
# for pasting in an email or posting online

gpg --sign message.txt
# Outputs message.txt.gpg a binary file

# Both of these can be verified with --decrypt
# but they are not _actually_ encrypted.
gpg --decrypt message.txt.gpg
gpg --decrypt message.txt.asc
# This will print out the message along with the signature info
```

## ENCRYPT AND SIGN
```bash
# You can encrypy-and-sign a message in one step. This will attach the
# signatures to the encrypted file. With these two encrypt-and-sign methods below,
# the signature is included in the encrypted .gpg/.asc file that is output. When
# someone decrypts them, the signature will be checked automatically.

## Symmetric encrypt with signature
gpg --sign --symmetric message.txt

## Asymmetric encrypt with signature
gpg --sign --encrypt --recipient user@hostname.com message.txt
```

## VERIFY SIGNATURES
```bash
# If a signature is included in the encrypted file, GPG will automatically
# output the verification of the signature when it decrypts the message. You don't
# have to take any special action to verify the signature you just use the normal
# -d or --decrypt option, it happens by default.

## When you decrypt the message it will verify the signature
gpg --decrypt message.txt.asc
You can also manually verify a signature for things like a clear signed file with the --verify option like this:

## Verify a signed message that included a signature
gpg --verify message.txt.asc
## Verify and extract original document
gpg --output message.txt message.txt.asc
```

## DETACHED SIGNATURES
```bash
# So far all the signatures we have seen have been embedded in to the file with
# the message. It is also possible to separate the signature file from the message
# file. This is called a detached signature.

## Create a separate signature file
gpg --detach-sign message.txt
# Will create message.txt.sig

## This verify will automatically check the signature against a file named "message.txt"
gpg --verify message.txt.sig

## Specify the file to check it against
gpg --verify some_signature.sig ./message.txt
```

## Verify the Other Person’s Identity
```bash
gpg --fingerprint your_email@address.com

# Sign Their Key

gpg --sign-key email@example.com
```

## Best practices
```bash 
# check you have at least a OpenPGPv4 key (v3 is not considered as robust)

gpg --export-options export-minimal --export '<fingerprint>' | gpg --list-packets | grep version

#check you have at least a DSA-2 or (preferably) RSA key with a length of 4K (or
#more).

gpg --export-options export-minimal --export '<fingerprint>' |  gpg --list-packets | grep -A2 '^:public key packet:$'| grep algo

#check the output : RSA corresponds to algo 1, DSA to algo 17, ECDSA corresponds
#to algo 19, ECC to algo 18

#in case you have RSA or DSA, check you have at least a key with a length of at
#least 4K (RSA) or at least 1K (DSA-2). ECDSA and ECC have different kind of keys

gpg --export-options export-minimal --export '<fingerprint>' | gpg --list-packets | grep -A2 'public key'| grep 'pkey\[0\]:'

#auto signatures should not use MD5. check that with the following command.

gpg --export-options export-minimal --export '<fingerprint>' | gpg --list-packets | grep -A 2 signature| grep 'digest algo'

#auto signatures should not use SHA-1. check that with the following command.

gpg --export-options export-minimal --export '<fingerprint>' | gpg --list-packets | grep -A 2 signature| grep 'digest algo 2,'

#If any of previous commands results contains 'digest algo 1' or 'digest algo 2',
#you should regenerate your key after adding `cert-digest-algo SHA512` in
#`~/.gnupg/gpg.conf` :

echo "cert-digest-algo SHA512" >> ~/.gnupg/gpg.conf
```

---

## resources
- https://gnupg.org/documentation/index.html
- https://riseup.net/en/security/message-security/openpgp/gpg-best-practices
- https://www.techrepublic.com/blog/it-security/10-tips-for-effective-use-of-openpgp-with-gnupg/
