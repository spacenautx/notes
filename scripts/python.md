# python

* TODO generator, map, filter, reduce, decorators, oop, flask login, async
* philosophy

```text
import this
# The Zen of Python, by Tim Peters
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
```

* functions()

| abs()         | delattr()   | hash()       | memoryview() | set()          |
| all()         | dict()      | help()       | min()        | setattr()      |
| any()         | dir()       | hex()        | next()       | slice()        |
| ascii()       | divmod()    | id()         | object()     | sorted()       |
| bin()         | enumerate() | input()      | oct()        | staticmethod() |
| bool()        | eval()      | int()        | open()       | str()          |
| breakpoint()  | exec()      | isinstance() | ord()        | sum()          |
| bytearray()   | filter()    | issubclass() | pow()        | super()        |
| bytes()       | float()     | iter()       | print()      | tuple()        |
| callable()    | format()    | len()        | property()   | type()         |
| chr()         | frozenset() | list()       | range()      | vars()         |
| classmethod() | getattr()   | locals()     | repr()       | zip()          |
| compile()     | globals()   | map()        | reversed()   | __import__()   |
| complex()     | hasattr()   | max()        | round()      |                |

* chr, ord

```python

# ord() function takes string argument of a single Unicode character
# and return its integer Unicode code point value.
# Let’s look at some examples of using ord() function.

x = ord('A')
print(x)
print(ord('ć'))
print(ord('ç'))
print(ord('$'))

# Output:
65
263
231
36

# chr() function takes integer argument and
# return the string representing a character at that code point.

y = chr(65)
print(y)
print(chr(123))
print(chr(36))

# Output:
A
{
$
ć

# Since chr() function takes an integer argument and converts it to character, there is a valid range for the input.
# The valid range for the argument is from 0 through 1,114,111 (0x10FFFF in hexadecimal format).
# ValueError will be raised if the input integer is outside that range.

# using ord() and chr() function together to confirm that they are exactly opposite of another one.

print(chr(ord('ć')))
print(ord(chr(65)))

Output:
ć
65

```

* map, filter, reduce, any, all
* lambda
*short function defintion*
```python
# lambda functions are just a short-hand way or writing
# simple
(lambda x, y, z: x + y + z)(1, 2, 3)

(lambda x, y, z=3: x + y + z)(1, 2)

(lambda x, y, z=3: x + y + z)(1, y=2)

(lambda *args: sum(args))(1,2,3)

(lambda **kwargs: sum(kwargs.values()))(one=1, two=2, three=3)

(lambda x, *, y=0, z=0: x + y + z)(1, y=2, z=3)

square_root2 = lambda x: x**0.5

# Declare a list of tuples
scores = [('John', 95), ('Danny', 98), ('Aaron', 90), ('Leo', 94)]
# Sort using the default order of the tuples
sorted(scores, reverse=True)
[('Leo', 94), ('John', 95), ('Danny', 98), ('Aaron', 90)]
# Sort using the scores
sorted(scores, key=lambda x: x[1], reverse=True)
[('Danny', 98), ('John', 95), ('Leo', 94), ('Aaron', 90)]
# Another use of lambda is to pass a small function as an argument.
pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
# Sort pairs by text key.
pairs.sort(key=lambda pair: pair[1])

```

* itertools
```python

```

* argparser
```python

import argparse

parser = argparse.ArgumentParser(description="Send file attachment to mail")

fromaddr = 'club.h4ck@gmail.com'
toaddr = ""
subject = "Files attached"
body = "Check the attached files"
filepath = ""

parser.add_argument('-s', '--Sender', type=str, default=fromaddr, help="Sender Email")
parser.add_argument('-r', '--Recepient', type=str, required=True, help="Recepient Email")
parser.add_argument('-t', '--Title', type=str, default=subject, help="Subject of the Email")
parser.add_argument('-b', '--Body', type=str, default=body, help="Body text of the Email")
# parser.add_argument('-a', '--Attachment', type=str, default=filepath, help="File path to attach")
parser.add_argument('-f', '--File', metavar='path', type=str, help='Path to the file to attach')

args = parser.parse_args()

print(args.Sender, args.Body, args.Recepient, args.File)
```

* comprehension
```python
# Create a list to be used in comprehensions
numbers = [1, 2, 3, -3, -2, -1]

# Create a new list of these numbers' squares
[x*x for x in numbers]
[1, 4, 9, 9, 4, 1]

# Create a new dictionary of these numbers' exponentiation
{x: pow(10, x) for x in numbers}
{1: 10, 2: 100, 3: 1000, -3: 0.001, -2: 0.01, -1: 0.1}

# Create a set of these numbers' absolutes
{abs(x) for x in numbers}
{1, 2, 3}

# dict
keys = ('a', 'b', 'c')
values = [True, 100, 'John Doe']
my_dict = {k: v for k, v in zip(keys, values)}
print(my_dict)
# Or like this:
my_dict2 = dict(zip(keys, values))
assert my_dict2 == my_dict

# search
print(bool(any([needlers.endswith(e) for e in ('ly', 'ed', 'ing', 'ers')])))

# Getting files of particular type from directory
files = [f for f in os.listdir(s_pdb_dir) if f.endswith(".txt")]

# if else
special = [x * 2 if x % 2 == 0 else x / 2 for x in range(1, 11)]

# 3x3
list1, list2, list3 = [list(t) for t in zip(*sorted(zip(list1, list2, list3)))]

# nested comprehension
[[print(val) for val in row ] for row in maze]

```

* f'strings

```python
# strings
book = 'text book'
p_nos = 23
print(f'this {book} has {p_nos} pages')
print(f'4 * 4 is {4 * 4}')

# bases
bases = {
       "b": "bin",
       "o": "oct",
       "x": "hex",
       "X": "HEX",
       "d": "decimal"
}

for n in range(1, 21):
    for base, desc in bases.items():
        print(f"{n:5{base}}", end=' ')
    print()

# rounding decimals
num = 4.123956

print(f'num rounded to 2 decimal palces = {num:.2f}')

# https://dev.to/miguendes/73-examples-to-help-you-master-python-s-f-strings-ln5?utm_source=dormosheio&utm_campaign=dormosheio
# https://kapeli.com/cheat_sheets/Python_Format_Strings.docset/Contents/Resources/Documents/index
```

* generators
*** simple

```python
def abc_generator():
    yield "a"
    yield "b"
    yield "c"


abc_gen = abc_generator()
print("Type of abc_gen:", type(abc_gen))
for letter in abc_gen:
    print(letter)

Type of abc_gen: <class 'generator'>
a
b
c

```

*** coroutines

```python
  def coroutine():
      print('My coroutine')
      while True:
          val = yield
          print('Got', val)
  >>> co = coroutine()
  >>> next(co)
  My coroutine
  >>> co.send(1)
  Got 1
  >>> co.send(2)
  Got 2
  >>> co.send(3)
  Got 3

#################################
  import time

  def coroutine():
      for x in [1, 2, 3]:
          print("resumed coroutine")
          time.sleep(0.5)  # to represent some computation
          yield x

      print("destroyed coroutine")


  def caller():
      context = coroutine()
      for x in context:
          print("suspended coroutine")
          time.sleep(0.25) # to represent some computation

  caller()

```

*** pipline

```python
def producer(consumer):
    print("Producer ready")
    while True:
        val = yield
        consumer.send(val * val)

def consumer():
    print("Consumer ready")
    while True:
        val = yield
        print('Consumer got', val)

>>> cons = consumer()
>>> prod = producer(cons)
>>> next(prod)
Producer ready
>>> next(cons)
Consumer ready
>>> prod.send(1)
Consumer got 1
>>> prod.send(2)
Consumer got 4
>>> prod.send(3)
Consumer got 9

# with coroutines, data can be sent to multiple destinations
def producer(consumers):
    print("Producer ready")
    try:
        while True:
            val = yield
            for consumer in consumers:
                consumer.send(val * val)
    except GeneratorExit :
        for consumer in consumers:
            consumer.close()
def consumer(name, low, high):
    print("%s ready" % name)
    try:
        while True:
            val = yield
            if low < val < high:
                print('%s got' % name, val)
    except GeneratorExit :
        print("%s closed" % name)

>>> con1 = consumer('Consumer 1', 00, 10)
>>> con2 = consumer('Consumer 2', 10, 20)
>>> prod = producer([con1, con2])
>>> next(prod)
Producer ready
>>> next(con1)
Consumer 1 ready
>>> next(con2)
Consumer 2 ready
>>> prod.send(1)
Consumer 1 got 1
>>> prod.send(2)
Consumer 1 got 4
>>> prod.send(3)
Consumer 1 got 9
>>> prod.send(4)
Consumer 2 got 16
>>> prod.close()
Consumer 1 closed
Consumer 2 closed
```

*** context manager

```python
from contextlib import contextmanager

@contextmanager
def my_context():
    print('Entering to my context')
    yield
    print('Exiting my context')

def do_stuff():
    with my_context():
        print('Doing stuff')

    print('Doing some stuff outside my context')

do_stuff()
```

*** decorator
* exceptions
* datetime

```python

```

* assert

```python

```

* zip

```python
"""The zip function aggregates items from different iterables,
"such as lists, tuples or sets, and returns an iterator."""

matrix = [[1, 2, 3], [1, 2, 3]]
# get the transpose of the above matrix
matrix_T = [list(i) for i in zip(*matrix)]
```

* subprocess.run

** example 1

```python
import subprocess
subprocess.check_output(['ls', '-l'])
```

** example 2

```python
from subprocess import check_output as co

cmd = [ x.strip().split(' ') for x in open('cmd.list', 'r') if x[:1] != '#' ]
print(cmd)

result = [ co(x).decode('utf-8') for x in cmd ]
print(result)
```

** example 3

```python
def bash(cmd):
     from subprocess import PIPE, Popen
     p = Popen(cmd, shell=True, stdout=PIPE, stdin=PIPE, stderr=PIPE)
     output, err = p.communicate()
     print(output.decode('utf-8'))

bash('ls -l')
bash('sqlite3 "$HOME/ctftime/backend/wup.db" "SELECT ctf FROM ctfs"')
```

* regular-expression
```text
re.match() : Determine if the RE matches at the beginning of the string.
re.search() : Scan through a string, looking for any location where this RE matches.
re.findall() : Find all substrings where the RE matches, and returns them as a list.
re.finditer() : Find all substrings where the RE matches, and returns them as an iterator.
re.compile(): For performance if same RE is used multiple times.
```

```python
needle = 'needlers'
# On-the-fly Regular expression in Python
print(bool(re.search(r'(?:ly|ed|ing|ers)$', needle)))

# Compiled Regular expression in Python
comp = re.compile(r'(?:ly|ed|ing|ers)$')
print(bool(comp.search(needle)))

pattern = r'(?i)(\w+)\.(jpeg|jpg|png|gif|tif|svg)$'
```
*email*
```python
pattern = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

str_true = ('test@mail.com',)

str_false = ('testmail.com', '@testmail.com', 'test@mailcom')

for t in str_true:
    assert(bool(re.match(pattern, t)) == True), '%s is not True' %t
for f in str_false:
    assert(bool(re.match(pattern, f)) == False), '%s is not False' %f
```
- *file extension*
- *username*
- *url*
- *negative numbers*
- *numbers*
- *dates*
- *12-HOUR format*
- *24-HOUR format*
- *HTML tags*
- *IpV4*
- *IpV6*
- *MAC*
[[https://nbviewer.jupyter.org/github/rasbt/python_reference/blob/master/tutorials/useful_regex.ipynb#Sections][link]]
* multiprocessing

** example 1

```python
#!/usr/bin/env python
from multiprocessing.pool import ThreadPool
from time import time as timer
import requests

urls = ["http://www.google.com",
        "http://www.apple.com",
        "http://www.microsoft.com",
        "http://www.amazon.com",
        "http://www.facebook.com"]

def fetch_url(url):
    try:
        response = requests.get(url)
        return url, response.text, None
    except Exception as e:
        return url, None, e

start = timer()
data = []
results = ThreadPool(20).imap_unordered(fetch_url, urls)
for url, html, error in results:
    if error is None:
        data.append(html)
        print("%r fetched in %ss" % (url, timer() - start))
    else:
        print("error fetching %r: %s" % (url, error))
print("Elapsed Time: %s" % (timer() - start,))
```

** example 2

```python
import multiprocessing as mp
import random
import string

random.seed(123)

# Define an output queue
output = mp.Queue()

# define a example function
def rand_string(length, output):
    """ Generates a random string of numbers, lower- and uppercase chars. """
    rand_str = ''.join(random.choice(
                        string.ascii_lowercase
                        + string.ascii_uppercase
                        + string.digits)
                   for i in range(length))
    output.put(rand_str)

# Setup a list of processes that we want to run
processes = [mp.Process(target=rand_string, args=(5, output)) for x in range(4)]

# Run processes
for p in processes:
    p.start()

# Exit the completed processes
for p in processes:
    p.join()

# Get process results from the output queue
results = [output.get() for p in processes]

print(results)

def cube(x):
    return x**3

pool = mp.Pool(processes=4)
results = [pool.apply(cube, args=(x,)) for x in range(1,7)]
print(results)
[1, 8, 27, 64, 125, 216]

pool = mp.Pool(processes=4)
results = pool.map(cube, range(1,7))
print(results)
[1, 8, 27, 64, 125, 216]

# The Pool.map and Pool.apply will lock the main program until all processes are finished,
# which is quite useful if we want to obtain results in a particular order for certain applications.
# In contrast, the async variants will submit all processes at once and retrieve the results
# as soon as they are finished. One more difference is that we need to use the get method after
# the apply_async() call in order to obtain the return values of the finished processes.

pool = mp.Pool(processes=4)
results = [pool.apply_async(cube, args=(x,)) for x in range(1,7)]
output = [p.get() for p in results]
print(output)

```

* asyncio
+ https://chsasank.com/digging-deep-into-async-await-python.html
```python
import asyncio

async def square(x):
    print('Square', x)
    await asyncio.sleep(1)
    print('End square', x)
    return x * x
# Create event loop
loop = asyncio.get_event_loop()
# Run async function and wait for completion
results = loop.run_until_complete(square(1))
print(results)
# Close the loop
loop.close()
results = loop.run_until_complete(asyncio.gather(
square(1),
square(2),
square(3)
))
print(results)
Square 2
Square 1
Square 3
End square 2
End square 1
End square 3
[1, 4, 9]

# results may be needed as soon as they are available.
async def when_done(tasks):
    for res in asyncio.as_completed(tasks):
    print('Result:', await res)

loop = asyncio.get_event_loop()
loop.run_until_complete(when_done([
                                 square(1),
                                 square(2),
                                 square(3)
                                 ]))
# result:
Square 2
Square 3
Square 1
End square 3
Result: 9
End square 1
Result: 1
End square 2
Result: 4

# async coroutines can call other async coroutine functions with the await keyword:
async def compute_square(x):
    await asyncio.sleep(1)
    return x * x

async def square(x):
    print('Square', x)
    res = await compute_square(x)
    print('End square', x)
    return res

```

* requests

```python
import requests
url     = "https://www.archlinux.org/packages/?page=1"
headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36'}
headers = {
            'User-Agent': 'User Agent 1.0',
            'From': 'test@try.in'
          }
cookies = {'session': '51f9bc3808a83bc19a3c5be6beecc82c'}
req     = requests.get(url, headers=headers, cookies=cookies)
data    = req.text
```

** tor requests
```python
import requests
session = requests.session()
headers = {
            'User-Agent': 'User Agent 1.0',
            'From': 'test@try.in'
          }
session.proxies = {'http':  'socks5h://localhost:9050',
                   'https': 'socks5h://localhost:9050'}
print(session.get('http://httpbin.org/ip', headers=headers).text) # prints {"origin": "67.205.146.164" }
print(session.get('http://nzxj65x32vh2fkhk.onion/all').text)
```

* BeautifulSoup

```python
from bs4 import BeautifulSoup

data = []
soup = BeautifulSoup(request.text)

# select by class

# select by id
soup.find("div", {"id": "articlebody"})

# table
table = soup.find("table", attrs={"class": "lineItemsTable"})
table_body = table.find("tbody")
rows = table_body.find_all("tr")
for row in rows:
    cols = row.find_all("td")
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols if ele])  # Get rid of empty values
```

* pandas
```text
Loading Some Example Data
Renaming Columns
Converting Column Names to Lowercase
Renaming Particular Columns
Applying Computations Rows-wise
Changing Values in a Column
Adding a New Column
Applying Functions to Multiple Columns
Missing Values aka NaNs
Counting Rows with NaNs
Selecting NaN Rows
Selecting non-NaN Rows
Filling NaN Rows
Appending Rows to a DataFrame
Sorting and Reindexing DataFrames
Updating Columns
Chaining Conditions - Using Bitwise Operators
Column Types
Printing Column Types
Selecting by Column Type
Converting Column Types
If-tests

```
[pandas](https://nbviewer.jupyter.org/github/rasbt/python_reference/blob/master/tutorials/things_in_pandas.ipynb)
* sqlite3
*** Guide
*datatypes*
```TEXT
INTEGER: A signed integer up to 8 bytes depending on the magnitude of the value.
REAL: An 8-byte floating point value.
TEXT: A text string, typically UTF-8 encoded (depending on the database encoding).
BLOB: A blob of data (binary large object) for storing binary data.
NULL: A NULL value, represents missing data or an empty cell.
```

*** Create

*create database and table*
```python
import sqlite3

def createTable(db_name , query):
    try:
        sqliteConnection = sqlite3.connect(db_name)
        # query = '''CREATE TABLE ctf_urls (
                                    # id INTEGER PRIMARY KEY,
                                    # name TEXT NOT NULL,
                                    # url TEXT NOT NULL,
                                    # email text NOT NULL UNIQUE,
                                    # joining_date datetime,
                                    # salary REAL NOT NULL);'''
        cursor = sqliteConnection.cursor()
        print("Successfully Connected to SQLite", db_name)

        cursor.execute(query)
        sqliteConnection.commit()
        print("SQLite table created")

        cursor.close()

    except sqlite3.Error as error:
        print("Error while creating a sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("sqlite connection is closed")

```

*** drop
```python
def dropTable(db_name , table):
    try:
        sqliteConnection = sqlite3.connect(database)
        query = "DROP TABLE " + table +";"
        cursor = sqliteConnection.cursor()
        print("Successfully Connected to SQLite", db_name)
        cursor.execute(query)
        sqliteConnection.commit()
        print("SQLite table deleted")

        cursor.close()

    except sqlite3.Error as error:
        print("Error while dropping a sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("sqlite connection is closed")

```

*** Insert

```python
import sqlite3

def insertVaribleIntoTable(id, name, email, joinDate, salary):
    try:
        sqliteConnection = sqlite3.connect('database.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sqlite_insert_with_param = """INSERT INTO SqliteDb_developers
                          (id, name, email, joining_date, salary)
                          VALUES (?, ?, ?, ?, ?);"""

        data_tuple = (id, name, email, joinDate, salary)
        cursor.execute(sqlite_insert_with_param, data_tuple)
        sqliteConnection.commit()
        print("Python Variables inserted successfully into SqliteDb_developers table")

        cursor.close()

    except sqlite3.Error as error:
        print("Failed to insert Python variable into sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("The SQLite connection is closed")

insertVaribleIntoTable(2, 'Joe', 'joe@pynative.com', '2019-05-19', 9000)
insertVaribleIntoTable(3, 'Ben', 'ben@pynative.com', '2019-02-23', 9500)
```

*** Select
```python
import sqlite3

def readSqliteTable():
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sqlite_select_query = """SELECT * from SqliteDb_developers"""
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        print("Total rows are:  ", len(records))
        print("Printing each row")
        for row in records:
            print("Id: ", row[0])
            print("Name: ", row[1])
            print("Email: ", row[2])
            print("JoiningDate: ", row[3])
            print("Salary: ", row[4])
            print("\n")

        cursor.close()

    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("The SQLite connection is closed")

readSqliteTable()

```

*** Update
```python
import sqlite3

def updateSqliteTable(id, salary):
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sql_update_query = """Update SqliteDb_developers set salary = ? where id = ?"""
        data = (salary, id)
        cursor.execute(sql_update_query, data)
        sqliteConnection.commit()
        print("Record Updated successfully")
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to update sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("The sqlite connection is closed")

updateSqliteTable(3, 7500)

```

*** Delete

```python
import sqlite3

def deleteRecord():
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        # Deleting single record now
        sql_delete_query = """DELETE from SqliteDb_developers where id = 6"""
        cursor.execute(sql_delete_query)
        sqliteConnection.commit()
        print("Record deleted successfully ")
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to delete record from sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("the sqlite connection is closed")

deleteRecord()

```

*** Create Functions
*** Parameterized Query
*** SQLite BLOB
*** SQLite DateTime
*** resources
https://pynative.com/python-sqlite/
* pymongo
*** setup
```python
import pymongo
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
```

*** create database
```python
mydb = myclient["mydatabase"]
dblist = myclient.list_database_names()
if "mydatabase" in dblist:
  print("The database exists.")
```

*** create collection
```python
mycol = mydb["customers"]
print(mydb.list_collection_names())
collist = mydb.list_collection_names()
if "customers" in collist:
  print("The collection exists.")
```

*** insert one and many
```python
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabase"]
mycol = mydb["customers"]
mydict = { "name": "John", "address": "Highway 37" }
x = mycol.insert_one(mydict)
# return insert id
print(x.inserted_id)

# insert many
mylist = [
  { "name": "Amy", "address": "Apple st 652"},
  { "name": "Hannah", "address": "Mountain 21"},
  { "name": "Michael", "address": "Valley 345"},
  { "name": "Sandy", "address": "Ocean blvd 2"},
  { "name": "Betty", "address": "Green Grass 1"},
  { "name": "Richard", "address": "Sky st 331"},
  { "name": "Susan", "address": "One way 98"},
  { "name": "Vicky", "address": "Yellow Garden 2"},
  { "name": "Ben", "address": "Park Lane 38"},
  { "name": "William", "address": "Central st 954"},
  { "name": "Chuck", "address": "Main Road 989"},
  { "name": "Viola", "address": "Sideway 1633"}
]

x = mycol.insert_many(mylist)

# print list of the _id values of the inserted documents:
print(x.inserted_ids)

# insert with _id
mylist = [
    { "_id": 13, "name": "Chuck", "address": "Main Road 989"},
    { "_id": 14, "name": "Viola", "address": "Sideway 1633"}
]

x = mycol.insert_many(mylist)

# print list of the _id values of the inserted documents:
print(x.inserted_ids)
```

*** find
```python

# find one
x = mycol.find_one()
print(x)

# find many
for x in mycol.find():
  print(x)

# return only some fields
for x in mycol.find({},{ "_id": 0, "name": 1, "address": 1 }):
  print(x)

```

*** query
```python
# Filter the Result
myquery = { "address": "Park Lane 38" }
mydoc = mycol.find(myquery)
for x in mydoc:
  print(x)

# field starts with the letter "S" or higher (alphabetically),
# use the greater than modifier: {"$gt": "S"}
myquery = { "address": { "$gt": "S" } }

mydoc = mycol.find(myquery)

for x in mydoc:
  print(x)

# field starts with the letter "S", use the regular expression {"$regex": "^S"}
myquery = { "address": { "$regex": "^S" } }

mydoc = mycol.find(myquery)

for x in mydoc:
  print(x)
```

*** sort

```python
# ascending
mydoc = mycol.find().sort("name")
for x in mydoc:
  print(x)

# descending
mydoc = mycol.find().sort("name", -1)
for x in mydoc:
  print(x)
```

*** delete
```python
# delete one
myquery = { "address": "Mountain 21" }
mycol.delete_one(myquery)

# delete many
myquery = { "address": {"$regex": "^S"} }
x = mycol.delete_many(myquery)
print(x.deleted_count, " documents deleted.")

# delete all
x = mycol.delete_many({})
print(x.deleted_count, " documents deleted.")
```

*** drop collection

```python
# drop() method returns true if the collection was dropped successfully,
# and false if the collection does not exist.
x = mycol.drop()

```

*** update

```python
mycol = mydb["customers"]

myquery = { "address": "Valley 345" }
newvalues = { "$set": { "address": "Canyon 123" } }

mycol.update_one(myquery, newvalues)

# print "customers" after the update:
for x in mycol.find():
  print(x)

# update many

mycol = mydb["customers"]

myquery = { "address": { "$regex": "^S" } }
newvalues = { "$set": { "name": "Minnie" } }

x = mycol.update_many(myquery, newvalues)

print(x.modified_count, "documents updated.")
```

*** limit
```python
mycol = mydb["customers"]

myresult = mycol.find().limit(5)

# print the result:
for x in myresult:
  print(x)
```

* flask
[[file:flask.org][flask]]
* ipython

```python
c.InteractiveShellApp.extensions = [
    'autotime'
]
c.TerminalInteractiveShell.editing_mode = 'vi'
c.InteractiveShell.colors = 'Linux'
c.TerminalInteractiveShell.highlighting_style = "fruity"
```

* pip

```bash
pip install ansible # Install a python package

pip install ansible==2.7.8 # Install a specific package version

pip uninstall ansible # Uninstall a python package

pip install ansible==foobar # See what versions are available

pip install --upgrade ansible or pip install ansible -U # Upgrade a package

pip list # List installed packages

pip list --outdated # List outdated installed packages

pip install --upgrade ansible or -U # Upgrade installed package

pip freeze > requirements.txt # Capture installed packages

pip freeze -r requirements.txt # Install a dependencies file

pip_search foo # Search for a package (pip install pip-search)
```

* virtualenv
```bash
virtualenv env
source env/bin/activate
deactivate
```

* import
* pwntools
* boilerplate
* shellcode
*** 32-bit

```python
from pwn import *

context.update(arch='i386', os='linux')

shellcode = shellcraft.sh()
print(shellcode)
print(hexdump(asm(shellcode)))

payload  = cyclic(cyclic_find(0x61616168))
payload += p32(0xdeadbeef)
payload += asm(shellcode)

p = process("./crackme0x00")
p.sendline(payload)
p.interactive()
```

*** 64-bit

```python
from pwn import *
    context.clear(arch=’amd64’)
```

* r2pipe

* numpy

* pytorch
```text
load dataset
make dataset iterable
create model class
epoch
instantiate model class
hidden layer --> tanh, relu
non-linear activation --> neurons
instantiate loss class --> cross entropy loss
instantiate optimizer class --> stochastic gradient descent
train model
```
-------------

- [import](https://realpython.com/python-import/)
- [30secondsofcode](https://www.30secondsofcode.org/python/t/list/p/1)
- [reference](https://github.com/rasbt/python_reference)
- [WTFpython](https://github.com/satwikkansal/wtfpython)
- [unit testing](https://github.com/cleder/awesome-python-testing)
- [design-patterns](https://github.com/faif/python-patterns)
- [28 patterns](https://github.com/oxnz/design-patterns)
- [algorithms](https://github.com/TheAlgorithms/Python/blob/master/DIRECTORY.md)
- [algorithms](https://github.com/keon/algorithms)
- [guide](https://docs.python-guide.org)
- [pythonspot](https://pythonspot.com)
- [python-ds](https://github.com/prabhupant/python-ds)
- [functionalprogramming](https://github.com/jmesyou/functional-programming-jargon.py)
- [read files faster](https://towardsdatascience.com/read-excel-files-with-python-1000x-faster-407d07ad0ed8)

* cheatsheets
- https://www.pythoncheatsheet.org/
- https://gto76.github.io/python-cheatsheet/
- https://www.debuggex.com/cheatsheet/regex/python
- https://cheatography.com/davechild/cheat-sheets/python/
- https://speedsheet.io/s/python
- https://github.com/charlax/python-education
- https://github.com/kirang89/pycrumbs
- https://github.com/svaksha/pythonidae

* exercises
- https://runestone.academy/ns/books/published/thinkcspy/index.html
