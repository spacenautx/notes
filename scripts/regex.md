# python

### Characters / Anchors
- Escape character: \
- Escape character: \A
- Escape character: \Z
- Digit: \d
- Not a digit: \D
- Word character: \w
- End of Word: \<
- End of Word: \>
- Not a word character: \W
- Whitespace: \s
- Not whitespace: \S
- Word boundary: \b
- Not a word boundary: \B
- Beginning of a string: ^
- End of a string: $

### Special Characters
- Escape character \
- New line \n
- Carriage return \r
- Tab \r
- Vertical tab \v
- Form feed \f
- Alarm \a
- Backspace \b
- Escape \e

### Character Class
- Control character \c
- White space \s
- Not white space \S
- Digit \d
- Not digit \D
- Word \w
- Not word \W
- Hex character \xhh

### Groupings
- Matches characters in brackets: [ ]
- Matches characters not in brackets: [^ ]
- Either or: |
- Capturing group: ( )

### Quantifiers
- 0 or more: *
- 1 or more: +
- 0 or 1: ?
- An exact number of characters: { }
- Range of number of characters: {Minimum, Maximum}
- Exactly {3}
- 3 or more  {3,}
- 3, 4 or 5+ {3,5}

### Ranges
- Any character except new line
- (a|b) a or b
- (...) Group
- (?:..) Passive Group
- [^abc] Not a or b or c
- [a-q] Lowercase letter between a and q
- [A-Q] Uppercase letter between A and Q
- [0-7] Digit between 0 and 7

### Assertions
- Lookahead assertion (?=)
- Negative lookahead (?!)
- Lookbehind assertion (?<=)
- Negative Lookbehind (?!=) or (?<!)
- If Then Condition (?())
- If Then Else Condition (?()|)
- Comment (?#)

### Metacharacters (Escape These!)
. * + ? ^ $ [ () { . \



* INPUT TEXT --> ACTION TO PERFORM --> PATTERN


### re engine
1, one char at a time
2, left to right
3, greedy, lazy and backtracking
4, groups
5, look ahead and look behind

### Always use Raw string for Regex Patterns
```python
s = 'a\tb'
# raw string. python does not interpret the content of the string.
# USE RAW STRING FOR REGEX PATTERNS
sr = r'a\tb'
print('regular string:', s)
print()
print('raw string:', sr)
```


### re.match - Find match at the beginning of a string
```python
pattern = r"\d+" # \d = digit. + = one or more.  This pattern matches one or more digits
text = "42 is my lucky number"
match = re.match(pattern,text)
# check if match was successful
if match:
    print (match.group(0))
else:
    print ("No match")

pattern = r"\d+" # \d = digit. + = one or more.  This pattern matches one or more digits

# number is not at the beginning. So, this match will fail
text = "my lucky number is 42"

match = re.match(pattern, text)

if match:
    print(match.group(0))
else:
    print("No Match")
```

### input validation
```python
def is_integer(text):
    # Pattern 1
    # \d = digit
    # \d+ = one or more digits
    # pattern = r"\d+"

    # Pattern 2
    # $ = end of string or line
    # one or more digits. followed by end of string or line
    # not cross-platform. works only with match method
    # pattern = r"\d+$"

    # Pattern 3
    # start of string or line. followed by one or more digits. followed by end of string or line
    # ^ = start of string or line.
    # $ = end of string or line
    # cross-platform
    pattern = r"^\d+$"

    match = re.match(pattern, text)

    if match:
        return True
    else:
        return False

is_integer("1234")

```

### Unit Test
```python
def test_is_integer():
    pass_list = ["123","456","900","0991"]
    fail_list = ["a123","124a","1 2 3","1\t2"," 12","45 "]

    for text in pass_list:
        if not is_integer(text):
            print('\tFailed to detect an integer', text)

    for text in fail_list:
        if is_integer(text):
            print('\tIncorrectly classified as an integer', text)

    print('Test complete')

test_is_integer()
```

### re.search - Find the first match anywhere
```python
pattern = r"\d+" # one or more digits

text = "42 is my lucky number"

match = re.search(pattern,text)

# check if match was successful
if match:
    print('Found a match:', match.group(0), 'at index:', match.start())
else:
    print ("No match")

pattern = r"\d+" # \d = digit. + = one or more.  This pattern matches one or more digits

# search method will look for the first match anywhere in the text
text = "my lucky number is 42"

match = re.search(pattern, text)

if match:
    print('Found a match:',match.group(0), 'at index:', match.start())
else:
    print("No Match")

# But, it finds only the first match in the text

pattern = r"\d+" # \d = digit. + = one or more.  This pattern matches one or more digits

# search method will look ONLY for the first match anywhere in the text
text = "my lucky numbers are 42 and 24"

match = re.search(pattern, text)

if match:
    print('Found a match:',match.group(0), 'at index:', match.start())
else:
    print("No Match")
```

### re.findall - Find all the matches
#### method returns only after scanning the entire text
```python
# Find all numbers in the text
pattern = r"\d+"
text = "NY Postal Codes are 10001, 10002, 10003, 10004"

print ('Pattern',pattern)
# successful match
match = re.findall(pattern, text)

if match:
    print('Found matches:', match)
else:
    print("No Match")
```

### re.finditer - Iterator
#### method returns an iterator with the first match and you have control to ask for more matches
```python
pattern = r"\d+"
text = "NY Postal Codes are 10001, 10002, 10003, 10004"

print ('Pattern',pattern)
# successful match
match_iter = re.finditer(pattern, text)

print ('Matches')
for match in match_iter:
    print('\t', match.group(0), 'at index:', match.start())
```

### groups - find sub matches
```python
# Separate year, month and day
# 1. pattern = r"\d+"
# 2. pattern = r"\d{4}\d{2}\d{2}"
# 3. pattern = r"(\d{4})(\d{2})(\d{2})"

pattern = r"(\d{4})(\d{2})(\d{2})"
text = "Start Date: 20200920"

print("Pattern",pattern)
match = re.search(pattern, text)

if match:
    print('Found a match', match.group(0), 'at index:', match.start())

    print('Groups', match.groups())

    for idx, value in enumerate(match.groups()):
        print ('\tGroup', idx+1, value, '\tat index', match.start(idx+1))

else:
    print("No Match")
```

### named groups
```python
# Separate year, month and day
pattern = r"(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})"
text = "Start Date: 20200920"

print("Pattern",pattern)
match = re.search(pattern, text)

if match:
    print('Found a match', match.group(0), 'at index:', match.start())
    print('\t',match.groupdict())
else:
    print("No Match")
```

### access by group name
```python
# Separate year, month and day
pattern = r"(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})"
text = "Start Date: 20200920"

print("Pattern",pattern)
match = re.search(pattern, text)

if match:
    print('Found a match', match.group(0), 'at index:', match.start())
    print('\tYear:',match.group('year'))
    print('\tMonth:',match.group('month'))
    print('\tDay:',match.group('day'))
else:
    print("No Match")
```

### re.sub - find and replace
#### two patterns: one to find the text and another pattern with replacement text
```python
# Format date
#  20200920 => 09-20-2020

pattern = r"(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})"
text = "Start Date: 20200920, End Date: 20210920"

# substitute with value space dollars
replacement_pattern = r"\g<month>-\g<day>-\g<year>"

print ('original text\t', text)
print()

# find and replace
new_text= re.sub(pattern, replacement_pattern, text)

print('new text\t', new_text)

# Make this an exercise
# find one or more digits followed by the word dollars. capture the digits in value group
pattern = r"(?P<value>\d+)dollars"

text = "movie ticket: 15dollars. popcorn: 8dollars"

# substitute with value space dollars
replacement_pattern = r"\g<value> dollars"

print ('original text\t', text)
print()

# find and replace
new_text= re.sub(pattern, replacement_pattern, text)

print('new text\t', new_text)
```

### custom function to generate replacement text
```python
# Format
#   20200920 => Sep-20-2020
import datetime

def format_date(match):
    in_date = match.groupdict()

    year = int(in_date['year'])
    month = int(in_date['month'])
    day = int(in_date['day'])

    #https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
    return datetime.date(year,month,day).strftime('%b-%d-%Y')

# Format date
pattern = r"(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})"
text = "Start Date: 20200920, End Date: 20210920"

print ('original text\t', text)
print()

# find and replace
new_text= re.sub(pattern, format_date, text)

print('new text\t', new_text)

# Make this an assignment
def celsius_to_fahrenheit(match):
    degCelsius =  float(match.group("celsius"))
    degF = 32.0 + (degCelsius * 9.0 / 5.0);
    return '{0}?F'.format(degF);

def substitution_example_custom_logic():
    pattern = r"(?P<celsius>\d+)\u00B0C"
    text = "Today's temperature is 25?C"

    print ('Pattern: {0}'.format(pattern))
    print ('Text before: {0}'.format(text))

    new_text = re.sub(pattern, celsius_to_fahrenheit, text)

    print('Text after:  {0}'.format(new_text))

substitution_example_custom_logic()


```

### re.split - split text based on specified pattern
```python
pattern = r","

text = "a-c,x,y,1"

re.split(pattern,text)
```



### SPECIAL CHARACTERS
```text
^ | Matches the expression to its right at the start of a string. It matches every such instance before each \n in the string.
$ | Matches the expression to its left at the end of a string. It matches every such instance before each \n in the string.
. | Matches any character except line terminators like \n.
\ | Escapes special characters or denotes character classes.
A|B | Matches expression A or B. If A is matched first, B is left untried.
+ | Greedily matches the expression to its left 1 or more times.
* | Greedily matches the expression to its left 0 or more times.
? | Greedily matches the expression to its left 0 or 1 times. But if ? is added to qualifiers (+, *, and ? itself) it will perform matches in a non-greedy manner.
{m} | Matches the expression to its left mtimes, and not less.
{m,n} | Matches the expression to its left m to n times, and not less.
{m,n}? | Matches the expression to its left mtimes, and ignores n. See ? above.
```

### CHARACTER CLASSES(A.K.A. SPECIAL SEQUENCES)
```text
\w | Matches alphanumeric characters, which means a-z, A-Z, and 0-9. It also matches the underscore, _.
\d | Matches digits, which means 0-9.
\D | Matches any non-digits.
\s | Matches whitespace characters, which include the \t, \n, \r, and space characters.
\S | Matches non-whitespace characters.
\b | Matches the boundary (or empty string) at the start and end of a word, that is, between \w and \W.
\B | Matches where \b does not, that is, the boundary of \w characters.
\A | Matches the expression to its right at the absolute start of a string whether in single or multi-line mode.
\Z | Matches the expression to its left at the absolute end of a string whether in single or multi-line mode.
```

### SETS
```text
[ ] | Contains a set of characters to match.
[amk] | Matches either a, m, or k. It does not match amk.
[a-z] | Matches any alphabet from a to z.
[a\-z] | Matches a, -, or z. It matches -because \ escapes it.
[a-] | Matches a or -, because - is not being used to indicate a series of characters.
[-a] | As above, matches a or -.
[a-z0-9] | Matches characters from a to zand also from 0 to 9.
[(+*)] | Special characters become literal inside a set, so this matches (, +, *, and ).
[^ab5] | Adding ^ excludes any character in the set. Here, it matches characters that are not a, b, or 5.
```

### GROUPS
```text
( ) | Matches the expression inside the parentheses and groups it.
(?) | Inside parentheses like this, ? acts as an extension notation. Its meaning depends on the character immediately to its right.
(?PAB) | Matches the expression AB, and it can be accessed with the group name.
(?aiLmsux) | Here, a, i, L, m, s, u, and x are flags:
a — Matches ASCII only
i — Ignore  case
L — Locale  dependent
m — Multi-line
s — Matches  all
u — Matches  unicode
x — Verbose
(?:A) | Matches the expression as represented by A, but unlike (?PAB), it cannot be retrieved afterwards.
(?#...) | A comment. Contents are for us to read, not for matching.
A(?=B) | Lookahead assertion. This matches the expression A only if it is followed by B.
A(?!B) | Negative lookahead assertion. This matches the expression A only if it is not followed by B.
(?<=B)A | Positive lookbehind assertion. This matches the expression A only if Bis immediately to its left. This can only matched fixed length expressions.(?<!B)A | Negative lookbehind assertion. This matches the expression A only if B is not immediately to its left. This can only matched fixed length expressions.
(?P=name) | Matches the expression matched by an earlier group named "name".
(...)\1 | The number 1 corresponds to the first group to be matched. If we want to match more instances of the same expression, simply use its number instead of writing out the whole expression again. We can use from 1 up to 99 such groups and their corresponding numbers.
```

### POPULAR PYTHON RE MODULE FUNCTIONS
```python
re.findall(A, B) # Matches all instances of an expression A in a string B and returns them in a list.
re.search(A, B)  # Matches the first instance of an expression A in a string B, and returns it as a re match object.
re.split(A, B)   # Split a string B into a list using the delimiter A.
re.sub(A, B, C)  # Replace A with B in the string C.
```

### Cheatsheet
- date
- uuid
- zip code
- guid
- html

- MD5
```
[a-fA-F0-9]{32} SHA256 [a-fA-F0-9]{64}
```
- SHA1
```
[a-fA-F0-9]{40} SHA512 [a-fA-F0-9]{128}
```
- IPv4 (Simple)
```
(?:\d{1,3}\.){3}\d{1,3}
```
- IPv4 (Accurate)
```
(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
```
- IPv6
```
(?:[a-fA-F0-9]{1,4}:){7}[a-fA-F0-9]{1,4}
```
- MAC Address
```
[0-9A-F]{2}([-:]?)(?:[0-9A-F]{2}\1){4}[0-9A-F]{2}
```
- Base64
```
^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$
```
- Hex
```
/^#?([a-f0-9]{6}|[a-f0-9]{3})$/
```
- E-Mail Address
```
/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
```
- Constituent parts of HTTP/HTTPS URL - Insensitive inline modifier
```
(?i)(?<URL>(?<scheme>https?)://(?<domain>[^/:]+(?<=\.(?<TLD>[^/:]+)))(?(?=:):(?<port>\d{1,5})|)(?(?=/)(?<URI>/[^/]+)+(?<file>/[^.]+\.\S+)?))
```
- Constituent parts of HTTP/HTTPS URL - No modifier
```
(<URL>(?<scheme>[hH][tT]{2}[pP][sS]?)://(?<domain>[^/:]+(?<=\.(?<TLD>[^/:]+)))(?(?=:):(?<port>\d{1,5})|)(?(?=/)(?<URI>/[^/]+)+(?<file>/[^.]+\.\S+)?))
```
- Drive
```
(?x)(?>\b[a-z]:|\\\\[a-z0-9 %._~-]{1,63}\\[a-z0-9 $%._~-]{1,80})\\
```
- Folder
```
(?>[^\\/:*?"<>|\x00-\x1F]{0,254}[^.\\/:*?"<>|\x00-\x1F]\\)*
```
- File
```
(?>[^\\/:*?"<>|\x00-\x1F]{0,254}[^.\\/:*?"<>|\x00-\x1F])?
```
- Standard Path
```
(?>\b[a-z]:|\\\\[a-z0-9 %._~-]{1,63}\\[a-z0-9 $%._~-]{1,80})\\(?>[^\\/:*?"<>|\x00-\x1F]{0,254}[^.\\/:*?"<>|\x00-\x1F]\\)*(?>[^\\/:*?"<>|\x00-\x1F]{0,254}[^.\\/:*?"<>|\x00-\x1F])?
```
- Windows Path
```
[a-z]:\\(?>[^\\/:*?"<>|\x00-\x1F]{0,254}[^.\\/:*?"<>|\x00-\x1F]\\)*(?>[^\\/:*?"<>|\x00-\x1F]{0,254}[^.\\/:*?"<>|\x00-\x1F])?
```
- Access keys
```
(?i)((access_key|access_token|admin_pass|admin_user|algolia_admin_key|algolia_api_key|alias_pass|alicloud_access_key|amazon_secret_access_key|amazonaws|ansible_vault_password|aos_key|api_key|api_key_secret|api_key_sid|api_secret|api.googlemaps AIza|apidocs|apikey|apiSecret|app_debug|app_id|app_key|app_log_level|app_secret|appkey|appkeysecret|application_key|appsecret|appspot|auth_token|authorizationToken|authsecret|aws_access|aws_access_key_id|aws_bucket|aws_key|aws_secret|aws_secret_key|aws_token|AWSSecretKey|b2_app_key|bashrc password|bintray_apikey|bintray_gpg_password|bintray_key|bintraykey|bluemix_api_key|bluemix_pass|browserstack_access_key|bucket_password|bucketeer_aws_access_key_id|bucketeer_aws_secret_access_key|built_branch_deploy_key|bx_password|cache_driver|cache_s3_secret_key|cattle_access_key|cattle_secret_key|certificate_password|ci_deploy_password|client_secret|client_zpk_secret_key|clojars_password|cloud_api_key|cloud_watch_aws_access_key|cloudant_password|cloudflare_api_key|cloudflare_auth_key|cloudinary_api_secret|cloudinary_name|codecov_token|config|conn.login|connectionstring|consumer_key|consumer_secret|credentials|cypress_record_key|database_password|database_schema_test|datadog_api_key|datadog_app_key|db_password|db_server|db_username|dbpasswd|dbpassword|dbuser|deploy_password|digitalocean_ssh_key_body|digitalocean_ssh_key_ids|docker_hub_password|docker_key|docker_pass|docker_passwd|docker_password|dockerhub_password|dockerhubpassword|dot-files|dotfiles|droplet_travis_password|dynamoaccesskeyid|dynamosecretaccesskey|elastica_host|elastica_port|elasticsearch_password|encryption_key|encryption_password|env.heroku_api_key|env.sonatype_password|eureka.awssecretkey)[a-z0-9_ .\-,]{0,25})(=|>|:=|\|\|:|<=|=>|:).{0,5}['\"]([0-9a-zA-Z\-_=]{8,64})['\"]

```

### List of regex for scraping secret API keys and juicy information.
```
"Cloudinary"  : "cloudinary://.*"
"Firebase URL": ".*firebaseio\.com"
"Slack Token": "(xox[p|b|o|a]-[0-9]{12}-[0-9]{12}-[0-9]{12}-[a-z0-9]{32})"
"RSA private key": "-----BEGIN RSA PRIVATE KEY-----"
"SSH (DSA) private key": "-----BEGIN DSA PRIVATE KEY-----"
"SSH (EC) private key": "-----BEGIN EC PRIVATE KEY-----"
"PGP private key block": "-----BEGIN PGP PRIVATE KEY BLOCK-----"
"Amazon AWS Access Key ID": "AKIA[0-9A-Z]{16}"
"Amazon MWS Auth Token": "amzn\\.mws\\.[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
"AWS API Key": "AKIA[0-9A-Z]{16}"
"Facebook Access Token": "EAACEdEose0cBA[0-9A-Za-z]+"
"Facebook OAuth": "[f|F][a|A][c|C][e|E][b|B][o|O][o|O][k|K].*['|\"][0-9a-f]{32}['|\"]"
"GitHub": "[g|G][i|I][t|T][h|H][u|U][b|B].*['|\"][0-9a-zA-Z]{35,40}['|\"]"
"Generic API Key": "[a|A][p|P][i|I][_]?[k|K][e|E][y|Y].*['|\"][0-9a-zA-Z]{32,45}['|\"]"
"Generic Secret": "[s|S][e|E][c|C][r|R][e|E][t|T].*['|\"][0-9a-zA-Z]{32,45}['|\"]"
"Google API Key": "AIza[0-9A-Za-z\\-_]{35}"
"Google Cloud Platform API Key": "AIza[0-9A-Za-z\\-_]{35}"
"Google Cloud Platform OAuth": "[0-9]+-[0-9A-Za-z_]{32}\\.apps\\.googleusercontent\\.com"
"Google Drive API Key": "AIza[0-9A-Za-z\\-_]{35}"
"Google Drive OAuth": "[0-9]+-[0-9A-Za-z_]{32}\\.apps\\.googleusercontent\\.com"
"Google (GCP) Service-account": "\"type\": \"service_account\""
"Google Gmail API Key": "AIza[0-9A-Za-z\\-_]{35}"
"Google Gmail OAuth": "[0-9]+-[0-9A-Za-z_]{32}\\.apps\\.googleusercontent\\.com"
"Google OAuth Access Token": "ya29\\.[0-9A-Za-z\\-_]+"
"Google YouTube API Key": "AIza[0-9A-Za-z\\-_]{35}"
"Google YouTube OAuth": "[0-9]+-[0-9A-Za-z_]{32}\\.apps\\.googleusercontent\\.com"
"Heroku API Key": "[h|H][e|E][r|R][o|O][k|K][u|U].*[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}"
"MailChimp API Key": "[0-9a-f]{32}-us[0-9]{1,2}"
"Mailgun API Key": "key-[0-9a-zA-Z]{32}"
"Password in URL": "[a-zA-Z]{3,10}://[^/\\s:@]{3,20}:[^/\\s:@]{3,20}@.{1,100}[\"'\\s]"
"PayPal Braintree Access Token": "access_token\\$production\\$[0-9a-z]{16}\\$[0-9a-f]{32}"
"Picatic API Key": "sk_live_[0-9a-z]{32}"
"Slack Webhook": "https://hooks.slack.com/services/T[a-zA-Z0-9_]{8}/B[a-zA-Z0-9_]{8}/[a-zA-Z0-9_]{24}"
"Stripe API Key": "sk_live_[0-9a-zA-Z]{24}"
"Stripe Restricted API Key": "rk_live_[0-9a-zA-Z]{24}"
"Square Access Token": "sq0atp-[0-9A-Za-z\\-_]{22}"
"Square OAuth Secret": "sq0csp-[0-9A-Za-z\\-_]{43}"
"Twilio API Key": "SK[0-9a-fA-F]{32}"
"Twitter Access Token": "[t|T][w|W][i|I][t|T][t|T][e|E][r|R].*[1-9][0-9]+-[0-9a-zA-Z]{40}"
"Twitter OAuth": "[t|T][w|W][i|I][t|T][t|T][e|E][r|R].*['|\"][0-9a-zA-Z]{35,44}['|\"]"
"Prime numbers": "/^1?$|^(11+?)\1+$/"

```

### url
- https://gist.github.com/gruber/8891611
----


### tools
- https://github.com/TheNittam/ReGen
- https://projects.lukehaas.me/regexhub/
- https://github.com/pemistahl/grex
- [visualizer](https://regexper.com)

### resources
- https://github.com/aykutkardas/regexlearn.com
- https://github.com/ChandraLingam/PyRegex/tree/master/UsageExample
- https://github.com/cipher387/regex-for-OSINT
- http://www.pyregex.com
- https://github.com/ziishaned/learn##regex
- https://medium.com/@amhoume/python##for##pentesters##beyond##the##basics##part1##a0adcb89911f
- https://perl.plover.com/Regex/article.html
- https://github.com/regexhq
- https://github.com/crazyguitar/pysheeet/blob/master/docs/notes/python-rexp.rst
- https://github.com/hahwul/RegexPassive
- http://rexegg.com/regex-best-trick.html
- [lets build regex](https://kean.blog/post/lets-build-regex)
- [python regex](https://build-your-own.org/b2a/r1_parse)
- https://github.com/learnbyexample/py_regular_expressions/blob/master/py_regex.md
- https://regex101.com/
- https://blog.gitleaks.io/finding-secrets-with-regular-expressions-d90493bb3784?gi=49afbb3daee8
- https://regex101.com/library
- http://xenon.stanford.edu/~xusch/regexp/
- https://cs.stackexchange.com/questions/125296/unusual-applications-of-regular-expressions
- https://death.andgravity.com/f-re
- https://www.freecodecamp.org/news/python-regex-tutorial-how-to-use-regex-inside-lambda-expression/

### readable version
- https://github.com/manoss96/pregex
- https://github.com/luciferreeves/edify
- https://inventwithpython.com/blog/2022/08/23/introducing-humre-human-readable-regular-expressions/
