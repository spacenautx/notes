# server

* update OS and install openvpn, ufw
```bash
sudo apt udpate
sudo apt upgrade
sudo apt-get install openvpn -y
sudo apt-get install ufw -y
ip a
ip a show
```

* enable ip forwarding and apply
```bash
sudo vim /etc/sysctl.conf
# add
net.ipv4.ip_forward = 1
#
sysctl -p
```

* setup certificate authority
```bash
cp -rf /user/share/easy-rsa /etc/openvpn/
cd /etc/openvpn/easy-rsa
sudo vim vars
# add
set_var EASYRSA                 "$PWD"
set_var EASYRSA_PKI             "$EASYRSA/pki"
set_var EASYRSA_DN              "cn_only"
set_var EASYRSA_REQ_COUNTRY     "USA"
set_var EASYRSA_REQ_PROVINCE    "NY"
set_var EASYRSA_REQ_CITY        "NYC"
set_var EASYRSA_REQ_ORG         "Company CERTIFICATE AUTHORITY"
set_var EASYRSA_REQ_EMAIL	    "user@domain.com"
set_var EASYRSA_REQ_OU          "Company EASY CA"
set_var EASYRSA_KEY_SIZE        2048
set_var EASYRSA_ALGO            rsa
set_var EASYRSA_CA_EXPIRE	    7500
set_var EASYRSA_CERT_EXPIRE     365
set_var EASYRSA_NS_SUPPORT	    "no"
set_var EASYRSA_NS_COMMENT	    "Company CERTIFICATE AUTHORITY"
set_var EASYRSA_EXT_DIR         "$EASYRSA/x509-types"
set_var EASYRSA_SSL_CONF        "$EASYRSA/openssl-easyrsa.cnf"
set_var EASYRSA_DIGEST          "sha256"
```

* initiate the PKI directory and build the CA certificates
```bash
./easyrsa init-pki
./easyrsa build-pki # use password with more than 4 chars
```

* generate server certificate files
```bash
./easyrsa gen-req company-server nopass
```

* sign the server key using CA
```bash
./easyrsa sign-req server company-server
openssl verify -CAfile pki/cs.crt pki/issued/company-server.crt
```

* generate diffie-hellman key for the key exchange
```bash
./easyrsa gen-dh
cp pki/ca.crt /etc/openvpn/server/
cp pki/dh.pem /etc/openvpn/server/
cp pki/private/company-server.key /etc/openvpn/server/
cp pki/issued/company-server.crt /etc/openvpn/server/
```

* generate client certificate and key file
```bash
./easyrsa gen-req client nopass
./easyrsa sign-req client client
cp pki/ca.crt /etc/openvpn/client/
cp pki/issued/client.crt /etc/openvpn/client/
cp pki/private/client.key /etc/openvpn/client/
```

* configure openvpn server
```bash
sudo vim /etc/openvpn/server.conf
# add
port 1194
proto udp
dev tun
ca /etc/openvpn/server/ca.crt
cert /etc/openvpn/server/company-server.crt
key /etc/openvpn/server/company-server.key
dh /etc/openvpn/server/dh.pem
server 10.8.0.0 255.255.255.0
push "redirect-gateway def1"

push "dhcp-option DNS 208.67.222.222"
push "dhcp-option DNS 208.67.220.220"
duplicate-cn
cipher AES-256-CBC
tls-version-min 1.2
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256:TLS-DHE-RSA-WITH-AES-128-GCM-SHA256:TLS-DHE-RSA-WITH-AES-128-CBC-SHA256
auth SHA512
auth-nocache
keepalive 20 60
persist-key
persist-tun
compress lz4
daemon
user nobody
group nogroup
log-append /var/log/openvpn.log
verb 3
```

* enable, start service. check service status
```bash
systemctl enable openvpn@server
systemctl start openvpn@server
systemctl status openvpn@server
ip a show tun0
```

* generate client configuration
```bash
sudo vim /etc/openvpn/client/client.ovpn
# add
client
dev tun
proto udp
remote vpn-server-ip 1194
ca ca.crt
cert client.crt
key client.key
cipher AES-256-CBC
auth SHA512
auth-nocache
tls-version-min 1.2
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256:TLS-DHE-RSA-WITH-AES-128-GCM-SHA256:TLS-DHE-RSA-WITH-AES-128-CBC-SHA256
resolv-retry infinite
compress lz4
nobind
persist-key
persist-tun
mute-replay-warnings
verb 3
```

* configure ufw to accept forwarded packets
```bash
sudo vim /etc/default/ufw
# change
DEFAULT_FORWARD_POLICY="ACCEPT"
```

```bash
sudo vim /etc/ufw/before.rules
# add
*nat
:POSTROUTING ACCEPT [0:0]
-A POSTROUTING -s 10.8.0.0/16 -o ens3 -j MASQUERADE
COMMIT
```

* ufw
```bash
ufw allow 1194/udp
ufw allow OpenSSH
ufw disable
ufw enable
tail -f /var/log/openvpn.log
```

# client

```bash
sudo apt-get install openvpn -y
# copy client folder from server
scp -r root@vpn-server-ip:/etc/openvpn/client .
cd client
sudo openvpn --config client.ovpn
ip a show tun0
```
---
* notes
- open the ports using firewall config in the web console.
---
# openvpn access server

```bash
sudo apt update && apt -y install ca-certificates wget net-tools gnupg

wget -qO - https://as-repository.openvpn.net/as-repo-public.gpg | sudo apt-key add -

echo "deb http://as-repository.openvpn.net/as/debian buster main" | sudo tee /etc/apt/sources.list.d/openvpn-as-repo.list

sudo apt update && sudo apt -y install openvpn-as
# change admin password
passwd openvpn
# https://ip_address/admin/
```

* resources
- https://openvpn.net/vpn-server-resources/recommendations-to-improve-security-after-installation/
- https://tecadmin.net/install-openvpn-debian-10/
- https://medium.com/teendevs/setting-up-an-openvpn-server-on-google-compute-engine-9ff760d775d9
- https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-16-04
- https://blog.g3rt.nl/openvpn-security-tips.html
