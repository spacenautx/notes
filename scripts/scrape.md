## requests

```python
import requests


def get_html(url: str) -> str:
    """Get raw HTML from a URL."""
    headers = {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Max-Age': '3600',
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
              }
    response = requests.get(url, headers=headers)
    return response.text
```

## bs4

```python
import requests
from bs4 import BeautifulSoup

url = ''
page = requests.get(url)
soup = BeautifulSoup(page.content, "lxml")

# find by id
# find by class
# table
soup.title # <title>The Dormouse's story</title>
soup.title.name # u'title'
soup.title.string # u'The Dormouse's story'
soup.title.parent.name # u'head'

#various finder
css_soup.select("p.strikeout.body") # css finder
soup.p # <p class="title"><b>The Dormouse's story</b></p>
soup.p['class'] # u'title'
soup.a # <a class="sister" href="http://example.com/elsie" id="link1">Elsie</a>
soup.find_all('a') # [<a ..>, ..]
soup.find(id="link3") # <a class="sister" href="http://example.com/tillie" id="link3">Tillie</a>
for link in soup.find_all('a'):
    print(link.get('href')) # http://example.com/elsi, # http://example.com/lacie

soup = BeautifulSoup(open("index.html"))
soup = BeautifulSoup("<html>data</html>")
# HTML
soup.prettify() #pretty print
str(soup) # non-pretty print

# String
soup.get_text() #all text under the element

##########################
# css selector
##########################
css_soup.select("p.strikeout.body")
soup.select("p nth-of-type(3)") # 3rd child
soup.select("head > title")
soup.select("p > a:nth-of-type(2)")
soup.select("p > #link1") # direct child
soup.select("#link1 ~ .sister")  # sibling
soup.select('a[href]') # existence of an attribute
soup.select_one(".sister")

# attribute value
soup.select('a[href="http://example.com/elsie"]') # exact attribute
soup.select('a[href^="http://example.com/"]') # negative match
soup.select('a[href$="tillie"]') # end match
soup.select('a[href*=".com/el"]') # middle match


##########################
# basic
##########################
soup.find_all('b') # match by tag
soup.find_all(re.compile("^b")) # match by tag using regex
soup.find_all(["a", "b"]) # match by tag in list

# function (complex condition)
def has_class_but_no_id(tag):
  return tag.has_attr('class') and not tag.has_attr('id')
soup.find_all(has_class_but_no_id)


##########################
# find_all_api
##########################
find_all(name, attrs, recursive, string, limit, **kwargs)

soup.find_all("title") # tag condition
soup.find_all("p", "title") # tag and attr
# [<p class="title"><b>The Dormouse's story</b></p>]
soup.find_all("a")

# keyword arguments
soup.find_all(id="link2")
soup.find_all(href=re.compile("elsie"), id='link1')
soup.find(string=re.compile("sisters")) # text contain sisters

# css class (class is researved keyword)
soup.find_all("a", class_="sister")


##############################
# going up/down/side
##############################
# going down
soup.head# <head><title>The Dormouse's story</title></head>
soup.title# <title>The Dormouse's story</title>
soup.body.b # <b>The Dormouse's story</b>
soup.a # <a class="sister" href="http://example.com/elsie" id="link1">Elsie</a>
soup.find_all('a')
# [<a class="sister" href="http://example.com/elsie" id="link1">Elsie</a>,
#  <a class="sister" href="http://example.com/lacie" id="link2">Lacie</a>,
#  <a class="sister" href="http:

# children = contents
head_tag.contents # [<title>The Dormouse's story</title>]
head_tag.children # [<title>The Dormouse's story</title>]

# descendants (all of a tag?s children, recursively)
for child in head_tag.descendants:
  print(child)

# .string is tricky
head_tag.contents # [<title>The Dormouse's story</title>]
head_tag.string # u'The Dormouse's story' (because head tag has only one child)
print(soup.html.string) # None (because html has many children)

# whitespace removed strings
for string in soup.stripped_strings:
  print(repr(string))


# going up
title_tag.parent # <head><title>The Dormouse's story</title></head>
# going up recursively
link.parents # [ p, body, html, [document], None]


# sideway
# sibling = include text node
sibling_soup.b.next_sibling
sibling_soup.c.previous_sibling

# multiple
sibling_soup.b.next_siblings
sibling_soup.c.previous_siblings

# element = not include text node
sibling_soup.b.next_element
sibling_soup.c.previous_element
sibling_soup.b.next_elements
sibling_soup.c.previous_elements


#############################
# change exisitng tag
#############################
tag.name = "blockquote" # modify tag name
tag['class'] = 'verybold' # modify tag attribute
del tag['class'] # delete attribute
tag.string= 'not too bold' # modify tag contents string
tag.append(" but bolder than usual") # append tag contents

#############################
# insert tag
#############################
new_tag = soup.new_tag("a", href="http://www.example.com")
original_tag.append(new_tag) # create child
new_tag.string = "Link text." # can edit element after creating child

soup.b.string.insert_before(tag)
soup.b.i.insert_after(soup.new_string(" ever "))

#############################
# delete tag
#############################
soup.i.clear() # removes the contents
i_tag = soup.i.extract() # completely removes a tag from tree and returns the element
soup.i.decompose() # completely removes a tag from tree and discard the tag

#############################
# replace/wrap/unwrap tag
#############################
a_tag.i.replace_with(soup.new_tag("b"))
a_tag.i.replace_with(Beautifulsoup("<b>bold element</b>")) # replace inner html
soup.p.string.wrap(soup.new_tag("b"))
a_tag.i.unwrap()



#output
soup.prettify("latin-1")
tag.encode("utf-8")
tag.encode("latin-1")
tag.encode("ascii")


# The SoupStrainer class allows you to choose which parts of an
# incoming document are parsed
from bs4 import SoupStrainer

# conditions
only_a_tags = SoupStrainer("a")
only_tags_with_id_link2 = SoupStrainer(id="link2")

def is_short_string(string):
  return len(string) < 10
only_short_strings = SoupStrainer(string=is_short_string)

# execute parse
BeautifulSoup(html_doc, "html.parser", parse_only=only_a_tags)
BeautifulSoup(html_doc, "html.parser", parse_only=only_tags_with_id_link2)
BeautifulSoup(html_doc, "html.parser", parse_only=only_short_strings)


```

## crud csv

```python
import csv, shutil

# write rows
csv_header_row = [""]
f = open("chart.csv", "w", encoding="utf-8")
writer = csv.DictWriter(f, fieldnames=csv_header_row)
writer.writeheader()
f.close()


# write rows
def append2csv(data, filename):
    with open(filename, "a", newline="", encoding="utf-8") as f:
        writer = csv.writer(f)
        writer.writerows([data])
    f.close()


# read rows
with open('Cars.csv', encoding="utf-8") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        print(row)


# update csv rows
def update_csv(outputfile, data):
    tempfile = NamedTemporaryFile(mode='w', delete=False)
    with open(outputfile, 'r', encoding="utf-8") as csvfile, tempfile:
        reader = csv.DictReader(csvfile, fieldnames=headers)
        writer = csv.DictWriter(tempfile, fieldnames=headers)
        for row in reader:
            if row['url'] == data[0] and row['summary'] == "todo":
                row['url'], row['wiki_id'], row['title'], row['summary'] = data[0], data[1], data[2], data[3]
            row = {'url': row['url'], 'wiki_id': row['wiki_id'], 'title': row['title'], 'summary': row['summary']}
            writer.writerow(row)
    shutil.move(tempfile.name, outputfile)


# csv to sqlite table
from pandas import read_csv
from sqlalchemy import create_engine

df = read_csv("data.csv", encoding="ISO-8859-1")
save_df = df
engine = create_engine('sqlite:///data.sqlite', echo=True)
sqlite_con = engine.connect()
sqlite_table = "football"
save_df.to_sql(sqlite_table, sqlite_con, if_exists='fail')
sqlite_con.close()

```

## find it

```python
def find_it(q, keys, dicts=None):
    if not dicts:
        dicts = [q]
        q = [q]
    data = q.pop(0)
    if isinstance(data, dict):
        data = data.values()
    for d in data:
        dtype = type(d)
        if dtype is dict or dtype is list:
            q.append(d)
            if dtype is dict:
                dicts.append(d)
    if q:
        return find_it(q, keys, dicts)
    return [(k, v) for d in dicts for k, v in d.items() if k in keys]

```

## argparser
```python
# Create ArgumentParser
parser = argparse.ArgumentParser(description="Description message displayed after usage and before positional arguments and options. Can be used to describe the application in a short summary. Optional, omitted if empty.",
                        epilog="Epilog message displayed at the bottom after everything else. Can be used to provide additional information, e.g. license, contact details, copyright etc. Optional, omitted if empty.",
                        argument_default=argparse.SUPPRESS, allow_abbrev=False, add_help=False)

# Add options
parser.add_argument("-c", "--config-file", action="store", dest="config_file", metavar="FILE", type=str, default="config.ini")
parser.add_argument("-d", "--database-file", action="store", dest="database_file", metavar="file", type=str, help="SQLite3 database file to read/write", default="database.db")
parser.add_argument("-l", "--log-file", action="store", dest="log_file", metavar="file", type=str, help="File to write log to", default="debug.log")
parser.add_argument("-t", "--threads", action="store", dest="threads", type=int, help="Number of threads to spawn", default=3)
parser.add_argument("-p", "--port", action="store", dest="port", type=int, help="TCP port to listen on for access to the web interface", default=12345)
parser.add_argument("--max-downloads", action="store", dest="max_downloads", metavar="value", type=int, help="Maximum number of concurrent downloads", default=5)
parser.add_argument("--download-timeout", action="store", dest="download_timeout", metavar="value", type=int, help="Download timeout in seconds", default=120)
parser.add_argument("--max-requests", action="store", dest="max_requests", metavar="value", type=int, help="Maximum number of concurrent requests", default=10)
parser.add_argument("--request-timeout", action="store", dest="request_timeout", metavar="value", type=int, help="Request timeout in seconds", default=60)
parser.add_argument("--console-output", action="store", dest="console_output", metavar="value", type=str.lower, choices=["stdout", "stderr"], help="Output to use for console", default="stdout")
parser.add_argument("--console-level", action="store", dest="console_level", metavar="value", type=str.lower, choices=["debug", "info", "warning", "error", "critical"], help="Log level to use for console", default="info")
parser.add_argument("--console-color", action="store", dest="console_color", metavar="value", type=bool, help="Colorized console output", default=True)
parser.add_argument("--log-template", action="store", dest="log_template", metavar="value", type=str)
parser.add_argument('-v', '--version', action="version", version="Version.  0.0.1a")
parser.add_argument("-h", "--help", action="help", help="Display this message")

# Add positionals
parser.add_argument("input_url", action="store", metavar="URL", type=str, help="URL to download from")
parser.add_argument("output_file", action="store", metavar="DEST", type=str, help="File to save download as")

# Parse command line
args = parser.parse_args()
print("Command line parsed successfully.")

```

## selenium
```python
# selenium webdriver config
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

options = Options()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
service = Service(ChromeDriverManager(log_level=0).install())

def use_chrome(url: str):
    i = 0
    try:
        driver = webdriver.Chrome(options=options, service=service)
        driver.get(url)
        html = driver.page_source
        soup = BeautifulSoup(html, 'lxml')
        driver.quit()
    except:
        i += 1
        if i > 5: sys.exit('ERROR', url, 'exiting')
        use_chrome(url)
    finally:
        if driver: driver.quit()
    return soup
```

## multiprocessing
```python
import tqdm
from multiprocessing import Pool
from bs4 import BeautifulSoup as bsp


def parse(html):
    soup = bsp(html, 'lxml')
    h2 = soup.find('h2')
    # print(h2.text, end='\r')
    return h2.text


with Pool(processes=2) as pool, tqdm.tqdm(total=len(res), position=0) as pbar:
    all_data = []
    for data in pool.imap_unordered(parse, res):
        all_data.extend(data)
        pbar.update()
```

## async
```python
import asyncio
import aiohttp
from colorama import Fore, Back, Style
from aiohttp import ClientSession
from aiohttp import ClientTimeout
from bs4 import BeautifulSoup as bsp

##  HEADER
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0)',
           'Accept-Encoding': 'gzip, deflate',
           'Accept': '*/*',
           'Connection': 'keep-alive',
           'Referer': 'https://www.google.com/'}
##  COLORS
cr = f"{Style.RESET_ALL}"
done = f"{Fore.LIGHTGREEN_EX}[*] done{cr}"
fail = f"{Fore.LIGHTRED_EX}[x] fail{cr}"
green = f"{Fore.LIGHTGREEN_EX}"
red = f"{Fore.LIGHTRED_EX}"
blue = f"{Fore.LIGHTBLUE_EX}"

##  MONGODB
from pymongo import MongoClient
client = MongoClient('192.168.1.16', 27017)
mongodb = client["collection"]
col = mongodb["document"]
##
urls = [x.strip() for x in open('data.csv', 'r').readlines()]
# urls = urls[:20]
# print(urls[:10])
##

async def get(url, session):
    try:
        async with session.get(url=url, headers=headers) as response:
            if response.status == 200:
                res = {}
                res['_id'] = url
                res['html'] = await response.text()
                # x = col.update_one({'_id': url},  {'$set': {'html': res}})
                x = col.insert_one(res)
                # data = (url, res)
                out = f"{green}[*]{cr} {response.status}{cr} | {blue}{url:<150} {cr}"
                print(out)
                await write_csv(url)
            else:
                await write_csv(url, 'failed_urls.csv')
                print(f"{Fore.YELLOW}[-] {response.status} | {url:<150}{cr} |")
    except Exception as e:
        await write_csv(url, 'failed_urls.csv')
        print(f"{red}[x] xxx | {url:<84} | {e.__class__}{cr}")
    return 'OK'


async def mainrun(urls):
    connector = aiohttp.TCPConnector(limit=10, ssl=False)
    timeout = ClientTimeout(total=96000)
    session = aiohttp.ClientSession(connector=connector, timeout=timeout)

    ret = await asyncio.gather( *[get(url, session) for url in urls])

    print(f"{Fore.LIGHTMAGENTA_EX}Finalized a list of {len(ret)} URLs.{cr}")

    await session.close()

async def write_csv(url, file='done.csv'):
    with open(file, 'a') as f:
        f.writelines(url)
        f.writelines('\n')
    return 'ok'

if __name__ == '__main__':
    asyncio.run(mainrun(urls))
```
--------

### playwright
- https://new.pythonforengineers.com/blog/web-automation-dont-use-selenium-use-playwright/

### Bot detection technology
1. Browser fingerprinting technology
2. Javascript-based detection
3. Simple custom-made detection techniques
4. Advanced custom-made detection techniques

### bypass
1. Bot detection tool detection
2. Browser fingerprint testing
    - browserleaks
    - bot.incolumitas
    - ipqualityscore
    - recaptcha-3-test-score
    - dnscookie
3. Stealth browsers
    - Kameleo
    - GoLogin
    - MultiLogin
    - Incogniton
4. Evasion Libraries
    - https://github.com/lwthiker/curl-impersonate
    - https://github.com/berstend/puppeteer-extra/tree/master/packages/puppeteer-extra-plugin-stealth
    - https://github.com/berstend/puppeteer-extra/tree/master/packages/puppeteer-extra
    - https://github.com/berstend/puppeteer-extra/tree/master/packages/playwright-extra
    - https://github.com/spacenick/jedi-crawler
5. CAPTCHA solvers
6. Web Scraping APIs
* https://levelup.gitconnected.com/web-scraping-and-the-art-of-war-5-tools-that-will-help-your-bot-win-c2a3840d8b71
-----

### tools
- https://github.com/JustAnotherArchivist/snscrape
- https://github.com/sergey-scat/unicaps
- https://github.com/QIN2DIM/hcaptcha-challenger
- https://jamesturk.github.io/scrapeghost/

### dark web
- https://infosecwriteups.com/creating-darkweb-crawler-using-python-and-tor-53169d146301 

### links
- https://github.com/reanalytics-databoutique/webscraping-open-project
- https://scrape-it.cloud/blog/web-scraping-with-python
- [tiktok](https://github.com/davidteather/TikTok-Api/blob/master/examples/video_example.py)
- https://github.com/champmq/TheScrapper
