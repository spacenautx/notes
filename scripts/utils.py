"""
import libraries
"""
from bs4 import BeautifulSoup
import requests

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0)',
           'Accept-Encoding': 'gzip, deflate',
           'Accept': '*/*',
           'Connection': 'keep-alive',
           'Referer': 'https://www.google.com/'}

def get_soup(url: str) -> BeautifulSoup | None:
    """ get soup object from url """
    try:
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            return None
        soup = BeautifulSoup(response.text, 'lxml')
        return soup if soup else None
    except requests.exceptions.RequestException as err:
        print(f'{url} {err}')
        return None
##
# read list from file
with open('urls.txt', 'r', encoding="utf-8") as f:
    urls = list(filter(None, f.read().split('n')))
##
# write list into file
with open('urls.txt', 'w', encoding="utf=8") as f:
    for x in urls:
        f.write(x + '\n')
## mongopy
from pymongo import MongoClient
HOST = "192.168.1.37"
PORT = 27017
client = MongoClient(HOST, PORT)
db = client["database_name"]
col = db["collections"]

mongo_data = lambda:  list(col.find())

col.delete_one("_id")   # {'_id': xxx}

mongo_del_many = lambda x: [col.delete_one(y) for y in x]

## excel_pandas
import pandas as pd

def read_excel(filename: str='data.xlsx') -> list | None:
    """ read excel file withe filename as argument """
    try:
        with pd.ExcelFile(filename) as reader:
            sheet = pd.read_excel(reader, sheet_name='sheet1')
        return list(sheet(['url']))
    except pd.Exception as err:
        print(f'{err}')
        return None

def write_xlsx(xlsx_file: str, data: list):
    """ write to excel file with data as argument """
    df = pd.DataFrame(data)
    writer = pd.ExcelWriter(xlsx_file, engine='openpyxl', mode='a', if_sheet_exists='overlay')
    df.to_excel(writer, index=False, sheet_name='sheet1', startrow=writer.sheets['sheet1'].max_row, header=False)
    writer.save()
    print(f'New row added to {xlsx_file}')

def create_header() -> None:
    try:
        df = pd.DataFrame()
        header_row = ['Date', 'League', 'Home', 'Status', 'Away']
        writer = pd.ExcelWriter(xlsx_file, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='sheet1', startrow=1, header=False)
        workbook  = writer.book
        worksheet = writer.sheets['sheet1']
        header_format = workbook.add_format({ 'bold': True, 'valign': 'top', 'border': 1})
        for col_num, value in enumerate(header_row):
            worksheet.write_string(0, col_num, value, header_format)
        writer.save()
    except Exception as e:
        print(f'{e}')
    return None

## colors
FR = '\033[31m'  # RED
FG = '\033[32m'  # GREEN
FY = '\033[33m'  # YELLOW
FB = '\033[34m'  # BLUE
FM = '\033[35m'  # MAGENTA
FC = '\033[36m'  # CYAN
FW = '\033[37m'  # WHITE

BR = '\033[41m'  # RED
BG = '\033[42m'  # GREEN
BY = '\033[43m'  # YELLOW
BB = '\033[44m'  # BLUE
BM = '\033[45m'  # MAGENTA
BC = '\033[46m'  # CYAN
BW = '\033[47m'  # WHITE

BRT = '\033[1m'  # BRIGHT
DIM = '\033[2m'  # DIM
RS = '\033[39m'  # RESET
RSA = '\033[0m'  # RESET ALL
NML = '\033[22m' # NORMAL


## sqlite3
import sqlite3
def sql_select_json(path_to_db: str, query: str) -> dict | None:
    """Returns data from an SQL query as a list of dicts."""
    try:
        con = sqlite3.connect(path_to_db)
        con.row_factory = sqlite3.Row
        things = con.execute(query).fetchall()
        unpacked = [{k: item[k] for k in item.keys()} for item in things]
        con.close()
        return unpacked
    except Exception as e:
        print(f"Failed to execute. Query: {query}\n with error:\n{e}")
        unpacked = None

##
import time


def timer(func):
    """
    A decorator to calculate how long a function runs.

    Parameters
    ----------
    func: callable
      The function being decorated.

    Returns
    -------
    func: callable
      The decorated function.
    """
    def wrapper(*args, **kwargs):
        # Start the timer
        start = time.time()
        # Call the `func`
        result = func(*args, **kwargs)
        # End the timer
        end = time.time()

        print(f"{func.__name__} took {round(end - start, 4)} "
                "seconds to run!")
        return result
    return wrapper

@timer
def sleep(n):
    """
    Sleep for n seconds

    Parameters
    ----------
    n: int or float
      The number of seconds to wait
    """
    time.sleep(n)

##
