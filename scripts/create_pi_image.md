- wget 2021-12-02-raspios-buster-armhf-lite.zip
- unzip 2021-12-02-raspios-buster-armhf-lite.zip

```bash
# Determine the partition number, and the "end" of the partition
# Find the first available loop device echo $loop
# extend raspbian image by 1GB
# grow the partition
# expand partition
# set up image as loop device
# check the filesystem
# mount partition
# mount binds
# copy app files to pi
# chroot  and get pi shell

sudo parted 2021-12-02-raspios-buster-armhf-lite.zip print
export loop=$(losetup -f)
dd if=/dev/zero bs=1M count=1024 >> 2021-12-02-raspios-buster-armhf-lite.img
sudo parted 2021-12-02-raspios-buster-armhf-lite.img resizepart 2 2800MB
sudo losetup -P $loop 2021-12-02-raspios-buster-armhf-lite.img
sudo e2fsck -y -f ${loop}p2
sudo resize2fs ${loop}p2
sudo mkdir /newroot
sudo mount -o rw ${loop}p2 /newroot
sudo mount --bind /dev /newroot/dev/
sudo mount --bind /sys /newroot/sys/
sudo mount --bind /proc /newroot/proc/
sudo mount --bind /dev/pts /newroot/dev/pts
sudo cp -rf ewrDS /newroot/home/pi
sudo chroot /newroot
```

### switch to pi
```bash
su pi
```

### customize img
```bash
apt-get update -y && \
apt-get upgrade -y && \
apt install python3-pip fbi vim tmux -y && \
apt autoremove && \
apt clean && \
sudo chown -R pi:pi ~/ewrDS && \
cd ~/ewrDS && \
pip3 install virtualenv && \
source ~/.profile && \
virtualenv -p python3 .venv && \
source .venv/bin/activate && \
pip install --no-cache-dir -r requirements.txt && \
sudo mv ~/ewrDS/supervisord.service /etc/systemd/system/ && \
sudo systemctl enable supervisord.service
```

### switch to host, after customize - unmount everything
```bash
sudo umount -R /newroot
sudo umount /newroot/{dev/pts,dev,sys,proc,}
sudo losetup -d ${loop0} # unmount loop device
sudo rm -rf /newroot
```

### create compressed file for sdcard
```bash
# fstrim without usb drive
sudo losetup -f
sudo losetup /dev/loop0 $IMAGE
sudo partprobe /dev/loop0
sudo gparted /dev/loop0 <-- use mouse to shrink/resize
sudo losetup -d /dev/loop0
sudo fdisk -l $IMAGE
truncate --size=$[(END+1)*512] $IMAGE
```

# Resources
- https://gist.github.com/jkullick/9b02c2061fbdf4a6c4e8a78f1312a689
