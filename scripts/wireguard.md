# server
## install
```
sudo apt update
sudo apt install wireguard
```
## generate keys
```
wg genkey | tee privatekey | wg pubkey > publickey
```

## config file
```
# /etc/wireguard/wg0.conf
[Interface]
PrivateKey = yAnz5TF+lXXJte14tji3zlMNq+hd2rYUIgJBgB3fBmk=
ListenPort = 51820

[Peer]
PublicKey = xTIBA5rboUvnH4htodjb6e697QjLERt1NAB4mZqp8Dg=
AllowedIPs = 10.192.122.3/32, 10.192.124.1/24

[Peer]
PublicKey = TrMvSoP4jYQlY6RIzBgbssQqY3vxI2Pi+y71lOWWXX0=
AllowedIPs = 10.192.122.4/32, 192.168.0.0/16

[Peer]
PublicKey = gN65BkIKy1eCE9pP1wdc8ROUtkHLF2PfAqYdyYBz6EA=
AllowedIPs = 10.10.10.230/32
```

## [TODO] iptables
```
iptables -A INPUT -p udp -m udp --dport 51820 -j ACCEPT
iptables -A FORWARD -i wg0 -j ACCEPT
iptables -A FORWARD -o wg0 -j ACCEPT
```

## port forwarding
#### IP Forwarding, open /etc/sysctl.conf and uncomment or add the line:
```
net.ipv4.ip_forward=1
```

#### apply the settings
```
sysctl -p
```

## start
#### create the interface and configure it
```
wg-quick up wg0
```
#### To see the WireGuard-specific details of the interface
```
wg
```
#### To start the VPN on boot
```
systemctl enable wg-quick@wg0
```

## [TODO] create config files for clients
## dashboard
- https://github.com/donaldzou/WGDashboard
- https://github.com/WeeJeWel/wg-easy/

## testing
#### relayed traffic
```
sudo tcpdump -nn -i wg0
sudo tcpdump -nn -i eth0 udp and port 51820
```

#### debugging
```
# modprobe wireguard && echo module wireguard +p > /sys/kernel/debug/dynamic_debug/control
tail -f /var/log/syslog
```
## backup
-  save the VPN server's private key offline, else we'll need to generate a new private key and reconfigure all VPN clients.

## [TODO] recover
### udp balancer
### manual failover
### floating IP
### cloudflare warp


# client
## generate keys
```
wg genkey | tee privatekey | wg pubkey > publickey
```

## config file
```
[Interface]
PrivateKey = gI6EdUSYvn8ugXOt8QQD6Yc+JyiZxIhp3GInSWRfWGE=
ListenPort = 21841

[Peer]
PublicKey = HIgo9xNzJMWLKASShiTqIybxZ0U3wGLiUeJ1PKf8ykw=
# Public IP address of VPN server
# Use the floating IP address if there is one
Endpoint = 192.95.5.69:51820
AllowedIPs = 0.0.0.0/0
```
- https://github.com/vx3r/wg-gen-web

## apply changes
```
wg-quick down wg0 && wg-quick up wg0
# avoid disrupting or dropping active VPN connections
wg syncconf wg0 <(wg-quick strip wg0)
```

## connect
```
wg-quick up wg0
```

# [TODO] apps (userspace implementations)
## lin
## win
## mob
