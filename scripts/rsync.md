# rsync

SRC=/path/to/source
DEST=/path/to/dest

# Copy
rsync -aP --stats --human-readable "${SRC}" "${DEST}"

# Move
rsync -aP --stats --human-readable --remove-source-files "${SRC}" "${DEST}"

# Sync
rsync -aP --stats --human-readable --delete "${SRC}" "${DEST}"

-avvz = archive, verbose x 2, compress

--times = preserve modification times

--stats = give some file-transfer stats

--checksum = skip based on checksum, not mod-time & size

--human-readable = output numbers in a human-readable format

--acls = preserve ACLs (implies -p)

--itemize-changes = output a change-summary for all updates

--progress = show progress during transfer

--out-format='[%t] [%i] (Last Modified: %M) (bytes: %-10l) %-100n'

%t = current date time

%i = an itemized list of what is being updated

%M = the last-modified time of the file

%-10l = the length of the file in bytes (-10 is for alignment and precision)

%-100n = the filename (short form; trailing "/" on dir) (-100 is for alignment and precision)
