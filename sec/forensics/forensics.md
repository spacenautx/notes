# forensics


### wireshark
---



### tshark

---
### tools
- https://github.com/AvillaDaniel/AvillaForensics

### resources

- [forensics guide](https://github.com/mikeroyal/Digital-Forensics-Guide)
- [A course on digital forensics](https://github.com/asiamina/A-Course-on-Digital-Forensics)
- [linux forensics](https://github.com/ashemery/LinuxForensics)
- [forensics wiki](https://forensicswiki.xyz/page/Main_Page)
- [Mem Labs](https://github.com/stuxnet999/MemLabs)
- https://github.com/volatilityfoundation/volatility/wiki/Memory-Samples
- https://www.pentestpartners.com/security-blog/how-to-do-firmware-analysis-tools-tips-and-tricks/
- https://www.blackhat.com/presentations/bh-usa-03/bh-us-03-willis-c/bh-us-03-willis.pdf
- https://trailofbits.github.io/ctf/forensics/
- https://theflagisnothere.wordpress.com/2018/04/22/timisoara-ctf-2018-neurosurgery/
- https://digitalinvestigator.blogspot.com/2022/08/pe-forensics-dos-and-pe-headers.html
- https://cyb3rops.medium.com/the-bicycle-of-the-forensic-analyst-6dc83fb6fb34
- https://github.com/ashemery/LinuxForensics
- https://github.com/shadawck/awesome-anti-forensic
