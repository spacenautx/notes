# Checklist

## recon

### RECON TOOLING
- [ ] Utilize port scanning
- Don't look for just the normal 80,443 - run a port scan against all 65536 ports.
- Tools useful for this: nmap, masscan, unicornscan
- Read the manual pages for all tools, they serve as gold dust for answering questions.
- [ ] Map visible content
- Click about the application, look at all avenues for where things can be clicked on, entered, or sent.
- Tools to help: Firefox Developer Tools - Go to Information>Display links.
- Discover hidden & default content
- Utilize shodan for finding similar apps and endpoints
- Utilize the waybackmachine for finding forgotten endpoints
- Map out the application looking for hidden directories, or forgotten things like /backup/ etc.
- Tools: dirb - Also downloadable on most linux distrobutions, dirbuster-ng - command line implementation of dirbuster, wfuzz,SecLists.
- [ ] Test for debug parameters & Dev parameters
- RTFM - Read the manual for the application you are testing, does it have a dev mode? is there a DEBUGTRUE flag that can be flipped to see more?
- [ ] Identify data entry points
- Look for where you can put data, is it an API? Is there a paywall or sign up ? Is it purely unauthenticated?
- [ ] Identify the technologies used
- Look for what the underlying tech is. useful tool for this is nmap again & for web apps specifically wappalyzer.
- [ ] Map the attack surface and application
- Look at the application from a bad guy perspective, what does it do? what is the most valuable part?
- Some applications will value things more than others.
- Look at the application logic too, how is business conducted?

### Methods
- [ ] Scraping
- [ ] Brute-force
- [ ] Alterations & permutations of already known subdomains
- [ ] Online DNS tools
- [ ] SSL certificates
- [ ] Certificate Transparency
- [ ] Search engines
- [ ] Public datasets
- [ ] DNS aggregators
- [ ] Git repositories
- [ ] Text parsing (HTML, JavaScript, documents?)
- [ ] VHost discovery
- [ ] ASN discovery
- [ ] Reverse DNS
- [ ] Zone transfer (AXFR)
- [ ] DNSSEC zone walking
- [ ] DNS cache snooping
- [ ] Content-Security-Policy HTTP headers
- [ ] Sender Policy Framework (SPF) records
- [ ] Subject Alternate Name (SAN)

### find
- [ ] OPERATING SYSTEM
- [ ] WEB SERVER
- [ ] DATABASE SERVERS
- [ ] PROGRAMMING LANGUAGES
- [ ] PLUGINS/VERSIONS
- [ ] OPEN PORTS
- [ ] USERNAMES
- [ ] SERVICES
- [ ] WEB SPIDERING
- [ ] GOOGLE HACKING

## VECTORS
- [ ] INPUT FORMS
- [ ] GET/POST PARAMS
- [ ] URI/REST STRUCTURE
- [ ] COOKIES
- [ ] HEADERS

## identify
- [ ] IP Addresses and Sub-domains
- [ ] External/3rd Party sites
- [ ] People
- [ ] Technologies
- [ ] Content of Interest
- [ ] Vulnerabilities

## records
- SOA Records - Indicates the server that has authority for the domain.
- MX Records - List of a host?s or domain?s mail exchanger server(s).
- NS Records - List of a host?s or domain?s name server(s).
- A Records - An address record that allows a computer name to be translated to an IP address.
  Each computer has to have this record for its IP address to be located via DNS.
- PTR Records - Lists a host?s domain name, host identified by its IP address.
- SRV Records - Service location record.
- HINFO Records - Host information record with CPU type and operating system.
- TXT Records - Generic text record.
- CNAME - A host?s canonical name allows additional names/ aliases to be used to locate a computer.
- RP - Responsible person for the domain.

## tools
- whois
- wappalyzer
- google HB
- sublist3r
- Async DNS Brute
- gobuster
- sn1p3r
- maltego
- shodan
- OSINT
- intrigue-core
- Recon-Ng
- theHarvester
- SpiderFoot
-----
## osint
- https://www.securitysift.com/passive-reconnaissance
- http://www.vulnerabilityassessment.co.uk/Penetration%20Test.html
- http://onstrat.com/osint/
-----
## checksites
- http://archive.fo
- http://community.riskiq.com
- http://fortiguard.com
- http://pub-db.com
- http://sitereview.bluecoat.com
- http://spyonweb.com
- http://vxcube.com
- http://www.ipinfo.com
- http://www.ipvoid.com
- https://abuseipdb.com
- https://aguse.jp
- https://alexa.com/siteinfo
- https://apility.io
- https://app.binaryedge.io
- https://app.threatconnect.com
- https://archive.org
- https://badpackets.net
- https://bgpview.io
- https://bitcoinabuse.com
- https://blockchain.com
- https://censys.io
- https://check-host.net
- https://crt.sh
- https://cxsecurity.com
- https://data.occrp.org
- https://dnslytics.com
- https://domainbigdata.com
- https://domaintools.com
- https://domainwat.ch
- https://emailrep.io
- https://en.ipip.net
- https://exchange.xforce.ibmcloud.com
- https://facebook.com
- https://findsubdomains.com
- https://fofa.so
- https://fortiguard.com
- https://github.com
- https://greynoise.io
- https://hackertarget.com
- https://hashdd.com
- https://hybrid-analysis.com
- https://hypestat.com < - - #
- https://instagram.com
- https://intelx.io
- https://ipinfo.io
- https://joesandbox.com
- https://linkedin.com
- https://live.blockcypher.com
- https://malshare.com
- https://maltiverse.com
- https://metrics.torproject.org/rs.html
- https://mxtoolbox.com
- https://nvd.nist.gov
- https://onyphe.io
- https://otx.alienvault.com
- https://pinterest.jp
- https://publicwww.com
- https://pulsedive.com
- https://qiita.com
- https://search.arin.net/rdap
- https://securitytrails.com
- https://shodan.io
- https://sploitus.com
- https://talosintelligence.com
- https://threatcrowd.org
- https://threatintelligenceplatform.com
- https://threatminer.org
- https://transparencyreport.google.com
- https://twitter.com
- https://ultratools.com
- https://urlscan.io
- https://viewdns.info
- https://virustotal.com
- https://viz.greynoise.io
- https://vulmon.com
- https://vulncode-db.com
- https://wa-com.com
- https://web.archive.org
- https://weleakinfo.com
- https://whois.domaintools.com
- https://zoomeye.org
## dorks [0/3]
- [ ] github
- [ ] google
- [ ] shodan

-----
## fingerprinting [0/8]
- [ ] Bruteforce subdomains with online tools and github scripts
- [ ] Directory enumeration dirb, gobuster, bruteforce
- [ ] Identify underlying web technology
- [ ] Uncover HTTP services running on ports other than 80 and 443
- [ ] Identify firewall
- [ ] Find sensitive keywords in HTML source such as admin, http, todo, redir, etc.
- [ ] Generate site structure
- [ ] Identify known vulnerabilities in web/app server

-----
## network testing [0/6]
- [ ] Test for PING (ICMP echo packets)
- [ ] Test for ZONE transfer
- [ ] Find all services running using NMAP
- [ ] Perform Nessus scan
- [ ] Test all common UDP ports and realted issues
- [ ] Test SSL/TLS using Qualys

-----
## session management [0/19]
- [ ] Establish how session management is handled in the application (eg, tokens in cookies, token in URL)
- [ ] Identify session cookie domain scope (path and domain)
- [ ] Check session termination after a maximum lifetime
- [ ] Check session termination after relative timeout
- [ ] Check session cookie duration (expires and max-age)
- [ ] Test to see if users can have multiple simultaneous sessions
- [ ] Test session cookies for randomness
- [ ] Confirm that new session tokens are issued on login, role change and logout
- [ ] Test for consistent session management across applications with shared session management
- [ ] Test for session puzzling
- [ ] Identify actual session cookie out of bulk cookies
- [ ] Decode cookies using standard algorithms such as base64, hex, etc.
- [ ] Modify 1 character in cookie token and resubmit, check whether session still exists or session invalidates, perform same till you actually find the characters being used in session cookies
- [ ] Token leakage via Referer header - Untrusted 3rd party
- [ ] Check flags HTTPOnly, Secure flag and same-site
- [ ] Check before and after session cookie values
- [ ] Reply the session cookie from a different public IP address and check if app maintains the state of the machine or not
- [ ] Check concurrent login through different IP
- [ ] Check if any user pertaining information stored in cookie value or not, if yes tamper for privesc
### Check session termination after logout [0/2]
- [ ] Old Session Does Not Expire
  1. create your account
  2. open two browser eg.,chrome and firefox
  3. Login in one browser eg.chrome
  4. In other browser(firefox) login either change your password or reset your password
  5. After successfully changed or reset go to other browser refresh the page if you are still logged in

- [ ] Server security misconfiguration -> Lack of security headers -> Cache control for a security page
  1. Login to the application
  2. Navigate around the pages
  3. Logout
  4. Press (Alt+left-arrow) buttons
  5. If you are logged in or can view the pages navigated by the user. Then you found a bug.

-----
## JWT Attack [0/6]
1. It consists of header,payload,Signature
Header
{
 "alg" : "HS256",
 "typ" : "JWT"
}
Payload
{
 "loggedInAs" : "admin",
 "iat" : 1422779638
}
Signature
HMAC-SHA256
(
 secret,
 base64urlEncoding(header) + '.' +
 base64urlEncoding(payload)
)
Changing alg to null
Example
{
 "alg" : "NONE",
 "typ" : "JWT"
}
Note;;////--remove the signuature
You can also use none,nOne,None,n0Ne
Change the payload like
Payload

{
 "loggedInAs" : "admin",
 "iat" : 1422779638
}
Here change user to admin

## TIPS AND METHOD
- [ ] First decode full token or 1 1 each part of token to base64
- [ ] Change the payload use jwt web token burp
- [ ] Changing encrption rs256 to sh256
- [ ] Signature not changes remove it or temper it,
- [ ] Brute forcing the key in hs256 because it use same key to sign and verify means publickeyprivate key
- [ ] Other Easy Method

    - Create a account
    - Inspect the token
    - Base64 decode the header
    - If any Kid parameter are there so you can find some bugs
    - Using that parameter you can also find directory traversal , i tell you how
    - Change that kid parameter with you directory traversal payload
    - Change payload {"user":"admin"}
    - Create a python script that generate a exploit token. (If you want that script so dm me in Twitter )
    - Put that token and reload the page
    - Done
    -
### TOOLS TO USE

- Jwt token attack burp extention
- (Link - https://github.com/portswigger/json-web-token-attacker)
- Base64 decoder
- jwt.io to analyse the struct of token
- jwt cat for weak secret token

-----
## registration [0/5]
- So there are multiple ways to do it but all are same.

Steps(For Registration):
  1. for registeration intercept the signup request that contains data you have entered.
  2. Click on action -> do -> intercept response to this request.
  3. Click forward.
  4. Check response it that contains any link,any token or otp.

- [ ] Check for duplicate registration with same email id for account takeover
- [ ] Check for weak password policy. So if you find that target have weak password policy try to go for no rate limit attacks in poc shows by creating very weak password of your account.
- [ ] Check for stored username as a part of welcome message post authentication and try stored xss
- [ ] Check for insufficient email verification process
- [ ] Weak registration implementation - Allows dispostable email addresses
-----
## weak password policy [0/2]
- [ ] First Way
    - Check if you can use Password same as that of Email Address
    - Check if you can use Username same as that of Email Address
    - Try above mentioned when Resetting Password , Creating Account , Changing Password from Account Settings
- [ ] Second Way
    - Check if you can use Password some Weak Passwords such as 123456, 111111 , abcabc , qwerty123
    - Try above mentioned when Resetting Password , Creating Account , Changing Password from Account Settings
    - Applications usually have Restrictions on Password while Creating Account, Make sure you check for both the cases when Resetting Password

-----
## login features [0/9]
- [ ] Check username enumeration
- [ ] Bypass login panel with common login SQL injection payloads using BurpSuite Intruder
- [ ] Try accessing resources without authentication
- [ ] Check if user creds are sent over http
- [ ] Check if user creds can forcefully be sumitted over http while http and https both are enabled
- [ ] Check account lockout threshold value
- [ ] Create custom password wordlist and try bruteforce
- [ ] Test 0auth functionality
- [ ] Test 0auth functionality for open redirect

-----
## error codes [0/5]
- [ ] Try accessing custom pages after root directory such as yourname.php, yourname.asp and determine custom or default error received by server
- [ ] Add multiple parameters in same post get request using different value and generate error
- [ ] Add [], ]], and [[ in cookie values and parameter values to create errors
- [ ] Try to generate unusual error code by giving input as /~yourname/%s at the end of website URL
- [ ] Use fuzzing technique to create errors and determine any information leakage

-----
## post login [0/7]
- [ ] Try CSRF on various features that are pertaining to a single user account
- [ ] Post login change email id and update with any existing email id. Check if its getting validated on server side or not. Does the application send any new email confirmation link to a new user or not? What if a user does not confirm the link in some time frame?
- [ ] Test for file upload. Test no AV detection, No Size limit, Extension bypass
- [ ] Open profile picture in new tab and check the URL. Find email id/user id info.  EXIF Geolocation Data Not Stripped From Uploaded Images.
- [ ] Check account deletion option if application provides it and confirm that via forgot password feature
- [ ] Change email id, account id, user id parameter and try to bruteforce other user's password
- [ ] Check whether application re-authenticates for performing sensitive operation for post authentication features

-----
## forgot password [0/14]
- [ ] Failure to invalidate session on Logout and Password reset
- [ ] Check if forget password reset link/code uniqueness
- [ ] Check if reset link does get expire or not if its not used by the user for certain amount of time
- [ ] Find user account identification parameter and tamper Id or parameter value to change other user's password
- [ ] Check for weak password policy
- [ ] Weak password reset implementation - Token is not invalidated after use
- [ ] If reset link have another params such as date and time then change date and time value in order to make active & valid reset link.
- [ ] Check if security questions are asked?  a. How many guesses allowed? -> Lockout policy maintaind or not?
- [ ] Add only spaces in new password and confirmed password. Then Hit enter and see the result.
- [ ] Does it display old password on the same page after completion of forget password formality?
- [ ] Ask for two password reset link and use the older one from user's email
- [ ] Check if active session gets destroyed upon changing the password or not?
- [ ] Weak password reset implementation - Password reset token sent over HTTP
- [ ] Send continuous forget password requests so that it may send sequencial tokens

-----
## password reset [0/4]
- [ ] poisoning:
- Go to password reset funtion.
- Enter email and intercept the request.
- Change host header to some other host i.e,
   Host:target.com
   Host:attacker.com
- Forward this if you found that in next request attacker.com means you successfully theft the token.

- [ ] Steps(For password reset):
- Intercept the forget password option.
- Click on action -> do -> intercept response to this request.
- Click forward.
- Check response it that contains any link,any token or otp.

- [ ] Password reset link token does not expire:
- Create your account on target
- request a forget password link
- Don't use that link
- Instead logged in with your old password and change your email to other
- Now use that password link sents to old email and check if you are able to change your password if yes than there is the title bug.

- [ ] Password Reset Token Leakage:
- Sent a password reset request using forget password
- Check your email
- copy your reset page link and paste in another tab and make burp intercept on.
- Look for every request if you find similiar token that is in reset link with other domain like: bat.bing.com or facebook.com
- Than there is reset password token leakage.
-----
## File Uploads [0/10]
- [ ] Test that acceptable file types are whitelisted
- [ ] Test that file size limits, upload frequency and total file counts are defined and are enforced
- [ ] Test that file contents match the defined file type
- [ ] Test that all file uploads have Anti-Virus scanning in-place.
- [ ] Test that unsafe filenames are sanitised
- [ ] Test that uploaded files are not directly accessible within the web root
- [ ] Test that uploaded files are not served on the same hostname/port
- [ ] Test that files and other media are integrated with the authentication and authorisation schemas
- [ ] Bypassing Normal extension
Now what we can do is we can upload a file which looks like this something.php.jpg or somethings.jpg.php.
- [ ] passing the magic Byte validation.
For this method we use polygots. Polyglots, in a security context, are files that are a valid form of multiple different file types. For example, a GIFAR is both a GIF and a RAR file. There are also files out there that can be both GIF and JS, both PPT and JS, etc.
so while we have to upload a JPEG file type we actaully can upload a PHAR-JPEG file which will appear to be a JPEg file type to the server while validating. the reason is the file PHAR-JPEg file has both the JPEG header and the PHP file also. so while uploading it didn?t get detected and later after processing the PHP file can be used to exploit.
And at last Uploading a shell to some random websites for fun is not really cool so don't ever try untill unless you have the permission to test.

How the bypass was possible?
1. Create a malicious file with an extension that is accepted by the application.
2. Upload that file and click on send.
3. Capture the request in any proxy tool, edit the file extension to the malicious extension that you want. In some cases, you might need to change the content type of a file.
4. Forward the request to the server.
-----
## contact us [0/2]
- [ ] Is CAPTCHA implemented on contact us form in order to restrict email flooding attacks?
- [ ] Does it allow to upload file on the server?

-----
## authentication bypass
### 2FA Bypass Techniques [0/13]
- [ ] Response Manipulation
        In response if "success":false
        Change it to "success":true
- [ ] Status Code Manipulation
        If Status Code is 4xx
        Try to change it to 200 OK and see if it bypass restrictions
- [ ] 2FA Code Leakage in Response
        Check the response of the 2FA Code Triggering Request to see if the code is leaked.
- [ ] JS File Analysis
        Rare but some JS Files may contain info about the 2FA Code, worth giving a shot
- [ ] 2FA Code Reusability
        Same code can be reused
- [ ] Lack of Brute-Force Protection
        Possible to brute-force any length 2FA Code
- [ ] Missing 2FA Code Integrity Validation
        Code for any user acc can be used to bypass the 2FA
- [ ] CSRF on 2FA Disabling
        No CSRF Protection on disabling 2FA, also there is no auth confirmation
- [ ] Password Reset Disable 2FA
        2FA gets disabled on password change/email change
- [ ] Backup Code Abuse
        Bypassing 2FA by abusing the Backup code feature
        Use the above mentioned techniques to bypass Backup Code to remove/reset 2FA restrictions
- [ ] Clickjacking on 2FA Disabling Page
        Iframing the 2FA Disabling page and social engineering victim to disable the 2FA
- [ ] Enabling 2FA doesn't expire Previously active Sessions
        If the session is already hijacked and there is a session timeout vuln
- [ ] Bypass 2FA with null or 000000
        Enter the code 000000 or null to bypass 2FA protection.
### repeating the form submission multiple times using repeater
Steps :
  1. Create an account with a non-existing phone number
  2. Intercept the Request in BurpSuite
  3. Send the request to the repeater and forward
  4. Go to Repeater tab and change the non-existent phone number to your phone number
  5. If you got an OTP to your phone, try using that OTP to register that non-existent number
### OTP Bypass on Register account via Response manipulation [0/3]
- [ ] Steps:-
    - Register account with mobile number and request for OTP.
    - Enter incorrect OTP and capture the request in Burpsuite.
    - Do intercept response to this request and forward the request.
    - response will be
    {"verificationStatus":false,"mobile":9072346577","profileId":"84673832"}

    5. Change this response to
    {"verificationStatus":true,"mobile":9072346577","profileId":"84673832"}

    6. And forward the response.
    7. You will be logged in to the account.
    Impact: Account Takeover

- [ ] Steps:-
    1. Go to login and wait for OTP pop up.
    2. Enter incorrect OTP and capture the request in Burpsuite.
    3. Do intercept response to this request and forward the request.
    4. response will be
    error

4. Change this response to
success

5. And forward the response.
6. You will be logged in to the account.
Impact: Account Takeover

- [ ] Steps:
- Register 2 accounts with any 2 mobile number(first enter right otp)
- Intercept your request
- click on action -> Do intercept -> intercept response to this request.
- check what the message will display like status:1
- Follow the same procedure with other account but this time enter wrong otp
- Intercept respone to the request
- See the message like you get status:0
- Change status to 1 i.e, status:1 and forward the request if you logged in means you just done authentication bypass.

-----
## product purchase [0/19]
- [ ] Buy now - Tamper product ID to purchase other high valued product with low prize
- [ ] Buy now - Tamper product data in order to increase the number of product with the same    prize
- [ ] Gift/Voucher - Tamper gift/voucher count in the request (if any) to increase/decrease the number of vouchers/gifts to be used
- [ ] Gift/Vourcher - Tamper gift/voucher value to increase/decrease the value of voucher in terms of money. (e.g. $100 is given as a voucher, tamper value to increase, decrease money)
- [ ] Gift/Voucher - Reuse gift/voucher by using old gift values in parameter tampering.
- [ ] Gift/Vourcher - Check the uniqueness of gift/voucher parameter and try guessing other gift/voucher code.
- [ ] Gift/Voucher - Use parameter polution technique to add same voucher twice by adding same parameter name and value again with & in the BurpSuite request.
- [ ] Cart - Tamper user id to delete products from other user's cart.
- [ ] Cart - Tamper cart id to add/delete products from other user's cart.
- [ ] Cart - Identify cart id/user id for cart feature to view the added items from other user's account.
- [ ] Shipping Address - Tamper BurpSuite request to change other user's shipping address to yours.
- [ ] Shipping Address - Try stored-XSS by adding XSS vector on shipping address.
- [ ] Shipping Address - Use parameter pollution technique to add two shipping address instead of one trying to manipulate application to send same item on two shipping address.
- [ ] Place Order - Tamper payment options parameter to change the payment method. E.g. Consider some items cannot be ordered for cash on delivery but tampering request parameters from debit/credit/paypal/netbanking option to cash on delivery may allow you to place order for that particular item.
- [ ] Place Order - Tamper the amount value for payment manipulation in each main and sub requests and responses.
- [ ] Place Order - Check if CVV is going in cleartext or not.
- [ ] Place Order - Check if credit/debit card details are masked or not.
- [ ] Place Order  - Check if application itself process your card details and then perform transaction or it calls any third party payment processing company to perform transaction.
- [ ] Track Order - Track other user's order by guessing order tracking number
- [ ] Refund - View other user's refund status.
- [ ] Refund - Refund more money than the intended one by parameter manipulation.
- [ ] Refund - If refund tracking is allowed then gain other user's refund tracking status.

-----
## CSP
CSP stands for Content Security Policy which is a mechanism to define which resources can be fetched out or executed by a web page. In other words, it can be understood as a policy that decides which scripts, images, iframes can be called or executed on a particular page from different locations. Content Security Policy is implemented via response headers or meta elements of the HTML page. From there, it?s browser?s call to follow that policy and actively block violations as they are detected.

How does it work?
CSP works by restricting the origins that active and passive content can be loaded from. It can additionally restrict certain aspects of active content such as the execution of inline JavaScript, and the use of eval().

Defining Resources
default-src 'none';
img-src 'self';
script-src 'self' https://code.jquery.com;
style-src 'self';
report-uri /__cspreport__
font-src 'self' https://addons.cdn.mozilla.net;
frame-src 'self' https://ic.paypal.com https://paypal.com;
media-src https://videos.cdn.mozilla.net;
object-src 'none';

Some defining resources

1. script-src: This directive specifies allowed sources for JavaScript. This includes not only URLs loaded directly into elements, but also things like inline script event handlers (onclick) and XSLT stylesheets which can trigger script execution.
2. default-src: This directive defines the policy for fetching resources by default. When fetch directives are absent in CSP header the browser follows this directive by default.
3. Child-src: This directive defines allowed resources for web workers and embedded frame contents.
4. connect-src: This directive restricts URLs to load using interfaces like ,fetch,websocket,XMLHttpRequest
5. frame-src: This directive restricts URLs to which frames can be called out.
6. frame-ancestors: This directive specifies the sources that can embed the current page. This directive applies to , , , and tags. This directive can't be used in tags and applies only to non-HTML resources.
7. img-src: It defines allowed sources to load images on the web page.
8. Manifest-src: This directive defines allowed sources of application manifest files.
9. media-src: It defines allowed sources from where media objects like , and can be loaded.
10. object-src: It defines allowed sources for the , and elements.
11. base-uri: It defines allowed URLs which can be loaded using element.
12. form-action: This directive lists valid endpoints for submission from tags.
13. plugin-types: It defineslimits the kinds of mime types a page may invoke. upgrade-insecure-requests: This directive instructs browsers to rewrite URL schemes, changing HTTP to HTTPS. This directive can be useful for websites with large numbers of old URL's that need to be rewritten.
14. sandbox: sandbox directive enables a sandbox for the requested resource similar to the sandbox attribute. It applies restrictions to a page's actions including preventing popups, preventing the execution of plugins and scripts, and enforcing a same-origin policy.

Basic CSP Bypass
There are quit a few ways to mess up your implementation of CSP. One of the easiest ways to misconfigure CSP is to use dangerous values when setting policies. For example suppose you have the following CSP header:

default-src 'self' *

As you know the default-src policy acts a catch all policy. You also know that * acts as a wild card. So this policy is basically saying allow any resources to be loaded. Its the same thing as not having a CSP header! You should always look out for wildcard permissions.

Lets look at another CSP header:

script-src 'unsafe-inline' 'unsafe-eval' 'self' data: https://www.google.com http://www.google-analytics.com/gtm/js  https://*.gstatic.com/feedback/ https://accounts.google.com;
Here we have the policy script-src which we know is used to define where we can load javascript files from. Normally things like  would be blocked but due to the value ?unsafe-inline? this will execute. This is something you always want to look out for as it is very handy as an attacker.

You can also see the value data: this will allow you to load javascript if you have the data: element as shown below: <iframe/src?data:text/html,?>.

So far all of the techniques used to bypass CSP have been due to some misconfiguration or abusing legitimate features of CSP. There are also a few other techniques which can be used to bypass the CSP.

JSONP CSP Bypass
If you don?t know what JSONP is you might want to go look at a few tutorials on that topic but ill give you a brief overview. JSONP is a way to bypass the same object policy (SOP). A JSONP endpoint lets you insert a javascript payload , normally in a GET parameter called ?callback? and the endpoint will then return your payload back to you with the content type of JSON allowing it to bypass the SOP. Basically we can use the JSONP endpoint to serve up our javascript payload. You can find an example below:

https://accounts.google.com/o/oauth2/revoke?callbackalert(1337)
As you can see above we have our alert function being displayed on the page.

The danger comes in when a CSP header has one of these endpoints whitelisted in the script-src policy. This would mean we could load our malicious javascript via the JSONP endpoint bypassing the CSP policy.

Look at the following CSP header:

script-src https://www.google.com http://www.google-analytics.com/gtm/js  https://*.gstatic.com/feedback/ https://accounts.google.com;
This would get blocked by the CSP

something.example.com?vuln_paramjavascript:alert(1);

This would pass because accounts.google.com is allowed to load javascript files. However, we are abusing the JSONP feature to load our malicious javascript.

something.example.com?vuln_paramhttps://accounts.google.com/o/oauth2/revoke?callbackalert(1337)

CSP Injection Bypass
The third type of CSP bypass is called CSP injection. This occurs when user supplied input is reflected in the CSP header. Suppose you have the following url:

example.com?vulnsomething_vuln_csp

-> If your input is reflected in the CSP header you should have somthing like this.

script-src something_vuln_csp;
object-src 'none';
base-uri 'none';
require-trusted-types-for 'script';
report-uri https://csp.example.com;
This means we can control what value the script-src value is set to. We can easily bypass the CSP by setting this value to a domain we control.

-----
## easycve
1. Grab all the subdomains i.e, subfinder -d domain.com | ** e -a domains.
2. Grapball alive domains i.e,  cat domains.txt | httpx -status-code | grep 200 | cut -d " " -f1 ** tee -a alive.
3. Run nuclei basic-detection,panels,workflows,cves templates differently and store results in different file. i.e, cat alive.txt | nuclei -t nuclei-templates/workflows | tee -a workflows.
4. Read each output carefully with patience.
5. Find interest tech used by target. i.e, jira
6. put that link into browser check the version used by target.
7. Go on google search with jira version exploit.
8. grep the cves
9. Go to twitter in explore tab search CVE(that you found from google) poc or CVE exploit
10. Go to google and put cve or some details grab from  twitter for a better poc read writeups related to that.
11. Try all cves if success report it.:)
-----
## Code review [0/10]
- [ ] 1.Important functions first
When reading source code, focus on important functions such as authentication, password reset, state-changing actions and sensitive info reads. (What is the most important would depend on the application.) Then, review how these components interact with other functionality. Finally, audit other less sensitive parts of the application.
- [ ] 2.Follow user input
Another approach is to follow the code that processes user input. User input such as HTTP request parameters, HTTP headers, HTTP request paths, database entries, file reads, and file uploads provide the entry points for attackers to exploit the application?s vulnerabilities.This may also help us to find some critical vulnerabilities like xxe,xxs,sql injection
- [ ] 3.Hardcoded secrets and credentials:
Hardcoded secrets such as API keys, encryption keys and database passwords can be easily discovered during a source code review. You can grep for keywords such as ?key?, ?secret?, ?password?, ?encrypt? or regex search for hex or base64 strings (depending on the key format in use).
- [ ] 4.Use of dangerous functions and outdated dependencies:
Unchecked use of dangerous functions and outdated dependencies are a huge source of bugs. Grep for specific functions for the language you are using and search through the dependency versions list to see if they are outdated.
- [ ] 5.Developer comments, hidden debug functionalities, configuration files, and the .git directory:
These are things that developers often forget about and they leave the application in a dangerous state. Developer comments can point out obvious programming mistakes, hidden debug functionalities often lead to privilege escalation, config files allow attackers to gather more information about your infrastructure and finally, an exposed .git directory allows attackers to reconstruct your source code.
- [ ] 6.Hidden paths, deprecated endpoints, and endpoints in development:
These are endpoints that users might not encounter when using the application normally. But if they work and they are discovered by an attacker, it can lead to vulnerabilities such as authentication bypass and sensitive information leak, depending on the exposed endpoint.
- [ ] 7.Weak cryptography or hashing algorithms:
This is an issue that is hard to find during a black-box test, but easy to spot when reviewing source code. Look for issues such as weak encryption keys, breakable encryption algorithms, and weak hashing algorithms. Grep for terms like ECB, MD4, and MD5.
- [ ] 8.Missing security checks on user input and regex strength:
Reviewing source code is a great way to find out what kind of security checks are missing. Read through the application?s documentation and test all the edge cases that you can think of. A great resource for what kind of edge cases that you should consider is PayloadsAllTheThings.(github)
- [ ] 9.Missing cookie flags:
Look out for missing cookie flags such as httpOnly and secure.
- [ ] 10.Unexpected behavior, conditionals, unnecessarily complex and verbose functions:
Additionally, pay special attention to the application?s unexpected behavior, conditionals, and complex functions. These locations are where obscure bugs are often discovered.
-----
## handling/validation of input/data [0/49]
- [ ] Fuzz all request parameters
- [ ] Test for SQL injection
- [ ] Identify all reflected data
- [ ] Test for reflected XSS
- [ ] Test for HTTP header injection
- [ ] Test for arbitrary redirection
- [ ] Test for stored attacks
- [ ] Test for OS command injection
- [ ] Test for path traversal
- [ ] Test for script injection
- [ ] Test for file inclusion
- [ ] Test for SMTP injection
- [ ] Test for native software flaws (buffer overflow, integer bugs, format strings)
- [ ] Test for SOAP injection
- [ ] Test for LDAP injection
- [ ] Test for XPath injection
- [ ] Test for Reflected Cross Site Scripting
- [ ] Test for Stored Cross Site Scripting
- [ ] Test for DOM based Cross Site Scripting
- [ ] Test for Cross Site Flashing
- [ ] Test for HTML Injection
- [ ] Test for SQL Injection
- [ ] Test for LDAP Injection
- [ ] Test for ORM Injection
- [ ] Test for XML Injection
- [ ] Test for SSI Injection
- [ ] Test for SSTI Injection
- [ ] Test for XPath Injection
- [ ] Test for IDOR Injection
- [ ] Test for XQuery Injection
- [ ] Test for IMAP/SMTP Injection
- [ ] Test for Code Injection
- [ ] Test for Expression Language Injection
- [ ] Test for Command Injection
- [ ] Test for Overflow (Stack, Heap and Integer)
- [ ] Test for Format String
- [ ] Test for incubated vulnerabilities
- [ ] Test for HTTP Splitting/Smuggling
- [ ] Test for HTTP Verb Tampering
- [ ] Test for Open Redirection
- [ ] Test for Local File Inclusion
- [ ] Test for Remote File Inclusion
- [ ] Compare client-side and server-side validation rules
- [ ] Test for NoSQL injection
- [ ] Test for auto-binding
- [ ] Test for Mass Assignment
- [ ] Test for NULL/Invalid Session Cookie
- [ ] Test for SSRF Injection
- [ ] Test for cache poisoning
### Test for XXE Injection [0/6]
- [ ] Convert the content type from "application/json"/"application/x-www-form-urlencoded" to "applcation/xml".
- [ ] File Uploads allows for docx/xlcs/pdf/zip , unzip the package and add your evil xml code into the xml files.
- [ ] If svg allowed in picture upload , you can inject xml in svgs.
- [ ] If the web app offers RSS feeds , add your milicious code into the RSS.
- [ ] Fuzz for /soap api, some applications still running soap apis.
- [ ] If the target web app allows for SSO integration, you can inject your malicious xml code in the SAML request/reponse.
### Test for HTTP parameter pollution [0/4]
- [ ] 1.Browse through your target.
  say https://target.com
- [ ] 2.Find a article or blog present on target website which must have a link to share that blog on different social networks such as
  Facebook,Twitter etc.
- [ ] 3.Let's say we got and article with url:
  https://taget.com/how-to-hunt
  then just appened it with payload ?&uhttps://attacker.com/vaya&textanother_site:https://attacker.com/vaya
  so our url will become
  https://taget.com/how-to-hunt?&uhttps://attacker.com/vaya&textanother_site:https://attacker.com/vaya
- [ ] 4.Now hit enter with the abover url and just click on share with social media.
  Just observe the content if it is including our payload i.e. https://attacker.com
  Then it is vulnerable or else try next target.
### Test for CORS Injection [0/5]
- [ ] Hunting method 1(Single target):
    Step->1. Capture the target website and spider or crawl all the website using burp.
    Step->2. Use burp search look for Access-Control
    Step->3. Try to add Origin Header i.e,Origin:attacker.com or Origin:null or Origin:attacker.target.com or Origin:target.attacker.com
    Step->4  If origin is reflected in response means the target is vuln to CORS

- [ ] Hunting method 2(mutliple means including subdomains):
    step 1-> find domains i.e subfinder -d target.com -o domains.txt
    step 2-> check alive ones : cat domains.txt | httpx | tee -a alive.txt
    step 3-> send each alive domain into burp i.e, cat alive.txt | parallel -j 10 curl --proxy "http://127.0.0.1:8080" -sk 2>/dev/null
    step 4-> Repeat hunting method 1

- [ ] Automate Way :
    step1-> find domains i.e, subfinder -d domain.com -o target.txt
    step2-> grep alive: cat target.txt | httpx | tee -a alive.txt
    step3-> grep all urls using waybackurls by tomnomnom and gau tool i.e,cat alive.txt | gau | tee -a urls.txt
    step4-> run any of these tools on each url
    step5-> configure the manually

- [ ] Steps
    1) Find Domains with the help of subfinder,assetfinder,findomain i.e , subfinder -d target.com | tee -a hosts1 , findomain -t target.com | tee -a hosts1 , assetfinder --subs-only target.com |tee -a hosts1 .
    2) Then cat hosts1 | sort -u | tee -a hosts2 and then cat hosts2 | httpx | tee -a hosts .
    3) Navigate through terminal where hosts file is located  echo "/" > paths
    4) Then type meg -v
    5) After the completion of process type gf cors.
    6) All the urls with Access-Control-Allow will be displayed.

- [ ] CORS Bypass
    Origin:null
    Origin:attacker.com
    Origin:attacker.target.com
    Origin:attackertarget.com
    Origin:sub.attackertarget.com

### Test for csrf [0/8]
- [ ] Always try to get csrf on:
        1.Change Password function.
        2.Email change
        3.Change Security Question
- [ ] Re-use CSRF token
- [ ] Check if token is validated on server side or not
- [ ] Check if token validation for full length or partial length
- [ ] Create few dummy account and compare the CSRF token for all those accounts
- [ ] Bypass CSRF token using 2 input type fields in for updating user's information in the same HTML file
- [ ] Convert POST request to GET and remove _csrf (anti-csrf token) to bypass the CSRF protection.
- [ ] Check if the value you are trying to change is passed in multiple parameters such as cookie, http headers along with GET and POST request.

- [ ] Change Request Method [POST > GET]
- [ ] Remove Total Token Parameter
- [ ] Remove The Token, And Give a Blank Parameter
- [ ] Copy a Unused Valid Token , By Dropping The Request and Use That Token
- [ ] Use Own CSRF Token To Feed it to Victim
- [ ] Replace Value With Of A Token of Same Length
- [ ] Reverse Engineer The Token
- [ ] Extract Token via HTML injection
- [ ] Switch From Non-Form `Content-Type: application/json` or `Content-Type: application/x-url-encoded` To `Content-Type: form-multipart`
- [ ] Bypass the regex
  If the site is looking for ?bank.com? in the referer URL, maybe ?bank.com.attacker.com? or ?attacker.com/bank.com? will work.
- [ ] Remove the referer header (add this <meta name?referrer? content?no-referrer?> in your payload or html code)
- [ ] Clickjacking
  (If you aren?t familiar with clickjacking attacks, more information can be found https://owasp.org/www-community/attacks/Clickjacking.)
  Exploiting clickjacking on the same endpoint bypasses all CSRF protection. Because technically, the request is indeed originating from the legitimate site. If the page where   the vulnerable endpoint is located on is vulnerable to clickjacking, all CSRF protection will be rendered irrelevant and you will be able to achieve the same results as a CSRF   attack on the endpoint, albeit with a bit more effort.

### Test for sqli [0/3]
- [ ] Search for mysql, mssql or any DB error codes
- [ ] Run SQLmap on all dynamic requests eventhough automated scanners didn't flag it. SQLmap has verbose payloads and they can detect injection points eventhough acunetix, netsparkr didn't report.
- [ ] Run authenticated SQL map using burp plugin if possible

### Test for xss [0/15]
- [ ] Test what's being sanitised and what not
- [ ] Try XSS using XSSstrike tool by Somdev Sangwan
- [ ] Upload file using xss
- [ ] Try all variations of IMG SRC onerror payloads
- [ ] If script tags are banned, use and other HTML tags
- [ ] If output is reflected back inside the javascript as a value of any variable just use alert(1)
- [ ] Try base64 payload
- [ ] If the logout button just performs the redirection then use old classic XSS payload
- [ ] Try Polygot payload

### Test for open redirection [0/13]
- [ ] Use burp 'find' option in order to find parameters such as url, red, redirect, redir, origin, redirect_uri, target etc.
- [ ] Check the vlaue of these parameter which may contain a URL.
- [ ] Change the URL value to www.chintan.com and check if gets redirected or not.
- [ ] Give below URL in web browser and check if application redirects to the www.site.com website or not.
- [ ] https://www.target.com/ÿ/www.twitter.com/
- [ ] https://www.target.com//www.twitter.com/
- [ ] https://www.target.com/Ã¿/www.twitter.com/
- [ ] https://www.target.com//www.twitter.com/ ...etc.
- [ ] Bypass filter using returnTo///site.com/
- [ ] Bypass filter using returnTohttp:///site.com/
- [ ] Insert new header in the GET/POST request as follows:  X-Forwarded-Host: www.site.com  If it gets redirected from the target application then its vulnerable
- [ ] Capture any request,  Change the host header in the request to google.com and see if its getting redirected or not

- [ ] Bypass Trick:
1. While you I trying to redirect https://targetweb.com?urlhttp://attackersite.com it did not redirected!
2. I Created a new subdomain with with www.targetweb.com.attackersite.com
3. And when I tried to redirect with https://targetweb.com?urlwww.targetweb.com.attackersite.com
4. It will successfully redirected to the www.targetweb.com.attackersite.com website!
5. Due to the bad regex it has been successfully bypass their protection!

- [ ] Steps:
  1. If the Applictaion have a user Sign-In/Sign-Up feature, then register a user and log in as the user.
  2. Go to your user profile page , for example : samplesite.me/accounts/profile
  3. Copy the profile page's URL
  4. Logout and Clear all the cookies and go to the homepage of the site.
  5. Paste the Copied Profile URL on the address bar
  6. If the site prompts for a login , check the address bar , you may find the login page with a redirect parameter like the following
     - https://samplesite.me/login?nextaccounts/profile
     - https://samplesite.me/login?retUrlaccounts/profile
  7. Try to exploit the parameter by adding an external domain and load the crafted URL
      eg:- https://samplesite.me/login?nexthttps://evil.com/
                     (or)
        https://samplesite.me/login?nexthttps://samplesite.me@evil.com/  #(to beat the bad regex filter)
  8. If it redirects to evil.com , thers's your open redirection bug.
  9. Try to leverage it to XSS
       eg:- https://samplesite.me/login?nextjavascript:alert(1);//

-----
### Identify the logic attack surface [0/7]
- [ ] Test transmission of data via the client
- [ ] Test for reliance on client-side input validation
- [ ] Test any thick-client components (Java, ActiveX, Flash)
- [ ] Test multi-stage processes for logic flaws
- [ ] Test handling of incomplete input
- [ ] Test trust boundaries
- [ ] Test transaction logic

-----
### Card Payment [0/11]
- [ ] Test for known vulnerabilities and configuration issues on Web Server and Web Application
- [ ] Test for default or guessable password
- [ ] Test for non-production data in live environment, and vice-versa
- [ ] Test for Injection vulnerabilities
- [ ] Test for Buffer Overflows
- [ ] Test for Insecure Cryptographic Storage
- [ ] Test for Insufficient Transport Layer Protection
- [ ] Test for Improper Error Handling
- [ ] Test for all vulnerabilities with a CVSS v2 score > 4.0
- [ ] Test for Authentication and Authorization issues
- [ ] Test for CSRF

-----
### Configuration Management [0/8]
- [ ] Check for commonly used application and administrative URLs
- [ ] Check for old, backup and unreferenced files
- [ ] Check HTTP methods supported and Cross Site Tracing (XST)
- [ ] Test file extensions handling
- [ ] Test for security HTTP headers (e.g. CSP, X-Frame-Options, HSTS)
- [ ] Test for policies (e.g. Flash, Silverlight, robots)
- [ ] Test for non-production data in live environment, and vice-versa
- [ ] Check for sensitive data in client-side code (e.g. API keys, credentials)

-----
## denial of service [0/3]
- [ ] Rate Limit Bypass Techniques
There are two ways to do that
1. Customizing HTTP Methods
2. Adding Headers to Spoof IP
- [ ] Customizing HTTP Methods
If the request goes on GET try to change it to POST, PUT, etc.,
If you wanna bypass the rate-limit in API's try HEAD method.
- [ ] Rate Limit Bypass using Header
Use the following Header just Below the Host Header
X-Forwarded-For: IP
X-Forwarded-IP: IP
X-Client-IP: IP
X-Remote-IP: IP
X-Originating-IP: IP
X-Host: IP
X-Client: IP
or use double X-Forwarded-For header
X-Forwarded-For:
X-Forwarded-For: IP

- [ ] Adding HTTP Headers to Spoof IP and Evade Detection
These are Headers I've collected so far to Bypass Rate-Limits.

X-Forwarded: 127.0.0.1
X-Forwarded-By: 127.0.0.1
X-Forwarded-For: 127.0.0.1
X-Forwarded-For-Original: 127.0.0.1
X-Forwarder-For: 127.0.0.1
X-Forward-For: 127.0.0.1
Forwarded-For: 127.0.0.1
Forwarded-For-Ip: 127.0.0.1
X-Custom-IP-Authorization: 127.0.0.1
X-Originating-IP: 127.0.0.1
X-Remote-IP: 127.0.0.1
X-Remote-Addr: 127.0.0.1

- [ ] Rate Limit Bypass using Special Characters
Adding Null Byte ( %00 ) at the end of the Email can sometimes Bypass Rate Limit.
Try adding a Space Character after a Email. ( Not Encoded )
Some Common Characters that help bypassing Rate Limit : %0d , %2e , %09 , %20 , %0, %00, %0d%0a, %0a, %0C
Adding a slash(/) at the end of api endpoint can also Bypass Rate Limit. domain.com/v1/login -> domain.com/v1/login/

- [ ] Using IP Rotate Burp Extension
Try changing the user-agent, the cookies... anything that could be able to identify you
If they are limiting to 10 tries per IP, every 10 tries change the IP inside the header. Change other headers
Burp Suite's Extension IP Rotate works well in many cases. Make sure you have Jython installed along.
Here You'll everything you need - https://github.com/PortSwigger/ip-rotate

- [ ] email bounce
Check if Application has Invite Functionality
Try sending Invites to Invalid Email Accounts
Try to find Email Service Provider such as AWS SES , Hubspot , Campaign Monitor
Note You can find Email Service Provider by checking Email Headers
Once you have the Email Service Provider, Check there Hard Bounce Limits. Here are the limits for some of them:
Hubspot Hard bounces: HubSpot's hard bounce limit is 5%. For reference, many ISPs prefer bounce rates to be under 2%.
AWS SES: The rate of SES ranges from first 2-5% then 5-10%
Impact: Once the Hard Bounce Limits are reached, Email Service Provider will block the Company which means, No Emails would be sent to the Users !

- [ ] Long Password DoS Attack
As the value of password is hashed and then stored in Databases. If there is no limit on the length of the Password, it can lead to consumption of resources for Hashing the Long Password.
How to test?
Use a Password of length around 150-200 words to check the presense of Length Restriction
If there is no Restriction, Choose a longer password and keep a eye on Response Time
Check if the Application Crashes for few seconds
Where to test?
Registration Password Field is usually restricted but the Length of Password on the Forgot Password Page and the Change Password (As Authenticated User) Functionality is usually missing.

- [ ] Long String DOS
When you set some string so long so server cannot process it anymore it cause DOS sometime
How to test
Create app and put field like username or address or even profile picture name parameter ( second refrence ) like 1000 character of string .
Search A's account from B's account either it will
Either it will keeping on searching for long time
Either the application will crash (500 - Error Code)
-----
### Assess application hosting [0/9]
- [ ] Test segregation in shared infrastructures
- [ ] Test segregation between ASP-hosted applications
- [ ] Test for web server vulnerabilities
- [ ] Default credentials
- [ ] Default content
- [ ] Dangerous HTTP methods
- [ ] Proxy functionality
- [ ] Virtual hosting mis-configuration
- [ ] Bugs in web server software
-----
### EXIF_Geo_Data_Not_Stripped
When a user uploads an image in example.com, the uploaded image?s EXIF Geolocation Data does not gets stripped. As a result, anyone can get sensitive information of example.com users like their Geolocation, their Device information like Device Name, Version, Software & Software version used etc.
Steps to reproduce:
1. Got to Github ( https://github.com/ianare/exif-samples/tree/master/jpg)
2. There are lot of images having resolutions (i.e 1280 * 720 ) , and also whith different MB?s .
3. Go to Upload option on the website
4. Upload the image
5. see the path of uploaded image ( Either by right click on image then copy image address OR right click, inspect the image, the URL will come in the inspect , edit it as html )
6. open it (http://exif.regex.info/exif.cgi)
7. See wheather is that still showing exif data , if it is then Report it.
-----
### Miscellaneous tests [0/9]
- [ ] Check for DOM-based attacks
- [ ] Check for frame injection
- [ ] Check for local privacy vulnerabilities
- [ ] Persistent cookies
- [ ] Caching
- [ ] Sensitive data in URL parameters
- [ ] Forms with autocomplete enabled
- [ ] Follow up any information leakage
- [ ] Check for weak SSL ciphers
-----

### links
- https://hariprasaanth.notion.site/hariprasaanth/WEB-APPLICATION-PENTESTING-CHECKLIST-0f02d8074b9d4af7b12b8da2d46ac998
