# recon

* passive
https://medium.com/bugbountywriteup/finding-the-origin-ip-behind-cdns-37cd18d5275

* RECONNAISSANCE
Utilize port scanning
Map visible content
Discover hidden & default content
Utilize shodan for finding similar apps and endpoints
Utilize the waybackmachine for finding forgotten endpoints
Test for debug parameters & Dev parameters
Identify data entry points
Identify the technologies used
Map the attack surface and application

* RECON TOOLING
Utilize port scanning
-Don't look for just the normal 80,443 - run a port scan against all 65536 ports.
Tools useful for this: nmap, masscan, unicornscan
Read the manual pages for all tools, they serve as gold dust for answering questions.
Map visible content
Click about the application, look at all avenues for where things can be clicked on, entered, or sent.
Tools to help: Firefox Developer Tools - Go to Information>Display links.
Discover hidden & default content
Utilize shodan for finding similar apps and endpoints
Utilize the waybackmachine for finding forgotten endpoints
Map out the application looking for hidden directories, or forgotten things like /backup/ etc.
Tools: dirb - Also downloadable on most linux distrobutions, dirbuster-ng - command line implementation of dirbuster, wfuzz,SecLists.
Tools: https://github.com/Drew-Alleman/DataSurgeon
Test for debug parameters & Dev parameters
RTFM - Read the manual for the application you are testing, does it have a dev mode? is there a DEBUG=TRUE flag that can be flipped to see more?
Identify data entry points
Look for where you can put data, is it an API? Is there a paywall or sign up ? Is it purely unauthenticated?
Identify the technologies used
Look for what the underlying tech is. useful tool for this is nmap again & for web apps specifically wappalyzer.
Map the attack surface and application
Look at the application from a bad guy perspective, what does it do? what is the most valuable part?
Some applications will value things more than others.
Look at the application logic too, how is business conducted?
* Methods
Scraping
Brute-force
Alterations & permutations of already known subdomains
Online DNS tools
SSL certificates
Certificate Transparency
Search engines
Public datasets
DNS aggregators
Git repositories
Text parsing (HTML, JavaScript, documents…)
VHost discovery
ASN discovery
Reverse DNS
Zone transfer (AXFR)
DNSSEC zone walking
DNS cache snooping
Content-Security-Policy HTTP headers
Sender Policy Framework (SPF) records
Subject Alternate Name (SAN)
* find
OPERATING SYSTEM
WEB SERVER
DATABASE SERVERS
PROGRAMMING LANGUAGES
PLUGINS/VERSIONS
OPEN PORTS
USERNAMES
SERVICES
WEB SPIDERING
GOOGLE HACKING
==========
VECTORS
===========
INPUT FORMS
GET/POST PARAMS
URI/REST STRUCTURE
COOKIES
HEADERS

1) Identifying IP Addresses and Sub-domains
2) Identifying External/3rd Party sites
3) Identifying People
4) Identifying Technologies
5) Identifying Content of Interest
6) Identifying Vulnerabilities

- SOA Records - Indicates the server that has authority for the domain.
- MX Records - List of a host’s or domain’s mail exchanger server(s).
- NS Records - List of a host’s or domain’s name server(s).
- A Records - An address record that allows a computer name to be translated to an IP address.
  Each computer has to have this record for its IP address to be located via DNS.
- PTR Records - Lists a host’s domain name, host identified by its IP address.
- SRV Records - Service location record.
- HINFO Records - Host information record with CPU type and operating system.
- TXT Records - Generic text record.
- CNAME - A host’s canonical name allows additional names/ aliases to be used to locate a computer.
- RP - Responsible person for the domain.

* whois
* wappalyzer
* google HB
* sublist3r
* Async DNS Brute
* gobuster
* sn1p3r
* maltego
* shodan
* OSINT
* intrigue-core
* Recon-Ng
* theHarvester
* SpiderFoot

* dorks
------------------------------------------------------------------------------------------
Explanations:

cache: If you include other words in the query, Google will highlight those words within
the cached document. For instance, [cache:www.google.com web] will show the cached
content with the word “web” highlighted. This functionality is also accessible by
clicking on the “Cached” link on Google’s main results page. The query [cache:] will
show the version of the web page that Google has in its cache. For instance,
[cache:www.google.com] will show Google’s cache of the Google homepage. Note there
can be no space between the “cache:” and the web page url.
------------------------------------------------------------------------------------------
link: The query [link:] will list webpages that have links to the specified webpage.
For instance, [link:www.google.com] will list webpages that have links pointing to the
Google homepage. Note there can be no space between the “link:” and the web page url.
------------------------------------------------------------------------------------------
related: The query [related:] will list web pages that are “similar” to a specified web
page. For instance, [related:www.google.com] will list web pages that are similar to
the Google homepage. Note there can be no space between the “related:” and the web
page url.
------------------------------------------------------------------------------------------
info: The query [info:] will present some information that Google has about that web
page. For instance, [info:www.google.com] will show information about the Google
homepage. Note there can be no space between the “info:” and the web page url.
------------------------------------------------------------------------------------------
define: The query [define:] will provide a definition of the words you enter after it,
gathered from various online sources. The definition will be for the entire phrase
entered (i.e., it will include all the words in the exact order you typed them).
------------------------------------------------------------------------------------------
stocks: If you begin a query with the [stocks:] operator, Google will treat the rest
of the query terms as stock ticker symbols, and will link to a page showing stock
information for those symbols. For instance, [stocks: intc yhoo] will show information
about Intel and Yahoo. (Note you must type the ticker symbols, not the company name.)
------------------------------------------------------------------------------------------
site: If you include [site:] in your query, Google will restrict the results to those
websites in the given domain. For instance, [help site:www.google.com] will find pages
about help within www.google.com. [help site:com] will find pages about help within
.com urls. Note there can be no space between the “site:” and the domain.
------------------------------------------------------------------------------------------
allintitle: If you start a query with [allintitle:], Google will restrict the results
to those with all of the query words in the title. For instance,
[allintitle: google search] will return only documents that have both “google”
and “search” in the title.
------------------------------------------------------------------------------------------
intitle: If you include [intitle:] in your query, Google will restrict the results
to documents containing that word in the title. For instance, [intitle:google search]
will return documents that mention the word “google” in their title, and mention the
word "search" anywhere in the document (title or no). Note there can be no space
between the "intitle:" and the following word. Putting [intitle:] in front of every
word in your query is equivalent to putting [allintitle:] at the front of your
query: [intitle:google intitle:search] is the same as [allintitle: google search].
------------------------------------------------------------------------------------------
allinurl: If you start a query with [allinurl:], Google will restrict the results to
those with all of the query words in the url. For instance, [allinurl: google search]
will return only documents that have both “google” and “search” in the url. Note
that [allinurl:] works on words, not url components. In particular, it ignores
punctuation. Thus, [allinurl: foo/bar] will restrict the results to page with the
words “foo” and “bar” in the url, but won’t require that they be separated by a
slash within that url, that they be adjacent, or that they be in that particular
word order. There is currently no way to enforce these constraints.
------------------------------------------------------------------------------------------
inurl: If you include [inurl:] in your query, Google will restrict the results to
documents containing that word in the url. For instance, [inurl:google search] will
return documents that mention the word “google” in their url, and mention the word
"search" anywhere in the document (url or no). Note there can be no space between
the “inurl:” and the following word. Putting “inurl:” in front of every word in your
query is equivalent to putting “allinurl:” at the front of your query:
[inurl:google inurl:search] is the same as [allinurl: google search].
------------------------------------------------------------------------------------------
* s.engine
* General Search
The main search engines used by users.
Advangle
Aol
Ask
Bing
Dothop
DuckDuckGo - an Internet search engine that emphasizes protecting searchers' privacy.
Factbites
Gigablast
Goodsearch
Google Search - Most popular search engine.
Instya
Impersonal.me
iSEEK Education
ixquick
Lycos
Parseek (Iran)
Search.com
SurfCanyon
Teoma
Wolfram Alpha
Yahoo! Search -

* Main National Search Engines
Localized search engines by country.
Alleba (Philippines) - Philippines search engine
Baidu (China) - The major search engine used in China
Eniro (Sweden)
Goo (Japan)
Najdsi (Slovenia)
Naver (South Korea)
Onet.pl (Poland)
Orange (France)
Parseek (Iran)
SAPO (Portugal)
Search.ch (Switzerland)
Walla (Israel)
Yandex (Russia)

* Meta Search======
Lesser known and used search engines.
All-in-One
AllTheInternet
Etools
FaganFinder
Goofram
iZito
Nextaris
Metabear
Myallsearch
Qwant
Sputtr
Trovando
WebOasis
Zapmeta

* Specialty Search Engines
Search engines for specific information or topics.
2lingual Search
Biznar
CiteSeerX
Digle
Google Custom Search
Harmari (Unified Listings Search)
Internet Archive
Million Short
WorldWideScience.org
Zanran

* Visual Search and Clustering Search Engines
Search engines that scrape multiple sites (Google, Yahoo, Bing, Goo, etc) at the same time and return results.
Carrot2 - Organizes your search results into topics.
Yippy - Search using multiple sources at once

* Similar Sites Search
Find websites that are similar. Good for business competition research.

Google Similar Pages
SimilarSites - Discover websites that are similar to each other
SitesLike - Find similar websites by category

* Document and Slides Search
Search for data located on PDFs, Word documents, presentation slides, and more.
Authorstream
Find-pdf-doc
Free Full PDF
Offshore Leak Database
PasteLert
PDF Search Engine
RECAP
Scribd
SlideShare
Slideworld
soPDF.com

* Pastebins
Find information that has been uploaded to Pastebin.
PasteLert - PasteLert is a simple system to search pastebin.com and set up alerts (like google alerts) for pastebin.com entries.

* Code Search
Search by website source code
NerdyData - Search engine for source code.
SearchCode - Help find real world examples of functions, API's and libraries across 10+ sources.
* resources
- [[http://www.vulnerabilityassessment.co.uk/Penetration%20Test.html][vuln-assess]]
