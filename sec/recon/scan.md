#+title: scan

-------
* robots
#+BEGIN_SRC text
traceroute
nslookup
dirbuster
wireshark
Enum4linux
OpenVas
Zap
SqlMap
#+END_SRC

* scanners
| Netsparker             | Web Application Security Scanner                        |
| Nexpose                | Vulnerability Management & Risk Management Software     |
| Nessus                 | Vulnerability, configuration, and compliance assessment |
| Nikto                  | Web application vulnerability scanner                   |
| OpenVAS                | Open Source vulnerability scanner and manager           |
| OWASP Zed Attack Proxy | Penetration testing tool for web applications           |
| Secapps                | Integrated web application security testing environment |
| w3af                   | Web application attack and audit framework              |
| Wapiti                 | Web application vulnerability scanner                   |
| WebReaver              | Web application vulnerability scanner for Mac OS X      |

* github scripts
| subenum           | enumerates subdomains                                     |
| portenum          | enumerates open ports                                     |
| webenum           | enumerates webservers                                     |
| headenum          | enumerates additional info from webservers                |
| asnenum           | enumerates asn                                            |
| dnsenum           | enumerates dns records                                    |
| conenum           | enumerates hidden files & directories                     |
| urlenum           | enumerates urls                                           |
| subscan           | hunts for subdomain takeovers                             |
| socscan           | scans search engines & webpages for social media accounts |
| cscan             | scan with customized templates                            |
| cvescan           | hunts for CVEs                                            |
| vulnscan          | hunts for other common vulnerabilites                     |
| portscan          | scans open ports                                          |
| parascan          | hunts for vulnerable parameters                           |
| endscan           | hunts for vulnerable endpoints                            |
| buckscan          | hunts for unreferenced aws s3 buckets                     |
| favscan           | fingerprints webservers using favicon                     |
| vizscan           | screenshots applications running on webservers            |
| idscan            | identifies applications running on webservers             |
| enum              | runs all enumerator modules                               |
| scan              | runs all scanner modules                                  |
| recon             | runs all modules                                          |
| hunt              | runs your custom workflow                                 |
| remlog            | removes log files                                         |
| upload            | switches upload functionality                             |
| upgrade           | upgrades kenzer to latest version                         |
| monitor           | monitors ct logs for new subdomains                       |
| monitor normalize | normalizes the enumerations from ct logs                  |
| monitor db        | monitors ct logs for kenzerdb's domains.txt               |
| sync              | synchronizes the local kenzerdb with github               |
| kenzer <module>   | runs a specific modules                                   |
| kenzer man        | shows this manual                                         |


* assets
- https://github.com/hackerscrolls/SecurityTips
* Subdomain Enumeration or Takeover
| https://github.com/lijiejie/subDomainsBrute            | A classical subdomain enumeration                            |
| https://github.com/ring04h/wydomain                    | A Speed and Precision subdomain enumeration                  |
| https://github.com/le4f/dnsmaper                       | Subdomain enumeration tool with map record                   |
| https://github.com/TheRook/subbrute                    | enumerates DNS records, and subdomains,supported API         |
| https://github.com/We5ter/GSDF                         | enumeration via Google certificate transparency              |
| https://github.com/mandatoryprogrammer/cloudflare_enum | Subdomain enumeration via CloudFlare                         |
| https://github.com/guelfoweb/knock                     | Knock subdomain scan                                         |
| https://github.com/code-scan/BroDomain                 | Find brother domain                                          |
| https://github.com/chuhades/dnsbrute                   | A fast domain brute tool                                     |
| https://github.com/yanxiu0614/subdomain3               | A simple and fast tool for bruting subdomains                |
| https://github.com/michenriksen/aquatone               | A powerful subdomain tool and domain takeovers finding tools |
| https://github.com/evilsocket/dnssearch                | A subdomain enumeration tool                                 |
| https://github.com/reconned/domained                   | Subdomain enumeration tools for bug hunting                  |
| https://github.com/bit4woo/Teemo                       | A domain name & Email address collection tool                |
| https://github.com/laramies/theHarvester               | Email, subdomain and people names harvester                  |
| https://github.com/nmalcolm/Inventus                   | find subdomains of a specific domain by crawling it          |
| https://github.com/aboul3la/Sublist3r                  | Fast subdomains enumeration tool for penetration testers     |
| https://github.com/jonluca/Anubis                      | Subdomain enumeration and information gathering tool         |
| https://github.com/n4xh4ck5/N4xD0rk                    | Listing subdomains about a main domain                       |
| https://github.com/infosec-au/altdns                   | Subdomain discovery through alterations and permutations     |
| https://github.com/FeeiCN/ESD                          | sub domains tool,based on AsyncIO and non-repeating dict     |
| https://github.com/UnaPibaGeek/ctfr                    | certificate transparency HTTPS websites subdomains           |
| https://github.com/giovanifss/Dumb                     | Dumain Bruteforcer, a fast and flexible domain bruteforcer   |
| https://github.com/caffix/amass                        | Subdomain enumeration in Go                                  |
| https://github.com/Ice3man543/subfinder                | successor to sublist3r project                               |
| https://github.com/Ice3man543/SubOver                  | A powerful subdomain takeover tool                           |
| https://github.com/janniskirschner/horn3t              | Powerful Visual Subdomain Enumeration                        |
| https://github.com/yunxu1/dnsub                        | A high concurrency subdomain scanner based on Golang         |
| https://github.com/shmilylty/OneForAll                 | scanner integrated multiple subdomain scanning tools         |
| https://github.com/knownsec/ksubdomain                 | speed up to 30w/s on Mac and Windows, and 160w/s on Linux    |
| https://github.com/gwen001/github-subdomains           | Find subdomains on GitHub                                    |

* Database SQL Injection Vulnerability or Brute Force
| https://github.com/0xbug/SQLiScanner                     | A SQLi vulnerability scanner                    |
| https://github.com/stamparm/DSSS                         | A SQLi vulnerability scanner                    |
| https://github.com/youngyangyang04/NoSQLAttack           | A SQLi vulnerability scanner                    |
| https://github.com/Neohapsis/bbqsql                      | A blind SQLi vulnerability scanner              |
| https://github.com/NetSPI/PowerUpSQL                     | A SQLi vulnerability scanner                    |
| https://github.com/WhitewidowScanner/whitewidow          | Another SQL vulnerability scanner               |
| https://github.com/stampery/mongoaudit                   | A powerful MongoDB auditing and pentesting tool |
| https://github.com/torque59/Nosql-Exploitation-Framework | NoSQL scanning and exploitation                 |
| https://github.com/missDronio/blindy                     | brutforcing blind sql injection vulnerabilities |
| https://github.com/fengxuangit/Fox-scan                  | passive SQL injection vulnerable test tools     |
| https://github.com/JohnTroony/Blisqy                     | time-based blind-SQL injection in HTTP-Headers  |
| https://github.com/ron190/jsql-injection                 | find database information from a distant server |
| https://github.com/Hadesy2k/sqliv                        | SQL injection vulnerability scanner             |
| https://github.com/s0md3v/sqlmate                        | friend of SQLmap                                |
| https://github.com/m8r0wn/enumdb                         | MySQL and MSSQL brute force                     |
| https://github.com/tariqhawis/injectbot                  | web-based SQL injection scanner                 |

* Weak Usernames or Passwords Enumeration For Web
https://github.com/lijiejie/htpwdScan - A python HTTP weak pass scanner
https://github.com/netxfly/crack_ssh - SSH, Redis, mongoDB weak password bruteforcer
https://github.com/shengqi158/weak_password_detect - A python HTTP weak password scanner
https://github.com/s0md3v/Blazy - a modern login bruteforcer which also tests for CSRF, Clickjacking, Cloudflare and WAF
https://github.com/MooseDojo/myBFF - Web application brute force framework,supports Citrix Gateway,CiscoVPN and so on
https://github.com/TideSec/web_pwd_common_crack - A common web weak_password cracking script,can detect batches of management backgrounds without verification codes

* Authorization Brute Force or Vulnerability Scan For IoT
https://github.com/rapid7/IoTSeeker - Weak-password IoT devices scanner
https://github.com/shodan-labs/iotdb - IoT Devices scanner via nmap
https://github.com/googleinurl/RouterHunterBR - Testing vulnerabilities in devices and routers connected to the Internet
https://github.com/scu-igroup/telnet-scanner - Weak telnet password scanner based on password enumeration
https://github.com/viraintel/OWASP-Nettacker - Network information gathering vulnerability scanner,most useful to scan IoT
https://github.com/threat9/routersploit - Exploitation Framework for embedded Devices,such as router
https://github.com/w3h/icsmaster/tree/master/nse - Digital bond's ICS enumeration tools

* Mutiple types of Cross-site scripting Detection
https://github.com/0x584A/fuzzXssPHP - A very simple reflected XSS scanner supports GET/POST
https://github.com/chuhades/xss_scan - Reflected XSS scanner
https://github.com/BlackHole1/autoFindXssAndCsrf - A plugin for browser that checks automatically whether a page haves XSS and CSRF vulnerabilities
https://github.com/shogunlab/shuriken - XSS command line tool for testing lists of XSS payloads on web apps
https://github.com/s0md3v/XSStrike - Fuzz and bruteforce parameters for XSS, WAFs detect and bypass
https://github.com/stamparm/DSXS - A fully functional cross-site scripting vulnerability scanner,supporting GET and POST parameters,and written in under 100 lines of code
https://github.com/fcavallarin/domdig - DOM XSS scanner for Single Page Applications
https://github.com/lwzSoviet/NoXss - Faster reflected-xss and dom-xss scanner based on Phantomjs
https://github.com/pwn0sec/PwnXSS - A powerful XSS scanner made in python 3.7

* Enterprise Assets Management or Data Protection
https://github.com/ysrc/xunfeng - Vulnerability rapid response,scanning system for intranet
https://github.com/x0day/Multisearch-v2 - Enterprise assets collector based on search engine
https://github.com/Ekultek/Zeus-Scanner - An advanced dork searching tool that is capable of finding IP address /URL blocked by search engine,and can run sqlmap and nmap scans on the URL's
https://github.com/metac0rtex/GitHarvester - Used for harvesting information from GitHub
https://github.com/repoog/GitPrey - Searching sensitive files and contents in GitHub
https://github.com/0xbug/Hawkeye - Github leak scan for enterprise
https://github.com/UnkL4b/GitMiner - Advanced search tool and automation in Github
https://github.com/dxa4481/truffleHog - Searches high entropy strings through git repositories
https://github.com/1N3/Goohak - Automatically launch Google hacking queries against a target domain
https://github.com/UKHomeOffice/repo-security-scanner - CLI tool that finds secrets accidentally committed to a git repo, eg passwords, private keys
https://github.com/FeeiCN/GSIL - Github sensitive information leakage scan
https://github.com/MiSecurity/x-patrol - Github leaked patrol
https://github.com/anshumanbh/git-all-secrets - A tool to capture all the git secrets by leveraging multiple open source git searching tools
https://github.com/VKSRC/Github-Monitor - Github sensitive information leakage monitor by vipkid SRC
https://github.com/eth0izzle/shhgit - A docker and web based monitor for finding secrets and sensitive files across GitHub
https://github.com/SAP/credential-digger - A GitHub scanning tool that identifies hardcoded credentials, filtering the false positive data through machine learning models.
https://github.com/TophantTechnology/ARL - An agile asset reconnaissance system

* Malicious Scripts or Binary Malware Detection
| https://github.com/he1m4n6a/findWebshell         | Simple webshell detector                             |
| https://github.com/Tencent/HaboMalHunter         | analysis and security assessment on the Linux system |
| https://github.com/PlagueScanner/PlagueScanner   | Open source multiple AV scanner framework            |
| https://github.com/nbs-system/php-malware-finder | detect potentially malicious PHP files               |
| https://github.com/emposha/PHP-Shell-Detector    | identify PHP/Perl/Asp/Aspx shells                    |
| https://github.com/erevus-cn/scan_webshell       | Simple webshell detector                             |
| https://github.com/emposha/Shell-Detector        | find and identify PHP/Perl/Asp/Aspx shells           |
| https://github.com/m4rco-/dorothy2               | malware/botnet analysis framework written in Ruby    |
| https://github.com/droidefense/engine            | Advance Android malware analysis framework           |

* Intranet Penetration
| https://github.com/lcatro/network_backdoor_scanner | An internal network scanner                  |
| https://github.com/fdiskyou/hunter                 | User hunter using WinAPI calls only          |
| https://github.com/BlackHole1/WebRtcXSS            | Use XSS automation invade intranet           |
| https://github.com/Tib3rius/AutoRecon              | A multi-threaded network reconnaissance tool |

* Vulnerability Assessment for Middleware or Information Leak Scan
https://github.com/ring04h/wyportmap - Target port scanning + system service fingerprint recognition
https://github.com/ring04h/weakfilescan - Dynamic multi - thread sensitive information leak detection tool
https://github.com/EnableSecurity/wafw00f - Identify and fingerprint Web Application Firewall
https://github.com/rbsec/sslscan - Tests SSL/TLS enabled services to discover supported cipher suites
https://github.com/TideSec/TideFinger - Web fingerprint identification tool, more fingerprint database, more detection methods
https://github.com/TideSec/FuzzScanner - Comprehensive web information collection platform, easy to deploy, versatile and practical
https://github.com/urbanadventurer/whatweb - Website fingerprinter
https://github.com/tanjiti/FingerPrint - Another website fingerprinter
https://github.com/nanshihui/Scan-T - A new spider based on Python with more function including Network fingerprint search
https://github.com/OffensivePython/Nscan - Fast internet-wide scanner
https://github.com/ywolf/F-NAScan - Scanning a network asset information script
https://github.com/maurosoria/dirsearch - Web path scanner
https://github.com/x0day/bannerscan - C-segment Banner with path scanner
https://github.com/RASSec/RASscan - Internal network port speed scanners
https://github.com/3xp10it/bypass_waf - Automatic WAF Bypass Fuzzing Tool
https://github.com/3xp10it/xcdn - Try to find out the actual ip behind cdn
https://github.com/Xyntax/BingC - Based on the Bing search engine C / side-stop query, multi-threaded, supported API
https://github.com/Xyntax/DirBrute - Multi-thread web directory enumerating tool
https://github.com/zer0h/httpscan - A HTTP service detector with a crawler from IP/CIDR
https://github.com/lietdai/doom - Distributed task distribution of the IP port vulnerability scanner based on thorn
https://github.com/chichou/grab.js - Fast TCP banner grabbing like zgrab, but supports much more protocol
https://github.com/Nitr4x/whichCDN - Detect if a given website is protected by a CDN
https://github.com/secfree/bcrpscan - Base on crawler result web path scanner
https://github.com/mozilla/ssh_scan - A prototype SSH configuration and policy scanner
https://github.com/18F/domain-scan - Scans domains for data on their HTTPS configuration and assorted other things
https://github.com/ggusoft/inforfinder - A tool made to collect information of any domain pointing at a server and fingerprinter
https://github.com/boy-hack/gwhatweb - Fingerprinter for CMS
https://github.com/Mosuan/FileScan - Sensitive files scanner
https://github.com/Xyntax/FileSensor - Dynamic file detection tool based on crawler
https://github.com/deibit/cansina - Web content discovery tool
https://github.com/mozilla/cipherscan - A very simple way to find out which SSL ciphersuites are supported by a target
https://github.com/xmendez/wfuzz - Web application framework and web content scanner
https://github.com/s0md3v/Breacher - An advanced multithreaded admin panel finder written in Python
https://github.com/ztgrace/changeme - A default credential scanner
https://github.com/medbenali/CyberScan - An open source penetration testing tool that can analyse packets,decoding,scanning ports, pinging and geolocation of an IP
https://github.com/m0nad/HellRaiser - HellRaiser scan with nmap then correlates cpe's found with cve-search to enumerate vulnerabilities
https://github.com/scipag/vulscan - Advanced vulnerability scanning with Nmap NSE
https://github.com/jekyc/wig - WebApp information gatherer
https://github.com/eldraco/domain_analyzer - Analyze the security of any domain by finding all the information possible
https://github.com/cloudtracer/paskto - Passive directory scanner and web crawler based on Nikto DB
https://github.com/zerokeeper/WebEye - A web service and WAF fingerprinter
https://github.com/m3liot/shcheck - Just check security headers on a target website
https://github.com/aipengjie/sensitivefilescan - A speed and awesome sensitive files scanner
https://github.com/fnk0c/cangibrina - A fast and powerfull dashboard (admin) finder
https://github.com/n4xh4ck5/CMSsc4n - Tool to identify if a domain is a CMS such as Wordpress, Moodle, Joomla
https://github.com/Ekultek/WhatWaf - Detect and bypass web application firewalls and protection systems
https://github.com/dzonerzy/goWAPT - Go web application penetration test and web application fuzz tool
https://github.com/blackye/webdirdig - Sensitive files scanner
https://github.com/GitHackTools/BillCipher - Information gathering tool for a website or IP address
https://github.com/boy-hack/w8fuckcdn - Get the website real IP address by scanning the entire net
https://github.com/boy-hack/w11scan - Distributed web fingerprint identification platform
https://github.com/Nekmo/dirhunt - Find web directories without bruteforce
https://github.com/MetaChar/pyHAWK - Searches the directory of choice for interesting files. Such as database files and files with passwords stored on them
https://github.com/H4ckForJob/dirmap - An advanced web directory scanning tool that will be more powerful than DirBuster, Dirsearch, cansina, and Yu Jian
https://github.com/s0md3v/Photon - **Incredibly fast crawler which extracts urls, emails, files, website accounts and much more
https://github.com/1N3/BlackWidow - Gather OSINT and fuzz for OWASP vulnerabilities on a target website
https://github.com/saeeddhqan/Maryam - OSINT and Web-based Footprinting modular framework based on the Recon-ng

* Special Components or Vulnerability Categories Scan
https://github.com/1N3/XSSTracer - A small python script to check for cross-Site tracing, Clickjacking etc
https://github.com/0xHJK/dumpall - .git / .svn / .DS_Store disclosure exploit
https://github.com/shengqi158/svnhack - A .svn folder disclosure exploit
https://github.com/lijiejie/GitHack - A .git folder disclosure exploit
https://github.com/blackye/Jenkins - Jenkins vulnerability detection, user grab enumerating
https://github.com/code-scan/dzscan - discuz scanner
https://github.com/chuhades/CMS-Exploit-Framework -CMS exploit framework
https://github.com/lijiejie/IIS_shortname_Scanner - An IIS shortname scanner
https://github.com/riusksk/FlashScanner - Flash XSS scanner
https://github.com/epinna/tplmap - Automatic Server-Side Template Injection detection and exploitation tool
https://github.com/cr0hn/dockerscan - Docker security analysis & hacking tools
https://github.com/m4ll0k/WPSeku - Simple Wordpress security scanner
https://github.com/rastating/wordpress-exploit-framework - Ruby framework for developing and using modules which aid in the penetration testing of WordPress powered websites and systems
https://github.com/ilmila/J2EEScan - A plugin for Burp Suite proxy to improve the test coverage during web application penetration tests on J2EE applications
https://github.com/riusksk/StrutScan - Struts2 vuls scanner base Perl script
https://github.com/D35m0nd142/LFISuite - Totally automatic LFI exploiterand scanner supports reverse shell
https://github.com/0x4D31/salt-scanner - Linux vulnerability scanner based on Salt Open and vulners audit API, with Slack notifications and JIRA integration
https://github.com/tijme/angularjs-csti-scanner - Automated client-side template injection detection for AngularJS
https://github.com/irsdl/IIS-ShortName-Scanner - Scanners for IIS short filename 8.3 disclosure vulnerability
https://github.com/swisskyrepo/Wordpresscan - WPScan rewritten in Python + some WPSeku ideas
https://github.com/CHYbeta/cmsPoc - CMS exploit framework
https://github.com/3gstudent/Smbtouch-Scanner - Automatically scan the inner network to detect whether they are vulnerable
https://github.com/OsandaMalith/LFiFreak - A unique automated LFI exploiter with bind/reverse shells
https://github.com/mak-/parameth - This tool can be used to brute discover GET and POST parameters
https://github.com/Lucifer1993/struts-scan - Struts2 vuls scanner,supported all vuls
https://github.com/hahwul/a2sv - Auto scanning to SSL vulnerability, such as heartbleed etc
https://github.com/NickstaDB/BaRMIe - Java RMI enumeration and attack tool
https://github.com/RetireJS/grunt-retire - Scanner detecting the use of Javascript libraries with known vulnerabilities
https://github.com/kotobukki/BDA - The vulnerability detector for Hadoop and Spark
https://github.com/jagracey/Regex-DoS - RegEx Denial of service scanner for Node.js package
https://github.com/milesrichardson/docker-onion-nmap - Scan .onion hidden services with nmap using Tor, proxychains and dnsmasq
https://github.com/Moham3dRiahi/XAttacker - Web CMS exploit framework
https://github.com/lijiejie/BBScan - A tiny batch web vulnerability scanner
https://github.com/almandin/fuxploider - File upload vulnerability scanner and exploitation tool
https://github.com/Jamalc0m/wphunter - A Wordpress vulnerability scanner
https://github.com/retirejs/retire.js - A scanner detecting the use of Javascript libraries with known vulnerabilities
https://github.com/3xp10it/xupload - A tool for automatically testing whether the upload function can upload webshell
https://github.com/rezasp/vbscan - OWASP VBScan is a Black Box vBulletin vulnerability scanner
https://github.com/MrSqar-Ye/BadMod - Detect websites CMS & auto exploit
https://github.com/Tuhinshubhra/CMSeeK - CMS detection and exploitation suite
https://github.com/cloudsploit/scans - AWS security scanning checks
https://github.com/radenvodka/SVScanner - Scanner vulnerability and massive exploit for Wordpress,Magento,Joomla and so on
https://github.com/rezasp/joomscan - OWASP Joomla vulnerability scanner project
https://github.com/6IX7ine/djangohunter - Tool designed to help identify incorrectly configured Django applications that are exposing sensitive information
https://github.com/vulmon/Vulmap - Local vulnerability scanning programs for Windows and Linux operating systems
https://github.com/seungsoo-lee/DELTA - Sdn security evaluation framework
https://github.com/thelinuxchoice/facebash - Facebook Brute Forcer in shellscript using TOR
https://github.com/cyberark/KubiScan - A tool to scan Kubernetes cluster for risky permissions

* Vulnerability Assessment for Wireless Network
https://github.com/savio-code/fern-wifi-cracker - Testing and discovering flaws in ones own network
https://github.com/P0cL4bs/WiFi-Pumpkin - Framework for Rogue Wi-Fi Access Point Attack
https://github.com/MisterBianco/BoopSuite - A Suite of Tools written in Python for wireless auditing and security testing
https://github.com/besimaltnok/PiFinger - Searches for wifi-pineapple traces and calculate wireless network security score
https://github.com/derv82/wifite2 - A complete re-write of Wifite,Automated Wireless Attack Tool

* Local Area Network detection
https://github.com/m4n3dw0lf/PytheM - Multi-purpose network pentest framework
https://github.com/sowish/LNScan - Local Network Scanner based on BBScan via.lijiejie
https://github.com/niloofarkheirkhah/nili - Tool for Network Scan, Man in the Middle, Protocol Reverse Engineering and Fuzzing
https://github.com/SkyLined/LocalNetworkScanner - PoC Javascript that scans your local network when you open a webpage

* Dynamic or Static Code Analysis
https://github.com/wufeifei/cobra - A static code analysis system that automates the detecting vulnerabilities and security issue
https://github.com/OneSourceCat/phpvulhunter - A tool that can scan php vulnerabilities automatically using static analysis methods
https://github.com/Qihoo360/phptrace - A tracing and troubleshooting tool for PHP scripts
https://github.com/ajinabraham/NodeJsScan - A static security code scanner for Node.js applications
https://github.com/shengqi158/pyvulhunter - A static security code scanner for Python applications
https://github.com/python-security/pyt - A static analysis tool for detecting security vulnerabilities in Python web applications
https://github.com/emanuil/php-reaper - PHP tool to scan ADOdb code for SQL injections
https://github.com/lowjoel/phortress - A PHP static code analyser for potential vulnerabilities

* Modular Design Scanners or Vulnerability Detecting Framework
https://github.com/infobyte/faraday - Collaborative penetration test and vulnerability management platform
https://github.com/az0ne/AZScanner - Automatic all-around scanner
https://github.com/blackye/lalascan - Distributed web vulnerability scanning framework
https://github.com/blackye/BkScanner - Distributed, plug-in web vulnerability scanner
https://github.com/ysrc/GourdScanV2 - Passive vulnerability scanning system
https://github.com/netxfly/passive_scan - Realization of web vulnerability scanner based on http proxy
https://github.com/1N3/Sn1per - Automated pentest recon scanner
https://github.com/RASSec/pentestEr_Fully-automatic-scanner - Directional fully automated penetration testing
https://github.com/Xyntax/POC-T - Penetration test plug-in concurrency framework
https://github.com/v3n0m-Scanner/V3n0M-Scanner - Scanner in Python3.5 for SQLi/XSS/LFI/RFI and other vulns
https://github.com/Skycrab/leakScan - Multiple vuls scan supports web interface
https://github.com/zhangzhenfeng/AnyScan - A automated penetration testing framework
https://github.com/Tuhinshubhra/RED_HAWK - An all In one tool For information gathering, SQL vulnerability scanning and crawling, coded In PHP
https://github.com/swisskyrepo/DamnWebScanner - Another web vulnerabilities scanner, this extension works on Chrome and Opera
https://github.com/anilbaranyelken/tulpar - Web Vulnerability Scanner written in Python,supported multiple web vulnerabilities scan
https://github.com/m4ll0k/Spaghetti - A web application security scanner tool,designed to find various default and insecure files, configurations and misconfigurations
https://github.com/Yukinoshita47/Yuki-Chan-The-Auto-Pentest - An automated penetration testing tool this tool will auditing all standard security test method for you
https://github.com/0xsauby/yasuo - ruby script that scans for vulnerable & exploitable 3rd-party web applications on a network
https://github.com/hatRiot/clusterd - application server attack toolkit
https://github.com/erevus-cn/pocscan - Open source and distributed web vulnerability scanning framework
https://github.com/TophantTechnology/osprey - Distributed web vulnerability scanning framework
https://github.com/yangbh/Hammer - A web vulnerability scanner framework
https://github.com/Lucifer1993/AngelSword - Web vulnerability scanner framework based on python3
https://github.com/zaproxy/zaproxy - One of the world’s most popular free security tools and is actively maintained by hundreds of international volunteers
https://github.com/s0md3v/Striker - Striker is an offensive information and vulnerability scanner
https://github.com/dermotblair/webvulscan - Written in PHP and can be used to test remote, or local, web applications for security vulnerabilities
https://github.com/alienwithin/OWASP-mth3l3m3nt-framework - Penetration testing aiding tool and exploitation framework
https://github.com/toyakula/luna - An open-source web security scanner which is based on reduced-code passive scanning framework
https://github.com/Manisso/fsociety - A Penetration Testing Framework including Information Gathering,Wireless Testing,Web Hacking and so on
https://github.com/boy-hack/w9scan - A web vulnerability scanner framework,running with 1200+ plugins
https://github.com/YalcinYolalan/WSSAT - Web service security assessment tool,provide simple .exe application to use based on Windows OS
https://github.com/AmyangXYZ/AssassinGo - An extenisble and concurrency pentest framework in Go
https://github.com/m4ll0k/Galileo - Web application audit framework,like metasploit
https://github.com/joker25000/Optiva-Framework - Web Application Scanner
https://github.com/theInfectedDrake/TIDoS-Framework - The offensive web application penetration testing framework
https://github.com/TideSec/WDScanner - A full-featured vulnerability scanner for enterprise security
https://github.com/j3ssie/Osmedeus - Fully automated offensive security tool for reconnaissance and vulnerability scanning
https://github.com/jeffzh3ng/Fuxi-Scanner - Open source network security vulnerability scanner with asset discovery & management
https://github.com/knownsec/Pocsuite - Open-sourced remote vulnerability testing framework
https://github.com/opensec-cn/kunpeng - An open source POC framework written by Golang that provides various language calls in the form of a dynamic link library
https://github.com/jaeles-project/jaeles - The Swiss Army knife for automated Web Application Testing
https://github.com/TideSec/Mars - The totally new generation of WDScanner
https://github.com/knassar702/scant3r - Yet another web security scanner
https://github.com/google/tsunami-security-scanner - A general purpose network security scanner with an extensible plugin system for detecting high severity vulnerabilities with high confidenc by Google
https://github.com/er10yi/MagiCude - A scanner based on the Spring Boot micro-service,supports distributed port (vulnerability) scanning, asset security management, real-time threat monitoring and notification, vulnerability lifecycle, vulnerability wiki, email notification, etc

* Advanced Persistent Threat
| https://github.com/Neo23x0/Loki   | Simple IOC and Incident Response Scanner |
| https://github.com/Neo23x0/Fenrir | Simple IOC and Incident Response Scanner |

* Mobile Apps Code Analysis
| https://github.com/dwisiswant0/apkleaks     | Scanning APK file for URIs, endpoints & secrets                  |
| https://github.com/kelvinBen/AppInfoScanner | Collecting information from APK file, support self-defined rules |

* headers
https://github.com/rfc-st/humble
https://github.com/koenbuyens/securityheaders
