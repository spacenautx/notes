# defender

- https://xret2pwn.github.io/Windows_Defender_Bypass/
- https://github.com/tastypepperoni/RunAsWinTcb
- https://sp00ks-git.github.io/posts/CLM-Bypass/
- https://github.com/huntandhackett/concealed_code_execution
- https://github.com/pwn1sher/KillDefender
- https://github.com/lab52io/StopDefender 
- https://github.com/EspressoCake/Defender_Exclusions-BOF
- https://github.com/S12cybersecurity/WinDefenderKiller
- https://github.com/reveng007/DarkWidow
