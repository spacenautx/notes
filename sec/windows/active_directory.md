# active directory

- Summary
This document was designed to be a useful, informational asset for those looking to understand the specific tactics, techniques, and procedures (TTPs) attackers are leveraging to compromise active directory and guidance to mitigation, detection, and prevention. And understand Active Directory Kill Chain Attack and Modern Post Exploitation Adversary Tradecraft Activity.

- https://casvancooten.com/posts/2020/11/windows-active-directory-exploitation-cheat-sheet-and-command-reference/
- https://gitlab.com/pentest-tools/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Active%20Directory%20Attack.md
- https://github.com/S1ckB0y1337/Active-Directory-Exploitation-Cheat-Sheet
- https://xakep.ru/2019/10/16/windows-ad-hack/
- https://github.com/Seji64/LAPS-WebUI
- https://github.com/Integration-IT/Active-Directory-Exploitation-Cheat-Sheet

- Active Directory Kill Chain Attack & Defense


Table of Contents
Discovery
Privilege Escalation
Defense Evasion
Credential Dumping
Lateral Movement
Persistence
Defense & Detection


- Discovery
SPN Scanning

SPN Scanning – Service Discovery without Network Port Scanning
https://adsecurity.org/?p=1508

Active Directory: PowerShell script to list all SPNs used
https://social.technet.microsoft.com/wiki/contents/articles/18996.active-directory-powershell-script-to-list-all-spns-used.aspx

Discovering Service Accounts Without Using Privileges
https://blog.stealthbits.com/discovering-service-accounts-without-using-privileges/

- Data Mining

A Data Hunting Overview

Push it, Push it Real Good
https://thevivi.net/2018/05/23/a-data-hunting-overview/

Finding Sensitive Data on Domain SQL Servers using PowerUpSQL
https://www.harmj0y.net/blog/redteaming/push-it-push-it-real-good/

Sensitive Data Discovery in Email with MailSniper
https://blog.netspi.com/finding-sensitive-data-domain-sql-servers-using-powerupsql/

Remotely Searching for Sensitive Files
https://www.youtube.com/watch?v=ZIOw_xfqkKM
Hidden Administrative Accounts: BloodHound to the Rescue
https://www.crowdstrike.com/blog/hidden-administrative-accounts-bloodhound-to-the-rescue/

Active Directory Recon Without Admin Rights
https://adsecurity.org/?p=2535

Gathering AD Data with the Active Directory PowerShell Module
https://adsecurity.org/?p=3719

Using ActiveDirectory module for Domain Enumeration from PowerShell Constrained Language Mode
http://www.labofapenetrationtester.com/2018/10/domain-enumeration-from-PowerShell-CLM.html

PowerUpSQL Active Directory Recon Functions
https://github.com/NetSPI/PowerUpSQL/wiki/Active-Directory-Recon-Functions

Derivative Local Admin
https://www.sixdub.net/?p=591

Dumping Active Directory Domain Info – with PowerUpSQL!
https://blog.netspi.com/dumping-active-directory-domain-info-with-powerupsql/

Local Group Enumeration
https://www.harmj0y.net/blog/redteaming/local-group-enumeration/

Attack Mapping With Bloodhound
https://blog.stealthbits.com/local-admin-mapping-bloodhound


Situational Awareness
https://pentestlab.blog/2018/05/28/situational-awareness/

Commands for Domain Network Compromise
https://www.javelin-networks.com/static/5fcc6e84.pdf
Hidden Administrative Accounts: BloodHound to the Rescue
Learn how cybercriminals use hidden administrative accounts to access your data and why BloodHound is the tool red teams use to find them.

Active Directory Security
Active Directory Recon Without Admin Rights
A fact that is often forgotten (or misunderstood), is that most objects and their attributes can be viewed (read) by authenticated users (most often, domain users). The challenge is that admins may think that since this data is most easily accessible via admin tools such as "...

Active Directory Security
Gathering AD Data with the Active Directory PowerShell Module
Microsoft provided several Active Directory PowerShell cmdlets with Windows Server 2008 R2 (and newer) which greatly simplify tasks which previously required putting together lengthy lines of code involving ADSI. On a Windows client, install the Remote Sever Administration To...

Using ActiveDirectory module for Domain Enumeration from PowerShel...
Home of Nikhil SamratAshok Mittal. Posts about Red Teaming, Offensive PowerShell, Active Directory and Pen Testing.

GitHub
https://github.com/NetSPI/PowerUpSQL
PowerUpSQL: A PowerShell Toolkit for Attacking SQL Server - NetSPI/PowerUpSQL

A Pentester’s Guide to Group Scoping


LAPS

Microsoft LAPS Security & Active Directory LAPS Configuration Recon
https://adsecurity.org/?p=3164

Running LAPS with PowerView
https://www.harmj0y.net/blog/powershell/running-laps-with-powerview/

RastaMouse LAPS Part 1 & 2
https://rastamouse.me/tags/laps/


AppLocker

Enumerating AppLocker Config
https://rastamouse.me/2018/09/enumerating-applocker-config/


Privilege Escalation

Passwords in SYSVOL & Group Policy Preferences

Finding Passwords in SYSVOL & Exploiting Group Policy Preferences
https://adsecurity.org/?p=2288

Pentesting in the Real World: Group Policy Pwnage
https://blog.rapid7.com/2016/07/27/pentesting-in-the-real-world-group-policy-pwnage/

MS14-068 Kerberos Vulnerability

MS14-068: Vulnerability in (Active Directory) Kerberos Could Allow Elevation of Privilege
https://adsecurity.org/?p=525

Digging into MS14-068, Exploitation and Defence
https://labs.mwrinfosecurity.com/blog/digging-into-ms14-068-exploitation-and-defence/

From MS14-068 to Full Compromise – Step by Step
https://www.trustedsec.com/2014/12/ms14-068-full-compromise-step-step/
Active Directory Security
Microsoft LAPS Security & Active Directory LAPS Configuration Recon
Over the years, there have been several methods attempted for managing local Administrator accounts: Scripted password change - Don't do this. The password is exposed in SYSVOL. Group Policy Preferences. The credentials are exposed in SYSVOL. Password vault/safe product (Thyc...

Running LAPS with PowerView
A year ago, Microsoft released the Local Administrator Password Solution (LAPS) which aims to prevent the reuse of local administrator passwords by setting, “…a different, random passwo…

Enumerating AppLocker Config
Very quick post to explore some different ways to enumerate the AppLocker configuration being applied to a host, both remotely and locally. Understanding these rules, particularly deny rules, are useful for engineering bypasses.

Active Directory Security
Finding Passwords in SYSVOL & Exploiting Group Policy Preferences
At Black Hat and DEF CON this year, I spoke about ways attackers go from Domain User to Domain Admin in modern enterprises. Every Windows computer has a built-in Administrator account with an associated password. Changing this password is a security requirement in most organi...

DNSAdmins

Abusing DNSAdmins privilege for escalation in Active Directory
http://www.labofapenetrationtester.com/2017/05/abusing-dnsadmins-privilege-for-escalation-in-active-directory.html

From DNSAdmins to Domain Admin, When DNSAdmins is More than Just DNS Administration
https://adsecurity.org/?p=4064


Unconstrained Delegation

Domain Controller Print Server + Unconstrained Kerberos Delegation = Pwned Active Directory Forest
https://adsecurity.org/?p=4056

Active Directory Security Risk #101: Kerberos Unconstrained Delegation (or How Compromise of a Single Server Can Compromise the Domain)
https://adsecurity.org/?p=1667

Unconstrained Delegation Permissions
https://blog.stealthbits.com/unconstrained-delegation-permissions/

Trust? Years to earn, seconds to break
https://labs.mwrinfosecurity.com/blog/trust-years-to-earn-seconds-to-break/

Hunting in Active Directory: Unconstrained Delegation & Forests Trusts
https://posts.specterops.io/hunting-in-active-directory-unconstrained-delegation-forests-trusts-71f2b33688e1

Constrained Delegation

Another Word on Delegation
https://www.harmj0y.net/blog/redteaming/another-word-on-delegation/

https://www.harmj0y.net/blog/redteaming/from-kekeo-to-rubeus/

http://www.harmj0y.net/blog/activedirectory/s4u2pwnage/

Kerberos Delegation, Spns And More...
https://www.secureauth.com/blog/kerberos-delegation-spns-and-more

Wagging the Dog: Abusing Resource-Based Constrained Delegation to Attack Active Directory
https://shenaniganslabs.io/2019/01/28/Wagging-the-Dog.html
Abusing DNSAdmins privilege for escalation in Active Directory
Home of Nikhil SamratAshok Mittal. Posts about Red Teaming, Offensive PowerShell, Active Directory and Pen Testing.

Active Directory Security
From DNSAdmins to Domain Admin, When DNSAdmins is More than Just D...
It's been almost 1.5 years since the Medium post by Shay Ber was published that explained how to execute a DLL as SYSTEM on a Domain Controller provided the account is a member of DNSAdmins. I finally got around to posting here since many I speak with aren't aware of this iss...
Active Directory Security
Domain Controller Print Server + Unconstrained Kerberos Delegation...
At DerbyCon 8 (2018) over the weekend Will Schroeder (@Harmj0y), Lee Christensen (@Tifkin_), & Matt Nelson (@enigma0x3), spoke about the unintended risks of trusting AD. They cover a number of interesting persistence and privilege escalation methods, though one in particular ...

Active Directory Security
Active Directory Security Risk #101: Kerberos Unconstrained Delega...
At Black Hat USA 2015 this summer (2015), I spoke about the danger in having Kerberos Unconstrained Delegation configured in the environment. When Active Directory was first released with Windows 2000 Server, Microsoft had to provide a simple mechanism to support scenarios wh...

Insider Threat Security Blog
Unconstrained Delegation Permissions | Insider Threat Security Blog
Discovering computers with Kerberos unconstrained delegation in Active Directory using PowerShell module cmdlet Get-ADComputer

Insecure Group Policy Object Permission Rights

Abusing GPO Permissions
https://www.harmj0y.net/blog/redteaming/abusing-gpo-permissions/

A Red Teamer’s Guide to GPOs and OUs
https://wald0.com/?p=179

File templates for GPO Abuse
https://github.com/rasta-mouse/GPO-Abuse

GPO Abuse - Part 1
https://rastamouse.me/2019/01/gpo-abuse-part-1/



Insecure ACLs Permission Rights

Exploiting Weak Active Directory Permissions With Powersploit
https://blog.stealthbits.com/exploiting-weak-active-directory-permissions-with-powersploit/

Escalating privileges with ACLs in Active Directory
https://blog.fox-it.com/2018/04/26/escalating-privileges-with-acls-in-active-directory/

Abusing Active Directory Permissions with PowerView
http://www.harmj0y.net/blog/redteaming/abusing-active-directory-permissions-with-powerview/

BloodHound 1.3 – The ACL Attack Path Update
https://wald0.com/?p=112

Scanning for Active Directory Privileges & Privileged Accounts
https://adsecurity.org/?p=3658

Active Directory Access Control List – Attacks and Defense
https://techcommunity.microsoft.com/t5/Enterprise-Mobility-Security/Active-Directory-Access-Control-List-8211-Attacks-and-Defense/ba-p/250315

Active Directory ACL exploitation with BloodHound
https://www.slideshare.net/DirkjanMollema/aclpwn-active-directory-acl-exploitation-with-bloodhound
Abusing GPO Permissions
A friend (@piffd0s) recently ran into a specific situation I hadn’t encountered before: the domain controllers and domain admins of the environment he was assessing were extremely locked down…

A Red Teamer’s Guide to GPOs and OUs
Intro Active Directory is a vast, complicated landscape comprised of users, computers, and groups, and the complex, intertwining permissions and privileges that connect them. The initial rel…

GitHub
https://github.com/rasta-mouse/GPO-Abuse
File templates for GPO Abuse. Contribute to rasta-mouse/GPO-Abuse development by creating an account on GitHub.

GPO Abuse - Part 1
Group Policy Objects (GPOs) is a subject I’ve wanted to write about for a long time and I’m happy to have finally started.

Insider Threat Security Blog
Exploiting Weak Active Directory Permissions with the PowerSploit ...
Active Directory security groups targeted by attackers who exploit AD permissions using PowerShell PowerSploit to find weak Active Directory permissions.


Privilege Escalation With DCShadow
https://blog.stealthbits.com/privilege-escalation-with-dcshadow/

https://pentestlab.blog/2018/04/16/dcshadow/

A technical deep dive into the latest AD attack technique
https://blog.alsid.eu/dcshadow-explained-4510f52fc19d

Silently turn off Active Directory Auditing
http://www.labofapenetrationtester.com/2018/05/dcshadow-sacl.html

Minimal permissions, Active Directory Deception, Shadowception and more
http://www.labofapenetrationtester.com/2018/04/dcshadow.html



Rid Hijacking: When Guests Become Admins
https://blog.stealthbits.com/rid-hijacking-when-guests-become-admins/



Microsoft SQL Server

How to get SQL Server Sysadmin Privileges as a Local Admin with PowerUpSQL
https://blog.netspi.com/get-sql-server-sysadmin-privileges-local-admin-powerupsql/

Compromise With Powerupsql – Sql Attacks
https://blog.stealthbits.com/compromise-with-powerupsql-sql-attacks/



Attack and defend Microsoft Enhanced Security Administrative
https://download.ernw-insight.de/troopers/tr18/slides/TR18_AD_Attack-and-Defend-Microsoft-Enhanced-Security.pdf
Insider Threat Security Blog
Privilege Escalation with DCShadow | Insider Threat Security Blog
So far we’ve covered how DCShadow works as well as ways this can enable attackers to create persistence within a domain without detection once they’ve obtained admin credentials.  DCShadow can enable attack scenarios beyond just creating persistence, and can actually be ...

Penetration Testing Lab
The DCShadow is an attack which tries to modify existing data in the Active Directory by using legitimate API’s which are used by domain controllers. This technique can be used in a workstati…

Medium
DCShadow explained: A technical deep dive into the latest AD attac...
A technical deep dive into the latest AD attack technique.

Silently turn off Active Directory Auditing using DCShadow
Home of Nikhil SamratAshok Mittal. Posts about Red Teaming, Offensive PowerShell, Active Directory and Pen Testing.

DCShadow - Minimal permissions, Active Directory Deception, Shadow...
Home of Nikhil SamratAshok Mittal. Posts about Red Teaming, Offensive PowerShell, Active Directory and Pen Testing.

Exchange

Exchange-AD-Privesc
https://github.com/gdedrouas/Exchange-AD-Privesc

Abusing Exchange: One API call away from Domain Admin
https://dirkjanm.io/abusing-exchange-one-api-call-away-from-domain-admin/

NtlmRelayToEWS
https://github.com/Arno0x/NtlmRelayToEWS


NTML Relay

Pwning with Responder – A Pentester’s Guide
https://www.notsosecure.com/pwning-with-responder-a-pentesters-guide/

Practical guide to NTLM Relaying in 2017 (A.K.A getting a foothold in under 5 minutes)
https://byt3bl33d3r.github.io/practical-guide-to-ntlm-relaying-in-2017-aka-getting-a-foothold-in-under-5-minutes.html

Relaying credentials everywhere with ntlmrelayx
https://www.fox-it.com/en/insights/blogs/blog/inside-windows-network/
GitHub
https://github.com/gdedrouas/Exchange-AD-Privesc
Exchange privilege escalations to Active Directory - gdedrouas/Exchange-AD-Privesc

dirkjanm.io
Abusing Exchange: One API call away from Domain Admin
In most organisations using Active Directory and Exchange, Exchange servers have such high privileges that being an Administrator on an Exchange server is enough to escalate to Domain Admin. Recently I came across a blog from the ZDI, in which they detail a way to let Exchang...
GitHub
https://github.com/Arno0x/NtlmRelayToEWS
ntlm relay attack to Exchange Web Services. Contribute to Arno0x/NtlmRelayToEWS development by creating an account on GitHub.

NotSoSecure
Pwning with Responder - A Pentester's Guide - NotSoSecure
Overview: Responder is a great tool that every pentester needs in their arsenal. If a client/target cannot resolve a name via DNS it will fall back to name resolution via LLMNR (introduced in Windows Vista) and NBT-NS. Now, assuming we have Responder running we will essential...

Practical guide to NTLM Relaying in 2017 (A.K.A getting a foothold...
byt3bl33d3r, /dev/random > blog.py
SEP-11-2019 PART-4

Lateral Movement

Microsoft SQL Server Database links

SQL Server – Link… Link… Link… and Shell: How to Hack Database Links in SQL Server!
https://blog.netspi.com/how-to-hack-database-links-in-sql-server/

SQL Server Link Crawling with PowerUpSQL
https://blog.netspi.com/sql-server-link-crawling-powerupsql/



Pass The Hash

Performing Pass-the-hash Attacks With Mimikatz
https://blog.stealthbits.com/passing-the-hash-with-mimikatz

How to Pass-the-Hash with Mimikatz
https://blog.cobaltstrike.com/2015/05/21/how-to-pass-the-hash-with-mimikatz/

Pass-the-Hash Is Dead: Long Live LocalAccountTokenFilterPolicy
https://www.harmj0y.net/blog/redteaming/pass-the-hash-is-dead-long-live-localaccounttokenfilterpolicy/

System Center Configuration Manager (SCCM)

Targeted Workstation Compromise With Sccm
https://enigma0x3.net/2015/10/27/targeted-workstation-compromise-with-sccm/

PowerSCCM - PowerShell module to interact with SCCM deployments
https://github.com/PowerShellMafia/PowerSCCM

WSUS
Remote Weaponization of WSUS MITM
https://www.sixdub.net/?p=623

WSUSpendu
https://www.blackhat.com/docs/us-17/wednesday/us-17-Coltel-WSUSpendu-Use-WSUS-To-Hang-Its-Clients-wp.pdf

Leveraging WSUS – Part One
https://ijustwannared.team/2018/10/15/leveraging-wsus-part-one/
Insider Threat Security Blog
Performing Pass-the-Hash Attacks with Mimikatz | Insider Threat Blog
Protecting Against Pass-the-Hash Attacks

Strategic Cyber LLC
How to Pass-the-Hash with Mimikatz
I’m spending a lot of time with mimikatz lately. I’m fascinated by how much capability it has and I’m constantly asking myself, what’s the best way to use this during a red team e…

Pass-the-Hash Is Dead: Long Live LocalAccountTokenFilterPolicy
Nearly three years ago, I wrote a post named “Pass-the-Hash is Dead: Long Live Pass-the-Hash” that detailed some operational implications of Microsoft’s KB2871997 patch. A specific sentence in the …

SEP-11-2019 PART-5

Defense Evasion

In-Memory Evasion

Bypassing Memory Scanners with Cobalt Strike and Gargoyle
https://labs.mwrinfosecurity.com/blog/experimenting-bypassing-memory-scanners-with-cobalt-strike-and-gargoyle/

In-Memory Evasions Course
https://www.youtube.com/playlist?list=PL9HO6M_MU2nc5Q31qd2CwpZ8J4KFMhgnK

Bring Your Own Land (BYOL) – A Novel Red Teaming Technique
https://www.fireeye.com/blog/threat-research/2018/06/bring-your-own-land-novel-red-teaming-technique.html

Endpoint Detection and Response (EDR) Evasion


Red Teaming in the EDR age
https://www.youtube.com/watch?v=l8nkXCOYQC4&feature=youtu.be

Sharp-Suite - Process Argument Spoofing
https://github.com/FuzzySecurity/Sharp-Suite

OPSEC


Modern Defenses and YOU!
https://blog.cobaltstrike.com/2017/10/25/modern-defenses-and-you/

OPSEC Considerations for Beacon Commands
https://blog.cobaltstrike.com/2017/06/23/opsec-considerations-for-beacon-commands/

Red Team Tradecraft and TTP Guidance
https://sec564.com/#!docs/tradecraft.md

Fighting the Toolset
https://www.youtube.com/watch?v=RoqVunX_sqA
MWR Labs
YouTube
In-memory Evasion (2018) - YouTube
In-memory Evasion is a four-part mini course on the cat and mouse game related to memory detections. This course is for red teams that want to update their t...

Bring Your Own Land (BYOL) – A Novel Red Teaming Technique
By developing custom C#-based assemblies, attackers no longer need to rely on the tools present on the target system; they can instead write and deliver their own tools using a technique we call Bring Your Own Land (BYOL).

YouTube
Wild West Hackin' Fest
Red Teaming in the EDR age

GitHub
https://github.com/FuzzySecurity/Sharp-Suite
My musings with C#. Contribute to FuzzySecurity/Sharp-Suite development by creating an account on GitHub.

SEP-11-2019 PART-6
Microsoft ATA & ATP Evasion


Red Team Techniques for Evading, Bypassing, and Disabling MS Advanced Threat Protection and Advanced Threat Analytics
https://www.blackhat.com/docs/eu-17/materials/eu-17-Thompson-Red-Team-Techniques-For-Evading-Bypassing-And-Disabling-MS-Advanced-Threat-Protection-And-Advanced-Threat-Analytics.pdf

Red Team Revenge - Attacking Microsoft ATA
https://www.slideshare.net/nikhil_mittal/red-team-revenge-attacking-microsoft-ata

Evading Microsoft ATA for Active Directory Domination
https://www.slideshare.net/nikhil_mittal/evading-microsoft-ata-for-active-directory-domination

PowerShell ScriptBlock Logging Bypass

PowerShell ScriptBlock Logging Bypass
https://cobbr.io/ScriptBlock-Logging-Bypass.html

PowerShell Anti-Malware Scan Interface (AMSI) Bypass


How to bypass AMSI and execute ANY malicious Powershell code
https://0x00-0x00.github.io/research/2018/10/28/How-to-bypass-AMSI-and-Execute-ANY-malicious-powershell-code.html

AMSI: How Windows 10 Plans to Stop Script-Based Attacks
https://www.blackhat.com/docs/us-16/materials/us-16-Mittal-AMSI-How-Windows-10-Plans-To-Stop-Script-Based-Attacks-And-How-Well-It-Does-It.pdf

AMSI Bypass: Patching Technique
https://www.cyberark.com/threat-research-blog/amsi-bypass-patching-technique/

Invisi-Shell - Hide your Powershell script in plain sight. Bypass all Powershell security features
https://github.com/OmerYa/Invisi-Shell


Loading .NET Assemblies Anti-Malware Scan Interface (AMSI) Bypass

A PoC function to corrupt the g_amsiContext global variable in clr.dll in .NET Framework Early Access build 3694
Red Team Revenge - Attacking Microsoft ATA

Evading Microsoft ATA for Active Directory Domination

How to bypass AMSI and execute ANY malicious Powershell code
Hello again. In my previous posts I detailed how to manually get SYSTEM shell from Local Administrators users. That’s interesting but very late game during a penetration assessment as it is presumed that you already owned the target machine.
SEP-11-2019 PART-7
AppLocker & Device Guard Bypass

Living Off The Land Binaries And Scripts - (LOLBins and LOLScripts)
https://github.com/LOLBAS-Project/LOLBAS


Subverting Sysmon: Application of a Formalized Security Product Evasion Methodology
https://github.com/mattifestation/BHUSA2018_Sysmon

sysmon-config-bypass-finder
https://github.com/mkorman90/sysmon-config-bypass-finder

HoneyTokens Evasion

Forging Trusts for Deception in Active Directory
http://www.labofapenetrationtester.com/2018/10/deploy-deception.html

Honeypot Buster: A Unique Red-Team Tool
https://jblog.javelin-networks.com/blog/the-honeypot-buster/

Disabling Security Tools

Invoke-Phant0m - Windows Event Log Killer
https://github.com/hlldz/Invoke-Phant0m
GitHub
https://github.com/LOLBAS-Project/LOLBAS
Living Off The Land Binaries And Scripts - (LOLBins and LOLScripts) - LOLBAS-Project/LOLBAS

GitHub
https://github.com/mattifestation/BHUSA2018_Sysmon
All materials from our Black Hat 2018 "Subverting Sysmon" talk - mattifestation/BHUSA2018_Sysmon

GitHub
https://github.com/mkorman90/sysmon-config-bypass-finder
Detect possible sysmon logging bypasses given a specific configuration - mkorman90/sysmon-config-bypass-finder

Forging Trusts for Deception in Active Directory
Home of Nikhil SamratAshok Mittal. Posts about Red Teaming, Offensive PowerShell, Active Directory and Pen Testing.

Honeypot Buster: A Unique Red-Team Tool

AD exploitation part 7
Credential Dumping

NTDS.DIT Password Extraction

How Attackers Pull the Active Directory Database (NTDS.dit) from a Domain Controller
https://adsecurity.org/?p=451

Extracting Password Hashes From The Ntds.dit File
https://blog.stealthbits.com/extracting-password-hashes-from-the-ntds-dit-file/

SAM (Security Accounts Manager)

Internal Monologue Attack: Retrieving NTLM Hashes without Touching LSASS
https://github.com/eladshamir/Internal-Monologue

Kerberoasting

Kerberoasting Without Mimikatz
https://www.harmj0y.net/blog/powershell/kerberoasting-without-mimikatz/

Cracking Kerberos TGS Tickets Using Kerberoast – Exploiting Kerberos to Compromise the Active Directory Domain
https://adsecurity.org/?p=2293

Extracting Service Account Passwords With Kerberoasting
https://blog.stealthbits.com/extracting-service-account-passwords-with-kerberoasting/

Cracking Service Account Passwords with Kerberoasting
https://www.cyberark.com/blog/cracking-service-account-passwords-kerberoasting/

Kerberoast PW list for cracking passwords with complexity requirements
https://gist.github.com/edermi/f8b143b11dc020b854178d3809cf91b5
Active Directory Security
How Attackers Pull the Active Directory Database (NTDS.dit) from a...
I performed extensive research on how attackers dump AD credentials, including pulling the Active Directory database (ntds.dit) remotely. This information is covered in two newer and greatly expanded posts: How Attackers Dump Active Directory Database Credentials Attack Metho...

Insider Threat Security Blog
Extracting Password Hashes from the Ntds.dit File | Insider Threat...
AD Attack #3 – Ntds.dit Extraction With so much attention paid to detecting credential-based attacks such as Pass-the-Hash (PtH) and Pass-the-Ticket (PtT), other more serious and effective attacks are often overlooked. One such attack is focused on exfiltrating the Ntds.di...

GitHub
https://github.com/eladshamir/Internal-Monologue
Internal Monologue Attack: Retrieving NTLM Hashes without Touching LSASS - eladshamir/Internal-Monologue

Kerberoasting Without Mimikatz
Just about two years ago, Tim Medin presented a new attack technique he christened “Kerberoasting”. While we didn’t realize the full implications of this at the time of release, t…

Active Directory Security
Cracking Kerberos TGS Tickets Using Kerberoast – Exploiting Kerb...
Microsoft's Kerberos implementation in Active Directory has been targeted over the past couple of years by security researchers and attackers alike. The issues are primarily related to the legacy support in Kerberos when Active Directory was released in the year 2000 with Win...

Kerberos AP-REP Roasting

Roasting AS-REPs
http://www.harmj0y.net/blog/activedirectory/roasting-as-reps/

Windows Credential Manager/Vault

Operational Guidance for Offensive User DPAPI Abuse
https://www.harmj0y.net/blog/redteaming/operational-guidance-for-offensive-user-dpapi-abuse/

Jumping Network Segregation with RDP
https://rastamouse.me/2017/08/jumping-network-segregation-with-rdp/

DCSync

Mimikatz and DCSync and ExtraSids, Oh My
https://www.harmj0y.net/blog/redteaming/mimikatz-and-dcsync-and-extrasids-oh-my/

Mimikatz DCSync Usage, Exploitation, and Detection
https://adsecurity.org/?p=1729

Dump Clear-Text Passwords for All Admins in the Domain Using Mimikatz DCSync
https://adsecurity.org/?p=2053

LLMNR/NBT-NS Poisoning

LLMNR/NBT-NS Poisoning Using Responder
https://www.4armed.com/blog/llmnr-nbtns-poisoning-using-responder/

Other

Compromising Plain Text Passwords In Active Directory
https://blog.stealthbits.com/compromising-plain-text-passwords-in-active-directory

https://github.com/w4fz5uck5/AD-Attack-Defense
Roasting AS-REPs
Last November, I published a post titled “Kerberoasting Without Mimikatz” that detailed new developments with PowerView and Tim Medin’s Kerberoasting attack. This started me down …

Operational Guidance for Offensive User DPAPI Abuse
I’ve spoken about DPAPI (the Data Protection Application Programming Interface) a bit before, including how KeePass uses DPAPI for its “Windows User Account” key option. I recently dove into some o…

Jumping Network Segregation with RDP
Introduction

This short post demonstrates how it may be possible to pivot into a segregated/protected network, via an RDP Jump Box.

Mimikatz and DCSync and ExtraSids, Oh My
Edit: Benjamin reached out and corrected me on a few points, which I’ve updated throughout the post. Importantly, with the ExtraSids (/sids) for the injected Golden Ticket, you need to specif…

Active Directory Security
Mimikatz DCSync Usage, Exploitation, and Detection
Note: I presented on this AD persistence method at DerbyCon (2015). A major feature added to Mimkatz in August 2015 is "DCSync" which effectively "impersonates" a Domain Controller and requests account password data from the targeted Domain Controller. DCSync was written by B...

AD exploitation part 8

Persistence


Golden Ticket

Golden Ticket
https://pentestlab.blog/2018/04/09/golden-ticket/

Kerberos Golden Tickets are Now More Golden
https://adsecurity.org/?p=1640



Sneaky Active Directory Persistence #14: SID History
https://adsecurity.org/?p=1772


How Attackers Use Kerberos Silver Tickets to Exploit Systems
https://adsecurity.org/?p=2011

Sneaky Active Directory Persistence #16: Computer Accounts & Domain Controller Silver Tickets
https://adsecurity.org/?p=2753


Creating Persistence With Dcshadow
https://blog.stealthbits.com/creating-persistence-with-dcshadow/



Sneaky Active Directory Persistence #15: Leverage AdminSDHolder & SDProp to (Re)Gain Domain Admin Rights
https://adsecurity.org/?p=1906

Persistence Using Adminsdholder And Sdprop
https://adsecurity.org/?p=1906


Group Policy Object

Sneaky Active Directory Persistence #17: Group Policy
https://adsecurity.org/?p=2716
Penetration Testing Lab
Golden Ticket
Network penetration tests usually stop when domain administrator access has been obtained by the consultant. However domain persistence might be necessary if there is  project time to spent and the…

Active Directory Security
Kerberos Golden Tickets are Now More Golden
At my talk at Black Hat USA 2015, I highlighted new Golden Ticket capability in Mimikatz ("Enhanced Golden Tickets"). This post provides additional detailed on "enhanced" Golden Tickets. Over the past few months, I researched how SID History can be abused in modern enterprise...

Active Directory Security
Sneaky Active Directory Persistence #14: SID History
The content in this post describes a method by which an attacker could persist administrative access to Active Directory after having Domain Admin level rights for 5 minutes. I presented on this AD persistence method in Las Vegas at DEF CON 23 (2015). Complete list of Sneaky ...

Active Directory Security
How Attackers Use Kerberos Silver Tickets to Exploit Systems
Usually Golden Tickets (forged Kerberos TGTs) get all the press, but this post is about Silver Tickets and how attackers use them to exploit systems. I have talked about how Silver Tickets can be used to persist and even re-exploit an Active Directory enterprise in presentati...

Active Directory Security
Sneaky Active Directory Persistence #16: Computer Accounts & Domai...
The content in this post describes a method by which an attacker could persist administrative access to Active Directory after having Domain Admin level rights for about 5 minutes. All posts in my Sneaky Active Directory Persistence Tricks series This post explores how an att...

AD exploitation part 9


Unlocking All The Doors To Active Directory With The Skeleton Key Attack
https://blog.stealthbits.com/unlocking-all-the-doors-to-active-directory-with-the-skeleton-key-attack/

https://pentestlab.blog/2018/04/10/skeleton-key/

Attackers Can Now Use Mimikatz to Implant Skeleton Key on Domain Controllers & BackDoor Your Active Directory Forest
https://adsecurity.org/?p=1275

SeEnableDelegationPrivilege

The Most Dangerous User Right You (Probably) Have Never Heard Of
https://www.harmj0y.net/blog/activedirectory/the-most-dangerous-user-right-you-probably-have-never-heard-of/

SeEnableDelegationPrivilege Active Directory Backdoor
https://adsecurity.org/?p=1785

- Security Support Provider

Sneaky Active Directory Persistence #12: Malicious Security Support Provider (SSP)
https://adsecurity.org/?p=1760

- Directory Services Restore Mode

Sneaky Active Directory Persistence #11: Directory Service Restore Mode (DSRM)
https://adsecurity.org/?p=1714

Sneaky Active Directory Persistence #13: DSRM Persistence v2
https://adsecurity.org/?p=1785

- ACLs & Security Descriptors

An ACE Up the Sleeve: Designing Active Directory DACL Backdoors
https://www.blackhat.com/docs/us-17/wednesday/us-17-Robbins-An-ACE-Up-The-Sleeve-Designing-Active-Directory-DACL-Backdoors-wp.pdf

Shadow Admins – The Stealthy Accounts That You Should Fear The Most
https://www.cyberark.com/threat-research-blog/shadow-admins-stealthy-accounts-fear/

The Unintended Risks of Trusting Active Directory
https://www.slideshare.net/harmj0y/the-unintended-risks-of-trusting-active-directory
Insider Threat Security Blog

Unlocking Active Directory with the Skeleton Key Attack | Insider Blog
Skeleton Key is malware targeted at Active Directory to hijack accounts by injection into LSASS to create a master password that will work for any account.

- Penetration Testing Lab
The Skeleton Key is a malware which is stored in memory which allows an attacker to authenticate as any domain user in the network by using a master password. The techniques that this malware was u…

- Active Directory Security
Attackers Can Now Use Mimikatz to Implant Skeleton Key on Domain C...
Once an attacker has gained Domain Admin rights to your Active Directory environment, there are several methods for keeping privileged access. Skeleton Key is an ideal persistence method for the modern attacker. More information on Skeleton Key is in my earlier post. Note tha...

The Most Dangerous User Right You (Probably) Have Never Heard Of
I find Windows user rights pretty interesting. Separate from machine/domain object DACLs, user rights govern things like “by what method can specific users log into a particular system”…

- Active Directory Security
Sneaky Active Directory Persistence #13: DSRM Persistence v2
The content in this post describes a method by which an attacker could persist administrative access to Active Directory after having Domain Admin level rights for 5 minutes. I presented on this AD persistence method at DerbyCon (2015). I also presented and posted on DSRM as ...

https://pentestlab.blog/2019/09/04/microsoft-exchange-domain-escalation/amp/#click=https://t.co/JvYu6Xr6m4

https://pentestlab.blog/2019/09/05/microsoft-exchange-password-spraying/amp/#click=https://t.co/vJy1oAyWEb

https://pentestlab.blog/2019/09/09/microsoft-exchange-ntlm-relay/amp/#click=https://t.co/ZWg0OOkC2W

https://pentestlab.blog/2019/09/10/microsoft-exchange-code-execution/amp/#click=https://t.co/ISnRl02GI5

https://pentestlab.blog/2019/09/11/microsoft-exchange-mailbox-post-compromise/amp/#click=https://t.co/uN2ZuYrO1e

https://pentestlab.blog/2019/09/16/microsoft-exchange-privilege-escalation/amp/#click=https://t.co/Gj8kGbKaEy
Penetration Testing Lab
- Microsoft Exchange – Domain Escalation
Microsoft Exchange servers are a high valuable target for red teams as they are the main entry point for the majority of the external attacks. From the internal perspective and if initial foothold …

Penetration Testing Lab
- Microsoft Exchange – Password Spraying
Outlook Web Access (OWA) portals typically are externally facing in order to allow users to get access to their emails from the Internet. This gives the opportunity to threat actors to use a common…

Penetration Testing Lab
- Microsoft Exchange – NTLM Relay
Gaining access to the mailbox of a user during a penetration test or a red team engagement can lead to arbitrary code execution, discovery of sensitive data such as credentials or performing intern…

Penetration Testing Lab
- Microsoft Exchange – Code Execution
Gaining access to the mailbox of a domain user can lead to execution of arbitrary code by utilising the credentials that have been discovered. Various techniques have been discovered by Nick Lander…

- Penetration Testing Lab
Microsoft Exchange – Mailbox Post Compromise
Gaining access to the mailbox of a domain user give the opportunity for a list of activities that could potentially expand the access of the red team. The trust relationships of the compromised use…

https://posts.specterops.io/attacking-azure-azure-ad-and-introducing-powerzure-ca70b330511a?gi=d55ce35b9253
- Medium
Attacking Azure, Azure AD, and Introducing PowerZure
Interacting with Azure, offensively

here is ytb channel with high quality course about windows and active directory https://www.youtube.com/user/itfreetraining/playlists
https://www.darknet.org.uk/2020/05/pingcastle-active-directory-security-assessment-tool/?utm_source=feedly&utm_medium=webfeeds
- Darknet
Pingcastle – Active Directory Security Assessment Tool
PingCastle is a Active Directory Security Assessment Tool designed to quickly assess the Active Directory security level with a methodology based on a risk assessment and maturity framework. It does not aim at a perfect evaluation but rather as an efficiency compromise. The ri...

https://www.blackhillsinfosec.com/domain-goodness-learned-love-ad-explorer/
Black Hills Information Security
BHIS
Domain Goodness - How I Learned to LOVE AD Explorer - Black Hills I...
Sally Vandeven // OR How to Pentest with AD Explorer! Mark Russinovich’s Sysinternals tools (Microsoft) are nothing new. They have been a favorite among system administrators for many, many years. Maybe a little less known is that they are super helpful for pentesters too! One...

https://github.com/Integration-IT/Active-Directory-Exploitation-Cheat-Sheet
A cheat sheet that contains common enumeration and attack methods for Windows Active Directory. - Integration-IT/Active-Directory-Exploitation-Cheat-Sheet

https://github.com/CasperGN/ActiveDirectoryEnumeration

# lab
- https://github.com/Orange-Cyberdefense/GOAD
- https://github.com/lkarlslund/deploy-goad
- https://tajdini.net/blog/penetration/active-directory-penetration-mind-map/
- https://infosecwriteups.com/building-a-virtual-security-home-lab-part-6-active-directory-lab-setup-part-1-315716fd51e1

### resources
- https://medium.com/falconforce/falconfriday-detecting-active-directory-data-collection-0xff21-c22d1a57494c
- https://sofblocks.github.io/azure-attack-paths/
- https://github.com/S1ckB0y1337/Active-Directory-Exploitation-Cheat-Sheet
- https://github.com/kha1ifuzz/AD-Config-Automation
