- https://github.com/imthenachoman/How-To-Secure-A-Linux-Server
- https://github.com/trimstray/the-practical-linux-hardening-guide


- [SEI CERT C++ Coding Standard](https://www.securecoding.cert.org/confluence/pages/viewpage.action?pageId=637)
- [SEI CERT C Coding Standard](https://www.securecoding.cert.org/confluence/display/c/SEI+CERT+C+Coding+Standard)
- [SEI CERT Oracle Coding Standard for Java](https://www.securecoding.cert.org/confluence/display/java/SEI+CERT+Oracle+Coding+Standard+for+Java)
- [Android Secure Coding Standard](https://www.securecoding.cert.org/confluence/display/android/Android+Secure+Coding+Standard)
- [Secure Coding Guidelines(.NET Framework (current version))]( https://msdn.microsoft.com/en-us/library/8a3x2b7f.aspx)
- [Secure Coding Guidelines for Java SE](http://www.oracle.com/technetwork/java/seccodeguide-139067.html)
