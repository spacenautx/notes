# deobfuscation

- https://www.perimeterx.com/tech-blog/2020/analyzing_magecart_malware_from_zero_to_hero/
- https://www.perimeterx.com/tech-blog/2022/defeating-javascript-obfuscation/
- https://www.perimeterx.com/tech-blog/2022/automating-skimmer-deobfuscation/
- https://www.perimeterx.com/tech-blog/2020/deobfuscating-caesar/
- https://www.perimeterx.com/tech-blog/2019/javascript-anti-debugging-1/

### resources
- [beginner's guide to obfuscation](https://github.com/BC-SECURITY/Beginners-Guide-to-Obfuscation)
- https://github.com/BC-SECURITY/Beginners-Guide-to-Obfuscation
