# kernel exploitation

- https://lookerstudio.google.com/reporting/68b02863-4f5c-4d85-b3c1-992af89c855c/page/n92nD
- https://github.com/google/security-research/tree/master/analysis/kernel/heap-exploitation
- https://0x434b.dev/dabbling-with-linux-kernel-exploitation-ctf-challenges-to-learn-the-ropes/
- https://intellabs.github.io/kAFL/tutorials/concepts.html
- https://github.com/hardik05/Damn_Vulnerable_Kernel_Module
- https://pawnyable.cafe/linux-kernel/
- https://ptr-yudai.hatenablog.com/entry/2020/03/16/165628
- https://lkmidas.github.io/posts/20210123-linux-kernel-pwn-part-1/
- https://0x434b.dev/dabbling-with-linux-kernel-exploitation-ctf-challenges-to-learn-the-ropes/
- https://github.com/xairy/linux-kernel-exploitation
- https://xairy.io/
- https://lkmidas.github.io/posts/20210123-linux-kernel-pwn-part-1/
- https://github.com/a13xp0p0v/linux-kernel-defence-map
- https://a13xp0p0v.github.io/
- https://duasynt.com/blog/linux-kernel-heap-feng-shui-2022
- https://david942j.blogspot.com/2018/10/note-learning-kvm-implement-your-own.html
- https://lwn.net/Articles/658511/
- https://www.jmeiners.com/lc3-vm/
- https://github.com/justinmeiners/lc3-vm
- https://github.com/winterknife/PINKPANTHER
- https://github.blog/2022-07-27-corrupting-memory-without-memory-corruption/
- https://cyber.wtf/2017/07/28/negative-result-reading-kernel-memory-from-user-mode/
- https://juggernaut-sec.com/kernel-exploits-lpe/
- https://github.com/Ascotbe/Kernelhub
- https://blog.theori.io/research/CVE-2022-32250-linux-kernel-lpe-2022/
- https://github.com/netspooky/notes/tree/main/linux
- https://github.com/cloudfuzz/android-kernel-exploitation
- https://www.willsroot.io/2022/12/entrybleed.html?m=1
- https://blog.exodusintel.com/2022/12/19/linux-kernel-exploiting-a-netfilter-use-after-free-in-kmalloc-cg/
- https://www.willsroot.io/2021/08/corctf-2021-fire-of-salvation-writeup.html
- https://syst3mfailure.io/wall-of-perdition
- https://accessvector.net/2022/linux-itimers-uaf
- https://docs.google.com/document/d/1a9uUAISBzw3ur1aLQqKc5JOQLaJYiOP5pe_B4xCT1KA/edit#
- https://blog.k3170makan.com/2021/01/linux-kernel-exploitation-0x2.html
- https://sam4k.com/analysing-linux-kernel-commits/
- https://docs.google.com/document/d/1a9uUAISBzw3ur1aLQqKc5JOQLaJYiOP5pe_B4xCT1KA/edit
- https://github.com/bsauce/kernel-security-learning

### bluetooth
- https://google.github.io/security-research/pocs/linux/bleedingtooth/writeup.html

### debugging
- https://www.timdbg.com/posts/remote-debugging/


- https://github.com/google/syzkaller
- https://github.com/xalicex/kernel-debug-lab-for-virtual-box

### learn
- https://www.lazenca.net/display/TEC/07.Linux+Kernel+exploitation+techniques
- https://sam4k.com/linternals-introduction/
- https://github.com/ocastejon/linux-kernel-learning
- https://sysprog21.github.io/lkmpg/
- https://breaking-bits.gitbook.io/breaking-bits/exploit-development/linux-kernel-exploit-development
- http://blog.k3170makan.com/
- https://santaclz.github.io/2023/11/03/Linux-Kernel-Exploitation-Getting-started-and-BOF.html
- https://linux-kernel-labs.github.io/refs/heads/master/index.html

### tools
- https://github.com/0xricksanchez/like-dbg
