# bug hunting
- https://github.com/Samsar4/Ethical-Hacking-Labs
- https://github.com/hahwul/WebHackersWeapons/tree/main
- https://github.com/xalgord/Massive-Web-Application-Penetration-Testing-Bug-Bounty-Notes
- https://github.com/pudsec/public_hacktivity
- https://attacker-codeninja.github.io/2022-06-06-awesome-bug-bounty-roadmap/
- https://github.com/trickest/inventory
- https://github.com/Anlominus/Bug-Bounty
- https://github.com/daffainfo/AllAboutBugBounty
- https://github.com/D3Ext/PentestDictionary
- https://github.com/pe3zx/my-infosec-awesome
- https://github.com/carpedm20/awesome-hacking
- https://github.com/jekil/awesome-hacking
- https://github.com/riramar/Web-Attack-Cheat-Sheet
- https://github.com/EdOverflow/bugbounty-cheatsheet
- https://github.com/Zeyad-Azima/Offensive-Resources
- https://github.com/sbilly/awesome-security
- https://github.com/qazbnm456/awesome-web-security
- https://github.com/Voorivex/pentest-guide
- https://github.com/nixawk/pentest-wiki
- https://github.com/zbetcheckin/Security_list
- https://github.com/firmianay/Life-long-Learner
- https://github.com/wwong99/pentest-notes/blob/master/enumeration/configuration_management.md
- https://gist.github.com/jivoi/724e4b4b22501b77ef133edc63eba7b4
- https://cheatsheetseries.owasp.org/Glossary.html
- https://github.com/Hack-with-Github/Awesome-Hacking
- https://hideandsec.sh
- https://github.com/Correia-jpv/fucking-the-book-of-secret-knowledge
- https://github.com/devanshbatham/Vulnerabilities-Unmasked
- https://github.com/Az0x7/vulnerability-Checklist
- https://github.com/Voorivex/pentest-guide
- https://github.com/mgeeky/Penetration-Testing-Tools
- https://github.com/vavkamil/awesome-bugbounty-tools
- https://github.com/6vr/Bug-Bounty-Tips
- https://github.com/tuhin1729/Bug-Bounty-Methodology
- https://github.com/bittentech/Bug-Bounty-Beginner-Roadmap
- https://github.com/glaucusec/awesome-repos
- https://github.com/Asifkhan127001/Bugbounty
- https://github.com/cipher387/Dorks-collections-list
- https://github.com/paragonie/awesome-appsec
- https://github.com/Hacker0x01/hacker101
- https://github.com/HolyBugx/HolyTips
- https://pentestbook.six2dez.com
- https://github.com/vitalysim/Awesome-Hacking-Resources
- https://techvomit.net/pentesting-notes-and-snippets/
- https://github.com/PalindromeLabs/awesome-websocket-security
- https://github.com/emadshanab/Some-BugBounty-Tips-from-my-Twitter-feed
- https://x0rb3l.github.io/Cyber-Bookmarks/bookmarks.html
- https://github.com/7RU7H/Archive
- https://github.com/bittentech/Bug-Bounty-Beginner-Roadmap
- https://github.com/BishopFox/cloudfox
- https://github.com/aquasecurity/cloud-security-remediation-guides
- https://github.com/0xsyr0/Awesome-Cybersecurity-Handbooks


# methodology
- https://twitter.com/ArchAngelDDay/status/1661924038875435008
- https://github.com/NafisiAslH/KnowledgeSharing/tree/main/CyberSecurity/Web/100BugBountySecrets
- https://exploit-notes.hdks.org/
- https://github.com/harsh-bothra/learn365
- https://labs.detectify.com/2021/11/30/hakluke-creating-the-perfect-bug-bounty-automation/
- http://www.pentest-standard.org/index.php/Main_Page
- https://www.tuicool.com/kans/1181838036
- http://www.pentest-standard.org/index.php/PTES_Technical_Guidelines
- https://code.google.com/archive/p/pentest-bookmarks/wikis/BookmarksList.wiki
- https://blog.csdn.net/weixin_34355881/article/details/86199308
- http://www.vulnerabilityassessment.co.uk/Penetration%20Test.html#
- https://www.billdietrich.me/PenetrationTestingAndBugBountyHunting.html
- https://www.bugbountynotes.com/getting-started
- https://www.defcon.org/html/links/book-list.html
- https://medium.com/bugbountywriteup/bug-bounty-hunting-methodology-toolkit-tips-tricks-blogs-ef6542301c65
- http://www.vulnerabilityassessment.co.uk/enum.htm
- https://medium.com/dvlpr/penetration-testing-methodology-part-1-6-recon-9296c4d07c8a
- http://www.0daysecurity.com/penetration-testing/enumeration.html
- http://www.vulnerabilityassessment.co.uk/Penetration%20Test.html
- [Penetration testing Process, Methods and Real world Attacks Collections](https://github.com/kyawthiha7/pentest-methodology)
- [Blackbox Network Penetration Testing Process (PTES Based)](https://github.com/jr69ss/process-pentest-blackbox-ptes)
- [Enumeration and Networking guidelines](https://github.com/Mdot0/Pentesting-Methodology-)
- [AZURE PENTEST]( https://github.com/rootsecdev/Azure-Red-Team)
- [IoT-Pentesting-Methodology]( https://github.com/adi0x90/IoT-Pentesting-Methodology)
- [My Web Recon methodology]( https://github.com/MrEmpy/My-Recon-Methodology)


# tools
- https://github.com/iamSm9l/Utils-and-Wrappers
- https://github.com/dwisiswant0/crlfuzz
- https://github.com/six2dez/reconftw
- https://github.com/S3cur3Th1sSh1t/Pentest-Tools
- https://github.com/We5ter/Scanners-Box
- https://github.com/Cyber-Guy1/API-SecurityEmpire
- https://github.com/hktalent/scan4all


# writeups
- https://github.com/alexbieber/Bug_Bounty_writeups
- https://github.com/xdavidhu/awesome-google-vrp-writeups
- https://github.com/fardeen-ahmed/Bug-bounty-Writeups
- https://github.com/ngalongc/bug-bounty-reference
- https://github.com/zapstiko/Bug-Bounty 
- https://github.com/reddelexc/hackerone-reports

# checklist
- https://github.com/Hari-prasaanth/Web-App-Pentest-Checklist
