- https://github.com/sushiwushi/bug-bounty-dorks
- https://github.com/InfuriousICC/Google-Dorks-Simplified
- https://github.com/cipher387/Dorks-collections-list
- https://github.com/CorrieOnly/google-dorks
- https://github.com/spekulatius/infosec-dorks
- https://gist.github.com/sundowndev/283efaddbcf896ab405488330d1bbc06
- https://github.com/TakSec/google-dorks-bug-bounty

### tools
- https://dorksearch.com/
- https://github.com/IvanGlinkin/Fast-Google-Dorks-Scan
- https://github.com/cryxnet/DorkStorm
- https://github.com/rtwillett/DorkLab
- https://osintteam.blog/mastering-osint-the-art-of-google-dorking-for-investigators-e0a908055873
