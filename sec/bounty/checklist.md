- https://portswigger.net/support/burp-testing-methodologies
- https://github.com/Az0x7/vulnerability-Checklist/blob/main/README.md
- https://wiki.owasp.org/index.php/Testing_Checklist
- https://github.com/Lissy93/personal-security-checklist
 

- **Recon Phase**
    - [ ]  Identify web server, technologies and database
    - [ ]  Subsidiary and Acquisition Enumeration
    - [ ]  Reverse Lookup
    - [ ]  ASN & IP Space Enumeration and Service Enumeration
    - [ ]  Google Dorking
    - [ ]  Github Recon
    - [ ]  Directory Enumeration
    - [ ]  IP Range Enumeration
    - [ ]  JS Files Analysis
    - [ ]  Subdomain Enumeration and Bruteforcing
    - [ ]  Subdomain Takeover
    - [ ]  Parameter Fuzzing
    - [ ]  Port Scanning
    - [ ]  Template-Based Scanning(Nuclei)
    - [ ]  Wayback History
    - [ ]  Broken Link Hijacking
    - [ ]  Internet Search Engine Discovery
    - [ ]  Misconfigured Cloud Storage
- **Registration Feature Testing**
    - [ ]  Check for duplicate registration/Overwrite existing user
    - [ ]  Check for weak password policy
    - [ ]  Check for reuse existing usernames
    - [ ]  Check for insufficient email verification process
    - [ ]  Weak registration implementation-Allows disposable email addresses
    - [ ]  Weak registration implementation-Over HTTP
    - [ ]  Overwrite default web application pages by specially crafted username registrations. => After registration, does your profile link appears something as [www.tushar.com/](http://www.chintan.com/chintan)tushar?
    
    a. If so, enumerate default folders of web application such as /images, /contact, /portfolio 
    
    b. Do a registration using the username such as images, contact, portfolio 
    
    c. Check if those default folders have been overwritten by your profile link or not."
    
- **Session Management Testing**
    - [ ]  Identify actual session cookie out of bulk cookies in the application
    - [ ]  Decode cookies using some standard decoding algorithms such as Base64, hex, URL, etc
    - [ ]  Modify cookie.session token value by 1 bit/byte. Then resubmit and do the same for all tokens. Reduce the amount of work you need to perform in order to identify which part of the token is actually being used and which is not
    - [ ]  If self-registration is available and you can choose your username, log in with a series of similar usernames containing small variations between them, such as A, AA, AAA, AAAA, AAAB, AAAC, AABA, and so on. If another user-specific data is submitted at login or stored in user profiles (such as an email address)
    - [ ]  Check for session cookies and cookie expiration date/time
    - [ ]  Identify cookie domain scope
    - [ ]  Check for HttpOnly flag in cookie
    - [ ]  Check for Secure flag in cookie if the application is over SSL
    - [ ]  Check for session fixation i.e. value of session cookie before and after authentication
    - [ ]  Replay the session cookie from a different effective IP address or system to check whether the server maintains the state of the machine or not
    - [ ]  Check for concurrent login through different machine/IP
    - [ ]  Check if any user pertaining information is stored in cookie value or not If yes, tamper it with other user's data
    - [ ]  Failure to Invalidate Session on (Email Change,2FA Activation)
- **Authentication Testing**
    - [ ]  Username enumeration
    - [ ]  Bypass authentication using various SQL Injections on username and password field
    - Lack of password confirmation on
        - [ ]  Change email address
        - [ ]  Change password
        - [ ]  Manage 2FA
    - [ ]  Is it possible to use resources without authentication? Access violation
    - [ ]  Check if user credentials are transmitted over SSL or not
    - [ ]  Weak login function HTTP and HTTPS both are available
    - Test user account lockout mechanism on brute force attack
        
        Variation : If server blocks instant user requests, then try with time throttle option from intruder and repeat the process again.
        
        - [ ]  Bypass rate limiting by tampering user agent to Mobile User agent
        - [ ]  Bypass rate limiting by tampering user agent to Anonymous user agent
        - [ ]  Bypass rate liniting by using null byte
    - [ ]  Create a password wordlist using cewl command
    - Test Oauth login functionality
        - OAuth Roles
            - [ ]  Resource Owner → User
            - [ ]  Resource Server → Twitter
            - [ ]  Client Application → [Twitterdeck.com](http://twitterdeck.com/)
            - [ ]  Authorization Server → Twitter
            - [ ]  client_id → Twitterdeck ID (This is a public, non-secret unique identifier_
            - [ ]  client_secret → Secret Token known to the Twitter and Twitterdeck to generate access_tokens
            - [ ]  response_type → Defines the token type e.g (code, token, etc.)
            - [ ]  scope → The requested level of access Twitterdeck wants
            - [ ]  redirect_uri → The URL user is redirected to after the authorization is complete
            - [ ]  state → Main CSRF protection in OAuth can persist data between the user being directed to the authorization server and back again
            - [ ]  grant_type → Defines the grant_type and the returned token type
            - [ ]  code → The authorization code twitter generated, will be like ?code= , the code is used with client_id and client_secret to fetch an access_token
            - [ ]  access_token → The token twitterdeck uses to make API requests on behalf of the user
            - [ ]  refresh_token → Allows an application to obtain a new access_token without prompting the user
        - Code Flaws
            - [ ]  Re-Using the code
            - [ ]  Code Predict/Bruteforce and Rate-limit
            - [ ]  Is the code for application X valid for application Y?
        - Redirect_uri Flaws
            - [ ]  URL isn't validated at all: ?redirect_uri=https://attacker.com
            - [ ]  Subdomains allowed (Subdomain Takeover or Open redirect on those subdomains): ?redirect_uri=https://sub.twitterdeck.com
            - [ ]  Host is validated, path isn't Chain open redirect): ?redirect_uri=https://twitterdeck.com/callback?redirectUrl=https://evil.com
            - [ ]  Host is validated, path isn't (Referer leakages): Include external content on HTML page and leak code via Referer
            - [ ]  Weak Regexes
            - [ ]  Bruteforcing the URL encoded chars after host: redirect_uri=https://twitterdeck.com§FUZZ§
            - [ ]  Bruteforcing the keywords whitelist after host (or on any whitelist open redirect filter): ?redirect_uri=https://§FUZZ§.com
            - [ ]  URI validation in place: use typical open redirect payloads
        - State Flaws
            - [ ]  Missing State parameter? (CSRF)
            - [ ]  Predictable State parameter?
            - [ ]  Is State parameter being verified?
        - Misc
            - [ ]  Is client_secret validated?
            - [ ]  Pre ATO using facebook phone-number signup
            - [ ]  No email validation Pre ATO
    - Test 2FA Misconfiguration
        - [ ]  Response Manipulation
        - [ ]  Status Code
        - [ ]  Manipulation
        - [ ]  2FA Code Leakage in Response
        - [ ]  2FA Code Reusability
        - [ ]  Lack of Brute-Force Protection
        - [ ]  Missing 2FA Code Integrity Validation
        - [ ]  With null or 000000
- **My Account (Post Login) Testing**
    - [ ]  Find parameter which uses active account user id. Try to tamper it in order to change the details of the other accounts
    - [ ]  Create a list of features that are pertaining to a user account only. Change Email Change Password -Change account details (Name, Number, Address, etc.) Try CSRF
    - [ ]  Post login change email id and update with any existing email id. Check if its getting validated on server side or not. Does the application send any new email confirmation link to a new user or not? What if a user does not confirm the link in some time frame?
    - [ ]  Open profile picture in a new tab and check the URL. Find email id/user id info. EXIF Geolocation Data Not Stripped From Uploaded Images.
    - [ ]  Check account deletion option if application provides it and confirm that via forgot password feature
    - [ ]  Change email id, account id, user id parameter and try to brute force other user's password
    - [ ]  Check whether application re authenticates for performing sensitive operation for post authentication features
- **Forgot Password Testing**
    - [ ]  Failure to invalidate session on Logout and Password reset
    - [ ]  Check if forget password reset link/code uniqueness
    - [ ]  Check if reset link does get expire or not if its not used by the user for certain amount of time
    - [ ]  Find user account identification parameter and tamper Id or parameter value to change other user's password
    - [ ]  Check for weak password policy
    - [ ]  Weak password reset implementation Token is not invalidated after use
    - [ ]  If reset link has another param such as date and time, then. Change date and time value in order to make active & valid reset link
    - [ ]  Check if security questions are asked? How many guesses allowed? --> Lockout policy maintained or not?
    - [ ]  Add only spaces in new password and confirmed password. Then Hit enter and see the result
    - [ ]  Does it display old password on the same page after completion of forget password formality?
    - [ ]  Ask for two password reset link and use the older one from user's email
    - [ ]  Check if active session gets destroyed upon changing the password or not?
    - [ ]  Weak password reset implementation Password reset token sent over HTTP
    - [ ]  Send continuous forget password requests so that it may send sequential tokens
- **Contact Us Form Testing**
    - [ ]  Is CAPTCHA implemented on contact us form in order to restrict email flooding attacks?
    - [ ]  Does it allow to upload file on the server?
    - [ ]  Blind XSS
- **Product Purchase Testing**
    - Buy Now
        - [ ]  Tamper product ID to purchase other high valued product with low prize
        - [ ]  Tamper product data in order to increase the number of product with the same prize
    - Gift/Voucher
        - [ ]  Tamper gift/voucher count in the request (if any) to increase/decrease the number of vouchers/gifts to be used
        - [ ]  Tamper gift/voucher value to increase/decrease the value of the voucher in terms of money. (e.g. $100 is given as a voucher, tamper value to increase, decrease money)
        - [ ]  Reuse gift/voucher by using old gift values in parameter tampering
        - [ ]  Check the uniqueness of gift/voucher parameter and try guessing other gift/voucher code
        - [ ]  Use parameter pollution technique to add the same voucher twice by adding same parameter name and value again with & in the BurpSuite request
    - Add/Delete Product from Cart
        - [ ]  Tamper user id to delete products from other user's cart
        - [ ]  Tamper cart id to add/delete products from other user's cart
        - [ ]  Identify cart id/user id for cart feature to view the added items from other user's account
    - Address
        - [ ]  Tamper BurpSuite request to change other user's shipping address to yours
        - [ ]  Try stored XSS by adding XSS vector on shipping address
        - [ ]  Use parameter pollution technique to add two shipping address instead of one trying to manipulate application to send same item on two shipping address
    - Place Order
        - [ ]  Tamper payment options parameter to change the payment method. E.g. Consider some items cannot be ordered for cash on delivery but tampering request parameters from debit/credit/PayPal/net banking option to cash on delivery may allow you to
        place order for that particular item
        - [ ]  Tamper the amount value for payment manipulation in each main and sub requests and responses
        - [ ]  Check if CVV is going in cleartext or not
        - [ ]  Check if the application itself processes your card details and then performs a transaction or it calls any third-party payment processing company to perform a transaction
    - Track Order
        - [ ]  Track other user's order by guessing order tracking number
        - [ ]  Brute force tracking number prefix or suffix to track mass orders for other users
    - Wish list page testing
        - [ ]  Check if a user A can add/remote products in Wishlist of other user B’s account
        - [ ]  Check if a user A can add products into user B’s cart from his/her (user A’s) Wishlist section.
    - Post product purchase testing
        - [ ]  Check if user A can cancel orders for user B’s purchase
        - [ ]  Check if user A can view/check orders already placed by user B
        - [ ]  Check if user A can modify the shipping address of placed order by user B
    - Out of band testing
        - [ ]  Can user order product which is out of stock?
- **Banking Application Testing**
    - Billing Activity
        - [ ]  Check if user 'A' can view the account statement for user 'B'
        - [ ]  Check if user 'A' can view the transaction report for user 'B'
        - [ ]  Check if user 'A' can view the summary report for user 'B'
        - [ ]  Check if user 'A' can register for monthly/weekly account statement via email behalf of user 'B'
        - [ ]  Check if user 'A' can update the existing email id of user 'B' in order to retrieve monthly/weekly account summary
    - Deposit/Loan/Linked/External Account Checking
        - [ ]  Check if user 'A' can view the deposit account summary of user 'B'
        - [ ]  Check for account balance tampering for Deposit accounts
    - Tax Deduction Inquiry Testing
        - [ ]  Check if user 'A' with it's customer id 'a' can see the tax deduction details of user 'B' by tampering his/her customer id 'b'
        - [ ]  Check parameter tampering for increasing and decreasing interest rate, interest amount, and tax refund
        - [ ]  Check if user 'A' can download the TDS details of user 'B’
    - [ ]  Check if user 'A' can request for the cheque book behalf of user ‘B’.
    - Fixed Deposit Account Testing
        - [ ]  Check if is it possible for user 'A' to open FD account behalf of user 'B'
        - [ ]  Check if Can user open FD account with the more amount than the current account balance
    - Stopping Payment on basis of cheque/date range
        - [ ]  Can user 'A' stop the payment of user 'B' via cheque number
        - [ ]  Can user 'A' stop the payment on basis of date range for user 'B’
    - Status Enquiry Testing
        - [ ]  Can user 'A' view the status enquiry of user 'B'
        - [ ]  Can user 'A' modify the status enquiry of user 'B'
        - [ ]  Can user 'A' post and enquiry behalf of user 'B' from his own account
    - Fund transfer testing
        - [ ]  Is it possible to transfer funds to user 'C' instead of user 'B' from the user 'A' which was intended to transfer from user 'A' to user 'B'
        - [ ]  Can fund transfer amount be manipulated?
        - [ ]  Can user 'A' modify the payee list of user 'B' by parameter manipulation using his/her own account
        - [ ]  Is it possible to add payee without any proper validation in user 'A' 's own account or to user 'B' 's account
    - Schedule transfer testing
        - [ ]  Can user 'A' view the schedule transfer of user 'B'
        - [ ]  Can user 'A' change the details of schedule transfer for user 'B’
    - Testing of fund transfer via NEFT
        - [ ]  Amount manipulation via NEFT transfer
        - [ ]  Check if user 'A' can view the NEFT transfer details of user 'B’
    - Testing for Bill Payment
        - [ ]  Check if user can register payee without any checker approval
        - [ ]  Check if user 'A' can view the pending payments of user 'B'
        - [ ]  Check if user 'A' can view the payment made details of user 'B'
- **Open Redirection Testing**
    - Common injection parameters
        
        ```markup
        /{payload}
        ?next={payload}
        ?url={payload}
        ?target={payload}
        ?rurl={payload}
        ?dest={payload}
        ?destination={payload}
        ?redir={payload}
        ?redirect_uri={payload}
        ?redirect_url={payload}
        ?redirect={payload}
        /redirect/{payload}
        /cgi-bin/redirect.cgi?{payload}
        /out/{payload}
        /out?{payload}
        ?view={payload}
        /login?to={payload}
        ?image_url={payload}
        ?go={payload}
        ?return={payload}
        ?returnTo={payload}
        ?return_to={payload}
        ?checkout_url={payload}
        ?continue={payload}
        ?return_path={payload}
        ```
        
    - [ ]  Use burp 'find' option in order to find parameters such as URL, red, redirect, redir, origin, redirect_uri, target etc
    - [ ]  Check the value of these parameter which may contain a URL
    - [ ]  Change the URL value to [www.tushar.com](http://www.chintan.com/) and check if gets redirected or not
    - [ ]  Try Single Slash and url encoding
    - [ ]  Using a whitelisted domain or keyword
    - [ ]  Using // to bypass http blacklisted keyword
    - [ ]  Using https: to bypass // blacklisted keyword
    - [ ]  Using \\ to bypass // blacklisted keyword
    - [ ]  Using \/\/ to bypass // blacklisted keyword
    - [ ]  Using null byte %00 to bypass blacklist filter
    - [ ]  Using ° symbol to bypass
- **Host Header Injection**
    - [ ]  Supply an arbitrary Host header
    - [ ]  Check for flawed validation
    - Send ambiguous requests
        - [ ]  Inject duplicate Host headers
        - [ ]  Supply an absolute URL
        - [ ]  Add line wrapping
    - [ ]  Inject host override headers
- **SQL Injection Testing**
    - Entry point detection
        - [ ]  Simple characters
        - [ ]  Multiple encoding
        - [ ]  Merging characters
        - [ ]  Logic Testing
        - [ ]  Weird characters
    - [ ]  Run SQL injection scanner on all requests
    - Bypassing WAF
        - [ ]  Using Null byte before SQL query
        - [ ]  Using SQL inline comment sequence
        - [ ]  URL encoding
        - [ ]  Changing Cases (uppercase/lowercase)
        - [ ]  Use SQLMAP tamper scripts
    - Time Delays
        
        ```markup
              Oracle 	      dbms_pipe.receive_message(('a'),10)
              
              Microsoft 	  WAITFOR DELAY '0:0:10'
              
              PostgreSQL 	  SELECT pg_sleep(10)
              
              MySQL 	      SELECT sleep(10)
        ```
        
    - Conditional Delays
        
        ```markup
              Oracle 	      SELECT CASE WHEN (YOUR-CONDITION-HERE) THEN 'a'||dbms_pipe.receive_message(('a'),10) ELSE NULL END FROM dual
              
              Microsoft 	  IF (YOUR-CONDITION-HERE) WAITFOR DELAY '0:0:10'
              
              PostgreSQL 	  SELECT CASE WHEN (YOUR-CONDITION-HERE) THEN pg_sleep(10) ELSE pg_sleep(0) END
              
              MySQL 	      SELECT IF(YOUR-CONDITION-HERE,sleep(10),'a')
        ```
        
- **Cross-Site Scripting Testing**
    - [ ]  Try XSS using QuickXSS tool by theinfosecguy
    - [ ]  Upload file using '"><img src=x onerror=alert(document.domain)>.txt
    - [ ]  If script tags are banned, use <h1> and other HTML tags
    - [ ]  If output is reflected back inside the JavaScript as a value of any variable just use alert(1)
    - [ ]  if " are filtered then use this payload /><img src=d onerror=confirm(/tushar/);>
    - [ ]  Upload a JavaScript using Image file
    - [ ]  Unusual way to execute your JS payload is to change method from POST to GET. It bypasses filters sometimes
    - Tag attribute value
        - [ ]  Input landed -<input type=”text” name=”state” value=”INPUT_FROM_ USER”>
        - [ ]  Payload to be inserted -“ onfocus=”alert(document.cookie)"
    - [ ]  Syntax Encoding payload “%3cscript%3ealert(document.cookie)%3c/script%3e"
    - XSS filter evasion
        - [ ]  < and > can be replace with html entities &lt; and &gt;
        - [ ]  You can try an XSS polyglot.Eg:-javascript:/*-></title></style></textarea></script></xmp><svg/onload='+/"/+/onmouseover=1/+/[*/[]/+alert(1)//'>
    - XSS Firewall Bypass
        - [ ]  Check if the firewall is blocking only lowercase
        - [ ]  Try to break firewall regex with the new line(\r\n)
        - [ ]  Try Double Encoding
        - [ ]  Testing for recursive filters
        - [ ]  Injecting anchor tag without whitespaces
        - [ ]  Try to bypass whitespaces using Bullet
        - [ ]  Try to change request method
- **CSRF Testing**
    - Application has Anti-CSRF token implemented
        - [ ]  Removing the Anti-CSRF Token
        - [ ]  Altering the Anti-CSRF Token
        - [ ]  Using the Attacker’s Anti-CSRF Token
        - [ ]  Spoofing the Anti-CSRF Token
        - [ ]  Using guessable Anti-CSRF Tokens
        - [ ]  Stealing Anti-CSRF Tokens
    - Application uses Double Submit Cookie
        - [ ]  Check for session fixation on subdomains
        - [ ]  Man in the the middle attack
    - Application validates the Referrer or the Origin of the request received
        - [ ]  Restricting the CSRF POC from sending the Referrer header
        - [ ]  Bypass the whitelisting/blacklisting mechanism used by the application
    - Sending data in JSON/XML format
        - [ ]  By using normal HTML Form1
        - [ ]  By using normal HTML Form2 (By Fetch Request)
        - [ ]  By using XMLHTTP Request/AJAX request
        - [ ]  By using Flash file
    - Samesite Cookie attribute
        - [ ]  SameSite Lax bypass via method override
        - [ ]  SameSite Strict bypass via client-side redirect
        - [ ]  SameSite Strict bypass via sibling domain
        - [ ]  SameSite Lax bypass via cookie refresh
- **SSO Vulnerabilities**
    - [ ]  If internal.company.com Redirects You To SSO e.g. auth.company.com, Do FUZZ
    On Internal.company.com
    - [ ]  If company.com/internal Redirects You To SSO e.g. Google login, Try To Insert
    public Before internal e.g. company.com/public/internal To Gain Access Internal
    - [ ]  Try To Craft SAML Request With Token And Send It To The Server And Figure
    Out How Server Interact With This
    - [ ]  If There Is AssertionConsumerServiceURL In Token Request Try To Insert Your
    Domain e.g. http://me.com As Value To Steal The Token
    - [ ]  If There Is AssertionConsumerServiceURL In Token Request Try To Do FUZZ
    On Value Of AssertionConsumerServiceURL If It Is Not Similar To Origin
    - [ ]  If There Is Any UUID, Try To Change It To UUID Of Victim Attacker e.g. Email Of
    Internal Employee Or Admin Account etc
    - [ ]  Try To Figure Out If The Server Vulnerable To XML Signature Wrapping OR Not?
    - [ ]  Try To Figure Out If The Server Checks The Identity Of The Signer OR Not?
    - [ ]  Try To Inject XXE Payloads At The Top Of The SAML Response
    - [ ]  Try To Inject XSLT Payloads Into The Transforms Element As A Child
    Node Of The SAML Response
    - [ ]  If Victim Can Accept Tokens Issued By The Same Identity Provider That Services
    Attacker, So You Can Takeover Victim Account
    - [ ]  While Testing SSO Try To search In Burp Suite About URLs In Cookie Header e.g.
    Host=IP; If There Is Try To Change IP To Your IP To Get SSRF
- **XML Injection Testing**
    - [ ]  Change the content type to text/xml then insert below code. Check via repeater
    
    ```markup
    <?xml version="1.0" encoding="ISO 8859 1"?>
    <!DOCTYPE tushar [
    <!ELEMENT tushar ANY
    <!ENTITY xxe SYSTEM "file:///etc/passwd" >]><tushar>&xxe;</
    <!ENTITY xxe SYSTEM "file:///etc/hosts" >]><tushar>&xxe;</
    <!ENTITY xxe SYSTEM "file:///proc/self/cmdline" >]><tushar>&xxe;</
    <!ENTITY xxe SYSTEM "file:///proc/version" >]><tushar>&xxe;</
    ```
    
    - [ ]  Blind XXE with out-of-band interaction
- **Cross-origin resource sharing (CORS)**
    - [ ]  Errors parsing Origin headers
    - [ ]  Whitelisted null origin value
- **Server-side request forgery (SSRF)**
    - Common injection parameters
        
        ```markup
        "access=", 
        "admin=", 
        "dbg=", 
        "debug=", 
        "edit=", 
        "grant=", 
        "test=", 
        "alter=", 
        "clone=", 
        "create=", 
        "delete=", 
        "disable=", 
        "enable=", 
        "exec=", 
        "execute=", 
        "load=", 
        "make=", 
        "modify=", 
        "rename=", 
        "reset=", 
        "shell=", 
        "toggle=", 
        "adm=", 
        "root=", 
        "cfg=",
        "dest=", 
        "redirect=", 
        "uri=", 
        "path=", 
        "continue=", 
        "url=", 
        "window=", 
        "next=", 
        "data=", 
        "reference=", 
        "site=", 
        "html=", 
        "val=", 
        "validate=", 
        "domain=", 
        "callback=", 
        "return=", 
        "page=", 
        "feed=", 
        "host=", 
        "port=", 
        "to=", 
        "out=",
        "view=", 
        "dir=", 
        "show=", 
        "navigation=", 
        "open=",
        "file=",
        "document=",
        "folder=",
        "pg=",
        "php_path=",
        "style=",
        "doc=",
        "img=",
        "filename="
        ```
        
    - [ ]  Try basic localhost payloads
    - Bypassing filters
        - [ ]  Bypass using HTTPS
        - [ ]  Bypass with [::]
        - [ ]  Bypass with a domain redirection
        - [ ]  Bypass using a decimal IP location
        - [ ]  Bypass using IPv6/IPv4 Address Embedding
        - [ ]  Bypass using malformed urls
        - [ ]  Bypass using rare address(short-hand IP addresses by dropping the zeros)
        - [ ]  Bypass using enclosed alphanumerics
    - Cloud Instances
        - AWS
            
            ```markup
            http://instance-data
            http://169.254.169.254
            http://169.254.169.254/latest/user-data
            http://169.254.169.254/latest/user-data/iam/security-credentials/[ROLE NAME]
            http://169.254.169.254/latest/meta-data/
            http://169.254.169.254/latest/meta-data/iam/security-credentials/[ROLE NAME]
            http://169.254.169.254/latest/meta-data/iam/security-credentials/PhotonInstance
            http://169.254.169.254/latest/meta-data/ami-id
            http://169.254.169.254/latest/meta-data/reservation-id
            http://169.254.169.254/latest/meta-data/hostname
            http://169.254.169.254/latest/meta-data/public-keys/
            http://169.254.169.254/latest/meta-data/public-keys/0/openssh-key
            http://169.254.169.254/latest/meta-data/public-keys/[ID]/openssh-key
            http://169.254.169.254/latest/meta-data/iam/security-credentials/dummy
            http://169.254.169.254/latest/meta-data/iam/security-credentials/s3access
            http://169.254.169.254/latest/dynamic/instance-identity/document
            ```
            
        - Google Cloud
            
            ```markup
            http://169.254.169.254/computeMetadata/v1/
            http://metadata.google.internal/computeMetadata/v1/
            http://metadata/computeMetadata/v1/
            http://metadata.google.internal/computeMetadata/v1/instance/hostname
            http://metadata.google.internal/computeMetadata/v1/instance/id
            http://metadata.google.internal/computeMetadata/v1/project/project-id
            ```
            
        - Digital Ocean
            
            ```markup
            curl http://169.254.169.254/metadata/v1/id
            http://169.254.169.254/metadata/v1.json
            http://169.254.169.254/metadata/v1/ 
            http://169.254.169.254/metadata/v1/id
            http://169.254.169.254/metadata/v1/user-data
            http://169.254.169.254/metadata/v1/hostname
            http://169.254.169.254/metadata/v1/region
            http://169.254.169.254/metadata/v1/interfaces/public/0/ipv6/address
            ```
            
        - Azure
            
            ```
            http://169.254.169.254/metadata/v1/maintenance
            http://169.254.169.254/metadata/instance?api-version=2017-04-02
            http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2017-04-02&format=text
            ```
            
    - [ ]  Bypassing via open redirection
- **File Upload Testing**
    - [ ]  upload the malicious file to the archive upload functionality and observe how the application responds
    - [ ]  upload a file and change its path to overwrite an existing system file
    - [ ]  Large File Denial of Service
    - [ ]  Metadata Leakage
    - [ ]  ImageMagick Library Attacks
    - [ ]  Pixel Flood Attack
    - Bypasses
        - [ ]  Null Byte (%00) Bypass
        - [ ]  Content-Type Bypass
        - [ ]  Magic Byte Bypass
        - [ ]  Client-Side Validation Bypass
        - [ ]  Blacklisted Extension Bypass
        - [ ]  Homographic Character Bypass
- **CAPTCHA Testing**
    - [ ]  Missing Captcha Field Integrity Checks
    - [ ]  HTTP Verb Manipulation
    - [ ]  Content Type Conversion
    - [ ]  Reusuable Captcha
    - [ ]  Check if captcha is retrievable with the absolute path such as
    [www.tushar.com/internal/captcha/images/24.png](http://www.chintan.com/internal/captcha/images/24.png)
    - [ ]  Check for the server side validation for CAPTCHA.Remove captcha block from GUI using firebug addon and submit request to the server
    - [ ]  Check if image recognition can be done with OCR tool?
- **JWT Token Testing**
    - [ ]  Brute-forcing secret keys
    - [ ]  Signing a new token with the “none” algorithm
    - [ ]  Changing the signing algorithm of the token (for fuzzing purposes)
    - [ ]  Signing the asymmetrically-signed token to its symmetric algorithm match (when you have the original public key)
- **Websockets Testing**
    - [ ]  Intercepting and modifying WebSocket messages
    - [ ]  Websockets MITM attempts
    - [ ]  Testing secret header websocket
    - [ ]  Content stealing in websockets
    - [ ]  Token authentication testing in websockets
- **GraphQL Vulnerabilities Testing**
    - [ ]  Inconsistent Authorization Checks
    - [ ]  Missing Validation of Custom Scalars
    - [ ]  Failure to Appropriately Rate-limit
    - [ ]  Introspection Query Enabled/Disabled
- **WordPress Common Vulnerabilities**
    - [ ]  XSPA in wordpress
    - [ ]  Bruteforce in wp-login.php
    - [ ]  Information disclosure wordpress username
    - [ ]  Backup file wp-config exposed
    - [ ]  Log files exposed
    - [ ]  Denial of Service via load-styles.php
    - [ ]  Denial of Service via load-scripts.php
    - [ ]  DDOS using xmlrpc.php
    - [ ]  CVE-2018-6389
    - [ ]  CVE-2021-24364
    - [ ]  WP-Cronjob DOS
- **XPath Injection**
    - [ ]  XPath injection to bypass authentication
    - [ ]  XPath injection to exfiltrate data
    - [ ]  Blind and Time-based XPath injections to exfiltrate data
- **LDAP Injection**
    - [ ]  LDAP injection to bypass authentication
    - [ ]  LDAP injection to exfiltrate data
- **Denial of Service**
    - [ ]  Cookie bomb
    - [ ]  Pixel flood, using image with a huge pixels
    - [ ]  Frame flood, using GIF with a huge frame
    - [ ]  ReDoS (Regex DoS)
    - [ ]  CPDoS (Cache Poisoned Denial of Service)
- **403 Bypass**
    - [ ]  Using "X-Original-URL" header
    - [ ]  Appending **%2e** after the first slash
    - [ ]  Try add dot (.) slash (/) and semicolon (;) in the URL
    - [ ]  Add "..;/" after the directory name
    - [ ]  Try to uppercase the alphabet in the url
    - [ ]  Tool-[**bypass-403**](https://github.com/daffainfo/bypass-403)
    - [ ]  Burp Extension-[403 Bypasser](https://portswigger.net/bappstore/444407b96d9c4de0adb7aed89e826122)
- **Other Test Cases (All Categories)**
    - Testing for Role authorization
        - [ ]  Check if normal user can access the resources of high privileged users?
        - [ ]  Forced browsing
        - [ ]  Insecure direct object reference
        - [ ]  Parameter tampering to switch user account to high privileged user
    - Check for security headers and at least
        - [ ]  X Frame Options
        - [ ]  X-XSS header
        - [ ]  HSTS header
        - [ ]  CSP header
        - [ ]  Referrer Policy
        - [ ]  Cache Control
        - [ ]  Public key pins
    - Blind OS command injection
        - [ ]  using time delays
        - [ ]  by redirecting output
        - [ ]  with out-of-band interaction
        - [ ]  with out-of-band data exfiltration
    - [ ]  Command injection on CSV export (Upload/Download)
    - [ ]  CSV Excel Macro Injection
    - [ ]  If you find phpinfo.php file, check for the configuration leakage and try to exploit any network vulnerability.
    - [ ]  Parameter Pollution Social Media Sharing Buttons
    - Broken Cryptography
        - [ ]  Cryptography Implementation Flaw
        - [ ]  Encrypted Information Compromised
        - [ ]  Weak Ciphers Used for Encryption
    - Web Services Testing
        - [ ]  Test for directory traversal
        - [ ]  Web services documentation disclosure Enumeration of services, data types, input types boundaries and limits
- **Burp Suite Extensions**
    - Scanners
        - [ ]  https://github.com/albinowax/ActiveScanPlusPlus
        - [ ]  https://github.com/portswigger/additional-scanner-checks
        - [ ]  https://github.com/PortSwigger/backslash-powered-scanner
    - Information Gathering
        - [ ]  https://github.com/capt-meelo/filter-options-method
        - [ ]  https://github.com/moeinfatehi/Admin-Panel_Finder
        - [ ]  https://github.com/raise-isayan/BigIPDiscover
        - [ ]  https://github.com/P3GLEG/PwnBack
    - Vulnerability Analysis
        - [ ]  https://github.com/matrix/Burp-NoSQLiScanner






# WEB APPLICATION PENTESTING CHECKLIST

**OWASP Based Checklist  🌟🌟**

**500+ Test Cases 🚀🚀**

Notion link: https://hariprasaanth.notion.site/WEB-APPLICATION-PENTESTING-CHECKLIST-0f02d8074b9d4af7b12b8da2d46ac998
</br></br>

- **INFORMATION GATHERING**
    
    **Open Source Reconnaissance**
    
    - [ ]  Perform Google Dorks search
    - [ ]  Perform OSINT
    
    **Fingerprinting Web Server**
    
    - [ ]  Find the type of Web Server
    - [ ]  Find the version details of the Web Server
    
    **Looking For Metafiles**
    
    - [ ]  View the Robots.txt file
    - [ ]  View the Sitemap.xml file
    - [ ]  View the Humans.txt file
    - [ ]  View the Security.txt file
    
    **Enumerating Web Server’s Applications**
    
    - [ ]  Enumerating with Nmap
    - [ ]  Enumerating with Netcat
    - [ ]  Perform a DNS lookup
    - [ ]  Perform a Reverse DNS lookup
    
    **Review The Web Contents**
    
    - [ ]  Inspect the page source for sensitive info
    - [ ]  Try to find Sensitive Javascript codes
    - [ ]  Try to find any keys
    - [ ]  Make sure the autocomplete is disabled
    
    **Identifying Application’s Entry Points**
    
    - [ ]  Identify what the methods used are?
    - [ ]  Identify where the methods used are?
    - [ ]  Identify the Injection point
    
    **Mapping Execution Paths**
    
    - [ ]  Use Burp Suite
    - [ ]  Use Dirsearch
    - [ ]  Use Gobuster
    
    **Fingerprint Web Application Framework**
    
    - [ ]  Use the Wappalyzer browser extension
    - [ ]  Use Whatweb
    - [ ]  View URL extensions
    - [ ]  View HTML source code
    - [ ]  View the cookie parameter
    - [ ]  View the HTTP headers
    
    **Map Application Architecture**
    
    - [ ]  Map the overall site structure
    
- **CONFIGURATION & DEPLOYMENT MANAGEMENT TESTING**
    
    **Test Network Configuration**
    
    - [ ]  Check the network configuration
    - [ ]  Check for default settings
    - [ ]  Check for default credentials
    
    **Test Application Configuration**
    
    - [ ]  Ensure only required modules are used
    - [ ]  Ensure unwanted modules are disabled
    - [ ]  Ensure the server can handle DOS
    - [ ]  Check how the application is handling 4xx & 5xx errors
    - [ ]  Check for the privilege required to run
    - [ ]  Check logs for sensitive info
    
    **Test File Extension Handling**
    
    - [ ]  Ensure the server won’t return sensitive extensions
    - [ ]  Ensure the server won’t accept malicious extensions
    - [ ]  Test for file upload vulnerabilities
    
    **Review Backup & Unreferenced Files**
    
    - [ ]  Ensure unreferenced files don’t contain any sensitive info
    - [ ]  Ensure the namings of old and new backup files
    - [ ]  Check the functionality of unreferenced pages
    
    **Enumerate Infrastructure & Admin Interfaces**
    
    - [ ]  Try to find the Infrastructure Interface
    - [ ]  Try to find the Admin Interface
    - [ ]  Identify the hidden admin functionalities
    
    **Testing HTTP Methods**
    
    - [ ]  Discover the supported methods
    - [ ]  Ensure the PUT method is disabled
    - [ ]  Ensure the OPTIONS method is disabled
    - [ ]  Test access control bypass
    - [ ]  Test for XST attacks
    - [ ]  Test for HTTP method overriding
    
    **Test HSTS**
    
    - [ ]  Ensure HSTS is enabled
    
    **Test RIA Cross Domain Policy**
    
    - [ ]  Check for Adobe’s Cross Domain Policy
    - [ ]  Ensure it has the least privilege
    
    **Test File Permission**
    
    - [ ]  Ensure the permissions for sensitive files
    - [ ]  Test for directory enumeration
    
    **Test For Subdomain Takeover**
    
    - [ ]  Test DNS, A, and CNAME records for subdomain takeover
    - [ ]  Test NS records for subdomain takeover
    - [ ]  Test 404 response for subdomain takeover
    
    **Test Cloud Storage**
    
    - [ ]  Check the sensitive paths of AWS
    - [ ]  Check the sensitive paths of Google Cloud
    - [ ]  Check the sensitive paths of Azure
    
- **IDENTITY MANAGEMENT TESTING**
    
    **Test Role Definitions**
    
    - [ ]  Test for forced browsing
    - [ ]  Test for IDOR (Insecure Direct Object Reference)
    - [ ]  Test for parameter tampering
    - [ ]  Ensure low privilege users can’t able to access high privilege resources
    
    **Test User Registration Process**
    
    - [ ]  Ensure the same user or identity can’t register again and again
    - [ ]  Ensure the registrations are verified
    - [ ]  Ensure disposable email addresses are rejected
    - [ ]  Check what proof is required for successful registration
    
    **Test Account Provisioning Process**
    
    - [ ]  Check the verification for the provisioning process
    - [ ]  Check the verification for the de-provisioning process
    - [ ]  Check the provisioning rights for an admin user to other users
    - [ ]  Check whether a user is able to de-provision themself or not?
    - [ ]  Check for the resources of a de-provisioned user
    
    **Testing For Account Enumeration**
    
    - [ ]  Check the response when a valid username and password entered
    - [ ]  Check the response when a valid username and an invalid password entered
    - [ ]  Check the response when an invalid username and password entered
    - [ ]  Ensure the rate-limiting functionality is enabled in username and password fields
    
    **Test For Weak Username Policy**
    
    - [ ]  Check the response for both valid and invalid usernames
    - [ ]  Check for username enumeration
    
- **AUTHENTICATION TESTING**
    
    **Test For Un-Encrypted Channel**
    
    - [ ]  Check for the HTTP login page
    - [ ]  Check for the HTTP register or sign-in page
    - [ ]  Check for HTTP forgot password page
    - [ ]  Check for HTTP change password
    - [ ]  Check for resources on HTTP after logout
    - [ ]  Test for forced browsing to HTTP pages
    
    **Test For Default Credentials**
    
    - [ ]  Test with default credentials
    - [ ]  Test organization name as credentials
    - [ ]  Test for response manipulation
    - [ ]  Test for the default username and a blank password
    - [ ]  Review the page source for credentials
    
    **Test For Weak Lockout Mechanism**
    
    - [ ]  Ensure the account has been locked after 3-5 incorrect attempts
    - [ ]  Ensure the system accepts only the valid CAPTCHA
    - [ ]  Ensure the system rejects the invalid CAPTCHA
    - [ ]  Ensure CAPTCHA code regenerated after reloaded
    - [ ]  Ensure CAPTCHA reloads after entering the wrong code
    - [ ]  Ensure the user has a recovery option for a lockout account
    
    **Test For Bypassing Authentication Schema**
    
    - [ ]  Test forced browsing directly to the internal dashboard without login
    - [ ]  Test for session ID prediction
    - [ ]  Test for authentication parameter tampering
    - [ ]  Test for SQL injection on the login page
    - [ ]  Test to gain access with the help of session ID
    - [ ]  Test multiple logins allowed or not?
    
    **Test For Vulnerable Remember Password**
    
    - [ ]  Ensure that the stored password is encrypted
    - [ ]  Ensure that the stored password is on the server-side
    
    **Test For Browser Cache Weakness**
    
    - [ ]  Ensure proper cache-control is set on sensitive pages
    - [ ]  Ensure no sensitive data is stored in the browser cache storage
    
    **Test For Weak Password Policy**
    
    - [ ]  Ensure the password policy is set to strong
    - [ ]  Check for password reusability
    - [ ]  Check the user is prevented to use his username as a password
    - [ ]  Check for the usage of common weak passwords
    - [ ]  Check the minimum password length to be set
    - [ ]  Check the maximum password length to be set
    
    **Testing For Weak Security Questions**
    
    - [ ]  Check for the complexity of the questions
    - [ ]  Check for brute-forcing
    
    **Test For Weak Password Reset Function**
    
    - [ ]  Check what information is required to reset the password
    - [ ]  Check for password reset function with HTTP
    - [ ]  Test the randomness of the password reset tokens
    - [ ]  Test the uniqueness of the password reset tokens
    - [ ]  Test for rate limiting on password reset tokens
    - [ ]  Ensure the token must expire after being used
    - [ ]  Ensure the token must expire after not being used for a long time
    
    **Test For Weak Password Change Function**
    
    - [ ]  Check if the old password asked to make a change
    - [ ]  Check for the uniqueness of the forgotten password
    - [ ]  Check for blank password change
    - [ ]  Check for password change function with HTTP
    - [ ]  Ensure the old password is not displayed after changed
    - [ ]  Ensure the other sessions got destroyed after the password change
    
    **Test For Weak Authentication In Alternative Channel**
    
    - [ ]  Test authentication on the desktop browsers
    - [ ]  Test authentication on the mobile browsers
    - [ ]  Test authentication in a different country
    - [ ]  Test authentication in a different language
    - [ ]  Test authentication on desktop applications
    - [ ]  Test authentication on mobile applications
    
- **AUTHORIZATION TESTING**
    
    **Testing Directory Traversal File Include**
    
    - [ ]  Identify the injection point on the URL
    - [ ]  Test for Local File Inclusion
    - [ ]  Test for Remote File Inclusion
    - [ ]  Test Traversal on the URL parameter
    - [ ]  Test Traversal on the cookie parameter
    
    **Testing Traversal With Encoding**
    
    - [ ]  Test Traversal with Base64 encoding
    - [ ]  Test Traversal with URL encoding
    - [ ]  Test Traversal with ASCII encoding
    - [ ]  Test Traversal with HTML encoding
    - [ ]  Test Traversal with Hex encoding
    - [ ]  Test Traversal with Binary encoding
    - [ ]  Test Traversal with Octal encoding
    - [ ]  Test Traversal with Gzip encoding
    
    **Testing Travesal With Different OS Schemes**
    
    - [ ]  Test Traversal with Unix schemes
    - [ ]  Test Traversal with Windows schemes
    - [ ]  Test Traversal with Mac schemes
    
    **Test Other Encoding Techniques**
    
    - [ ]  Test Traversal with Double encoding
    - [ ]  Test Traversal with all characters encode
    - [ ]  Test Traversal with only special characters encode
    
    **Test Authorization Schema Bypass**
    
    - [ ]  Test for Horizontal authorization schema bypass
    - [ ]  Test for Vertical authorization schema bypass
    - [ ]  Test override the target with custom headers
    
    **Test For Privilege Escalation**
    
    - [ ]  Identify the injection point
    - [ ]  Test for bypassing the security measures
    - [ ]  Test for forced browsing
    - [ ]  Test for IDOR
    - [ ]  Test for parameter tampering to high privileged user
    
    **Test For Insecure Direct Object Reference**
    
    - [ ]  Test to change the ID parameter
    - [ ]  Test to add parameters at the endpoints
    - [ ]  Test for HTTP parameter pollution
    - [ ]  Test by adding an extension at the end
    - [ ]  Test with outdated API versions
    - [ ]  Test by wrapping the ID with an array
    - [ ]  Test by wrapping the ID with a JSON object
    - [ ]  Test for JSON parameter pollution
    - [ ]  Test by changing the case
    - [ ]  Test for path traversal
    - [ ]  Test by changing words
    - [ ]  Test by changing methods
    
- **SESSION MANAGEMENT TESTING**
    
    **Test For Session Management Schema**
    
    - [ ]  Ensure all Set-Cookie directives are secure
    - [ ]  Ensure no cookie operation takes place over an unencrypted channel
    - [ ]  Ensure the cookie can’t be forced over an unencrypted channel
    - [ ]  Ensure the HTTPOnly flag is enabled
    - [ ]  Check if any cookies are persistent
    - [ ]  Check for session cookies and cookie expiration date/time
    - [ ]  Check for session fixation
    - [ ]  Check for concurrent login
    - [ ]  Check for session after logout
    - [ ]  Check for session after closing the browser
    - [ ]  Try decoding cookies (Base64, Hex, URL, etc)
    
    **Test For Cookie Attributes**
    
    - [ ]  Ensure the cookie must be set with the secure attribute
    - [ ]  Ensure the cookie must be set with the path attribute
    - [ ]  Ensure the cookie must have the HTTPOnly flag
    
    **Test For Session Fixation**
    
    - [ ]  Ensure new cookies have been issued upon a successful authentication
    - [ ]  Test manipulating the cookies
    
    **Test For Exposed Session Variables**
    
    - [ ]  Test for encryption
    - [ ]  Test for GET and POST vulnerabilities
    - [ ]  Test if GET request incorporating the session ID used
    - [ ]  Test by interchanging POST with GET method
    
    **Test For Back Refresh Attack**
    
    - [ ]  Test after password change
    - [ ]  Test after logout
    
    **Test For Cross Site Request Forgery**
    
    - [ ]  Check if the token is validated on the server-side or not
    - [ ]  Check if the token is validated for full or partial length
    - [ ]  Check by comparing the CSRF tokens for multiple dummy accounts
    - [ ]  Check CSRF by interchanging POST with GET method
    - [ ]  Check CSRF by removing the CSRF token parameter
    - [ ]  Check CSRF by removing the CSRF token and using a blank parameter
    - [ ]  Check CSRF by using unused tokens
    - [ ]  Check CSRF by replacing the CSRF token with its own values
    - [ ]  Check CSRF by changing the content type to form-multipart
    - [ ]  Check CSRF by changing or deleting some characters of the CSRF token
    - [ ]  Check CSRF by changing the referrer to Referrer
    - [ ]  Check CSRF by changing the host values
    - [ ]  Check CSRF alongside clickjacking
    
    **Test For Logout Functionality**
    
    - [ ]  Check the log out function on different pages
    - [ ]  Check for the visibility of the logout button
    - [ ]  Ensure after logout the session was ended
    - [ ]  Ensure after logout we can’t able to access the dashboard by pressing the back button
    - [ ]  Ensure proper session timeout has been set
    
    **Test For Session Timeout**
    
    - [ ]  Ensure there is a session timeout exists
    - [ ]  Ensure after the timeout, all of the tokens are destroyed
    
    **Test For Session Puzzling**
    
    - [ ]  Identify all the session variables
    - [ ]  Try to break the logical flow of the session generation
    
    **Test For Session Hijacking**
    
    - [ ]  Test session hijacking on target that doesn’t has HSTS enabled
    - [ ]  Test by login with the help of captured cookies
    
- **INPUT VALIDATION TESTING**
    
    **Test For Reflected Cross Site Scripting**
    
    - [ ]  Ensure these characters are filtered <>’’&””
    - [ ]  Test with a character escape sequence
    - [ ]  Test by replacing < and > with HTML entities &lt; and &gt;
    - [ ]  Test payload with both lower and upper case
    - [ ]  Test to break firewall regex by new line /r/n
    - [ ]  Test with double encoding
    - [ ]  Test with recursive filters
    - [ ]  Test injecting anchor tags without whitespace
    - [ ]  Test by replacing whitespace with bullets
    - [ ]  Test by changing HTTP methods
    
    **Test For Stored Cross Site Scripting**
    
    - [ ]  Identify stored input parameters that will reflect on the client-side
    - [ ]  Look for input parameters on the profile page
    - [ ]  Look for input parameters on the shopping cart page
    - [ ]  Look for input parameters on the file upload page
    - [ ]  Look for input parameters on the settings page
    - [ ]  Look for input parameters on the forum, comment page
    - [ ]  Test uploading a file with XSS payload as its file name
    - [ ]  Test with HTML tags
    
    **Test For HTTP Parameter Pollution**
    
    - [ ]  Identify the backend server and parsing method used
    - [ ]  Try to access the injection point
    - [ ]  Try to bypass the input filters using HTTP Parameter Pollution
    
    **Test For SQL Injection**
    
    - [ ]  Test SQL Injection on authentication forms
    - [ ]  Test SQL Injection on the search bar
    - [ ]  Test SQL Injection on editable characteristics
    - [ ]  Try to find SQL keywords or entry point detections
    - [ ]  Try to inject SQL queries
    - [ ]  Use tools like SQLmap or Hackbar
    - [ ]  Use Google dorks to find the SQL keywords
    - [ ]  Try GET based SQL Injection
    - [ ]  Try POST based SQL Injection
    - [ ]  Try COOKIE based SQL Injection
    - [ ]  Try HEADER based SQL Injection
    - [ ]  Try SQL Injection with null bytes before the SQL query
    - [ ]  Try SQL Injection with URL encoding
    - [ ]  Try SQL Injection with both lower and upper cases
    - [ ]  Try SQL Injection with SQL Tamper scripts
    - [ ]  Try SQL Injection with SQL Time delay payloads
    - [ ]  Try SQL Injection with SQL Conditional delays
    - [ ]  Try SQL Injection with Boolean based SQL
    - [ ]  Try SQL Injection with Time based SQL
    
    **Test For LDAP Injection**
    
    - [ ]  Use LDAP search filters
    - [ ]  Try LDAP Injection for access control bypass
    
    **Testing For XML Injection**
    
    - [ ]  Check if the application is using XML for processing
    - [ ]  Identify the XML Injection point by XML metacharacter
    - [ ]  Construct XSS payload on top of XML
    
    **Test For Server Side Includes**
    
    - [ ]  Use Google dorks to find the SSI
    - [ ]  Construct RCE on top of SSI
    - [ ]  Construct other injections on top of SSI
    - [ ]  Test Injecting SSI on login pages, header fields, referrer, etc
    
    **Test For XPATH Injection**
    
    - [ ]  Identify XPATH Injection point
    - [ ]  Test for XPATH Injection
    
    **Test For IMAP SMTP Injection**
    
    - [ ]  Identify IMAP SMTP Injection point
    - [ ]  Understand the data flow
    - [ ]  Understand the deployment structure of the system
    - [ ]  Assess the injection impact
    
    **Test For Local File Inclusion**
    
    - [ ]  Look for LFI keywords
    - [ ]  Try to change the local path
    - [ ]  Use the LFI payload list
    - [ ]  Test LFI by adding a null byte at the end
    
    **Test For Remote File Inclusion**
    
    - [ ]  Look for RFI keywords
    - [ ]  Try to change the remote path
    - [ ]  Use the RFI payload list
    
    **Test For Command Injection**
    
    - [ ]  Identify the Injection points
    - [ ]  Look for Command Injection keywords
    - [ ]  Test Command Injection using different delimiters
    - [ ]  Test Command Injection with payload list
    - [ ]  Test Command Injection with different OS commands
    
    **Test For Format String Injection**
    
    - [ ]  Identify the Injection points
    - [ ]  Use different format parameters as payloads
    - [ ]  Assess the injection impact
    
    **Test For Host Header Injection**
    
    - [ ]  Test for HHI by changing the real Host parameter
    - [ ]  Test for HHI by adding X-Forwarded Host parameter
    - [ ]  Test for HHI by swapping the real Host and X-Forwarded Host parameter
    - [ ]  Test for HHI by adding two Host parameters
    - [ ]  Test for HHI by adding the target values in front of the original values
    - [ ]  Test for HHI by adding the target with a slash after the original values
    - [ ]  Test for HHI with other injections on the Host parameter
    - [ ]  Test for HHI by password reset poisoning
    
    **Test For Server Side Request Forgery**
    
    - [ ]  Look for SSRF keywords
    - [ ]  Search for SSRF keywords only under the request header and body
    - [ ]  Identify the Injection points
    - [ ]  Test if the Injection points are exploitable
    - [ ]  Assess the injection impact
    
    **Test For Server Side Template Injection**
    
    - [ ]  Identify the Template injection vulnerability points
    - [ ]  Identify the Templating engine
    - [ ]  Use the tplmap to exploit
    
- **ERROR HANDLING TESTING**
    
    **Test For Improper Error Handling**
    
    - [ ]  Identify the error output
    - [ ]  Analyze the different outputs returned
    - [ ]  Look for common error handling flaws
    - [ ]  Test error handling by modifying the URL parameter
    - [ ]  Test error handling by uploading unrecognized file formats
    - [ ]  Test error handling by entering unrecognized inputs
    - [ ]  Test error handling by making all possible errors
    
- **WEAK CRYPTOGRAPHY TESTING**
    
    **Test For Weak Transport Layer Security**
    
    - [ ]  Test for DROWN weakness on SSLv2 protocol
    - [ ]  Test for POODLE weakness on SSLv3 protocol
    - [ ]  Test for BEAST weakness on TLSv1.0 protocol
    - [ ]  Test for FREAK weakness on export cipher suites
    - [ ]  Test for Null ciphers
    - [ ]  Test for NOMORE weakness on RC4
    - [ ]  Test for LUCKY 13 weakness on CBC mode ciphers
    - [ ]  Test for CRIME weakness on TLS compression
    - [ ]  Test for LOGJAM on DHE keys
    - [ ]  Ensure the digital certificates should have at least 2048 bits of key length
    - [ ]  Ensure the digital certificates should have at least SHA-256 signature algorithm
    - [ ]  Ensure the digital certificates should not use MDF and SHA-1
    - [ ]  Ensure the validity of the digital certificate
    - [ ]  Ensure the minimum key length requirements
    - [ ]  Look for weak cipher suites
    
- **BUSINESS LOGIC TESTING**
    
    **Test For Business Logic**
    
    - [ ]  Identify the logic of how the application works
    - [ ]  Identify the functionality of all the buttons
    - [ ]  Test by changing the numerical values into high or negative values
    - [ ]  Test by changing the quantity
    - [ ]  Test by modifying the payments
    - [ ]  Test for parameter tampering
    
    **Test For Malicious File Upload**
    
    - [ ]  Test malicious file upload by uploading malicious files
    - [ ]  Test malicious file upload by putting your IP address on the file name
    - [ ]  Test malicious file upload by right to left override
    - [ ]  Test malicious file upload by encoded file name
    - [ ]  Test malicious file upload by XSS payload on the file name
    - [ ]  Test malicious file upload by RCE payload on the file name
    - [ ]  Test malicious file upload by LFI payload on the file name
    - [ ]  Test malicious file upload by RFI payload on the file name
    - [ ]  Test malicious file upload by SQL payload on the file name
    - [ ]  Test malicious file upload by other injections on the file name
    - [ ]  Test malicious file upload by Inserting the payload inside of an image by the bmp.pl tool
    - [ ]  Test malicious file upload by uploading large files (leads to DOS)
    
- **CLIENT SIDE TESTING**
    
    **Test For DOM Based Cross Site Scripting**
    
    - [ ]  Try to identify DOM sinks
    - [ ]  Build payloads to that DOM sink type
    
    **Test For URL Redirect**
    
    - [ ]  Look for URL redirect parameters
    - [ ]  Test for URL redirection on domain parameters
    - [ ]  Test for URL redirection by using a payload list
    - [ ]  Test for URL redirection by using a whitelisted word at the end
    - [ ]  Test for URL redirection by creating a new subdomain with the same as the target
    - [ ]  Test for URL redirection by XSS
    - [ ]  Test for URL redirection by profile URL flaw
    
    **Test For Cross Origin Resource Sharing**
    
    - [ ]  Look for “Access-Control-Allow-Origin” on the response
    - [ ]  Use the CORS HTML exploit code for further exploitation
    
    **Test For Clickjacking**
    
    - [ ]  Ensure “X-Frame-Options” headers are enabled
    - [ ]  Exploit with iframe HTML code for POC
    
- **OTHER COMMON ISSUES**
    
    **Test For No-Rate Limiting**
    
    - [ ]  Ensure rate limiting is enabled
    - [ ]  Try to bypass rate limiting by changing the case of the endpoints
    - [ ]  Try to bypass rate limiting by adding / at the end of the URL
    - [ ]  Try to bypass rate limiting by adding HTTP headers
    - [ ]  Try to bypass rate limiting by adding HTTP headers twice
    - [ ]  Try to bypass rate limiting by adding Origin headers
    - [ ]  Try to bypass rate limiting by IP rotation
    - [ ]  Try to bypass rate limiting by using null bytes at the end
    - [ ]  Try to bypass rate limiting by using race conditions
    
    **Test For EXIF Geodata**
    
    - [ ]  Ensure the website is striping the geodata
    - [ ]  Test with EXIF checker
    
    **Test For Broken Link Hijack**
    
    - [ ]  Ensure there is no broken links are there
    - [ ]  Test broken links by using the blc tool
    
    **Test For SPF**
    
    - [ ]  Ensure the website is having SPF record
    - [ ]  Test SPF by nslookup command
    
    **Test For Weak 2FA**
    
    - [ ]  Try to bypass 2FA by using poor session management
    - [ ]  Try to bypass 2FA via the OAuth mechanism
    - [ ]  Try to bypass 2FA via brute-forcing
    - [ ]  Try to bypass 2FA via response manipulation
    - [ ]  Try to bypass 2FA by using activation links to login
    - [ ]  Try to bypass 2FA by using status code manipulation
    - [ ]  Try to bypass 2FA by changing the email or password
    - [ ]  Try to bypass 2FA by using a null or empty entry
    - [ ]  Try to bypass 2FA by changing the boolean into false
    - [ ]  Try to bypass 2FA by removing the 2FA parameter on the request
    
    **Test For Weak OTP Implementation**
    
    - [ ]  Try to bypass OTP by entering the old OTP
    - [ ]  Try to bypass OTP by brute-forcing
    - [ ]  Try to bypass OTP by using a null or empty entry
    - [ ]  Try to bypass OTP by response manipulation
    - [ ]  Try to bypass OTP by status code manipulation







# Bug Bounty Checklist for Web App

## Table of Contents

* [Recon on wildcard domain](#"Recon_on_wildcard_domain")
* [Single domain](#Single_domain)
* [Information Gathering](#Information)
* [Configuration Management](#Configuration)
* [Secure Transmission](#Transmission)
* [Authentication](#Authentication)
* [Session Management](#Session)
* [Authorization](#Authorization)
* [Data Validation](#Validation)
* [Denial of Service](#Denial)
* [Business Logic](#Business)
* [Cryptography](#Cryptography)
* [Risky Functionality - File Uploads](#File)
* [Risky Functionality - Card Payment](#Card)
* [HTML 5](#HTML)


## <a name="Recon_on_wildcard_domain">Recon on wildcard domain</a>  

- [ ] Run amass  
- [ ] Run subfinder  
- [ ] Run assetfinder  
- [ ] Run dnsgen  
- [ ] Run massdns  
- [ ] Use httprobe  
- [ ] Run aquatone (screenshot for alive host)  


## <a name="Single_domain">Single Domain</a>  

### Scanning  

- [ ] Nmap scan   
- [ ] Burp crawler   
- [ ] ffuf (directory and file fuzzing)
- [ ] hakrawler/gau/paramspider  
- [ ] Linkfinder  
- [ ] Url with Android application   

### Manual checking  

- [ ] Shodan  
- [ ] Censys  
- [ ] Google dorks  
- [ ] Pastebin  
- [ ] Github  
- [ ] OSINT     

### <a name="Information">Information Gathering</a>
- [ ] Manually explore the site  
- [ ] Spider/crawl for missed or hidden content  
- [ ] Check for files that expose content, such as robots.txt, sitemap.xml, .DS_Store  
- [ ] Check the caches of major search engines for publicly accessible sites  
- [ ] Check for differences in content based on User Agent (eg, Mobile sites, access as a Search engine Crawler)  
- [ ] Perform Web Application Fingerprinting  
- [ ] Identify technologies used  
- [ ] Identify user roles  
- [ ] Identify application entry points  
- [ ] Identify client-side code  
- [ ] Identify multiple versions/channels (e.g. web, mobile web, mobile app, web services)  
- [ ] Identify co-hosted and related applications  
- [ ] Identify all hostnames and ports  
- [ ] Identify third-party hosted content  
- [ ] Identify Debug parameters  


### <a name="Configuration">Configuration Management</a>

- [ ] Check for commonly used application and administrative URLs  
- [ ] Check for old, backup and unreferenced files  
- [ ] Check HTTP methods supported and Cross Site Tracing (XST)  
- [ ] Test file extensions handling  
- [ ] Test for security HTTP headers (e.g. CSP, X-Frame-Options, HSTS)  
- [ ] Test for policies (e.g. Flash, Silverlight, robots)  
- [ ] Test for non-production data in live environment, and vice-versa  
- [ ] Check for sensitive data in client-side code (e.g. API keys, credentials)  


### <a name="Transmission">Secure Transmission</a>

- [ ] Check SSL Version, Algorithms, Key length  
- [ ] Check for Digital Certificate Validity (Duration, Signature and CN)  
- [ ] Check credentials only delivered over HTTPS  
- [ ] Check that the login form is delivered over HTTPS  
- [ ] Check session tokens only delivered over HTTPS  
- [ ] Check if HTTP Strict Transport Security (HSTS) in use  



### <a name="Authentication">Authentication</a>
- [ ] Test for user enumeration  
- [ ] Test for authentication bypass  
- [ ] Test for bruteforce protection  
- [ ] Test password quality rules  
- [ ] Test remember me functionality  
- [ ] Test for autocomplete on password forms/input  
- [ ] Test password reset and/or recovery  
- [ ] Test password change process  
- [ ] Test CAPTCHA  
- [ ] Test multi factor authentication  
- [ ] Test for logout functionality presence  
- [ ] Test for cache management on HTTP (eg Pragma, Expires, Max-age)  
- [ ] Test for default logins  
- [ ] Test for user-accessible authentication history  
- [ ] Test for out-of channel notification of account lockouts and successful password changes  
- [ ] Test for consistent authentication across applications with shared authentication schema / SSO  



### <a name="Session">Session Management</a>
- [ ] Establish how session management is handled in the application (eg, tokens in cookies, token in URL)  
- [ ] Check session tokens for cookie flags (httpOnly and secure)  
- [ ] Check session cookie scope (path and domain)  
- [ ] Check session cookie duration (expires and max-age)  
- [ ] Check session termination after a maximum lifetime  
- [ ] Check session termination after relative timeout  
- [ ] Check session termination after logout  
- [ ] Test to see if users can have multiple simultaneous sessions  
- [ ] Test session cookies for randomness  
- [ ] Confirm that new session tokens are issued on login, role change and logout  
- [ ] Test for consistent session management across applications with shared session management  
- [ ] Test for session puzzling  
- [ ] Test for CSRF and clickjacking  



### <a name="Authorization">Authorization</a>
- [ ] Test for path traversal  
- [ ] Test for bypassing authorization schema  
- [ ] Test for vertical Access control problems (a.k.a. Privilege Escalation)  
- [ ] Test for horizontal Access control problems (between two users at the same privilege level)  
- [ ] Test for missing authorization  


### <a name="Validation">Data Validation</a>
- [ ] Test for Reflected Cross Site Scripting  
- [ ] Test for Stored Cross Site Scripting  
- [ ] Test for DOM based Cross Site Scripting  
- [ ] Test for Cross Site Flashing  
- [ ] Test for HTML Injection  
- [ ] Test for SQL Injection  
- [ ] Test for LDAP Injection  
- [ ] Test for ORM Injection  
- [ ] Test for XML Injection  
- [ ] Test for XXE Injection  
- [ ] Test for SSI Injection  
- [ ] Test for XPath Injection  
- [ ] Test for XQuery Injection  
- [ ] Test for IMAP/SMTP Injection  
- [ ] Test for Code Injection  
- [ ] Test for Expression Language Injection  
- [ ] Test for Command Injection  
- [ ] Test for Overflow (Stack, Heap and Integer)  
- [ ] Test for Format String  
- [ ] Test for incubated vulnerabilities  
- [ ] Test for HTTP Splitting/Smuggling  
- [ ] Test for HTTP Verb Tampering  
- [ ] Test for Open Redirection  
- [ ] Test for Local File Inclusion  
- [ ] Test for Remote File Inclusion  
- [ ] Compare client-side and server-side validation rules  
- [ ] Test for NoSQL injection  
- [ ] Test for HTTP parameter pollution  
- [ ] Test for auto-binding  
- [ ] Test for Mass Assignment  
- [ ] Test for NULL/Invalid Session Cookie  

### <a name="Denial">Denial of Service</a>
- [ ] Test for anti-automation  
- [ ] Test for account lockout  
- [ ] Test for HTTP protocol DoS  
- [ ] Test for SQL wildcard DoS  


### <a name="Business">Business Logic</a>
- [ ] Test for feature misuse  
- [ ] Test for lack of non-repudiation  
- [ ] Test for trust relationships  
- [ ] Test for integrity of data  
- [ ] Test segregation of duties  


### <a name="Cryptography">Cryptography</a>
- [ ] Check if data which should be encrypted is not  
- [ ] Check for wrong algorithms usage depending on context  
- [ ] Check for weak algorithms usage  
- [ ] Check for proper use of salting  
- [ ] Check for randomness functions  


### <a name="File">Risky Functionality - File Uploads</a>
- [ ] Test that acceptable file types are whitelisted  
- [ ] Test that file size limits, upload frequency and total file counts are defined and are enforced  
- [ ] Test that file contents match the defined file type  
- [ ] Test that all file uploads have Anti-Virus scanning in-place.  
- [ ] Test that unsafe filenames are sanitised  
- [ ] Test that uploaded files are not directly accessible within the web root  
- [ ] Test that uploaded files are not served on the same hostname/port  
- [ ] Test that files and other media are integrated with the authentication and authorisation schemas  


### <a name="Card">Risky Functionality - Card Payment</a>
- [ ] Test for known vulnerabilities and configuration issues on Web Server and Web Application  
- [ ] Test for default or guessable password  
- [ ] Test for non-production data in live environment, and vice-versa  
- [ ] Test for Injection vulnerabilities  
- [ ] Test for Buffer Overflows  
- [ ] Test for Insecure Cryptographic Storage  
- [ ] Test for Insufficient Transport Layer Protection  
- [ ] Test for Improper Error Handling  
- [ ] Test for all vulnerabilities with a CVSS v2 score > 4.0  
- [ ] Test for Authentication and Authorization issues  
- [ ] Test for CSRF  


### <a name="HTML">HTML 5</a>
- [ ] Test Web Messaging  
- [ ] Test for Web Storage SQL injection  
- [ ] Check CORS implementation  
- [ ] Check Offline Web Application  

Source:  
[OWASP](https://www.owasp.org/index.php/Web_Application_Security_Testing_Cheat_Sheet)  







## Web Application attack checklist

### Recon and analysis

- [ ] Map visible content
- [ ] Discover hidden & default content
- [ ] Test for debug parameters
- [ ] Identify data entry points
- [ ] Identify the technologies used
- [ ] Map the attack surface

### Test handling of access

- [ ] Authentication
- [ ] Test password quality rules
- [ ] Test for username enumeration
- [ ] Test resilience to password guessing
- [ ] Test any account recovery function
- [ ] Test any "remember me" function
- [ ] Test any impersonation function
- [ ] Test username uniqueness
- [ ] Check for unsafe distribution  of credentials
- [ ] Test for fail-open conditions
- [ ] Test any multi-stage mechanisms
- [ ] Session handling
- [ ] Test tokens for meaning
- [ ] Test tokens for predictability
- [ ] Check for insecure transmission  of tokens
- [ ] Check for disclosure of tokens  in logs
- [ ] Check mapping of tokens to sessions
- [ ] Check session termination
- [ ] Check for session fixation
- [ ] Check for cross-site request forgery
- [ ] Check cookie scope
- [ ] Access controls
- [ ] Understand the access control  requirements
- [ ] Test effectiveness of controls,  using multiple accounts if possible
- [ ] Test for insecure access control  methods (request parameters, Referer header, etc)

### Test handling of input

- [ ] Fuzz all request parameters
- [ ] Test for SQL injection
- [ ] Identify all reflected data
- [ ] Test for reflected XSS
- [ ] Test for HTTP header injection
- [ ] Test for arbitrary redirection
- [ ] Test for stored attacks
- [ ] Test for OS command injection
- [ ] Test for path traversal
- [ ] Test for script injection
- [ ] Test for file inclusion
- [ ] Test for SMTP injection
- [ ] Test for native software flaws  (buffer overflow, integer bugs, format strings)
- [ ] Test for SOAP injection
- [ ] Test for LDAP injection
- [ ] Test for XPath injection

### Test application logic

- [ ] Identify the logic attack surface
- [ ] Test transmission of data via the  client
- [ ] Test for reliance on  client-side input validation
- [ ] Test any thick-client components (Java, ActiveX, Flash)
- [ ] Test multi-stage processes for  logic flaws
- [ ] Test handling of  incomplete input
- [ ] Test trust  boundaries
- [ ] Test transaction  logic

### Assess application hosting

- [ ] Test segregation in shared  infrastructures
- [ ] Test segregation  between ASP-hosted applications
- [ ] Test for web server vulnerabilities
- [ ] Default credentials
- [ ] Default content
- [ ] Dangerous HTTP methods
- [ ] Proxy functionality
- [ ] Virtual hosting mis-configuration
- [ ] Bugs in web server software

### Miscellaneous tests

- [ ] Check for DOM-based attacks
- [ ] Check for frame injection
- [ ] Check for local privacy vulnerabilities
- [ ] Persistent cookies
- [ ] Caching
- [ ] Sensitive data in URL parameters
- [ ] Forms with autocomplete enabled
- [ ] Follow up any information leakage
- [ ] Check for weak SSL ciphers









# Fast Testing Checklist
A combination of my own methodology and the Web Application Hacker's Handbook Task checklist, as a Github-Flavored Markdown file
## Contents
- [Recon and analysis](#recon-and-analysis)
- [Test handling of access](#test-handling-of-access)
- [Test handling of input](#test-handling-of-input)
- [Test application logic](#test-application-logic)
- [Assess application hosting](#assess-application-hosting)
- [Miscellaneous tests](#miscellaneous-tests)

## Task Checklist

### App Recon and analysis

- [ ] Map visible content
- [ ] Discover hidden & default content
- [ ] Test for debug parameters
- [ ] Identify data entry points
- [ ] Identify the technologies used
- [ ] Research existing vulnerabilitties in technology
- [ ] Gather wordlists for specific techniology (Assetnote ones are excellent)
- [ ] Map the attack surface (Spider)
- [ ] Identify all javascript files for later analysis

## Test handling of access
### Authentication
- [ ] Test password quality rules
- [ ] Test for username enumeration
- [ ] Test resilience to password guessing
- [ ] Test any account recovery function
- [ ] Test any "remember me" function
- [ ] Test any impersonation function
- [ ] Test username uniqueness
- [ ] Check for unsafe distribution of credentials
- [ ] Test for fail-open conditions
- [ ] Test any multi-stage mechanisms
### Session handling
- [ ] Test tokens for meaning
- [ ] Test tokens for predictability
- [ ] Check for insecure transmission of tokens
- [ ] Check for disclosure of tokens in logs
- [ ] Check mapping of tokens to sessions
- [ ] Check session termination
- [ ] Check for session fixation
- [ ] Check for cross-site request forgery
- [ ] Check cookie scope
### Access controls
- [ ] Understand the access control requirements
- [ ] Test effectiveness of controls, using multiple accounts if possible
- [ ] Test for insecure access control methods (request parameters, Referer header, etc)

## Test handling of input

- [ ] Fuzz all request parameters
- [ ] Test for SQL injection
- [ ] Identify all reflected data
- [ ] Test for reflected XSS
- [ ] Test for HTTP header injection
- [ ] Test for arbitrary redirection
- [ ] Test for stored attacks
- [ ] Test for OS command injection
- [ ] Test for path traversal
- [ ] Test for script injection
- [ ] Test for file inclusion
- [ ] Test for SMTP injection
- [ ] Test for native software flaws (buffer overflow, integer bugs, format strings)
- [ ] Test for SOAP injection
- [ ] Test for LDAP injection
- [ ] Test for XPath injection
- [ ] Test for SSRF and HTTP Redirrects in all redirecting parameters

## Test application logic

- [ ] Identify the logic attack surface
- [ ] Test transmission of data via the client
- [ ] Test for reliance on client-side input validation
- [ ] Test any thick-client components (Java, ActiveX, Flash)
- [ ] Test multi-stage processes for logic flaws
- [ ] Test handling of incomplete input
- [ ] Test trust boundaries
- [ ] Test transaction logic

## Assess application hosting

- [ ] Test segregation in shared infrastructures
- [ ] Test segregation between ASP-hosted applications
- [ ] Test for web server vulnerabilities
- [ ] Default credentials
- [ ] Default content
- [ ] Dangerous HTTP methods
- [ ] Proxy functionality
- [ ] Virtual hosting mis-configuration
- [ ] Bugs in web server software

## Miscellaneous tests

- [ ] Check for DOM-based attacks
- [ ] Check for frame injection
- [ ] Check for local privacy vulnerabilities
- [ ] Persistent cookies
- [ ] Caching
- [ ] Sensitive data in URL parameters
- [ ] Forms with autocomplete enabled
- [ ] Follow up any information leakage
- [ ] Check for weak SSL ciphers
