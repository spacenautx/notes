# redteam

### kill chain
[Lockheed Martin Cyber Kill Chain](https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html)
[Unified Kill Chain](https://unifiedkillchain.com/)
[Varonis Cyber Kill Chain](https://www.varonis.com/blog/cyber-kill-chain/)
[Active Directory Attack Cycle](https://github.com/infosecn1nja/AD-Attack-Defense)
[MITRE ATT&CK Framework](https://attack.mitre.org/)

### Components of the kill chain.

| Technique             | Purpose                                                                           | Examples                                         |
| :----                 | :-----                                                                            | :-----                                           |
| Reconnaissance        | Obtain information on the                                                         | target	Harvesting emails, OSINT               |
| Weaponization         | Combine the objective with an exploit. Commonly results in a deliverable payload. | Exploit with backdoor, malicious office document |
| Delivery              | How will the weaponized function be delivered to the target                       | Email, web, USB                                  |
| Exploitation          | Exploit the target's system to execute code                                       | MS17-010, Zero-Logon, etc.                       |
| Installation          | Install malware or other tooling                                                  | Mimikatz, Rubeus, etc.                           |
| Command & Control     | Control the compromised asset from a remote central controller                    | Empire, Cobalt Strike, etc.                      |
| Actions on Objectives | Any end objectives: ransomware, data exfiltration, etc.                           | Conti, LockBit2.0, etc.                          |

### resources
- https://github.com/A-poc/RedTeam-Tools
- https://github.com/Ondrik8/RED-Team
- https://github.com/netbiosX/Checklists
- https://github.com/RoseSecurity/Red-Teaming-TTPs
- https://github.com/morph3/Windows-Red-Team-Cheat-Sheet
- https://github.com/RistBS/Awesome-RedTeam-Cheatsheet
- https://github.com/bigb0sss/RedTeam-OffensiveSecurity
- https://github.com/ihebski/A-Red-Teamer-diaries
- https://github.com/tarahmarie/investigations
- https://redteam.guide/docs/definitions/
- https://github.com/H4CK3RT3CH/RedTeaming_CheatSheet
- https://github.com/mthcht/ThreatHunting-Keywords
- https://github.com/jatrost/awesome-detection-rules
- https://riccardoancarani.github.io
- https://her0ness.github.io/2023-08-03-c2-Attacking-an-EDR-Part-1/
- https://github.com/assume-breach/Home-Grown-Red-Team

### tools
- https://github.com/cisagov/RedEye/

### courses
- https://gist.github.com/soheilsec/8310eea7913de6457f0dd89614fd843c
