# r2


### run

```bash
r2 -e dbg.profile=s0.rr2 -i init.r2 -d ./bin
```

### init.r2

```bash
aaaa
ood
dcu main
v 4k
V!
```

### s0.rr2

```bash
#!/usr/bin/rarun2
program=bin
input=!cat input.txt
stdout=/dev/pts/0
```

### rabin2

```bash
rabin2 -I binary
```

### panes

```bash
pxq 256@r:SP
drr
xc
```

### ragg2

```bash
ragg2 -rP 100
ragg2 -q 0x41415441
```

### r2pipe

```python
import r2pipe
r = r2pipe.open('binary')
r.cmd('doo; db main; dc')
print(r2.cmd("afl"))
```

### rarun2

```bash
#!/usr/bin/rarun2
program=bin
input=input.txt
stdout=/dev/pts/0
# stdout=foo.txt
# stderr=foo.txt
# stdin=input.txt # or !program to redirect input from another program
# stdio=blah.txt
# arg1=/bin
# arg2=hello
# arg3="hello\nworld"
# arg4=:048490184058104849
# arg5=:!ragg2 -p n50 -d 10:0x8048123
# arg6=@arg.txt
# arg7=@300@ABCD # 300 chars filled with ABCD pattern
# system=r2 -
# aslr=no
# setenv=FOO=BAR
# unsetenv=FOO
# clearenv=true
# envfile=environ.txt
# timeout=3
# timeoutsig=SIGTERM # or 15
# connect=localhost:8080
# listen=8080
# pty=false
# fork=true
# bits=32
# pid=0
# pidfile=/tmp/foo.pid
# sleep=0
# maxfd=0
# execve=false
# maxproc=0
# maxstack=0
# core=false
# chdir=/
# chroot=/mnt/chroot
# libpath=$PWD:/tmp/lib
# r2preload=yes
# preload=/lib/libfoo.so
# setuid=2000
# seteuid=2000
# setgid=2001
# setegid=2001
# nice=5
```

### heap
- https://gist.github.com/Zer1t0/9a1d6108148e862dd61065ec8ae0c03c

### resources
- https://ktln2.org/2017/08/31/rhme3-exploitation-writeup/
- https://reverseengineering.stackexchange.com/questions/16755/what-is-radare2s-equivalent-to-gdbs-find-system-9999999-bin-sh/16760#16760
- http://r2wiki.readthedocs.io/en/latest/home/themes/
- http://www.aperikube.fr/docs/xiomara_re4/#tl-dr
- https://www.u235.io/single-post/2017/07/23/Simplistic-Binary-Patching-With-Radare2
- https://sgros-students.blogspot.in/2015/08/reverse-engineering-with-radare2.html
- https://www.real0day.com/resources/
- https://medium.com/@trufae/reply-to-the-peda-vs-r2-blogpost-5d55d5ef875c
- https://github.com/radare/radare2/blob/master/doc/intro.md
- https://quizlet.com/182492323/radare2-debugger-complete-cheat-sheet-flash-cards/
- https://reverser.ninja/2015/23/radare2-cheat-sheet.html
- http://www.aboureada.com/cheat_sheet/2017/12/20/radare2_cheat_sheet.html
- https://youremindmeofmymother.com/tag/radare2/
- https://github.com/radareorg/r2con/blob/7ae29910bbcfe2e21ac68fe5946f5ce32f3a9913/2016/trainings/03-linux-xpl/r2con-Linux_Exploiting_final.pdf
- https://github.com/radareorg/r2con/tree/7ae29910bbcfe2e21ac68fe5946f5ce32f3a9913/2016/trainings/03-linux-xpl/exercises
- https://monosource.github.io/2016/10/radare2-peda
- https://blankhat.blogspot.com/2018/
- http://radare.today/posts/interview-of-ret2libc/
- http://hacktracking.blogspot.in/search/label/r2
- https://github.com/dukebarman/awesome-radare2
- https://blog.devit.co/unpacking-with-r2pipe/
- http://blog.devit.co/diving-into-radare2/
- http://curvetube.com/Defusing_a_binary_bomb_with_radare2-phase_5/5KwddWml2tM.video
- http://radare.today/posts/defeating-baby_rop-with-radare2/
- http://r2wiki.readthedocs.io/en/latest/home/ctf-solving-using-radare2/
- https://github.com/radareorg/blog/tree/master/content/posts
- http://radare.today/posts/adventures-with-radare2-1-a-simple-shellcode-analysis/
- https://www.megabeets.net/a-journey-into-radare-2-part-2/
- https://www.archcloudlabs.com/projects/loadlibrary-analysis/
