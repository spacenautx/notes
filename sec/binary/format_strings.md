#format string

### hints

1) look for scanf
2) inputs - %d %u %x %s %n
3) find user controlled address offset
4) padding
5) overwrite rip
6) bypass size restirctions - short write - %hn
7) stackpop
8) direct parameter access
9) bruteforce
10) got overwrite
11) rop
12) nullbytes
13) 64b & 32b

### resources
- https://samsclass.info/127/proj/p6-fs.htm
- https://julianor.tripod.com/bc/NN-formats.txt
- https://github.com/arthaud/formatstring
- https://github.com/firmianay/Life-long-Learner/blob/master/SEED-labs/format_string-vulnerability-lab.md
- https://github.com/r0hi7/BinExp/blob/master/Lecture6/README.md
- http://blog.coffinsec.com/nday/2022/08/04/CVE-2022-1215-libinput-fmt-canary-leak.html

