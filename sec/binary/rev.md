### reversing methods
*elf, exe, unicorn*

*hints*
1) patch - anti debugging
2) unpackers

### resources
- https://margin.re/2021/11/an-opinionated-guide-on-how-to-reverse-engineer-software-part-1/
- https://github.com/romainthomas/reverse-engineering-workshop
- https://github.com/rosehgal/BinExp
- https://github.com/Captainarash/The_Holy_Book_of_X86

tools
-----
- frida
- [[http://web.archive.org/web/20140910051410/http://www.dirac.org/linux/gdb/][gdb]]
