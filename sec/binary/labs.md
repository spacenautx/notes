# binary labs

- https://exploit-pack.gitbook.io/exploit-pack-documentation/exploit-training
- https://exploit.courses/#/challenges
- https://exploit.courses/files/bfh2018/content.html
- https://github.com/Billy-Ellis/Exploit-Challenges
- https://github.com/hardenedlinux/linux-exploit-development-tutorial
- https://github.com/mytechnotalent/Reverse-Engineering
- https://github.com/nnamon/linux-exploitation-course
- https://github.com/tharina/BlackHoodie-2018-Workshop
- https://github.com/xairy/easy-linux-pwn
- https://legend.octopuslabs.io/sample-page.html
- https://linxz.tech/post/hevd/2022-05-14-hevd3-stackbufferoverflow/
- https://prezi.com/a5tm-lf0879-/reverse-engineering-101-nyupoly-2010/
- https://ropemporium.com/
- https://exploit.courses/#/challenges
