# gdbinit

```bash
set pagination off
set logging file gdb.txt
set logging on
file a.out
b f
commands
bt
continue
end
info breakpoints
r
set logging off
quit
```

# test.gdb

```shell
gdb --command=test.gdb ./bin
```


### resources
- https://apoorvaj.io/hitchhikers-guide-to-the-gdb/
- https://sourceware.org/gdb/onlinedocs/gdb/index.html#SEC_Contents
- https://blog.csdn.net/lla520/article/details/77776809
- https://www.skullsecurity.org/2023/gdb-tricks--tricking-the-application-into-generating-test-data
