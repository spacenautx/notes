### malloc


### free


### principles
-   Heap helps make memory usage more efficient, but to use it effectively and safely, users need to adhere to the following

```text
+----+------------------------------------------------------------------------------+
| 1) | Do not read or write to a memory chunk already freed. Otherwise, it will     |
|    | lead to use after free errors .                                              |
+----+------------------------------------------------------------------------------+
| 2) | Do not use or leak uninitialized information in a chunk. Otherwise it will   |
|    | lead to information leaks or uninitialized data errors .                     |
+----+------------------------------------------------------------------------------+
| 3) | Do not read or write to the bytes behind a chunk. Otherwise, heap overflow   |
|    | or read beyond bounds will result .                                          |
+----+------------------------------------------------------------------------------+
| 4) | Not free()a chunk 2 times. Otherwise it will lead to a double free error .   |
+----+------------------------------------------------------------------------------+
| 5) | Do not read or write to the bytes preceding a chunk. Otherwise, it will lead |
|    | to heap underflow errors .                                                   |
+----+------------------------------------------------------------------------------+
| 6) | No free()pointers have not been initialized by the function yet malloc().    |
|    | Otherwise it will lead to an invalid free error .                            |
+----+------------------------------------------------------------------------------+
| 7) | Do not use a pointer initialized by the function malloc()if the return value |
|    | of malloc is NULL. Otherwise it will lead to arbitrary write errors .        |
+----+------------------------------------------------------------------------------+
```

### structure

-   A chunk has 2 states: allocated and freed

```text
+--------------------+----------------------------------------------+
| abbr               | details                                      |
+--------------------+----------------------------------------------+
| prev_size          | If the previous chunk is free, this field    |
|                    | contains the size of previous chunk. Else if |
|                    | previous chunk is allocated, this field      |
|                    | contains previous chunk’s user data.         |
+--------------------+----------------------------------------------+
| size               | This field contains the size of this         |
|                    | allocated chunk. Last 3 bits of this field   |
|                    | contains flag information.                   |
+--------------------+----------------------------------------------+
| fd forward pointer | Points to next chunk in the same bin (and NOT|
|                    |to the next chunk present in physical memory).|
+--------------------+----------------------------------------------+
| bk back pointer    |Points to previous chunk in the same bin (and |
|                    |NOT to the previous chunk present in physical |
|                    |                   memory).                   |
+--------------------+----------------------------------------------+
| IS_MMAPPED (M)     | The chunk is obtained through mmap. The other|
|                    | two bits are ignored. mmapped chunks are     |
|                    | neither in an arena, not adjacent to a       |
|                    | free chunk.                                  |
+--------------------+----------------------------------------------+
| NON_MAIN_ARENA (A) | 0 for chunks in the main arena. Each thread  |
|                    | spawned receives its own arena and for those |
|                    | chunks, this bit is set.                     |
+--------------------+----------------------------------------------+
| PREV_INUSE (P)     | 0 when previous chunk (not the previous chunk|
|                    | in the linked list, but the one directly     |
|                    | before it in memory) is free (and hence the  |
|                    | size of previous chunk is stored in the      |
|                    | first field). The very first chunk allocated |
|                    | has this bit set. If it is 1, then we cannot |
|                    | determine the size of the previous chunk.    |
+--------------------+----------------------------------------------+
| prev_size          | No two free chunks can be adjacent together. |
|                    |When both the chunks are free, its gets       |
|                    |combined into one single free chunk. Hence    |
|                    | always previous chunk to this freed chunk    |
|                    |would be allocated and therefore prev_size    |
|                    | contains previous chunk’s user data.         |
+--------------------+----------------------------------------------+
| size               | This field contains the size of this free    |
|                    |chunk.                                        |
+--------------------+----------------------------------------------+
```


### allocated chunk

The metadata for one allocated chunkis 8 bytes (for 32-bit systems) or 16 bytes (for 64-bit systems)

```text

    chunk -> +---------------------------------------------------------+
             | prev_size: size of the previous chunk, in bytes (used   |
             | by dlmalloc only if this previous chunk is free)        |
             |---------------------------------------------------------|
             | size: size of the chunk (the number of bytes between    |
             | "chunk" and "nextchunk") and 2 bits status information  |
      mem -> |---------------------------------------------------------|
             | fd: not used by dlmalloc because "chunk" is allocated   |
             | (user data therefore starts here)                       |
             |---------------------------------------------------------|
             | bk: not used by dlmalloc because "chunk" is allocated   |
             | (there may be user data here)                           |
             |---------------------------------------------------------|
             |                                                         |
             |                                                         |
             | user data (may be 0 bytes long)                         |
             |                                                         |
             |                                                         |
nextchunk -> |---------------------------------------------------------|
             | prev_size: not used by dlmalloc because "chunk" is      |
             | allocated (may hold user data, to decrease wastage)     |
             +---------------------------------------------------------+

    chunk-> +---------------------------------------------------------------+
            |Size of previous chunk, if unallocated (P clear)               |
            +---------------------------------------------------------------+
            |Size of chunk, in bytes                                  |A|M|P|
      mem-> +---------------------------------------------------------------+
            |User data starts here...                                       |
            |                                                               |
            |(malloc_usable_size() bytes)                                   |
            |                                                               |
nextchunk-> +---------------------------------------------------------------+
            |(size of chunk, but used for application data)                 |
            +---------------------------------------------------------------+
            |Size of next chunk, in bytes                             |A|0|1|
            +---------------------------------------------------------------+

```


### freed chunk

```text
Free chunks are stored in circular doubly-linked lists

    chunk -> +---------------------------------------------------------+
             | prev_size: may hold user data (indeed, since "chunk" is |
             | free, the previous chunk is necessarily allocated)      |
             |---------------------------------------------------------|
             | size: size of the chunk (the number of bytes between    |
             | "chunk" and "nextchunk") and 2 bits status information  |
             |---------------------------------------------------------|
             | fd: forward pointer to the next chunk in the circular   |
             | doubly-linked list (not to the next _physical_ chunk)   |
             |---------------------------------------------------------|
             | bk: back pointer to the previous chunk in the circular  |
             | doubly-linked list (not the previous _physical_ chunk)  |
             |---------------------------------------------------------|
             |                                                         |
             |                                                         |
             | unused space (may be 0 bytes long)                      |
             |                                                         |
             |                                                         |
nextchunk -> |---------------------------------------------------------|
             | prev_size: size of "chunk", in bytes (used by dlmalloc  |
             | because this previous chunk is free)                    |
             +---------------------------------------------------------+

    chunk-> +---------------------------------------------------------------+
            |Size of previous chunk, if unallocated (P clear)               |
            |---------------------------------------------------------------|
       head |Size of chunk, in bytes                                  |A|0|P|
      mem-> |---------------------------------------------------------------|
            |Forward pointer to next chunk in list                          |
            |---------------------------------------------------------------|
            |Back pointer to previous chunk in list                         |
            |---------------------------------------------------------------|
            |                                                               |
            |Unused space (may be 0 bytes long)                             |
            |                                                               |
nextchunk-> |---------------------------------------------------------------|
     foot   |Size of chunk, in bytes                                        |
            |---------------------------------------------------------------|
            |Size of next chunk, in bytes                             |A|0|0|
            +---------------------------------------------------------------+

```


### history

```text
+------+-----------------------------------------------+
| year | method                                        |
+------+-----------------------------------------------+
| 2001 | Once upon a free()                            |
+------+-----------------------------------------------+
| 2003 | Advanced Doug lea’s malloc exploits           |
+------+-----------------------------------------------+
| 2004 | Exploiting the wilderness                     |
+------+-----------------------------------------------+
| 2007 | The use of set_head to defeat the wilderness  |
+------+-----------------------------------------------+
| 2007 | Understanding the heap by breaking it         |
+------+-----------------------------------------------+
| 2009 | Yet another free() exploitation technique     |
+------+-----------------------------------------------+
| 2009 | Malloc Des-Maleficarum                        |
+------+-----------------------------------------------+
| 2010 | The house of lore: Reloaded                   |
+------+-----------------------------------------------+
| 2014 | The poisoned NUL byte, 2014 edition           |
+------+-----------------------------------------------+
| 2015 | Glibc adventures: The forgotten chunk         |
+------+-----------------------------------------------+
| 2016 | Ptmalloc fanzine                              |
+------+-----------------------------------------------+
| 2016 | New exploit methods against Ptmalloc of Glibc |
+------+-----------------------------------------------+
| 2016 | House of Einherjar                            |
+------+-----------------------------------------------+
| 2018 | ARCHEAP                                       |
+------+-----------------------------------------------+
```


### how2heap

```text
+---------------------------+----------------------------------------+
| first_fit.c               |Demonstrating glibc malloc's first-fit  |
|                           |behavior.                               |
+---------------------------+----------------------------------------+
| fastbin_dup.c             |Tricking malloc into returning an       |
|                           |already-allocated heap pointer by       |
|                           |abusing the fastbin freelist.           |
+---------------------------+----------------------------------------+
| fastbin_dup_into_stack.c  |Tricking malloc into returning a        |
|                           |nearly-arbitrary pointer by abusing the |
|                           |fastbin freelist.                       |
+---------------------------+----------------------------------------+
| fastbin_dup_consolidate.c |Tricking malloc into returning an       |
|                           |already-allocated heap pointer by       |
|                           |putting a pointer on both - fastbin     |
|                           |freelist and unsorted bin freelist      |
+---------------------------+----------------------------------------+
| unsafe_unlink.c           |Exploiting free on a corrupted chunk to |
|                           |get arbitrary write.                    |
+---------------------------+----------------------------------------+
| house_of_spirit.c         |Frees a fake fastbin chunk to get       |
|                           |malloc to return a nearly-arbitrary     |
|                           |pointer.                                |
+---------------------------+----------------------------------------+
| poison_null_byte.c        |Exploiting a single null byte overflow. |
+---------------------------+----------------------------------------+
| house_of_lore.c           |Tricking malloc into returning a        |
|                           |nearly-arbitrary pointer by abusing the |
|                           |smallbin freelist.                      |
+---------------------------+----------------------------------------+
| overlapping_chunks.c      |Exploit the overwrite of a freed chunk  |
|                           |size in the unsorted bin in order to    |
|                           |make a new allocation - overlap with an |
|                           |existing chunk                          |
+---------------------------+----------------------------------------+
| overlapping_chunks_2.c    |Exploit the overwrite of an in use      |
|                           |chunk size in order to make a new       |
|                           |allocation overlap with an - existing   |
|                           |chunk                                   |
+---------------------------+----------------------------------------+
| house_of_force.c          |Exploiting the Top Chunk (Wilderness)   |
|                           |header in order to get malloc to return |
|                           |a nearly-arbitrary - pointer            |
+---------------------------+----------------------------------------+
| unsorted_bin_into_stack.c |Exploiting the overwrite of a freed     |
|                           |chunk on unsorted bin freelist to return|
|                           |a nearly-arbitrary - pointer.           |
+---------------------------+----------------------------------------+
| unsorted_bin_attack.c     |Exploiting the overwrite of a freed     |
|                           |chunk on unsorted bin freelist to write |
|                           |a large value into - arbitrary address  |
+---------------------------+----------------------------------------+
| large_bin_attack.c        |Exploiting the overwrite of a freed     |
|                           |chunk on large bin freelist to write a  |
|                           |large value into - arbitrary address    |
+---------------------------+----------------------------------------+
| house_of_einherjar.c      |Exploiting a single null byte overflow  |
|                           |to trick malloc into returning a        |
|                           |controlled pointer                      |
+---------------------------+----------------------------------------+
| house_of_orange.c         |Exploiting the Top Chunk (Wilderness)   |
|                           |in order to gain arbitrary code         |
|                           |execution                               |
+---------------------------+----------------------------------------+
| tcache_dup.c              |Tricking malloc into returning an       |
|                           |already-allocated heap pointer by       |
|                           |abusing the tcache freelist.            |
+---------------------------+----------------------------------------+
| tcache_poisoning.c        |Tricking malloc into returning a        |
|                           |completely arbitrary pointer by abusing |
|                           |the tcache freelist.                    |
+---------------------------+----------------------------------------+
| tcache_house_of_spirit.c  |Frees a fake chunk to get malloc to     |
|                           |return a nearly-arbitrary pointer.      |
+---------------------------+----------------------------------------+
```

### xploit table

```text
+---------------+----------------+--------------------------------------------------+------------------------+--------------------------+
|               |   Access area  |                   overwrite area                 |          free          |        fake chunk        |
|    EXPLOIT    +----------------+--------------------------------------------------+------------------------+--------------------------+
|               | stack |  heap  |  top  | F - size | F - bk |    A -    | A - size | Double  |     free     | stack area |  heap area  |
|               |       |        | chunk |          |        | prev_size |          |   free  | (stack area) |            |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
|   first       |       |    0   |       |          |        |           |          |         |              |            |             |
|    dup        |       |        |       |          |        |           |          |         |              |            |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
|  fastbin      |       |    0   |       |          |        |           |          |    0    |              |            |             |
|     dup       |       |        |       |          |        |           |          |         |              |            |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
|  fast bin dup |   0   |        |       |          |        |     0     |          |    0    |              |      0     |             |
|   into stack  |       |        |       |          |        |           |          |         |              |   (size)   |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
|  poison null  |       |    0   |       |    0     |        |           |          |         |              |            |      0      |
|      byte     |       |        |       | (1 byte) |        |           |          |         |              |            | (prev_size) |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
|  overlapping  |       |    0   |       |     0    |        |           |          |         |              |            |             |
|     chunks    |       |        |       |          |        |           |          |         |              |            |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
|  overlapping  |       |    0   |       |     0    |        |           |          |         |              |            |             |
|    chunks 2   |       |        |       |          |        |           |          |         |              |            |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
|   House of    |   0   |        |       |          |        |     0     |     0    |         |              |   0 (free  |             |
|   einherjar   |       |        |       |          |        |           |          |         |              |    chunk)  |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
| The House of  |   0   |        |   0   |          |        |           |          |         |              |            |             |
|     Force     |       |        |       |          |        |           |          |         |              |            |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
| The House of  |   0   |        |       |          |    0   |           |          |         |              |   0 (free  |             |
|      Lore     |       |        |       |          |        |           |          |         |              |    chunk)  |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
| The House of  |   0   |        |       |          |        |           |          |         |       0      |  O (size,  |             |
|     Spirit    |       |        |       |          |        |           |          |         |              |  size of   |             |
|               |       |        |       |          |        |           |          |         |              | top chunk) |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
| Unsorted bin  |   0   |        |       |          |    0   |           |          |         |              |            |             |
|     attack    |       |        |       |          |        |           |          |         |              |            |             |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
|    Unsafe     |   0   |        |       |          |        |     0     |     0    |         |              |            |   0 (Free   |
|     unlink    |       |        |       |          |        |           |          |         |              |            |    chunk)   |
+---------------+-------+--------+-------+----------+--------+-----------+----------+---------+--------------+------------+-------------+
```



### tutorials

- https://sensepost.com/blog/2017/painless-intro-to-the-linux-userland-heap/
- https://danluu.com/malloc-tutorial/
- https://github.com/r0hi7/BinExp/blob/master/Lecture7/README.md
- https://maxwelldulin.com/BlogPost?post=6295828480
- https://blog.frizn.fr/glibc/glibc-heap-to-rip
- https://googleprojectzero.blogspot.com/2014/08/the-poisoned-nul-byte-2014-edition.html


### resources

* kernal exploitation
- https://0x434b.dev/dabbling-with-linux-kernel-exploitation-ctf-challenges-to-learn-the-ropes/
* tutorials
- https://sensepost.com/blog/2017/painless-intro-to-the-linux-userland-heap/
- https://labs.cognisys.group/posts/Advanced-Module-Stomping-and-Heap-Stack-Encryption/
- https://danluu.com/malloc-tutorial/
- https://github.com/r0hi7/BinExp/blob/master/Lecture7/README.md
- https://github.com/SoftwareSecurityLab/Heap-Overflow-Detection
- https://infosecwriteups.com/the-toddlers-introduction-to-heap-exploitation-house-of-spirit-part-4-4-252cd8928f84


| *resources*                                                                                                    |
|:---------------------------------------------------------------------------------------------------------------|
| ftp://g.oswego.edu/pub/misc/malloc.c                                                                           |
| http://4ngelboy.blogspot.com/2016/10/hitcon-ctf-qual-2016-house-of-orange.html                                 |
| http://blog.angelboy.tw/                                                                                       |
| http://blog.infosectcbr.com.au/2019/08/linux-heap-fast-bin-double-free.html                                    |
| http://blog.infosectcbr.com.au/2019/08/linux-heap-house-of-force-exploitation.html                             |
| http://btv.melezinek.cz/binary-heap.html                                                                       |
| http://g.oswego.edu/dl/html/malloc.html                                                                        |
| http://gee.cs.oswego.edu/dl/html/malloc.html                                                                   |
| http://grantcurell.com/2015/08/16/protostar-exploit-challenges-heap3-solution-exploiting-dlmalloc/             |
| http://hackability.kr/entry/                                                                                   |
| http://hacktracking.blogspot.in/2015/03/protostar-heap.html                                                    |
| http://inaz2.hatenablog.com/entry/2015/03/02/014252                                                            |
| http://lcamtuf.coredump.cx/afl/                                                                                |
| http://llvm.org/docs/LibFuzzer.html                                                                            |
| http://pages.cs.wisc.edu/~danb/google-perftools-0.98/heap_checker.html                                         |
| http://phrack.org/issues/57/8.html                                                                             |
| http://phrack.org/issues/57/9.html                                                                             |
| http://phrack.org/issues/66/10.html                                                                            |
| http://phrack.org/issues/66/6.html#article                                                                     |
| http://phrack.org/papers/cyber_grand_shellphish.html                                                           |
| http://resources.infosecinstitute.com/exploiting-protostar-heap-levels-0-2/#gref                               |
| http://studyfoss.egloos.com/5206220                                                                            |
| http://thisisdearmo.blogspot.in/2012/06/protostar-heap-3-solution.html                                         |
| http://tukan.farm/                                                                                             |
| http://tukan.farm/2016/09/04/fastbin-fever/                                                                    |
| http://tuxdiary.com/2016/02/09/how2heap/                                                                       |
| http://uaf.io/                                                                                                 |
| http://uaf.io/exploitation/2017/03/19/0ctf-Quals-2017-BabyHeap2017.html                                        |
| http://uaf.io/exploitation/2017/09/03/TokyoWesterns-2017-Parrot.html                                           |
| http://www.fuzzysecurity.com/tutorials/expDev/11.html                                                          |
| http://www.fuzzysecurity.com/tutorials/expDev/8.html                                                           |
| http://www.h-online.com/security/features/A-Heap-of-Risk-747161.html                                           |
| http://www.hackdig.com/?03/hack-2239.htm                                                                       |
| http://www.mathyvanhoef.com/2013/02/understanding-heap-exploiting-heap.html                                    |
| http://www.phrack.org/issues/68/10.html                                                                        |
| http://www.vuln.cn/6172                                                                                        |
| https://0x00sec.org/t/heap-exploitation-abusing-use-after-free/3580                                            |
| https://0x00sec.org/t/heap-exploitation-fastbin-attack/3627                                                    |
| https://0xabe.io/ctf/exploit/2016/05/02/GoogleCTF-forced-puns.html                                             |
| https://0xabe.io/ctf/exploit/2016/05/23/DEFCONCTF-heapfun4u.html                                               |
| https://amritabi0s.wordpress.com/2018/03/12/n1ctf-2018-vote-writeup/                                           |
| https://amritabi0s.wordpress.com/category/pwn/                                                                 |
| https://animal0day.blogspot.com/2017/10/hacklu-heapheaven-write-up-with-radare2.html                           |
| https://animal0day.blogspot.in/2017/10/hacklu-heapheaven-write-up-with-radare2.html                            |
| https://binarystud.io/googlectf-2017-inst-prof-152-final-value.html                                            |
| https://blackndoor.fr/protostar-heap3/                                                                         |
| https://blog.csdn.net/conansonic/article/details/50121589                                                      |
| https://blog.csdn.net/gfgdsg/article/details/42709943                                                          |
| https://blog.csdn.net/iextract/article/details/78218055                                                        |
| https://blog.csdn.net/krrrr/article/details/5557391                                                            |
| https://blog.csdn.net/maokelong95/article/details/52006379                                                     |
| https://blog.csdn.net/sinat_38245860/article/details/80694754                                                  |
| https://blog.csdn.net/whatday/article/details/82937388                                                         |
| https://blog.csdn.net/wizardforcel/article/details/71082760                                                    |
| https://blog.csdn.net/wsclinux/article/details/48374539                                                        |
| https://blog.csdn.net/xiaobai_aaa/article/details/78552972                                                     |
| https://blog.csdn.net/zdy0_2004/article/details/51485198                                                       |
| https://code.woboq.org/userspace/glibc/malloc/arena.c.html                                                     |
| https://comydream.github.io/2019/05/26/heap-first-step/                                                        |
| https://comydream.github.io/timeline/                                                                          |
| https://conceptofproof.wordpress.com/2013/11/19/protostar-heap3-walkthrough/                                   |
| https://ctf.acebear.site/challenges                                                                            |
| https://cysecguide.blogspot.in/2017/10/memory-structurestack-heap-bss-data-code.html                           |
| https://dangokyo.me/2017/12/05/introduction-on-ptmalloc-part1/                                                 |
| https://dangokyo.me/2018/01/01/heap-exploitation-unsafe-unlink-fastbin-corruption/                             |
| https://demonteam.org/2017/10/06/reversing-engineering-resources/                                              |
| https://demonteam.org/2017/10/10/rhme3-exploitation-pwnable/                                                   |
| https://dl.packetstormsecurity.net/papers/attack/MallocMaleficarum.txt                                         |
| https://dustri.org/b/exploiting-zengarden-boston-key-party-2014-pwn300-with-radare2.html                       |
| https://gbmaster.wordpress.com/                                                                                |
| https://gbmaster.wordpress.com/2014/08/11/x86-exploitation-101-heap-overflows-unlink-me-would-you-please/      |
| https://gbmaster.wordpress.com/2014/08/24/x86-exploitation-101-this-is-the-first-witchy-house/                 |
| https://github.com/0x01f/main_arena_offset                                                                     |
| https://github.com/0x01f/pwn_repo                                                                              |
| https://github.com/DhavalKapil/heap-exploitation                                                               |
| https://github.com/TechSecCTF/pwn_challs                                                                       |
| https://github.com/cloudburst/libheap/blob/master/README.md                                                    |
| https://github.com/danigargu/heap-viewer                                                                       |
| https://github.com/f0110vv3r/MyStudy/tree/master/Note                                                          |
| https://github.com/firmianay/Binary-Reading-List                                                               |
| https://github.com/google/fuzzer-test-suite/blob/master/tutorial/libFuzzerTutorial.md                          |
| https://github.com/google/fuzzer-test-suite/tree/master/woff2-2016-05-06                                       |
| https://github.com/google/oss-fuzz                                                                             |
| https://github.com/gperftools/gperftools                                                                       |
| https://github.com/roothuntervn/Heap-Exploitation/blob/master/Day%201%20-%20Introduction/README.md             |
| https://github.com/shellphish/how2heap                                                                         |
| https://github.com/stong/how-to-exploit-a-double-free                                                          |
| https://github.com/str8outtaheaps/heapwn                                                                       |
| https://github.com/toomanybananas/ctf_solutions/tree/master/defcon/2018/mario                                  |
| https://github.com/wapiflapi/exrs                                                                              |
| https://github.com/wapiflapi/villoc                                                                            |
| https://googleprojectzero.blogspot.com/2014/08/the-poisoned-nul-byte-2014-edition.html                         |
| https://heap-exploitation.dhavalkapil.com/introduction.html                                                    |
| https://paper.seebug.org/255/                                                                                  |
| https://radare.gitbooks.io/radare2book/debugger/heap.html                                                      |
| https://secinject.wordpress.com/2018/01/10/protostar-heap2/                                                    |
| https://secinject.wordpress.com/2018/01/18/protostar-heap3/                                                    |
| https://secwriteups.blogspot.in/2015/02/protostar-final-2.html                                                 |
| https://segmentfault.com/a/1190000005118060                                                                    |
| https://sensepost.com/blog/2017/painless-intro-to-the-linux-userland-heap/                                     |
| https://simonuvarov.com/protostar-heap1/                                                                       |
| https://sourceware.org/glibc/wiki/MallocInternals                                                              |
| https://sploitfun.wordpress.com/2015/02/10/understanding-glibc-malloc/                                         |
| https://sploitfun.wordpress.com/2015/02/26/heap-overflow-using-unlink/                                         |
| https://sploitfun.wordpress.com/2015/03/04/heap-overflow-using-malloc-maleficarum/                             |
| https://stfpeak.github.io/2017/03/17/freenote-record/                                                          |
| https://stfpeak.github.io/2017/04/19/peda-pwngdb-make-heap-clear/                                              |
| https://thesprawl.org/research/exploit-exercises-protostar-heap/#finalizing-the-exploit                        |
| https://vishnudevtj.github.io/notes/asis-quals-2018-message                                                    |
| https://visualgo.net/en/heap                                                                                   |
| https://web.archive.org/web/20060713194734/http://doc.bughunter.net/buffer-overflow/heap-corruption.html       |
| https://www.anquanke.com/post/id/84965                                                                         |
| https://www.cgsecurity.org/exploit/heaptut.txt                                                                 |
| https://www.coresecurity.com/blog/sapcar-heap-buffer-overflow-crash-exploit                                    |
| https://www.cs.usfca.edu/~galles/visualization/Heap.html                                                       |
| https://www.ctolib.com/shellphish-how2heap.html#articleHeader0                                                 |
| https://www.ctolib.com/shellphish-how2heap.html#articleHeader2                                                 |
| https://www.fwhibbit.es/introduccion-al-reversing-con-radare2-0x07-use-after-free                              |
| https://www.kitploit.com/2018/07/memoro-detailed-heap-profiler.html                                            |
| https://www.lazenca.net/display/TEC/02.Heap                                                                    |
| https://www.lazenca.net/display/TEC/Heap+Exploitation                                                          |
| https://www.thc.org/root/docs/exploit_writing/Exploiting%20the%20wilderness.txt                                |
| https://www.tuicool.com/articles/7vyQfm6                                                                       |
| https://www.tuicool.com/articles/A7n2ueq                                                                       |
| https://www.tuicool.com/articles/E3Ezu2u                                                                       |
| https://www.tuicool.com/kans/3818294661                                                                        |
| https://www.valinux.co.jp/technologylibrary/document/linux/malloc0001/                                         |
| https://www.win.tue.nl/~aeb/linux/hh/hh-11.html                                                                |
| https://yannayl.github.io/glibc_malloc_for_exploiters/                                                         |
| https://0x434b.dev/overview-of-glibc-heap-exploitation-techniques/                                             |
| https://hackliza.gal/en/posts/r2heap/                                                                          |
| https://medium.com/@kevin.massey1189/everything-in-its-right-place-20aacd17fe3f                                |
| https://www.foonathan.net/                                                                                     |
| https://github.com/stong/how-to-exploit-a-double-free                                                          |
| https://starlabs.sg/blog/2023/07-prctl-anon_vma_name-an-amusing-heap-spray/                                    |
| https://infosecwriteups.com/the-toddlers-introduction-to-heap-exploitation-part-1-515b3621e0e8                 |
|                                                                                                                |

----

```text
Everything should be made as simple as possible, but not any simpler.
-- Albert Einstein
```
