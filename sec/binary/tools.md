# tools


- https://github.com/KeenSecurityLab/BinAbsInspector
- https://blog.securitybreak.io/10-python-libraries-for-malware-analysis-and-reverse-engineering-622751e6ebd0

- https://github.com/angr/angrop
- https://github.com/shellphish/patcherex
- https://github.com/angr/rex
- https://github.com/shellphish/driller
- https://github.com/ainfosec/FISSURE
- https://twitter.com/0xor0ne/status/1634290583857512449
- https://twitter.com/2Ourc3/status/1629916820713947143

- https://github.com/elfmaster/maya
- https://www.kitploit.com/2022/12/ofrak-unpack-modify-and-repack-binaries.html
