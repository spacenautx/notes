# binary exploitation

- [[file:rev.org][rev]]
- [[file:pwn.org][pwn]]
- patch
- race condition
https://github.com/firmianay/Life-long-Learner/blob/master/SEED-labs/race-condition-vulnerability-lab.md
- crack
- [[file:radare.org][radare]]

* Analyse
- strings
- rabin2
- checksec
- objdump
- ltrace
- strace
- https://github.com/BroadbentT/BINARY-MASTER.git
- https://www.tecmint.com/strace-commands-for-troubleshooting-and-debugging-linux/

* Common dangerous functions
+ Input
+ Gets, reads a line directly, ignores '\x00'
+ Scanf
+ Vscanf
+ Output
+ Sprintf
+ String
+ strcpy, string copy, encounters '\x00'stop
+ strcat, string concatenation, encounters '\x00'stop
+ Bcopy
  
* resources
- [[file:ropemporium.org][ropemporium]]

- fuzz
- https://gamozolabs.github.io/fuzzing/2020/12/06/fuzzos.html
- https://sthbrx.github.io/blog/2021/03/04/fuzzing-grub-part-1/

- syscall
- https://brennan.io/2016/11/14/kernel-dev-ep3/
- https://tmpout.sh/2/
