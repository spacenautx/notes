# pwn

- stack

|----|--------------------|----------------------------------------------------------------------------------------------------------------------------------|
| 01 | local-overflow     | overflow buffer and overwrite x with the desired value.                                                                          |
| 02 | overwrite-ret      | overwrite any of the return addresses on stack with the address of not_called().                                                 |
| 03 | one-gadget         | jump to a "one gadget" address. Make sure to satisfy the required constaints if there are any.                                   |
| 04 | shellcode-static   | allocate a shellcode on the stack that launches /bin/sh and jump to it. Assume that the shellcode address on the stack is known. |
| 05 | -shellcode-dynamic | same as the previous task, but here the stack address (and therefore the shellcode address on the stack) is unknown.             |
| 06 | -system-rop        | compose a ROP chain to execute system("/bin/sh").                                                                                |
| 07 | -execve-rop        | compose a ROP chain to execute execve("/bin/sh", NULL, NULL) via a syscall. Explicitly specify the second and third arguments.   |
| 08 | -overwrite-global  | compose a ROP chain to overwrite x with the desired value and then jump to "not called()".                                       |



* canary
1) canary value ends with "\x00"
2) Canary is different after each process restart


* race condition
* sandbox

Python sandbox escaping is to bypass the simulated Python terminal and ultimately implement command execution.
*hints*
1) import xxx
2) from xxx import *
3) __import__('xxx')
4) execfile
5) timeit
6) import platform
7) ( __builtins__ )
8) eval('__import__("os").system("dir")')

- rbash
- fork
- integer overflow
- checklist
- steps to get rip
- rabin2
- checksec
- libc
- shellcode
- ropper
- ROPgadget
- one_gadget
- radare2
- angr
- frida


### aslr
https://github.com/google/security-research/tree/master/pocs/cpus/ret2aslr

### resources
- https://insecure.org/stf/smashstack.html
- https://avicoder.me/2016/02/01/smashsatck-revived/
- https://blog.techorganic.com/2018/02/23/dc416-introduction-to-64-bit-linux-exploit-development-vuln03-solution/
- https://github.com/NoviceLive/bintut
- https://github.com/hardenedlinux/grsecurity-101-tutorials
- https://github.com/invictus1306/Workshop-BSidesMunich2018
- https://github.com/r0hi7/BinExp
- https://github.com/saelo/armpwn
- https://www.blackhat.com/presentations/bh-usa-07/Ferguson/Whitepaper/bh-usa-07-ferguson-WP.pdf
- https://www.tenouk.com/cncplusplusbufferoverflow.html
- https://github.com/Naetw/CTF-pwn-tips
- https://fakenews.sh/blog/startingpwnt-rop-part-walkthrough/
- https://pwn.college/

------
- [binary analysis](https://www.matteomalvica.com/minutes/binary_analysis/)
