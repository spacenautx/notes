#+title: cryptography
#+HUGO_BASE_DIR: ~/club/
#+hugo_section: ctftime
#+startup: content

* types
- symmetric encryption
** asymmetric encryption

In an asymmetric password, the key used by the encryptor and the decrypter is not the same,
typically RSA encryption, backpack encryption, and elliptic curve encryption.
1) lattice
2) knapsack
	* backpack problem
	* merkle-hellman
3) discrete-log
4) rsa
	* theory
	* chosen cipher
	* complex
	* coppersmith
	* d attack
	* e attack
	* module attack
	* pkcs_attack
	* side_channel
- [[file:classical_cipher.org][classical cipher]]
- [[file:block_cipher.org][block cipher]]
- stream cipher
** fsr
** rc4
** prng
** lcg


Stream ciphers typically process information byte by byte or bit by bit. Generally speaking

	- The key length of the stream cipher will be the same as the length of the plaintext.
	- The stream cipher key is derived from a shorter key, and the derivation algorithm is
	  usually a pseudo-random number generation algorithm.

It should be noted that stream encryption is currently symmetric encryption.
The more random the sequence generated by the pseudo-random number generation algorithm, the
better the statistical features in the plaintext are covered.
Stream cipher encryption and decryption is very simple, and in the case of known plaintext,
the key stream can be obtained very easily.
The key to stream ciphers is the well-designed pseudo-random number generator. In general,
the basic building block of a pseudo-random number generator is a feedback shift register.
Of course, there are also some specially designed stream ciphers, such as RC4.

- certificate

#+BEGIN_SRC text

DER or CRT
- Certificates using this extension are binary coded , although these certificates can also use CER or CRT as extensions.

PEM
- Certificates that use this extension are encoded in Base64, and the beginning of the file is one line -----BEGIN.

Format conversion
 openssl x509 -outform der -in certificate.pem -out certificate.der
 openssl x509 -inform der -in certificate.cer -out certificate.pem

#+END_SRC

- hash
- signature
#+BEGIN_SRC text

Digital signatures rely on asymmetric cryptography because we have to make sure
that one party can do something while the other party cannot do something like this.

In a key-only attack, the attacker is only given the public verification key.
In a known message attack, the attacker is given valid signatures for a variety of messages
known by the attacker but not chosen by the attacker.
In an adaptive chosen message attack, the attacker first learns signatures on arbitrary messages of the attacker's choice.

A total break results in the recovery of the signing key.
A universal forgery attack results in the ability to forge signatures for any message.
A selective forgery attack results in a signature on a message of the adversary's choice.
An existential forgery merely results in some valid message/signature pair not already known to the adversary.
The strongest notion of security, therefore, is security against existential forgery under an adaptive chosen message attack.

#+END_SRC
- prng
- entropy
- barcode
- qrcode
https://www.thonky.com/qr-code-tutorial/introduction
-------
encoding
-------

* methods
- Frequency analysis
- Violent attack
- Intercommunication attack
- Linear analysis
- Differential analysis
- Impossible diff
- Integral analysis
- Algebraic analysis
- Related key attack
- Side channel attack
- Known-plaintext
- Chosen-plaintext
- Known ciphertext
- Chosen-ciphertext
- Chosen-key
- Brute force
- Attack on Two-Time Pad
- KRACK attack
- Frequency analysis
- Meet-in-the-middle
- Replay attack
- Homograph attack
- Coppersmith's attack
- Padding oracle attack
- Boomerang attack
- Wiener's attack
- Index of coincidence
- Integral cryptanalysis

* tools
- S-box
- rsatool
- cryptool
- hashpump
- sage
- jtr
- hashcat
- xortool
- fastcoll
- foresight
- featherduster
- galois
- hashkill
- hash-identifier
- pemcrack
- pkcrack
- python-paddingoracle
- reveng
- ssh_decoder
- sslsplit
- yafu
- rainbow table
- gnupg
- openssl
- hydra
- john
- hoB0Rules
---------
* resources
- https://github.com/ashutosh1206/Crypton (must learn)
-----
- https://github.com/mufeedvh/basecrack
- https://github.com/HashPals/Name-That-Hash
- https://github.com/Ciphey/Ciphey
- https://github.com/Deadlyelder/Tools-for-Cryptanalysis
- https://github.com/JohnHammond/katana
- https://github.com/mattnotmax/cyberchef-recipes
- https://github.com/securisec/chepy
------
- https://book.hacktricks.xyz/crypto/crypto-ctfs-tricks
- https://github.com/carlospolop/hacktricks/blob/master/crypto/crypto-ctfs-tricks.md
- https://gist.github.com/tqbf/be58d2d39690c3b366ad
- https://gist.github.com/paragonie-scott/e9319254c8ecbad4f227
- https://loup-vaillant.fr/articles/rolling-your-own-crypto
- https://simpleaswater.com/cryptography/
- https://qvault.io/2020/09/17/very-basic-intro-to-elliptic-curve-cryptography/
- https://soatok.blog/2020/11/27/the-subtle-hazards-of-real-world-cryptography/
- https://github.com/guidovranken/cryptofuzz
- https://www.cimt.org.uk/resources/codes/index.htm
- https://web.engr.oregonstate.edu/~rosulekm/crypto/
------
* [[file:cryptopals.org][cryptopals]]
