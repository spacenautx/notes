# stegnography

### tools
- file
- strings
- grep
- exiv2
- openpuff
- binwalk
- exiftools
- [volatility](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference)
- foremost
- dd
- binwalk
- bless
- xxd
- hxd
- dnscat
- registry dumper
- creddump
- usbforensics
- dislocker
- exetractor
- firmware-mod-kit
- pdf-parser
- peepdf
- scrdec
- testdisk
- scalpel
- TrID
- foremost
- DFF
- sleuth kit
- Computer aided investigative environment
-----

### images
- cloackedpixel
- cloackedpixel-analyze
- e2fsck
- f5
- ffmpeg
- giffy
- ImageMagick
- identify
- jphide/jpseek
- jsteg
- jphs
- lsbsteg
- LSB-Steganography
- openstego
- outguess
- pyexiftool
- pngcheck
- Outguess
- Pngtools
- SmartDeblur
- StegCracker
- Stegextract
- steganabara
- stegano
- stegbreak
- stegdetect
- steghide
- [stegseek](https://github.com/RickdeJager/stegseek)
- stegosuite
- stegsolve.jar
- StegSpy
- StegSnow
- Stego-Toolkit
- StegoVeritas
- xortool
- zsteg

### online stegano decoder
- https://futureboy.us/stegano/decinput.html
- http://stylesuxx.github.io/steganography/
- https://www.mobilefish.com/services/steganography/steganography.php
- https://manytools.org/hacker-tools/steganography-encode-text-into-image/
- https://steganosaur.us/dissertation/tools/image
- https://georgeom.net/StegOnline
-----

### windows
- Redline.
- Bulk-extractor
- FTK Imager
- Use Autopsy
- ProDiscover
- EnCase software
- Recuva
- RegRipper
- Windows event viewer
-----

### audio
- sonicvisualizer
- deepsound
- spectrology
- mp3stego
- audiostego
- wavsteg
-----

### Compressed file
- Unzip it.
- Use zipdetails -v command to display details about the internal structure of a Zip file.
- Use zipinfo command to know details info about Zip file.
- Use zip -FF input.zip --out output.zip attempt to repair a corrupted zip file.
- Brute-force the zip password using fcrackzip -D -u -p rockyou.txt  filename.zip
- To crack 7z run 7z2hashcat32-1.3.exe filename.7z. Then john --wordlist=/usr/share/wordlists/rockyou.txt hash
-----

### create
- https://medium.com/@stux/malicious-steganography-efa6013f6375

### resources
- https://github.com/mindcrypt/covertchannels-steganography
- https://github.com/lucacav/steg-in-the-wild
- https://github.com/lucacav/steg-tools
- https://kinyabitch.wordpress.com/2016/09/14/forensics-asis-ctf-finals-2016-p1ng/
- https://shankaraman.wordpress.com/category/ctf/stegano/
- https://github.com/jaybosamiya/busysteg
- https://www.secsy.net/easy_stegoCTF
- https://github.com/livz/cloacked-pixel
- https://github.com/DominicBreuker/stego-toolkit
- http://ctfs.github.io/resources/topics/steganography/file-in-image/README.html
- https://github.com/shiltemann/CTF-writeups-public/blob/master/Pragyan_2016/writeup.md
- https://www.prodefence.org/comprehensive-guide-to-steghide-tool/?fbclid=IwAR0POX9pvtzbbp2fU-PiURmNBuQR1fda4RR2L_YvW70CNDI8BewqiuUatkw
- https://pequalsnp-team.github.io/cheatsheet/steganography-101
- https://www.pwndiary.com/write-ups/seccon-ctf-2017-jpeg-file-write-up-forensics100/

### tools
- https://github.com/Zeecka/AperiSolve
