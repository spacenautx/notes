- https://dazzyddos.github.io/posts/Offensive-Lateral-Movement/
- https://riccardoancarani.github.io/2019-10-04-lateral-movement-megaprimer/

- Lateral Movement Windows and Active Directory
https://riccardoancarani.github.io/2019-10-04-lateral.../
- Understanding_Windows_Lateral_Movements (attl4s)
https://attl4s.github.io/.../Understanding_Windows...
- Windows Red Team Lateral Movement Techniques
https://www.linode.com/.../windows-red-team-lateral.../
- Attacking Windows: Performing Lateral Movement with Impacket
https://jb05s.github.io/Attacking-Windows-Lateral.../
- Lateral Movement Using DCOM Objects and C#
https://klezvirus.github.io/RedTeaming/LateralMovement/
- Windows Lateral Movement¶
https://obscuritylabs.github.io/ope.../windows/lat_movement/
- Windows Lateral Movement with smb, psexec and alternatives
https://nv2lt.github.io/.../smb-psexec-smbexec-winexe.../
- Offensive Lateral Movement
https://eaneatfruit.github.io/.../Offensive-Lateral.../
- Misc
https://mrw0r57.github.io/
https://igor-blue.github.io/2021/02/07/sybase.html
