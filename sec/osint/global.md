### People Search
1. [Locate Family](https://www.locatefamily.com/index.html)
2. [NNDB](https://www.nndb.com/)
3. [Free Adverse Media Check](https://namescan.io/FreeAdverseMediaCheck.aspx)
4. [Free Sanction Lists Search](https://namescan.io/FreeSanctionsCheck.aspx)
5. [Free PEP Check](https://namescan.io/FreePEPCheck.aspx)
6. [Free PEP check](https://pepchecker.com/)
7. [Sanctions List Search](https://sanctionssearch.ofac.treas.gov/)
8. [Data.fei.org](https://data.fei.org/Person/Search.aspx)
9. [LittleSis](https://littlesis.org/)
10. [Yandex People](https://yandex.ru/people)
11. [Yearbooks Online](http://www.e-yearbook.com/)
12. [InterNations cities worldwide | InterNations](https://www.internations.org/all-internations-cities/)
13. [Rate My Teachers](https://ratemyteachers.com/)
14. [European Graduates](https://graduates.name/)
15. [Famechain.com](https://www.famechain.com/)
16. [WebMii](https://webmii.com/)
17. [facesaerch](http://www.facesaerch.com/)
18. [Gofindwho.com](https://gofindwho.com/)
19. [IDCrawl](https://www.idcrawl.com/)
20. [recruitin](http://recruitin.net/)
21. [www.alltrails.com/members](https://www.alltrails.com/members/)
----

### Phone Numbers
1. [truecaller](http://truecaller.com)
2. [Google Voice: Forgot Password](https://www.google.com/voice/b/0/account/recovery)
3. [PhoneInfoga](https://demo.phoneinfoga.crvx.fr/)
4. [Free Carrier Lookup](https://www.freecarrierlookup.com/)
----

### Businesses
1. [OpenCorporates](https://opencorporates.com/)
2. [ICIJ Offshore Leaks Database](https://offshoreleaks.icij.org/)
3. [HolaConnect](https://holaconnect.com/)
4. [WhatClinic • Read Reviews & Compare Prices](https://www.whatclinic.com/)
5. [Layoffs](https://www.thelayoff.com/)
6. [Tax Exempt Organization Search| Internal Revenue Service](https://apps.irs.gov/app/eos/)
7. [List of legal entity types by country](https://en.wikipedia.org/wiki/List_of_legal_entity_types_by_country)
8. [THE ORG](https://theorg.com/)
9. [AnnualReports.com](https://www.annualreports.com/)
10. [Salary Database | Fairygodboss](https://fairygodboss.com/salary-database)
11. [Openleis.com](http://openleis.com/)
12. [LEI Search 2.0](https://search.gleif.org/#/search/)
----

### Social Media
1. [Facebook](https://www.facebook.com/)
2. [Twitter](https://twitter.com/)
3. [Instagram](https://www.instagram.com/)
4. [Reddit](https://www.reddit.com/)
5. [VSCO](https://vsco.co/)
6. [Snapchat](https://www.snapchat.com)
7. [TikTok](https://www.tiktok.com)
8. [LinkedIn](https://www.linkedin.com)
9. [Pinterest](https://www.pinterest.co.uk/)
10. [4chan](http://www.4chan.org/)
11. [Fotki](https://www.fotki.com/)
----

### Search Engines
1. [Google](https://www.google.com/)
2. [Runnaroo](https://www.runnaroo.com/)
3. [Yandex](https://yandex.com/)
4. [Bing](https://bing.start.me/?a=gsb_startme_00_00_ssg01)
5. [Qwant](https://www.qwant.com/)
6. [All the Internet](https://www.alltheinternet.com/)
7. [sploitus](https://sploitus.com/)
8. [Search by File Format](http://www.faganfinder.com/filetype/)
9. [Fagan Finder: find anything and everything online](https://www.faganfinder.com/)
10. [Sputtr.](http://www.sputtr.com/)
11. [Google Dataset Search](https://datasetsearch.research.google.com/)
12. [100searchengines](https://www.100searchengines.com/)
13. [SEQE - Search Engines Query Engine](https://seqe.me/)
----

### Maps
1. [Google Maps](https://www.google.co.uk/maps/)
2. [Bing Maps](https://www.bing.com/maps)
3. [Yandex Maps](https://yandex.com/maps/)
4. [Wikimapia](http://wikimapia.org)
5. [what3words](https://map.what3words.com)
6. [Thingful](http://www.thingful.net)
7. [WiGLE](https://wigle.net/)
8. [Dual Maps](http://data.mashedworld.com/dualmaps/map.htm)
9. [TRAVIC](https://tracker.geops.ch/)
10. [Open Railway Map](https://www.openrailwaymap.org/)
11. [Historic Aerials: Viewer](https://www.historicaerials.com/viewer)
12. [FIRMS](https://firms.modaps.eosdis.nasa.gov/)
13. [Government GitHub Accounts](http://jrmistry.maps.arcgis.com/apps/webappviewer/index.html?id=5a54e77bd1fe43829add95e3d24455ca)
14. [Live Piracy Map](https://www.icc-ccs.org/piracy-reporting-centre/live-piracy-map)
15. [Cyber Data Points Map](https://chinatechmap.aspi.org.au/#/map/)
16. [Travel Time](https://app.traveltime.com/)
17. [Mapillary](https://www.mapillary.com/)
18. [Copernix.io](https://copernix.io/)
19. [Zoom Earth](https://zoom.earth/)
20. [Soar.earth](https://soar.earth/?)
----

### Malware
1. [MalwareConfig](https://malwareconfig.com/)
2. [#totalhash | Malware Analysis Database](https://totalhash.cymru.com/)
----

### Education
1. [Top Universities](https://www.topuniversities.com/)
2. [Open Knowledge Maps](https://openknowledgemaps.org/index)
----

### Dark Web
1. [Ahmia — Search Tor Hidden Services](https://ahmia.fi/)
2. [DarkSearch](https://darksearch.io/)
3. [Hunchly - Free Daily Dark Web Reports](https://www.hunch.ly/darkweb-osint/)
4. [Dark Web Map v2 — Hyperion Gray](https://www.hyperiongray.com/dark-web-map/)
5. [DarknetLive](https://darknetlive.com/)
6. [Torwhois.com](https://torwhois.com/)
----

### Legal
1. [lumendatabase.org](https://lumendatabase.org/)
----

### Misc
1. [CRXcavator](https://crxcavator.io/)
2. [Human Trafficking Flow Map](http://dataviz.du.edu/projects/htc/flow/)
3. [Small Arms Survey - The Weapons ID Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
4. [List of circulating currencies](https://en.wikipedia.org/wiki/List_of_circulating_currencies)
----

### Patents
1. [Global Patents](https://www.tmdn.org/tmdsview-web/welcome.html)
2. [Google Patents](https://patents.google.com/)
----

### NEWS
1. [IBM Watson News Explorer](https://news-explorer.mybluemix.net/)
----

### Newspapers
1. [Elephind](https://www.elephind.com/)
2. [Newspapers.com](https://www.newspapers.com/)
----

### BARCODES
1. [Free online Barcode Reader](https://online-barcode-reader.inliteresearch.com/)
----

### Misc
1. [Worldmap of OSINT tools](https://cipher387.github.io/osintmap/)
2. [ProxySite.com](https://proxysite.com)
3. [Top 50 Live Audio Feeds](https://www.broadcastify.com/listen/top)
4. [CyberChef](https://gchq.github.io/CyberChef/)
5. [Timestamp Converter](https://www.epochconverter.com/)
6. [T E X T F I L E S D O T C O M](http://textfiles.com/)
7. [📙 Emojipedia — 😃 Home of Emoji Meanings 💁👌🎍😍](https://emojipedia.org/)
8. [Fakespot](https://www.fakespot.com/analyzer)
9. [Catalogue of Research Databases](https://id.occrp.org/databases/)
----

### Obituaries
1. [Find a grave](https://www.findagrave.com/)
2. [Legacy](https://www.legacy.com/)
3. [BillionGraves](https://billiongraves.com/)
4. [Find Cemeteries and Memorials](https://www.cwgc.org/find/find-cemeteries-and-memorials)
----

### Data Breaches
1. [Have I Been Pwned: Emails](https://haveibeenpwned.com/)
2. [Have I Been Pwned: Pwned Passwords](https://haveibeenpwned.com/Passwords)
3. [Dehashed](https://dehashed.com/)
4. [ghostproject](https://ghostproject.fr/)
5. [SpyCloud](https://spycloud.com/)
6. [Directory of Breached Databases index by Breach Check](https://breachcheck.io/data-sources/page/5/)
7. [Breach Check](https://breachcheck.io/)
8. [USA Data Breach Archives](https://databreacharchives.com/states)
----

### Default Passwords
1. [Default Passwords - CIRT](https://cirt.net/passwords)
2. [Default passwords list](https://default-password.info/)
3. [Default Password Lookup Utility](https://www.fortypoundhead.com/tools_dpw.asp)
4. [Router Passwords](https://www.routerpasswords.com/)
5. [5342 Default Passwords from Open Sez Me!](https://open-sez.me/)
6. [default password](https://passwordsdatabase.com/)
7. [Default Password](http://defaultpassword.us/)
8. [Default-Credentials · danielmiessler/SecLists · GitHub](https://github.com/danielmiessler/SecLists/tree/master/Passwords/Default-Credentials)
----

### Hashes
1. [Online Free Hash Identification](https://www.onlinehashcrack.com/hash-identification.php)
2. [Decrypt MD5, SHA1, MySQL, NTLM, SHA256, SHA512, Wordpress, Bcrypt hashes for free online](https://hashes.com/en/decrypt/hash)
----

### Email Addresses
1. [Epieos](https://tools.epieos.com/google-account.php)
2. [Identificator](https://identificator.space/search)
3. [mailcat - GitHub](https://github.com/sharsil/mailcat)
4. [List of disposable email provider domains](https://gist.github.com/michenriksen/8710649)
5. [Disposable Email Address (DEA) Detector](https://tools.verifyemailaddress.io/Apps/Disposable_Email_Address_Detector)
6. [Email Address Verifier](https://tools.verifyemailaddress.io/)
7. [Email Verifier](https://hunter.io/email-verifier)
8. [Trumail | Free Email Verification API](https://trumail.io/)
9. [Verify Email Address Online](https://verify-email.org/)
----

### Usernames
1. [Whatsmyname.app](https://whatsmyname.app/)
2. [namechk](https://namechk.com/)
3. [NameMC](https://namemc.com/)
4. [Laby.net](https://laby.net/)
5. [Steam Community](https://steamcommunity.com/search/users/#text=)
6. [Username Search](https://usersearch.org/index.php)
7. [Keybase](https://keybase.io/)
8. [Namevine](https://namevine.com/)
9. [Social Searcher](https://www.social-searcher.com/)
10. [Steamid.uk](https://steamid.uk/)
11. [NameCheckup](https://namecheckup.com/)
----

### Domains
1. [RiskIQ](https://community.riskiq.com/home)
2. [Whoxy](https://www.whoxy.com)
3. [domainbigdata](https://domainbigdata.com/)
4. [SiteSleuth.io](https://www.sitesleuth.io)
5. [Internet Archive](https://archive.org/)
6. [ROBTEX](https://www.robtex.com/)
7. [dnsdumpster](https://dnsdumpster.com/)
8. [visualping](https://visualping.io/)
9. [BuiltWith](https://builtwith.com/)
10. [Visual Site Mapper](http://www.visualsitemapper.com/)
11. [Backlink Checker](https://smallseotools.com/backlink-checker/)
12. [VirusTotal](https://www.virustotal.com/gui/home/url)
13. [CachedView](https://cachedview.nl/)
14. [Reverse Analytics ID](https://dnslytics.com/reverse-analytics)
15. [Shodan](https://www.shodan.io/)
16. [pulsedive](https://pulsedive.com/)
17. [desenmascara.me](http://desenmascara.me/)
18. [AlternativeTo](https://alternativeto.net/)
19. [Offliberty](http://offliberty.com/)
20. [UKWA Home](https://www.webarchive.org.uk/)
21. [Phonebook.cz](https://phonebook.cz/)
22. [Chaos.projectdiscovery.io](https://chaos.projectdiscovery.io/#/)
23. [IANA — Root Zone Database](https://www.iana.org/domains/root/db)
24. [My IP Address](https://myip.ms/)
25. [unfurl](https://dfir.blog/unfurl/)
26. [Sitelike.org](https://www.sitelike.org/)
27. [whoisxmlapi](https://tools.whoisxmlapi.com/reverse-whois-search)
28. [WHOIS History API](https://whois-history.whoisxmlapi.com/)
29. [research.domaintools.com/research/whois-history](https://research.domaintools.com/research/whois-history/)
30. [SimilarWeb](https://www.similarweb.com/)
----

### File Metadata
1. [VirusTotal](https://www.virustotal.com/gui/home/upload)
2. [Metashield](https://metashieldclean-up.elevenpaths.com/)
3. [Metadata Viewer](https://www.extractmetadata.com/)
4. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
----

### Politics
1. [EveryPolitician](http://everypolitician.org/)
2. [LittleSis](https://littlesis.org/)
----

### Vehicles
1. [European Database of Stolen Vehicles](http://www.stolencars.eu/en/)
2. [Stolencars24](http://www.stolencars24.eu/en/main.php)
3. [Salvage Cars Auction History](https://poctra.com/)
4. [Exotic Cars | The Largest Photo Collection](https://www.autogespot.com/)
5. [Poctra.com](https://poctra.com/)
6. [enterprise Rental Receipts](https://www.enterprise.com/en/reserve/receipts.html?icid=legacy.redirect-_-receipts-_-ENUS.NULL#)
7. [Alamo Rental Receipts](https://legacy.alamo.com/en_US/car-rental/receipts.html)
8. [Hertz Rent Receipt](https://www.hertz.com/rentacar/receipts/request-receipts.do)
9. [Carnet.ai](https://carnet.ai/)
----

### Crime
1. [Europe's most wanted](https://eumostwanted.eu/)
2. [Global Terrorism Database](https://www.start.umd.edu/gtd/)
3. [Extremist Groups](https://www.counterextremism.com/global_extremist_groups)
4. [Interpol - Red Notices](https://www.interpol.int/How-we-work/Notices/View-Red-Notices)
5. [StAR - Stolen Asset Recovery Initiative - Corruption Cases](https://star.worldbank.org/corruption-cases/)
6. [https://www.cityprotect.com/map/](https://www.cityprotect.com/map/)
7. [BikeRegister](https://www.bikeregister.com/bike-checker)
----

### Webcams
1. [Insecam](http://www.insecam.org/)
2. [lookr](https://www.lookr.com/)
----

### Apps
1. [APPlyzer - App Rankings - ASO Tools - API](https://www.applyzer.com/?mmenu=worldcharts)
----

### Financial
1. [Bank Codes & IBAN Lookup Search](https://bank.codes/)
2. [IBAN Calculate/Search](https://www.ibancalculator.com/iban_berechnen_bic.html)
3. [Online BIC Search](https://www2.swift.com/bsl/facelets/bicsearch.faces)
----

### Cryptocurrency
1. [Bitcoin Address Balance](https://www.blockonomics.co/)
2. [bitcoin whos who](https://bitcoinwhoswho.com/)
3. [Blockchain Explorer](https://www.blockchain.com/explorer)
4. [WalletExplorer.com: smart Bitcoin block explorer](https://www.walletexplorer.com/)
5. [Bitcoin Block Explorer](https://blockexplorer.com/)
6. [Bitcoin Block Explorer](https://btc.com/)
----

### Military
1. [Service Stories](https://one-name.org/service_story/?)
2. [Trade Registers](http://armstrade.sipri.org/armstrade/page/trade_register.php)
3. [Camopedia](http://camopedia.org/)
4. [FSI](https://cisac.fsi.stanford.edu/mappingmilitants)
5. [List of comparative military ranks](https://en.wikipedia.org/wiki/List_of_comparative_military_ranks)
----

### Documents
1. [Yumpu](https://www.yumpu.com/en)
2. [DocumentCloud](https://www.documentcloud.org/public/search/)
----

### Public Records
1. [Free Public Records](http://publicrecords.searchsystems.net/)
2. [OCCRP](https://data.occrp.org/)
3. [FamilySearch](https://www.familysearch.org/search/)
4. [Ancestry](https://www.ancestry.co.uk/search/)
5. [myheritage.com](https://www.myheritage.com/names/firstName_lastName)
6. [The World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
7. [Snowden Doc Search](https://search.edwardsnowden.com/)
8. [Our Family History](http://sherrysharp.com/genealogy/index.php)
9. [Familyrelatives](https://www.familyrelatives.com/)
10. [Find War Dead - Search for the fallen Commonwealth soldiers](https://www.cwgc.org/find/find-war-dead)
11. [Ellis Island - Passenger Search](https://www.libertyellisfoundation.org/passenger)
12. [Marriages of the World](https://one-name.org/marriages-of-the-world-search/)
13. [National History Museum - Data Portal](https://data.nhm.ac.uk/)
14. [One-Step Webpages by Stephen P. Morse](https://stevemorse.org/index.html)
----

### Aviation
1. [flightradar24](https://www.flightradar24.com/)
2. [UKGA](https://ukga.com/gis/airfield)
3. [ADS-B Exchange](https://www.adsbexchange.com/)
4. [Airportia](https://www.airportia.com/)
5. [Aviation's Databases](http://www.landings.com/_landings/pages/search/reg-world.html)
----

### Marine
1. [Marine Traffic](https://www.marinetraffic.com)
2. [BoatInfoWorld](https://www.boatinfoworld.com/)
----

### Facial Recognition
1. [Betaface](https://www.betafaceapi.com/demo.html)
2. [How Old do I Look?](https://www.how-old.net/)
3. [PimEyes](https://pimeyes.com)
4. [pictriev](http://www.pictriev.com/)
5. [Face Comparing](https://www.faceplusplus.com/face-comparing/)
----

### IP Addresses
1. [Proxy Or Not](http://www.proxyornot.com/)
2. [Shodan](https://www.shodan.io/)
3. [CyberHub](https://cyber-hub.net/ip2skype.php)
4. [Trace An IP](https://www.ip-adress.com/ip-address/lookup)
5. [iknowwhatyoudownload](https://iknowwhatyoudownload.com/en/peer/)
6. [Robtex](https://www.robtex.com/ip-lookup/)
7. [IP Address Details](https://ipinfo.io/)
----

### Images
1. [Google Images](https://www.google.co.uk/imghp)
2. [Google Advanced Image Search](https://www.google.com/advanced_image_search)
3. [Yandex Images](http://yandex.com/images/)
4. [Bing Images](https://www.bing.com/images/)
5. [Adobe Stock](https://stock.adobe.com/)
6. [Karma Decay](http://karmadecay.com/)
7. [Library of Congress Online Catalog](http://www.loc.gov/pictures/collection/guide/)
8. [rootabout](http://rootabout.com/)
9. [image identify](https://www.imageidentify.com/)
10. [TinEye](https://www.tineye.com/)
11. [Giphy](https://giphy.com/)
12. [Pic2Map Photo Location Viewer](https://www.pic2map.com/)
13. [Colorize Black and White Photos](https://demos.algorithmia.com/colorize-photos/)
14. [Forensically - 29a.ch](https://29a.ch/photo-forensics/#forensic-magnifier)
15. [Remove Background](https://www.remove.bg/)
16. [LetsEnhance](https://letsenhance.io/)
17. [Diff Checker](https://www.diffchecker.com/image-diff)
18. [FotoForensics](http://fotoforensics.com/)
19. [InVid](http://logos.iti.gr/logos/)
20. [loc.alize.us](https://loc.alize.us)
21. [ImgOps](http://imgops.com/)
22. [Free Online OCR](https://www.onlineocr.net/)
23. [Photopea Photo Editor](https://www.photopea.com/)
24. [Photo Editor](https://pixlr.com/x/)
25. [picsearch](http://www.picsearch.co.uk/index.cgi?q=&start=1)
26. [Resize Images Online - Reduce JPG, BMP, GIF, PNG images](https://www.reduceimages.com/)
27. [EXIF Data Viewer](http://exifdata.com/)
28. [Gallery of AI Generated Faces](https://generated.photos/faces)
29. [This Person Does Not Exist](https://thispersondoesnotexist.com/)
30. [3D Face Reconstruction](https://cvl-demos.cs.nott.ac.uk/vrn/index.php)
31. [CameraTrace](http://www.cameratrace.com/trace)
32. [Identify.plantnet.org](https://identify.plantnet.org/)
33. [Geolocation Estimation](https://labs.tib.eu/geoestimation/)
----

### Time Zones
1. [The Time Zone Converter](http://www.thetimezoneconverter.com/)
2. [Time.is](https://time.is/)
3. [World Time](http://localtimes.info/)
4. [Time Zone Map](https://www.timeanddate.com/time/map/)
5. [World Clock](https://24timezones.com/#/map)
6. [everytimezone.com](https://everytimezone.com/)
----

### Classified Ads
1. [bedpage](https://www.bedpage.com/)
2. [FatFingers](http://fatfingers.com/default.aspx)
3. [Auction Sniper & eBay Bidding Tools](https://www.goofbid.com/)
4. [Ebay - Advanced search](https://www.ebay.co.uk/sch/ebayadvsearch)
5. [Craigslist](https://www.craigslist.org)
6. [Fakespot](https://www.fakespot.com/)
7. [Carousell](https://sg.carousell.com/)
----

### Other Forums
1. [Crunchyroll - Forum](https://www.crunchyroll.com/forum)
2. [Destructoid](https://www.destructoid.com/blogs/)
3. [Forums Archive | Gap Year](https://www.gapyear.com/boards)
4. [Game FAQs Community - Activity Feed](https://gamefaqs.gamespot.com/community)
5. [IGN Boards](https://www.ign.com/boards/)
6. [Off Topic - Minecraft Forum - Minecraft Forum](https://www.minecraftforum.net/forums/off-topic)
7. [MMORPG.com - A MMO Gaming Community](https://forums.mmorpg.com/)
8. [r/gaming](https://www.reddit.com/r/gaming/)
9. [Platform & Game Forums | Polygon](https://www.polygon.com/forums)
10. [Nexopia - Lifestyle and Community Forums](https://forums.nexopia.com/)
11. [Off-Topic Discussion | NeoGAF](https://www.neogaf.com/forums/off-topic-discussion.22/)
12. [📚 Everything Else - Tidbits](http://tidbits.io/c/general)
13. [Gaming Forum, Discussion & Latest News | The Verge](https://www.theverge.com/forums/gaming)
14. [Gaming Forum - Video Game Forums](https://www.vgr.com/forum/)
----

### Hacking Forums
1. [HackForums](https://hackforums.net/)
2. [RaidForums](https://raidforums.com/)
3. [Forgot e-mail | EpicNPC Marketplace](https://www.epicnpc.com/forgotemail/)
----

### Radio
1. [vTuner Internet Radio](http://vtuner.com/setupapp/guide/asp/BrowseStations/StartPage.asp?sBrowseType=Location)
2. [Radio Garden](https://radio.garden/live/)
----

