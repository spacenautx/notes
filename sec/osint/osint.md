### Reverse Image
1. [Baidu Reverse Image](http://shitu.baidu.com/)
2. [Bing Reverse Image](https://www.bing.com/?scope=images&nr=1&FORM=NOFORM)
3. [Google Images](https://images.google.com/?gws_rd=ssl)
4. [Karma Decay](http://karmadecay.com/)
5. [Pictriev](http://www.pictriev.com/)
6. [TinEye](https://tineye.com/)
7. [Yandex.Images: search for images on the internet, search by image](https://yandex.com/images/)
----

### Imagen
1. [CameraTrace: Trace A Camera For Free](http://www.cameratrace.com/trace)
2. [Diff Checker](https://www.diffchecker.com/image-diff/)
3. [Digital Image Forensic Analyzer](https://www.imageforensic.org/#work)
4. [Exifpurge.com](http://www.exifpurge.com/)
5. [ExifTool by Phil Harvey](https://exiftool.org/)
6. [Font Finder](https://www.whatfontis.com/)
7. [Forensically, free online photo forensics tools - 29a.ch](https://29a.ch/photo-forensics/#forensic-magnifier)
8. [FotoForensics](http://fotoforensics.com/)
9. [Identificación logos](https://logos.iti.gr/logos/)
10. [Identifont](http://www.identifont.com/index.html)
11. [ImgOps](http://imgops.com/)
12. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
13. [Metadata2go](https://www.metadata2go.com/)
14. [metapicz](http://metapicz.com/)
15. [Reveal](http://reveal-mklab.iti.gr/reveal/index.html)
16. [Sensity.ai](https://platform.sensity.ai/deepfake-detection)
17. [SunCalc sun position- und sun phases calculator](http://suncalc.org/)
18. [WhatTheFont! « MyFonts](https://www.myfonts.com/WhatTheFont/)
----

### Webcam
1. [EarthCam](https://www.earthcam.com/)
2. [Insecam](http://insecam.org/)
----

### Video
1. [Community Video](https://archive.org/details/opensource_movies)
2. [deturl.com](http://deturl.com/)
3. [Extract Meta Data](http://amnestyusa.org/sites/default/custom-scripts/citizenevidence)
4. [Geo Search Tool](http://youtube.github.io/geo-search-tool/search.html)
5. [http://rg3.github.io/youtube-dl/](http://rg3.github.io/youtube-dl/)
6. [inVID](https://www.invid-project.eu/)
7. [Location Search](https://mattw.io/youtube-geofind/location)
8. [Online video downloader](https://es.savefrom.net/1-youtube-video-downloader-5.html)
9. [Online Video Downloader](https://yoodownload.com/)
10. [RadiTube — A Search Engine for Radical Content on YouTube](https://tool.raditube.com/)
11. [Screenshot YouTube - Chrome Web Store](https://chrome.google.com/webstore/detail/screenshot-youtube/gjoijpfmdhbjkkgnmahganhoinjjpohk/related)
12. [watch frame by frame](http://www.watchframebyframe.com/)
13. [you-tldr](https://you-tldr.com/)
14. [YouTube Metadata](https://mattw.io/youtube-metadata/)
----

### Twitter
1. [Accountanalysis.app](https://accountanalysis.app/)
2. [All My Tweets](https://www.allmytweets.net/connect/)
3. [Botometer by OSoMe](http://botometer.iuni.iu.edu/)
4. [Descargar Videos de Twitter](https://www.downloadtwittervideo.com/es/)
5. [GeoChirp](http://www.geochirp.com/)
6. [GitHub - sferik/t: A command-line power tool for Twitter.](http://github.com/sferik/t)
7. [Hoaxy: How claims spread online](https://hoaxy.iuni.iu.edu/)
8. [InVID Verification Plugin](http://www.invid-project.eu/verify)
9. [paulgb/Treeverse](https://github.com/paulgb/Treeverse)
10. [Phantombuster](https://phantombuster.com/automations/twitter/4130/twitter-follower-collector)
11. [SMAT](https://www.smat-app.com/network/account/realDonaldTrump)
12. [Socialbearing](https://socialbearing.com/)
13. [Socialblade](https://socialblade.com/)
14. [Spoonbill](https://spoonbill.io/)
15. [The one million tweet map](http://onemilliontweetmap.com/)
16. [tinfoleak](https://tinfoleak.com/)
17. [trends24](https://trends24.in/)
18. [TweetBeaver](https://tweetbeaver.com/)
19. [Tweetdeck](https://tweetdeck.twitter.com/)
20. [TweeterID](https://tweeterid.com/)
21. [twintproject/twint](http://github.com/twintproject/twint)
22. [Twitonomy](http://www.twitonomy.com/)
23. [Twitter Advanced Search](http://twitter.com/search-advanced)
24. [Twitter List Copy](http://projects.noahliebman.net/listcopy/connect.php)
25. [twitter video downloader](http://twittervideodownloader.com/)
26. [TwitterAudit](http://twitteraudit.com/)
27. [Twitterfall](https://twitterfall.com/)
28. [twlets](http://twlets.com/)
----

### Instagram
1. [instaloader/instaloader](https://github.com/instaloader/instaloader)
2. [Storysaver.net](https://www.storysaver.net/)
----

### TikTok
1. [TikTok Reverse Images](https://search4faces.com/tt00/index.html)
2. [TikTokApi · PyPI](https://pypi.org/project/TikTokApi/)
3. [TT Downloader](https://chrome.google.com/webstore/detail/tiktok-downloader/blbckhiepgpniilpmlionnkjoeehhgao)
----

### Telegram
1. [Google Custom Search](http://bit.ly/telegago)
2. [Search.buzz.im](https://search.buzz.im/)
3. [TelegramDB.org](http://telegramdb.org/)
4. [Tgstat.com](http://tgstat.com/)
5. [Tools](https://intelx.io/tools?tab=telegram)
----

### SnapChat
1. [Snap Map](http://map.snapchat.com/)
----

### Linkedin
1. [vysecurity/LinkedInt](https://github.com/vysecurity/LinkedInt)
----

### Facebook
1. [¿Cuál es mi ID de Facebook?](https://es.piliapp.com/facebook/id/)
2. [Facebook Graph Search Generator by Henk van Ess and Torment Gerhardsen](http://graph.tips/beta)
3. [Facebook ID](https://www.duplichecker.com/find-facebook-id.php)
4. [Facebook Video Downloader Online](https://www.fdown.net/)
5. [Find my Facebook ID](https://lookup-id.com/)
6. [searchisback](http://searchisback.com/)
7. [Who posted what?](http://whopostedwhat.com/)
----

### Reddit
1. [How F5Bot Slurps All of Reddit](http://intoli.com/blog/f5bot/)
2. [Removeddit](https://www.removeddit.com/)
----

### Username
1. [Instant Username Search](https://instantusername.com/#/)
2. [KnowEm](https://knowem.com/)
3. [namecheckr](https://www.namecheckr.com/)
4. [Namechk](http://namechk.com/)
5. [Pipl](http://pipl.com/)
6. [Thats them](https://thatsthem.com/)
7. [Username Search](https://usersearch.org/index.php)
8. [WebBreacher/WhatsMyName](https://github.com/WebBreacher/WhatsMyName)
9. [WhatsMyName Web](https://whatsmyname.app/)
10. [WebMii](https://webmii.com/)
----

### Teléfonos
1. [abc telefonos](https://www.abctelefonos.com/)
2. [CNMC](https://numeracionyoperadores.cnmc.es/numeracion)
3. [GitHub - codeonlinux/ohmyqr](https://github.com/codeonlinux/ohmyqr)
4. [Listado de todos los operadores y proveedores VoIP de España](https://www.sinologic.net/2014-03/lista-todos-operadores-voip-espana.html)
5. [numverify API](https://numverify.com/)
6. [PhoneInfoga](https://demo.phoneinfoga.crvx.fr/)
7. [thewhiteh4t/seeker](https://github.com/thewhiteh4t/seeker)
8. [Peru Operadoras](https://deperu.com/celulares/)
----

### Email
1. [Analyze my mail header](https://mailheader.org/)
2. [Email Address Verifier](https://tools.emailhippo.com/)
3. [Email Lookup EPIEOS](https://tools.epieos.com/email.php)
4. [Emailrep.io](https://emailrep.io/)
5. [Emkei's Fake Mailer](https://emkei.cz/)
6. [Gravatar](https://en.gravatar.com/site/check/)
7. [Guerrilla Mail](https://www.guerrillamail.com/#/inbox)
8. [Have I been pwned?](https://haveibeenpwned.com/)
9. [Hunter](https://hunter.io/)
10. [Messageheader](https://toolbox.googleapps.com/apps/messageheader/)
11. [MXtoolbox](https://mxtoolbox.com/)
12. [Quantika14/email-osint-ripper](https://github.com/Quantika14/email-osint-ripper)
13. [Verify Email Address Online](https://verify-email.org/)
14. [YOPmail](https://yopmail.com/es/)
15. [Email Sherlock](https://www.emailsherlock.com/)
----

### cuentas falsas
1. [Avatar Maker](https://avatarmaker.com/)
2. [Fake Identity Generator](https://datafakegenerator.com/generador.php)
3. [Fake Name Generator](https://www.fakenamegenerator.com/advanced.php)
4. [Fake Name Generator](https://en.namefake.com/)
5. [Fake Tweet,Chat & Facebook Status Generator](https://www.prankmenot.com/?facebook_full_chat)
6. [Fake Tweetgen](https://www.tweetgen.com/create/tweet.html)
7. [Generated Faces](https://generated.photos/faces)
8. [Img2Go.com - Online photo editor and image converter](https://www.img2go.com/)
9. [Name Generator](http://namegenerators.org/fake-name-generator/)
10. [Random name](https://minirandom.com/)
11. [Fake WhatsApp Chat Generator](http://www.fakewhats.com/generator)
12. [Random User Generator](https://randomuser.me/photos)
13. [Receive SMS Online](https://receive-smss.com/)
14. [Receive text messages](https://www.spoofbox.com/en/tool/trash-mobile)
15. [Sensity](https://sensity.ai/)
16. [SMSPVA](http://smspva.com/)
17. [This Person Does Not Exist](https://www.thispersondoesnotexist.com/)
18. [UK-OSINT Framework](https://www.uk-osint.net/creatingids.html)
19. [Buy Facebook PVA Accounts | Email Verified - Bulk Social Media PVA Accounts | Email Verified Accounts | Bulk PVA](https://www.bulkpva.com/product/buy-facebook-pva-accounts-email-verified/)
20. [Textverified. Bypass Voice, SMS and Text Verifications](https://www.textverified.com/)
----

### SMS virtual/Telefonos
1. [Burstsms.com.au](https://burstsms.com.au/)
2. [directSMS](https://www.directsms.com.au/)
3. [Esendex.co.uk](https://www.esendex.co.uk/)
4. [GetSMS.ONLINE - receive sms online, sms activations Facebook, sms activations Instagram and other services, rent phone numbers for sms and activations, rent virtual phone number](https://getsms.online/en/)
5. [Número virtual - Llamadas internacionales baratas | Sonetel](https://sonetel.com/es/)
6. [Proovl SMS Services](https://www.proovl.com/)
7. [Receive SMS Online](https://smsreceivefree.com/)
8. [Receive SMS Online for FREE](https://receive-sms-online.info/)
9. [Receive SMS Online For Free](https://hs3x.com/)
10. [ReceiveaSMS.COM](https://www.receiveasms.com/)
11. [Receivesmsonline.in](https://receivesmsonline.in/)
12. [Receivesmsverification.com](https://receivesmsverification.com/)
13. [Sms-receive.net](https://sms-receive.net/)
14. [SMSPVA](http://smspva.com/)
15. [Vumber, get a phone number for your business](https://www.vumber.com/)
16. [WhatsApp Tools](https://watools.io/)
----

### web
1. [Alertas de Google: controla la Web en busca de contenido nuevo e interesante](https://www.google.es/alerts)
2. [archive.is](https://archive.is/)
3. [CachedPages](http://www.cachedpages.com/)
4. [Google Cached Pages of Any Website](http://cachedview.com/)
5. [Internet Archive: Wayback Machine](https://archive.org/web/)
6. [Lampyre](https://lampyre.io/)
7. [opsdisk/pagodo Dorks](https://github.com/opsdisk/pagodo)
8. [The Exploit Database Dorks](https://www.exploit-db.com/)
9. [UKWA Home](https://www.webarchive.org.uk/ukwa/)
10. [WikiLeaks](https://wikileaks.org/)
----

### Traductor idiomas
1. [Bing Microsoft Translator](https://www.bing.com/translator)
2. [DeepL Translator](https://www.deepl.com/translator)
3. [Google Translate](https://translate.google.com/)
4. [Translate text from photos from English and other languages – Yandex.Translate](https://translate.yandex.com/ocr)
----

### GITHUB
1. [Ph055a/OSINT_Collection](https://github.com/Ph055a/OSINT_Collection)
2. [s0md3v/Photon](https://github.com/s0md3v/Photon)
3. [megadose/holehe](https://github.com/megadose/holehe)
----

### Mapas
1. [bing.com/maps](http://bing.com/maps)
2. [Carte.ma](http://carte.ma/)
3. [EOLi-SA Dismissed](http://earth.esa.int/web/guest/eoli)
4. [F4map Demo - Interactive 3D map](https://demo.f4map.com/#lat=52.5208538&lon=13.4061429&zoom=17&camera.theta=80&camera.phi=-60.734)
5. [Free Map Tools](https://www.freemaptools.com/)
6. [Gaode.com](https://gaode.com/)
7. [Geograph Worldwide](http://geograph.org/)
8. [GeoNames](http://www.geonames.org/)
9. [Gjirafa.biz](https://gjirafa.biz/)
10. [Google Maps](http://maps.google.com/)
11. [HERE WeGo](http://wego.here.com/)
12. [Hitta](http://hitta.se/)
13. [LatLong](https://www.latlong.net/)
14. [MapChecking](https://www.mapchecking.com/)
15. [musafir-py/geolocatethis](http://github.com/musafir-py/find2places)
16. [Open Access Hub](http://scihub.copernicus.eu/)
17. [OpenSeaMap](https://map.openseamap.org/)
18. [Satellites.pro](https://satellites.pro/)
19. [Sentinel-hub Playground](http://apps.sentinel-hub.com/sentinel-playground)
20. [Ukraine Interactive map](https://liveuamap.com/)
21. [UTM <-> Latitude/Longitude online converter](http://synnatschke.de/geo-tools/coordinate-converter.php)
22. [Welcome to the QGIS project!](http://www.qgis.org/)
23. [what3words](http://map.what3words.com/)
24. [Wikimapia](http://wikimapia.org/)
25. [World Imagery Wayback](https://livingatlas.arcgis.com/wayback/?active=51423&ext=-115.35069,35.99744,-115.24631,36.13051)
26. [Yandex.Maps — detailed map of the world](http://yandex.com/maps)
27. [Mapas Chinos Kakao](https://map.kakao.com/)
28. [Mapas Chinos Baidu](http://map.baidu.com/)
----

### Medios de transporte
1. [Flight-data.adsbexchange.com](http://flight-data.adsbexchange.com/)
2. [FlightAware](http://flightaware.com/)
3. [Flightradar24](http://flightradar24.com/)
4. [Global Fishing Watch shared workspace](http://globalfishingwatch.org/map)
5. [https://archive.is/07xaa](https://archive.is/07xaa)
6. [https://archive.is/pQGms](https://archive.is/pQGms)
7. [https://archive.is/QZD7A](https://archive.is/QZD7A)
8. [IMO Web Accounts](http://webaccounts.imo.org/)
9. [Licenseplatemania.com](http://licenseplatemania.com/)
10. [Marine Traffic](http://marinetraffic.com/)
11. [MarineTraffic: Global Ship Tracking Intelligence](https://www.marinetraffic.com/en/ais/home/centerx:-12.0/centery:25.0/zoom:4)
12. [Memorandum of Understanding on Port State Control in the Asia-Pacific](http://www.tokyo-mou.org/)
13. [Plane Finder Flight Tracker](http://planefinder.net/)
14. [RadarBox](http://radarbox24.com/)
15. [The OpenSky Network](http://opensky-network.org/)
16. [Tumblr](https://bosphorusobserver.com/)
17. [VesselFinder](http://vesselfinder.com/)
18. [VIN decoder](http://www.vindecoderz.com/)
19. [wikiroutes](http://wikiroutes.info/)
----

### IP
1. [Andy21.com](https://www.andy21.com/ip/)
2. [Free online network tools](https://centralops.net/co/)
3. [Geo IP Tool](https://www.geoiptool.de/)
4. [Grabify IP Logger & URL Shortener](https://grabify.link/)
5. [Infosniper.net](https://www.infosniper.net/)
6. [IP Address API and Data Solutions](https://ipinfo.io/)
7. [IP Location Finder](https://www.iplocation.net/)
8. [IP Locator](https://www.ipfingerprints.com/)
9. [IP Logger URL Shortener](https://iplogger.com/)
10. [IP2Location](https://www.ip2location.com/)
11. [Ipaddress.my](https://www.ipaddress.my/)
12. [Liveipmap.com](https://liveipmap.com/)
13. [Online Port Scanner Powered by Nmap](https://hackertarget.com/nmap-online-port-scanner/)
14. [Shodan](http://shodan.io/)
15. [Visualping](https://visualping.io/)
16. [WhatIsMyIP.com®](https://www.whatismyip.com/)
17. [Whatportis.com](https://whatportis.com/)
18. [WTF is my IP?!?!??](https://wtfismyip.com)
----

### Dominios
1. [archive.fo](http://archive.today/)
2. [Bit.do URL Shortener](http://bit.do/)
3. [Bitly](https://bitly.com/)
4. [Carbondate.cs.odu.edu](https://carbondate.cs.odu.edu/)
5. [complete dns](http://completedns.com/)
6. [dnsdumpster](https://dnsdumpster.com/)
7. [Domains Integrity Service by Hold Security LLC.](https://holdintegrity.com/checker)
8. [Dominios.es |](https://www.dominios.es/)
9. [Free WHOIS](https://www.ip2whois.com/)
10. [hartator/wayback-machine-downloader](http://github.com/hartator/wayback-machine-downloader)
11. [IANA — Root Zone Database](https://www.iana.org/domains/root/db)
12. [Internet Archive](http://archive.org/)
13. [lanmaster53/recon-ng](https://github.com/lanmaster53/recon-ng)
14. [List of Accredited Registrars](https://www.icann.org/en/accredited-registrars?filter-letter=a&sort-direction=asc&sort-param=name&page=1)
15. [Reverse Whois Lookup](https://viewdns.info/reversewhois/)
16. [Robtex](https://www.robtex.com/)
17. [Root Server Technical Operations Assn](https://root-servers.org/)
18. [SecurityTrails](http://dnstrails.com/)
19. [SpyOnWeb](http://spyonweb.com/)
20. [spyse.com/tools/dns-lookup](https://spyse.com/tools/dns-lookup)
21. [theHarvester](http://www.edge-security.com/theharvester.php)
22. [ViewDNS.info](https://viewdns.info/)
23. [Who.is](https://who.is/)
24. [Whois whoxy](https://www.whoxy.com/whois-database/)
25. [Whois. DomainTools](https://whois.domaintools.com/)
26. [Whoisology](https://whoisology.com/#advanced)
----

### VPN/Proxy
1. [Free ProxyScrape](https://proxyscrape.com/free-proxy-list)
2. [Free-proxy-list.net](https://free-proxy-list.net/)
3. [ProtonVPN](https://protonvpn.com/)
4. [Proxy Server List](https://www.proxynova.com/proxy-server-list)
5. [TunnelBear VPN](https://www.tunnelbear.com/)
----

### criptografía
1. [SHA-1 conversion and SHA-1 reverse lookup](https://sha1.gromweb.com/)
2. [MD5 Online](https://www.md5online.org/md5-decrypt.html)
3. [Online PGP - Web Encrypt](https://webencrypt.org/openpgpjs/)
4. [Online PGP SELA](https://sela.io/pgp-en/)
----

### criptomonedas
1. [AlertingCryptocurrency](https://cryptocurrencyalerting.com/wallet-watch.html)
2. [Best Bitcoin Mixers](https://bestbitcoinmixers.com/)
3. [Bitcoin Block Explorer](https://blockexplorer.com/)
4. [bitcoin whos who](https://bitcoinwhoswho.com/)
5. [BitInfoCharts](https://bitinfocharts.com/)
6. [Blockchain Explorer](https://www.blockchain.com/explorer)
7. [Check Bitcoin Wallet Address Balance](https://bitref.com/)
8. [coinmarketcap](https://coinmarketcap.com/es/)
9. [DeFi Pulse](https://defipulse.com/)
10. [LiveCoinWatch](https://www.livecoinwatch.com/)
11. [The Ethereum  Block Explorer](https://etherscan.io/)
12. [WalletExplorer](https://www.walletexplorer.com/)
13. [xmrchain.net monero explorer](https://xmrchain.net/)
----

### Dark web
1. [Ahmia —
      Search Tor Hidden Services](https://ahmia.fi/)
2. [geti2p.net/en](https://geti2p.net/en/)
3. [Hidden Wiki](http://thehiddenwiki.org/)
4. [The Tor Project](https://www.torproject.org/download/)
5. [Tor2web: Browse the Tor Onion Services](https://www.tor2web.org/)
6. [WebOProxy](https://weboproxy.com/)
7. [Dark Web Links - The Best Dark Web Links list in 2021](https://darkweblinks.wiki/)
8. [Torch Buscador](http://xmh57jrknzkhv6y3ls3ubitzfqnkrwxhopf5aygthi7d6rplyvk3noyd.onion)
----

