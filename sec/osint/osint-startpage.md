### Directories & Tools
1. [OSINT Framework](https://osintframework.com/)
2. [Reuser's New Repertorium](http://rr.reuser.biz/)
3. [Online Strategies](http://www.onstrat.com/osint/)
4. [Awesome OSINT](https://github.com/jivoi/awesome-osint)
5. [OSINT Tools](https://www.osinttechniques.com/osint-tools.html)
6. [Divine Intel - Tools & Resources](https://divineintel.com/tools-resources/)
7. [Aviva Directory](https://www.avivadirectory.com)
----

### More OSINT pages
1. [The Ultimate OSINT Collection](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
2. [V3nari Bookmarks](https://start.me/p/1kxyw9/v3nari-bookmarks)
3. [AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
4. [OSINT](https://start.me/p/gy1BgY/osint)
5. [1. OSINT Toolset](https://start.me/p/MEw7be/1-osint-toolset)
6. [OSINT](https://start.me/p/ZME8nR/osint)
----

### General Search engines
1. [Advangle](http://advangle.com/)
2. [Answer The Public](https://answerthepublic.com/)
3. [Duck Duck Go](https://duckduckgo.com/)
4. [Google Advanced Search](https://www.google.com/advanced_search)
5. [iSEEK](http://www.iseek.com/iseek/home.page)
6. [iZito](https://www.izito.com/)
7. [Startpage](https://www.startpage.com/)
8. [Swisscows.ch](https://swisscows.ch/)
9. [Yahoo Advanced Web Search](https://search.yahoo.com/web/advanced)
10. [Yippy](https://www.yippy.com/)
----

### Scientific search engines
1. [Advanced Patent Search](https://books.google.com/advanced_patent_search)
2. [Elsevier](https://www.elsevier.com)
3. [Google Scholar](https://scholar.google.com)
4. [Science.gov Search](https://www.science.gov/scigov/desktop/en/search.html)
5. [scitopia.org](http://ww12.scitopia.org)
6. [WorldWideScience](https://worldwidescience.org)
----

### Public Sources
1. [Data Catalog](https://datacatalog.worldbank.org)
2. [Death Check Records Search](https://www.melissa.com/v2/lookups/deathcheck)
3. [FollowTheMoney](https://www.followthemoney.org)
4. [OpenSecrets](http://www.opensecrets.org)
5. [Political MoneyLine](http://www.politicalmoneyline.com)
6. [Public Records](https://www.brbpub.com)
7. [KVK - Kamer van Koophandel](https://www.kvk.nl)
8. [Delpher (Nederland)](https://www.delpher.nl/nl/kranten?cid=cn:NL-S-NB-Kranten_c:ps_sc:adwords&gclid=EAIaIQobChMI8pj9ksWN6AIVzZ13Ch1gMQxxEAEYASAAEgK7HvD_BwE)
9. [GovData (Deutschland)](https://www.govdata.de)
----

### Country searches
1. [WHO](https://www.who.int/countries/en)
2. [Refworld](https://www.refworld.org/type,COUNTRYPROF,,,,,0.html)
3. [The World Bank](https://data.worldbank.org/country)
4. [FAO United Nations](http://www.fao.org/countryprofiles/en)
5. [BBC Country Profiles](http://news.bbc.co.uk/2/hi/country_profiles/default.stm)
6. [Country Insights](https://www.international.gc.ca/cil-cai/country_insights-apercus_pays/countryinsights-apercuspays.aspx?lang=eng)
7. [globalEDGE](https://globaledge.msu.edu)
8. [Gov.uk Country Information](https://www.gov.uk/government/collections/country-policy-and-information-notes)
9. [Routledge](https://www.routledge.com/reference/collections/8789/18864)
10. [The World Factbook CIA](https://www.cia.gov/library/publications/the-world-factbook/index.html)
----

### Domain and IP search
1. [analyzeid](http://analyzeid.com/)
2. [Daily DNS changes](http://dailychanges.domaintools.com/)
3. [Domain Crawler](http://www.domaincrawler.com/)
4. [Domain Dossier](http://centralops.net/co/DomainDossier.aspx)
5. [domainiq](https://www.domainiq.com/)
6. [Easywhois.com](https://www.easywhois.com/)
7. [ViewDNS.info](https://viewdns.info/)
8. [Website Informer](https://website.informer.com/)
9. [Who.is](https://who.is/)
10. [Whoisology](https://whoisology.com/)
----

### Email search & verification
1. [Email Checker Tool](https://www.melissa.com/v2/lookups/emailcheck/email/)
2. [Email-validator.net](https://www.email-validator.net/)
3. [Emailrep.io](https://emailrep.io/)
4. [Free Email Search](http://www.reversegenie.com/email.php)
5. [Hunter](https://hunter.io/)
6. [MailDB](https://maildb.io/)
7. [Mailshunt.com](https://mailshunt.com/)
8. [MailTester.com](https://mailtester.com/testmail.php)
9. [Skymem.info](http://www.skymem.info/)
10. [Verify Email Address Online](https://verify-email.org/)
11. [VoilaNorbert](https://www.voilanorbert.com/)
----

### People search
1. [Familytreenow](http://www.familytreenow.com/)
2. [PeekYou](https://www.peekyou.com/)
3. [Pipl](https://pipl.com/)
4. [Radaris](https://radaris.com/)
5. [Spokeo](https://www.spokeo.com/)
6. [ThatsThem](https://thatsthem.com/reverse-email-lookup)
7. [Ussearch](https://www.ussearch.com/)
8. [Webmii](https://webmii.com/)
9. [zaba search](https://www.zabasearch.com/)
10. [Zoominfo.com](https://www.zoominfo.com/people_directory/professional_profile/A-0-0)
----

### Training
1. [OSINTCurio.us](https://osintcurio.us)
2. [AutomatingOSINT.com](https://register.automatingosint.com/)
3. [IntelTechniques](https://inteltechniques.com/)
4. [Netbootcamp](https://netbootcamp.org/trainingprogram/)
5. [SANS SEC487](https://www.sans.org/course/open-source-intelligence-gathering)
6. [OSINT Pathfinder Training Programme](http://arnoreuser.com/osint-pathfinder/)
7. [Plessas.net](https://plessas.net/online-training)
----

