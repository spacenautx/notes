### Reddit and other communities
1. [r/RBI](https://www.reddit.com/r/RBI/)
2. [r/whatisthisthing](https://www.reddit.com/r/whatisthisthing/)
3. [r/traceanobject](https://www.reddit.com/r/TraceAnObject/)
4. [Open Source Intelligence • r/OSINT](https://www.reddit.com/r/OSINT/)
5. [r/catfish](https://www.reddit.com/r/catfish/)
6. [bellingcat](https://www.bellingcat.com/)
7. [OSINT Essentials](https://www.osintessentials.com/)
----

### Maps and Locations
1. [Mapillary](https://www.mapillary.com/)
2. [OpenStreetMap](http://www.openstreetmap.org/)
3. [http://wikimapia.org/](http://wikimapia.org/)
4. [TRAVEL with DRONE](https://travelwithdrone.com/)
----

### Misinformation Tools
1. [Bot Electioneering Volume](https://botometer.osome.iu.edu/bev/)
2. [Hoaxy: How claims spread online](https://hoaxy.osome.iu.edu/)
3. [BotSlayer](https://osome.iu.edu/tools/botslayer)
4. [Social Media Echo Chamber](https://osome.iu.edu/demos/echo/)
5. [Bot Sentinel Dashboard ‹ Bot Sentinel](https://botsentinel.com/)
6. [Botometer by OSoMe](https://botometer.osome.iu.edu/)
7. [CoVaxxy](https://osome.iu.edu/tools/covaxxy)
----

### Misc Tools
1. [Where to begin? | OSINT Essentials](https://www.osintessentials.com/starter-tool)
2. [Internet Archive: Wayback Machine](https://archive.org/web/)
3. [License Plates of the World](http://www.worldlicenseplates.com/)
4. [Small Arms Survey - The Weapons ID Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
5. [Uniforminsignia](http://www.uniforminsignia.org/)
6. [Railways of the World](https://www.sinfin.net/railways/world/index.html)
7. [Boardreader](http://boardreader.com/)
8. [How Many of Me](http://howmanyofme.com/)
9. [Google Translator](https://addons.mozilla.org/en-US/firefox/addon/google-translator-for-firefox/)
10. [fastpeoplesearch](https://www.fastpeoplesearch.com/)
11. [Thats them](https://thatsthem.com/)
12. [Grabify IP Logger & URL Shortener](https://grabify.link/)
----

### Image searching
1. [Yandex Image search](https://yandex.ru/images/)
2. [remove.bg](http://remove.bg/)
3. [TinEye](https://tineye.com/)
4. [InVid- Logo detector](http://logos.iti.gr/logos/)
5. [A Comparison of Reverse Image Searching Platforms | Security Research](https://www.domaintools.com/resources/blog/a-brief-comparison-of-reverse-image-searching-platforms?fbclid=IwAR3u9Ja5MkXha2wPvF_lwl9YcfOD4X9lNU44UY7dPffDYzQJydNKtK1xCPo)
----

