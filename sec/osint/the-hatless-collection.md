### Discussion Forums
1. [osint.team](https://osint.team/home)
2. [r/InfoSecNews](https://www.reddit.com/r/InfoSecNews/)
3. [r/OSINT](https://www.reddit.com/r/OSINT/)
4. [r/RBI](https://www.reddit.com/r/RBI/)
5. [r/TraceAnObject](https://www.reddit.com/r/TraceAnObject/)
----

### Discord & Slack
1. [Bellingcat Discord](https://discord.gg/nTaNPmz)
2. [Independent OSINT Investigations](https://discord.gg/2DGJ2EC)
3. [OSINT Editor Discord](https://discord.gg/M5pk9rE)
4. [Project Owl Discord](https://discord.gg/projectowl)
5. [Searchlight Lab Discord](http://discord.gg/BtFpNJ2)
6. [The Many Hats Club Discord](https://discord.gg/infosec)
7. [Trace Labs Slack](https://tracelabs.slack.com)
8. [The OSINTion Discord](https://discord.gg/p78TTGa)
----

### #OSINTforGood Sites | Groups
1. [Battling Against Demeaning & Abusive Selfie Sharing -](https://badassarmy.org/)
2. [CITIZEN EVIDENCE LAB - Amnest Intl](https://citizenevidence.org/)
3. [Crisismapping.ning.com](http://crisismapping.ning.com/)
4. [Humanityroad.org](https://www.humanityroad.org/)
5. [ILF - The Innocent Lives Foundation](https://www.innocentlivesfoundation.org/)
6. [Informnapalm.org](https://informnapalm.org/en/)
7. [Locate.international](https://locate.international/)
8. [NCMEC](https://www.missingkids.org/)
9. [NCPTF.org](https://www.ncptf.org/)
10. [Op Underground Railroad](https://ourrescue.org/)
11. [Stop Child Abuse – Trace an Object](https://www.europol.europa.eu/stopchildabuse)
12. [Stopthetraffik.org](https://www.stopthetraffik.org/)
13. [Trace Labs](https://www.tracelabs.org/)
----

### News/Blogs
1. [bellingcat.com](https://bellingcat.com)
2. [Benjamin Strick Blog](https://benjaminstrick.com/blog/)
3. [Cyber-Cops Blog](https://cyber-cops.com/blog)
4. [Dan Nemec Blog](https://blog.nem.ec/)
5. [Dfir.blog](https://dfir.blog/)
6. [Exposing the Invisible](https://exposingtheinvisible.org/)
7. [Fiete Stegers - Medium](https://medium.com/@stegers)
8. [Hakin9 Blog](https://hakin9.org/blog-2/)
9. [Hunchly Articles](https://www.hunch.ly/osint-articles/)
10. [InfoSecSherpa's Nuzzel Newsletter](https://nuzzel.com/InfoSecSherpa)
11. [Key Findings Blog - MW OSINT](https://keyfindings.blog/)
12. [NixIntel](https://nixintel.info/)
13. [Null Byte - Medium](https://medium.com/@NullByteWht)
14. [Offensive OSINT](https://www.offensiveosint.io/)
15. [OSINT Combine Blog](https://www.osintcombine.com/blog)
16. [OSINTCurio.us](https://osintcurio.us/)
17. [Osinteditor.com](https://www.osinteditor.com/)
18. [osintme.com](https://www.osintme.com/)
19. [Petro Cherkasets - Medium](https://medium.com/@Peter_UXer)
20. [Secjuice](https://www.secjuice.com/)
21. [Sector035 & Week In OSINT](https://sector035.nl/)
22. [SecurityTrails Blog](https://securitytrails.com/blog)
23. [The OSINT Journal](http://www.theosintjournal.org/)
24. [Trace Labs](https://www.tracelabs.org/)
25. [WebBreacher's Hacking and Hiking Blog](https://webbreacher.com/category/infosec/)
26. [Wojciech - Offensive OSINT](https://www.offensiveosint.io/author/wojciech/)
27. [Zerotrust.ghost.io](https://zerotrust.ghost.io/)
----

### Training | How-To | Guides
1. [AutomatingOSINT](https://register.automatingosint.com/)
2. [Building an OSINT box | Chris J](https://www.rattis.net/2020/03/16/building-an-osint-box-based-on-open-source-intelligence-techniques-7th-edition/)
3. [Cybrary - OSINT Fundamentals](https://www.cybrary.it/course/osint-fundamentals/)
4. [gerryguy311 List Conf/Training/Webinars](https://github.com/gerryguy311/CyberProfDevelopmentCovidResources/blob/master/README.md)
5. [Hakin9 Online Courses](https://hakin9.org/online-courses-2/)
6. [OSINT Combine - Online OSINT Academy](https://www.osintcombine.com/online-osint-academy)
7. [SANS - Info Security Training](https://www.sans.org/)
8. [The OSINTION](https://www.theosintion.com/)
9. [Toddington](https://www.toddington.com/)
10. [Trace Labs OSINT VM Install/Guide](https://www.tracelabs.org/trace-labs-osint-vm/)
----

### Videos/Playlists
1. [0x4rkØ - YouTube](https://www.youtube.com/c/0x4rk%C3%98/videos)
2. [10 Minute Tips - Osintcurio.us](https://www.youtube.com/playlist?list=PL423I_gHbWUUOs09899rex4t2l5py9YIk)
3. [Adrian Crenshaw OSINT videos](https://www.youtube.com/user/irongeek/search?query=OSINT)
4. [Authentic8 - YouTube](https://www.youtube.com/user/Authentic8TV)
5. [Drop In And Learn - Toddington](https://www.youtube.com/channel/UCAqnnQkeSVTC3ZJ7urNiD8Q)
6. [Kali Linux - OSINT tools playlist](https://www.youtube.com/playlist?list=PL0A5SH4w3NaIBKahXMaO29uToGLn3dARF)
7. [Layer 8 Conference](https://www.youtube.com/channel/UCynWOUeHAOflEQtJnrZpkNA)
8. [Null Byte](https://www.youtube.com/channel/UCgTNupxATBfWmfehv21ym-g)
9. [OSINT Curious](https://www.youtube.com/channel/UCjzceWf-OT3ImIKztzGkipA/videos)
10. [Penn State World Campus Tech Club](https://www.youtube.com/channel/UCdtHf4HX8lzKcxN8D4qfa4g/videos)
11. [SANS Institute](https://www.youtube.com/channel/UC2uPNhGken-ogEpJDi4ly6w)
12. [SCSP OSINT Tools Series - YouTube](https://www.youtube.com/playlist?list=PL7yUP1guJz7fZNfZM-zkUieKSeA1TCG2S)
13. [Trace Labs - YouTube](https://www.youtube.com/channel/UCezKbcbnYtrwRXfGzgQMI3w)
----

### Challenges/CTFs
1. [Aviation Privacy Treasure Hunt CTF](https://aviationtreasurehunt.ctfd.io/login?next=%2Fchallenges%3F)
2. [CyberSoc CTF](https://ctf.cybersoc.wales/)
3. [geoguessr](https://geoguessr.com/)
4. [Google News Initiative Weekly Challenge](https://docs.google.com/forms/d/e/1FAIpQLSfBZqS-JefNzGXmKqFtBpagaERM9v3C0R9RRvHAtnKWrgxblw/viewform)
5. [HackTheBox CTFs](https://ctf.hackthebox.eu/)
6. [Intelligence CTF](https://ictf.io/)
7. [NMP Hackathon (annual)](https://www.missingpersonshackathon.com.au/home)
8. [OhSINT - TryHackMe](https://tryhackme.com/room/ohsint)
9. [osintquiz email game](https://www.reddit.com/r/OSINT/comments/ekcp5b/osint_quiz/)
10. [PictureGame](https://www.reddit.com/r/PictureGame/)
11. [Sourcing.Games](https://www.sourcing.games/)
12. [Trace Labs Events | Eventbrite](https://www.eventbrite.ca/o/trace-labs-18447984222)
13. [TryHackMe](https://tryhackme.com/room/ohsint)
14. [Verif!cation Quiz Bot](https://twitter.com/quiztime)
15. [WhatIsThisThing](https://www.reddit.com/r/whatisthisthing/)
----

### Investigative Tools/Resources Collections
1. [2019 OSINT Guide](https://www.randhome.io/blog/2019/01/05/2019-osint-guide/)
2. [AML Toolbox - Travis Birch](https://start.me/p/rxeRqr/aml-toolbox)
3. [Andy Black UK OSINT Toolkit](https://www.andyblackassociates.co.uk/resources-andy-black-associates/osint-toolkit/)
4. [AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
5. [Aware Online OSINT tools](https://www.aware-online.com/en/osint-tools/)
6. [Bellingcat OSINT landscape](https://start.me/p/ELXoK8/bellingcat-osint-landscape)
7. [Bellingcat's Online Investigation Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit#)
8. [CTI Start.me Paranoid_ch1ck](https://start.me/p/rxRbpo/ti?locale=en)
9. [CyberSecStu: Set it on 🔥child -OSINT for Finding People](https://docs.google.com/spreadsheets/d/1JxBbMt4JvGr--G0Pkl3jP9VDTBunR2uD3_faZXDvhxc/edit#gid=603724104)
10. [Digintel - CNTY USA](https://start.me/p/kxMBv9/cnty-usa)
11. [Digintel OSINT Start.me](https://start.me/p/ZME8nR/osint)
12. [Digintel Toolkit](https://start.me/p/W1AXYo/toolkit)
13. [Exploit Database - Google Hacking](https://www.exploit-db.com/google-hacking-database)
14. [Google Advanced Search Operators](https://docs.google.com/document/d/1ydVaJJeL1EYbWtlfj9TPfBTE5IBADkQfZrQaBZxqXGs/edit)
15. [Google Custom Searches CSE Utopia](https://start.me/p/EL84Km/cse-utopia)
16. [Hun-OSINT Start.me](https://start.me/p/kxGLzd/hun-osint)
17. [i-intelligence handbook (2020)](https://i-intelligence.eu/uploads/public-documents/OSINT_Handbook_2020.pdf)
18. [Intelligence X Tools](https://intelx.io/tools)
19. [inteltechniques OSINT cheat sheet](https://inteltechniques.com/JE/OSINT_Packet_2019.pdf)
20. [jivoi/awesome-osint](https://github.com/jivoi/awesome-osint)
21. [Lorand Bodo Start.me](https://start.me/p/7kxyy2/osint-tools-curated-by-lorand-bodo)
22. [NixIntel Start.Me](https://start.me/p/rx6Qj8/start-page)
23. [Offensive Security Cheatsheet](https://cheatsheet.haax.fr/)
24. [Online Research Cheat Sheets - Toddington](https://www.toddington.com/resources/cheat-sheets/)
25. [OSINT 🔎 STASH](https://osint.best/)
26. [OSINT Bookmarks - OSINT Combine](https://www.osintcombine.com/osint-bookmarks)
27. [Osint Curious OSINT Resource List](https://docs.google.com/document/d/14li22wAG2Wh2y0UhgBjbqEvZJCDsNZY8vpUAJ_jJ5X8/edit)
28. [OSINT Framework](https://osintframework.com/)
29. [OSINT Techniques Tools](https://www.osinttechniques.com/osint-tools.html)
30. [Osint.link](https://osint.link/)
31. [OSINT54 start.me](https://start.me/p/q6naJo/osint-links)
32. [OSINTgeek | Tools](https://osintgeek.de/tools)
33. [Osintia Social Media](https://start.me/p/4K0DXg/social-media)
34. [Ph055a/OSINT_Collection](https://github.com/Ph055a/OSINT_Collection)
35. [Reuser's Repertorium](http://rr.reuser.biz/#company%20informationInternational)
36. [Sector035's Links](https://sector035.nl/links)
37. [sinwindie/OSINT](https://github.com/sinwindie/osint)
38. [Technisette Tools](https://www.technisette.com/p/tools)
39. [The Cyber Post OSINT Tools & Resources](https://thecyberpost.com/open-source-intelligence-osint-tools/)
40. [Toddington OSINT and Online Research Resources](https://www.toddington.com/resources/free-osint-resources-open-source-intelligence-search-tools-research-tools-online-investigation/)
41. [UK-OSINT](https://www.uk-osint.net/)
42. [Verification Toolset - Julia Bayer](https://start.me/p/ZGAzN7/verification-toolset)
----

### Podcasts
1. [bellingcatPodcasts - bellingcat](https://www.bellingcat.com/category/resources/podcasts/)
2. [‎Cyber Warrior Princess](https://podcasts.apple.com/us/podcast/cyber-warrior-princess/id1447704787)
3. [CYBER: A Hacking Podcast](https://www.vice.com/en_us/article/59vpnx/introducing-cyber-a-hacking-podcast-by-motherboard)
4. [Darknetdiaries.com](https://darknetdiaries.com/)
5. [Hackablepodcast.com](https://hackablepodcast.com/)
6. [Hacking Humans Podcast](https://thecyberwire.com/podcasts/hacking-humans)
7. [Jake Creps](https://jakecreps.com/)
8. [Krebs on Security](https://krebsonsecurity.com/)
9. [Layer 8 Podcast](https://anchor.fm/layer-8-podcast)
10. [Malicious Life](https://malicious.life/)
11. [OSINTCurious Podcast](https://anchor.fm/osintcurious)
12. [‎PI Perspectives on Apple Podcasts](https://podcasts.apple.com/us/podcast/pi-perspectives/id1481502619?at=1l3vwYf)
13. [shadowdragon.io](https://podcast.shadowdragon.io/)
14. [social-engineer podcast](https://www.social-engineer.org/category/podcast/)
15. [Techagainstterrorism.fm](https://www.techagainstterrorism.fm/)
16. [‎The InfoSec & OSINT Show](https://podcasts.apple.com/us/podcast/the-infosec-osint-show/id1507803617)
17. [The Investigation Game Podcast — Workman Forensics](https://www.workmanforensics.com/the-podcast)
18. [The Many Hats Club](https://themanyhats.club/tag/episodes/)
19. [The Privacy, Security, & OSINT Show](https://inteltechniques.com/podcast.html)
20. [The World of Intelligence Podcast](https://www.janes.com/podcast)
21. [‎Tribe of Hackers Podcast](https://podcasts.apple.com/us/podcast/tribe-of-hackers-podcast/id1510526299)
----

### Webcasts
1. [OSINT Curious Webcasts](https://osintcurio.us/osintvideosandpodcasts/)
2. [Drop In and Learn Webinars - Toddington](https://www.toddington.com/drop-in-and-learn-webcasts/)
----

