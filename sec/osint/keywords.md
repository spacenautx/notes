### Keywords SEO
1. [Keyword Tool***](http://keywordtool.io/)
2. [Wordtracker](https://app.wordtracker.com/)
3. [Keyword Discovery](http://www.keyworddiscovery.com/)
4. [RecruitEm Job Ad Optimiser HR Keywords](https://recruitin.net/job-ad-optimiser/)
5. [Soovle Wordcloud](http://soovle.com/)
6. [SpyFu](https://www.spyfu.com)
7. [Ubersuggest: your friendly keyword tool (csv download) ***](https://ubersuggest.io/)
8. [Wort Suche.com: Kreuzworträtsel Hilfe](http://wortsuche.com/)
9. [Search Volume Tool - Keywords for Google SEO](https://searchvolume.io/)
10. [Keyword Explorer: Moz's Keyword Research Tool](https://moz.com/explorer)
11. [Related Words](https://relatedwords.org/)
12. [https://synoniemen.net/](https://synoniemen.net/)
----

### Wordcloud Wortwolken
1. [@WordArt](https://wordart.com/)
2. [Word Cloud Generator](https://www.jasondavies.com/wordcloud/)
3. [wortwolken.com](https://www.wortwolken.com/)
4. [Create word clouds – WordItOut](https://worditout.com/word-cloud/create)
----

### Plagiarism & Stylometry
1. [Copyscape Plagiarism Checker](http://www.copyscape.com/)
2. [Plagiarism Checker](https://smallseotools.com/plagiarism-checker/)
3. [evllabs/JGAAP](https://github.com/evllabs/JGAAP)
4. [psal/jstylo](https://github.com/psal/jstylo)
5. [Hacker Factor: Gender Guesser](https://www.hackerfactor.com/GenderGuesser.php#Analyze)
6. [JStylo-Anonymouth Authorship Attribution](https://psal.cs.drexel.edu/index.php/JStylo-Anonymouth)
7. [AgeAnalyzer - Determine the age of a blogger](http://ageanalyzer.com/)
8. [Citethisforme.com](http://www.citethisforme.com/welcome)
9. [Konsequenzen von Plagiat - was dir alles drohen kann](https://www.scribbr.de/plagiat/konsequenzen/)
10. [XTools Wikipedia Tool](https://xtools.wmflabs.org/)
11. [Wikipedia:Checkuser – Wikipedia](https://de.wikipedia.org/wiki/Wikipedia:Checkuser)
----

### Language resources
1. [Open Language Archive OLAC](http://olac.ldc.upenn.edu/)
2. [Ethnologue.com](https://www.ethnologue.com/)
----

### Glossary Abbreviations Acronyms
1. [AGA - Glossary of Fraud Terms](https://www.agacgfm.org/Intergov/Fraud-Prevention/Resources-Best-Practices/Glossary-of-Terms.aspx)
2. [AML Glossary of AML Terms](https://www.acams.org/aml-glossary/)
3. [Dictionary of Banking Terms and Phrases](https://www.helpwithmybank.gov/dictionary/index-dictionary.html)
4. [Drug Slang (PDF)](https://www.snohd.org/DocumentCenter/View/2516/Drug_Names_Slang_2019_05_09?bidId=)
5. [Drug Slang ndews.umd.edu (PDF)](https://ndews.umd.edu/sites/ndews.umd.edu/files/dea-drug-slang-terms-and-code-words-july2018.pdf)
6. [Drug Slang Word Glossary | Banyan Detox Stuart](https://www.banyanstuart.com/slang-glossary/)
7. [Glossary of Human-Trafficking Acronyms and TermsAmerican Bar Association TMAmerican Bar AssociationAmerican Bar Association TM](https://www.americanbar.org/groups/judicial/publications/judges_journal/2013/winter/glossary_of_humantrafficking_acronyms_and_terms/)
8. [Glossary of Legal Terms](https://seconddistrictcourt.nmcourts.gov/glossary-of-legal-terms.aspx)
9. [Industry Acronyms | IPC](https://www.ipc.org/ContentPage.aspx?pageid=Industry-Acronyms)
10. [List of All 50 US State Abbreviations](https://abbreviations.yourdictionary.com/articles/state-abbrev.html)
11. [List of Texting & Chat Abbreviations](https://www.webopedia.com/quick_ref/textmessageabbreviations.asp)
12. [Logistics Glossary](https://www.logisticsglossary.com/)
13. [Stolen Asset Recovery Terminology - The Sentry](https://thesentry.org/terminology/)
14. [TheFreeDictionary.com](https://acronyms.thefreedictionary.com/)
----

### LoremIpsum Generator
1. [Loremipsum.io](https://loremipsum.io/)
2. [How Do I Cite Sources?](https://www.plagiarism.org/article/how-do-i-cite-sources)
----

### Translation Tools (side by side)
1. [@DeepL Translator](https://www.deepl.com)
2. [Bing Microsoft Translator](https://www.bing.com/translator)
3. [Google 2lingual  Search](http://2lingual.com)
4. [Google Translate](https://translate.google.com/)
5. [Yandex.Translate](https://translate.yandex.com/)
----

### Transliteration Transcription
1. [Text Transliteration for SMS Messages](https://www.textmagic.com/free-tools/text-transliteration-tool)
2. [Google Input Tools - Transliteration](https://www.google.com/inputtools/services/features/transliteration.html)
3. [Transliterating Arabic to English in One Step](https://stevemorse.org/arabic/ara2eng.html)
4. [Arabic Transliteration - Online Romanization - Latin Script - LEXILOGOS](https://www.lexilogos.com/keyboard/arabic_latin.htm)
5. [http://mylanguages.org/arabic_romanization.php](http://mylanguages.org/arabic_romanization.php)
----

### Translation Languages
1. [@Languages Spoken in Each Country of the World](https://www.infoplease.com/world/countries/languages-spoken-in-each-country-of-the-world)
2. [Babelfish](http://babelfish.altavista.com/)
3. [Cevirsozluk.com](https://cevirsozluk.com/)
4. [Dict](http://www.dict.cc/)
5. [dict.tu-chemnitz](http://dict.tu-chemnitz.de/)
6. [English to Indonesian Translation and Dictionary /Bahasa](http://www.indotranslate.com/)
7. [FreeTranslation.com](https://www.freetranslation.com/)
8. [Google iTools  Translate Web](http://itools.com/tool/google-translate-web-page-translator)
9. [Leo](http://www.leo.org/)
10. [Linguee (Deepl)](http://www.linguee.de/)
11. [MDBG English to Chinese dictionary](https://www.mdbg.net/chinese/dictionary)
12. [PONS Deutsch » Englisch-Wörterbuch](https://de.pons.com/%C3%BCbersetzung/deutsch-englisch)
13. [PROMT Übersetzer](http://www.online-translator.com/)
14. [TextRanch](https://textranch.com/)
15. [Translate.eu](http://translate.eu/)
16. [Word Reference](https://www.wordreference.com/)
----

### Dictionary
1. [Dictionary by Merriam-Webster](https://www.merriam-webster.com/)
2. [Cambridge Dictionary](https://dictionary.cambridge.org/)
3. [Dictionary.com](http://www.dictionary.com/)
4. [Larousse.fr](http://www.larousse.fr/)
5. [Online Business Dictionary](http://www.businessdictionary.com/)
6. [Online Wörterbuch von Langenscheidt](https://de.langenscheidt.com/)
7. [Dict.cc](https://www.dict.cc/)
8. [ludwig.guru Linguistic search engine and smart translator](https://ludwig.guru/)
9. [قاموس ومعجم المعاني متعدد اللغات والمجالات](https://www.almaany.com/)
10. [Urban Dictionary](https://www.urbandictionary.com/)
----

### Translating Management Titles - Companies
1. [IHK HH German US UK French](https://www.hk24.de/produktmarken/beratung-service/recht_und_steuern/wirtschaftsrecht/gesellschaftsrecht/uebersetzung-gesellschaftsrechtliche-bezeichnungen/1156900)
2. [Wer heißt wo wie?: Andere Länder, andere Titel](https://www.handelsblatt.com/karriere/wer-heisst-wo-wie-andere-laender-andere-titel/2511916.html?ticket=ST-4672421-jzDjViUfeiXhOl0Ea4ej-ap3)
----

### Language Courses
1. [https://www.mosalingua.com/de](https://www.mosalingua.com/de)
2. [https://survivalphrases.com/](https://survivalphrases.com/)
3. [Streetsmartbrazil.com](https://streetsmartbrazil.com)
4. [How to Learn Cyrillic in 4 Easy Steps | FluentU Russian](https://www.fluentu.com/blog/russian/how-to-learn-cyrillic/)
5. [Learning the Russian Alphabet: As Easy as Д Б Ж! | FluentU Russian](https://www.fluentu.com/blog/russian/learn-russian-alphabet/)
6. [Поём алфавит. Учим буквы русского алфавита - YouTube](https://www.youtube.com/watch?v=HT9I2HmF0a4)
7. [Rim Kahya - Quora](https://www.quora.com/profile/Rim-Kahya)
----

### Thesaurus
1. [Urban Thesaurus - Find Synonyms for Slang Words](https://urbanthesaurus.org/synonyms/look%20up%20any%20word%3A)
2. [Merriam-Webster Thesaurus](https://www.merriam-webster.com/)
----

### Structure of Names | Naming conventions
1. [Law Enforcement Guide to International Names](https://info.publicintelligence.net/ROCICInternationalNames.pdf)
2. [UK Guide to Naming and Naming Practices](https://www.fbiic.gov/public/2008/nov/Naming_practice_guide_UK_2006.pdf)
3. [The structure of Names in Kosovo](http://ask.rks-gov.net/media/3820/names-and-surnames.pdf)
4. [Indian Naming conventions](http://learningindia.in/indian-naming-conventions/)
5. [russlandjournal.de/russisch-lernen/russische-namen/](https://www.russlandjournal.de/russisch-lernen/russische-namen/)
6. [The traditional structure of Russian personal names](https://www.justrussian.com/news/article/russian-names.html)
7. [The Structure of Persian Names](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.717.1899&rep=rep1&type=pdf)
8. [Romanian Personal Names - Slavic Cataloging Manual](https://sites.google.com/site/seesscm/romanian-personal-names)
9. [The Most Common Last Name in Every Country - NetCredit Blog](https://www.netcredit.com/blog/most-common-name-country/)
----

### UNIT CONVERTER | CALC DATE TIME  CALENDAR
1. [Current Millis](https://currentmillis.com/)
2. [Dezimalstunden aus Zeitwerten berechnen](https://www.zeiterfassungonline.com/umrechner.html)
3. [Epochconverter.com](https://www.epochconverter.com/)
4. [Julian Date Converter](http://aa.usno.navy.mil/data/docs/JulianDate.php)
5. [PTB Uhr](https://uhr.ptb.de/)
6. [SunCalc sun position and sunlight phases calculator](http://suncalc.net/#/51.508,-0.125,2/2018.01.19/19:22)
7. [Zeit-Einheiten Jahre, Tage, Stunden, Minuten, Sekunden umrechnen](http://www.umrechnung.org/zeit-einheiten-umrechnen/wochen-tage-minuten-sekunden-umrechnen.htm)
8. [Zeitrechner von bis, Stunden und Minuten Rechner • PION](https://www.pion.at/service/zeitrechner/)
----

### Time Zone
1. [Time Zone Converter: Calculate time difference between time zones in the world](https://www.zeitverschiebung.net/en/)
2. [Time.is ***](https://time.is/)
3. [Timeanddate](https://www.timeanddate.de)
4. [World Clock & Time Converter](http://www.worldtimebuddy.com/pdt-to-gmt-converter)
5. [Every Time Zone Converter](https://everytimezone.com/)
----

### Calendar
1. [@2020 World Holidays](http://portalseven.com/calendar/)
2. [Iran Chamber Society: Iranian Calendar Converter](http://www.iranchamber.com/calendar/converter/iranian_calendar_converter.php)
----

### %
1. [mehrwertsteuerrechner.de](https://www.mehrwertsteuerrechner.de/)
2. [Prozentrechner](https://www.blitzrechner.de/prozent/)
----

### Mini Tools
1. [Abbreviations.com](http://www.abbreviations.com/)
2. [Abkuerzungen (GER)](http://www.abkuerzungen.de/)
3. [allestörungen.de (GER)](http://xn--allestrungen-9ib.de/)
4. [Alphabetizer (GER)](http://alphabetizer.flap.tv/)
5. [Alternative.to](http://www.alternative.to/)
6. [Barcode Reader](https://online-barcode-reader.inliteresearch.com/)
7. [Dateiendungen File extensions (GER)](http://www.endungen.de/)
8. [Random Number Service](https://www.random.org/)
9. [Random words](http://listofrandomwords.com/)
10. [Transcription and Transliteration](http://www.sttmedia.com/transcription)
11. [Universal Leet](http://www.robertecker.com/)
12. [NAICS Code](https://www.census.gov/cgi-bin/sssd/naics/naicsrch)
13. [Sprachkürzel ISO](https://wiki.selfhtml.org/wiki/Sprachkürzel)
14. [Add/Remove Line Breaks – Text Mechanic](http://textmechanic.com/text-tools/basic-text-tools/addremove-line-breaks/)
15. [Find and Replace Text – Text Mechanic](http://textmechanic.com/text-tools/basic-text-tools/find-and-replace-text/)
16. [Juristische Abkuerzungen](http://www.juristische-abkuerzungen.de)
17. [cap.slocked.com](http://cap.slocked.com/)
18. [Abbreviations and acronyms dictionary](https://www.acronymfinder.com/)
19. [Random Generator - Subset of Names](https://www.ultimatesolver.com/en/random-subset)
20. [Abkuerzung.info](https://xn--abkrzung-85a.info/)
----

### Input Tools Keyboard
1. [typeit.org Virtual Keyboard](http://www.typeit.org/)
2. [Google Input Tools- Eingabe von Text in der von Ihnen gewählten Sprache](https://www.google.com/inputtools/try/)
----

### Yandex | CYRILLIC
1. [Yandex English](https://yandex.com/)
2. [Yandex](https://www.yandex.com/)
3. [LEXILOGOS Russian Conversion: Cyrillic <> Latin Alphabet](https://www.lexilogos.com/keyboard/russian_conversion.htm)
4. [Universal online Cyrillic decoder - recover your texts](https://2cyr.com/decode/?lang=en)
----

