### TI Feeds
1. [Anomali Threatstream](https://www.anomali.com/)
2. [AutoFocus Threat Intelligence - Palo Alto Networks](https://www.paloaltonetworks.com/products/secure-the-network/autofocus.html)
3. [c1fapp.com/](https://www.c1fapp.com/)
4. [Collective Intelligence Framework (CIF)](https://csirtgadgets.com/collective-intelligence-framework)
5. [Critical Stack Intel](http://intel.criticalstack.com)
6. [CyberCure](http://cybercure.ai/)
7. [dfirgeek/cabby](https://github.com/eclecticiq/cabby)
8. [EclecticIQ's OpenTAXII](https://www.eclecticiq.com/platform)
9. [ET Intelligence](https://threatintel.proofpoint.com/)
10. [FireEye - iSIGHT (PAID)](https://www.fireeye.com/solutions/isight-cyber-threat-intelligence-subscriptions.html)
11. [hail a taxii](http://hailataxii.com/)
12. [intelgraph.idefense.com (PAID)](http://intelgraph.idefense.com)
13. [IntelMQ - Documentation](https://github.com/certtools/intelmq-feeds-documentation)
14. [International Cyber Counter Intelligence Network](http://www.iccin.com)
15. [MISP/MISP-Taxii-Server](https://github.com/MISP/MISP-Taxii-Server)
16. [Netlab OpenData Project](http://data.netlab.360.com/)
17. [OpenPhish - Phishing Feeds](https://openphish.com/phishing_feeds.html)
18. [opensourcesec/Forager](https://github.com/opensourcesec/Forager)
19. [OPSWAT MetaDefender Cloud | Threat Intelligence Feeds](https://metadefender.opswat.com/threat-intelligence-feeds)
20. [paulpc/nyx](https://github.com/paulpc/nyx)
21. [Recorded Future TI Feeds](https://www.recordedfuture.com/solutions/threat-intelligence-feeds/)
22. [rjmolesa/feeds.opml](https://gist.github.com/rjmolesa/db96afb10e8fc54c06d6)
23. [Secureworks Cyber TI (PAID)](https://www.secureworks.com/capabilities/threat-intelligence/global-threats)
24. [TAXIIProject/libtaxii](https://github.com/TAXIIProject/libtaxii)
25. [The Cyber Threat](http://thecyberthreat.com/cyber-threat-intelligence-feeds/)
26. [Threat Intelligence Review](http://threatintelligencereview.com/)
27. [ThreatTrack](https://www.threattrack.com/)
----

### Fact Checking
1. [Snopes](https://www.snopes.com/)
2. [Factcheck](https://www.factcheck.org/)
3. [Poynter Fact-Checking Archives](https://www.poynter.org/news/fact-checking/)
----

### Orgs / Assns
1. [FS-ISAC : Financial Services - Information Sharing and Analysis Center](https://www.fsisac.com/)
2. [Infragard](https://www.infragard.org/)
3. [Intelligence and National Security Alliance](https://www.insaonline.org/)
4. [Multi-State Information Sharing & Analysis Center](https://www.cisecurity.org/ms-isac/)
----

### Data Breaches / Leaks
1. [Breach Directory — DeHashed](https://www.dehashed.com/data)
2. [Leaked Password Check](https://cybernews.com/password-leak-check/)
3. [Personal Data Leak Checker](https://cybernews.com/personal-data-leak-check/)
4. [SMWYG (Show-Me-What-You-Got)](https://github.com/Viralmaniar/SMWYG-Show-Me-What-You-Got)
----

### Web Browsers
1. [Chromium](https://www.chromium.org/getting-involved/download-chromium)
2. [Brave](https://brave.com/download/)
3. [The Tor Project](https://www.torproject.org/download/)
4. [Google Chrome](https://www.google.com/chrome/)
5. [Firefox](https://www.mozilla.org/en-US/firefox/all/#product-desktop-release)
6. [MS Edge](https://www.microsoft.com/en-us/edge)
7. [Opera](https://www.opera.com/)
8. [OSIRT](http://osirtbrowser.com/)
9. [OnionBrowser](https://github.com/onionbrowser/onionbrowser)
----

### Twitter Peeps
1. [Bellingcat (@bellingcat)](https://twitter.com/bellingcat)
2. [Benjamin Strick (@BenDoBrown)](https://twitter.com/BenDoBrown)
3. [Bruno Mortier (@digintelosint)](https://twitter.com/digintelosint)
4. [Christina Lekati (@christinalekati)](https://twitter.com/christinalekati)
5. [Christopher Hadnagy (@humanhacker)](https://twitter.com/humanhacker)
6. [Cyber Detective (@cyb_detective)](https://twitter.com/cyb_detective)
7. [Dutch OsintGuy (@dutch_osintguy)](https://twitter.com/dutch_osintguy)
8. [Eliot Higgins (@eliothiggins)](https://twitter.com/eliothiggins)
9. [Griffin (@hatless1der)](https://twitter.com/hatless1der)
10. [Hunchly (@hunchly)](https://twitter.com/hunchly)
11. [Julia Bayer (@bayer_julia)](https://twitter.com/bayer_julia)
12. [Justin Seitz (@jms_dot_py)](https://twitter.com/jms_dot_py)
13. [kirbstr (@kirbstr)](https://twitter.com/kirbstr)
14. [Micah (@WebBreacher)](https://twitter.com/WebBreacher)
15. [Michael James(@Ginsberg5150)](https://twitter.com/ginsberg5150)
16. [nixintel (@nixintel)](https://twitter.com/nixintel)
17. [OSINT Research (@osint_research)](https://twitter.com/osint_research)
18. [OSINT Techniques (@OSINTtechniques)](https://twitter.com/osinttechniques)
19. [osint_unleashed (@UnleashedOsint)](https://twitter.com/unleashedosint)
20. [OSINTCurious (@OsintCurious)](https://twitter.com/osintcurious)
21. [technisette (@technisette)](https://twitter.com/technisette)
22. [The Innocent Lives Foundation (@InnocentOrg)](https://twitter.com/innocentorg)
23. [Trace Labs (@TraceLabs)](https://twitter.com/tracelabs)
24. [The DBIR (@VZDBIR)](https://twitter.com/vzdbir)
----

### OSINT Games
1. [A Google a Day](http://www.agoogleaday.com/)
2. [CyberSoc - Cyber Detective CTF](https://ctf.cybersoc.wales/)
3. [CyberSoc - Cyber Investigator CTF](https://investigator.cybersoc.wales/)
4. [GeoGuessr](https://www.geoguessr.com/)
5. [GeoGuessr (free)](https://www.geoguessr.com/free)
6. [OSINT Challenge](https://courses.thecyberinst.org/courses/osint-challenge)
7. [OSINT Sourcing.Game](https://sourcing.games/game-15/)
8. [Verif!cation Quiz Bot (@quiztime)](https://twitter.com/quiztime)
----

### Data Searching
1. [s0md3v/Photon (OSINT crawler)](https://github.com/s0md3v/Photon)
----

### Login Portals Search
1. [Login-DB](http://login-db.co.uk/)
2. [LoginFan](https://loginfan.com)
3. [LoginRadar](https://loginradar.com)
4. [LoginSimple](https://loginsimple.com/)
----

### Misc
1. [ATT&CK™ Navigator - Enterprise](https://mitre.github.io/attack-navigator/enterprise/)
2. [ATT&CK™ Navigator - Mobile](https://mitre.github.io/attack-navigator/mobile/)
3. [mitre/attack-navigator](https://github.com/mitre/attack-navigator)
4. [Parse a User Agent String](https://developers.whatismybrowser.com/useragents/parse/)
5. [UserAgentString](http://useragentstring.com/)
----

### Online Repositories Search
1. [Awesome AWS S3 Security](https://github.com/mxm0z/awesome-sec-s3)
2. [CloudScraper](https://github.com/jordanpotti/CloudScraper)
3. [dpaste](https://dpaste.com/)
4. [dxa4481/truffleHog](https://github.com/dxa4481/truffleHog)
5. [Grayhat Warfare](https://buckets.grayhatwarfare.com/)
6. [grep.app](https://grep.app/)
7. [Pastebin](https://pastebin.com/)
8. [Pastr.io](https://pastr.io/)
9. [s0md3v/Zen](https://github.com/s0md3v/Zen)
10. [Source Code Search Engine](https://publicwww.com/)
11. [Sourcegraph](https://sourcegraph.com/search)
12. [Throwbin.io](https://throwbin.io/)
----

### Phishing
1. [isitPhishing](https://isitphishing.org/)
2. [PhishTank](https://www.phishtank.com/)
----

### Spam
1. [SpamCop](https://www.spamcop.net/bl.shtml)
2. [SpamHaus](https://www.spamhaus.org/)
----

### Geolocation
1. [BatchGeo: Create an interactive map from your data](https://batchgeo.com/)
2. [Bing Maps](https://www.bing.com/maps)
3. [Creepy by ilektrojohn](http://www.geocreepy.com)
4. [Dual Maps](http://data.mashedworld.com/dualmaps/map.htm)
5. [Dual Maps - Main Page](https://www.mapchannels.com/DualMaps.aspx)
6. [Echosec (PAID)](https://www.echosec.net/)
7. [FireHOL IP Lists](https://iplists.firehol.org/)
8. [Geo IP Lookup](http://geoiplookup.net/ip-lookup/)
9. [Google Maps](https://www.google.com/maps)
10. [IP2Location](https://www.ip2location.com/)
11. [IP2Location](https://www.ip2location.com/demo)
12. [IPdeny IP country blocks](http://www.ipdeny.com/)
13. [IPGeek](http://www.ipgeeks.org/)
14. [IPIP.NET](https://en.ipip.net/ip.html)
15. [Mashed World](http://www.mashedworld.com/)
16. [MaxMind - GeoIP2 Demo](https://www.maxmind.com/en/geoip2-precision-demo)
17. [Maxmind - GeoLite2 Free DBs](https://www.maxmind.com/en/open-source-data-and-api-for-ip-geolocation)
18. [Neustar IP & Geolocation Lookup Tool](https://www.risk.neustar/resources/tools/ip-geolocation-lookup-tool)
19. [WiGLE: Wireless Network Mapping](https://wigle.net/)
20. [Sentinel Hub - EO Browser](https://apps.sentinel-hub.com/eo-browser/)
----

### Research
1. [8ackprotect](https://8ackprotect.com/)
2. [AlienVault OTX](https://www.alienvault.com/open-threat-exchange)
3. [AlienVault OTX DirectConnect API](https://otx.alienvault.com/api)
4. [Blueliv Threat Exchange Network](http://community.blueliv.com/)
5. [Certstream](https://certstream.calidog.io/)
6. [Cyber Analytics Repository Exploration Tool](https://car.mitre.org/caret/#/)
7. [ET Rules](https://rules.emergingthreats.net)
8. [FluidDATA - Podcast Search Engine](https://fluiddata.com/)
9. [Global FTP Search Engine](http://globalfilesearch.com/)
10. [GreyNoise Visualizer](https://viz.greynoise.io/table)
11. [hpHosts](https://hosts-file.net/)
12. [Hurricane Electric Network Looking Glass](https://lg.he.net/)
13. [IBM X-Force Exchange](https://exchange.xforce.ibmcloud.com)
14. [INFOSEC - CERT-PA](https://infosec.cert-pa.it/)
15. [iseek.ai](https://iseek.com/#/web)
16. [Malc0de Database](http://malc0de.com/database/)
17. [Maltego CE](https://www.maltego.com/maltego-community/)
18. [MITRE - CAPEC](https://capec.mitre.org/)
19. [NerdyData](https://nerdydata.com/)
20. [NormShield Services](https://services.normshield.com/)
21. [PulseDive](https://pulsedive.com/)
22. [SANS ICS - InfoSec Reports](https://isc.sans.edu/reports.html)
23. [SANS ICS - InfoSec Tools](https://isc.sans.edu/tools/)
24. [searchcode](https://searchcode.com/)
25. [SpiderFoot](http://spiderfoot.net/)
26. [SpiderFoot HX (PAID)](https://www.spiderfoot.net/hx/)
27. [TC Open](https://www.threatconnect.com/free/)
28. [Team Cymru - Community Services](http://www.team-cymru.com/community-services.html)
29. [Threat Crowd](https://threatcrowd.org/)
30. [Threat Miner](https://threatminer.org/)
----

### Research - Terrorism
1. [FAROS Terrorism Research](https://start.me/p/b57786/faros-terrorism-research)
2. [Lorand Bodo - Terrorism & Radicalisation Research Dashboard](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
3. [OSTER](https://start.me/p/7kmvEK/oster)
----

### Web Archiving
1. [Archive-it.org](https://archive-it.org/)
2. [archive.ph](https://archive.ph/)
3. [Internet Archive: Wayback Machine](https://archive.org/web/web.php)
4. [WebCite](https://webcitation.org/)
----

### Honeypot Stuff
1. [foospidy/HoneyPy](https://github.com/foospidy/HoneyPy)
2. [HoneyDB](https://riskdiscovery.com/honeydb/)
3. [leeberg/BlueHive](https://github.com/leeberg/BlueHive)
4. [Project HoneyPot](https://www.projecthoneypot.org/)
- https://hacklido.com/blog/258-honeypots-the-game-of-traps
----

### Red Team - Misc
1. [aiodnsbrute](https://github.com/blark/aiodnsbrute)
2. [Armitage - Metasploit Cyber Attack Management](http://www.fastandeasyhacking.com/)
3. [CIRT.net - Default Passwords](https://cirt.net/passwords)
4. [CrackStation](https://crackstation.net)
5. [CVE security vulnerability database](https://www.cvedetails.com/)
6. [exploit-db (GHDB)](https://www.exploit-db.com/about-exploit-db/)
7. [Google Hacking Database](https://www.exploit-db.com/google-hacking-database)
8. [hashcat](https://hashcat.net/hashcat/)
9. [Hashkiller.io](https://hashkiller.io/listmanager)
10. [John the Ripper](https://www.openwall.com/john/)
11. [KatzKatz:](https://github.com/xFreed0m/KatzKatz)
12. [masscan](https://github.com/robertdavidgraham/masscan)
13. [md5 cracker](http://www.md5this.com/)
14. [Medusa](http://foofus.net/goons/jmk/medusa/medusa.html)
15. [Metasploit](https://www.metasploit.com/)
16. [Metasploit - Scanner HTTP Auxiliary Modules](https://www.offensive-security.com/metasploit-unleashed/scanner-http-auxiliary-modules/)
17. [ODIN](https://github.com/chrismaddalena/ODIN)
18. [recon-ng](https://github.com/lanmaster53/recon-ng)
19. [social-engineer-toolkit](https://github.com/trustedsec/social-engineer-toolkit)
20. [Sysinternals Suite](https://docs.microsoft.com/en-us/sysinternals/downloads/sysinternals-suite)
21. [thc-hydra](https://github.com/vanhauser-thc/thc-hydra)
22. [The C2 Matrix](https://www.thec2matrix.com/)
23. [The Hackers Arsenal Tools Portal](https://www.toolswatch.org/)
----

### Vuln Research
1. [CIRCL CVE Search](https://cve.circl.lu/)
2. [CVE Details](https://www.cvedetails.com/)
3. [CVE STALKER](https://www.cvestalker.com/all.php)
4. [cve-search](https://github.com/cve-search/cve-search)
5. [CWE](https://cwe.mitre.org/)
6. [Mitre CVE](http://cve.mitre.org)
7. [NIST - NVD](https://nvd.nist.gov/)
----

