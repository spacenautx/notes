### Business General and Leadership
1. [OpenCorporates](https://opencorporates.com/)
2. [Corporation Wiki](https://www.corporationwiki.com/)
3. [LittleSis](https://littlesis.org/)
4. [Salary.com](https://www.salary.com/)
----

### Business Emails
1. [Email Format](https://www.email-format.com)
2. [Hunter](https://hunter.io)
3. [Phonebook.cz](https://phonebook.cz/)
----

### Business Offical Documents
1. [aleph.occrp.org](https://aleph.occrp.org/)
2. [Edgar search](https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK=0000104169&type=10-k&dateb=&owner=exclude&count=40&search_text=)
3. [Find a Tender - UK](https://www.find-tender.service.gov.uk/Search)
4. [Opengovsg.com - Singapore](https://opengovsg.com/)
5. [USAspending.gov](https://www.usaspending.gov/search/)
6. [US Published Application Full-Text Database Boolean Search](http://appft.uspto.gov/netahtml/PTO/search-bool.html)
7. [WIPO](https://patentscope.wipo.int/search/en/search.jsf)
----

### OSINT Automation
1. [Google Alerts](https://www.google.com/alerts)
2. [Talkwalker](https://www.talkwalker.com/alerts)
3. [IFTTT](https://ifttt.com/)
4. [Microsoft Power Automate](https://flow.microsoft.com/en-us/)
5. [Versionista](https://versionista.com/)
6. [Visualping](https://visualping.io/)
----

### Breach Data
1. [Have I been pwned?](https://haveibeenpwned.com)
2. [Intelligence X](https://intelx.io)
3. [DeHashed — #FreeThePassword](https://dehashed.com)
----

