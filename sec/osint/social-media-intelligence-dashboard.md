### General
1. [Photon](https://github.com/s0md3v/Photon)
2. [Recon_NG](https://bitbucket.org/LaNMaSteR53/recon-ng)
3. [Maltego CE](https://www.paterva.com/web7/buy/maltego-clients/maltego-ce.php)
4. [Google Dorks](https://en.wikipedia.org/wiki/Google_hacking)
5. [Recon_NG Guide](https://strikersecurity.com/pdfs/recon-ng-guide.pdf?utm_source=drip&utm_medium=email&utm_campaign=free-reconng-guide&__s=xmsdtksdz1kxvcnouqzp)
6. [GasMask](https://github.com/twelvesec/gasmask)
----

### Instagram
1. [GramSpy](http://gramspy.com/)
2. [BuzzWeb](https://buzzweb.pro/)
3. [InstaLooter](https://github.com/althonos/InstaLooter)
----

### Twitter
1. [Tweet Beaver](https://tweetbeaver.com/)
2. [Twint](https://github.com/twintproject/twint)
3. [TweetLord](https://github.com/snovvcrash/tweetlord)
4. [SocioViz](http://socioviz.net/SNA/eu/sna/login.jsp)
5. [TreeVerse](https://github.com/paulgb/Treeverse/blob/master/README.md?utm_content=buffer33d48&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer#readme)
6. [Trendsmap](https://www.trendsmap.com/)
7. [Twitter-Maltego Tutorial](https://null-byte.wonderhowto.com/how-to/use-maltego-monitor-twitter-for-disinformation-campaigns-0185998/)
8. [Followerwonk](https://followerwonk.com/bio/)
9. [Socialbearing](https://socialbearing.com/)
10. [Mentionmapp.com](https://mentionmapp.com/)
11. [Geochirp.com](http://www.geochirp.com)
12. [Geosocialfootprint.com](http://geosocialfootprint.com/)
----

### LinkedIn
1. [Linkedin2Username](https://github.com/initstring/linkedin2username)
2. [The Endorser](https://github.com/eth0izzle/the-endorser)
----

### Reddit
1. [Reddit Investigator](http://www.redditinvestigator.com/)
2. [Redditinsight.com](https://www.redditinsight.com/)
3. [Ceddit](https://snew.github.io/r/all)
4. [Reddit Check](https://chrome.google.com/webstore/detail/reddit-check/mllceaiaedaingchlgolnfiibippgkmj?hl=en)
5. [Snoopsnoo.com](https://snoopsnoo.com/)
6. [Reddit History Search](https://github.com/omegavesko/RedditHistorySearch)
----

### YouTube
1. [YouTube GeoFind](https://mattwright324.github.io/youtube-geofind/location)
2. [YouTube MetaData](https://citizenevidence.amnestyusa.org/)
3. [YouTube Data Tools](https://tools.digitalmethods.net/netvizz/youtube/)
4. [YouTube Data Web](https://yasiv.com/youtube#?q=osint)
----

### Facebook
1. [IntelTechniques - Facebook](https://inteltechniques.com/OSINT/facebook.html)
2. [Searchisback.com](https://searchisback.com/)
3. [Stalkscan.com](https://stalkscan.com/)
4. [Facebook.com/livemap](https://www.facebook.com/livemap#)
----

### Snapchat
1. [Snapchat Map](https://map.snapchat.com/)
2. [SnapStory - Download Snapchat Stories](https://github.com/sdushantha/SnapStory)
----

