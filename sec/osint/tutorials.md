### Tutorials - Browsers
1. [General - Micah Hoffman - Getting the Good Stuff: Understanding the Web to Achieve Your OSINT Goals - YouTube](https://www.youtube.com/watch?v=56stZqeWifc)
2. [Google Chrome - Simulate Mobile Devices with Device Mode in Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/device-mode)
----

### Tutorials - Coding/technical knowledge
1. [CodeAcademy](http://codeacademy.com)
2. [Coursera](http://coursera.org)
3. [Learn the command line](https://hellowebbooks.com/news/command-line-zine-launch/)
----

### Tutorials - Cryptocurrencies
1. [Bitcoin - Tracing a Jihadi cell, kidnappers and a scammer using the blockchain](https://medium.com/@benjamindbrown/tracing-syrian-cell-kidnappers-scammers-finances-through-blockchain-e9c52fb6127d)
2. [Bitcoin - Wired - Guide to Bitcoin](https://www.wired.com/story/guide-bitcoin?mbid=social_twitter)
3. [General - Introduction to the blockchain](https://jakecreps.com/2018/10/03/blockchain-osint/)
----

### Tutorials - Cybercrime
1. [3LAB: Hackers (NL)](https://www.npo3.nl//3lab-hackers)
2. [Book : Alias Fortezza (NL)](https://www.bol.com/nl/f/alias-fortezza/9200000045731411/)
3. [Book: Helpende Hackers (NL)](https://www.bol.com/nl/f/-/9200000040692815/)
4. [Brain Krebs - Who's begint the screencan extortion](https://krebsonsecurity.com/2018/08/whos-behind-the-screencam-extortion-scam/)
5. [Dark net - series](https://www.netflix.com/nl-en/title/80182553)
6. [Darknet Diaries Podcast](http://Darknetdiaries.com)
7. [GotPhish - How to get phishing sites blacklisted](http://gotphish.com)
8. [Hoe fileer je een phishing mail? (NL)](https://rickdehaan.wordpress.com/2014/08/02/hoe-fileer-je-een-phishing-mail/)
9. [LaatJeNietHackMaken (NL)](http://laatjeniethackmaken.nl)
10. [Mag de politie jouw computer hacken? (NL)](http://www.universiteitvannederland.nl/college/mag-de-politie-jouw-computer-hacken/)
11. [Robosapiens (NL/UK)](http://filmdoc.nl/robo-sapiens/)
12. [Social engineering (Defcon)](http://www.usatoday.com/story/tech/news/2016/08/15/hacker-social-engineering-defcon-black-hat/88621412)
13. [The Most Dangerous Town on the Internet](https://youtu.be/CashAq5RToM)
14. [We are legion (Anonymous story)](http://www.imdb.com/title/tt2177843/)
----

### Tutorials - Darkweb
1. [Darkweb research](https://hackmd.io/s/rJ-3VKNPG#)
2. [Deanonymizing Tor Hidden Service Users Through Bitcoin Transactions Analysis](https://arxiv.org/pdf/1801.07501.pdf)
3. [Deepweb - Most populair sites](https://www.deepweb-sites.com/deep-web-links-2015/)
4. [Hyperiongray - Visualisation of the Darkweb](https://blog.hyperiongray.com/dark-web-map-introduction/)
5. [Marktplaats van de drugs (NL)](http://volkskrant.nl/kijkverder/2015/drugsmarktplaats/#howto)
6. [Wired - Take down of Hansa Market](https://www.wired.com/story/hansa-dutch-police-sting-operation/)
7. [Wired - Tor is Easier than ever](https://www.wired.com/story/tor-anonymity-easier-than-ever/?mbid=social_twitter&utm_brand=wired&utm_campaign=wired&utm_medium=social&utm_social-type=owned&utm_source=twitter)
----

### Tutorials - Dating
1. [Datingsite Cheatsheet](https://start.me/p/VRxaj5/dating-apps-and-sites-for-investigators)
2. [Tinder - Investigate Tinder](https://www.learnallthethings.net/osmosis)
3. [Tinder - Spoofing location](https://youtu.be/b7bpXlRHgqY)
----

### Tutorials - Financial/Businesses
1. [Corporate Profiling - Advanced LinkedIn searching & more](https://www.osintcombine.com/post/corporate-profiling-advanced-linkedin-searching-more)
2. [OpenCorporates - How to use it](https://blog.opencorporates.com/2017/10/31/the-investigators-handbook-a-guide-to-using-opencorporates/?utm_content=bufferffc39&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
----

### Tutorials - Image/video verification
1. [Bellingcat - Dali, Warhol, Boshirov: Determining the Time of an Alleged Photograph from Skripal Suspect Chepi](https://www.bellingcat.com/resources/how-tos/2018/10/24/dali-warhol-boshirov-determining-time-alleged-photograph-skripal-suspect-chepiga/)
2. [Bellingcat - Using Searchface.ru](https://www.bellingcat.com/resources/how-tos/2019/02/19/using-the-new-russian-facial-recognition-site-searchface-ru/)
3. [Bing image search](http://searchresearch1.blogspot.com/2018/09/new-search-by-image-method-on-bingcom.html)
4. [Citizen Lab: How To: Create a Panoramic Image From a Video](https://citizenevidence.org/2013/10/20/how-to-creating-a-panoramic-picture-from-a-video/)
5. [Get exif data from Archive.org](http://www.automatingosint.com/blog/2016/12/vacuuming-image-metadata-from-the-wayback-machine/)
6. [Glass Reflections in Pictures + OSINT = More Accurate Location](http://blog.ioactive.com/2014/05/glass-reflections-in-pictures-osint.html)
7. [How To Use Reverse Image Search](https://www.searchenginepeople.com/blog/16012-reverse-image-search.html)
8. [Kirby Plessas Tumblr with exif data](http://kirbstr.tumblr.com)
9. [Klopt die foto? Doe de check! (NL)](http://www.gestolengrootmoeder.nl/wordpress/klopt-die-foto-doe-de-check/)
10. [PopSpotSnyc](http://www.popspotsnyc.com/)
11. [Reddit - WhatIsThisThing?](https://www.reddit.com/r/whatisthisthing/)
12. [Reverse engineer airplane tickets](http://youtube.com/watch?v=n8WVo-YLyAg)
13. [Resverse image search from your phone](https://www.pcmag.com/article2/0,2817,2492468,00.asp)
14. [Sector035 - Image verification (importance of details)](https://medium.com/@sector035/quiztime-december-8-2017-827b83079f7a)
15. [Subway typography](https://blog.prototypr.io/typography-on-the-subway-a-trip-around-the-world-463788a76a57)
16. [Twitter - Quiztime](http://twitter.com/quiztime)
17. [Unsolved @Quiztime](https://medium.com/quiztime/unsolved-quiztime-riddles-26eaa5f3fe7b)
18. [Verificatie nieuwsfoto's (NL)](https://www.frankwatching.com/archive/2016/06/29/foto-aanslag-istanbul-hoax-of-echt-tijd-voor-een-handboek-verificatie/)
19. [Verify Eyewitness Videos](https://library.witness.org/product/video-as-evidence-verifying-eyewitness-video/)
20. [VerifyContest (images)](http://dish.andrewsullivan.com/vfyw-contest/)
----

### Tutorials - Law/abuse
1. [Hoe krijg je die ene gênante foto weer van het internet af? (NL)](http://www.universiteitvannederland.nl/college/hoe-krijg-je-die-ene-genante-foto-weer-van-het-internet-af/)
2. [Wie is er de baas op internet? (NL)](http://www.universiteitvannederland.nl/college/wie-is-er-eigenlijk-de-baas-op-het-internet/)
3. [Zijn Adblockers illegaal? (NL)](http://blog.iusmentis.com/2016/04/26/detectie-adblockers-volgens-europese-commissie-illegaal/)
4. [Zo werk de reportbutton bij Facebook (NL)](http://www.nrc.nl/nieuws/2016/04/26/zo-werkt-de-geheime-dienst-van-facebook)
----

### Tutorials - Maps
1. [Strava - How to Use and Interpret Data from Strava's Activity Map - bellingcat](https://www.bellingcat.com/resources/how-tos/2018/01/29/strava-interpretation-guide/)
----

### Tutorials - Mobile apps
1. [Bellingcat - Creating an Android Open Source Research Device on Your PC](https://www.bellingcat.com/resources/how-tos/2018/08/23/creating-android-open-source-research-device-pc/)
----

### Tutorials - OSINT
1. [Bellingcat - Archive your OSINT work](https://www.bellingcat.com/resources/how-tos/2018/02/22/archive-open-source-materials/)
2. [Bellingcat - Daily verification quiz](https://www.bellingcat.com/resources/2017/11/13/daily-verification-quizzes/)
3. [Bellingcat - How to Collect Sources from Syria If You Don't Read Arabic](https://www.bellingcat.com/resources/2018/07/10/how-to-collect-sources-from-syria-if-you-dont-read-arabic/)
4. [Bellingcat - Phone number research](https://www.bellingcat.com/resources/how-tos/2019/04/08/using-phone-contact-book-apps-for-digital-research/)
5. [Check User Guide (PUBLIC)](https://drive.google.com/drive/folders/0B8ssHSpx1n0qcW9TYnNuYmx1VWc)
6. [Creating Sock Puppets](https://osintcurio.us/2018/12/27/the-puppeteer/)
7. [Dfir.training - Cheatsheets](https://www.dfir.training/resources/downloads/cheatsheets-infographics)
8. [Exposing the visible](http://exposingthevisible.org)
9. [Find out what people are searching for](https://www.searchenginejournal.com/seo-101/what-people-search-for/?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%253A+searchenginejournal)
10. [First draft news](http://firstdraftnews.com)
11. [Gathering Open Source Intelligence – Posts By SpecterOps Team Members](https://posts.specterops.io/gathering-open-source-intelligence-bee58de48e05)
12. [GeoGuessr](http://geoguessr.com/)
13. [How metadata helps you with your research](https://blog.sweepatic.com/metadata-hackers-best-friend/)
14. [How to See Shortened URLs Without Opening Them](https://www.hongkiat.com/blog/see-shortened-urls-without-opening-them/)
15. [Hunchly - How to Blow Your Online Cover With URL Previews](https://hunch.ly/osint-articles/osint-article-how-to-blow-your-online-cover)
16. [I-Intelligence OSINT handbook 2018](https://www.i-intelligence.eu/wp-content/uploads/2018/06/OSINT_Handbook_June-2018_Final.pdf)
17. [IntelTechniques - Flow charts](https://inteltechniques.com/blog/2018/03/06/updated-osint-flowcharts/)
18. [Inteltechniques - YouTube investigation](https://inteltechniques.com/wp/2017/11/01/youtube-online-investigation-resources/)
19. [OpenCoorpates - investigator's Handbook](https://blog.opencorporates.com/2017/10/31/the-investigators-handbook-a-guide-to-using-opencorporates/?utm_content=bufferffc39&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
20. [Osintcurious](http://Osintcurio.us)
21. [Osintcurio.us YouTube channel](https://www.youtube.com/channel/UCjzceWf-OT3ImIKztzGkipA)
22. [Osintcurio.us - After the GDPR: researching domain name registrations](https://osintcurio.us/2019/01/08/after-the-gdpr-researching-domain-name-registrations/)
23. [Osintcurio.us - Basic Google](https://osintcurio.us/2019/07/15/how-to-search-effectively-and-efficiently-basic-principles-tips-tricks-for-osint/)
24. [Osintcurio.us - Certificates](https://osintcurio.us/2019/03/12/certificates-the-osint-gift-that-keeps-on-giving/)
25. [Osintcurio.us - Reporting the bad](https://osintcurio.us/2019/03/27/osint-for-good-reporting-the-bad/)
26. [Osintcurio.us - Searching Instagram](https://osintcurio.us/2019/07/16/searching-instagram/)
27. [Osintcurio.us - Searching Instagram - part 2](https://osintcurio.us/2019/10/01/searching-instagram-part-2/)
28. [Osintcurio.us - The New Facebook Graph - part 1](https://osintcurio.us/2019/08/22/the-new-facebook-graph-search-part-1/)
29. [Osintcurio.us - The New Facebook graph Search - part 2](https://osintcurio.us/2019/08/22/the-new-facebook-graph-search-part-2/)
30. [Osintcurio.us - The OSINT Puppeteer](https://osintcurio.us/2018/12/27/the-puppeteer/)
31. [Osintcurio.us - Tracking WiFi](https://osintcurio.us/2019/01/15/tracking-all-the-wifi-things/)
32. [Osintcurio.us - Using OSINT for your personal threat model](https://osintcurio.us/2019/01/30/using-osint-for-your-personal-threat-model/)
33. [OutilsFroids (FR)](http://www.outilsfroids.net/)
34. [Plessas Network - How to investigate in a different language](https://www.youtube.com/watch?v=YgSD4S2Eza8)
35. [SANS OSINT webcast 02-05-2018](https://www.youtube.com/watch?v=6ADHWxCBsUo)
36. [SlechtNieuws (NL)](http://www.slechtnieuws.nl/)
37. [Sourcing.games](https://www.sourcing.games)
38. [Twitter bots who participated in the French elections](https://blog.0day.rocks/uncovering-foreign-trolls-trying-to-influence-french-elections-on-twitter-a78a8c12953)
39. [Verification handbook](http://verificationhandbook.com/)
40. [Verify fake images](https://www.wikitribune.com/story/2017/12/01/media/spotting-fake-images-guide/6867/?utm_content=buffer690a5&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
41. [Learn New tools](http://learn-new-tools.org/)
42. [Washington post - Check fake news](https://www.washingtonpost.com/news/fact-checker/wp/2016/11/22/the-fact-checkers-guide-for-detecting-fake-news/)
----

### Tutorials - OSINT libraries
1. [Advanced Digital Toolkit - OSINT - Andy Black Associates](https://www.andyblackassociates.co.uk/resources-andy-black-associates/advanced-digital-toolkit/)
2. [AndyBlackAssociates](https://www.andyblackassociates.co.uk/resources-andy-black-associates/advanced-digital-toolkit/)
3. [Asint collection](https://start.me/p/b5Aow7/asint_collection)
4. [Arno Reusers page](http://rr.reuser.biz/)
5. [AndreaFortuna - OSINT links](https://andreafortuna.org/osint/open-source-intelligence-tools-for-social-media-my-own-list/)
6. [Bellingcat - Guides](https://www.bellingcat.com/category/rescources/how-tos/)
7. [BG Hun-OSINT](https://start.me/p/kxGLzd/hun-osint)
8. [Booleanstrings Open Source Tools](https://booleanstrings.com/tools/)
9. [Bruno Mortier (DEU)](https://start.me/p/3g0aKK/sources)
10. [Bruno Mortier OSINT collection](https://start.me/p/ZME8nR/osint)
11. [Ciberpatrulla (ESP)](https://ciberpatrulla.com/links/)
12. [Country sorted OSINT links](https://start.me/p/W2kwBd/sources-cnty)
13. [Denis Dinkevich OSINT library](http://workinukraine.space/)
14. [DiRT - Digital Research Tools](https://dirtdirectory.org/)
15. [i-Sight](https://i-sight.com/resources/101-osint-resources-for-investigators/)
16. [IntelTechniques.com](http://inteltechniques.com)
17. [IntelTechniques - bookmarks](https://inteltechniques.com/ivmachiavelli/)
18. [OSINTToolkit Github](https://osinttoolkit.github.io/index.html)
19. [OSINT Anti money laundering](https://start.me/p/rxeRqr/aml-toolbox?embed=1)
20. [Lorand Bodo - Radicalisation & terorristic library](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
21. [Peerlyst - OSINT](https://www.peerlyst.com/posts/resource-osint-tools-and-how-you-learn-how-to-use-them-guurhart?utm_source=twitter&utm_medium=social&utm_content=peerlyst_post&utm_campaign=peerlyst_shared_post)
22. [Ph055a/awesome_osint](https://github.com/Ph055a/awesome_osint/blob/master/README.md)
23. [RecruiterHunt - Sourcing tools](https://recruiterhunt.com/collection/sourcing/)
24. [ResearchClinic links](http://www.researchclinic.net/)
25. [Sapient Intelligence](https://start.me/p/wMAGAQ/osint-toolbox)
26. [Sprp77 Costum Search Engines](https://start.me/p/b5ynOQ/sprp77-search-engines)
27. [Tomokodiscovery](https://tomokodiscovery.com/free-tools-osint-socmint-dark-web-darknet-tor-bitcoin/)
28. [UK-Osint](http://www.uk-osint.net/index.html)
29. [Verification set for datingsites](https://start.me/p/VRxaj5/dating-apps-and-sites-for-investigators)
30. [Verification Toolset](https://start.me/p/ZGAzN7/verification-toolset)
31. [Web OSINT Resources by @CryptoCypher](https://brokemy.network/osint-resources/)
32. [QWARIE: UK-OSINT](http://www.uk-osint.net/index.html)
----

### Tutorials - Search engines
1. [General - Job Search websites](https://www.makeuseof.com/tag/job-search-engines/)
2. [Google - 4500+ Google Dork List 2018](http://www.conzu.de/en/google-dork-liste-2018-conzu/)
3. [Google - A Google a Day](http://agoogleaday.com)
4. [Google - Advanced Google Search Methods (IRE 2017)](https://drive.google.com/file/d/0BxlpTzK9iG-2MG4zWGxpSVRRaWc/view)
5. [Google  - Exploit DB Google Dorks](http://exploit-db.com/google-hacking-database)
6. [Google - Google dorks](https://ahrefs.com/blog/google-advanced-search-operators/)
7. [Google - Google maps on steroids](https://www.linkedin.com/pulse/google-maps-steroids-henk-van-ess/)
8. [Google - Google Search, Google Scholar, Google Public Data Explorer, Google Maps, Google Earth Pro](https://newsinitiative.withgoogle.com/training/course/investigative-reporting)
9. [Google - How to find almost anything on Google](https://www.socialmediatoday.com/news/how-to-find-almost-anything-on-google-infographic/531539/)
10. [Google - How to restore image search](https://booleanstrings.com/2019/09/01/how-to-restore-image-search-functions/)
----

### Tutorials - Social media
1. [Facebook - Convert your public Facebook pages to RSS](https://blog.inoreader.com/2018/06/how-to-convert-public-facebook-pages-to-rss-feeds.html)
2. [Facebook - Download video](https://hdinternet.nl/download-video-from-facebook/)
3. [Facebook - Filtering private/public common friends on Facebook](https://inteltechniques.com/blog/2018/05/13/filtering-private-public-common-friends-on-facebook/)
4. [Facebook - Find a profile linked to an email address](https://booleanstrings.com/2018/05/06/how-to-identify-facebook-profiles-from-email-addresses/)
5. [Facebook - Phone number to profile](https://medium.com/@ct.osint.001/osint-post-1-finding-facebook-profiles-through-phone-numbers-5c37910865c5)
6. [Instagram - Documentary #Followme (NL)](https://www.vpro.nl/programmas/follow-me.html)
7. [LinkedIn - Find a profile linked to an email address](https://booleanstrings.com/2018/04/25/rapportive-without-gmail-or-chrome/)
8. [LinkedIn - How to find anyone's phone number and email address in under 30 seconds](https://www.sourcecon.com/how-to-find-anyones-email-address-phone-number-in-under-30-seconds/)
9. [Reddit - Basic Reddit part 1](https://www.hetheringtongroup.com/reddit/)
10. [Reddit - Basic Reddit part 2](https://www.hetheringtongroup.com/reddit-2/)
11. [Twitter - Create Twitter lists (NL)](https://www.frankwatching.com/archive/2014/03/24/twitterlijsten-zo-zet-je-ze-slim-blijf-je-betrokken/)
12. [Twitter - Create Twitter lists (NL)](http://lifehacking.nl/persoonlijk-tips/twitter-zo-stiekem-iemand-volgen/)
13. [Twitter - Find tweets by device](http://thesocialchic.com/2013/10/01/how-to-master-twitter-search-searching-for-tweets-by-device/)
14. [Twitter - How to find breaking news](https://firstdraftnews.com/how-to-find-breaking-news-on-twitter-social-media-journalism/)
15. [Twitter - How to search Twitter like a pro](https://www.labnol.org/internet/twitter-search-tricks/13693/)
16. [Twitter - Tweetdeck (NL)](http://bit.ly/EbookTD)
17. [Twitter - Tweetdeck](https://jakecreps.com/2018/05/19/tweetdeck/)
----

### Tutorials - Privacy and Security
1. [20 Tools to manage and generate your passwords](https://www.hongkiat.com/blog/password-tools/)
2. [Blog: Opting out like a Boss](https://www.learnallthethings.net/blog/2018/1/23/opting-out-like-a-boss-the-osint-way)
3. [Book: Je hebt wel iets te verbergen (NL)](https://www.bol.com/nl/p/je-hebt-wel-iets-te-verbergen/9200000063121305/?Referrer=ADVNLGOO002008J-F7CQGFIURUYLW-226740458382&gclid=EAIaIQobChMIxdj2zqK-2AIVx7ftCh2rbABfEAAYASAAEgL9bPD_BwE)
4. [Choose your VPN wisely](https://www.pcmag.com/article2/0,2817,2403388,00.asp)
5. [Do Not Track](https://donottrack-doc.com/en/)
6. [How to create an anonymous email address](https://thebestvpn.com/anonymous-email/)
7. [How to Create an Anonymous Email Account](https://www.pcmag.com/article2/0,2817,2476288,00.asp)
8. [Inteltechniques - Hiding from the internet](https://inteltechniques.com/blog/2018/05/15/free-online-personal-data-removal-workbook/)
9. [MyDataRequest](https://mydatarequest.com/)
10. [Secured.fyi](https://secured.fyi/)
11. [Security advise from an ethical hacker](https://www.huffingtonpost.com/entry/hacker-online-safety_us_5b201b58e4b09d7a3d77e471?h6b&guccounter=1)
12. [Security in a Box](http://securityinabox.org/en)
13. [Top10VPN](https://www.top10vpn.com/)
14. [VPN Comparison](https://thatoneprivacysite.net/#simple-vpn-comparison)
15. [VPRO Privacyweken (NL)](http://www.npo.nl/npo3/privacyweken-op-npo-3)
----

### Tutorials - Who.is/website information
1. [Find out who's behind Cloudflare hosted domains](https://pielco11.ovh/posts/cloud-hunting/)
2. [Reverse DNS](https://securitytrails.com/blog/use-reverse-dns-records-to-identify-mass-scanners)
----

### Tutorials - YouTube
1. [How to download YouTube comments in Excel](http://www.ilovefreesoftware.com/17/tutorial/how-to-download-youtube-comments-excel.html)
----

