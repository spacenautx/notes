### Monitoring
1. [Nachrichtentisch](https://nachrichtentisch.de/index.php)
2. [NewsBrief](https://emm.newsbrief.eu/NewsBrief/clusteredition/en/latest.html)
3. [Tweetdeck](https://tweetdeck.twitter.com/#)
4. [Crowdtangle](https://apps.crowdtangle.com/)
5. [Rdddeck.com](https://rdddeck.com/)
6. [Google Trends](https://trends.google.com/trends/)
7. [Google Alerts](https://www.google.com/alerts)
8. [LiveUMap](https://myanmar.liveuamap.com/)
9. [Human Rights News](https://reliefweb.int/)
10. [Amnesty News](https://www.amnesty.org/en/latest/news/)
11. [EVNSocialNewswire](https://www.evnsocialnewswire.ch)
12. [Advanced Search Operators (Google)](https://docs.google.com/document/d/1ydVaJJeL1EYbWtlfj9TPfBTE5IBADkQfZrQaBZxqXGs/edit?fbclid=IwAR3Iape2uggcHOGgEPnmQF_pfjFCtxjDaAwKXUjjulSP0y7l8tbgUbfMcXo)
----

### Twitter
1. [TweetDeck](https://tweetdeck.twitter.com/#)
2. [Twitter Advanced Search](https://twitter.com/search-advanced)
3. [Twitter Operators](https://www.labnol.org/internet/twitter-search-tricks/13693/)
4. [Twxplorer (Topic)](https://twxplorer.knightlab.com/)
5. [BirdHunt (geocode)](https://birdhunt.co/)
6. [Mentionmapp (Topic)](http://mentionmapp.com/)
7. [Trends24 (Topic)](https://trends24.in/)
8. [Tweetmap (Topic)](http://www.onemilliontweetmap.com/)
9. [Trendsmap (Topic)](https://www.trendsmap.com/)
10. [Twitter List Copy](http://projects.noahliebman.net/listcopy/index.php)
11. [Twitter lists via Google](https://twitter.com/henkvaness/status/996985124817338369)
12. [TweetBeaver (Source)](http://tweetbeaver.com/)
13. [Followerwonk (Source)](https://moz.com/followerwonk/analyze)
14. [AccountAnalysis (Source)](https://accountanalysis.lucahammer.com/)
15. [Videos download](http://www.downloadtwittervideo.com/de/)
16. [Epoch Converter](https://www.epochconverter.com/)
----

### Facebook
1. [GraphTips (Topic/Source)](http://graph.tips/)
2. [WhoPostedWhat (Source)](https://whopostedwhat.com/)
3. [IntelligenceX (Topic/Source)](https://intelx.io/tools?tab=facebook)
4. [Formular:Lang&location](https://twitter.com/henkvaness/status/1062672054477078528)
5. [Facebook Video Downloader (Chrome plugin)](https://www.fbdown.net/)
6. [Ad Analyse](https://www.facebook.com/politicalcontentads)
----

### Youtube
1. [Location & Time (Youtube)](https://mattw.io/youtube-geofind/location)
2. [FramebyFrame](http://www.watchframebyframe.com/)
3. [Data Viewer](http://citizenevidence.amnestyusa.org/)
4. [DownloadDeturl](http://deturl.com/)
5. [YouTube Converter & Downloader](https://www.onlinevideoconverter.com/de/youtube-converter)
6. [DownloadVideos/Audios](https://www.y2mate.com/de/home)
----

### Insta/Reddit /Snap/TikTok
1. [Reddit (Source)](https://atomiks.github.io/reddit-user-analyser/)
2. [Reddit Tweetdeck](https://rdddeck.com/)
3. [Reverse (Reddit)](http://karmadecay.com/)
4. [Reddit Search](https://camas.unddit.com/)
5. [SnapMap](https://map.snapchat.com/@50.958500,6.924390,12.00z)
6. [Discord search](https://disboard.org/de/search)
7. [GitHub (Insta)](https://github.com/sc1341/InstagramOSINT)
8. [Download Insta Stories](https://www.storysaver.net/)
9. [Instagram search](https://www.instagram.com/juannaba/)
10. [Instagram locations](https://www.instagram.com/explore/locations/)
11. [Tutorial-Search (TikTok)](https://docs.google.com/document/d/1dasZ8tCdM12Nwhw0Z-07od-ASMU5Z2uZ1LAgPH7Jhdo/edit)
12. [Search #s TikTok via Google](https://www.google.com/search?q=site%3Atiktok.com%2Ftag+puthashtagyouwanttosearchhere&rlz=1C5CHFA_enDE931DE939&oq=site%3Atiktok.com%2Ftag+puthashtagyouwanttosearchhere&aqs=chrome.0.69i59j69i58.741j0j9&sourceid=chrome&ie=UTF-8)
13. [Search user without exact username (TikTok)](inurl:https://m.tiktok.com/h5/share/usr filetype:html julia)
14. [Download social media videos without tools](https://www.osintcombine.com/post/download-social-media-videos-without-tools)
----

### Telegram
1. [Intelligence X (Telgago)](https://intelx.io/tools?tab=telegram)
2. [Search.buzz](https://search.buzz.im/)
3. [Telemetr (reach of channels)](http://telemetr.io/)
4. [Telegram Search](https://tgstat.com/search)
5. [Telegram channel rating](https://telemetr.io/en/channels)
6. [Telegram-OSINT](https://github.com/ItIsMeCall911/Awesome-Telegram-OSINT#-search-engines)
----

### LinkedIn
1. [Are You Linked In? – OSINT, OPSEC, Privacy & Hostile Profiling](https://www.cqcore.uk/are-you-linked-in/)
----

### Breaking News
1. [Instagram locations](https://www.instagram.com/explore/locations/)
2. [Snap Map](https://map.snapchat.com/)
3. [Afghanistan.liveuamap.com](https://afghanistan.liveuamap.com/)
4. [Nachrichtentisch Start](https://nachrichtentisch.de/)
----

### Tutorials / Toollists
1. [Tweetdeck Tips](https://www.bellingcat.com/resources/how-tos/2019/06/21/the-most-comprehensive-tweetdeck-research-guide-in-existence-probably/)
2. [How to find breaking news on Twitter](https://firstdraftnews.com/how-to-find-breaking-news-on-twitter-social-media-journalism/)
3. [Newsgathering Twitter](http://sarahmarshall.io/post/115007936164/15-tips-for-newsgathering-via-twitter)
4. [Twitter Location](https://medium.com/1st-draft/quick-guide-finding-eyewitness-media-from-an-exact-location-on-twitter-1fa28f48838a)
5. [Twitter lists](https://medium.com/1st-draft/starting-from-scratch-twitter-lists-expose-the-heart-of-a-story-d1f8bee73e9d)
6. [Search videos via Google](https://www.bellingcat.com/resources/how-tos/2017/10/17/conduct-comprehensive-video-collection/)
7. [Video Verification](https://www.bellingcat.com/resources/how-tos/2017/06/30/advanced-guide-verifying-video-content/)
8. [Facebook secrets](http://researchclinic.net/facebooksecrets/)
9. [Google search](https://support.google.com/websearch/answer/2466433?p=adv_operators&hl=en&rd=1)
10. [SnapMap](https://firstdraftnews.com/snapchat-map-newsgathering/)
11. [SnapMap1](http://www.niemanlab.org/2017/11/snap-maps-offered-real-time-coverage-of-tuesdays-terror-attacks-in-manhattan-plus-a-lot-of-emoji/?utm_content=bufferf62ae&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
12. [Verification Handbook](http://verificationhandbook.com/)
13. [Research Clinic aka Paul Myers](http://researchclinic.net/links.html)
14. [Bellingcat](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
15. [Bit.ly/VerificationToollist](http://bit.ly/VerificationToollist)
16. [Checklist - A roadmap to the truth](https://revealproject.eu/a-roadmap-to-the-truth/)
17. [19 Aviation](https://start.me/p/Nx1g5M/19-aviation)
18. [Verification Handbook - Toollist](https://docs.google.com/document/d/14jugVfxt3DLi2IlrevFKlXRg9C4Dl93Cvs73LxJmwqQ/edit)
19. [Truly Media - collaborative verification platform](http://www.truly.media/)
20. [OSINT on anyone](https://medium.com/@Peter_UXer/osint-how-to-find-information-on-anyone-5029a3c7fd56)
----

### Geolocation
1. [Google Maps](https://www.google.de/maps/@50.9251688,6.9604035,15z?hl=de)
2. [Yandex Maps](https://yandex.com/maps/10402/bonn/)
3. [Baidu Map](http://map.baidu.com/)
4. [Bing Map](https://www.bing.com/maps/)
5. [Bing SAT - Bird´s eye (nesw)](https://www.bing.com/maps?&cp=50.941157~6.958324&lvl=20&sty=o&w=100%&dir=0)
6. [Wikimapia](http://wikimapia.org/#lang=de&lat=51.529000&lon=7.480200&z=12&m=b)
7. [Mapillary (Openstreetmap & UGC)](https://www.mapillary.com/app)
8. [KartaView](https://kartaview.org/landing)
9. [OpenStreetCam (Alternative GoogleStreetView)](http://www.openstreetcam.org/)
10. [OpenStreetMap](http://www.openstreetmap.org/#map=6/51.330/10.453)
11. [Dual Maps](http://data.mashedworld.com/dualmaps/map.htm)
12. [Latitude/Longitude Coordinates](https://www.latlong.net/)
13. [Convert Latitude/Longitude to Decimal](https://andrew.hedges.name/experiments/convert_lat_long/)
14. [Map Switcher](https://chrome.google.com/webstore/detail/map-switcher/fanpjcbgdinjeknjikpfnldfpnnpkelb)
15. [Descartes Labs: Search](https://search.descarteslabs.com/?layer=naip_v2_rgb_2014-2015#lat=39.2322531&lng=-100.8544921&skipTut=true&zoom=5)
16. [Tencent Maps (China)](https://map.qq.com/#)
17. [Map service (China)](https://map.sogou.com/)
18. [Amap (China, street view pics)](https://www.amap.com/)
19. [Geofind (Youtube)](https://mattw.io/youtube-geofind/location)
20. [HERE WeGo - Satellite](https://wego.here.com/)
21. [iTouchmap](https://www.itouchmap.com)
22. [GeoNames](http://www.geonames.org/)
23. [EarthExplorer](https://earthexplorer.usgs.gov/)
24. [Intelligence Techniques](https://inteltechniques.com/osint/maps.html)
25. [Interactive live map of conflict news](http://liveuamap.com/)
26. [Syriancivilwarmap.com](https://syriancivilwarmap.com/)
27. [Power Grid](https://nadoloni.com/power.html#6/49/23)
28. [OpenRailwayMap](http://www.openrailwaymap.org/)
29. [Directory of Cities and Towns in World](http://www.fallingrain.com/world/index.html)
30. [PeakFinder - mountains](https://www.peakfinder.org/)
31. [PeakVisor](http://peakvisor.com/panorama.html?lat=45.9420159&lng=7.8699421&alt=4598)
32. [Panorama](https://www.udeuschle.de/panoramas/makepanoramas_en.htm)
33. [MapChecking - People on a square](https://www.mapchecking.com/#1;48.8629656,2.2869780,18)
34. [Transform coordinates (baidu)](https://tool.lu/coordinate/)
35. [Linklist - Geospatial | Sector035](https://sector035.nl/links/geospatial)
36. [3D map of the world by PeakVisor](https://peakvisor.com/panorama.html?lat=45.9420159&lng=7.8699421&alt=4598)
----

### Satellite
1. [Planet Explorer](https://www.planet.com/explorer/#)
2. [Discover Maxar](https://discover.maxar.com/)
3. [Sentinel Playground | Sentinel Hub](https://apps.sentinel-hub.com/sentinel-playground/?source=S2&lat=40.4&lng=-3.73&zoom=12&preset=1-NATURAL-COLOR&layers=B01,B02,B03&maxcc=20&gain=1.0&gamma=1.0&time=2020-02-01%7C2020-08-19&atmFilter=&showDates=false)
4. [Sentinel Hub EO Browser](https://apps.sentinel-hub.com/eo-browser/?zoom=10&lat=41.57025&lng=12.0623&themeId=DEFAULT-THEME)
5. [Google Earth](https://earth.google.com/web/@0,0,0a,22251752.77375655d,35y,0h,0t,0r)
6. [Apollomapping.com](https://apollomapping.com/)
7. [🗺Compare Satellite Imagery 🛰](https://mc.bbbike.org/mc/#)
8. [Satellites.pro](https://satellites.pro/)
9. [Soar.earth](https://soar.earth/)
10. [How to...](https://towardsdatascience.com/how-to-use-open-source-satellite-data-for-your-investigative-reporting-d662cb1f9f90)
11. [OpenAerialMap](https://openaerialmap.org/)
----

### Satellite + Features
1. [Radius, Distance, Area calculator](https://www.freemaptools.com/)
2. [Crisis Region Interactive map](https://liveuamap.com/#)
3. [old Terraserver (paid)](https://www.precisionhawk.com/satellite)
4. [Storm historical data](https://zoom.earth/)
5. [Weather documentation with S I](https://eos.com/landviewer/?lat=37.15290&lng=36.06550&z=15&id=S2A_tile_20190521_37SBB_1&b=Red,Green,Blue&anti)
6. [Wayback Imagery](http://livingatlas.arcgis.com/wayback)
----

### Flight Tracking
1. [19 Aviation](https://start.me/p/Nx1g5M/19-aviation)
2. [Flightradar (commercial)](https://www.flightradar24.com/51.47,7.14/7)
3. [FlightAware (alerts)](http://uk.flightaware.com/)
4. [ADS-B Exchange](https://globe.adsbexchange.com/)
5. [Ads-b exchange (contact request)](https://www.adsbexchange.com/)
6. [ADS-B.NL (military aircrafts)](http://www.ads-b.nl/)
7. [Radarbox (military)](https://www.radarbox24.com/)
8. [Flight Tracker](https://www.flightstats.com/v2/flight-tracker/search)
9. [The OpenSky Network](https://opensky-network.org/network/explorer)
10. [Aircraft Database](https://opensky-network.org/aircraft-database/)
11. [Aircraft registration](https://en.wikipedia.org/wiki/Aircraft_registration)
12. [AeroTransport (owners)](https://aerotransport.org/)
13. [Airframes (owners)](http://www.airframes.org/)
14. [Rzjets (owners)](https://rzjets.net/aircraft/)
15. [JetPhotos (good)](https://www.jetphotos.com/)
16. [Plane Finder Flight Tracker](https://planefinder.net/)
17. [Airliners.net](https://www.airliners.net/)
18. [Radar.freedar - all in one](https://radar.freedar.uk/VirtualRadar/desktop.html)
19. [Planespotters.net](https://www.planespotters.net/)
20. [Planespotting: A Guide to Tracking Aircraft Around the World - GIJN](https://gijn.org/flight-tracking/)
21. [Ducis.net](https://ducis.net/)
----

### Chat Apps
1. [Russian Telegram-channels. Statistics, analytics, TOP chart. Telegram Analytics.](https://tgstat.ru/en)
----

### Original / Context
1. [Google Images](https://images.google.com/)
2. [Yandex images](https://yandex.com/images/?redircnt=1515665874.1)
3. [Baidu images](https://image.baidu.com/)
4. [Bing images](https://www.bing.com/?scope=images&nr=1&FORM=NOFORM)
5. [TinEye](https://www.tineye.com/)
6. [RevEye](https://chrome.google.com/webstore/detail/reveye-reverse-image-sear/keaaclcjhehbbapnphnmpiklalfhelgf?hl=en)
7. [InVID Video Verification](https://www.invid-project.eu/tools-and-services/invid-verification-plugin/)
8. [Google Lens](https://lens.google.com/)
9. [Google Advanced Search](https://www.google.com/advanced_search)
----

### Source
1. [Intelligence X](https://intelx.io/tools?tab=general)
2. [KnowEm Username search](https://knowem.com/)
3. [Namechk](https://namechk.com/)
4. [namecheckr](https://www.namecheckr.com/)
5. [Have I been pwned?](https://haveibeenpwned.com/)
6. [OSINT Framework](https://osintframework.com/)
7. [Pipl](https://pipl.com/)
8. [PeekYou](https://www.peekyou.com/)
9. [IntelTechniques](https://inteltechniques.com/osint/menu.name.html)
10. [Whitepages](https://www.whitepages.com/)
11. [Online exif data viewer PDFs](https://www.get-metadata.com/)
12. [SSL/TLS Certificates Data | Spyse](https://spyse.com/search/certificate?q=domain:silensec.com)
13. [Fingerprint certificate](https://crt.sh/)
14. [https://pimeyes.com/en/](https://pimeyes.com/en/)
15. [WhatsMyName Profiles](https://whatsmyname.app/)
----

### Security
1. [Fireshot - screenshot (free)](https://chrome.google.com/webstore/detail/take-webpage-screenshots/mcbpblocgmgfnpjjppndjkmgjaogfceg?hl=de)
2. [Hunchly (premium / Chrome)](https://www.hunch.ly/)
3. [Screenshot (Firefox)](https://inteltechniques.com/blog/2018/07/29/firefox-screen-capture-tool/)
----

### Newsletter/Toolset
1. [Week in OSINT @Sector035](https://sector035.nl/articles/category:week-in-osint)
2. [OSINT Framework](http://osintframework.com/)
3. [osintcurio](https://osintcurio.us/)
4. [Bellingcat's OSINT Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
5. [NixIntel](https://nixintel.info/)
6. [Technisette - Start.me list](https://start.me/p/m6XQ08/osint)
7. [Digintel -Start.me list](https://start.me/p/ZME8nR/osint)
8. [i-intelligence.eu/uploads/public-documents/OSINT_Handbook_2020.pdf](https://i-intelligence.eu/uploads/public-documents/OSINT_Handbook_2020.pdf)
9. [Blog](https://benjaminstrick.com/blog/)
----

### Image
1. [Text in image translation](https://translate.yandex.com/ocr)
----

### Ship tracking
1. [VesselFinder](https://www.vesselfinder.com/)
2. [MarineTraffic](http://www.marinetraffic.com/en/ais/home/centerx:-12.0/centery:25.0/zoom:4)
3. [myShipTracking](https://www.myshiptracking.com/)
4. [Shipspotting](http://www.shipspotting.com/)
5. [CruiseMapper](https://www.cruisemapper.com/)
6. [Live AIS Ships Map](https://www.shipfinder.com/)
7. [FleetMon](https://www.fleetmon.com/)
8. [Ships directory - contact details from a ship's name](https://www.inmarsat.com/en/support-and-info/support/ships-directory.html)
9. [Shipping-related businesses and ports](https://www.maritime-database.com/)
10. [Maritime Connector](http://maritime-connector.com/ships/)
----

### GEOSINT *extras
1. [SkylineWebcams](https://www.skylinewebcams.com/en.html)
2. [Street signs](https://en.wikipedia.org/wiki/Comparison_of_European_road_signs#Warning)
3. [Logos Detection](http://logos.iti.gr)
4. [Rail Cab Rides](https://railcabrides.com/en/)
5. [https://endcoal.org/tracker/](https://endcoal.org/tracker/)
6. [Search from a different location & device](http://isearchfrom.com/)
7. [License Plates of the World](http://worldlicenseplates.com/)
8. [Train Germany](http://www.apps-bahn.de/bin/livemap/query-livemap.exe/dn?L=vs_livefahrplan&livemap)
----

### Date&Time
1. [WolframAlpha](http://www.wolframalpha.com/)
2. [SunCalc](https://suncalc.org)
3. [World time](https://www.timeanddate.de/)
4. [Wetter Germany](https://kachelmannwetter.com/de/)
5. [Sonnenverlauf](https://www.sonnenverlauf.de/#/49.495,11.073,5/2017.11.10/12:52/1/0)
6. [Weather Underground](https://www.wunderground.com/)
7. [Mooncalc](https://www.mondverlauf.de/#/50.9421,6.9582,17/2018.03.06/21:53/1/2)
8. [Time converter](https://www.worldtimebuddy.com/)
9. [ShadowCalculator](http://shadowcalculator.eu)
10. [Wind & Webcams](https://www.windy.com/station/wmo-10384?satellite,52.467,13.400,8)
----

### Websites-Tracking
1. [Web Archive](https://archive.org/web/)
2. [Advanced Archive Search](https://archive.org/advancedsearch.php)
3. [Archive Today](http://archive.today)
4. [Domaintools: Research](https://research.domaintools.com/)
5. [Whois](https://www.whois.com/)
6. [Mailadresse](https://hunter.io/)
7. [Whois Lookup](https://whois.domaintools.com/)
8. [Denic](https://www.denic.de/webwhois/)
9. [Hosting a website](https://w3bin.com)
10. [ViewDNS.info](https://viewdns.info/)
11. [Historical Registrant](https://domainbigdata.com/)
12. [Website change detection](https://visualping.io/)
13. [domainbigdata](https://domainbigdata.com/)
14. [SpyOnWeb](http://spyonweb.com/)
15. [dnslytics](https://dnslytics.com/)
16. [SecurityTrails](https://securitytrails.com/)
17. [Whoisology](https://whoisology.com/)
18. [analyzeid](http://analyzeid.com/)
19. [Tutorial](https://dataharvest.eu/wp-content/uploads/2021/05/laura-ranca.pdf)
----

### Metadata
1. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
2. [FotoForensics](http://fotoforensics.com/)
3. [EXIF Data Viewer](http://exifdata.com/)
4. [Image Metadata](http://exif.regex.info/exif.cgi)
5. [IrfanView](http://www.irfanview.com/)
6. [Exif viewer](http://metapicz.com/#landing)
7. [Forensic Analyzer](https://www.imageforensic.org/#work)
8. [Forensically](https://29a.ch/photo-forensics/#forensic-magnifier)
9. [Resemble.js - analysis & comparison](https://huddle.github.io/Resemble.js/)
10. [Diff Checker](https://www.diffchecker.com/image-diff)
11. [Logo detection](http://logos.iti.gr/)
12. [Imageforensic.org](https://www.imageforensic.org/)
----

### Chrome extensions
1. [InVID-Fake video news debunker](https://chrome.google.com/webstore/detail/fake-video-news-debunker/mhccpoafgdgbhnjfhkcmgknndkeenfhe?hl=en)
2. [Treeverse - Visualize Twitter Conversations](https://chrome.google.com/webstore/detail/treeverse/aahmjdadniahaicebomlagekkcnlcila)
3. [RevEye Image Search](https://chrome.google.com/webstore/detail/reveye-reverse-image-sear/keaaclcjhehbbapnphnmpiklalfhelgf?hl=en)
4. [Wayback Machine](https://chrome.google.com/webstore/detail/wayback-machine/fpnmgdkabkmnadcjpehmlllkndpkmiak)
5. [Map switcher](https://chrome.google.com/webstore/detail/map-switcher/fanpjcbgdinjeknjikpfnldfpnnpkelb)
6. [FirstDraftNewsCheck](https://chrome.google.com/webstore/detail/firstdraftnewscheck/japockpeaaanknlkhagilkgcledilbfk)
7. [Google Translate](https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb)
8. [Keep for Chrome](https://chrome.google.com/webstore/detail/keep-for-chrome/ghhhikeonlfpibbaibecmiellnmaikoh)
9. [Truly Media](https://chrome.google.com/webstore/detail/truly-media/nkgnebcoenckmflihkceknmglbkeiano)
10. [360social](https://chrome.google.com/webstore/detail/360social/bplilbogidkdmacifmkmciboihbcchom)
11. [CrowdTangle Link Checker](https://chrome.google.com/webstore/detail/crowdtangle-link-checker/klakndphagmmfkpelfkgjbkimjihpmkh)
12. [Falcon](https://chrome.google.com/webstore/detail/falcon/fcbcnboheaijdfnkchlbeilgmaebdogd)
13. [Mappeo](https://chrome.google.com/webstore/detail/mappeo/lnempicjilmahngopecohbcbldlijkib)
14. [Storyful Multisearch](https://chrome.google.com/webstore/detail/storyful-multisearch/hkglibabhninbjmaccpajiakojeacnaf)
15. [VideoVault](https://chrome.google.com/webstore/detail/videovault-for-chrome/onoidkfboifjpllldnfnaebfigijckmk)
16. [Wayback Machine](https://chrome.google.com/webstore/detail/wayback-machine/fpnmgdkabkmnadcjpehmlllkndpkmiak)
17. [Go Back In Time](https://chrome.google.com/webstore/detail/go-back-in-time/hgdahcpipmgehmaaankiglanlgljlakj)
18. [Extensions Manager](https://chrome.google.com/webstore/detail/extensions-manager/kllcckbicipekmojkcopeameajngoomf)
19. [Ghostery – identify Google AdSense IDs](https://chrome.google.com/webstore/detail/ghostery-%E2%80%93-privacy-ad-blo/mlomiejdfkolichcflejclcbmpeaniij?hl=en)
20. [Web Archives](https://chrome.google.com/webstore/detail/web-archives/hkligngkgcpcolhcnkgccglchdafcnao?hl=en)
21. [Teeverse - Twitter Thread Analyse](https://chrome.google.com/webstore/detail/treeverse/aahmjdadniahaicebomlagekkcnlcila/related?hl=en)
----

### Deepfake
1. [This Deepfake Tool Can Make Someone Say Nearly Anything - YouTube](https://www.youtube.com/watch?v=347jdBs-mRU)
2. [The ethics of deepfakes aren’t always black and white](https://thenextweb.com/podium/2019/06/16/the-ethics-of-deepfakes-arent-always-black-and-white/)
3. [The Best (And Scariest) Examples Of AI-Enabled Deepfakes](https://www.forbes.com/sites/bernardmarr/2019/07/22/the-best-and-scariest-examples-of-ai-enabled-deepfakes/)
4. [Deepfake videos pose a threat, but 'dumbfakes' may be worse](https://apnews.com/e810e38894bf4686ad9d0839b6cef93d)
----

### Extras
1. [Remove Exif data](http://www.verexif.com/en/)
2. [‘Synonames’ across languages](https://aleph.occrp.org/)
3. [ISearchFrom - Google](http://isearchfrom.com/)
4. [Colorize Black and White Photos](https://demos.algorithmia.com/colorize-photos)
----

### Images
1. [Image super-resolution and enhancement](https://letsenhance.io)
2. [Background Removal](https://online.photoscissors.com/)
3. [Compare two images](https://www.diffchecker.com/image-diff)
----

### IPs/Email/Phone
1. [Using Lampyre for Basic Email and Phone Number OSINT](https://medium.com/@raebaker/using-lampyre-for-basic-email-and-phone-number-osint-e0e36c710880)
2. [Wifi network](https://wigle.net/mapsearch?maplat=37.7656&maplon=-122.4246&mapzoom=12)
3. [Find GSM base stations cell id coordinates](https://cellidfinder.com/)
4. [IP adress](https://www.onyphe.io/)
5. [Gmail Adressen](https://tools.epieos.com/google-account.php)
6. [Map IPs](https://ipinfo.io/map)
7. [Gmail to Username](https://cq21.uk/email-to-username/)
----

### Extras
1. [Extract data from pdf´s](https://gijn.org/2017/07/17/beginners-guide-to-extracting-data-from-pdfs/)
2. [3D creation for free](https://www.capturingreality.com/)
3. [3D creation for free](https://www.blender.org/)
----

### Best of
1. [Line of Actual Control on Twitter: "Good question! Here are mine:

- @suncalc_net
- @SecForceMonitor 's data sets
- @MarineTraffic 
- Transover Chrome extension
- Instant Multi Search Chrome extension

And some people I'd love to hear from: @marksnoeck @Bashar24836440 @Nrg8000 @SecForceMonitor… https://t.co/GRd9oz4KDO"](https://twitter.com/LOActualControl/status/1250419566062305281)
2. [Line of Actual Control on Twitter: "Good question! Here are mine:

- @suncalc_net
- @SecForceMonitor 's data sets
- @MarineTraffic 
- Transover Chrome extension
- Instant Multi Search Chrome extension

And some people I'd love to hear from: @marksnoeck @Bashar24836440 @Nrg8000 @SecForceMonitor… https://t.co/GRd9oz4KDO"](https://twitter.com/LOActualControl/status/1250419566062305281)
----

### Corporates
1. [OpenCorporates](https://opencorporates.com/)
2. [Registerportal](https://www.handelsregister.de/rp_web/normalesuche.xhtml)
3. [SpiderFoot](https://www.spiderfoot.net/)
4. [https://www.wikipedia.org/](https://www.wikipedia.org/)
5. [Intelligence X](https://intelx.io/)
6. [Hunter - emails](https://hunter.io/)
7. [Tutorial Rae Baker 2020](https://youtu.be/S3qAdcgGZuQ)
8. [RiskIQ](https://www.riskiq.com/)
9. [Google Advanced Search](https://www.google.com/search?q=site%3A%28linkedin.com%2Fin+%7C+zoominfo.com%2Fp+%7C+rocketreach.co+%7C+xing.com%2Fpeople+%7C+contactout.com%29+%22Company%22&rlz=1C5CHFA_enDE931DE939&biw=1440&bih=796&tbm=nws&ei=4vkUYs2xBsuPxc8PuIu4KA&ved=0ahUKEwjNqqmPyZP2AhXLR_EDHbgFDgU4HhDh1QMIDQ&oq=site%3A%28linkedin.com%2Fin+%7C+zoominfo.com%2Fp+%7C+rocketreach.co+%7C+xing.com%2Fpeople+%7C+contactout.com%29+%22Company%22&gs_lcp=Cgxnd3Mtd2l6LW5ld3MQDFCPKFjeMGCa7g5oAHAAeAKAAUGIAdwCkgEBNpgBAKABAcABAQ&sclient=gws-wiz-news)
----

### Database
1. [Panjiva](https://panjiva.com/)
2. [SIPRI Arms Transfers Database](https://www.sipri.org/databases/armstransfers)
3. [License Plates of the World](http://www.worldlicenseplates.com/)
4. [Data Catalog](https://datacatalog.worldbank.org/)
5. [Dokukratie.de](https://www.dokukratie.de/)
6. [FragDenStaat](https://fragdenstaat.de/)
----

### Archive
1. [Using Archive.org for OSINT Investigations](https://osintcurio.us/2021/03/03/using-archive-org-for-osint-investigations/)
----

