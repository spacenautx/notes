### Law Enforcement Information Requests
1. [Facebook/Instagram Law Enforcement Online Requests - Portal](https://www.facebook.com/records/login/)
2. [Facebook Law Enforcement Online Requests - Guidelines](https://www.facebook.com/safety/groups/law/guidelines/)
3. [Twitter Law Enforcement Online Requests - Portal](https://legalrequests.twitter.com/forms/landing_disclaimer)
4. [Twitter Law Enforcement Online Requests - Guidelines](https://help.twitter.com/en/rules-and-policies/twitter-law-enforcement-support)
5. [WhatsApp Law Enforcement Request - Portal](https://www.whatsapp.com/records/login/)
6. [WhatsApp Information for Law Enforcement - Guidelines](https://faq.whatsapp.com/en/android/26000050/?category=5245250)
7. [Google/Youtube Information Requests - Guidelines](https://support.google.com/transparencyreport/answer/7381738?hl=en)
8. [Google/Youtube information Requests - Portal log in](https://lers.google.com/signup_v2/landing#/requestaccount)
9. [LinkedIn Law Enforcement Data Requests](https://www.linkedin.com/help/linkedin/answer/16880/linkedin-law-enforcement-data-request-guidelines?lang=en)
10. [Pinterest Request Guidelines](https://help.pinterest.com/en/article/law-enforcement-guidelines)
11. [VK Guidelines](https://vk.com/data_protection?section=requests)
12. [Tumblr Law Enforcement Guidelines and Requests](https://www.tumblr.com/docs/en/law_enforcement)
13. [Reddit Law Enforcement Guidelines and Reports](https://www.reddit.com/wiki/law_enforcement_guidelines)
14. [Line - User info Disclosure Requests by Law Agencies](https://linecorp.com/en/security/article/35)
15. [Snap Chat - Law Enforcement Guidelines and Contact Information](https://storage.googleapis.com/snap-inc/privacy/lawenforcement.pdf)
16. [WeChat - Law Enforcement Contact Information](https://www.wechat.com/en/contact_us.html)
17. [Viber Guidelines and Contact Information](https://www.viber.com/terms/viber-privacy-policy/#disclosure)
18. [Signal - Information Request Contact Information](https://signal.org/bigbrother/)
19. [Microsoft Request Guidelines](https://blogs.microsoft.com/datalaw/our-practices/)
20. [Apple Request Portal](https://www.apple.com/sg/privacy/government-information-requests/)
21. [Yahoo Guidelines and Contact information](https://www.eff.org/document/yahoo-law-enforcement-guide)
22. [Adobe Guidelines and Contact Information](https://www.adobe.com/legal/lawenforcementrequests.html)
23. [Report Abuse to Telegram](https://t.me/isiswatch/2)
24. [Twitch Information Requests](https://www.twitch.tv/p/legal/terms-of-service/#17-requests-for-user-information)
25. [Tiktok Law Enforcement Request Guidelines](https://www.tiktok.com/legal/law-enforcement?lang=en)
26. [Tam Tam Contact Information](https://about.tamtam.chat/en/policy-eu/)
----

### 'How to' Guides
1. [How to find information on Facebook](https://www.youtube.com/watch?v=vJvXcJBZRmw)
2. [How to find mutual friends on Facebook](https://www.aware-online.com/en/find-mutual-friends-on-facebook/)
3. [How to do a Linked In search](https://www.osintcombine.com/post/corporate-profiling-advanced-linkedin-searching-more)
4. [How to view LinkedIn profiles anonymously 1](http://expresstricks.com/how-to-view-someones-linkedin-profile-anonymously-without-account/)
5. [How to view LinkedIn profiles anonymously 2](https://www.youtube.com/watch?v=bIAdx3CAjtM)
6. [How to use Twitter Search Operators](https://learn.g2crowd.com/how-to-search-twitter)
7. [How to is use Tweetdeek](https://jakecreps.com/2018/05/19/tweetdeck/)
8. [How to search for profiles on Instagram](https://gijn.org/2019/11/15/all-you-need-to-know-to-search-like-a-pro-on-instagram/)
9. [How to search Instagram](https://osintcurio.us/2019/07/16/searching-instagram/)
10. [How to do a Reddit search](https://www.secjuice.com/reddit-osint-techniques/)
11. [How to investigate TikTok Accounts 1](https://inteltechniques.com/blog/2019/10/27/investigating-tiktok-accounts/)
12. [How to investigate TikTok Accounts 2](https://www.secjuice.com/osint-investigations-on-tiktok/)
13. [How to create an effective 'sock puppet' 1](https://jakecreps.com/2018/11/02/sock-puppets/)
14. [How to create an effective 'sock puppet' 2](https://osintcurio.us/2018/12/27/the-puppeteer/)
15. [How to do a reverse image search 1](http://www.automatingosint.com/blog/2015/05/osint-reverse-image-search/)
16. [How to do a reverse image search 2](https://www.bellingcat.com/resources/how-tos/2019/12/26/guide-to-using-reverse-image-search-for-investigations/)
17. [How to make a strong password](https://securityinabox.org/en/guide/passwords/)
18. [How to use Google Search effectively](https://bird.tools/how_to/how-to-search-online-google-basics/)
19. [How to verify online information](http://verificationhandbook.com/)
----

### Additional Resources
1. [Bellingcat OSINT landscape](https://start.me/p/ELXoK8/bellingcat-osint-landscape)
2. [Hun-OSINT](https://start.me/p/kxGLzd/hun-osint)
3. [AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
4. [NIXOSINT](https://start.me/p/rx6Qj8/start-page)
5. [TI](https://start.me/p/rxRbpo/ti)
6. [OSINT Framework](https://osintframework.com/)
7. [Netbootccamp - Open source and Social media search](https://netbootcamp.org/osinttools/)
8. [Google Search Operators](https://en.wikipedia.org/wiki/Google_hacking)
9. [Video Downloader (multiple platfroms)](https://www.dredown.com/facebook)
10. [Watch videos frame by frame](http://watchframebyframe.com)
11. [Date and Time searches](https://howlongagogo.com/)
12. [This person does not exist - Face Generator](https://thispersondoesnotexist.com/)
13. [Fake name generator](https://www.fakenamegenerator.com/)
14. [Temporary email 1](https://www.emailondeck.com/)
15. [Temporary email 2](https://temp-mail.org/en/)
16. [Global Naming Guide](https://www.fbiic.gov/public/2008/nov/Naming_practice_guide_UK_2006.pdf)
17. [OSINT Investigation Checklists](https://www.toddington.com/resources/cheat-sheets/)
18. [Firefox extensions for OSINT gathering](https://corma.de/en/iresearch-series-06-the-20-best-osint-add-ons-for-mozilla-firefox/)
19. [Google Chrome Extensions for OSINT Gathering](https://jakecreps.com/2018/05/30/chromeextensions/)
20. [Export Comments](https://exportcomments.com/)
21. [International Telephone Number Formats 1](https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers)
22. [International Telephone Number Formats 2](https://countrycode.org/)
23. [Time Stamp Converter](https://www.epochconverter.com/)
24. [Bookmark for OSINT tools](https://www.osintcombine.com/osint-bookmarks)
25. [A Guide To Social Media Intelligence Gathering (SOCMINT)](https://www.secjuice.com/social-media-intelligence-socmint/)
26. [Tools for better thinking](https://untools.co/)
----

### Twitter Content Search
1. [Find my Twitter ID](https://tweeterid.com/)
2. [User accounts details](https://foller.me/)
3. [Twitter Advanced Search](https://twitter.com/search-advanced)
4. [User profiles Directory](https://twitter.com/i/directory/profiles)
5. [User profile, content, and connection searches](http://tweetbeaver.com/)
6. [Twitter Video Downloader](http://www.downloadtwittervideo.com/)
7. [First Tweet/Link/Hashtag search](http://ctrlq.org/first/tweets/)
8. [Show (and copy) all Tweets on one page](https://www.allmytweets.net/)
9. [Check if account is likely a Bot](https://botometer.iuni.iu.edu)
10. [Create RSS Feed for Twitter Account](https://feeder.co/knowledge-base/rss-feed-creation/twitter-rss-feeds/)
11. [Search Twitter Bios](https://followerwonk.com/bio)
12. [Scrape conversations](https://threadreaderapp.com/)
13. [Followerwonk: Sort your followers, ranking your Twitter social graph by follower count, Social Authority, more](https://followerwonk.com/sort)
----

### Twitter Geolocation
1. [Geolocation search](https://www.tweeplers.com/map/)
2. [Geolocation search (if location services tuned on by user)](http://geosocialfootprint.com/)
3. [Visualise hashtags and tweets - global map](https://www.mapd.com/demos/tweetmap)
4. [Latitude and longitude search guide](https://www.hrmarketer.com/blog/2016/03/how-to-search-tweets-by-location-twittertips/)
----

### Twitter Analytics (paid services with free trials)
1. [Analysis of Twitter Accounts](https://accountanalysis.lucahammer.com/)
2. [Twitonomy: Twitter #analytics and much more...](http://www.twitonomy.com/)
3. [Hashtag Analytics for Twitter, Instagram and Facebook](http://keyhole.co/)
4. [Twitterfall](https://twitterfall.com/)
5. [Twitter Analytics for Tweets, Timelines & Twitter Maps](https://socialbearing.com/)
6. [Mentionmapp Analytics](http://mentionmapp.com/)
7. [Sleeping Time](http://sleepingtime.org/)
8. [Spoonbill](http://spoonbill.io/)
9. [Twlets](http://twlets.com/)
----

### Reddit Search
1. [User search](http://www.redditinvestigator.com/)
2. [User search](https://atomiks.github.io/reddit-user-analyser/)
3. [Content Search - Filter 1](https://imgur.com/search?q=)
4. [Content Search - Filter 2](https://redditsearch.io/)
5. [Content search - Directory](http://files.pushshift.io/reddit/)
6. [User information](https://snoopsnoo.com/)
7. [Video Downloader](https://redv.co/)
8. [Export comments/saved items (Login needed)](https://redditmanager.com/)
9. [Phrase tracking and alerts (Login needed)](https://www.trackreddit.com/)
10. [Reverse image search](http://karmadecay.com/?__cf_chl_jschl_tk__=79c7cb5cfc63fdba3df62a6438529355a2d95c4b-1584412891-0-AQUeHY3W0Ikazh5jCg1fcw4Q5sdVODf9ceDX4IjFJ1kZlu-RrxMVuqcp54TMpsXVNRCUt1Hro70lLjTgt04sYFIV_i9plhpNnzDOqA8l97_7Z00EpooxTDOd2cJ4C2uSwJrlWpWNE71TVW5fDg-7OzRQ3zd3KlEvPtmtcCBouW5oTaFbUJa14KEyEQOHimuhDHrjVO8LIH-JWDBgBhEl2tV6BnYwph6mKymsDMtAziR9kVb1I87EO_oZCRXtGLlwuvTF9AKz_lQhEs801VHoS-WOFtCckIsWVspgF3M-XaImsi0ujgQTXzMzQGlESqlIqehELVcNtVbyku-4OJjcv60)
----

### VKontakte
1. [Profile search](http://spotlight.svezet.ru/)
2. [Поиск ВК](http://vk5.city4me.com)
3. [Reverse image search](https://findclone.ru/)
----

### Periscope Search
1. [Download videos from Periscope](https://downloadperiscopevideos.com/index.php)
----

### Tumblr Search
1. [Tumblr search](http://www.tumblr.com/search)
----

### Facebook Search
1. [Find my Facebook ID](https://lookup-id.com/)
2. [Find me Facebook ID - multiple uses](https://seotoolstation.com/bulk-facebook-id-finder)
3. [Facebook Advanced Search (Must be logged in)](https://www.facebook.com/search/top/?q=enter%20search%20query%20here&epa=SEARCH_BOX)
4. [Basic Graph Search  (Must be logged in)](https://sowdust.github.io/fb-search/)
5. [User content - Who posted what? (Must be logged in)](http://whopostedwhat.com/)
6. [Key Word Search by Date (Must be logged in)](https://intelx.io/tools?tab=facebook)
7. [Facebook Video Downloader](https://fdownloader.net/en)
8. [User Directory](https://en-gb.facebook.com/directory/people_remain/)
9. [Search Facebook Pages](https://www.osintcombine.com/facebook-geo-pages)
----

### Instagram Search
1. [Find my Instagram ID](https://codeofaninja.com/tools/find-instagram-user-id)
2. [Search Accounts and Hashtags from Instagram](https://sometag.org/)
3. [Search Accounts and Hashtags 2](https://picbabun.com/)
4. [Search user bios](http://searchmy.bio/)
5. [Search location of posts](http://worldc.am)
6. [Instagram video and image downloader](https://downloadgram.com/)
7. [Download Stories](https://storiesig.com/)
----

### Youtube Search
1. [Video Data Information](https://intelx.io/tools?tab=youtube)
2. [YouTube video downloader](https://en.savefrom.net/1-how-to-download-youtube-video/)
3. [Find video ID and upload information](https://citizenevidence.amnestyusa.org/)
4. [Download video comments](http://ytcomments.klostermann.ca/)
5. [Bypass age and login restrictions](http://nsfwyoutube.com/)
6. [Check country restriction of video](http://polsy.org.uk/stuff/ytrestrict.cgi)
----

### LinkedIn
1. [User and occupation search](https://freepeoplesearchtool.com/)
2. [User search](https://recruitin.net/)
3. [List of Search operaters](https://booleanstrings.com/2018/12/06/linkedin-operators-one-more-tip-sheet/)
4. [https://recruitin.net](http://Keyword and Location Search.com)
----

### TikTok Search
1. [TikTok Quick Search](https://www.osintcombine.com/tiktok-quick-search)
2. [TikTok Hashtag Search](https://www.tiktokdom.com/)
3. [Search Users and Hashtags](https://vidnice.com/)
4. [Tiktok Web Viewer](https://www.tiktokdom.com/)
5. [Download TikTok Videos](https://ttdown.org/)
6. [User analytics](https://cloutmeter.com/)
----

### Telegram Channels Search
1. [Search public channels](https://tgstat.com/)
2. [Text search in Channels](https://tgstat.ru/en/search)
3. [Link search](https://cse.google.com/cse?q=+&cx=006368593537057042503:efxu7xprihg#gsc.tab=0&gsc.q=%20&gsc.page=1)
4. [Search for channels and groups](https://lyzem.com/)
----

### Snapchat Search
1. [Global map of recently public Snapchat content](http://map.snapchat.com/)
2. [Search Snap chat User names](http://snapchat-usernames.com/)
----

### Pinterest Search
1. [Pinterest group board filter](http://pingroupie.com/)
----

### People Search Engines
1. [Pipl](https://pipl.com/)
2. [Spokeo](https://www.spokeo.com/)
3. [PeekYou.com](https://www.peekyou.com/)
4. [Find People for Free](https://thatsthem.com/)
5. [Qwant](https://www.qwant.com/)
6. [FastPeopleSearch.com](https://www.fastpeoplesearch.com/)
7. [People and Business search - Public records  - Background check](https://radaris.com/)
8. [Canada Phone Directory Search](http://www.canada411.ca/)
9. [US Phone Directory Search](https://www.411.com/)
10. [192.com](http://www.192.com/)
11. [Fold3 - US military records](https://www.fold3.com/)
12. [Family Tree now - US only](https://www.familytreenow.com/)
13. [Cluster Maps - US only](https://clustrmaps.com)
----

### User Name Search
1. [Know'em?](https://knowem.com/)
2. [Name Checkup](https://namecheckup.com/)
3. [Lullar Com](http://com.lullar.com/)
4. [Namevine](https://namevine.com/)
5. [Username Search](https://usersearch.org/)
6. [Free Username Search](https://www.peekyou.com/username)
7. [www.usersherlock.com/usersearch](http://www.usersherlock.com/usersearch/)
8. [WhatsMyName Web](https://whatsmyname.app/)
----

### Email Searches
1. [dehashed.com/](https://dehashed.com/)
2. [Email Assumptions](https://inteltechniques.com/osint/email.html)
3. [Email checker: Searches email via Social Networks](https://www.manycontacts.com/en/mail-check)
4. [Gravatar - Email Search](https://en.gravatar.com/site/check/)
5. [Have I been pwned? Check if your email has been compromised in a data breach](https://haveibeenpwned.com/)
6. [MailTester.com](http://mailtester.com/testmail.php)
7. [Verify Email Address - Email Hippo](https://tools.verifyemailaddress.io/)
8. [Verify Email Address Online - Free Email Verifier](https://verify-email.org/)
----

### Phone Number
1. [Truecaller](https://www.truecaller.com/search/us/515-419-8701)
2. [Advanced Background Checks](https://www.advancedbackgroundchecks.com/phone/)
3. [Best Phone Lookup](http://phonesearch.us/login.php)
4. [CarrierLookup | Find Cell Phone Carrier For Free &amp; Carrier Lookup API](https://www.carrierlookup.com/)
5. [CNAM and caller name lookup service provider - CID(name)Professional cnam service provider for asterisk, freeswitch, opensips, and all VoIP devices](http://www.cidname.com/)
6. [Data24-7 - Data on Demand.](https://www.data24-7.com/)
7. [Facebook](https://www.facebook.com/)
8. [Infobel - Phone directory](https://www.infobel.com/)
9. [Makelia - Public Records Repository](https://makelia.com/)
10. [NANPA : Area Code Map](https://www.nationalnanpa.com/area_code_maps/ac_map_static.html)
11. [NANPA Area Code Query](https://www.nationalnanpa.com/enas/npa_query.do)
12. [Numberway - Free White Pages - People Search - International Phone Books](https://www.numberway.com/)
13. [Pipl - Phone Number](https://pipl.com/)
14. [Reverse Phone Lookup - Find Name, Address &amp; More For Any Phone Number - Addresses.com](http://www.addresses.com/phone)
15. [slydial](https://www.slydial.com/)
16. [Spy Dialer](https://www.spydialer.com/default.aspx)
17. [SpyTox](https://www.spytox.com/)
18. [Superpages Online Yellow Pages, Local Business Directory](https://www.superpages.com/)
19. [Sync.ME - Caller ID &amp; Phone Number Search](https://sync.me/)
20. [TextMagic](https://www.textmagic.com/free-tools/carrier-lookup)
21. [Thats Them](https://thatsthem.com/reverse-phone-lookup)
22. [True People Search](https://www.truepeoplesearch.com/results?phoneno=(515)419-8701)
23. [US Phonebook](https://www.usphonebook.com/515-419-8701/detailed)
24. [White Pages | People Finder - AnyWho](https://www.anywho.com/reverse-lookup)
25. [Whocalld](https://whocalld.com/)
----

