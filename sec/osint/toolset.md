### Monitoring
1. [Trendsmap - Real-time local Twitter trends](https://www.trendsmap.com/)
2. [Google Trends](https://trends.google.com/trends/)
3. [Nuzzel](http://nuzzel.com/)
4. [Trends24](https://trends24.in/)
5. [osint mindmaps](https://github.com/WebBreacher/osinttools)
----

### Twitter
1. [Twitter Advanced Search](https://twitter.com/search-advanced)
2. [TweetDeck](https://tweetdeck.twitter.com/#)
3. [Twitter List Copy](http://projects.noahliebman.net/listcopy/index.php)
4. [Twitter lists via Google](https://www.google.de/search?source=hp&ei=FnsEWuWMIZKpsAfVmYuIDg&q=site%253Atwitter.com%252F*%252Flists%252F&oq=site%253Atwitter.com%252F*%252Flists%252F&gs_l=psy-ab.12...915.915.0.2778.2.1.0.0.0.0.68.68.1.1.0....0...1.1.64.psy-ab..1.0.0.0...0.ayCcVVziq3I)
5. [Twxplorer](https://twxplorer.knightlab.com/)
6. [First Tweet](http://ctrlq.org/first/)
7. [Locate tweets](http://qtrtweets.com/twitter/)
8. [Twitter Videos download](http://www.downloadtwittervideo.com/de/)
9. [TweetBeaver (Source)](http://tweetbeaver.com/)
10. [Foller.me (Source)](https://foller.me/)
11. [Snap Bird (Source)](https://snapbird.org/)
12. [Followerwonk (Source)](https://moz.com/followerwonk/analyze)
13. [Mentionmapp](http://mentionmapp.com/)
14. [Epoch Converter](https://www.epochconverter.com/)
15. [Tweetreach](https://tweetreach.com/)
16. [Onemilliontweetmap](http://www.onemilliontweetmap.com/)
17. [Spoonbill (Bio change ´16)](http://spoonbill.io/data/)
----

### Facebook
1. [Facebook Graph](http://graph.tips/)
2. [Facebook Scanner](http://www.stalkscan.com/)
3. [FB Signal](https://signal.fb.com/#/dashboard)
4. [Peoplefindthor](https://peoplefindthor.dk/)
5. [Search is Back!](https://searchisback.com/)
6. [Facebook Live Map](https://www.facebook.com/live#@50.911685901,6.964047646000012,3z)
7. [Facebook Video Downloader](https://www.fbdown.net/)
8. [Aged Facebook Accounts For Sale – Buy Top Rated Old Facebook Aged Accounts on Sale](https://fbaccs.com)
----

### AWS / Dumps
1. [sa7mon/S3Scanner](https://github.com/sa7mon/S3Scanner)
2. [initstring/cloud_enum](https://github.com/initstring/cloud_enum)
3. [GitHub - Ibonok/elastic_scan: Dump elasticsearch instance](https://github.com/Ibonok/elastic_scan)
4. [woj-ciech/LeakLooker](https://github.com/woj-ciech/LeakLooker)
5. [eth0izzle/bucket-stream](https://github.com/eth0izzle/bucket-stream)
6. [opens3buckets](https://blog.rapid7.com/2013/03/27/open-s3-buckets/)
----

### Subdomains
1. [Find subdomains using Project Sonar by Rapid7 – Marduc812](http://marduc812.com/2018/12/19/find-subdomains-using-project-sonar-by-rapid7/)
----

### Primarys
1. [PacketTotal - IOC Search (Experimental)](https://www.packettotal.com/app/ioc_search)
2. [RISKIQ EP](https://www.digitalguardianservices.com/a/main/index#/dashboard/events)
3. [IRIS-H Digital Forensics](https://iris-h.services/#/pages/dashboard)
4. [Free Automated Malware Analysis Service](https://www.hybrid-analysis.com/yara-search)
5. [pulsedive](https://pulsedive.com/)
6. [ROBTEX](https://www.robtex.com/)
7. [https://research.domaintools.com/iris/](https://research.domaintools.com/iris/)
8. [https://research.domaintools.com/bulk-parsed-whois/](https://research.domaintools.com/bulk-parsed-whois/)
9. [RiskIQ Community Edition](https://community.riskiq.com/search)
10. [dehashed](https://www.dehashed.com/)
11. [Search - urlscan.io](https://urlscan.io/search/#*)
12. [VirusTotal](https://www.virustotal.com/gui/home/search)
13. [https://app.riskiq.net/a/main/index#/events/search/eventType+=+%22BRAND_IN_DOMAIN%22+|+reviewCode+in+(%22NEW%22,+%22SUSPECT%22,+%22Confirmed%22,+%22TENACIOUS%22,+%22MONITOR%22)+|+$(1,1,25,1,createdAt,6050)$](https://app.riskiq.net/a/main/index#/events/search/eventType+=+%22BRAND_IN_DOMAIN%22+|+reviewCode+in+(%22NEW%22,+%22SUSPECT%22,+%22Confirmed%22,+%22TENACIOUS%22,+%22MONITOR%22)+|+$(1,1,25,1,createdAt,6050)$)
14. [Interactive Online Malware Analysis Sandbox](https://app.any.run/)
15. [We Leak Info](https://weleakinfo.com/)
16. [Global Conflict Tracker l Council on Foreign Relations](https://www.cfr.org/interactives/global-conflict-tracker#!/)
17. [World Intelligence Guide](https://www.globalsecurity.org/intell/world/index.html)
18. [Democracy](https://ourworldindata.org/democracy)
19. [Political Regime - Our World in Data](https://ourworldindata.org/grapher/political-regime-updated2016?country=AUS)
20. [Sign in • Hunter](https://hunter.io/search)
21. [OCCRP](https://www.occrp.org/en)
22. [Catalogue of Research Databases](https://investigativedashboard.org/databases/)
23. [Bellingcat's Online Investigation Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
24. [OSINT Framework](https://osintframework.com/)
25. [wikileaks.org](https://wikileaks.org/)
26. [ATT&CK™ Navigator](https://mitre-attack.github.io/attack-navigator/enterprise/)
27. [Malware Families](https://malpedia.caad.fkie.fraunhofer.de/families)
28. [Admiralty code](https://en.wikipedia.org/wiki/Admiralty_code)
29. [APT Groups and Operations](https://docs.google.com/spreadsheets/d/1H9_xaxQHpWaa4O_Son4Gx0YOIzlcBWMsdvePFX68EKU/edit#gid=361554658)
30. [www.dni.gov/index.php/cyber-threat-framework](https://www.dni.gov/index.php/cyber-threat-framework)
31. [The Breached Database Search Engine](https://nuclearleaks.com/index)
32. [Log in](https://leakedsource.ru/main/login/)
33. [We Leak Info](https://weleakinfo.com/)
34. [https://www.ncsc.gov.uk/guidance/nis-directive-cyber-assessment-framework](https://www.ncsc.gov.uk/guidance/nis-directive-cyber-assessment-framework)
35. [InfoDIG.is](http://infodig.is)
36. [https://www.virustotal.com/graph/](https://www.virustotal.com/graph/)
37. [Digital Shadows](https://portal-digitalshadows.com/client/explore/data-breaches)
38. [ThreatStream](https://ui.threatstream.com/dashboard?type=overview)
39. [Intel471](https://titan.intel471.com/search/FreeText:customer%20details/reports?ordering=latest)
40. [OSINT RESOURCE](https://docs.google.com/spreadsheets/d/1JxBbMt4JvGr--G0Pkl3jP9VDTBunR2uD3_faZXDvhxc/edit#gid=164143315)
----

### Secure Transfer
1. [Whisply](https://whisp.ly/en?)
----

### Organisation
1. [OpenCorporates](https://opencorporates.com/)
----

### Hunting
1. [VirusTotal](https://www.virustotal.com/gui/hunting/rulesets)
----

### Code
1. [searchcode](https://searchcode.com/)
----

### Original / Context
1. [Google Images](https://images.google.com/)
2. [Yandex images](https://yandex.com/images/?redircnt=1515665874.1)
3. [Baidu images](https://image.baidu.com/)
4. [Bing images](https://www.bing.com/?scope=images&nr=1&FORM=NOFORM)
5. [TinEye](https://www.tineye.com/)
6. [Google Advanced Search](https://www.google.com/advanced_search)
----

### Source
1. [Pipl](https://pipl.com/)
2. [PeekYou](https://www.peekyou.com/)
3. [IntelTechniques](https://inteltechniques.com/osint/menu.name.html)
4. [Whois](https://www.whois.com/)
5. [Denic](https://www.denic.de/webwhois/)
6. [Whitepages](https://www.whitepages.com/)
7. [Internet Archive Wayback Machine](https://archive.org/web/)
8. [FullContact Dashboard](https://dashboard.fullcontact.com/)
----

### Youtube
1. [Data Viewer](http://citizenevidence.amnestyusa.org/)
2. [Geo Search Tool](http://youtube.github.io/geo-search-tool/search.html)
3. [Deturl](http://deturl.com/)
4. [Watch Frame by Frame](http://www.watchframebyframe.com/)
----

### Instagram / Tumblr / Reddit
1. [Instagram search](https://www.instagram.com/juannaba/)
2. [Signal-Instagram](https://signal.fb.com/#/curation/instagram)
3. [Websta.me/ID of Location](https://websta.me/)
4. [Reddit (Source)](https://atomiks.github.io/reddit-user-analyser/)
5. [Subreddits](https://www.reddit.com/subreddits)
6. [KarmaDecay -Reddit Image](http://karmadecay.com/)
7. [SnapMap](https://map.snapchat.com/@50.958500,6.924390,12.00z)
8. [osi.ig](https://github.com/th3unkn0n/osi.ig/blob/master/README.md)
----

### Domain
1. [OWASP/Amass](https://github.com/OWASP/Amass)
2. [jakecreps/swamp](https://github.com/jakecreps/swamp)
3. [stefanoj3/dirstalk](https://github.com/stefanoj3/dirstalk)
4. [OJ/gobuster](https://github.com/OJ/gobuster)
5. [expireddomains](https://member.expireddomains.net/)
6. [elceef/dnstwist](https://github.com/elceef/dnstwist)
----

### pastes
1. [PasteBeen - Homepage](https://pastebeen.com/)
----

### pastes
1. [Online IDE and Paste Search Tool](https://tools.redhuntlabs.com/online-ide-and-paste-search-tool.html)
----

### Country Threat
1. [OSAC](https://www.osac.gov/)
2. [National Terrorism Threat Advisory System | 									Australian National Security](https://www.nationalsecurity.gov.au/Securityandyourcommunity/Pages/National-Terrorism-Threat-Advisory-System.aspx)
3. [Global Peace index](https://www.visionofhumanity.org/maps/#/)
4. [Smartraveller](https://www.smartraveller.gov.au/)
5. [The World Factbook](https://www.cia.gov/the-world-factbook/)
----

### Date & Time
1. [WolframAlpha](http://www.wolframalpha.com/)
2. [SunCalc](http://suncalc.net/#/51.508,-0.125,2/2017.11.10/11:41)
3. [World time](https://www.timeanddate.de/)
----

### Metadata
1. [FotoForensics](http://fotoforensics.com/)
2. [EXIF Data Viewer](http://exifdata.com/)
3. [Image Metadata](http://exif.regex.info/exif.cgi)
4. [IrfanView](http://www.irfanview.com/)
5. [Exif viewer](http://metapicz.com/#landing)
6. [Forensic Analyzer](https://www.imageforensic.org/#work)
7. [Forensically](https://29a.ch/photo-forensics/#forensic-magnifier)
8. [Diff Checker](https://www.diffchecker.com/image-diff)
9. [Logo detection](http://logos.iti.gr/)
10. [GitHub - Ge0rg3/StegOnline: A web-based, accessible and open-source port of StegSolve.](https://github.com/Ge0rg3/StegOnline)
11. [FOCA](https://www.elevenpaths.com/labstools/foca/index.html)
12. [https://github.com/GuidoBartoli/sherloq](https://github.com/GuidoBartoli/sherloq)
----

### Email
1. [laramies/theHarvester](https://github.com/laramies/theHarvester)
2. [m4ll0k/Infoga](https://github.com/m4ll0k/infoga)
3. [MailDB](https://dash.maildb.io/search/domain)
4. [Free Email Search](http://www.reversegenie.com/email.php)
5. [Skymem web page](http://www.skymem.info/)
6. [email-format](https://www.email-format.com)
7. [Email Permutator](http://metricsparrow.com/toolkit/email-permutator/)
8. [Free Email Search](http://www.reversegenie.com/email.php)
9. [khast3x/h8mail](https://github.com/khast3x/h8mail)
10. [Emailrep.io](https://emailrep.io/)
----

### Breach Search
1. [dehashed](https://www.dehashed.com)
2. [ghostproject](https://ghostproject.fr/)
3. [Leak - Lookup | Databases](https://leak-lookup.com/databases)
4. [We Leak Info](https://weleakinfo.com/)
5. [Doxbin](https://doxbin.org/)
----

### Money Search
1. [Unclaimed Moneys Search - Treasury](https://apps.treasury.act.gov.au/unclaimedmoneys/search)
----

### Age Calculator
1. [Reverse Age Calculator - Find date of birth from age number](https://everydaycalculation.com/reverse-age.php)
----

### Usernames
1. [Check Usernames](https://checkusernames.com/)
----

### General Tools
1. [Wayback Machine](https://archive.org/web/)
2. [OSINT Framework](http://osintframework.com/)
3. [Street signs](https://en.wikipedia.org/wiki/Comparison_of_European_road_signs#Warning)
4. [Quick Name Generator](https://www.name-generator.org.uk/quick/)
----

### Tutorials / Toollists
1. [Truly Media - collaborative verification platform](http://www.truly.media/)
2. [How to find breaking news on Twitter](https://firstdraftnews.com/how-to-find-breaking-news-on-twitter-social-media-journalism/)
3. [Newsgathering Twitter](http://sarahmarshall.io/post/115007936164/15-tips-for-newsgathering-via-twitter)
4. [Twitter Location](https://medium.com/1st-draft/quick-guide-finding-eyewitness-media-from-an-exact-location-on-twitter-1fa28f48838a)
5. [Twitter lists](https://medium.com/1st-draft/starting-from-scratch-twitter-lists-expose-the-heart-of-a-story-d1f8bee73e9d)
6. [Video Verification](https://www.bellingcat.com/resources/how-tos/2017/06/30/advanced-guide-verifying-video-content/)
7. [Facebook secrets](http://researchclinic.net/facebooksecrets/)
8. [Google search](https://support.google.com/websearch/answer/2466433?p=adv_operators&hl=en&rd=1)
9. [SnapMap](https://firstdraftnews.com/snapchat-map-newsgathering/)
10. [SnapMap1](http://www.niemanlab.org/2017/11/snap-maps-offered-real-time-coverage-of-tuesdays-terror-attacks-in-manhattan-plus-a-lot-of-emoji/?utm_content=bufferf62ae&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
11. [Verification Handbook](http://verificationhandbook.com/)
12. [Bellingcat](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
13. [Bit.ly/VerificationToollist](http://bit.ly/VerificationToollist)
14. [Checklist - A roadmap to the truth](https://revealproject.eu/a-roadmap-to-the-truth/)
15. [Search videos via Google](https://www.bellingcat.com/resources/how-tos/2017/10/17/conduct-comprehensive-video-collection/)
16. [Verification Handbook - Toollist](https://docs.google.com/document/d/14jugVfxt3DLi2IlrevFKlXRg9C4Dl93Cvs73LxJmwqQ/edit)
----

### Cards / Clearnet
1. [hackforums](https://hackforums.net/)
2. [centralshop CVV Shop, Best CC Dumps. Dumps With Pin Avaible. Trusted Vendors](https://centralshop.cn/ru/#/profile)
3. [Ferum-Shop.net | Login](http://www.fe-acc18.ru/store)
4. [uas-service.ru](http://uas-service.ru/)
5. [Cardz.su](http://cardz.su)
6. [cardhouse.cc](http://cardhouse.cc/)
7. [CARDERBAY.COM 2.0](https://carderbay.com)
8. [PARAMOUNTEMPORIUM.VIP](http://paramountemporium.vip/)
----

### Malware
1. [http://fireeye/flare-vm.com](http://fireeye/flare-vm.com)
----

### cpanel access
1. [https://verox.io/home](https://verox.io/home)
----

### IOT
1. [GitHub - IoT-PTv/IoT-PT: A Virtual environment for pentest IoT Devices](https://github.com/IoT-PTv/IoT-PT)
----

### VMware Images
1. [Trace Labs OSINT VM - Trace Labs](https://www.tracelabs.org/trace-labs-osint-vm/)
2. [Buscador](https://drive.google.com/file/d/17R54eNh-GNKa_UNKFzrnw90AwA6LWCRW/view)
----

