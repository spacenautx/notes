### Torrents
1. [Piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion](http://piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion/)
----

### Leak Tests
1. [elfq2qefxx6dv3vy.onion/binfo_check_anonymity.php](http://elfq2qefxx6dv3vy.onion/binfo_check_anonymity.php)
2. [donionsixbjtiohce24abfgsffo2l4tk26qx464zylumgejukfq2vead.onion/test.php](http://donionsixbjtiohce24abfgsffo2l4tk26qx464zylumgejukfq2vead.onion/test.php)
----

### Deep Links DIR
1. [deep search](http://xjypo5vzgmo7jca6b322dnqbsdnp3amd24ybx26x5nxbusccjkm4pwid.onion/)
2. [Tor2Web: Tor Hidden Services Gateway](https://eu.onionsearchengine.com/)
3. [Tor2web.onionsearchengine.com](https://tor2web.onionsearchengine.com/)
4. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion/)
5. [Deep Links Dump - Uncensored Deep Web Link Directory](http://deepqelxz6iddqi5obzla2bbwh5ssyqqobxin27uzkr624wtubhto3ad.onion/)
6. ["The Safe Onion Links"](https://safeonu4c4xme2kzhuaer4ucm7enrjv23oznzwkz4d4f2wm7cc4ucuid.onion)
7. [DeepLink Onion Directory](http://mega7hxbfrjay3qkavy6jw2hw6lvkanaojxuottd45m4i4qr5wdenyad.onion/)
8. [Tasty Onions - Deep Web Link Directory](http://22tojepqmpah32fkeuurutki7o5bmb45uhmgzdg4l2tk34fkdafgt7id.onion/)
9. [DarkDir](http://l7vh56hxm3t4tzy75nxzducszppgi45fyx2wy6chujxb2rhy7o5r62ad.onion/)
10. [Onion Link Directory](http://torlinkuqd6ntr57y35m53pitu3qzfdbmhpkqmzn7yunzuwzrxjpmmyd.onion/link-list.html)
11. [Dark-Catalog](http://catalogxlotjjnu6vyevngf675vu6beefzxw6gd6a3fwgmgznksrqmqd.onion/forums.html)
12. [Link list v3.2.](http://torv5dp5n46htkqu42r3ywtt75ll2zu24a3yhcsgm6aslp43woclc2id.onion/)
13. [Ableonion Links](http://notbumpz34bgbz4yfdigxvd6vzwtxc3zpt5imukgl6bvip2nikdmdaad.onion/links/)
14. [MISC](http://omensx7ucu4gossi2nwn3l227ka73vsob36yytr3oe32u63ftgi4t4qd.onion/Links/links.html)
15. [COOL directory access](http://uzowkytjk4da724giztttfly4rugfnbqkexecotfp5wjc2uhpykrpryd.onion)
----

### onion forums
1. [darknetlive](http://darkzzx4avcsuofgfez5zq75cqc4mprjvfqywo45dfcaxrwqg6qrlfid.onion/onions/)
2. [DIR of Links for Search Engines, Catalogs, Forums, News, ETC](http://reycdxyc24gf7jrnwutzdn3smmweizedy7uojsa7ols6shttp://reycdxyc24gf7jrnwutzdn3smmweizedy7uojsa7ols6sflwu25ijoyd.onion/tags/flwu25ijoyd.onion/2019/11/14/onionlinks/#Forums)
3. [$$$ DARK0DE - market, databases, etc](http://darkodemard3wjoe63ld6zebog73ncy77zb2iwjtdjam4xwvpjmjitid.onion/search/Database/all/1)
4. [Helium Forum & Marketplace](http://7qiouusobthfxjw47albrqffk4lbqwauqlu6dpwh4kosddqt2x26ocad.onion/#)
5. [$$$ DARKNETFORUM](http://7qiouusobthfxjw47albrqffk4lbqwauqlu6dpwh4kosddqt2x26ocad.onion/#)
6. [$$$ HiddenForum](http://hiddenuip5qlthdkbeqrpcfja4k5qr5urordvm4sm3gnz6wcy7yo5qqd.onion/forum)
7. [Dread - Forum :)](http://dreadytofatroptsdj6io7l3xptbet6onoyno2yv7jicoxknyazubrad.onion/)
8. [XSS.IS forum on Darkenet](http://xssforolrt4zlzqsvinl5tg3wgx33nc2abrvmrreauvafy4jxqseomad.onion/)
----

### Note Taking / Text Hosting (pastes)
1. [Cryptornetzamrhytcxhr3ekth6vom4ewns7pqxtywfvn5eezxgcqgqd.onion](http://cryptornetzamrhytcxhr3ekth6vom4ewns7pqxtywfvn5eezxgcqgqd.onion/)
2. [Black Cloud - Image Hosting](http://bcloudwenjxgcxjh6uheyt72a5isimzgg4kv5u74jb2s22y3hzpwh6id.onion/)
3. [BlackHost](https://blackhost7pws76u6vohksdahnm6adf7riukgcmahrwt43wv2drvyxid.onion/?id=pst)
4. [BlackHost](https://blackhost7pws76u6vohksdahnm6adf7riukgcmahrwt43wv2drvyxid.onion/?id=pst)
----

### Bookmarks misc onion
1. [menu](http://cgjzkysxa4ru5rhrtr6rafckhexbisbtxwg2fg743cjumioysmirhdad.onion/bookmarks.html)
2. [http://skhz5s6z5dpjz3flxudesptuc53lsfykjjzk6mwxabssrgqwz6k7cyid.onion/all-tor-sites-in-hidden-wiki.html](http://skhz5s6z5dpjz3flxudesptuc53lsfykjjzk6mwxabssrgqwz6k7cyid.onion/all-tor-sites-in-hidden-wiki.html)
3. [Wiki Tor List](http://ub6bby4x2djuerj4i2cbxnzmef566zgb2f7x6iqnu6dzwznxw2tscmqd.onion/)
----

### Onion Search Engines / Links
1. [Venusoseaqnafjvzfmrcpcq6g47rhd7sa6nmzvaa4bj5rp6nm5jl7gad.onion](https://venusoseaqnafjvzfmrcpcq6g47rhd7sa6nmzvaa4bj5rp6nm5jl7gad.onion)
2. [OURREALM - search beyond censorship](http://orealmvxooetglfeguv2vp65a3rig2baq2ljc7jxxs4hsqsrcemkxcad.onion/)
3. [Phobos](http://phobosxilamwcg75xt22id7aywkzol6q6rfl2flipcqoc4e4ahima5id.onion/)
4. [dark.fail (limited)](http://darkfailenbsdla5mal2mxn2uz66od5vtzd5qozslagrfzachha3f3id.onion/)
5. [Ahmia](http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/)
6. [The Hidden Wiki](http://hiddenwwiqg2jb5s3wyvzxeipcl5ese2pa2cqnu6myi3d5bcmhmdagqd.onion/index.php/Main_Page)
7. [Dark Eye - profiles on the darknet, pgp keys, reviews](http://darkeyepxw7cuu2cppnjlgqaav6j42gyt43clcn4vjjf7llfyly5cxid.onion/)
8. [darkweblink.com onion version](http://dwltorbltw3tdjskxn23j2mwz2f4q25j4ninl5bdvttiy4xb6cqzikid.onion/)
9. [Digdeep4orxw6psc33yxa2dgmuycj74zi6334xhxjlgppw6odvkzkiad.onion](http://digdeep4orxw6psc33yxa2dgmuycj74zi6334xhxjlgppw6odvkzkiad.onion/)
10. [http://ankexpn6vk3qc5ooyyj7ufi6nmyt44vxbjtbxxkq4bxo7xzghai7kiqd.onion/ANK%20Stash%20-%20%E2%9A%A1hacking%E2%9A%A1%20-%20leaked-data%20[861360279076864090].html](http://ankexpn6vk3qc5ooyyj7ufi6nmyt44vxbjtbxxkq4bxo7xzghai7kiqd.onion/ANK%20Stash%20-%20%E2%9A%A1hacking%E2%9A%A1%20-%20leaked-data%20[861360279076864090].html)
11. [TorBot Search](http://torbotzotnpygayi724oewxnynjp4pwumgmpiy3hljwuou3enxiyq3qd.onion/)
12. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion/)
13. [TOR66](http://tor66sewebgixwhcqfnp5inzp5x5uohhdy3kvtnyfxc2e5mxiuh34iid.onion/)
14. [Deep Search - Search Engine](http://search7tdrcvri22rieiwgi5g46qnwsesvnubqav2xakhezv4hjzkkad.onion/)
15. [Torch: The Original Tor Search Engine](http://torchdeedp3i2jigzjdmfpn5ttjhthh5wbmda2rr3jvqjg5p77c54dqd.onion/search?query=forum)
16. [SearchDemon](http://srcdemonm74icqjvejew6fprssuolyoc2usjdwflevbdpqoetw4x3ead.onion/)
17. [Torgle - The Dark Web Search Engine](http://iy3544gmoeclh5de6gez2256v6pjh4omhpqdh2wpeeppjtvqmjhkfwad.onion/torgle/)
18. [ThirdEye Search Engine](http://3666eyedb2c6dlvtys4nkjfxnyfjjielks2fijvmdivqp2noku2fzqqd.onion/)
19. [EXCAVATOR search engine](http://2fd6cemt4gmccflhm6imvdfvli3nf7zn6rfrwpsy7uhxrgbypvwf5fad.onion/)
20. [Kraken Search Engine](http://krakenai2gmgwwqyo7bcklv2lzcvhe7cxzzva2xpygyax5f33oqnxpad.onion/)
21. [Tor SE](http://torse2ghyb4ytifb2yb7wxbttembxzhpvvoybgkrjyvwl2hutephs3yd.onion/)
22. [SENTOR search engine](http://e27slbec2ykiyo26gfuovaehuzsydffbit5nlxid53kigw3pvz6uosqd.onion/)
23. [OSS Onion Search Server](http://3fzh7yuupdfyjhwt3ugzqqof6ulbcl27ecev33knxe3u7goi3vfn2qqd.onion/oss/)
24. [Tor2Web: Tor Hidden Services Gateway](https://tor2web.onionsearchengine.com/)
25. [MetaGer -German Search Engine](http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion/)
26. [FindTor](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/)
27. [Dark Search Enginer](http://l4rsciqnpzdndt2llgjx3luvnxip7vbyj6k6nmdy4xs77tx6gkd24ead.onion/)
28. [abiko](http://abikoifawyrftqivkhfxiwdjcdzybumpqrbowtudtwhrhpnykfonyzid.onion/)
29. [GDARK Search Engine](http://zb2jtkhnbvhkya3d46twv3g7lkobi4s62tjffqmafjibixk6pmq75did.onion/gdark/search.php)
30. [haystak](http://haystak5njsmn2hqkewecpaxetahtwhsbsa64jom2k22z5afxhnpxfid.onion/)
31. [KILOS](http://mlyusr6htlxsyc7t2f4z53wdxh3win7q3qpxcrbam6jf3dmua7tnzuyd.onion/search)
32. [Venus Search Engine](http://venusoseaqnafjvzfmrcpcq6g47rhd7sa6nmzvaa4bj5rp6nm5jl7gad.onion/Search?Query=DATA%20LEAK)
33. [TorDex Search Engine](http://tordexu73joywapk2txdr54jed4imqledpcvcuf75qsas2gwdgksvnyd.onion)
34. [TorLanD Search Engine](http://torlgu6zhhtwe73fdu76uiswgnkfvukqfujofxjfo7vzoht2rndyhxyd.onion)
35. [Danexio627wiswvlpt6ejyhpxl5gla5nt2tgvgm2apj2ofrgm44vbeyd.onion](http://danexio627wiswvlpt6ejyhpxl5gla5nt2tgvgm2apj2ofrgm44vbeyd.onion)
36. [Devil Search](http://search65pq2x5oh4o4qlxk2zvoa5zhbfi6mx4br4oc33rpxuayauwsqd.onion/)
37. [Brave Search ENgine](https://search.brave4u7jddbv7cyviptqjc7jusxh72uik7zt6adtckl5f4nwy2v72qd.onion)
38. [Advanced Search - Bobby](http://bobby64o755x3gsuznts6hf6agxqjcz5bop6hs7ejorekbm7omes34ad.onion/)
----

### DarkNetRandom Chat
1. ["random chat"](http://notbumpz34bgbz4yfdigxvd6vzwtxc3zpt5imukgl6bvip2nikdmdaad.onion/rchat/)
2. [live chat](http://pfpmd7dd5ijt4add2sfi4djsaij4u3ebvnwvyvuj6aeipe2f5llptkid.onion/ui3/)
3. [jitjat email and instant msg](http://jitjatj3qbb42jvik4udcehxpkoidppz3gojslh7jcatuo4hx4xwayid.onion/login.php)
4. [black hat chat](http://blkhatjxlrvc5aevqzz5t6kxldayog6jlx5h7glnu44euzongl4fh5ad.onion/)
----

### breached/leaked data checker
1. [Search Instagram Users Leaked Password Database](http://breachdbsztfykg2fdaq2gnqnxfsbj5d35byz3yzj73hazydk4vq72qd.onion/)
2. [Deep Search](http://xjypo5vzgmo7jca6b322dnqbsdnp3amd24ybx26x5nxbusccjkm4pwid.onion/)
3. [Dsitributed Denial of Secrets](http://ddosxlvzzow7scc7egy75gpke54hgbg2frahxzaw6qq5osnzm7wistid.onion/wiki/Distributed_Denial_of_Secrets)
4. [Instagram - Leaked Password Database (Limited)](http://breachdbsztfykg2fdaq2gnqnxfsbj5d35byz3yzj73hazydk4vq72qd.onion/LeakedPass)
5. [FreedomHQ](http://freedomzw5x5tzeit4jgc3gvic3bmecje53hwcoc3nnwe2c3gsukdfid.onion/databases?page=2)
6. [Trashchan public DB dump](http://trashbakket2sfmaqwmvv57dfnmacugvuhwxtxaehcma6ladugfe2cyd.onion/)
7. [pulls up Leaked facebook data](http://4wbwa6vcpvcr3vvf4qkhppgy56urmjcj2vagu2iqgp3z656xcmfdbiqd.onion/)
----

### directories
1. [Rznvg5sjacavz5kpshrq4urm75xzruha6iiyuggidnioo5ztvwdfroyd.onion](http://rznvg5sjacavz5kpshrq4urm75xzruha6iiyuggidnioo5ztvwdfroyd.onion/)
2. [http://lnbpcgk4mem5vvvwilqsk7yb5i2bgtjphnvq37kajdycu56i5omt4cqd.onion/cat/7](http://lnbpcgk4mem5vvvwilqsk7yb5i2bgtjphnvq37kajdycu56i5omt4cqd.onion/cat/7)
3. [Jaz45aabn5vkemy4jkg4mi4syheisqn2wn2n4fsuitpccdackjwxplad.onion](http://jaz45aabn5vkemy4jkg4mi4syheisqn2wn2n4fsuitpccdackjwxplad.onion/)
4. [Qrtitjevs5nxq6jvrnrjyz5dasi3nbzx24mzmfxnuk2dnzhpphcmgoyd.onion](http://qrtitjevs5nxq6jvrnrjyz5dasi3nbzx24mzmfxnuk2dnzhpphcmgoyd.onion/)
5. [Torlinkuqd6ntr57y35m53pitu3qzfdbmhpkqmzn7yunzuwzrxjpmmyd.onion](http://torlinkuqd6ntr57y35m53pitu3qzfdbmhpkqmzn7yunzuwzrxjpmmyd.onion/)
6. [http://zaejy2lskzkbxo5vlc3qxs4mpwg25lgoadwtwxquvhkkq5eid43oqryd.onion/wiki/index.php](http://zaejy2lskzkbxo5vlc3qxs4mpwg25lgoadwtwxquvhkkq5eid43oqryd.onion/wiki/index.php)
7. [Wclekwrf2aclunlmuikf2bopusjfv66jlhwtgbiycy5nw524r6ngioid.onion](http://wclekwrf2aclunlmuikf2bopusjfv66jlhwtgbiycy5nw524r6ngioid.onion/)
8. [http://wclekwrf2aclunlmuikf2bopusjfv66jlhwtgbiycy5nw524r6ngioid.onion/hlink-category/catalogue/](http://wclekwrf2aclunlmuikf2bopusjfv66jlhwtgbiycy5nw524r6ngioid.onion/hlink-category/catalogue/)
9. [Pvhwsb7a3d2oq73xtr3pzvmrruvswyqgkyahcb7dmbbfftr4qtsmvjid.onion](http://pvhwsb7a3d2oq73xtr3pzvmrruvswyqgkyahcb7dmbbfftr4qtsmvjid.onion/)
10. [Gtl3bpvdnzijqmna2sd25ktixabzt7pval6fpkd2ur6vpov52zzv74yd.onion](http://gtl3bpvdnzijqmna2sd25ktixabzt7pval6fpkd2ur6vpov52zzv74yd.onion/)
11. [Ryf3kesi2lx6ujbg66bqqoo2grsgcavcqpvovp2ovwi73s3yqinrhhad.onion](http://ryf3kesi2lx6ujbg66bqqoo2grsgcavcqpvovp2ovwi73s3yqinrhhad.onion/)
12. [Rqagvqs4qx3t5masyja42io7ukaffohowgauvymi2xgcp26chrirf5yd.onion](http://rqagvqs4qx3t5masyja42io7ukaffohowgauvymi2xgcp26chrirf5yd.onion/)
13. [http://gtl3bpvdnzijqmna2sd25ktixabzt7pval6fpkd2ur6vpov52zzv74yd.onion/index.html#search-engines](http://gtl3bpvdnzijqmna2sd25ktixabzt7pval6fpkd2ur6vpov52zzv74yd.onion/index.html#search-engines)
14. [Deepxfmkjlils3vd7bru5rrxy3x3lchjgmv3wsgycjfynktmuwgm64qd.onion](http://deepxfmkjlils3vd7bru5rrxy3x3lchjgmv3wsgycjfynktmuwgm64qd.onion/)
15. [Bwravzt2xdkc3r3ha6ivyizbtxw4bplohjaswqfe6zlh5tt22eb5iaid.onion](http://bwravzt2xdkc3r3ha6ivyizbtxw4bplohjaswqfe6zlh5tt22eb5iaid.onion/)
16. [Onionu2dme6j7myzoyotcpzzmo6vmozl2vmflucwogr2euc2nucqc7yd.onion](http://onionu2dme6j7myzoyotcpzzmo6vmozl2vmflucwogr2euc2nucqc7yd.onion/)
17. [Linkssfpaeihh6nkzf373e45avva6x7kjkiqnceomh275motaxoc3ead.onion](http://linkssfpaeihh6nkzf373e45avva6x7kjkiqnceomh275motaxoc3ead.onion/)
18. [2ja7yz5b3owazz76uvs3zjgcobf4ui63afscjc4azlu67khfuy3iq5yd.onion](http://2ja7yz5b3owazz76uvs3zjgcobf4ui63afscjc4azlu67khfuy3iq5yd.onion/)
----

### DarkNet Archievin
1. [archievecastytosk.onion aka archive.today (i can't get it to work LOL)](http://archiveiya74codqgiixo33q62qlrqtkgmcitqx5u2oeqnmn5bpcbiyd.onion/)
----

### Guides: RU
1. [Telegram боты для пробива инфы | Planworld](https://planworld.ru/ru/telegram/telegram-boty-dlya-probiva-infy.html)
2. [@jms_dot_py https://twitter.com/jms_dot_py](https://www.hunch.ly/darkweb-osint/darkwebthanks)
3. [Tracking Disinformation during the invasion of Ukraine](https://www.logically.ai/)
4. [@cyb_detective https://twitter.com/cyb_detective](https://telegra.ph/working-with-foreign-languages-dialects-and-slang-in-osint-01-16)
5. [Russian OSINT](https://boosty.to/russian_osint)
6. [@ranlocar on Twitter: translating telegram channels](https://twitter.com/ranlocar/status/1529093524813189126?s=20&t=ht4IAC_xOUXLC5XPNIy27g)
7. [Russia - Ukraine Cyberwar - SOCRadar® Cyber Intelligence Inc.StarStarStarStarStarYouTube iconTwitter iconLinkedIn icon](https://socradar.io/resources/russia-ukraine-cyber-war/)
----

### ClearNet Links to DarkWeb
1. [DarkWebWiki.org – Darknet Markets Links](https://darkwebwiki.org/)
2. [150+ Working Dark Web Links – Onion v3 | Darkweb-links.com](https://darkweb-links.com/150-dark-web-links-2021/#Search_Engines)
3. [Eu.onionsearchengine.com](https://eu.onionsearchengine.com/)
----

### Ukraine (dark web)
1. ["ukrainenews" -- seems like a good faith effort here...](http://ukrainewslsunpkh7tlahmi3sx5rzsftjzp53p32zyfv73d4mp3bnqad.onion/info/cyberwar.php)
----

### Ukraine (clear net)
1. [Itarmy.com.ua](https://itarmy.com.ua/?lang=en)
----

### Others' start me pages on Russia / Ukraine  trackers - OSINT
1. ["Ukraine Expects & Live Map" -- Arnaud V. Aka @Bizcom](https://onionlandsearchengine.com/)
2. ["Ukraine Crisis Tracker" -- SUSSEX GLOBAL ANALYSIS](https://start.me/p/lLBdE6/ukraine-crisis-tracker)
----

### Database |organizations, politicians, wealthy, and gov affiliations
1. [http://www.ved.gov.ru/eng/companies/](http://www.ved.gov.ru/eng/companies/)
2. [PEP: PUBLIC DATABASE OF DOMESTIC POLITICALLY EXPOSED PERSONS OF RUSSIA AND BELARUS](https://rupep.org/en/)
3. [aleph.occrp.org](https://aleph.occrp.org/)
4. [Поиск компании](https://xn--c1aubj.xn--80asehdb/companies/)
5. [Projecthoneypot.org](https://www.projecthoneypot.org/)
6. [Federated Search](https://biznar.com/)
7. [LittleSis - Opposite from Big Brother :)](https://littlesis.org)
8. [Search for ORG, Name, ADD, etc,. -- verify](https://www.list-org.com)
----

### Social Media/Search ENgines/PhoneNumber Lookup
1. [OK.ru](https://ok.ru)
2. [Поиск Mail.Ru](https://go.mail.ru)
3. [Sociuminfo.com](https://sociuminfo.com)
4. [Портал "Чекко" – Проверь любую компанию или ИП](https://checko.ru/)
5. [https://www.roum.ru//](https://www.roum.ru//)
6. [Яндекс](https://yandex.ru)
7. [RUSSIAN TELEGRAM ANALYTICS https://tgstat.ru/en](RUSSIAN TELEGRAM ANALYTICS https://tgstat.ru/en)
8. [ВКонтакте](https://m.vk.com/)
9. [Rutube](https://rutube.ru/)
10. [Career.habr.com](https://career.habr.com/)
11. [Нигма.РФ](http://nigma.ru/)
12. [Рамблер](http://www.rambler.ru/)
13. [Gismeteo](https://gismeteo.ru)
14. [Государственные услуги](https://gosuslugi.ru)
15. [SubscribeRu](https://subscribe.ru/)
16. [Referat.ru](https://www.referat.ru/)
17. [Whitepages.com.ru](https://whitepages.com.ru/)
18. [Russian-personals.com](https://russian-personals.com/)
19. [Anon Forum -- Russian](http://commudazrdyhbullltfdy222krfjhoqzizks5ejmocpft3ijtxq5khqd.onion/)
20. [Socialmediaholding.ru](https://socialmediaholding.ru/)
21. [rusprofile](https://www.rusprofile.ru/)
22. [Рамблер](https://www.rambler.ru/)
23. [rusprofile](https://www.rusprofile.ru/)
24. [Phone Book of Russia.com  +7   by Phonebook of the World.com](https://phonebookoftheworld.com/russia/)
25. [Hashatit](https://www.hashatit.com/)
26. [Criminal IP](https://www.criminalip.io/)
27. [Vk.watch](https://vk.watch)
28. [Directory of websites](https://www.kazancity.ru)
29. [Russian Search - "Most powerful Meta-Search Engine on the Web!" RU](https://www.metabear.ru)
30. [RU Metabear](https://www.metabot.ru)
31. ["craigslist" of RU -- for Goods and Services](https://www.russia.aport.ru)
32. [See and post Advertisements](https://epilot.ru/)
33. [ru search - ru guide](https://rin.ru/index_e.html)
34. [Russian Whitepages](https://whitepages.rin.ru/search.html)
35. [Job & Employee Search site/RU](https://hh.ru/)
----

### Phone numberlookup
1. [English.spravkaru.net](http://english.spravkaru.net)
2. [Russia Reverse Lookup 7, Phone Number Search](https://www.searchyellowdirectory.com/reverse-phone/7/)
3. [Russian Companies Directory](http://www.ved.gov.ru/eng/companies/)
4. [CIS White Pages: People Search for Free in Russia, Ukraine, Belarus, Kazahstan, Latvia, Moldova](http://english.spravkaru.net/?nj)
5. [International Calling Cards](https://www.comfi.com)
6. [Поиск по номеру телефона](https://mysmsbox.ru/phone-search)
7. [Ru-98.ru](https://ru-98.ru/)
8. [https://кто-звонит.рф](https://кто-звонит.рф)
9. [Определить оператора и регион по номеру телефона](https://phonenum.info/)
10. [Mysmsbox.ru](https://mysmsbox.ru)
11. [https://xn----dtbofgvdd5ah.xn--p1ai/](https://xn----dtbofgvdd5ah.xn--p1ai/)
12. [8call.ru](https://8call.ru)
13. [Apertonet.ru](https://apertonet.ru)
14. [Znep.ru](https://znep.ru/)
15. [Apk-shatura.ru](https://apk-shatura.ru)
16. [Allnum.ru](http://allnum.ru)
17. [Interweb.spb.ru](http://interweb.spb.ru)
18. [Определить оператора, регион и страну по номеру телефона](https://phonenum.info/phone/)
19. [Phone Number Track](https://www.phonenumbertrack.com)
----

### Hacking Forums
1. [Sinisterly](https://sinister.ly/)
2. [Demon Forums - Advanced Hacking](https://demonforums.net/Forum-Advanced-Hacking)
3. [Cracked](https://cracked.to/)
4. [hackforums](https://hackforums.net/)
5. [Nulled](https://www.nulled.to/)
6. [RaidForums](https://raidforums.com/)
7. [0x00sec](https://0x00sec.org/)
8. [BlackHatWorld](https://www.blackhatworld.com/)
9. [HACKCRAZE - The Hacking & Security Forum](https://hackcraze.com/)
10. [Nulledbb.com](https://nulledbb.com/)
11. [Cracking.org](https://cracking.org/)
12. [Crackia.com](https://crackia.com/)
13. [Cracking Portal | Cracking Begins | Best Cracking Forum](https://crackingportal.com/)
14. [Dread (similiar to Reddit)](http://dreadytofatroptsdj6io7l3xptbet6onoyno2yv7jicoxknyazubrad.onion/discover)
----

### Research
1. [Sci-hub.ru](https://sci-hub.ru/)
----

### Search Engines
1. [One Search](https://www.onesearch.com)
2. [Experimental Search Engine by PRIVACORE](https://www.findx.com/)
3. ["web crawler" search egine](https://www.webcrawler.com/)
4. [GigaBlast Search Engine DECENT SEARCHES](https://gigablast.com/)
5. [Visual Search Engine](https://peekier.com)
6. [A rlly blah search engine](https://www.discretesearch.com/)
7. [No tracking Search Engine](https://www.mojeek.com)
8. [Privacy oriented Search Engine](https://you.com/)
9. [Education, Products, and Data access without a cost](https://swisscows.com/)
10. [(a bit of ads but Searches are great)](https://www.swisscows.com)
11. [RU Search Engine](http://www.metabot.ru)
12. [Yandex Search Engine (.com or .ru)](https://www.yandex.com)
----

### Leaked/Breached Data
1. [Instagram Leaks - Username and Email Search option (view passwords must pay) Only tells you WHERE the leak came from](http://breachdbsztfykg2fdaq2gnqnxfsbj5d35byz3yzj73hazydk4vq72qd.onion/)
----

