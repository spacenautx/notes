### Secure Web Browser VM Silo Browser Authentic8
1. [Cloud Browser | Secure Web Browser | Authentic8 - Silo](https://www.authentic8.com/)
----

### Secure Web Browser
1. [Brave Browser](https://brave.com/)
2. [Epic Privacy Browser](https://www.epicbrowser.com/)
3. [Getting started with Oryon OSINT Browser](https://www.youtube.com/watch?v=EUK3Q6mfjo4)
4. [Oryon OSINT Browser](https://sourceforge.net/projects/oryon-osint-browser/)
5. [Remote Browsing Isolation Technology](https://webgap.io/remote-browser-isolation-technology.html)
6. [SnowHaze: Anonymes VPN & gratis iOS Private Browser](https://snowhaze.com/de/)
----

### Gogle Chrome AddOn | Tabs
1. [@OneTab](https://chrome.google.com/webstore/detail/onetab/chphlpgkkbolifaimnlloiipkdnihall?utm_source=chrome-ntp-icon)
2. [GraphiTabs - Chrome Web Store](https://chrome.google.com/webstore/detail/graphitabs/dcfclemgmkccmnpgnldhldjmflphkimp?utm_source=chrome-ntp-icon)
3. [Tabs Outliner](https://chrome.google.com/webstore/detail/tabs-outliner/eggkanocgddhmamlbiijnphhppkpkmkl?utm_source=chrome-ntp-icon)
4. [Cluster - Window & Tab Manager - Chrome Web Store](https://chrome.google.com/webstore/detail/cluster-window-tab-manage/aadahadfdmiibmdhfmpbeeebejmjnkef?utm_source=chrome-ntp-icon)
5. [Workona](https://chrome.google.com/webstore/detail/workona/ailcmbgekjpnablpdkmaaccecekgdhlh?utm_source=chrome-ntp-icon)
----

### xBlog
1. [xBlog: The Best Chrome Extensions for OSINT Professionals, Researchers and Journalists in 2021–i-intelligence](https://i-intelligence.eu/insights/best-chrome-extensions-for-osint-professionals-analysts-journalists-academic-researchers-and-students-february-2021)
----

### Google Chrome AddOn |  Security I
1. [Delete Browser Data | Browserdaten löschen](https://support.google.com/chrome/answer/2392709?hl=de&visit_id=1-636125606380798142-1233573202&rd=1)
2. [Download Chrome Developer Version](https://www.google.com/intl/en_ca/chrome/dev/)
3. [Download the Chrome-Browser from a trusted source](https://www.google.de/chrome/browser/desktop/)
4. [Google Help/-Hilfe](https://support.google.com/?hl=de)
5. [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)
6. [xBlog: The end of uBlock Origin for Google Chrome? - gHacks Tech News](https://www.ghacks.net/2019/10/12/the-end-of-ublock-origin-for-google-chrome/)
----

### Google Chrome AddOn | Security II
1. [ClearURLs - Chrome Web Store](https://chrome.google.com/webstore/detail/clearurls/lckanjgmijmafbedllaakclkaicjfmnk)
2. [Disconnect](https://chrome.google.com/webstore/detail/disconnect/jeoacafpbcihiomhlakheieifhpjdfeo?hl=de)
3. [DuckDuckGo Privacy Essentials](https://chrome.google.com/webstore/detail/duckduckgo-privacy-essent/bkdgflcldnnnapblkhphbgpggdiikppg)
4. [Hotcleaner Click&Clean](http://www.hotcleaner.com/)
5. [IP Address and Domain Information](https://chrome.google.com/webstore/detail/ip-address-and-domain-inf/lhgkegeccnckoiliokondpaaalbhafoa)
6. [Location Guard](https://chrome.google.com/webstore/detail/location-guard/cfohepagpmnodfdmjliccbbigdkfcgia)
7. [Manual Geolocation - Chrome Web Store](https://chrome.google.com/webstore/detail/manual-geolocation/jpiefjlgcjmciajdcinaejedejjfjgki?utm_source=chrome-ntp-icon)
8. [Privacy Badger](https://chrome.google.com/webstore/detail/privacy-badger/pkehgijcmpdhfbdbbnkijodmdjhbjlgp?hl=de)
9. [ScriptSafe](https://chrome.google.com/webstore/detail/scriptsafe/oiigbmnaadbkfbmpbfijlflahbdbdgdf?hl=de)
10. [Social Book Post Manager DELETE Facebook Posts](https://chrome.google.com/webstore/detail/social-book-post-manager/ljfidlkcmdmmibngdfikhffffdmphjae)
11. [uMatrix HELP Github.com/gorhill/httpswitchboard/wiki](https://github.com/gorhill/httpswitchboard/wiki)
12. [uMatrix Matrix-basierte Firewall Fortgeschritten](https://chrome.google.com/webstore/detail/umatrix/ogfcmafjalglgifnmanfmnieipoejdcf)
13. [Unshorten.It! - Chrome Web Store](https://chrome.google.com/webstore/detail/unshortenit/lgmkbhnfldpklfakbcopgkkhonofficm?utm_source=chrome-ntp-icon)
14. [WebRTC Leak Prevent](https://chrome.google.com/webstore/detail/webrtc-leak-prevent/eiadekoaikejlgdbkbdfeijglgfdalml)
15. [[ninoseki/mitaka: A browser extension for OSINT search](https://github.com/ninoseki/mitaka)]([ninoseki/mitaka: A browser extension for OSINT search](https://github.com/ninoseki/mitaka))
----

### Google Chrome AddOn | Productivity I
1. [Explain und Send](https://chrome.google.com/webstore/detail/explain-and-send-screensh/mdddabjhelpilpnpgondfmehhcplpiin?utm_source=chrome-ntp-icon)
2. [Google Similar Pages](https://chrome.google.com/webstore/detail/google-similar-pages/pjnfggphgdjblhfjaphkjhfpiiekbbej)
3. [Google Translate](https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb)
4. [Image Search options](https://chrome.google.com/webstore/detail/image-search-options/kljmejbpilkadikecejccebmccagifhl/reviews)
5. [Save to google drive](https://chrome.google.com/webstore/detail/save-to-google-drive/gmbmikajjgmnabiglmofipeabaddhgne?hl=en)
6. [TXT Als reinen Text kopieren](https://chrome.google.com/webstore/detail/copy-as-plain-text-amazin/mkkcgjeddgdnikkeoinjgbocghokolck)
7. [Grammar and Spell Checker - LanguageTool - Chrome Web Store](https://chrome.google.com/webstore/detail/grammar-and-spell-checker/oldceeleldhonbafppcapldpdifcinji?utm_source=chrome-ntp-icon)
----

### Google Chrome AddOn | Productivity II
1. [Capture Webpage Screenshot Entirely. FireShot](https://chrome.google.com/webstore/detail/capture-webpage-screensho/mcbpblocgmgfnpjjppndjkmgjaogfceg)
2. [Chrome Themes Web Store](https://chrome.google.com/webstore/category/themes)
3. [Cite This For Me: Web Citer](https://chrome.google.com/webstore/detail/cite-this-for-me-web-cite/nnnmhgkokpalnmbeighfomegjfkklkle)
4. [Data Scraper](https://chrome.google.com/webstore/detail/data-scraper/nndknepjnldbdbepjfgmncbggmopgden?hl=de)
5. [Instant Data Scraper](https://chrome.google.com/webstore/detail/instant-data-scraper/ofaokhiedipichpaobibbnahnkdoiiah)
6. [JSONView](https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc)
7. [Linkclump - Chrome Web Store](https://chrome.google.com/webstore/detail/linkclump/lfpjkncokllnfokkgpkobnkbkmelfefj?hl=de)
8. [Sputnik — An Open Source Intelligence Browser Extension](https://medium.com/bugbountywriteup/sputnik-an-open-source-intelligence-browser-extension-da2f2c22c8ec)
9. [WayBack Machine](https://chrome.google.com/webstore/detail/wayback-machine/fpnmgdkabkmnadcjpehmlllkndpkmiak?hl=de)
10. [ZapInfo](https://chrome.google.com/webstore/search/zapinfo)
11. [I don't care about cookies](https://chrome.google.com/webstore/detail/i-dont-care-about-cookies/fihnjjcciajhdojfnbdddfaoknhalnja)
----

### Google Chrome AddOn | Monitoring RSS
1. [@Inoreader Companion](https://chrome.google.com/webstore/detail/inoreader-companion/kfimphpokifbjgmjflanmfeppcjimgah?hl=de)
2. [Distill-web-monitor](https://chrome.google.com/webstore/detail/distill-web-monitor/inlikjemeeknofckkjolnjbpehgadgge?hl=en)
3. [Feedly Subscribe Button](https://chrome.google.com/webstore/detail/feedly-subscribe-button/gbbnddjfcllebfcnihfgmdplgaiejepc)
4. [Shoyu RSS/Atom Feed Preview - Chrome Web Store](https://chrome.google.com/webstore/detail/shoyu-rssatom-feed-previe/ilicaedjojicckapfpfdoakbehjpfkah?hl=fr)
5. [SPOI Filter - Keywords filtern in Feedly & The Old Reader (€)](https://chrome.google.com/webstore/detail/spoi-filter/ccklmambcfmhahigkfmpmcafjlbjnnha)
6. [Visual Ping Page Monitor (New)](https://chrome.google.com/webstore/detail/visualping/pemhgklkefakciniebenbfclihhmmfcd/related?hl=de)
7. [Visualping (Monitoring Websites for Changes)](http://goo.gl/fPORAc)
----

### Facebook extensions from OSINT Support
1. [Facebook ID Grabber](https://osint.support/downloads/facebook-profile-id-grabber-ob.zip)
2. [Email Reverse Lookup](https://osint.support/downloads/facebook-email-reverse-lookup-ob.zip)
3. [Home · OSINT SUPPORT](https://osint.support/)
4. [DumpItBlue+](https://chrome.google.com/webstore/detail/dumpitblue%2B/igmgknoioooacbcpcfgjigbaajpelbfe/related)
----

### Google Chrome AddOn | Twitter
1. [Treeverse](https://chrome.google.com/webstore/detail/treeverse/aahmjdadniahaicebomlagekkcnlcila)
----

### Google Chrome AddOn| Foto Video
1. [Frame by Frame for YouTube](https://chrome.google.com/webstore/detail/frame-by-frame-for-youtub/elkadbdicdciddfkdpmaolomehalghio?hl=en-GB)
2. [RevEye Reverse Image Search](https://chrome.google.com/webstore/detail/reveye-reverse-image-sear/keaaclcjhehbbapnphnmpiklalfhelgf)
3. [Send to Exif Viewer](https://chrome.google.com/webstore/detail/send-to-exif-viewer/gogiienhpamfmodmlnhdljokkjiapfck)
4. [Storyful Multisearch](https://chrome.google.com/webstore/detail/storyful-multisearch/hkglibabhninbjmaccpajiakojeacnaf)
5. [VideoVault](https://chrome.google.com/webstore/detail/videovault-for-chrome/onoidkfboifjpllldnfnaebfigijckmk)
----

### GEOINT
1. [Map Switcher](https://chrome.google.com/webstore/detail/map-switcher/fanpjcbgdinjeknjikpfnldfpnnpkelb)
----

### Firefox AddOn |  Security I MUST HAVE
1. [Add-ons für Firefox](https://addons.mozilla.org/de/firefox/)
2. [BLOG: Firefox Privacy - The Complete How-To Guide | Restore Privacy](https://restoreprivacy.com/firefox-privacy/)
3. [Download Firefox from a trusted source (Deu)](https://www.mozilla.org/de/firefox/new/)
4. [Download Firefox from a trusted source (ENG)](https://www.firefox.com/)
5. [uBlock Origin – Add-ons for Firefox (2)](https://addons.mozilla.org/firefox/addon/ublock-origin/)
----

### Firefox AddOn | Security II
1. [ClearURLs – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/en-US/firefox/addon/clearurls/)
2. [Decentraleyes Tracking Protection](https://addons.mozilla.org/de/firefox/addon/decentraleyes/)
3. [Disconnect – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/firefox/addon/disconnect/)
4. [FoxyProxy](https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/?)
5. [Hotcleaner Click&Clean](http://www.hotcleaner.com/)
6. [LastPass – Holen Sie sich diese Erweiterung für 🦊 Firefox (de)](https://addons.mozilla.org/de/firefox/addon/lastpass-password-manager/?src=search)
7. [Location Guard – Holen Sie sich diese Erweiterung für 🦊 Firefox (de)](https://addons.mozilla.org/de/firefox/addon/location-guard/?src=search)
8. [PassSec+ – Holen Sie sich diese Erweiterung für 🦊 Firefox (de)](https://addons.mozilla.org/de/firefox/addon/passsec/)
9. [uMatrix – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/firefox/addon/umatrix/)
10. [User Agent Switcher](https://addons.mozilla.org/en-US/firefox/addon/user-agent-switcher-revived/?)
11. [User-Agent Switcher](https://mybrowseraddon.com/useragent-switcher.html)
----

### Firefox AddOn | Productivity I
1. [Bulk Media Downloader](https://addons.mozilla.org/en-US/firefox/addon/bulk-media-downloader/?)
2. [Copy Plaintext](https://addons.mozilla.org/en-US/firefox/addon/copy-plaintext)
3. [Exif Viewer](https://addons.mozilla.org/en-US/firefox/addon/exif-viewer/?)
4. [Google Translate for Firefox](https://addons.mozilla.org/en-US/firefox/addon/google-translator-for-firefox/)
5. [HTTPS Everywhere](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp?utm_source=gmail)
6. [Image Search Options](https://addons.mozilla.org/en-US/firefox/addon/image-search-options/?)
7. [Multi Account Container](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/?)
8. [unMHT Save Page as MHTML](https://addons.mozilla.org/de/firefox/addon/save-page-we/?src=search)
9. [Video Download Helper](https://addons.mozilla.org/en-US/firefox/addon/video-downloadhelper/?)
----

### Firefox AddOn | Productivity II
1. [Copy selected URL](https://addons.mozilla.org/en-US/firefox/addon/copy-selected-links/?)
2. [Download Star](https://addons.mozilla.org/en-US/firefox/addon/download-star/)
3. [JSON XML Viewer](https://addons.mozilla.org/en-US/firefox/addon/mjsonviewer/?)
4. [Nimbus Screen Capture](https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/?)
5. [Resurrect Pages](https://addons.mozilla.org/en-US/firefox/addon/resurrect-pages/?)
----

### Firefox AddOn | Monitoring RSS
1. [Distill Monitor](https://addons.mozilla.org/en-US/firefox/addon/distill-web-monitor-ff/)
2. [Innoreader Companion](https://addons.mozilla.org/en-US/firefox/addon/inoreader-companion/?)
3. [Update Scanner Monitor Webpages for Updates](https://addons.mozilla.org/en-US/firefox/addon/update-scanner/?src=userprofile)
4. [Want my RSS](https://addons.mozilla.org/de/firefox/addon/want-my-rss/)
----

