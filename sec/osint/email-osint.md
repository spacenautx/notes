### EMAIL VERIFICATION (SMTP):
1. [Zerobounce (SMTP)](https://www.zerobounce.net/)
2. [IPinfo (SMTP)](https://ipinfo.io/)
3. [Mailboxlayer (SMTP)](https://mailboxlayer.com/)
4. [2ip (SMTP)](https://2ip.ru/mail-checker/)
5. [ivit.pro (SMTP)](https://ivit.pro/services/email-valid/)
6. [Email Validation (SMTP)](https://htmlweb.ru/service/email_verification.php)
7. [smart-ip.net (SMTP)](http://ru.smart-ip.net/check-email/)
----

### DOMAIN DATA (WHOIS):
1. [DomainTools (WHOIS)](https://whois.domaintools.com/)
2. [Whoer (WHOIS)](https://whoer.net/ru/checkwhois)
3. [RIPN (WHOIS)](http://www.ripn.su/nic/whois/)
4. [2ip (WHOIS)](https://2ip.ru/whois/)
5. [Securrity (WHOIS)](http://www.securrity.ru/whoiz.html)
6. [Email Sherlock (WHOIS)](https://www.emailsherlock.com/)
7. [WHOIS History (Whois History)](https://drs.whoisxmlapi.com/whois-history)
8. [WhoisHistory (Whois History)](http://whoishistory.ru/)
----

### HEADERS ANALYSIS:
1. [Messageheader (headers check)](https://toolbox.googleapps.com/apps/messageheader/analyzeheader)
2. [Show IP Address (headers check)](https://mailheader.org/)
3. [Header Analyzer (headers check)](https://mxtoolbox.com/EmailHeaders.aspx)
4. [Trace Email (headers check)](http://ru.smart-ip.net/trace-email)
5. [Analyse, track IP (headers check)](https://www.iptrackeronline.com/email-header-analysis.php)
6. [Azurewebsites (headers check)](https://mha.azurewebsites.net/)
7. [Email Headres (headers check)](https://suip.biz/ru/?act=email)
8. [Msgeml (EML viewer)](https://msgeml.com/)
9. [Conholdate (EML viewer)](https://products.conholdate.app/viewer/eml)
10. [EML Reader (EML viewer)](https://products.aspose.app/email/viewer/eml)
----

### CREATING A PHISHING EMAIL:
1. [PhishMailer (open source)](https://github.com/BiZken/PhishMailer)
2. [SniperPhish (open source)](https://github.com/GemGeorge/SniperPhish)
3. [phishmonger (open source)](https://github.com/fkasler/)
4. [Getgophish (service)](https://getgophish.com/)
5. [Hook Security (service)](https://hooksecurity.co/phishing-examples/github-phishing-example)
6. [ThePhish (phishing email analysis)](https://github.com/emalderson/ThePhish)
----

### EMAIL TRACKING SERVICES:
1. [Email Tracker (extension)](https://chrome.google.com/webstore/detail/email-tracker/bnompdfnhdbgdaoanapncknhmckenfog)
2. [Emailtrackerpro (email tracking)](http://www.emailtrackerpro.com/)
3. [Didtheyreadit (email tracking)](http://www.didtheyreadit.com/)
4. [Readnotify (email tracking)](http://www.readnotify.com/readnotify/)
5. [Getnotify (email tracking)](https://www.getnotify.com/)
6. [Mailtracking (email tracking)](http://mailtracking.com/)
7. [GeoTrack (email tracking)](https://geotrack.email/)
----

### LOGGERS IN DOCUMENTS:
1. [Canarytokens (logger in file)](http://canarytokens.org/generate)
2. [PMDoctrack (logger in file)](http://www.mailtracking.com/mailtracking/pmdoctrack.asp)
3. [Track PDF (logger in file)](https://www.locklizard.com/track-pdf-monitoring/)
----

### LOGGERS IN HYPERLINK:
1. [Canarytokens (logger in hyperlink)](http://canarytokens.org/generate)
2. [Grabify (logger in hyperlink)](https://grabify.link/)
3. [2IP (logger in hyperlink)](https://2ip.ru/strange-ip/)
4. [IP spy (logger in hyperlink)](http://ru.itsip.info/spy)
5. [Iplogger (logger in hyperlink)](https://iplogger.ru/)
6. [iknowwhatyoudownload (logger in hyperlink)](https://iknowwhatyoudownload.com/ru/link/)
----

### GEOLOCATION OF E-MAIL:
1. [Seeker (Geologger)](https://github.com/thewhiteh4t/seeker.git)
2. [TrackUrl (Geologger)](https://github.com/Mauladen/TrackUrl)
3. [TRape (Geologger)](https://github.com/boxug/trape.git)
4. [IPLogger (Geologger)](https://iplogger.ru/location-tracker/)
5. [BigBro (Geologger)](https://github.com/Bafomet666/Bigbro)
6. [Yandex Audience (ADINT)](https://audience.yandex.ru/)
7. [Google ADS (ADINT)](https://ads.google.com/)
8. [My Target (ADINT)](https://target.my.com/)
----

### EMAIL FORENSIC TOOLS:
1. [Mailxaminer (forensic)](https://www.mailxaminer.com/)
2. [Aid4Mail (forensic)](https://www.aid4mail.com/email-forensics-ediscovery)
3. [MailPro+ (forensic)](https://www.systoolsgroup.com/mail-pro-plus.html?AFFILIATE=118387&__c=1)
4. [Xtraxtor (forensic)](https://xtraxtor.com/)
5. [Emailforensicwizard (forensic)](https://www.emailforensicwizard.com/)
6. [mailMeta (forensic)](https://github.com/gr33nm0nk2802/mailMeta)
7. [EML viewer (forensic)](https://www.systoolsgroup.com/eml-viewer-pro.html)
----

### EMAIL IDENTIFICATION (LEAKS):
1. [@OSINTInfoRobot (leaks)](https://t.me/OSINTInfoRobot)
2. [@InfoSearchRobot (leaks)](https://t.me/InfoSearchRobot)
3. [@QuickOSINT (leaks)](https://t.me/QuickOSINT_Robot)
4. [Eye of God (leaks)](https://gb.sbs/)
5. [@Tpoisk_Bot (leaks)](https://t.me/Tpoisk_Bot)
6. [@Usersbox_bot (leaks)](https://t.me/usersbox_bot)
7. [Saverudata.info (leaks)](https://saverudata.info/map/)
8. [AVinfoBot (leaks)](https://avclick.me/v/AVinfoBot)
9. [InfoTracer (leaks)](https://infotracer.com/)
10. [Pipl.com (leaks)](https://pipl.com/)
11. [Intelligence X (dorks)](https://intelx.io/tools?tab=email)
----

### GMAIL CHECK:
1. [Restore Access](https://accounts.google.com/signin/v2/usernamerecovery)
2. [Epieos (user data)](https://tools.epieos.com/google-account.php)
3. [Universal Search (user data)](https://t.me/UniversalSearchRobot)
4. [GHunt (user data)](https://github.com/mxrch/GHunt)
5. [Xeuledoc (user data)](https://github.com/Malfrats/xeuledoc)
----

### YANDEX MAIL CHECK:
1. [Restore Access](https://passport.yandex.ru/auth/restore/password)
2. [Universal Search (user data)](https://t.me/UniversalSearchRobot)
----

### PROTONMAIL
1. [Date of registration](https://mail.protonmail.com/contacts)
2. [ProtOSINT (user data)](https://github.com/pixelbubble/ProtOSINT)
----

### MAIL.RU
1. [Restore Access](https://account.mail.ru/recovery)
2. [Get avatar](http://filin.mail.ru/pic?width=180&height=180&email=example@mail.ru)
----

### SEARCH BY NAME:
1. [Mail.ru (search by name and surname)](https://go.mail.ru/search_social)
2. [NAMINT (search by name and surname)](https://seintpl.github.io/NAMINT/)
----

### SEARCH BY NICKNAME:
1. [@maigret_osint_bot (search by nickname)](https://t.me/maigret_osint_bot)
2. [NameCheckup (search by nickname)](https://namecheckup.com/)
3. [WhatsMyName (search by nickname)](https://whatsmyname.app/)
4. [Namechk (search by nickname)](https://namechk.com/)
5. [Username Search (search by nickname)](https://instantusername.com/#/)
6. [Username Search (search by nickname)](https://usersearch.org/)
7. [Sherlock (search by nickname)](https://github.com/sherlock-project/sherlock)
----

### SEARCH BY PHOTO:
1. [Search4faces (face identification)](https://search4faces.com/)
2. [Eye of God (face identification)](https://eyeofgod.global/)
3. [Findclone (face identification)](https://findclone.ru/)
4. [PimEyes (face identification)](https://pimeyes.com/en/)
5. [Smart_SearchBot (face identification)](https://t.me/ssb_russian_probiv_bot)
6. [Quick OSINT (face identification)](https://t.me/Quick_OSINT_bot)
7. [Findfacerobot (face identification)](https://t.me/findfacerobot)
8. [Yandex Pictures (image search)](https://yandex.ru/images/)
9. [Google Images (image search)](https://images.google.com/)
10. [Mail.ru (image search)](https://go.mail.ru/search_images)
----

### SEARCH EMAIL BY WEBSITE:
1. [Domainbigdata (Find Contacts)](https://domainbigdata.com/)
2. [Phonebook (Find Contacts)](https://phonebook.cz/)
3. [Hunter (Find Contacts)](https://hunter.io/)
4. [Mailshunt (Find Contacts)](https://www.mailshunt.com/)
5. [2ip (Find Contacts)](https://2ip.ru/domain-list-by-email/)
6. [Domainwat (Find Contacts)](https://domainwat.ch/)
----

### SEARCH EMAIL BY LEAKED PASSWORD:
1. [Leak-lookup (need registration)](https://leak-lookup.com/)
2. [Leakpeek (Free)](https://leakpeek.com/)
3. [BreachDirectory (Need Registration)](https://breachdirectory.org/passwords.html)
4. [Eye of God (command: /pas)](https://gb.sbs)
5. [LeakCheck (Enterprise Plan)](https://leakcheck.io/)
6. [Karma (open source)](https://github.com/jdiazmx/karma/tree/dd49cace6f68772397a66b04860c10a93f4eb505)
7. [Darknet (Need TOR)](http://xjypo5vzgmo7jca6b322dnqbsdnp3amd24ybx26x5nxbusccjkm4pwid.onion/)
----

### SEARCH EMAIL BY PGP-KEY:
1. [Peegeepee (public PGP)](https://peegeepee.com/)
2. [Keybase (public PGP)](https://keybase.io/)
3. [Gpg4win (extract pgp key)](https://www.gpg4win.org/)
----

### PERMUTATION MAIL:
1. [NAMINT (permutation)](https://seintpl.github.io/NAMINT/)
2. [Email Permutator Tool (permutation)](https://www.polished.app/email-permutator/)
3. [Email Permutator (permutation)](http://metricsparrow.com/toolkit/email-permutator/)
4. [Google (permutation)](https://docs.google.com/spreadsheets/d/17URMtNmXfEZEW9oUL_taLpGaqTDcMkA79J8TRw4xnz8/edit#gid=0)
5. [Mailcat (permutation)](https://github.com/soxoj/mailcat)
6. [@mailcat_s_bot (permutation)](https://t.me/mailcat_s_bot)
----

