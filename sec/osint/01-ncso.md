### Copy Of Recent Added Bookmarks
1. [My OSINT Training](https://www.myosint.training/)
2. [DorkSearch](https://dorksearch.com/)
3. [Fagan Finder](https://www.faganfinder.com/)
4. [Trumail](https://trumail.io/)
5. [Check Usernames](https://checkusernames.com/)
6. [Namevine](https://namevine.com/#/)
7. [Dark Web Resources](https://sites.google.com/view/the-dark-net/home)
8. [Tools Epieos](https://tools.epieos.com/)
9. [Identity Leak Checker](https://sec.hpi.de/ilc/search)
10. [BreachDirectory](https://breachdirectory.org/)
11. [Boolean Strings](https://booleanstrings.com/)
12. [Core OSINT Skills](https://start.me/p/ydEgyG/core-osint-skills)
13. [Ro-Intelligence](https://ro-intelligence.com/)
14. [XMind](https://www.xmind.net/)
15. [Webbreacher](https://webbreacher.com/)
16. [V3nari Bookmarks](https://start.me/p/1kxyw9/v3nari-bookmarks)
17. [TypeIt](https://www.typeit.org/)
18. [STATE of OSINT](https://stateofosint.com/)
19. [Researching the Extreme Right](https://start.me/p/vjY406/researching-the-extreme-right)
20. [Defining Second Generation OSINT For The Defence Enterprise](https://www.rand.org/content/dam/rand/pubs/research_reports/RR1900/RR1964/RAND_RR1964.pdf.)
----

### OSINT Blog Search
1. [OSINT Blog Search Engine](https://cse.google.com/cse?cx=fd4729049350a76d0)
----

### Start.me Resources
1. [1. OSINT Toolset](https://start.me/p/MEw7be/1-osint-toolset)
2. [Access-osint](https://start.me/p/GEbBzz/access-osint)
3. [AML Toolbox](https://start.me/p/rxeRqr/aml-toolbox)
4. [AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
5. [Bellingcat OSINT landscape](https://start.me/p/ELXoK8/bellingcat-osint-landscape)
6. [Brians Links To Things](https://start.me/p/kxB4eX/brians-links-to-things)
7. [Commandergirl's suggestions](https://start.me/p/1kJKR9/commandergirl-s-suggestions)
8. [Core OSINT Skills](https://start.me/p/ydEgyG/core-osint-skills)
9. [Dating apps and hook-up sites for investigators](https://start.me/p/VRxaj5/dating-apps-and-hook-up-sites-for-investigators)
10. [FAROS OSINT Resources](https://start.me/p/1kvvxN/faros-osint-resources)
11. [Financial Crime Homepage](https://start.me/p/eknElD/financial-crime-homepage?fbclid=IwAR1xieDn1TWldf32dW6s4w9G7O80UKcykH0Q4CHqj6pH90U32jXnW9Zpko4)
12. [Fusion Intelligence](https://start.me/p/dl7q50/fusion-intelligence)
13. [Gewalt Auf Pornoplattformen](https://start.me/p/kxwppk/dossier-digitale-gewalt-auf-pornoplattformen)
14. [Global](https://start.me/p/rxDad8/global)
15. [Illicit Trade - OSINT](https://start.me/p/Yan2Nq/15illicittf)
16. [INT.INFRA](https://start.me/p/V0PXgr/int-infra)
17. [Jonny OSINT LINKS](https://start.me/p/q6naJo/osint-links)
18. [Jornadas OSINT](https://start.me/p/BnBb5v/jornadas-osint)
19. [JUNGLA OSINT](https://start.me/p/7k48PK/jungla-osint-por-ra1000)
20. [OSINT resarch databases](https://start.me/p/QRENnO/databases)
21. [Nixintel's OSINT Resource List](https://start.me/p/rx6Qj8/nixintel-s-osint-resource-list)
22. [Open Source D-A-CH](https://start.me/p/b5DNmo/open-source-d-a-ch)
23. [Open Source Investigative Tools](https://start.me/p/gyvaAJ/open-source-investigative-tools)
24. [ORN Werkzeugkasten](https://start.me/p/m65arv/orn-werkzeugkasten)
25. [OSINT](https://start.me/p/b56xX8/osint)
26. [OSINT  -  Phone (Russian)](https://start.me/p/z4jqxM/osint-phone?fbclid=IwAR2llSJJFt4WU_zdk-EWmUlfvpv8nw86glU7R7KgqA2GSZFm3XgZK8n9uOE)
27. [OSINT  -  Telegram  (Russian)](https://start.me/p/NxkOl7/osint-telegram?fbclid=IwAR1ogXZwvJAy6mBy-QVoe3vL31njmtbio6bEgmAex-eA2Ks6yUfw3XtTm8E)
28. [OSINT Framework](https://start.me/p/ZME8nR/osint)
29. [OSINT INSTITUTE](https://start.me/p/Pw5GB4/osint-institute?fbclid=IwAR2V2z93inR0NscDTej192ZsOKhEv4NKGg7hdg6lUW_gg8UUIALdKPiR8mw)
30. [OSINT Resources](https://start.me/p/1kAP0b/osint-resources)
31. [OSINT Tools (curated by Lorand Bodo)](https://start.me/p/7kxyy2/osint-tools-curated-by-lorand-bodo)
32. [OSINT Trainingcenter](https://start.me/p/vjaQJQ/trainingcenter)
33. [Osint-IO](https://start.me/p/1kOJ9N/16osint-io)
34. [OSINT US](https://start.me/p/GEQXv7/osint-us)
35. [OSINT4ALL](https://start.me/p/L1rEYQ/osint4all)
36. [Osintia OSINT](https://start.me/p/0PGKad/darkweb)
37. [OSTER - Investigate terrorist Networks](https://start.me/p/7kmvEK/oster)
38. [Risk Dashboard](https://start.me/p/vjKgz1/risk-dashboard)
39. [SA OSINT](https://start.me/p/5vN2a0/sa-osint)
40. [Social Media Intelligence Dashboard](https://start.me/p/m6MbeM/social-media-intelligence-dashboard)
41. [SOCMINT (Osint Framework)](https://start.me/p/Wp1kpe/socmint)
42. [Sprp77 Search Engines](https://start.me/p/b5ynOQ/sprp77-search-engines)
43. [Technisette.com - OSINT](https://start.me/p/m6XQ08/osint)
44. [The BBC Africa Eye /Forensics Dashboard](https://start.me/p/m6OJgv/the-bbc-africa-eye-forensics-dashboard)
45. [The Ultimate OSINT Collection](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
46. [Threat Intel (mostly OSINT)](https://start.me/p/rxRbpo/ti?fbclid=IwAR2zJ1yU2gLcfkQuo0m4m2QWNRuFbXCfWLrE_MN-YI4wo8HptOQY2ywnj0k)
47. [Threat Intelligence OSINT](https://start.me/p/rxRbpo/ti)
48. [TOOLKIT](https://start.me/p/W1AXYo/toolkit)
49. [Tools](https://start.me/p/Wrrzk0/tools)
50. [Verification Toolset](https://start.me/p/ZGAzN7/verification-toolset)
51. [Forensics Tools](https://start.me/p/q6mw4Q/forensics)
52. [Researching the Extreme Right](https://start.me/p/vjY406/researching-the-extreme-right)
53. [V3nari Bookmarks](https://start.me/p/1kxyw9/v3nari-bookmarks)
54. [Terrorism & Radicalisation Research Dashboard](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
----

### Country or Region Specific
1. [OSINT-GLOBAL (Non-US)](https://start.me/p/jj2XEr/osint-global-non-us?fbclid=IwAR2-cpVOh5z707cMxQ-AA_8bsoNw55HZUVNlcXwQrJSVBnN7-5lDywDvxCw)
2. [World](https://start.me/p/kxNv55/cnty-world)
3. [United Kingdom](https://start.me/p/gyq0Rz/united-kingdom)
4. [USA](https://start.me/p/kxMBv9/cnty-usa)
5. [Hungary](https://start.me/p/kxGLzd/hun-osint)
6. [China](https://start.me/p/GE7JQb/osint)
7. [OSINT CHINE](https://start.me/p/7kLY9R/osint-chine)
8. [World News](https://start.me/p/lLaoXv/07world)
9. [Russia - OSINT Phone](https://start.me/p/z4jqxM/osint-phone)
10. [OSINT Framework - Poland](https://www.otwartezrodla.pl/?fbclid=IwAR3E_dK20xcC3hWPeGHZxYS9u4v1DbW_wFv-fLLEJr-P_fvKA1JjeLWljYg)
11. [Russia - OSINT Telegram](https://start.me/p/NxkOl7/osint-telegram)
12. [OSINT Resources in Canada](https://start.me/p/aLe0vp/osint-resources-in-canada)
----

### Websites and Blogs
1. [IntelTechniques.com](https://inteltechniques.com/index.html)
2. [OSINT Curious Project](https://osintcurio.us/)
3. [Sector035](https://sector035.nl/)
4. [Technisette](https://www.technisette.com/p/home)
5. [Osint Dojo](https://www.osintdojo.com/)
6. [Hunchly - OSINT Articles](https://hunch.ly/osint-articles/)
7. [STATE of OSINT](https://stateofosint.com/)
8. [Webbreacher](https://webbreacher.com/)
9. [LORÁND BODÓ](https://www.lorandbodo.com/)
10. [OSINT Training Center](https://www.aware-online.com/en/)
11. [Osintme.com](https://www.osintme.com/)
12. [Secjuice](https://www.secjuice.com/)
13. [[The OSINTion's OSINT Wiki]](https://theosintion.com/wiki/doku.php?id=home)
14. [Learn All The Things](https://www.learnallthethings.net/)
15. [i-intelligence](https://i-intelligence.eu/)
16. [https://osint.link/](https://osint.link/)
17. [UK-OSINT](https://www.uk-osint.net/index.html)
18. [Online Strategies - OSINT](http://www.onstrat.com/osint/)
19. [os2int - Recommended OSINT Tools and Processes](https://os2int.com/toolbox/)
20. [OSINT Framework](https://osintframework.com/)
21. [Arnoreuser.com](https://arnoreuser.com/)
22. [OSINT Essentials](https://www.osintessentials.com/)
23. [OSINT Techniques](https://www.osinttechniques.com/)
24. [OSINT Toolkit - Andy Black Associates](https://www.andyblackassociates.co.uk/resources-andy-black-associates/osint-toolkit/)
25. [MIDASearch](https://midasearch.org/)
26. [NixIntel](https://nixintel.info/)
27. [Key Findings](https://keyfindings.blog/)
28. [OSINT Team](https://osint.team/home)
29. [OSINT Combine](https://www.osintcombine.com/)
30. [AccessOSINT](https://accessosint.com/)
31. [Exposing the Invisible](https://exposingtheinvisible.org/)
32. [bellingcat](https://www.bellingcat.com/)
33. [Trace Labs](https://www.tracelabs.org/)
34. [Searchlight Community](https://searchlight.community/)
35. [@hatless1der | Blog](https://hatless1der.com/)
36. [NCPTF.org](https://www.ncptf.org/)
37. [The Many Hats Club](https://themanyhats.club/)
38. [Intelligence X Blog](https://blog.intelx.io/)
39. [DFIR Diva](https://dfirdiva.com/)
40. [Wonder How To Blog - OSINT](https://www.wonderhowto.com/search/osint/)
41. [Hackers Arise - OSINT](https://www.hackers-arise.com/osint)
42. [Skopenow News](https://www.skopenow.com/news)
43. [Osinteditor](https://www.osinteditor.com/)
44. [Research Clinic](http://researchclinic.net/)
45. [4n6link.com](https://4n6link.com/)
46. [WeSupportTor · Wiki](https://gitlab.torproject.org/legacy/trac/-/wikis/org/projects/WeSupportTor)
47. [Blockint](https://www.blockint.nl/)
48. [Ro-Intelligence](https://ro-intelligence.com/)
49. [OSINT and Internet Dumpster Diving by Stu](https://www.getrevue.co/profile/cybersecstu)
50. [Boolean Strings](https://booleanstrings.com/)
----

### Twitter Profiles
1. [Tokyo_v2](https://twitter.com/Tokyo_v2)
2. [TCM Security](https://twitter.com/TCMSecurity)
3. [Heath Adams (@thecybermentor)](https://twitter.com/thecybermentor)
4. [FolkerJr](https://twitter.com/FolkerJr)
5. [Ph055a](https://twitter.com/ph055a)
6. [Knowsint1](https://twitter.com/Knowsint1)
7. [Osinteditor](https://twitter.com/osinteditor)
8. [Serge Courrier (@secou)](https://twitter.com/secou)
9. [i-intelligence](https://twitter.com/i_intelligence)
10. [Corma investigations](https://twitter.com/corma_agency)
11. [Bruno Mortier (@digintelosint)](https://twitter.com/digintelosint)
12. [Justin Seitz (@jms_dot_py)](https://twitter.com/jms_dot_py)
13. [Zewensec](https://twitter.com/Zewensec)
14. [Global Investigative Journalism Network](https://twitter.com/gijn)
15. [Theprescomm](https://twitter.com/theprescomm)
16. [TryHackMe (@RealTryHackMe)](https://twitter.com/RealTryHackMe)
17. [Brenna__smith](https://twitter.com/brenna__smith)
18. [RIS OSINT (@ArnoReuser)](https://twitter.com/ArnoReuser)
19. [CynOsint](https://twitter.com/CynOsint)
20. [TheOSINTion](https://twitter.com/TheOSINTion)
21. [Verif!cation Quiz Bot (@quiztime)](https://twitter.com/quiztime)
22. [ʜᴇɴᴋ ᴠᴀɴ ᴇss (@henkvaness)](https://twitter.com/henkvaness)
23. [OSINTDojo](https://twitter.com/OSINTDojo)
24. [DarknessGate](https://twitter.com/DarknessGate)
25. [Skopenow](https://twitter.com/Skopenow)
26. [NorOsint](https://twitter.com/NorOsint)
27. [IWN_LX](https://twitter.com/IWN_LX)
28. [DfirDiva](https://twitter.com/DfirDiva)
29. [Jane Lytvynenko 🤦🏽‍♀️🤦🏽‍♀️🤦🏽‍♀️](https://twitter.com/JaneLytv)
30. [Elisethoma5](https://twitter.com/elisethoma5)
31. [Justin Nordine (@jnordine)](https://twitter.com/jnordine)
32. [uk-osint.net 🇪🇺🏳️‍🌈😷](https://twitter.com/UKOSINT)
33. [Benjamin Strick (@BenDoBrown)](https://twitter.com/BenDoBrown)
34. [Cybersecstu](https://twitter.com/cybersecstu)
35. [Griffin (@hatless1der)](https://twitter.com/hatless1der)
36. [Adrian Korn (@AK47Intel)](https://twitter.com/AK47Intel)
37. [Wondersmith_Rae](https://twitter.com/wondersmith_rae)
38. [Kirbstr](https://twitter.com/kirbstr)
39. [Micah (@WebBreacher)](https://twitter.com/WebBreacher)
40. [𝕊𝕖𝕔𝕥𝕠𝕣𝟘𝟛𝟝](https://twitter.com/Sector035)
41. [MW-OSINT](https://twitter.com/MwOsint)
42. [Secjuice](https://twitter.com/Secjuice)
43. [CONINT_io](https://twitter.com/CONINT_io)
44. [OSINTgeek](https://twitter.com/OSINTgeek)
45. [Christina Lekati](https://twitter.com/ChristinaLekati)
46. [Loránd Bodó](https://twitter.com/LorandBodo)
47. [Sinwindie](https://twitter.com/sinwindie)
48. [Aware Online](https://twitter.com/aware_online)
49. [nixintel](https://twitter.com/nixintel)
50. [Jake Creps](https://twitter.com/jakecreps)
51. [OSINT Research](https://twitter.com/OSINT_Research)
52. [Technisette](https://twitter.com/technisette)
53. [Dutch OsintGuy](https://twitter.com/dutch_osintguy)
54. [Michael Bazzell (@IntelTechniques)](https://twitter.com/IntelTechniques)
55. [We Are OSINTCurious](https://twitter.com/OsintCurious)
56. [Bellingcat](https://twitter.com/bellingcat)
57. [OSINTessentials](https://twitter.com/OSINTessentials)
58. [OSINT (@AccessOSINT)](https://twitter.com/AccessOSINT)
59. [Osintme](https://twitter.com/osintme)
60. [OSINT Combine](https://twitter.com/osintcombine)
61. [OSINT Techniques](https://twitter.com/OSINTtechniques)
----

### Google Doc Resources
1. [Bellingcat's Online Investigation Toolkit](https://docs.google.com/spreadsheets/d/18rtqh8EG2q1xBo2cLNyhIDuK9jrPGwYr9DI2UncoqJQ/edit#gid=930747607)
2. [CTI & OSINT Online Resources](https://docs.google.com/spreadsheets/d/1klugQqw6POlBtuzon8S0b18-gpsDwX-5OYRrB7TyNEw/htmlview)
3. [OSINT for Finding People](https://docs.google.com/spreadsheets/d/1JxBbMt4JvGr--G0Pkl3jP9VDTBunR2uD3_faZXDvhxc/htmlview)
4. [OSINT Tool Comparison Table](https://docs.google.com/spreadsheets/d/18U1qcaPaqIF8ERVLI-g5Or3gUbv0qP_-JUtc0pbEs0E/htmlview#gid=0)
5. [Sans Osint Resources](https://docs.google.com/document/d/1CrfMFa6ww2Awz37jYon_M8Uq6Yl1eCpcjfbnyRDibLQ/view?pru=AAABd7wXDp8*R9brczng8RPp1FUu48cFPw#heading=h.pk1tm314pji)
6. [Osint Curious OSINT Resource List](https://docs.google.com/document/d/14li22wAG2Wh2y0UhgBjbqEvZJCDsNZY8vpUAJ_jJ5X8/edit)
7. [Public Information Opt-Out Guide - OSINT Ninja](https://docs.google.com/spreadsheets/d/1UY9U2CJ8Rnz0CBrNu2iGV3yoG0nLR8mLINcnz44XESI/htmlview#gid=1864750866)
----

### GitHub
1. [Ph055a/OSINT_Collection](https://github.com/Ph055a/OSINT_Collection)
2. [WebBreacher](https://github.com/WebBreacher)
3. [IVMachiavelli/OSINT_Team_Links](https://github.com/IVMachiavelli/OSINT_Team_Links)
4. [jivoi/awesome-osint](https://github.com/jivoi/awesome-osint)
5. [Github](https://github.com/)
6. [ilovecode2018/awesome-osint](https://github.com/ilovecode2018/awesome-osint)
7. [jmortega (José Manuel Ortega) · GitHub](https://github.com/jmortega)
8. [5nacks/OSINTBookmarks](https://github.com/5nacks/OSINTBookmarks)
9. [r3mlab/datajournalism-resources](https://github.com/r3mlab/datajournalism-resources)
----

### YouTube
1. [OSINT Curious](https://www.youtube.com/channel/UCjzceWf-OT3ImIKztzGkipA)
2. [Bendobrown](https://www.youtube.com/channel/UCW2WOgSiMr216a27KWG_aqg)
3. [conINT](https://m.youtube.com/channel/UCBtSOceclpKcvunVNw82tFQ)
4. [Null Byte - OSINT](https://www.youtube.com/c/NullByteWHT/search?query=osint)
5. [0x4rkØ - OSINT](https://www.youtube.com/c/0x4rk%C3%98/search?query=osint)
6. [Security Weekly - OSINT](https://www.youtube.com/c/SecurityWeekly/search?query=osint)
7. [Adrian Crenshaw - OSINT](https://www.youtube.com/user/irongeek/search?query=osint)
8. [SANS Institute - OSINT](https://m.youtube.com/c/sansinstitute/search?query=Osint&disable_polymer=true&itct=CAYQuy8YACITCLPgv4aH3e0CFQh0sgodKwEODA%3D%3D)
9. [Trace Labs](https://m.youtube.com/channel/UCezKbcbnYtrwRXfGzgQMI3w)
10. [Drop In And Learn - OSINT](https://m.youtube.com/channel/UCAqnnQkeSVTC3ZJ7urNiD8Q/search?query=Osint)
11. [Ntrepid - OSINT](https://m.youtube.com/user/ntrepidcorp/search?query=Osint)
12. [OSINT Insiders](https://www.youtube.com/playlist?list=PLgC1X8zH-TyIFWSRjhmRL-aA6Qj7Bb8E7&mkt_tok=eyJpIjoiTUdNek1qa3haak5tWVdKaCIsInQiOiJwY1lNaDlQQmUrRm9ibTVaU0VDeFFPQWJVUlZ4YTd4ZWdmdUFkMDZTdDgrTGFQQ0d5Nk5FNjFvcENjVHZvdlVHWnV3b1BZT3lHYUpzV0pGcmZWTUZvRlNmUG9BVExWUnJpKzZBeCtQeEdtWUdUdUc5eHB4cnA5UmZ5Z2tHbmU3cCJ9)
13. [Irfan Shakeel](https://www.youtube.com/channel/UCyEXkaNrS9TNiTnyhKj3iQA)
14. [intelligence-i1 - Andy Fordred](https://www.youtube.com/channel/UCcHd6FBS7V0e4hTNHePq_hQ/featured)
15. [OSINT Dojo](https://www.youtube.com/channel/UChbp7r-Lezl1CBNQWBDYGeQ?app=desktop)
----

### Medium Blogs
1. [What The OSINT!](https://medium.com/what-the-osint)
2. [Petro Cherkasets](https://medium.com/@Peter_UXer)
3. [Sector035](https://medium.com/@sector035)
4. [The OSINT Toolkit](https://medium.com/osint/the-osint-toolkit-3b9233d1cdf9)
5. [Steve Micallef](https://medium.com/@micallst)
6. [Irfan Shakeel](https://medium.com/@irfaanshakeel)
7. [Hunchly](https://medium.com/@hunchly)
8. [Argonyte](https://medium.com/@argonyte.cybersec)
9. [Quiztime](https://medium.com/quiztime)
10. [wondersmith_rae](https://wondersmithrae.medium.com/)
11. [Osint – Human Rights Center](https://medium.com/humanrightscenter/tagged/osint)
12. [Somdev Sangwan](https://s0md3v.medium.com/)
13. [OSINT Essentials](https://osintessentials.medium.com/)
----

### OSINT PDF Resources
1. [Open Source INTelligence (OSINT) Reference Sheet](https://tpia.com/resources/Pictures/2019%20CPE%20files/OSINT%20Resources.pdf)
2. [i-intelligence.eu - OSINT Handbook 2020.pdf](https://i-intelligence.eu/uploads/public-documents/OSINT_Handbook_2020.pdf)
3. [The Not Yet Exploited Goldmine of OSINT: Opportunities,Open Challenges and Future Trends](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8954668)
4. [Open Source Intelligence Methodology 2019](https://phs.brage.unit.no/phs-xmlui/bitstream/handle/11250/2617479/master_Furuhaug.pdf?sequence=1&isAllowed=y)
5. [Open Source Intelligence and OSINT Application 2019](https://www.theseus.fi/bitstream/handle/10024/171315/Tuominen_Sanna.pdf?sequence=2)
6. [Compass Security - OSINT Cheat Sheet](https://compass-security.com/fileadmin/Research/White_Papers/2017-01_osint_cheat_sheet.pdf?fbclid=IwAR0mW2xlIbBI8WnCn8WFyglAgUJzGPpLXw3rT9xA7Z3-4242S5HSYE1Al5c)
7. [Defining Second Generation OSINT For The Defence Enterprise](https://www.rand.org/content/dam/rand/pubs/research_reports/RR1900/RR1964/RAND_RR1964.pdf.)
----

### OSINT Books
1. [Open Source Intelligence Techniques by Michael Bazzell](https://inteltechniques.com/book1.html)
2. [Open Source Intelligence Methods and Tools: A Practical Guide to Online Intelligence 1st ed. Edition](https://www.amazon.com/Open-Source-Intelligence-Methods-Tools/dp/1484232127/ref=sr_1_5?dchild=1&keywords=osint&qid=1611904418&sr=8-5)
----

### OSINT Communities and Platforms
1. [Discord](https://discord.com/)
2. [The OSINT Curious Project](https://discord.com/invite/FHagzwXqbT)
3. [Reddit](https://www.reddit.com/)
4. [Open Source Intelligence • r/OSINT](https://www.reddit.com/r/OSINT/)
5. [r/Intelligence](https://www.reddit.com/r/Intelligence/)
6. [Team Searchlight - OSINT AMA : cybersecurity](https://www.reddit.com/r/cybersecurity/comments/k9sjhi/team_searchlight_osint_ama/)
----

### OSINT Combine OSINT Bookmarks
1. [OSINT Bookmarks](https://www.osintcombine.com/osint-bookmarks)
----

### Toolkits / Other Resources
1. [List of Resources OSINT Summit 2021](https://www.sans.org/blog/list-of-resource-links-from-open-source-intelligence-summit-2021/)
2. [OSINT Bookmarks](https://www.osintcombine.com/osint-bookmarks)
3. [OSINT Toolkit](https://one-plus.github.io/access.html?fbclid=IwAR3EmpxIo-EgAzz31kHhpnE8cYAwyAhqivBwDDCnPnE39VU5vci-oJoUhtY)
4. [OSINT Tools](https://www.aware-online.com/en/osint-tools/)
5. [Telegram Channels](https://telegramchannels.me/)
6. [Alexa top sited different countries](https://www.alexa.com/topsites/countries)
7. [Intelligence X Tools](https://intelx.io/tools)
8. [OSINTgeek | Tools](https://osintgeek.de/tools)
9. [GIJN Toolbox](https://gijn.org/series/the-toolbox/)
10. [Go Find Who Toolkit](https://gofindwho.com/)
11. [Paliscope - OSINT Investigation Tools](https://www.paliscope.com/2020/11/04/200-of-our-best-osint-investigation-tools-free-download/)
12. [OSINT RECHERCHE RESSOURCEN](https://atlas.mindmup.com/digintel/digital_intelligence_training/index.html)
13. [OSINT Toolkit | Links To Digital Tools Used By Researchers](https://comskills-ukraine.co.uk/resources/osint-toolkit/)
14. [Data For Journalists: A Practical Guide for Computer-Assisted Reporting | Investigative Journalism Education Consortium	Data For Journalists: A Practical Guide for Computer-Assisted Reporting – Investigative Journalism Education Consortium](https://ijec.org/databook/)
15. [Open Source Intelligence (OSINT) - 4VIO](https://4vio.com/osint?fbclid=IwAR2M4Z009pqBpF1AN6jf7eRWV13BzIY0_CoFzTppSyf0M38qAd41DNo9_3c)
16. [Rmusser OSINT Links](https://rmusser.net/docs/Osint.html)
17. [Skip Tracing Framework](https://makensi.es/stf/)
18. [The Kit 1.0](https://kit.exposingtheinvisible.org/en/contents.html)
19. [Bibliography of Essential Sources in OSINT](https://bib.opensourceintelligence.biz/)
----

### OSINT Tutorials
1. [Technisette Tutorials](https://start.me/p/aLBELX/tutorials)
2. [Jake's Resource Pages - OSINT Tutorials](https://sites.google.com/view/osint-tutorials/home)
3. [Bellingcat - Justin Seitz](https://www.bellingcat.com/author/justin-seitz/)
4. [A Guide to Open Source Intelligence (OSINT)](https://www.cjr.org/tow_center_reports/guide-to-osint-and-hostile-communities.php)
5. [OSINT Tutorials](https://www.aware-online.com/en/osint-tutorials/)
6. [Introduction to OSINT  – OSINTCurio](https://osintcurio.us/2020/01/16/introduction-to-osint-video/?utm_source=GIJN+Mailing+List&utm_campaign=7664b2669d-EMAIL_CAMPAIGN_2020_02_05_02_01&utm_medium=email&utm_term=0_eae0e8c5a9-7664b2669d-121222577&mc_cid=7664b2669d&mc_eid=4fecedb4d5)
7. [Sans Security Google Cheat Sheet](https://www.sans.org/security-resources/GoogleCheatSheet.pdf)
8. [2019 OSINT Guide](https://www.randhome.io/blog/2019/01/05/2019-osint-guide/)
----

### Use Your Phone to Verify Online Material
1. [Verification on the go: How to use your phone to verify online material | Webinar - YouTube](https://www.youtube.com/watch?v=MX3U0-F4Zvo)
2. [First Draft's Toolkit](https://firstdraft-toolkit.glideapp.io/)
----

### OSINT - Practical Training
1. [National Missing Persons Aackathon AU](https://www.missingpersonshackathon.com.au/home)
2. [Cyber Detective CTF - OSINT Focussed](https://ctf.cybersoc.wales/)
3. [Verif!cation Quiz Bot (@quiztime)](https://twitter.com/quiztime)
4. [TryHackMe Geolocating Images](https://tryhackme.com/room/geolocatingimages)
5. [TryHackMe Google Dorking](https://tryhackme.com/room/googledorking)
6. [TryHackMe OhSINT](https://tryhackme.com/room/ohsint)
7. [Tryhackme Searchlight Osint](https://tryhackme.com/room/searchlightosint)
8. [Sourcing.Games – Games For Recruiters and Sourcers](https://sourcing.games/)
9. [Ictf.io](https://ictf.io/)
10. [GeoGuessr](https://www.geoguessr.com/)
11. [Quiztime – Medium](https://medium.com/quiztime)
12. [CTF Academy Osint Challenge](http://ctfacademy.net/osint/index.php)
13. [First Draft Online Verification Exercise](https://ftp.firstdraftnews.org/articulate/temp/ovcR/story_html5.html)
14. [Roppers Academy CTF Course](https://www.hoppersroppers.org/courseCTF.html)
15. [Github (apsdehal/awesome) CTF Resources](https://github.com/apsdehal/awesome-ctf)
16. [Reddit - What Is This Thing](https://www.reddit.com/r/whatisthisthing/)
----

### Trace Labs
1. [Trace Labs - Get Involved](https://www.tracelabs.org/get-involved)
2. [Trace Labs Search Party](https://www.tracelabs.org/initiatives/search-party)
3. [Trace Labs OSINT Search Party CTF](https://www.youtube.com/playlist?list=PLlaJQR699XLt4ib7yWgvaWM7KA12uTlB9)
4. [Getting Started with Trace Labs](https://www.youtube.com/watch?v=7OrI4MYv9i4)
5. [Join Trace Labs on Slack | Slack](https://tracelabs.slack.com/join/shared_invite/zt-v9lce1ra-L9sV4ULfLMGsyPbrXxlKkw#/shared-invite/email)
----

### Help Law Enforcement
1. [Interpol - Help Us Find](https://www.interpol.int/What-you-can-do/Help-us-find)
2. [INTERPOL - Help Track Environmental Fugitives](https://www.interpol.int/News-and-Events/News/2019/INTERPOL-makes-public-appeal-to-help-track-environmental-fugitives)
3. [ECAP - Endangered Child Alert Program](https://www.fbi.gov/wanted/ecap)
4. [Stop Child Abuse – Trace an Object](https://www.europol.europa.eu/stopchildabuse)
5. [Europe's most wanted](https://eumostwanted.eu/)
6. [DEA - Most Wanted Fugitives](https://www.dea.gov/fugitives)
7. [FBI - Most Wanted](https://www.fbi.gov/wanted)
8. [UK National Crime Agency - Most Waned](https://www.nationalcrimeagency.gov.uk/most-wanted)
----

