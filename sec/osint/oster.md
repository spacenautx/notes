### Other terrorism research dashboard
1. [the radicalisation & terrorism research dashboard](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
----

### Radicalisation
1. [Profiles of Individual Radicalization in the United States (PIRUS)](http://www.start.umd.edu/data-tools/profiles-individual-radicalization-united-states-pirus)
2. [Terrorism in America After 9/11](https://www.newamerica.org/in-depth/terrorism-in-america/)
----

### Terrorism classification
1. [Consilium.europa.eu/en/templates/content.aspx](http://www.consilium.europa.eu/en/templates/content.aspx/?id=26698)
2. [Foreign Terrorist Organizations](https://www.state.gov/j/ct/rls/other/des/123085.htm)
----

### Dedicated Counter Terrorism Research Centers/ Think Tanks
1. [ICCT Improves Standing in Global Think Tank Index](https://icct.nl/)
2. [Counterterrorism](https://www.rand.org/topics/counterterrorism.html)
3. [Ctc.usma.edu](https://ctc.usma.edu/)
4. [Ict.org.il/Home.aspx](http://www.ict.org.il/Home.aspx#gsc.tab=0)
5. [Cat-int.org](http://cat-int.org/?lang=en)
6. [Searcct.gov.my](http://www.searcct.gov.my/)
7. [START.umd.edu](http://www.start.umd.edu/)
8. [Perspectives on Terrorism](http://www.terrorismanalysts.com/pt/index.php/pot/index)
9. [Criticalthreats.org](https://www.criticalthreats.org/)
10. [Frc.org.pk](http://frc.org.pk/)
11. [Hiraal Institute | Addressing Horn of Africa's Security Challenges](https://hiraalinstitute.org/)
12. [Perspectives on Terrorism](https://www.universiteitleiden.nl/perspectives-on-terrorism)
13. [Western Balkan Counter-Terrorism initiative](https://wbcti.wb-iisg.com/)
----

### Terrorist groups
1. [Terrorists and Extremists Database (TED)](https://www.counterextremism.com/extremists)
2. [Gctat.org](http://www.gctat.org/)
3. [The Guantánamo Docket](https://www.nytimes.com/interactive/projects/guantanamo)
4. [Web.stanford.edu/group/mappingmilitants/cgi-bin/groups/view/1](http://web.stanford.edu/group/mappingmilitants/cgi-bin/groups/view/1)
5. [Web.stanford.edu/group/mappingmilitants/cgi-bin/groups/view/65](http://web.stanford.edu/group/mappingmilitants/cgi-bin/groups/view/65)
6. [Investigativeproject.org](https://www.investigativeproject.org/)
7. [Jihad Identifier Database](http://jihadintel.meforum.org/)
8. [Principaux groupes rebelles/ djihadistes Idlib](https://www.syriaintel.com/conflit-militaire/les-principaux-groupes-rebelles-et-djihadistes-dans-la-region-idlib/)
----

### Foreign fighters
1. [Jihadi foreign fighters monitor](https://dwh.hcss.nl/apps/ftf_monitor/)
2. [Foreign Fighters | ICCT](https://icct.nl/topic/foreign-fighters/)
3. [START US Foreigner fighters](https://www.dhs.gov/sites/default/files/publications/OPSR_TP_Overview-Profiles-Individual-Radicalization-US-Foreign-Fighters_508.pdf)
4. [Talking with FF](http://www.tsas.ca/wp-content/uploads/2018/02/TSASWP16-14_Dawson-Amarasingam-Bain.pdf)
5. [report on Leaked Mujahid Data](https://ctc.usma.edu/app/uploads/2016/11/Caliphates-Global-Workforce1.pdf)
6. [Www.egmontinstitute.be](http://www.egmontinstitute.be/content/uploads/2018/02/egmont.papers.101_online_v1-3.pdf?type=pdf)
7. [FF in Libya](https://www.washingtoninstitute.org/uploads/PolicyNote45-Zelin.pdf)
8. [Chechen in Syria](http://www.chechensinsyria.com)
----

### Terrorism Financing
1. [Counter Terrorism Sanctions](https://www.treasury.gov/resource-center/sanctions/Programs/pages/terror.aspx)
2. [Money Jihad](https://moneyjihad.wordpress.com/)
3. [AML resources](https://www.acams.org/aml-resources/combatting-terrorist-financing/)
4. [Terrorism Financing by RAND](https://www.rand.org/topics/terrorism-financing.html)
5. [US financial sanctions database](https://sanctionssearch.ofac.treas.gov/)
6. [EU sanctions list](https://webgate.ec.europa.eu/cas/login?loginRequestId=ECAS_LR-10136318-d6tNHsnzv2dI94jgZ0a9tpPb1Q7X03967BIIKd3kdEOmHaF48ib62uO5BYsFjica7LAzQ8DjowhbXmW7XazURS0-jpJZscgsw0KaEwGAvEFBby-D5q7PUHymwodA4rwIjujzZXX00VfS0vfzsfscHcxFxZ0)
7. [Following the Money - report](https://www.cnas.org/publications/reports/following-the-money-1)
----

### Terrorism financing: hawala & money exchanger system
1. [ROLE OF HAWALA](https://www.imolin.org/pdf/imolin/Role-of-hawala-and-similar-in-ml-tf-1.pdf)
2. [Money Exchange Dealers of Kabul](http://documents.worldbank.org/curated/en/335241467990983523/pdf/269720PAPER0Money0exchange0dealers.pdf)
3. [Somalia Licensed Money Remittance Providers (Hawalas)](http://www.centralbank.gov.so/hawalas.html)
4. [Syrian hawala](http://www.cashlearning.org/downloads/beechwood-technical-assessment-syria-31-july-15.pdf)
5. [Money exchangers peshawar](http://www.peshawar.ebizpk.com/currency-exchange-companies-peshawar.htm)
6. [Money exchanger islamabad](http://www.ebizpk.com/money-changers-islamabad.htm)
7. [Hawala study OFAC](https://www.treasury.gov/resource-center/terrorist-illicit-finance/Documents/FinCEN-Hawala-rpt.pdf)
----

### Terrorism financing: cyber financing
1. [Bitcoin Address Lookup, Checker and Alerts](https://bitcoinwhoswho.com/)
2. [Blockchain](https://www.blockchain.com/)
3. [WalletExplorer.com: smart Bitcoin block explorer](https://www.walletexplorer.com/)
4. [Bitcoin Block Explorer](https://blockexplorer.com/)
----

### Terrorism financing: charities
1. [Charity Navigator](https://www.charitynavigator.org/)
2. [List of charities accused of ties to terrorism](https://en.wikipedia.org/wiki/List_of_charities_accused_of_ties_to_terrorism)
----

### GEOINT for Terro
1. [Annual Terrorist Attacks](https://storymaps.esri.com/stories/terrorist-attacks/)
2. [Mapping Militant Organizations](http://web.stanford.edu/group/mappingmilitants/cgi-bin/)
3. [Arcgis.com/home/item.html](https://www.arcgis.com/home/item.html?id=9c1a2ce3f83a402ba6d558851f62cf25)
4. [Ukraine Interactive map](https://liveuamap.com/)
----

### Terrorism weapons
1. [Monterey WMD Terrorism Database: Home](http://wmddb.miis.edu/)
2. [Havocscope](https://www.havocscope.com/)
3. [Conflictarm.com](http://www.conflictarm.com/)
4. [Welcome to iTrace - Please log in](https://itrace.conflictarm.com/Home/Login)
5. [Tracing IED components report](https://wb-iisg.com/wp-content/uploads/bp-attachments/4891/Tracing_The_Supply_of_Components_Used_in_Islamic_State_IEDs.pdf)
----

### Terrorism Incidents Database
1. [Global Terrorism Database](http://www.start.umd.edu/gtd/)
2. [RAND](http://smapp.rand.org/rwtid/search_form.php)
3. [Suicide attacks database](http://cpostdata.uchicago.edu/search_new.php)
4. [NACJD | Discover Data](https://www.icpsr.umich.edu/icpsrweb/NACJD/discover-data.jsp)
5. [Start.umd.edu/pubs/STARTBackgroundReport_BokoHaramRecentAttacks_May2014_0.pdf](http://www.start.umd.edu/pubs/STARTBackgroundReport_BokoHaramRecentAttacks_May2014_0.pdf)
----

### Terrorism court proceedings
1. [International crime database - terrorism](http://www.internationalcrimesdatabase.org/Cases/ByCategory/Terrorism)
2. [International crime database - foreign fighters](http://www.internationalcrimesdatabase.org/foreignfighters)
3. [AUS Court Cases](https://www.aph.gov.au/About_Parliament/Parliamentary_Departments/Parliamentary_Library/Browse_by_Topic/TerrorismLaw/Courtcases)
4. [US Court Cases](https://www.investigativeproject.org/cases.php)
5. [Terrorism Trials: Civilian Court](https://www.lawfareblog.com/topic/terrorism-trials-civilian-court)
----

### Investigation terrorism
1. [https://www.unodc.org/documents/terrorism/Publications/FTF%20SSEA/Foreign_Terrorist_Fighters_Asia_Ebook.pdf](Asia FF investigation manual)
----

### Cyber Terrorism
1. [Recordedfuture.com/al-qaeda-encryption-technology-part-1](https://www.recordedfuture.com/al-qaeda-encryption-technology-part-1/)
2. [Electronic Horizon Foundation](https://pastethis.at/K8T25rr5)
3. [Hacking for ISIS](https://fortunascorner.com/wp-content/uploads/2016/05/Flashpoint_HackingForISIS_April2016-1.pdf)
4. [Cyber Caliphate Analysis](https://www.recordedfuture.com/cyber-caliphate-analysis/)
5. [UCC exposed](http://www.securitynewspaper.com/2016/07/09/ghost-squad-hackers-dox-united-cyber-caliphate-including-mauritania-attacker/)
6. [Hacking database](https://www.zone-h.org/archive)
----

### Digital Propaganda
1. [An Analysis of Islamic State Propaganda Distribution](https://go.flashpoint-intel.com/docs/an-analysis-of-islamic-state-propaganda-distribution)
2. [Cjlab.memri.org](http://cjlab.memri.org/)
3. [Digitalislam.eu](http://www.digitalislam.eu/)
4. [Jihadology](https://jihadology.net/)
5. [Jihadoscope.com](http://www.jihadoscope.com/)
6. [SITE Intelligence Group](https://ent.siteintelgroup.com/)
7. [Jihadica.com](http://www.jihadica.com/)
8. [Pieter Van Ostayen blog](https://pietervanostaeyen.com)
----

### Digital platforms
1. [ZeroNet: Decentralized websites using Bitcoin crypto and the BitTorrent network](https://zeronet.io/)
2. [Riot – Riot – open team collaboration](https://about.riot.im/)
3. [Telegram](https://telegram.org/)
4. [Tor](https://www.torproject.org/)
5. [Pastebin](https://pastebin.com/)
6. [Risala.ga](https://risala.ga/)
----

### Counter terrorism Strategic comms
1. [CT Strategic Communications | ICCT](https://icct.nl/topic/counter-terrorism-strategic-communications-ctsc/)
2. [Understanding ISIS propaganda](https://wb-iisg.com/wp-content/uploads/bp-attachments/4844/the-virtual-caliphate-understanding-islamic-states-propaganda-strategy.pdf)
----

### Reporting
1. [Plateforme PHAROS](https://www.interieur.gouv.fr/A-votre-service/Ma-securite/Conseils-pratiques/Sur-internet/Signaler-un-contenu-suspect-ou-illicite-avec-PHAROS)
2. [CtrlSec (@CtrlSec)](https://twitter.com/CtrlSec)
3. [Report online material promoting terrorism or extremism](https://www.gov.uk/report-terrorism)
----

### Subject Matter Experts
1. [Rita Katz](https://twitter.com/Rita_Katz)
2. [Nolwenn Bervas](https://twitter.com/NoBrvs)
3. [Dabiel Lebowitz](https://twitter.com/DanielLebowitz)
4. [Gilles N [FR]](https://twitter.com/VegetaMoustache)
5. [Sidi Kounta [FR]](https://twitter.com/sidikounta7)
6. [Charles Lister (@Charles_Lister)](https://twitter.com/Charles_Lister)
7. [Wassim Nasr [FR]](https://twitter.com/SimNasr)
8. [Laurence Bindner](https://twitter.com/LoBindner)
9. [Rukmini Callimachi (@rcallimachi)](https://twitter.com/rcallimachi)
10. [Aaron Zelin](https://twitter.com/azelin)
11. [Caleb Weissenberg](https://twitter.com/Weissenberg7)
12. [Historicoblog [FR]](https://twitter.com/historicoblog4)
13. [Paweł Wójcik](https://twitter.com/SaladinAlDronni)
14. [Jerome Drevon](https://twitter.com/jeromedrev)
15. [Abdou Cisse [FR]](https://twitter.com/rebayacall)
16. [Fadi Hussein](https://twitter.com/fadihussein8)
17. [Romain Caillet [FR]](https://twitter.com/RomainCaillet)
18. [David Thomson [FR]](https://twitter.com/_DavidThomson)
19. [Julia Ebner](https://twitter.com/julie_renbe)
20. [Qalaat Al Mudiq](https://twitter.com/QalaatAlMudiq)
21. [Alastair Reed](https://twitter.com/reed_alastair)
22. [Charlie Winter](https://twitter.com/charliewinter)
23. [JM Berger](https://twitter.com/intelwire)
24. [Truls H Tønnessen](https://twitter.com/trulstonnessen)
25. [Ahmet S. Yayla](https://twitter.com/ahmetsyayla)
26. [Sebastian Bott](https://twitter.com/bott_sebastian)
27. [Mark Youngman](https://twitter.com/MarkYoungman_UK)
28. [Phil Gurski](https://twitter.com/borealissaves)
29. [A Radica](https://twitter.com/aradica911)
30. [Joas Wagemakers](https://twitter.com/JoasWagemakers)
31. [Bokostan](https://twitter.com/BokoWatch)
32. [Joanna Paraszczuk](https://twitter.com/joaska_)
33. [Rida Lyammouri](https://twitter.com/rmaghrebi)
34. [Pieter Van Ostaeyen (@p_vanostaeyen)](https://twitter.com/p_vanostaeyen)
35. [Ahmad Al-Zaky](https://twitter.com/AhmadAlZ)
36. [Hassan Hassan](https://twitter.com/hxhassan)
37. [Shiraz Maher](https://twitter.com/ShirazMaher)
38. [Lorand Bobo](https://twitter.com/LorandBodo)
----

