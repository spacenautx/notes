### OSINT Webs
1. [[ --> Investigaciones OSINT <-- ]](https://start.me/p/MEw7be/osint-toolset)
2. [[ --> SOCMINT Tools <-- ]](https://start.me/p/RMKeQv/search-social-media)
----

### Reference OSINT Frameworks
1. [OSINT Framework](https://start.me/p/ZME8nR/osint)
2. [OSINT Framework Justin Nordine](http://osintframework.com/)
3. [@IntelTechniques.com***](https://inteltechniques.com/menu.links.html)
4. [Bellingcat's Digital Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
5. [Toddington Resources](https://www.toddington.com/resources/)
6. [OSINT Arno Reuser](http://rr.reuser.biz/)
7. [i-intelligence Chris Pallaris](https://www.i-intelligence.eu/osint-tools-and-resources-handbook-2018/)
8. [Boolean Strings Irina Shamaeva](https://booleanstrings.com/)
9. [DiRT Directory](https://dirtdirectory.org/)
10. [GIJC2017 handout - Margot Williams.docx](https://drive.google.com/file/d/1pLjKuNjFyHGDiOAW7sLmX7L9o2j5kdbF/view)
11. [IVMachiavelli/OSINT_Team_Links](https://github.com/IVMachiavelli/OSINT_Team_Links)
12. [Jivoi Awesome-osint](https://github.com/jivoi/awesome-osint)
13. [Michael Bazzell OSINT Search Tool](https://inteltechniques.com/menu.html)
14. [Netbootcamp OSINT Tools](http://netbootcamp.org/osinttools/)
15. [OSINTessentials - Eoghan Sweeneyt](https://www.osintessentials.com/)
16. [Osintpost](https://osintpost.com/open-source-investigation-guides/)
17. [Qwarie UK OSINT Neil Smith](http://www.uk-osint.net/favorites.html)
18. [Reddit OSINT Page](https://old.reddit.com/r/OSINT/)
19. [OSINT Technisette](https://start.me/p/m6XQ08/osint)
20. [Research Clinic Paul Meyers](http://researchclinic.net/)
21. [Sapient Intelligence OSINT Toolbox](https://start.me/p/wMAGAQ/osint-toolbox)
22. [Search Phil Bradley](http://www.philb.com/)
23. [Sprp77 Awesome OSINT Stefanie Proto](https://drive.google.com/drive/folders/0BwyWDQ59jRVGMFp3UnBfeUEzanM)
24. [ilove OSINT on Github](https://github.com/ilovecode2018/awesome-osint)
25. [Hun-OSINT](https://start.me/p/kxGLzd/hun-osint)
26. [Open Source Intelligence (OSINT)](https://start.me/p/gy0NXp/open-source-intelligence-osint)
27. [TOOLKIT](https://start.me/p/W1AXYo/toolkit)
----

### Forensic Investigation Tools
1. [FAW - Forensics Acquisition of Websites | The first forensic browser for the acquisition of web pages](https://en.fawproject.com/)
2. [Forensic Investigator | Splunkbase](https://splunkbase.splunk.com/app/2895/)
3. [HashMyFiles: Calculate MD5/SHA1/CRC32 hash of files](https://www.nirsoft.net/utils/hash_my_files.html)
4. [ExifTool by Phil Harvey](https://www.sno.phy.queensu.ca/~phil/exiftool/)
5. [NFI Defraser](https://sourceforge.net/projects/defraser/)
6. [Hunchly](https://www.hunch.ly/)
----

### Maltego
1. [Maltego CE](https://www.paterva.com/web7/buy/maltego-clients/maltego-ce.php)
----

### Spyderfoot
1. [SpiderFoot - Official Webpage](https://www.spiderfoot.net/)
----

### Recon-NG
1. [LaNMaSteR53 / Recon-ng   — Bitbucket](https://bitbucket.org/LaNMaSteR53/recon-ng)
----

### Step 1 Virtualization Software
1. [1. Oracle VirtualBox - Linux, Windows and OSX](https://www.virtualbox.org/wiki/Downloads)
2. [2. VMware - Linux, Windows and OSX](https://www.vmware.com/sg.html)
3. [3. Hiper-V - Windows](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/about/)
4. [4. KVM - Linux](https://www.linux-kvm.org/page/Main_Page)
5. [--> Comparison of platform virtualization software](https://en.wikipedia.org/wiki/Comparison_of_platform_virtualization_software)
----

### Step 2 Virtual Desktops Alternatives
1. [1. Buscador OSINT Virtual Machine](https://inteltechniques.com/buscador/)
2. [2. Windows VM - Download Testing Microsoft Virtual Machine](https://developer.microsoft.com/es-es/windows/downloads/virtual-machines)
3. [3. SADD.IO - 15 Minutes Free VM + TOR](https://sadd.io)
4. [4. Osintux (Spanish) - OSINT Distro](http://www.osintux.org/)
5. [5. Huron (Spanish) - OSINT Distro](https://github.com/HuronOsint/OsintDistro)
6. [6. SIFT SANS Forensics VM](https://digital-forensics.sans.org/community/downloads/#overview)
7. [7. CAINE Live USB/DVD - computer forensics digital forensics](https://www.caine-live.net/)
----

### Step 2 Privacy Focus Operative Systems
1. [1. Whonix | Uses TOR](https://www.whonix.org)
2. [2. Subgraph OS | Uses TOR](https://subgraph.com/)
3. [3. Tails | Uses TOR](https://tails.boum.org/)
4. [4. Qubes OS](https://www.qubes-os.org/)
5. [5. IprediaOS | Uses I2P instead of Tor](https://www.ipredia.org/os/)
6. [8. Kali Linux](https://www.kali.org/)
7. [9. Tsurugi Linux](https://tsurugi-linux.org/index.php)
8. [10. Discreete Linux](https://www.discreete-linux.org/features.html)
----

### Step 2 Mobile Device Virtualization
1. [1. Genymotion (Android Emulator)](https://www.genymotion.com/)
2. [2. BlueStacks (Android Emulator)](https://www.bluestacks.com/es/index.html)
3. [3. Sudoapp.com (IOS Emulator)](https://sudoapp.com)
4. [4. Andy (Android Emulator)](https://www.andyroid.net/)
5. [5. KOPLAYER (Android Emulator)](http://www.koplayer.com/)
6. [6. MEmu (Android Emulator)](https://www.memuplay.com/)
7. [7. BigNox (Android Emulator)](https://www.bignox.com/)
----

### Step 3 Anonymous Connection
1. [1. Whonix](https://whonix.org)
2. [2. Tor Connection](https://www.torproject.org/index.html.en)
3. [3. VPN Gate](https://www.vpngate.net/en/)
4. [4. Safejumper - OpenVPN made simple for Windows, Mac, Linux, Android & iOS](https://proxy.sh/software)
5. [5. Private VPNs](https://www.privacytools.io/)
6. [6. I2P Anonymous Network](https://geti2p.net/en/)
7. [7. Freenet](https://freenetproject.org/author/freenet-project-inc.html)
----

### Visual Investigation Scenario
1. [1. Cherrytree – Info Management](https://www.giuspen.com/cherrytree/)
2. [2. OneNote 2016 – Digital note taking app](https://products.office.com/en-sg/onenote/digital-note-taking-app)
3. [3. VIS. Visual Investigative Scenarios platform](https://vis.occrp.org)
4. [4. Timeline editor](https://time.graphics/editor)
5. [PortableApps.com - Windows](https://portableapps.com/)
6. [ScribbleMaps](https://www.scribblemaps.com)
----

### Extensions/Addons - Privacy/security
1. [Adblock Plus (Chrome)](https://chrome.google.com/webstore/detail/adblock-plus/cfhdojbkjhnklbpkdaibdccddilifddb)
2. [Ghostery (Chrome)](https://chrome.google.com/webstore/detail/ghostery/mlomiejdfkolichcflejclcbmpeaniij?hl=en)
3. [Ghostery (FF)](https://addons.mozilla.org/en-US/firefox/addon/ghostery/)
4. [Go Back in Time (Chrome)](https://chrome.google.com/webstore/detail/go-back-in-time/hgdahcpipmgehmaaankiglanlgljlakj)
5. [Fea Keylogger](https://chrome.google.com/webstore/detail/fea-keylogger/fgkghpghjcbfcflhoklkcincndlpobja?hl=en)
6. [HTTPS Everywhere (Chrome)](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp?hl=en)
7. [HTTPS Everywhere (FF)](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)
8. [Hoxx VPN (Chrome)](https://chrome.google.com/webstore/detail/hoxx-vpn-proxy/nbcojefnccbanplpoffopkoepjmhgdgh)
9. [Hoxx VPN (FF)](https://addons.mozilla.org/en-US/firefox/addon/hoxx-vpn-proxy/)
10. [Nimbus Screenshot (Chrome)](https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj?hl=en)
11. [Nimbus Screenshot (FF)](https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/)
12. [Privacy Badger (Chrome)](https://chrome.google.com/webstore/detail/privacy-badger/pkehgijcmpdhfbdbbnkijodmdjhbjlgp)
13. [Privacy Badger (FF)](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)
14. [Referer Button (Chrome)](https://chrome.google.com/webstore/detail/referer-control/hnkcfpcejkafcihlgbojoidoihckciin?hl=en)
15. [Referer Button (FF)](https://addons.mozilla.org/en-US/firefox/addon/change-referer-button/)
16. [Resurrect Pages (FF)](https://addons.mozilla.org/en-US/firefox/addon/resurrect-pages/)
17. [User-Agent Switcher (Chrome)](https://chrome.google.com/webstore/detail/user-agent-switcher-for-c/djflhoibgkdhkhhcedjiklpkjnoahfmg)
18. [User-Agent Switcher (FF)](https://addons.mozilla.org/en-US/firefox/addon/user-agent-switcher/)
----

### Addons - PGP
1. [Mailvelope](https://www.mailvelope.com/en/)
----

### Addons - Archive
1. [Go Back in Time (Chrome)](https://chrome.google.com/webstore/detail/go-back-in-time/hgdahcpipmgehmaaankiglanlgljlakj)
2. [Passive Cache (FF)](https://addons.mozilla.org/en-US/firefox/addon/passive-cache/)
----

### Addons - Image search
1. [Exif viewer (FF)](https://addons.mozilla.org/en-US/firefox/addon/exif-viewer/)
2. [Exif viewer (Chrome)](https://chrome.google.com/webstore/detail/exif-viewer/nafpfdcmppffipmhcpkbplhkoiekndck?hl=en)
3. [Video verification InVid (FF and Chrome)](http://www.invid-project.eu/tools-and-services/invid-verification-plugin/)
4. [Who Stole My Pictures? (FF)](https://addons.mozilla.org/en-US/firefox/addon/who-stole-my-pictures/)
----

### Profiling
1. [CanaryTokens](https://www.stationx.net/canarytokens)
----

### Copy of Tools - URL Shortners
1. [Bit.do](https://bit.do)
2. [Bit.ly](http://bit.ly)
3. [Unfurlr](https://unfurlr.com/)
4. [Unshorten.it](http://unshorten.it)
----

### Verification Tools
1. [verificationhandbook.com/downloads/verification.handbook.pdf](http://verificationhandbook.com/downloads/verification.handbook.pdf)
----

### Secure Identity
1. [Fake Name Generator](https://www.fakenamegenerator.com)
----

### Blogs of interest
1. [Week in OSINT – Medium](https://medium.com/week-in-osint)
2. [IntelTechniques Blog](https://inteltechniques.com/blog)
3. [https://keyfindings.blog](https://keyfindings.blog)
4. [https://www.reddit.com/r/Intelligence](https://www.reddit.com/r/Intelligence)
5. [The Cipher Brief](https://www.thecipherbrief.com)
6. [https://www.bellingcat.com/tag/osint](https://www.bellingcat.com/tag/osint)
7. [https://www.wired.com/category/security](https://www.wired.com/category/security)
8. [https://www.intellignece101.com/category/open-source-intelligence](https://www.intellignece101.com/category/open-source-intelligence)
9. [OSINT (Open Source Intelligence) – secjuice™ – Medium](https://medium.com/secjuice/osint/home)
10. [We are OSINTCurio.us](https://osintcurio.us)
----

