### Investigate IoC
1. [URLscan.io](https://urlscan.io)
2. [urlquery.net](https://urlquery.net)
3. [DNSdumpster.com](https://dnsdumpster.com)
4. [Censys](https://censys.io)
5. [Reputation Center -  Cisco Talos](https://talosintelligence.com/reputation)
6. [Browse Indicators  - AlienVault - Open Threat Exchange](https://otx.alienvault.com/browse/indicators?type=all)
7. [Threat Crowd](https://www.threatcrowd.org)
8. [ThreatMiner.org](https://www.threatminer.org)
9. [IP Address Search Tool by Michael Bazzell](https://inteltechniques.com/osint/ip.search.html)
10. [crt.sh | Certificate Search](https://crt.sh/)
11. [VirusTotal](https://virustotal.com)
12. [IBM X-Force Exchange](https://exchange.xforce.ibmcloud.com)
13. [DNSdumpster.com](https://dnsdumpster.com)
14. [DNSTrails](https://dnstrails.com)
----

### Attack / IR Tools
1. [PowerShellMafia/PowerSploit](https://github.com/PowerShellMafia/PowerSploit)
2. [davehull/Kansa](https://github.com/davehull/Kansa)
3. [PowerShellMafia/CimSweep](https://github.com/PowerShellMafia/CimSweep)
4. [BloodHoundAD/BloodHound](https://github.com/BloodHoundAD/BloodHound)
5. [ANSSI-FR/AD-control-paths](https://github.com/ANSSI-FR/AD-control-paths)
6. [JPCERTCC/LogonTracer](https://github.com/JPCERTCC/LogonTracer)
7. [marcurdy/dfir-toolset](https://github.com/marcurdy/dfir-toolset)
8. [giMini/NOAH](https://github.com/giMini/NOAH)
9. [hlldz/Invoke-Phant0m](https://github.com/hlldz/Invoke-Phant0m/blob/master/README.md)
10. [siliconshecky/Powershell-Scripts](https://github.com/siliconshecky/Powershell-Scripts?files=1)
----

### OSQuery
1. [osquery Across the Enterprise – Palantir – Medium](https://medium.com/@palantir/osquery-across-the-enterprise-3c3c9d13ec55)
2. [osquery For Security – Chris Long](https://medium.com/@clong/osquery-for-security-b66fffdf2daf)
3. [osquery for Security — Part 2 – Chris Long](https://medium.com/@clong/osquery-for-security-part-2-2e03de4d3721)
4. [Building a Testbed for go-audit & osquery – Chris Long – Medium](https://medium.com/@clong/building-a-testbed-for-go-audit-osquery-ea4c0271b0c)
5. [What are the current pain points of osquery?](https://blog.trailofbits.com/2017/12/21/osquery-pain-points/)
----

### Hostbased detection tools
1. [codeexpress/respounder](https://github.com/codeexpress/respounder)
2. [shellster/DCSYNCMonitor](https://github.com/shellster/DCSYNCMonitor)
3. [R-Smith/tcpTrigger](https://github.com/R-Smith/tcpTrigger)
4. [Neo23x0/Loki](https://github.com/Neo23x0/Loki)
----

### Simulation
1. [uber-common/metta](https://github.com/uber-common/metta)
2. [TryCatchHCF/DumpsterFire](https://github.com/TryCatchHCF/DumpsterFire)
3. [ubeeri/Invoke-UserSimulator](https://github.com/ubeeri/Invoke-UserSimulator)
4. [alphasoc/flightsim](https://github.com/alphasoc/flightsim)
5. [Neo23x0/APTSimulator](https://github.com/Neo23x0/APTSimulator)
6. [redcanaryco/atomic-red-team](https://github.com/redcanaryco/atomic-red-team)
7. [endgameinc/RTA](https://github.com/endgameinc/RTA)
8. [guardicore/monkey](https://github.com/guardicore/monkey)
9. [mitre/caldera](https://github.com/mitre/caldera)
----

### Development / Test
1. [clong/DetectionLab](https://github.com/clong/DetectionLab)
2. [StefanScherer/packer-windows](https://github.com/StefanScherer/packer-windows)
3. [mhassan2/splunk-n-box](https://github.com/mhassan2/splunk-n-box)
4. [redhuntlabs/RedHunt-OS](https://github.com/redhuntlabs/RedHunt-OS/)
----

### Case management
1. [TheHive-Project/TheHive](https://github.com/TheHive-Project/TheHive)
----

### ThreatHunting
1. [ThreatHuntingProject/ThreatHunting](https://github.com/ThreatHuntingProject/ThreatHunting/tree/master/hunts)
2. [giMini/NOAH](https://github.com/giMini/NOAH)
3. [PowerShellMafia/CimSweep](https://github.com/PowerShellMafia/CimSweep)
4. [threat-hunting-10-adversary-behaviors](https://www.linkedin.com/pulse/threat-hunting-10-adversary-behaviors-hunt-ely-kahn)
----

### Tools
1. [GitHub - FSecureLABS/FLAIR: F-Secure Lightweight Acqusition for Incident Response (FLAIR)](https://github.com/FSecureLABS/FLAIR)
2. [GitHub - FSecureLABS/LinuxCatScale: Incident Response collection and processing scripts with automated reporting scripts](https://github.com/FSecureLABS/LinuxCatScale)
3. [Fast Incident Response](https://github.com/certsocietegenerale/FIR)
4. https://github.com/nsacyber/Mitigating-Web-Shells
5. https://github.com/last-byte/PersistenceSniper
6. https://github.com/gtworek/VolatileDataCollector
----

### Other
1. [Stealthy Peer-to-peer C&C over SMB pipes](https://blog.cobaltstrike.com/2013/12/06/stealthy-peer-to-peer-cc-over-smb-pipes/)
2. [PwnWiki.io](http://pwnwiki.io/#!index.md)
3. [security-apis](https://github.com/deralexxx/security-apis)
4. [0x4D31/awesome-threat-detection](https://github.com/0x4D31/awesome-threat-detection)
5. [Tool Analysis Result Sheet](https://jpcertcc.github.io/ToolAnalysisResultSheet/)
6. [Descent Security; Enterprise](https://decentsecurity.com/enterprise/#/adblocking-for-internet-explorer-deployment/)
7. [IR_EndPointSolutions.xlsx](http://www.hexacorn.com/edr/IR_EndPointSolutions.xlsx)
8. [Windows Defender ATP Hunting Queries](https://github.com/beahunt3r/Windows-Hunting/tree/master/WindowsDefenderATP%20Hunting%20Queries)
9. [marcurdy/dfir-toolset](https://github.com/marcurdy/dfir-toolset)
----

### Languages
1. [Neo23x0/sigma](https://github.com/Neo23x0/sigma)
2. [How to write Sigma rules](https://www.nextron-systems.com/2018/02/10/write-sigma-rules/)
3. [YARA - The pattern matching swiss knife for malware researchers](https://virustotal.github.io/yara/)
----

### Mitre ATT&CK
1. [Windows Technique Matrix - enterprise](https://attack.mitre.org/wiki/Windows_Technique_Matrix)
2. [MHaggis/bookish-happiness](https://github.com/MHaggis/bookish-happiness)
3. [mitre/caldera](https://github.com/mitre/caldera)
4. [veramine/Detections](https://github.com/veramine/Detections/wiki)
5. [ATT&CK™ Navigator](https://mitre.github.io/attack-navigator/enterprise/#)
----

### Purple Team
1. [GitHub - ch33r10/EnterprisePurpleTeaming: Purple Team Resources for Enterprise Purple Teaming: An Exploratory Qualitative Study. Doctor of Science Cybersecurity at Marymount University Dissertation by Xena Olsen.](https://github.com/ch33r10/EnterprisePurpleTeaming)
----
### Methodolgy
1. https://github.com/atc-project/atc-react
