### Browser Extensions
1. [chatziko/location-guard](https://github.com/chatziko/location-guard)
2. [dessant/search-by-image](https://github.com/dessant/search-by-image)
3. [Firefox Multi-Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)
4. [Hunchly](https://hunch.ly)
5. [Instant Data Scraper](https://webrobots.io/instantdata/)
6. [Privacy Badger (EFF)](https://privacybadger.org/)
7. [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
----

### Sock Puppets
1. [Fake Name Generator](https://fakenamegenerator.com)
2. [Generated Photos](https://generated.photos/)
3. [Mailinator](https://mailinator.com)
4. [Random User Generator](http://randomuser.me/)
5. [This Person Does Not Exist](https://Thispersondoesnotexist.com)
6. [10 Minute Mail](https://10minutemail.com)
7. [20minutemail.com](https://20minutemail.com)
----

### Screen Capture Tools
1. [FireShot](https://getfireshot.com/)
2. [Nimbus Capture](https://nimbusweb.me/screenshot.php)
3. [ShareX](https://getsharex.com/)
4. [Shutter](https://shutter-project.org/)
5. [TechSmith Snagit](https://www.techsmith.com/screen-capture.html)
----

### Setting up an OSINT Platform
1. [Device Info](https://deviceinfo.me)
2. [Whoer](https://whoer.net/)
----

### Data Analysis
1. [Alexa](https://www.alexa.com/topsites)
2. [Similar Web](https://www.similarweb.com/top-websites/)
3. [APPlyzer](https://www.applyzer.com/)
4. [World Social Media Platforms](https://www.osintcombine.com/world-social-media-platforms)
----

### Documentation
1. [OSINT Combine Visualizer](http://osintcombine.tools/)
2. [Maltego](https://www.maltego.com/)
3. [Gephi](https://gephi.org/)
----

