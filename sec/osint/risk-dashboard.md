### Frequently Used
1. [Negative Media String Old](https://www.google.com/search?q=%22ENTITYNAME%22+AND+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&oq=%22ENTITYNAME%22+AND+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&aqs=chrome..69i57.113385j0j7&sourceid=chrome&ie=UTF-8)
2. [Negative Media String New](https://www.google.com/search?ei=3qcwXePzLNe3tAaZ24aICQ&q=%E2%80%9CFIRST+NAME+LAST+NAME%E2%80%9D+AND+%7Efraud+OR+%7Elaunder+OR+%7Ecrime+OR+%7Ecriminal+OR+%7Eterror+OR+%7ENegative%2Bmedia+OR+%7Ecivil+OR+%7Ecorruption+OR+%7Eadverse+OR+%7Enews+OR+%7Emoney%2Blaundering+OR+%7Eforgery+OR+%7Eidentity+theft+OR+%7Eembezzlement+OR+%7Ebribery+OR+%7Emarijuana+OR+%7Edrug%2Btrafficking+OR+%7Earrest+OR+%7Ebankruptcy+OR+%7Ebsa+OR+%7Econviction+OR+%7Elawsuit+OR+OFAC+OR+Ponzi+OR+%7Eterrorist+OR+%7Eviolation&oq=%E2%80%9CFIRST+NAME+LAST+NAME%E2%80%9D+AND+%7Efraud+OR+%7Elaunder+OR+%7Ecrime+OR+%7Ecriminal+OR+%7Eterror+OR+%7ENegative%2Bmedia+OR+%7Ecivil+OR+%7Ecorruption+OR+%7Eadverse+OR+%7Enews+OR+%7Emoney%2Blaundering+OR+%7Eforgery+OR+%7Eidentity+theft+OR+%7Eembezzlement+OR+%7Ebribery+OR+%7Emarijuana+OR+%7Edrug%2Btrafficking+OR+%7Earrest+OR+%7Ebankruptcy+OR+%7Ebsa+OR+%7Econviction+OR+%7Elawsuit+OR+OFAC+OR+Ponzi+OR+%7Eterrorist+OR+%7Eviolation&gs_l=psy-ab.3...44049.44049..44501...0.0..0.0.0.......0....2j1..gws-wiz.6R9WvN3argQ&ved=0ahUKEwjj2vnr-r7jAhXXG80KHZmtAZEQ4dUDCAo&uact=5)
3. [Google Translate](https://translate.google.com/)
4. [Country Code List](https://www.nationsonline.org/oneworld/country_code_list.htm)
5. [SWIFT Code Lookup](https://www2.swift.com/bsl/facelets/bicsearch.faces)
6. [Wayback Machine](https://archive.org/web/)
7. [SearchOpener](http://www.searchopener.com/)
8. [IRS TIN/SSN Search](https://la1.www4.irs.gov/eauth/pub/login.jsp?Data=VGFyZ2V0TG9BPUY%253D&TYPE=33554433&REALMOID=06-0004b429-9e8b-1a23-9e85-163b0acf4037&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=UOkC7yx4eMTO24FGxPfBRb5q3Mj3Xh3pyXfBEjYyHJ97nGCXu16wx5MzFHjfZmlG&TARGET=-SM-https%3a%2f%2fla1%2ewww4%2eirs%2egov%2fesrv%2ftinm%2flandingPageInsert%2efaces)
9. [INFO sniper](http://www.infosniper.net/)
10. [Fraud Red Flags](https://www.osc.state.ny.us/localgov/pubs/red_flags_fraud.pdf)
11. [Take Five - To Stop Fraud](https://takefive-stopfraud.org.uk/)
12. [IC3](https://www.ic3.gov/complaint/splash.aspx)
13. [Adult Protective Services Online Reporting](https://www.txabusehotline.org/Login/Default.aspx)
----

### Identity Theft
1. [Identity Theft  | Office of the Attorney GeneralAsset 1](https://www.texasattorneygeneral.gov/consumer-protection/identity-theft)
2. [Credit Freeze FAQs | Consumer Information](https://www.consumer.ftc.gov/articles/0497-credit-freeze-faqs#what)
3. [Identity Theft — FBISubmit SearchHomeFacebook IconEmail IconTwitter IconYoutube IconFlickr IconLinkedIn IconInstagram IconSubmit SearchSubmit Search](https://www.fbi.gov/investigate/white-collar-crime/identity-theft)
4. [Identity Theft | CRIMINAL-FRAUD | Department of Justice](https://www.justice.gov/criminal-fraud/identity-theft/identity-theft-and-identity-fraud)
5. [Identity Theft Recovery Steps](https://www.identitytheft.gov/Info-Lost-or-Stolen)
6. [Internet Crime Complaint Center (IC3)](https://www.ic3.gov/default.aspx)
----

### Elder Abuse and Exploitation
1. [Texas Adult Protective Services](https://www.txabusehotline.org/Login/Default.aspx)
2. [Senior Scams | Office of the Attorney GeneralAsset 1](https://www.texasattorneygeneral.gov/consumer-protection/seniors-and-elderly/senior-scams)
3. [Shine a spotlight on elder financial abuse | 2017-07-26 | CUNA News](https://news.cuna.org/articles/112669-shine-a-spotlight-on-elder-financial-abuse?utm_source=real_magnet&utm_medium=email&utm_campaign=Your%2520Friday%2520Pocast%253A%2520Jeff%2520Rendel%2520and%2520the%2520future%252Dfoc)
----

### Arms Trade
1. [Small Arms Survey Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
2. [Camopedia](http://camopedia.org/index.php?title=Main_Page)
3. [SIPRI Trade Register](http://armstrade.sipri.org/armstrade/page/trade_register.php)
4. [Arms Embargoes](https://www.sipri.org/databases/embargoes)
5. [Mapping Arms Data (MAD)](http://nisatapps.prio.org/armsglobe/index.php)
6. [HotGunz Stolen Firearms](https://www.hotgunz.com/search.php)
7. [Military Ranks Wiki](https://en.wikipedia.org/wiki/List_of_comparative_military_ranks#United_States)
----

### Drug Trade
1. [International Narcotics Control Strategy Report](https://www.state.gov/j/inl/rls/nrcrpt/2017/)
2. [DEA Drug Slang Terms](https://ndews.umd.edu/sites/ndews.umd.edu/files/dea-drug-slang-terms-and-code-words-july2018.pdf)
3. [NIH Commonly Abused Drugs](https://www.drugabuse.gov/drugs-abuse/commonly-abused-drugs-charts#pcp)
4. [UN International Drug Prices](https://data.unodc.org/)
5. [InSight Crime](https://www.insightcrime.org/)
6. [US Gangs List](https://en.wikipedia.org/wiki/List_of_gangs_in_the_United_States)
7. [Illicit Drugs World Factbook](https://www.cia.gov/library/publications/the-world-factbook/fields/2086.html#rs)
8. [DEA Wanted Fugitives](https://www.dea.gov/fugitives/all)
9. [DEA Drug Images](https://www.dea.gov/media-gallery/drug-Images)
----

### Human Trafficking
1. [Trafficking in Persons Report](https://www.state.gov/j/tip/rls/tiprpt/index.htm)
2. [Polaris Project](https://polarisproject.org/sites/default/files/A%20Roadmap%20for%20Systems%20and%20Industries%20to%20Prevent%20and%20Disrupt%20Human%20Trafficking%20-%20Financial%20Industry.pdf)
3. [Mixed Migration Centre](http://www.mixedmigration.org/)
4. [Migration Policy Data](https://www.migrationpolicy.org/programs/migration-data-hub)
5. [Financial Flows from Human Trafficking](https://www.fatf-gafi.org/media/fatf/content/images/Human-Trafficking-2018.pdf)
6. [Global Modern Slavery Directory](http://www.globalmodernslavery.org/)
7. [Human Trafficking Flow Map](http://dataviz.du.edu/projects/htc/flow/)
----

### Terrorism
1. [UMD Start](https://www.start.umd.edu/gtd/)
2. [Islamism Map](https://islamism-map.com/#/)
3. [GWU Project on Extremism](https://extremism.gwu.edu/)
4. [Mapping Militant Organizations](http://web.stanford.edu/group/mappingmilitants/cgi-bin/)
5. [Crime Terror Nexus](https://crimeterrornexus.com/)
6. [SPLC Hate Map](https://www.splcenter.org/hate-map)
----

### Reverse Image & Facial Recognition Searc
1. [Google Reverse Image Search for Mobile](https://ctrlq.org/google/images/)
2. [Imagewiki](http://image.wiki/#/)
3. [picwiser.com](http://picwiser.com/)
4. [Instant Logo Reverse Image Search](http://instantlogosearch.com/)
5. [PicQuery Reverse Image Search](https://www.picquery.com/)
6. [RIS Reverse Image Search](http://www.reverse-image-search.org/)
7. [RootAbout Reverse Image Search](http://rootabout.com/)
8. [SmallSEOTools Reverse Image Search](https://smallseotools.com/reverse-image-search)
9. [SocialCatfish Reverse Image Search](https://socialcatfish.com/reverse-image-search)
10. [TinEye Reverse Image Search](https://www.tineye.com/)
11. [BetaFaceAPI Image Recognition](https://www.betafaceapi.com/demo.html)
12. [EagleEye Image Recognition](https://github.com/ThoughtfulDev/EagleEye)
13. [FaceDetection Image Recognition](http://www.facedetection.com/)
14. [FaceRec Image Recognition](http://www.face-rec.org/)
15. [FaceSaerch Image Recognition](http://www.facesaerch.com/)
16. [FindFace Image Recognition](https://findface.ru/login)
17. [How Old Image Recognition](https://www.how-old.net/#)
18. [Pictriev Image Recognition](http://Pictriev.com)
19. [PimEyes Image Recognition](https://pimeyes.com/en/)
20. [Wolfram Image ID Image Recognition](https://www.imageidentify.com/)
----

### Companies
1. [OpenCorporates](https://opencorporates.com/)
2. [List-Org (Russian Companies)](https://www.list-org.com/)
3. [Companies House](https://beta.companieshouse.gov.uk/)
4. [EU National Registries](https://e-justice.europa.eu/content_business_registers_in_member_states-106-de-en.do?member=1)
5. [ICIJ Offshore Leaks Database String](https://www.google.com/search?q=%22XX%22+AND+site%3Ahttps%3A%2F%2Foffshoreleaks.icij.org&oq=%22XX%22+AND+site%3Ahttps%3A%2F%2Foffshoreleaks.icij.org&aqs=chrome..69i57.13022j0j7&sourceid=chrome&ie=UTF-8)
6. [Offshore Jurisdictions](https://ioserv.com/en/jurisdictions/jurisdlist/)
7. [Global Incorporation Guide](https://www.lowtax.net/g/jurisdictions/)
8. [Legal Entity Types by Country](https://en.wikipedia.org/wiki/List_of_legal_entity_types_by_country)
9. [FinCEN MSB Registrant Search](https://www.fincen.gov/msb-registrant-search)
10. [SEC Company Search](https://www.sec.gov/edgar/searchedgar/companysearch.html)
11. [NAICS Code Search](https://www.census.gov/cgi-bin/sssd/naics/naicsrch)
12. [Fortune Global 500](http://fortune.com/fortune500/)
13. [Ripoff Report](https://www.ripoffreport.com/)
----

### NGO Links
1. [C4ADS](https://c4ads.org)
2. [Centre for Financial Crime & Security Studies](https://rusi.org/expertise/research/centre-financial-crime-and-security-studies)
3. [FDD Center on Sanctions and Illicit Finance](http://www.defenddemocracy.org/csif)
4. [ICIJ](https://www.icij.org/)
5. [Project Follow](http://www.projectfollow.org)
6. [ICLG](https://iclg.com/practice-areas/anti-money-laundering-laws-and-regulations)
----

### Datasets
1. [Google Dataset Search](https://toolbox.google.com/datasetsearch)
2. [World Bank Open Data](https://data.worldbank.org)
3. [State Court Records](https://docs.google.com/document/d/1DHHv7GS6mycat97RTzlZDkokPZcm3QpoJgQ0W_QqaiY/edit#)
4. [The Law Pages](https://www.thelawpages.com/criminal-offence/#)
5. [Financial Crime Stats](https://utica.libguides.com/c.php?g=203170&p=3771160)
6. [GSA Contractor List](https://www.gsaelibrary.gsa.gov/ElibMain/contractorList.do?contractorListFor=A)
7. [OCCRP Data](https://data.occrp.org)
8. [OCCRP Investigative Dashboard](https://investigativedashboard.org/databases/global/)
----

### Cryptocurrency
1. [Coinmonks: Blockchain Tracing](https://medium.com/coinmonks/tracing-an-offshore-bank-and-a-dark-web-service-using-the-blockchain-an-osint-investigation-a1000251c3ec)
2. [Top 100 Bitcoin Wallets](https://bitinfocharts.com/top-100-richest-bitcoin-addresses.html)
3. [Blockchain Explorer](https://www.blockchain.com/explorer)
4. [WalletExplorer](https://www.walletexplorer.com/)
5. [Bitcoin Who's Who](https://bitcoinwhoswho.com/)
----

### Banking/Finance
1. [SWIFT Code Lookup](https://www2.swift.com/bsl/facelets/bicsearch.faces)
2. [SWIFT Message Standards](https://www2.swift.com/knowledgecentre/publications/usgf_20180720/3.0?topic=mt202cov-5-field-52a.htm)
3. [Country Codes List](https://www.nationsonline.org/oneworld/country_code_list.htm)
4. [ABA Routing Number Search](https://routingnumber.aba.com/Search1.aspx)
5. [FFIEC Records](https://cdr.ffiec.gov/public/ManageFacsimiles.aspx)
6. [Know Your Country](https://www.knowyourcountry.com/copy-of-country-reports)
7. [Bank Lists](https://en.wikipedia.org/wiki/Lists_of_banks)
8. [FDIC BankFind](https://research.fdic.gov/bankfind/)
9. [Binlist](https://binlist.net/)
10. [Currency Converter (by Date)](https://www.oanda.com/currency/converter/)
11. [Currencies List](https://en.wikipedia.org/wiki/List_of_circulating_currencies)
12. [Financial Secrecy Index](https://www.financialsecrecyindex.com/introduction/fsi-2018-results)
13. [U.S. Currency Verification and Security Features](https://www.uscurrency.gov/denominations/50)
----

### Trade
1. [Trade Finance Guide](https://2016.export.gov/tradefinanceguide/index.asp)
2. [Export Documents](https://2016.export.gov/logistics/eg_main_018121.asp)
3. [Commodities Prices](https://www.bloomberg.com/markets/commodities)
4. [Remittance Map](http://www.pewglobal.org/interactives/remittance-map/)
5. [Container Tracking](https://www.track-trace.com/container)
6. [HS Code Lookup](https://www.freightos.com/freight-resources/harmonized-system-code-finder-hs-code-lookup/)
7. [Seaport Codes](https://www.worldnetlogistics.com/seaport-codes/)
8. [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
9. [Logistics Glossary](https://www.google.com/search?rlz=1C1GCEU_enUS821US821&ei=z4IuXN3pMYKMjwT6y5aYCg&q=inurl%3ASEARCHTERM+AND+site%3Ahttps%3A%2F%2Fwww.logisticsglossary.com%2F&oq=inurl%3ASEARCHTERM+AND+site%3Ahttps%3A%2F%2Fwww.logisticsglossary.com%2F&gs_l=psy-ab.12...1771.10363..12313...3.0..0.98.1419.16......0....1..gws-wiz.9Oz_2c3oAv0)
10. [Observatory of Economic Complexity](https://atlas.media.mit.edu/en/rankings/country/eci/)
11. [Country Commercial Guides](https://www.export.gov/ccg)
----

### Email Breached Search Engines
1. [@ Have I been pwned? HIBP***](https://haveibeenpwned.com/)
2. [Ashley Madison hacked email checker](https://ashley.cynic.al/)
3. [@ Dehashed pwnd Database](https://dehashed.com/)
4. [Find My Snap -  Check if your SnapChat account was leaked on the 2013  hack.](http://findmysnap.com/)
5. [Ghostproject leaked Data and Password](https://ghostproject.fr/)
6. [Intelligence X Pastebin Search](https://intelx.io/)
7. [IntelX Email/PAste Search](https://intelx.io/)
8. [LeakedSource](https://leakedsource.ru/)
9. [PSBDMP Pastebin Search](https://psbdmp.ws/)
10. [Weleakinfo.com/ Raw Data for Payment)](https://weleakinfo.com/)
11. [World’s Biggest Data Breaches & Hacks — Information is Beautiful](http://www.informationisbeautiful.net/visualizations/worlds-biggest-data-breaches-hacks/)
12. [Steve Turner | IdentityForce® Data Breach Roundup](https://www.identityforce.com/author/steve-turner)
----

### Social Media
1. [Twitter Advanced Search](https://twitter.com/search-advanced?lang=en)
2. [TweetDeck](https://tweetdeck.twitter.com/)
3. [Node XL](https://www.smrfoundation.org/nodexl/)
4. [Spoonbill](http://spoonbill.io/)
5. [Reverse Image Search](https://socialcatfish.com/reverse-image-search)
----

### IP Address Geo Location
1. [INFO Sniper](http://www.infosniper.net/)
2. [IP Locator](https://www.ipfingerprints.com/)
3. [Instant IP Address Lookup](https://whatismyipaddress.com/ip-lookup)
4. [Cisco Talos Intelligence Group Reputation Lookup](https://www.talosintelligence.com/reputation_center/lookup?search=cpe-98-24-49-136.carolina.res.rr.com#top)
5. [CuteStat.com](https://www.cutestat.com/)
6. [Whois Lookup & IP](https://www.whois.net/)
----

### Search Strings
1. [Constant Contact Dork](https://www.google.com/search?ei=tpuuW83xJaqUjwTB_6zoAw&q=%22XX%22+AND+site%3A*.constantcontact.com&oq=%22XX%22+AND+site%3A*.constantcontact.com&gs_l=psy-ab.3...3118.13595..16539...1.0..0.105.1567.18j1......0....1..gws-wiz.wZK5_0lx1Oo)
2. [Negative Media String Old](https://www.google.com/search?q=%22ENTITYNAME%22+AND+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&oq=%22ENTITYNAME%22+AND+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&aqs=chrome..69i57.113385j0j7&sourceid=chrome&ie=UTF-8)
3. [FFIEC String](https://www.google.com/search?ei=slGRW-j0GtHcswWm_rTIDA&q=%22XX%22+AND+site%3Ahttps%3A%2F%2Fwww.ffiec.gov%2Fbsa_aml_infobase%2F&oq=%22XX%22+AND+site%3Ahttps%3A%2F%2Fwww.ffiec.gov%2Fbsa_aml_infobase%2F&gs_l=psy-ab.3...7341.9765..11245...2.0..0.94.543.6......0....1..gws-wiz.......0i71.V5iqxPN9JFc)
4. [Negative Media String](https://www.google.com/search?source=hp&ei=0JLQXJ-AAsLs_QaR4pewBg&q=%E2%80%9CFIRST+NAME+LAST+NAME%E2%80%9D+fraud+OR+launder+OR+crime+OR+criminal+OR+terror+OR+Negative+OR+civil+OR+corruption+OR+bribe+OR+adverse+OR+money+laundering+OR+forgery+OR+identity+theft+OR+embezzlement+OR+bribery+OR+marijuana+OR+drug+trafficking+OR+arrest+OR+bankruptcy+OR+bsa+OR+conviction+OR+lawsuit+OR+ofac+OR+ponzi+OR+terrorist+OR+violation+OR+honorary+consul+OR+consul+OR+panama+papers&btnK=Google+Search&oq=%E2%80%9CFIRST+NAME+LAST+NAME%E2%80%9D+fraud+OR+launder+OR+crime+OR+criminal+OR+terror+OR+Negative+OR+civil+OR+corruption+OR+bribe+OR+adverse+OR+money+laundering+OR+forgery+OR+identity+theft+OR+embezzlement+OR+bribery+OR+marijuana+OR+drug+trafficking+OR+arrest+OR+bankruptcy+OR+bsa+OR+conviction+OR+lawsuit+OR+ofac+OR+ponzi+OR+terrorist+OR+violation+OR+honorary+consul+OR+consul+OR+panama+papers&gs_l=psy-ab.3...1585.1585..2084...0.0..0.97.97.1......0....2j1..gws-wiz.....0.vAIlUtvjNso)
5. [Investopedia Search String](https://www.google.com/search?ei=SNqfW4D8NYWzjwSZ7aToBw&q=%22XX%22+AND+site%3Ahttps%3A%2F%2Fwww.investopedia.com&oq=%22XX%22+AND+site%3Ahttps%3A%2F%2Fwww.investopedia.com&gs_l=psy-ab.3...3002.11305..12640...0.0..0.84.629.8......0....1j2..gws-wiz.......0i71.DQD05M7P3vA)
----

### Internet Search Tools
1. [Wayback Machine](https://archive.org/web/)
2. [DuckDuckGo (Private Search Engine)](https://duckduckgo.com/)
3. [PDF My URL](http://pdfmyurl.com)
4. [Alexa Top 500](https://www.alexa.com/topsites)
5. [Google Trends](https://trends.google.com/trends/?geo=US)
6. [Google Search Operators](https://ahrefs.com/blog/google-advanced-search-operators/)
7. [Mastering Google Operators](https://moz.com/blog/mastering-google-search-operators-in-67-steps)
8. [Fake Name Generator](https://www.fakenamegenerator.com/)
9. [Is This Website Safe](https://safeweb.norton.com)
10. [Numberway](https://www.numberway.com)
11. [De-Hashed](https://www.dehashed.com/)
12. [Have I been pwned?](https://haveibeenpwned.com/)
13. [Email Address Verification](https://tools.verifyemailaddress.io/)
14. [PenTest Tools](https://pentest-tools.com/home)
15. [Site Map Crawler](https://www.xml-sitemaps.com/)
16. [ccTLD List](https://icannwiki.org/Country_code_top-level_domain#Current_ccTLDs)
17. [SearchOpener](http://www.searchopener.com/)
18. [Donotlink.it](https://donotlink.it/)
19. [Yandex Image Search](https://yandex.com/images/)
----

### Translation
1. [Translate](https://translate.google.com/?hl=en&tab=wT)
2. [Google Translate](https://translate.google.com/)
3. [Ethnologue](https://www.ethnologue.com/)
----

### Charities
1. [IRS Charities Search](https://apps.irs.gov/app/eos/)
2. [Charity Navigator](https://www.charitynavigator.org/)
3. [GuideStar](www.guidestar.org/search)
----

### Red Flags
1. [Fraud Red Flags](https://www.osc.state.ny.us/localgov/pubs/red_flags_fraud.pdf)
2. [AML Red Flags](https://docs.google.com/spreadsheets/d/1geN2h3KezCF_EPu9EMMDmXCk2dfEFdzQKXEdYD7sG4Y/edit#gid=0)
----

### Flight/Maritime
1. [FAA Registry](https://registry.faa.gov/aircraftinquiry/)
2. [Passenger Airlines Wiki](https://en.wikipedia.org/wiki/List_of_passenger_airlines#List_of_passenger_airlines)
3. [Flight Tracker](https://global.adsbexchange.com/VirtualRadar/desktop.html)
4. [AirNav RadarBox](https://www.radarbox24.com/flight/AA2097#)
5. [Marine Traffic](https://www.marinetraffic.com/en/ais/home/centerx:4.1/centery:40.2/zoom:7)
6. [BoatInfoWorld.com - Search, View &amp; Download Vessel Information](https://www.boatinfoworld.com/)
7. [IOC Live Piracy Map](https://www.icc-ccs.org/piracy-reporting-centre/live-piracy-map)
8. [Flags of Convenience](https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/List_of_flags_of_convenience.html)
----

### OSINT
1. [Bellingcat Digital Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
2. [Reuser OSINT Tools](http://rr.reuser.biz/#company%20informationInternational)
3. [Bellingcat Thread](https://twitter.com/trbrtc/status/895734898647945220?lang=en)
4. [Awesome OSINT](https://github.com/Ph055a/awesome_osint)
5. [The OSINT Podcast](https://www.buzzsprout.com/209598)
6. [Bellingcat Resources](https://www.bellingcat.com/category/resources/)
7. [Creps Social Media Intel](https://start.me/p/m6MbeM/social-media-intelligence-dashboard)
8. [Terrorism & Radicalization](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
9. [Social Media Intelligence](https://start.me/p/m6MbeM/social-media-intelligence-dashboard)
10. [Bruno Mortier > Sources](https://start.me/p/3g0aKK/sources)
11. [Technisette > Tools](https://start.me/p/wMdQMQ/tools)
12. [TDiDdy > OSINT](https://start.me/p/b56xX8/osint)
13. [Jk Priv > AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
14. [TI](https://start.me/p/rxRbpo/ti)
15. [KYC - AML - FINTECH - start.me](https://start.me/p/Om7x8L/kyc-aml-fintech?embed=1)
----

### Visualization
1. [Draw.io](https://www.draw.io/)
2. [Datawrapper](https://www.datawrapper.de/)
3. [Kibana](https://www.elastic.co/products/kibana)
4. [Data Viz Catalogue](https://datavizcatalogue.com/)
5. [RAWGraphs](http://app.rawgraphs.io/)
6. [Visual Investigative Scenarios](https://vis.occrp.org/)
----

### Fraud Blog/Articles
1. [Take Five - To Stop Fraud](https://takefive-stopfraud.org.uk/)
2. [Types of scams | Scamwatch](https://www.scamwatch.gov.au/types-of-scams)
3. [Krebs on Security](https://krebsonsecurity.com/)
4. [Frank on Fraud – Fraud Blog by Frank Mckenna](https://frankonfraud.com/)
5. [Identity Theft  | USAGov](https://www.usa.gov/identity-theft)
6. [Forter Fraud Blog](https://blog.forter.com/)
7. [Fraud Files](http://www.sequenceinc.com/fraudfiles/2019/02/the-most-common-money-scams-and-schemes-in-2018/)
8. [Walmart Fraud Alerts](https://corporate.walmart.com/privacy-security/fraud-alerts/)
9. [Senior Scams | Office of the Attorney GeneralAsset 1](https://www.texasattorneygeneral.gov/consumer-protection/seniors-and-elderly/senior-scams)
10. [Cardnotpresent.com | CNP Fraud & Payments | News, Education and Events Decoding Digital Payments & Fraud](https://www.cardnotpresent.com/)
11. [Identity Theft  | Office of the Attorney GeneralAsset 1](https://www.texasattorneygeneral.gov/consumer-protection/identity-theft)
12. [Prevention tips - Fraud.org](https://www.fraud.org/prevent_fraud)
13. [The Fraud Blog](http://www.bankinfosecurity.com/blogs/fraud-blog-b-18)
14. [The Master List of Fraud Scams, Scamsters Do – Frank on Fraud](http://frankonfraud.com/fraud-scams/the-master-list-of-fraud-scams-scamsters-do/)
----

### Financial Crimes News
1. [ACAMS Free Webinars](https://www.acams.org/aml-resources/training/)
2. [ACFE Fraud Talk (Podcast)](http://www.acfe.com/podcast.aspx#paging:number=25|paging:currentPage=0|date-filter:path=default|sort:path~type~order=.date~datetime~desc)
3. [AML Conversations (Podcast)](https://soundcloud.com/user-45942002/tracks)
4. [Bribe, Swindle or Steal (Podcast)](http://www.traceinternational.org/bribe_swindle_or_steal)
5. [FT Banking Weekly (Podcast)](https://www.ft.com/banking-weekly-podcast)
6. [SanctionLaw Blog](https://sanctionlaw.com/blog/)
7. [WSJ Risk and Compliance Journal](https://www.wsj.com/news/risk-compliance-journal)
8. [Taxcast](http://www.tackletaxhavens.com/taxcast/)
----

### Press
1. [BBC Media Profiles](https://www.google.com/search?ei=KhTKW5qcD6zHjwTC_aUQ&q=intitle%3A%22COUNTRYNAME%22+AND+intitle%3A%22profile+-+Media+-+BBC+News%22&oq=intitle%3A%22COUNTRYNAME%22+AND+intitle%3A%22profile+-+Media+-+BBC+News%22&gs_l=psy-ab.3...6293.8181..14020...0.0..0.86.896.11......0....1..gws-wiz.oHO_CeGB42g)
2. [Google Alerts](https://www.google.com/alerts)
3. [Press Reader (Global Newspapers)](https://www.pressreader.com/catalog)
4. [vTuner Radio](http://vtuner.com/setupapp/guide/asp/BrowseStations/StartPage.asp?sBrowseType=Location)
----

### Tech
1. [TechCrunch](http://www.techcrunch.com)
2. [CNET](http://www.cnet.com)
3. [Hacker News](http://thehackernews.com)
4. [Mashable](http://mashable.com)
----

### Popular
1. [Youtube](http://www.youtube.com)
2. [Facebook](http://www.facebook.com/)
3. [WhatsApp Web](http://web.whatsapp.com/)
4. [Twitter](http://www.twitter.com/)
5. [Google](http://google.com)
6. [GMail](http://mail.google.com/mail/?tab=wm)
7. [Google Drive](http://drive.google.com)
8. [LinkedIn](http://www.linkedin.com)
9. [Yahoo](http://yahoo.com)
10. [Netflix](http://netflix.com)
11. [Instagram](http://instagram.com)
12. [Reddit](http://www.reddit.com)
----

