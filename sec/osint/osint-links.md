### Basic Tools
1. [Radaris - People search - USA](https://radaris.com/)
2. [Spytox - Number search](https://www.spytox.com/)
3. [TweetBeaver - Deeper Twitter Tool](https://tweetbeaver.com/)
4. [urlscan - Comprehensive Scan](https://urlscan.io/)
5. [Internet Archive: Wayback Machine](https://web.archive.org/)
6. [OpenCorporates - International Biz Search](https://opencorporates.com/)
7. [ProPublica Non-prof USA](https://projects.propublica.org/nonprofits)
8. [Instant Google Street View](https://www.instantstreetview.com/)
9. [WebMii - Search for image by name](http://webmii.com/)
----

### PEOPLE SEARCH BY COUNTRY
1. [192 - People Search - UK](https://www.192.com/)
2. [Person Lookup - People Search - Australia](https://personlookup.com.au/)
3. [Canada411.ca - People Search - Canada](http://www.canada411.ca/search)
----

### NAME - USA
1. [Thatsthem - People Search](https://thatsthem.com/)
2. [True People Search - People search](https://www.truepeoplesearch.com/#)
3. [Radaris - People search](https://radaris.com/)
4. [Cubib - Public data Search](https://cubib.com/)
5. [nuwber](https://nuwber.com/)
6. [FamilyTree](https://www.familytreenow.com/)
----

### NAME - UK
1. [192](https://www.192.com/)
2. [UK Phone Book](https://www.ukphonebook.com/)
3. [Births, deaths, marriages](https://www.freebmd.org.uk/)
4. [Electoral Rolls - Genes Reunited](https://www.genesreunited.co.uk/articles/world-records/full-list-of-united-kingdom-records/census-land-and-surveys/electoral-rolls)
5. [Birth Records](https://search.findmypast.co.uk/search-world-records/england-and-wales-births-1837-2006)
6. [Search for People](https://www.192.com/)
----

### DOCUMENTS
1. [Pastebin dump collection](https://psbdmp.ws//)
2. [Public buckets by grayhatwarfare](https://buckets.grayhatwarfare.com/)
3. [Napalm FTP Indexer](https://www.searchftps.net/)
4. [PDF Search engine](http://www.findpdfdoc.com/)
5. [PDF search engine](http://www.freefullpdf.com/index.html)
6. [PDF Search Engine](http://www.sopdf.com/)
7. [Pastebin Alerts](https://andrewmohawk.com/pasteLert/)
8. [Metadata Viewer](https://www.extractmetadata.com/)
----

### TELEPHONE - USA
1. [Sync.me - Global number lookup](https://sync.me/)
2. [OpenCNAM - Global telephone lookup](https://www.opencnam.com/)
3. [TextMagic - Global Tool for phones](https://www.textmagic.com/free-tools/)
4. [CarrierLookup](https://www.carrierlookup.com/)
5. [Numberguru - Reverse number lookup](https://www.numberguru.com/)
6. [Free Reverse Phone Number Lookup](https://www.spydialer.com/default.aspx)
----

### INTERNATIONAL TELEPHONE
1. [Sync.me - Global number lookup](https://sync.me/)
2. [OpenCNAM - Global telephone lookup](https://www.opencnam.com/)
3. [TextMagic - Global tool for phones](https://www.textmagic.com/free-tools/)
4. [International](https://www.infobel.com/)
----

### IP ADDRESS
1. [DomainBigData](https://domainbigdata.com/)
2. [ViewDNS.info](https://viewdns.info/)
3. [Torrent downloads from](https://iknowwhatyoudownload.com/en/peer/)
4. [WiGLE: Wireless Network Mapping](https://wigle.net/)
5. [Shodan](https://www.shodan.io/)
6. [IP Locator](https://www.ipfingerprints.com/)
----

### DOMAIN
1. [Carbon Dating The Web](http://carbondate.cs.odu.edu/)
2. [Intelligence X](https://intelx.io/)
3. [DomainBigData](https://domainbigdata.com/)
4. [Securitytrails](https://securitytrails.com)
5. [ROBTEX - Domain data](https://www.robtex.com/)
6. [NerdyData - Code data](https://nerdydata.com/)
7. [searchcode](https://searchcode.com/)
8. [Threat Crowd](https://www.threatcrowd.org)
9. [SharedCount](https://www.sharedcount.com/)
10. [Visual Site Mapper](http://www.visualsitemapper.com/)
11. [visualping - Email monitor](https://visualping.io/)
12. [Cache View](http://cachedview.com/)
13. [Free link analysis tool - Link research](https://openlinkprofiler.org/)
14. [Find subdomains](https://findsubdomains.com/)
15. [dnsdumpster.com/](https://dnsdumpster.com/)
16. [visualping - Monitor site](https://visualping.io/)
17. [Time Travel](https://timetravel.mementoweb.org/)
----

### SITE SECURITY CHECK
1. [Phishing Test](https://www.immuniweb.com/radar/)
2. [Threat Crowd](https://www.threatcrowd.org)
3. [webcookies Scan](https://webcookies.org/)
4. [Censys - IP safety check](https://censys.io/)
5. [BinaryEdge Portal](https://app.binaryedge.io/)
6. [VirusTotal](https://www.virustotal.com/#/home/upload)
7. [Threat Intelligence Platform](https://threatintelligenceplatform.com/)
8. [Fakespot](https://www.fakespot.com/)
9. [urlvoid](https://www.urlvoid.com/)
10. [MXtoolbox](https://mxtoolbox.com/)
----

### DATABREACH
1. [Pastebin dump collection](https://psbdmp.ws//)
2. [Offshore Leaks Database](https://offshoreleaks.icij.org/)
3. [Search.weleakinfo.com](https://search.weleakinfo.com/search)
4. [dehashed](https://dehashed.com/)
5. [Vigilante.pw](https://www.vigilante.pw/)
6. [Nuclear Links](https://nuclearleaks.com/)
7. [Hashing and Anonymity toolkit](https://md5hashing.net/)
8. [CrackStation](https://crackstation.net/)
9. [SpyCloud](https://spycloud.com/)
10. [Leak -Database Search Engine](https://leak-lookup.com/)
11. [Reverse Hash Calculator](https://isc.sans.edu/tools/reversehash.html)
12. [Online Password Hash Crack](https://www.onlinehashcrack.com/)
13. [World’s Biggest Data Breaches & Hacks](https://www.informationisbeautiful.net/visualizations/worlds-biggest-data-breaches-hacks/)
14. [DataBreaches.net](https://www.databreaches.net/)
15. [raidforums](https://raidforums.com/)
16. [Public Database Directory](https://databases.today/)
----

### BUSINESS - GLOBAL
1. [data.occrp.org - International court/Biz](https://data.occrp.org/)
2. [Overseas registries - Links to all Nations](https://www.gov.uk/government/publications/overseas-registries/overseas-registries)
3. [Offshore Leaks Database](https://offshoreleaks.icij.org/)
4. [bad Business Dork](https://www.google.com/search?ei=cPS6W-WpF4SM0wKvuKOACg&q=%22ENTITYNAME%22+AND+marijuana+OR+~embezzelment+OR+corruption+OR+bribery+OR+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&oq=%22ENTITYNAME%22+AND+marijuana+OR+~embezzelment+OR+corruption+OR+bribery+OR+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&gs_l=psy-ab.3...52335.125837..144250...0.0..0.0.0.......32....1j2..gws-wiz.96VJym89q-Y)
5. [Ripoff Report](https://www.ripoffreport.com/)
6. [Indeed](http://indeed.com)
7. [Global Incorporation guide](https://www.lowtax.net/g/jurisdictions/)
8. [European e-Justice Portal](https://e-justice.europa.eu/content_business_registers_in_member_states-106-de-en.do?member=1)
9. [Orbis directory](https://orbisdirectory.bvdinfo.com/version-20181213/OrbisDirectory/Companies)
10. [Overseas registries](https://www.gov.uk/government/publications/overseas-registries/overseas-registries)
11. [BBB - United States, Canada, and Mexico](https://www.bbb.org/)
12. [AnnualReports.com](http://www.annualreports.com/)
----

### BUSINESS USA
1. [Radaris - Biz Directory](https://radaris.com/#findBussH)
2. [OpenCorporates - Biz Search](https://opencorporates.com/)
3. [Corporation Wiki](https://www.corporationwiki.com/)
4. [SEC.gov Search](https://www.sec.gov/edgar/searchedgar/webusers.htm)
5. [Bad business google dork](https://www.google.com/search?ei=cPS6W-WpF4SM0wKvuKOACg&q=%22ENTITYNAME%22+AND+marijuana+OR+~embezzelment+OR+corruption+OR+bribery+OR+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&oq=%22ENTITYNAME%22+AND+marijuana+OR+~embezzelment+OR+corruption+OR+bribery+OR+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&gs_l=psy-ab.3...52335.125837..144250...0.0..0.0.0.......32....1j2..gws-wiz.96VJym89q-Y)
6. [Indeed](http://indeed.com)
7. [AIHIT](https://www.aihitdata.com/)
8. [SEC.gov](https://www.sec.gov/edgar/searchedgar/companysearch.html)
----

### NON-PROFIT
1. [GuideStar](https://www.guidestar.org/)
2. [Charity Navigator](https://www.charitynavigator.org/)
3. [Tax Exempt Organization Search| Internal Revenue Service](https://apps.irs.gov/app/eos/)
4. [Search the charity register UK](https://www.gov.uk/find-charity-information)
----

### POLITICAL
1. [Resource Center | OpenSecrets](https://www.opensecrets.org/resources/)
2. [Library of Congress](https://www.loc.gov/)
3. [FOIA.gov](https://www.foia.gov/search.html)
4. [voter records](https://voterrecords.com/)
5. [politicalmoneyline](http://www.politicalmoneyline.com/)
6. [FEC Donor Search](https://www.melissa.com/v2/lookups/fec/index)
7. [wikileaks.org](https://wikileaks.org/)
8. [donations registered by the day](https://www.fec.gov/data/filings/?data_type=processed)
9. [followthemoney](www.followthemoney.org)
10. [Lobbylinx](https://www.lobbylinx.com/search.php)
----

### OPEN DATA
1. [Global data](https://knoema.com/atlas)
2. [Open data USA gov](https://catalog.data.gov/dataset)
3. [Open data EU](https://data.europa.eu/)
4. [Open data Africa](https://africaopendata.org/)
5. [Open data Australia](https://data.gov.au/search)
6. [NYC Open data](https://opendata.cityofnewyork.us/)
----

### COURT RECORDS USA
1. [CourtListener](https://www.courtlistener.com/)
2. [Free Criminal Records Search](https://www.blackbookonline.info/criminalsearch.aspx)
3. [Free State Criminal Background Check](http://www.felonspy.com/)
4. [PacerMonitor Federal Court Case Tools](https://www.pacermonitor.com/)
5. [theinmatelocator](http://theinmatelocator.com/)
6. [Free Criminal Records Search](https://www.blackbookonline.info/criminalsearch.aspx)
----

### LEAKS & DECLASSIFIED
1. [WikiLeaks - The Podesta Emails](https://wikileaks.org/podesta-emails/)
2. [Search WikiLeaks](https://search.wikileaks.org/)
3. [Offshore Leaks - Panama Papers](https://offshoreleaks.icij.org/)
4. [The Black Vault - Declassified docs](https://www.theblackvault.com/documentarchive/)
5. [FBI Records](https://vault.fbi.gov/)
6. [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
----

### MAPPING
1. [Yandex.Maps — detailed map of the world](https://yandex.com/maps)
2. [Wayback Imagery](https://livingatlas.arcgis.com/wayback/)
3. [Zillow](https://www.zillow.com/)
4. [Wikimapia](http://wikimapia.org/)
5. [Planet Labs $$$](https://www.planet.com)
----

### GEOLOCATION
1. [DigitalGlobe](https://discover.digitalglobe.com/)
2. [Dual Maps](http://data.mashedworld.com/dualmaps/map.htm)
3. [Wayback Imagery](https://livingatlas.arcgis.com/wayback/)
4. [Tools.wmflabs.org](https://tools.wmflabs.org/geohack/)
----

### VIDEOS/IMAGES
1. [Download Facebook Videos](http://www.downfacebook.com/)
2. [Extract Meta Data Youtube](https://citizenevidence.amnestyusa.org/)
3. [Community Video](https://archive.org/details/opensource_movies)
4. [Scopedown - download Periscope Videos](https://downloadperiscopevideos.com/index.php)
5. [deturl.com - Downlod any video on the web](http://deturl.com/)
6. [online photo forensics tools](https://29a.ch/photo-forensics/#error-level-analysis)
7. [Download videos from many video sites](https://www.tubeoffline.com/)
8. [YouTube Comment Scraper](http://ytcomments.klostermann.ca/)
9. [Pic2Map Photo Location Viewer](https://www.pic2map.com/)
----

### IMAGE RESEARCH
1. [The Wolfram Language Image Identification Project](image identify)
2. [Extract camera information](https://camerasummary.com/)
3. [Image Metadata Viewer](http://exif.regex.info/exif.cgi)
----

### REVERSE IMAGE SEARCH
1. [Google Images](https://www.google.com/imghp?sbi=1)
2. [Bing Image Feed](https://www.bing.com/images/discover?form=HDRSC2)
3. [Yandex.Images](https://www.yandex.com/images/search)
4. [TinEye](https://www.tineye.com/)
5. [Karma Decay](http://karmadecay.com/)
6. [The Wolfram Language Image Identification](https://www.imageidentify.com/)
----

### EMAIL TOOLS
1. [rocketreach.co - Email Search](https://rocketreach.co/)
2. [Hunter - Find sources and email domain link](https://hunter.io/)
3. [FindEmails - Email finder & Verify](https://www.findemails.com/)
4. [peepmail - Find email for name by domain](http://samy.pl/peepmail/elift.cgi)
5. [Email Formt - email string type by domain](https://www.email-format.com)
6. [Facebook - Check email Recover](https://www.facebook.com/login/identify?ctx=recover)
7. [DNS Tools - Email valid check](https://dnslytics.com/email-test)
8. [data.occrp.org - Email search data](https://data.occrp.org)
9. [Pastebin dump collection](https://psbdmp.ws/)
10. [Reverse Whois - Domain own search](https://www.whoxy.com/reverse-whois/)
11. [Lullar Com](http://com.lullar.com/)
12. [email address validator](https://verifalia.com/validate-email)
----

### SOCMINT - GENERAL
1. [Profilr Social - Search 6+ SM by name](https://www.profilr.social/)
2. [Social Searcher - Search by name & #](https://www.social-searcher.com/)
3. [Hashatit - # Search](https://www.hashatit.com/)
----

### SOCMINT -  FACEBOOK
1. [Facebook - Email Recover](https://www.facebook.com/login/identify?ctx=recover)
2. [stalkscan - Comprehensive FB Search](https://stalkscan.com/)
3. [Find my Facebook ID](https://findmyfbid.com/)
4. [Facebook intersect search](https://graph.tips/beta/)
5. [Search is Back - Extra Search tool](https://searchisback.com/)
6. [Who posted what - About & date](https://www.whopostedwhat.com/)
7. [peoplefindThor](https://peoplefindthor.dk/)
----

### SOCMINT - TWITTER
1. [Twoogel Search Engine](http://twoogel.com/)
2. [Socialbearing - Twitter geolocation & more](https://socialbearing.com/)
3. [geosocialfootprint](http://geosocialfootprint.com/)
4. [Tweetmap](https://www.omnisci.com/demos/tweetmap/)
5. [DoesFollow?](https://doesfollow.com/)
6. [quarter tweets - Geo Tweets](http://qtrtweets.com/twitter/)
7. [tinfoleak - Twitter leaks](https://tinfoleak.com/)
8. [Twitter event dork](https://twitter.com/search?q=paris%20since:2012-01-01%20until:2012-01-02)
9. [Twitter geolocation dork](https://twitter.com/search?q=geocode%3A38.952451%2C-90.195011%2C1km&src=typd)
10. [View all tweets on one page](https://www.allmytweets.net/connect/)
11. [Fake followers](https://fakers.statuspeople.com/)
12. [Twitter Search — BackTweets](http://backtweets.com/)
13. [Sleeping Time](http://sleepingtime.org/)
14. [Hashtag Tracking for Twitter, Instagram and Facebook](https://keyhole.co/)
15. [tweepsect](https://tweepsect.com/)
16. [sTwity • Online Twitter Web Viewer](https://stwity.com/)
17. [Spoonbill - Twitter account changes](https://spoonbill.io/)
18. [Foller - Twitter user data](https://foller.me/)
----

### SOCMINT - INSTAGRAM
1. [Websta - Search Instagram](https://web.stagram.com/)
2. [GramFly - Search for person](https://gramfly.com/)
3. [downloadgram - Video & Photo download](https://downloadgram.com/)
4. [Searchmy.bio](https://www.searchmy.bio/)
5. [Anon remote instagram view](https://www.inst4gram.com/)
6. [Hashtag Search](https://tagboard.com/TEST/search)
7. [Instagram Online Viewer](http://picdeer.com/)
8. [Instagram Online Viewer](https://tofo.me/)
----

### SOCMINT - LINKEDIN
1. [LinkedIN - email checker](https://www.linkedin.com/sales/gmail/profile/viewByEmail/example@example.com)
2. [recruitin.net - Find Linkedin](https://recruitin.net/)
----

### USERNAME
1. [namecheckr - Check sites with a username](https://www.namecheckr.com/)
2. [KnowEm Username Search](https://knowem.com/)
3. [namechk.com/](https://namechk.com/)
4. [checkusernames.com](https://checkusernames.com/)
5. [Username Search](https://usersearch.org/)
6. [Instant Username Search](https://instantusername.com/#/)
----

### BLOGS
1. [Blogspot Blog Search](https://www.searchblogspot.com/)
2. [WordPress Search](https://en.search.wordpress.com/)
3. [Blog Search Engine](http://www.blogsearchengine.org/)
----

### TELEGRAM GROUPS
1. [Telegram Search](https://search.buzz.im/)
2. [Telegram Channels List](https://tlgrm.eu/channels)
3. [telegram search all](https://lyzem.com/)
4. [Telegram Post search](https://tgstat.ru/en/search)
5. [telegago - Search](https://cse.google.com/cse?q=+&cx=006368593537057042503:efxu7xprihg#gsc.tab=0&gsc.q=%20&gsc.page=1)
----

### SNAPCHAT
1. [Snap Map](https://map.snapchat.com/)
2. [Snapdex - Snap search](https://www.snapdex.com/)
3. [somechater - napchat username search](https://somesnapcode.com/)
4. [Snapdex: Snapchat Names Index](https://www.snapdex.com/)
----

### GITHUB TOOLKIT
1. [h8mail - databreach emails](https://github.com/khast3x/h8mail)
2. [PhoneInfoga - OSINT for Phone numbers](https://github.com/sundowndev/PhoneInfoga)
3. [Creepy - geolocation](https://www.geocreepy.com/)
4. [Inspy - LinkedIn tool](https://github.com/leapsecurity/InSpy)
5. [theHarvester - Email, domain scrape](https://github.com/laramies/theharvester)
6. [face_recognition](https://github.com/ageitgey/face_recognition)
----

### MISC
1. [SnoopSnoo - Reddit user search](https://snoopsnoo.com/)
2. [Search All Junk - Classifieds search](http://www.searchalljunk.com/)
3. [Boardreader - Forum searchbar](http://boardreader.com/)
4. [NewspaperArchive](https://newspaperarchive.com/advancedsearch/)
5. [US & World newspapers](https://search.findmypast.com/search/us-and-world-newspapers)
6. [discool - Disqus search](https://dis.cool/)
7. [Vizit | Visual Reddit](https://redditstuff.github.io/sna/vizit/)
8. [Find a grave](https://www.findagrave.com/)
9. [SearchOpener](http://www.searchopener.com/)
10. [Melissa look ups](https://www.melissa.com/v2/lookups)
11. [RedditSearch](https://reddysearchgo.herokuapp.com/)
12. [Reddit Investigator](http://www.redditinvestigator.com/)
13. [Reddit Insight](https://www.redditinsight.com/#trackuser)
14. [Google Groups Search](https://groups.google.com/forum/?fromgroups#!overview)
15. [resume "name"](https://www.google.com/search?newwindow=1&tbm=isch&q=resume+%22name%22)
16. [Ashley Madison hacked email checker](https://ashley.cynic.al/)
17. [How Many of Me](http://howmanyofme.com/search/)
18. [Ventusky](https://www.ventusky.com)
19. [Meet Up](https://www.meetup.com/)
20. [Every Disputed Territory in the World [Interactive Map]](http://metrocosm.com/disputed-territories-map.html)
21. [Newslookup.com](http://www.newslookup.com/)
22. [Acled Data - Current crisis global map](https://www.acleddata.com/dashboard/)
23. [International Phone Numbers](http://world.192.com/)
24. [Fresh conflict news on the map](https://liveuamap.com/)
25. [EU Sanctions Map](https://sanctionsmap.eu/#/main)
26. [SSN Validator](https://www.ssnvalidator.com/index.aspx)
27. [Free VIN Code Search Service](https://carsowners.net/)
28. [VINCheck®](https://www.nicb.org/vincheck)
29. [Check that VIN!](https://checkthatvin.com/ctv#/home)
30. [License Plate Search](http://www.reversegenie.com/plate.php)
31. [Million Short - look deep](https://millionshort.com/)
32. [EMPORIS - Global building detail](https://www.emporis.com/)
33. [Events all over the world](https://www.wherevent.com/)
34. [Phorio - Global building data](https://en.phorio.com/)
35. [dronestagr.am/](www.dronestagr.am/)
36. [SunCalc sun position and sunlight](http://suncalc.net)
37. [Malware-Traffic-Analysis.net](http://www.malware-traffic-analysis.net)
38. [Jotti's malware scan](https://virusscan.jotti.org/)
39. [Free BIN/IIN Lookup Web Service](https://binlist.net/)
40. [MAC Address Lookup](https://www.techzoom.net/tools/CheckMacAddress)
41. [Fake WhatsApp Chat Generator](http://www.fakewhats.com/generator)
42. [Assessor and Property Tax Records Search Directory](https://publicrecords.onlinesearches.com/Assessor-and-Property-Tax-Records.htm)
43. [Write.as](https://write.as/)
----

### ACCOUNT CREATION
1. [This Person Does Not Exist](https://www.thispersondoesnotexist.com/)
2. [mailinator](https://www.mailinator.com/)
3. [Get a whole new identity](https://www.fakenamegenerator.com/)
4. [✉ Guerrilla Mail](https://www.guerrillamail.com/)
5. [10 Minute Mail](https://10minutemail.com/10MinuteMail/index.html)
6. [MailDrop](https://maildrop.cc/)
7. [Nada - Passwordless  email](https://getnada.com/)
8. [receivesmsonline](https://www.receivesmsonline.net/)
9. [Receive SMS Online](https://receive-sms.com/)
----

### PRIVACY
1. [IP/DNS Detect](https://ipleak.net/)
2. [Down For Everyone Or Just Me](https://downforeveryoneorjustme.com/)
3. [What's my user agent?](https://www.whatsmyua.info/)
4. [Device Info](https://www.deviceinfo.me/)
5. [Whoer](https://whoer.net/)
6. [IP Locator](https://www.ipfingerprints.com/)
----

### LIVE DASHBOARD
1. [Snap Map](https://map.snapchat.com/)
2. [Periscope Watch on Web | Periscope Search](https://www.perisearch.xyz/)
3. [Realtime Tweets Map](http://tweeplers.com/map/)
4. [www.echosec.net $$](https://www.echosec.net/)
5. [TerraServer - Live sat Mapping $](https://www.terraserver.com/)
6. [EarthCam](https://www.earthcam.com/)
7. [Global Incident Map](https://www.globalincidentmap.com/)
8. [Latest Earthquakes](https://earthquake.usgs.gov/earthquakes/map/#%7B%22autoUpdate%22%3A%5B%22autoUpdate%22%5D%2C%22basemap%22%3A%22grayscale%22%2C%22feed%22%3A%221day_m25%22%2C%22listFormat%22%3A%22default%22%2C%22mapposition%22%3A%5B%5B-23.725011735951796%2C-145.8984375%5D%2C%5B46.255846818480315%2C-5.2734375%5D%5D%2C%22overlays%22%3A%5B%22plates%22%5D%2C%22restrictListToMap%22%3A%5B%22restrictListToMap%22%5D%2C%22search%22%3Anull%2C%22sort%22%3A%22newest%22%2C%22timezone%22%3A%22utc%22%2C%22viewModes%22%3A%5B%22list%22%2C%22map%22%5D%2C%22event%22%3Anull%7D)
9. [Insecam - Security cams global](https://www.insecam.org/)
10. [Open Source Intelligence](https://www.osintcombine.com/social-geo-lens)
----

### TRANSLATION
1. [DeepL Translator](https://www.deepl.com/translator)
2. [translate.google.com](https://translate.google.com/)
3. [Bing Microsoft Translator](https://www.bing.com/translator)
4. [2lingual Google Search](https://2lingual.com/)
----

### SHIPPING
1. [Iris - live global customs crime](https://iris.wcoomd.org/?locale=en)
2. [MarineTraffic: Live Global Ship Tracking Intelligence](https://www.marinetraffic.com/en/ais/home/centerx:-12.0/centery:25.0/zoom:4)
3. [NOSI – Naval Open Source Intelligence .](https://nosi.org/)
4. [Live Piracy Map](https://www.icc-ccs.org/piracy-reporting-centre/live-piracy-map)
5. [BoatInfoWorld.com](https://www.boatinfoworld.com/)
6. [Ship Tracker](https://shiptracker.shodan.io/)
7. [Live Cruise Ship Tracker](https://www.livecruiseshiptracker.com/)
----

### FLIGHTS
1. [Virtual Radar](https://global.adsbexchange.com/VirtualRadar/desktop.html)
2. [Live Flight Tracker ✈ FlightAware](https://uk.flightaware.com/live/)
3. [Flightradar24](https://www.flightradar24.com)
4. [jetphotos.com](https://www.jetphotos.com/)
----

### HUMAN TRAFFICKING
1. [Human Trafficking Search](https://humantraffickingsearch.org/)
2. [Stop Child Abuse – Trace an Object](https://www.europol.europa.eu/stopchildabuse)
3. [Human Trafficking Flow Map](http://dataviz.du.edu/projects/htc/flow/)
4. [Global Modern Slavery Directory](http://www.globalmodernslavery.org/)
5. [Migration Data Hub](https://www.migrationpolicy.org/programs/migration-data-hub)
----

### TERRORISM
1. [Global Terrorism Database](https://www.start.umd.edu/gtd/)
2. [Mapping Militant Organizations](http://web.stanford.edu/group/mappingmilitants/cgi-bin/)
3. [Crime Terror Nexus](https://crimeterrornexus.com/)
4. [Islamism World Map](https://islamism-map.com/#/)
5. [Far-right news tracker](https://farright.liveuamap.com/)
6. [Far-left news tracker](https://farleft.liveuamap.com/)
7. [RiskMap – The free security and risk mapping platform](https://www.riskmap.com/)
8. [Jihadist Identifiers :: Jihad Intel](https://jihadintel.meforum.org/identifiers/)
----

### OSINT TOOL SITES
1. [Week in OSINT – Medium](https://medium.com/week-in-osint)
2. [Bellingcat](https://www.bellingcat.com/category/resources/how-tos/)
3. [Facebook graph searches](http://researchclinic.net/graph.html)
4. [OSINT Combine](https://www.osintcombine.com/facebook-intersect-search-tool)
5. [Page2Images](http://www.page2images.com/URL-Live-Website-Screenshot-Generator)
6. [Google Exploit Database Archive](https://www.exploit-db.com/google-hacking-database)
7. [Google Search Operators](http://www.googleguide.com/advanced_operators_reference.html)
8. [This Person Does Not Exist](https://www.thispersondoesnotexist.com/)
9. [TechTrick](http://www.techtrick.in/)
10. [Security in a Box](https://securityinabox.org/en/)
11. [Warwire](https://www.warwire.net/)
12. [BelleBytes OSINT Guide](https://stormctf.ninja/ctf/blog/stormctf/bellebytes-osint-guide)
13. [The Stoic Approach To OSINT](https://www.secjuice.com/osint-engagement-101/)
14. [How to Find Anyone's Email Address & Phone Number in Under 30 Seconds - SourceCon](https://www.sourcecon.com/how-to-find-anyones-email-address-phone-number-in-under-30-seconds/)
15. [Exposing the Invisible](https://exposingtheinvisible.org/)
16. [Privacy Tools - Encryption Against Global Mass Surveillance](https://www.privacytools.io/classic/)
17. [Deep Web](https://www.deepweb-sites.com/)
18. [Mastering Google Search](https://moz.com/blog/mastering-google-search-operators-in-67-steps)
19. [Dating apps and hook-up sites for investigators](https://start.me/p/VRxaj5/dating-apps-and-hook-up-sites-for-investigators)
20. [DarknetLive](https://darknetlive.com/)
21. [OSINT Training by Michael Bazzell](https://inteltechniques.com/)
----

### DARKNET RESOURCES
1. [DarknetLive](https://darknetlive.com/)
2. [Daily Dark Web Reports](https://www.hunch.ly/darkweb-osint/)
----

