### AML and Financial Crimes News
1. [ACFE Fraud Talk (Podcast)](http://www.acfe.com/podcast.aspx#paging:number=25|paging:currentPage=0|date-filter:path=default|sort:path~type~order=.date~datetime~desc)
2. [AML Conversations (Podcast)](https://soundcloud.com/user-45942002/tracks)
3. [Bribe, Swindle or Steal (Podcast)](http://www.traceinternational.org/bribe_swindle_or_steal)
4. [FT Banking Weekly (Podcast)](https://www.ft.com/banking-weekly-podcast)
5. [Grahambarrow.com](https://grahambarrow.com/)
6. [WSJ Risk and Compliance Journal](https://www.wsj.com/news/risk-compliance-journal)
7. [The Dark Money Files by Ray Blake & Graham Barrow](https://www.buzzsprout.com/242645)
8. [The Dark Money Files – Blog.](https://thedarkmoneyfiles.com/)
9. [www.anti-geldwaesche.de - anti-gw.de](https://www.anti-gw.de/)
10. [Travis Birch AML Page](https://start.me/p/rxeRqr/aml-toolbox?embed=1)
----

### Currency Converter | Country list
1. [Oanda](https://www.oanda.com/lang/de/currency/converter/)
2. [Historical Currency Converter](https://www.oanda.com/solutions-for-business/historical-rates/main.html)
3. [XE Währungsrechner](http://www.xe.com/de/currencyconverter/)
4. [Währungsumrechnung  in der Vergangenheit](http://fxtop.com/de/vergangene-rechner.php)
5. [Rechner](https://web2.0rechner.de/)
6. [List of countries and currencies](https://fxtop.com/en/countries-currencies.php)
----

### IBAN | BIC | Credit Cards
1. [BINDB - Issuer ID Numbers](https://www.bindb.com/bin-database.html)
2. [DC - Luhn (Mod10) Algorithm](https://www.dcode.fr/luhn-algorithm)
3. [DFG - Luhn (Mod10) Validator](http://www.datafakegenerator.com/valitar.php)
4. [FNG - Credit Card Validator](https://www.fakenamegenerator.com/credit-card-validator.php)
5. [Free BIN/IIN Lookup Web Service](https://binlist.net/)
6. [FreeFormatter CC Number](https://www.freeformatter.com/credit-card-number-generator-validator.html#howToValidate)
7. [Inquiry IID (IID stands for Institution Identification, formerly: BC number) – SIX](https://www.six-interbank-clearing.com/en/home/bank-master-data/inquiry-bc-number.html)
8. [Mindestlohnrechner](https://www.shiftjuggler.com/de/mindestlohnrechner/)
9. [PC - Luhn (Mod10) Algorithm](https://planetcalc.com/2464/)
10. [SM - Issuer ID Numbers](https://stevemorse.org/ssn/List_of_Bank_Identification_Numbers.html)
11. [ValidateCreditCardNumber](https://www.validcreditcardnumber.com/)
12. [Währungscode ISO_4217](https://en.wikipedia.org/wiki/ISO_4217)
----

### International Bank Account Number IBAN Check / BIC Code
1. [Ibancalculator Iban validieren](http://www.ibancalculator.com/iban_validieren.html)
2. [BIC Search](https://www2.swift.com/bsl/)
3. [IBAN Validator](https://www.iban.com/)
4. [IBAN Check](http://www.tbg5-finance.org/?ibancheck.shtml)
5. [XE IBAN Calculator](http://www.xe.com/ibancalculator/)
6. [International Bank Account Number - Wikipedia](https://en.wikipedia.org/wiki/International_Bank_Account_Number)
----

### Legal Identifier LEI
1. [Legal Entity Identifier | Search LEI code](https://lei.info/)
2. [LEI ROC List](https://www.leiroc.org/publications/gls/lou_20131003_2.pdf)
3. [Legal Entity Identifier LEI leiroc.org](https://www.leiroc.org/)
4. [LEI # Search  – GLEIF](https://www.gleif.org/de)
5. [[BaFin - Legal Entity Identifier (LEI)]](https://www.bafin.de/DE/Aufsicht/VersichererPensionsfonds/Berichtspflichten/LEI/leicode_node.html#:~:text=Der%20LEI%20%2DCode%20ist%20eine,Zuordnung%20zu%20einem%20konkreten%20Unternehmen)
----

### Validate Tax ID
1. [Umsatzsteuer-ID prüfen](https://ust-id-pruefen.de/)
2. [Validate TAX/VAT ID - Validierung UST Nr.](http://ec.europa.eu/taxation_customs/vies/vieshome.do?selectedLanguage=de)
3. [⚖ Aufbau Steuernummer Finanzamt Deutschland](https://www.liste-finanzamt.de/)
4. [Find VAT #](https://tva-recherche.lu/)
----

### Check Passport #
1. [Belgium](https://www.checkdoc.be)
----

### Line of business Code
1. [NAICS Association](https://www.naics.com/)
2. [NACE Codes](https://www.euregiolocator.eu/downloads/NACE-Codes.pdf)
----

### Beneficial Owner Registry (UBO)
1. [BELGIUM Finanzen.belgium.be](https://finanzen.belgium.be/de/E-services/registre-ubo)
2. [GERMANY Transparenzregister](https://www.transparenzregister.de)
3. [DENMARK Datacvr.virk.dk](https://datacvr.virk.dk)
4. [Beneficial Ownership Status in Ireland - RBO](https://rbo.gov.ie/how-to.html)
----

### Financial Intelligence Unit FIU Jahresbericht
1. [Bis 2016 beim BKA](https://www.bka.de/DE/AktuelleInformationen/StatistikenLagebilder/Lagebilder/FinancialIntelligenceUnitDeutschland/financialintelligenceunitdeutschland_node.html)
2. [2017 Generalzolldirektion](http://fiu_jahresbericht_2017.pdf)
3. [zoll.de/ FIU](https://www.zoll.de/DE/Fachthemen/FIU/fiu_node.html)
----

### Data Breach GDPR
1. [Guidelines on Personal data breach notification](https://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=612052)
2. [Data-Breach-Meldungen nach Art. 33 DSGVO in Hamburg](https://datenschutz-hamburg.de/assets/pdf/2018.11.15_Data%20Breach_Vermerk_extern.pdf)
----

### Brand IP | Supply Chain | Anti-Counterfeit | Illi
1. [Coalition Against Piracy (CAP)](http://www.casbaa.com/about-us/cap/)
2. [EU IPO Enforcement Database EDB](https://euipo.europa.eu/ohimportal/de/web/observatory/home)
3. [Eurosint Forum](https://www.eurosint.eu/)
4. [GIRP European Healthcare Distribution Association](http://girp.eu/)
5. [GS1](https://www.gs1-germany.de/gs1-standards/)
6. [INTCEN (PDF)](http://rieas.gr/images/editorial/NomikosEUintelligence15.pdf)
7. [UNICRI United Nations Interregional Crime and Justice Research](http://www.unicri.it/)
8. [USCIB Anti-Ilicit Trade](https://www.uscib.org/uscib-sets-up-new-anti-illicit-trade-committee/)
9. [INTA International Trademark Association](http://www.inta.org)
----

### COUNTRY and BRIBERY RISK
1. [Basel AML Index | Basel Institute on Governance](https://www.baselgovernance.org/basel-aml-index)
2. [Corporate Tax Haven Index](https://www.corporatetaxhavenindex.org/)
3. [Countries - Financial Action Task Force (FATF)](http://www.fatf-gafi.org/countries/#high-risk)
4. [Country Public AML Ranking | Basel Institute on Governance](https://www.baselgovernance.org/basel-aml-index/public-ranking)
5. [Europa.eu High Risk Countries](https://europa.eu/rapid/press-release_IP-19-781_en.htm)
6. [Europa.eu Q&A Risk Countries](https://europa.eu/rapid/press-release_MEMO-19-782_en.htm)
7. [TI Corruption Perceptions Index 2018](https://www.transparency.org/cpi2018)
8. [TRACE International](https://www.traceinternational.org/trace-matrix)
9. [Tax Justice Network Financial Secrecy Index](https://www.financialsecrecyindex.com/en/)
10. [Country Profiles Archive | GAN Integrity](https://www.ganintegrity.com/portal/country-profiles/)
----

### Crime
1. [International Criminal Court](https://www.icc-cpi.int/)
2. [Papillonfoundation.org](https://www.papillonfoundation.org/)
----

### Company Information Sources  GERMANY
1. [Bundesanzeiger (GER)](https://www.bundesanzeiger.de/ebanzwww/wexsservlet)
2. [Unternehmensregister (GER)](https://www.unternehmensregister.de/ureg/)
3. [Handelsregister Bekanntmachungen (GER)](http://www.handelsregisterbekanntmachungen.de/?aktion=suche)
4. [Handelsregister Deutschland (GER)](https://www.handelsregister.de/rp_web/welcome-members.do;jsessionid=BE0A60EEEE0BD1140C00402C22957467-n1.tc032n01)
5. [Handelsregister HILFE (GER)](https://www.handelsregister.de/rp_web/help.do?Thema=suchergebnismaske)
6. [OpenCorporates : German Registry information](https://opencorporates.com/companies/de)
7. [Northdata (GER)](https://www.northdata.de/)
8. [Transparenzregister (Wirtschaftlich Berechtigte UBE) (GER)](https://www.transparenzregister.de)
9. [Melderegister Berlin (GER)](https://olmera.verwalt-berlin.de/std/Login/start.do)
10. [BaFin](http://www.bafin.de)
11. [German Registry Information as Download](https://offeneregister.de/)
12. [Supervisory Boards - Board-directory.de](https://www.board-directory.de/)
13. [AcquiredBy](https://acquiredby.co/)
14. [Klassifikation der Wirtschaftszweige, Ausgabe 2008 (WZ 2008)  -  Statistisches Bundesamt](https://www.destatis.de/DE/Methoden/Klassifikationen/Gueter-Wirtschaftsklassifikationen/klassifikation-wz-2008.html)
15. [OffeneRegister.de](https://offeneregister.de/#download)
16. [Aktuelle Finanznachrichten und Börseninfos direkt von der Quelle - dgap.de](https://www.dgap.de/dgap/News/?newsType=CMS)
----

### Company Information Sources GLOBAL
1. [e-justice.europa](https://e-justice.europa.eu)
2. [E-justice.Find a Company](https://e-justice.europa.eu/content_find_a_company-489-de.do)
3. [Europäisches Justizportal Register](https://e-justice.europa.eu/content_business_registers-104-de.do)
4. [Aleph.occrp.org](https://aleph.occrp.org/)
5. [Business Registers in Member States EUR](https://e-justice.europa.eu/content_business_registers_in_member_states-106-en.do)
6. [Corporation Wiki (USA) D&B Data](https://www.corporationwiki.com/)
7. [Crunchbase.com](https://www.crunchbase.com/#/home/index)
8. [DocumentCloud Search](https://www.documentcloud.org/public/search/)
9. [fortune.com/fortune500/](http://fortune.com/fortune500/)
10. [GCRD International](https://www.gcrd.info/)
11. [Global Incorporation Guide](https://www.lowtax.net/g/jurisdictions/)
12. [Global Open Data Index](http://index.okfn.org/)
13. [Gov UK Overseas registries](https://www.gov.uk/government/publications/overseas-registries/overseas-registries)
14. [International Monetary Fund (IMF): Staff Country Reports](https://globaledge.msu.edu/global-resources/resource/1250)
15. [Investigative Dashboard](https://investigativedashboard.org/databases/)
16. [list-org (Russian)](https://www.list-org.com/)
17. [MarketVisual Search](http://www.marketvisual.com/)
18. [Nonprofit Explorer NGO Search](https://projects.propublica.org/nonprofits/)
19. [Offshore Jurisdictions](https://ioserv.com/en/jurisdictions/jurisdlist/)
20. [OpenCorporates](https://opencorporates.com/)
21. [Openownership.org](https://www.openownership.org/)
22. [ORG-ID Guide Identifiers](http://org-id.guide/)
23. [SEC.gov Company Search](https://www.sec.gov/edgar/searchedgar/companysearch.html)
24. [siteleaks.com quick check](http://www.siteleaks.com)
25. [The Company Database AIHITDATA](https://www.aihitdata.com/)
26. [Theofficialboard.com](https://www.theofficialboard.com/)
27. [US DOJ Country Links](https://www.justice.gov/jmd/ls/countries-d-k#I)
28. [Wikipedia List of company registers](https://en.wikipedia.org/wiki/List_of_company_registers)
29. [Opengazettes.com](http://opengazettes.com/)
30. [List of Countries of the world in alphabetical order (A to Z) - Worldometer](https://www.worldometers.info/geography/alphabetical-list-of-countries/)
31. [knowyourcountry](https://www.knowyourcountry.com/)
32. [[Public disclosure of identity of directors, officers and shareholders around the world - DLA Piper Guide to Going Global](https://www.dlapiperintelligence.com/goingglobal/corporate/index.html?t=26-public-disclosure-of-identity)]([Public disclosure of identity of directors, officers and shareholders around the world - DLA Piper Guide to Going Global](https://www.dlapiperintelligence.com/goingglobal/corporate/index.html?t=26-public-disclosure-of-identity))
----

### AML related Leak Databases
1. [ICIJ Offshore Leaks / Panama Papers Database](https://offshoreleaks.icij.org/)
2. [Organized Crime Reporting and Corruption Project (OCCR) DATA](https://data.occrp.org/)
3. [Wikispooks](https://wikispooks.com/wiki/Main_Page)
4. [Organized Crime Reporting and Corruption Project (OCCR) Website](https://www.occrp.org/en)
5. [Panama Offshore Leaks Database](https://panamadb.org/)
----

### Finance
1. [Definition of Public Interest Entities (PIEs)](https://www.accountancyeurope.eu/publications/definition-public-interest-entities-europe/)
2. [Deutsche-boerse](http://deutsche-boerse.com)
3. [ESMA Register European regulatory framework for investment firms and credit institutions](https://registers.esma.europa.eu/publication/)
4. [Google finance](https://www.google.com/finance)
5. [ING-DiBa](https://www.ing-diba.de/)
6. [Investopedia](http://www.investopedia.com/)
7. [List of European stock exchanges](https://en.wikipedia.org/wiki/List_of_European_stock_exchanges)
8. [SIX Swiss Exchange](https://www.six-group.com/exchanges/index.html)
9. [Steuern in Europa](http://europa.eu/youreurope/business/vat-customs/buy-sell/index_de.htm)
10. [World Finance – Business, Investment, Economy, Stock Market, Updates](http://finance.mapsofworld.com/)
11. [Yahoo Finance](https://finance.yahoo.com)
12. [Investopedia](https://www.investopedia.com/)
----

### FinCEN
1. [Financial Crimes Enforcement Network (FinCEN) US Dept. of Treasury](https://www.fincen.gov/)
2. [FinCen MSB Database](https://www.fincen.gov/msb-registrant-search)
3. [FinCEN Advisory on Shell Companies | FinCEN.gov](https://www.fincen.gov/resources/statutes-regulations/guidance/potential-money-laundering-risks-related-shell-companies)
4. [FinCEN Advisory on Virtual Currency](https://www.fincen.gov/resources/advisories/fincen-advisory-fin-2019-a003)
5. [FinCEN Advisory on Funnel Accounts/TBML](http://fincen.gov/resources/advisories/fincen-advisory-fin-2014-a005)
6. [FinCEN Advisory on Fentanyl Trafficking](https://www.fincen.gov/sites/default/files/advisory/2019-08-21/Fentanyl%20Advisory%20FINAL%20508.pdf)
7. [Fedpaymentsimprovement.org](https://fedpaymentsimprovement.org/strategic-initiatives/payments-security/fraudclassifier-model/)
----

### Securities (USA)
1. [EDGAR | Company Search Page](http://www.sec.gov/edgar/searchedgar/companysearch.html)
2. [NASAA](http://www.nasaa.org/)
3. [NFA Futures](https://www.nfa.futures.org/)
4. [SCAC](http://securities.stanford.edu/)
5. [SEC](https://www.sec.gov/)
6. [SEC EDGAR](https://www.sec.gov/edgar/searchedgar/webusers.htm)
7. [SEC EDGAR Filings Full Text Search Tips](http://groups.secdatabase.com/community/f/38/t/8.aspx)
8. [SEC Enforcement](https://www.sec.gov/litigation.shtml)
9. [SEC Enforcement Actions](https://www.sec.gov/divisions/enforce/enforceactions.shtml)
10. [SEC Filings & Forms](https://www.sec.gov/edgar.shtml)
11. [SEC Info](http://www.secinfo.com/)
12. [SEC Research Guide](https://www.sec.gov/oiea/Article/edgarguide.html)
----

### Anti Money Laundering AML | Anti Corruption | Transparency
1. [@Federal Reserve Fraud Classifier Model](https://fedpaymentsimprovement.org/strategic-initiatives/payments-security/fraudclassifier-model/)
2. [Antibriberyguidance.org](https://www.antibriberyguidance.org/)
3. [BaFin  -  Acts - German GwG Money Laundering Act in English](https://www.bafin.de/SharedDocs/Veroeffentlichungen/EN/Aufsichtsrecht/Gesetz/GwG_en.html)
4. [BaFin  -  Auslegungsentscheidungen - Auslegungs- und Anwendungshinweise zum Geldwäschegesetz (Stand: …](https://www.bafin.de/SharedDocs/Downloads/DE/Auslegungsentscheidung/dl_ae_auas_gw_2018.html)
5. [Basel Committee on Banking Supervision](https://www.bis.org/bcbs/publ/d353.htm)
6. [Basel Governance ICAR](https://www.baselgovernance.org/)
7. [Bundesfinanzministerium  Ers­te Na­tio­na­le Ri­si­ko­ana­ly­se                                                                                     -                 Erste Nationale Risikoanalyse](https://www.bundesfinanzministerium.de/Content/DE/Downloads/Broschueren_Bestellservice/2019-10-19-erste-nationale-risikoanalyse_2018-2019.html)
8. [Centre for Global Development (CGD)](https://www.cgdev.org/)
9. [Compliancedigital.de](https://www.compliancedigital.de/)
10. [Corporate Tax Haven Index (Tax Justice Network)](https://www.corporatetaxhavenindex.org/)
11. [Country AML Reports (Know Your Country)](https://www.knowyourcountry.com/copy-of-country-reports)
12. [CSO INternational Centre for Asset Recovery](http://https://cso.assetrecovery.org/)
13. [DB Nomics World Economic Data](https://db.nomics.world/)
14. [DEA Intel Report on Drug Slang Code Words](https://www.dea.gov/documents/2017/05/01/drug-slang-code-words)
15. [DICO e.V.](http://www.dico-ev.de/)
16. [European Anti-Fraud Office](http://ec.europa.eu/anti-fraud//home_en)
17. [European Bank for Reconstruction and Development (EBRD)](http://ebrd.com/home)
18. [Europol](https://www.europol.europa.eu/)
19. [Federal Register Customer Due Diligence Requirements for Financial Institutions (US)](https://www.federalregister.gov/documents/2016/05/11/2016-10567/customer-due-diligence-requirements-for-financial-institutions)
20. [FFIEC BSA/AML Bank Secrecy Act/ Anti-Money Laundering Examination Manual](https://www.occ.treas.gov/publications/publications-by-type/other-publications-reports/ffiec-bsa-aml-examination-manual.pdf)
21. [Financial Action Task Force (FATF)](http://www.fatf-gafi.org/)
22. [Financial Conduct Authority - UK](http://www.fca.org.uk/)
23. [Financial Conduct Authority Handbook (FCA)](https://www.handbook.fca.org.uk/)
24. [FINRA (US)](https://www.finra.org/)
25. [Geldwäschereiverordnung FINMA (Schweiz)](http://www.forum-sro.ch/d/finma-eb-gwv-finma.pdf)
26. [Global Financial Integrity](https://www.gfintegrity.org/)
27. [Global Witness](http://www.globalwitness.org/)
28. [Group of States against Corruption (GRECO)](https://www.coe.int/en/web/greco)
29. [Guernsey Financial-Services Commission Handbook ) GFSC)](https://www.gfsc.gg/sites/default/files/Handbook-for-Financial-Services-Businesses-on-Countering-Financial-Crime-and-Terrorist-Financing-July-2016_0.pdf)
30. [GwG - nichtamtliches Inhaltsverzeichnis](https://www.gesetze-im-internet.de/gwg_2017/)
31. [GwG Geldwäschegesetz](https://www.buzer.de/GwG_Geldwaschegesetz.htm)
32. [Home.treasury.gov NATIONAL STRATEGY  FOR COMBATING TERRORIST AND OTHER ILLICIT FINANCING](https://home.treasury.gov/system/files/136/National-Strategy-to-Counter-Illicit-Financev2.pdf)
33. [How Britain can help you get away with stealing millions: a five-step guide | World news | The Guardian](https://www.theguardian.com/world/2019/jul/05/how-britain-can-help-you-get-away-with-stealing-millions-a-five-step-guide)
34. [ICC Commercial Crime Services Fraudnet](https://icc-ccs.org/)
35. [IMF Homepage](http://www.imf.org/external/)
36. [IMOLIN International Money Laundering Information Network](http://www.imolin.org/imolin/en/index.html)
37. [Intergrity Sanctions](https://www.ebrd.com/ineligible-entities.html)
38. [INTERPOL](http://www.interpol.int/en)
39. [Isle of Man Financial Supervisory Authority (IoMFSA)](https://www.iomfsa.im/designated-business/amlcft/)
40. [JMLSG Guidance](http://www.jmlsg.org.uk/industry-guidance/article/jmlsg-guidance-current)
41. [Joint Money Laundering Steering Group Home Page](http://www.jmlsg.org.uk/what-is-jmlsg)
42. [Korruptionswahrnehmungsindex Wikiwand](https://www.wikiwand.com/de/Korruptionswahrnehmungsindex)
43. [OECD Tax Transparency](https://www.oecd.org/tax/transparency/)
44. [Openownership - Beneficial Ownership Register](https://www.openownership.org/)
45. [Paul Renner](http://kycmap.com/)
46. [SUERF Policy Notes](https://www.suerf.org/policynotes)
47. [Tax Justice Network: What is a secrecy jurisdiction?](https://www.financialsecrecyindex.com/faq/what-is-a-secrecy-jurisdiction)
48. [The Egmont Group of Financial Intelligence Units (FIUs)](http://www.egmontgroup.org/)
49. [TIAG - Forensic Information Litigation Support Group](https://www.tiagnet.com/)
50. [Transparency International (TI)](https://www.transparency.org/)
51. [Transparency International Deutschland e.V.](https://www.transparency.de/)
52. [Transparency.org CPI Corruption Perception Index](https://www.transparency.org/research/cpi/overview)
53. [United Nations Convention Against Corruption](https://www.unodc.org/unodc/en/treaties/CAC/)
54. [United Nations Office on Drugs & Crime (UNODC)](https://www.unodc.org/)
55. [US DOJ Evaluation of corporate compliance programs](http://US DOJ Evaluation of corporate compliance programs.com)
56. [WEF Partnering Against Corruption Initiative](https://www.weforum.org/communities/partnering-against-corruption-initiative)
57. [Wolfsberg Group](http://www.wolfsberg-principles.com/)
58. [Wolfsberg Group CBDDQ Capacity Building Materials](https://www.wolfsberg-principles.com/cbddq-capacity-building-materials)
59. [Wolfsberg Group updates PEP guidance | AML-CFTFacebookLinkedInRSSTwitter](https://aml-cft.net/wolfsberg-group-updates-pep-guidance/)
60. [xBlog_www.linkedin.com](https://www.linkedin.com/in/marcos-tinedo-42199a14/detail/recent-activity/posts/)
61. [AML NAME SCREENING BEST PRACTICES (THIRD PART)](https://www.linkedin.com/pulse/aml-name-screening-best-practices-third-part-marcos-tinedo)
----

### PDF Beneficial Ownership
1. [EU MLD 5 EUR-Lex - 32018L0843 - EN - EUR-Lex](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32018L0843)
2. [FATF AML UK Mutual Evaluation Report 2018](https://www.fatf-gafi.org/media/fatf/documents/reports/mer4/MER-United-Kingdom-2018.pdf)
3. [FATF Concealment of beneficial ownership information 2018](https://www.fatf-gafi.org/media/fatf/documents/reports/FATF-Egmont-Concealment-beneficial-ownership.pdf)
4. [Tax Justice Network Beneficial Ownership Verification 2019](https://www.taxjustice.net/wp-content/uploads/2019/01/Beneficial-ownership-verification_Tax-Justice-Network_Jan-2019.pdf)
----

### DPA & Reports (PDF)
1. [HSBC Deferred Prosecution Agreement](https://www.justice.gov/archives/opa/documents-and-resources-december-11-2012hsbc-press-conference)
2. [Standard Chartered Bank Amended Deferred Prosecution Agreement](https://www.iranwatch.org/sites/default/files/sxb_amended_dpa_-_filed_0.pdf)
3. [rlp.de | Gutachtliche Äußerung - Flughafen Frankfurt-Hahn GmbH (2017) | Willkommen in Rheinland-Pfalz](https://rechnungshof.rlp.de/de/veroeffentlichungen/gutachten-und-stellungnahmen/gutachtliche-aeusserung-flughafen-frankfurt-hahn-gmbh-2017/)
4. [Report on the Non-Resident Portfolio at Danske Bank’s Estonian branch](https://danskebank.com/-/media/danske-bank-com/file-cloud/2018/9/report-on-the-non-resident-portfolio-at-danske-banks-estonian-branch.pdf?rev=56b16dfddae94480bb8cdcaebeaddc9b&hash=B7D825F2639326A3BBBC7D524C5E341E)
----

### Sanctionslist Watchlist
1. [Mr. Watchlist***](https://mrwatchlist.com/)
2. [Global Sanctionlist Profiles ***](https://sanctions-intelligence.com/global-lists/)
3. [OPEN SANCTIONS](http://pudo.org/material/opennames/)
4. [Bis.doc.gov Denied-Persons List](https://www.bis.doc.gov/index.php/policy-guidance/lists-of-parties-of-concern/denied-persons-list)
5. [Specially Designated Nationals And Blocked Persons List (SDN) Human Readable Lists](https://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/default.aspx)
6. [Common Foreign and Security Policy (CFSP)](https://eeas.europa.eu/topics/common-foreign-security-policy-cfsp_en)
7. [BaFin](https://www.bafin.de)
8. [Embargo- und Sanktionslisten](http://www.zolltarifnummern.de/embargo/overview.php)
9. [bigger312/Sanctions-Lists](https://github.com/bigger312/Sanctions-Lists)
10. [info4c Compliance Info broker](https://www.info4c.net/de/)
11. [bigger312/Sanctions-Lists](https://github.com/bigger312/Sanctions-Lists)
12. [- INTERPOL](https://www.interpol.int/notice/search/wanted)
13. [Denied Persons](https://2016.export.gov/ecr/eg_main_023148.asp)
14. [Export Gov Consolidated Screening List](https://sanctionssearch.ofac.treas.gov/)
15. [Open Sanctions Open-source data for due diligence](https://www.opensanctions.org/)
16. [data.world US OFAC](https://data.world/datasets/opensanctions)
17. [Alphabetical List of Iranian Entities | Iran Watch](https://www.iranwatch.org/iranian-entities)
18. [Deutschen Bundesbank  - Beschränkungen des Kapital- und Zahlungsverkehrs](http://www.bundesbank.de)
19. [Bundesamt für Wirtschaft und Ausfuhrkontrolle (BAFA)](https://www.bafa.de/DE/Aussenwirtschaft/aussenwirtschaft_node.html)
20. [Bundesministerium für Wirtschaft und Technologie (BMWi), Referat V B 2](http://www.bwmi.bund.de)
----

### Politically Exposed Personen PEP Sources
1. [Rulers](http://rulers.org/index.html)
2. [EveryPolitician](http://everypolitician.org/)
3. [CIDOB Biografías Líderes Políticos](https://www.cidob.org/en/biografias_lideres_politicos_only_in_spanish)
4. [US Diplomat List](https://2009-2017.state.gov/s/cpr/rls/dpl/243893.htm)
5. [Sistema Financiero Banco Central del Uruguay](https://www.bcu.gub.uy/Servicios-Financieros-SSF/Paginas/Personas-Politicamente-Expuestas.aspx)
6. [LittleSis watchdog network](https://littlesis.org/)
7. [OpenSecrets](https://www.opensecrets.org/)
----

### Export Control Restrictions
1. [Export Compliance Acronyms - Export Compliance Training Institute](https://www.learnexportcompliance.com/tools/export-compliance-acronyms/)
----

### PEP Free Screening Sources
1. [Dilisense - Free Sanction, PEP and Criminal Screening](https://www.dilisense.com/en_US)
2. [Namescan Free PEP Check](https://namescan.io/FreePEPCheck.aspx)
----

### Credit rating & Insolvency Info | Bonitäts Informationen
1. [Justizportal Insolvenzbekanntmachungen](https://www.insolvenzbekanntmachungen.de/)
2. [Insolvex.de](https://insolvex.de/)
3. [Indat.info - Insolvenzen (€)](http://www.indat.info/)
4. [InsolNet](http://www.insolnet.de/)
5. [Insolvenz-Portal](https://app.insolvenz-portal.de)
6. [Global Credit Reports](http://www.global-creditreports.com/)
7. [Negativmerkmalen-aus-dem-schuldnerverzeichnis](https://www.ra-micro.de/erlaeuterungen-zu-negativmerkmalen-aus-dem-schuldnerverzeichnis/)
8. [Vonunsbekommtihrnix.noblogs.org](https://vonunsbekommtihrnix.noblogs.org/)
----

### STIFTUNGEN NON PROFIT
1. [Bundesverband Deutscher Stiftungen](https://www.stiftungen.org/verband/was-wir-tun/forschung-daten-und-wissen/stiftungssuche.html)
2. [Deutsches-stiftungszentrum](https://www.deutsches-stiftungszentrum.de/stiftungen/index.html)
3. [Lobbypedia Parteispenden](https://lobbypedia.de/wiki/Parteispenden-Datenbank)
4. [Nonprofit Explorer](https://projects.propublica.org/nonprofits/)
----

### Cryptocurrency
1. [@ Blockchaininfo Bitcoin Block Explorer](https://blockchain.info/)
2. [@BlockCypher](https://www.blockcypher.com/)
3. [@BlockExplorer Bitcoin Block Explorer](https://blockexplorer.com/)
4. [Bitcoin Abuse Database](https://www.bitcoinabuse.com/)
5. [Bitcoin Address Lookup, Checker and Alerts - BitcoinWhosWho](https://bitcoinwhoswho.com/)
6. [Bitcoin ATM Map – Find Bitcoin ATM](https://coinatmradar.com/)
7. [Bitcoin Block Explorer](https://blockexplorer.com)
8. [Bitcoinity.org](https://bitcoinity.org/)
9. [BitcoinWhosWho Bitcoin Address Search](https://bitcoinwhoswho.com/)
10. [BitRef Bitcoin Address Balance Tool](https://bitref.com/)
11. [Blockchain](http://blockchain.com)
12. [Blockchain Block Explorer Search](https://www.blockchain.com/explorer)
13. [Blockchain.com/learning-portal/how-to-get-bitcoins](https://www.blockchain.com/learning-portal/how-to-get-bitcoins)
14. [Blockonomics Bitcoin Transaction Search](https://www.blockonomics.co/)
15. [ChainRadar Monero Block Explorer](https://chainradar.com/xmr/blocks)
16. [ChipMixer Bitcoin Tumbler](https://chipmixer.com/)
17. [Coinbase Cryptocurrency Market](https://www.coinbase.com/)
18. [DASH Digital Cash](https://www.dash.org/)
19. [EtherChain Ethereum Block Explorer](https://www.etherchain.org/accounts/)
20. [@ Etherscan Ethereum Block Explorer](https://etherscan.io/)
21. [Financial Underground Kingdom](https://fuk.io/)
22. [Follow the Bitcoin | Automating OSINT BLOGPOST](http://www.automatingosint.com/blog/2017/09/follow-the-bitcoin-with-python-blockexplorer-and-webhose-io/)
23. [Fried Cryptocurrency Scam Checker](https://fried.com/crypto-scam-checker/)
24. [http://blockseer.com](http://blockseer.com)
25. [http://coinatmradar.com](http://coinatmradar.com)
26. [http://learnmeabitcoin.com/tools/path/](http://learnmeabitcoin.com/tools/path/)
27. [http://walletexplorer.com](http://walletexplorer.com)
28. [https://twitter.com/chainalysis TOOL](https://twitter.com/chainalysis)
29. [LiveCoinWatch Cryptocurrency Market Index](https://www.livecoinwatch.com/)
30. [MoneroBlocks Monero Block Explorer](https://moneroblocks.info/)
31. [WalletExplorer Bitcoin Block Explorer](https://www.walletexplorer.com/)
32. [WalletExplorer.com: smart Bitcoin block explorer](https://www.walletexplorer.com/)
33. [XMRChain Block Explorer](https://xmrchain.net/)
34. [Bitcoin Block Reward Halving Countdown](https://bitcoinblockhalf.com/)
35. [SHA-256 hash calculator | Xorbin](https://xorbin.com/tools/sha256-hash-calculator)
36. [@Browserling Web Developer Tools](https://www.browserling.com/tools)
37. [@www.random.org Bytes](https://www.random.org/bytes/)
38. [https://localbitcoins.com/](https://localbitcoins.com/)
39. [Bitcoin ATM MAp](https://coinatmradar.com/)
40. [Coinlib Cryptocurrency Exchanges](https://coinlib.io/exchanges)
41. [xBlog: Using OSINT to investigate Cryptocurrency Transactions](https://medium.com/ax1al/using-osint-to-investigate-cryptocurrency-transactions-7229f64c671a)
----

### Whistleblower Infrastructure
1. [WikiLeaks](http://wikileaks.org/)
2. [balkanleaks.eu/](https://balkanleaks.eu/)
3. [TOOL: globaleaks (Software for Whistleblowers)](https://www.globaleaks.org/)
----

### Associations
1. [Association of Certified Anti-Money Laundering Specialists | ACAMS](https://www.acams.org/)
2. [International Compliance Association ICA](https://www.int-comp.org/)
3. [DICO e.V. - Deutsches Institut für Compliance e.V.](https://www.dico-ev.de/)
4. [Anti Money Laundering (AML) | Compliance | CDD | Financial Crime Prevention | Qualifications, Training, Courses & Membership](https://www.int-comp.org/)
5. [ICT - Financial Compliance Training, AML Training, and Compliance Courses | ICT](https://www.int-comp.com/)
6. [The FraudClassifier Model - The Federal Reserve - FedPayments Improvement](https://fedpaymentsimprovement.org/fraudclassifier/index.html)
----

### Misc
1. [Dark Commerce: How a New Illicit Economy Is Threatening Our Future: Louise I. Shelley: 9780691170183: Amazon.com: Books](https://www.amazon.com/Dark-Commerce-Illicit-Economy-Threatening/dp/0691170185)
2. [Grand Theft Europe](https://news-gamer.com/starter/qAh48EFikwvm6afHh?lang=en)
----

### Staatsanzeiger
1. [Staatsanzeiger Hessen](https://www.staatsanzeiger-hessen.de/startseite/)
----

