### GPS Coordinates GEO Locate
1. [@GeoNames](https://www.geonames.org/)
2. [ADDRESS LOOKUP Find the address of any place on Google Maps](https://ctrlq.org/maps/address/)
3. [Address to GPS Conversion](http://www.mapseasy.com/adress-to-gps-coordinates.php)
4. [Coordinate conversion](https://www.doogal.co.uk/BatchReverseGeocoding.php)
5. [GeolocateThis!](http://geolocatethis.site/)
6. [Google Maps GPS Koordinaten, Breiten- und Längengrad](https://www.gpskoordinaten.de/)
7. [GPS Visualizer: Quick Geocoder](http://www.gpsvisualizer.com/geocode)
8. [Latitude and Longitude Finder on Map Get Coordinates](http://www.latlong.net/)
9. [Meet Me](https://maper.info)
10. [MyGeoPosition.com](http://mygeoposition.com/)
11. [ShadowCalculator - Show sun shadows on google maps](http://shadowcalculator.eu/#/lat/50.08/lng/19.899999999999977)
----

### Google & Bing AddOn
1. [Bing Maps Jotpix](http://www.bing.com/maps/?form=MPSRCH&cp=38.885518%7e-90.159361&lvl=13&style=u&app=60306%7e)
2. [Bing Maps OSIT](http://www.bing.com/maps/)
3. [Bing Maps Twitter](http://www.bing.com/maps/?form=MPSRCH&cp=38.613548%7e-90.291924&lvl=11&style=u&app=40326%7e%7et%7e155845683591643137)
4. [Ctrlq Find the address of any place on Google Maps](https://ctrlq.org/maps/address/)
5. [Dual Maps: Google Maps and Bing Maps OSIT](http://data.mashedworld.com/dualmaps/map.htm)
6. [Google Maps OSIT](https://maps.google.com/)
7. [http://Map Switcher GitHub.com](http://Map Switcher GitHub.com)
8. [Instant Google Street View](http://www.instantstreetview.com/)
9. [Instant Google Street View OSIT](http://qsview.com)
10. [KMLMap Clasic Google Maps Interface](http://gokml.net/maps#ll=42.345573,-71.098326&z=3&t=r)
11. [Map Switcher](https://chrome.google.com/webstore/detail/map-switcher/fanpjcbgdinjeknjikpfnldfpnnpkelb)
----

### Maps
1. [Arcgis](https://www.arcgis.com)
2. [Baidu Maps 百度地图](http://map.baidu.com/)
3. [ctrlq.org Where am I](https://ctrlq.org/maps/where/)
4. [Daum 지도 Korean Maps](http://map.daum.net/)
5. [De.batchgeo.com](Batchgeo)
6. [Descartes GeoVisual Search](https://search.descarteslabs.com/)
7. [Every Disputed Territory in the World [Interactive Map]](http://metrocosm.com/disputed-territories-map.html)
8. [Flickr: Explore everyone's photos on a Map](http://www.flickr.com/map/)
9. [gosur Interaktive Karte Online Map](http://www.gosur.com/map/)
10. [HERE WeGo](https://wego.here.com/?x=ep&map=49.8044,9.977,10,normal)
11. [Kepler Geospatial Datamining](http://kepler.gl/#/)
12. [Mapify.us OSIT](http://mapify.us)
13. [Mapillary](https://www.mapillary.com/)
14. [Mapillary Crowdsourced Street-level Photos](https://www.mapillary.com/)
15. [MapMachine - National Geographic](http://maps.nationalgeographic.com/map-machine#s=r&c=43.74999999999998,%20-99.71000000000001&z=4)
16. [MapQuest](http://www.mapquest.com/)
17. [Maps - Sahel and West Africa Club Secretariat](https://www.oecd.org/swac/maps/)
18. [Maps | West Africa Gateway | Portail de l'Afrique de l'Ouest](http://www.west-africa-brief.org/maps)
19. [Metrocosm  Disputed Territory](http://metrocosm.com/disputed-territories-map.html)
20. [Naver Korean Maps  네이버 지도](http://map.naver.com/)
21. [Official MapQuest](https://www.mapquest.com/)
22. [OpenStreetCam](http://openstreetcam.org/map/)
23. [Perry-Castañeda Library Map Collection](http://www.lib.utexas.edu/maps/index.html)
24. [Scribble Maps OSIT](http://scribblemaps.com)
25. [Snap Map](https://map.snapchat.com/)
26. [TRAVIC TRACKER GEOPS](https://tracker.geops.ch)
27. [Truesize Compare Countries With This Simple Tool](http://thetruesize.com/)
28. [Washington Post- 40 maps that explain the world](http://www.washingtonpost.com/blogs/worldviews/wp/2013/08/12/40-maps-that-explain-the-world/)
29. [Wikimapia](http://wikimapia.org/)
30. [World Aeronautical Database](http://worldaerodata.com/)
31. [Yandex.Maps a detailed world map](https://yandex.com/maps/)
32. [HERE WeGo](https://wego.here.com/)
----

### Street View Player FMPS 2,  1024 width  576 height
1. [Brianfolts Google Maps Streetview Player](http://www.brianfolts.com/driver/)
2. [www.streetviewmovie.com/](http://www.streetviewmovie.com/)
3. [Directions Map (with Animated Street View)](http://www.tripgeo.com/Directionsmap.aspx)
4. [RouteView](http://routeview.org/VirtualRide/)
----

### Satellite Data / Drone footage
1. [@Soar.earth](https://soar.earth/)
2. [Airbusds sampledata](http://www.intelligence-airbusds.com/worlddem-sampledata/)
3. [Bhuvan | NRSC Open EO Data Archive |](http://bhuvan.nrsc.gov.in/data/download/index.php)
4. [Class NOAA](http://www.class.noaa.gov/)
5. [Corona @ CAST UA](http://corona.cast.uark.edu/)
6. [DgiInpe Catálogo de Imagens](http://www.dgi.inpe.br/CDSR/)
7. [Digital Coast Home](https://coast.noaa.gov/digitalcoast/)
8. [DigitalGlobe](http://www.digitalglobe.com/)
9. [EarthExplorer](https://earthexplorer.usgs.gov/)
10. [EOLi-SA  Earth Online - ESA](https://earth.esa.int/web/guest/eoli)
11. [Eorc](http://www.eorc.jaxa.jp/ALOS/en/aw3d30/)
12. [EOSDATA Land Viewer satellite and aerial imagery](https://lv.eosda.com/)
13. [GLCF: Welcome](http://landcover.org/)
14. [Land Viewer | EOS](https://eos.com/landviewer/)
15. [NOAA: Data Access Viewer](https://coast.noaa.gov/dataviewer/#)
16. [Reverb Echo Nasa](https://reverb.echo.nasa.gov/reverb/)
17. [sasgis.ru Sasplaneta](http://sasgis.ru/sasplaneta/)
18. [Satellite image Decision tree Twitter.com](https://twitter.com/hatless1der/status/1334685220923174913/photo/1)
19. [Scihub Copernicus](https://scihub.copernicus.eu/dhus/)
20. [Terraserver](https://www.terraserver.com)
21. [VITO Earth Observation](http://www.vito-eodata.be/PDF/portal/Application.html#Home)
22. [xBlog: Aerial Imagery in QGIS](https://sector035.nl/articles/aerial-imagery-in-qgis)
23. [xBlog: QUIZTIME — Berlin Hotel](https://medium.com/@bearhunt38/quiztime-berlin-hotel-f850d6bbda67)
24. [xBlog: Resources for Finding and Using Satellite Images](https://gijn.org/resources-for-finding-and-using-satellite-images/)
25. [xBlog: Three Useful Resources For Maps and Satellite Images – NixIntel](https://nixintel.info/osint-tools/three-useful-resources-for-maps-and-satellite-images/)
26. [xBlog: Visual photo identification with Charles White – osintme.com](https://www.osintme.com/index.php/2020/10/05/visual-photo-identification-with-charles-white/)
27. [xBlog: Where to Access Satellite Imagery –  Citizen Evidence Lab – Amnesty International](https://citizenevidence.org/2020/03/20/magery/)
28. [Zoom Earth](https://zoom.earth/)
29. [Zoom Earth](http://www.flashearth.com)
----

### GEO Located News
1. [Newsmap](http://newsmap.jp/)
2. [newspaper map](http://newspapermap.com/)
----

### GEO Located Imagery
1. [Flickr: Explore everyone's photos on a Map](https://www.flickr.com/map)
2. [Worldcam](http://worldc.am/)
3. [bearpanther.com/instamap/](http://bearpanther.com/instamap/)
----

### Map Tools
1. [BatchGeo: Erstellen Sie Karten aus Ihren Daten](https://de.batchgeo.com/)
2. [Custom Map Creator & Map Maker | Maptive](https://www.maptive.com/)
3. [Free Map Tools](https://www.freemaptools.com/)
4. [Google My Maps](https://www.google.com/maps/d/)
5. [Google My Maps Help](https://support.google.com/mymaps#topic=3188329)
6. [Map Tools: List of Google maps tools](https://www.mapdevelopers.com/map_tools.php)
7. [Photo-Interpretation-Student-Handbook.-Photo-Interpretation-Principles.pdf](http://sites.miis.edu/geospatialtools2012/files/2012/07/Photo-Interpretation-Student-Handbook.-Photo-Interpretation-Principles.pdf)
8. [Scribble Draw On Maps and Make Them Easily.](https://www.scribblemaps.com)
9. [www.arcgis.com](https://www.arcgis.com)
10. [gmapgis Draw on Google Maps](https://www.gmapgis.com/)
11. [Map Chart Tool](https://mapchart.net/europe.html)
12. [StoryMap JS](https://storymap.knightlab.com/)
----

### IP2GeoAPIs
1. [What Is the Best API for Geolocating an IP Address?](https://ahmadawais.com/best-api-geolocating-an-ip-address/)
2. [IP Address API and Data Solutions](https://ipinfo.io/)
3. [ipapi - IP Address Location API](https://ipapi.co/)
----

### Mobile tracking
1. [FPF_Geolocation_01](https://fpf.org/wp-content/uploads/2020/05/FPF_Geolocation_Infographic_May_2020.pdf)
----

### Weather
1. [Windy.com](https://www.windy.com/?53.574,9.951,5)
----

### Webcam
1. [Webcams from around the World](https://worldcam.eu/)
2. [Insecam](http://www.insecam.org/)
3. [EarthCam](https://www.earthcam.com/)
4. [Welcome to Webcamtaxi](https://www.webcamtaxi.com/en/)
----

### Maritime Assets
1. [@Shipping Glossary (OOCL) & Incoterms](https://www.oocl.com/eng/resourcecenter/shippinglossary/Pages/default.aspx)
2. [AIS-Schiffspositionen/-verkehr in Echtzeit](https://www.marinetraffic.com/de/)
3. [BIC: Bureau of International Containers](https://www.bic-code.org/)
4. [BoatFax Hull ID Number (HIN) Search](https://www.boatfax.com/index.php?option=com_bfx_checkit&Itemid=184)
5. [BoatInfoWorld Documented Vessels](https://www.boatinfoworld.com/)
6. [BoatInfoWorld.com - Search, View &amp; Download Vessel Information](https://www.boatinfoworld.com/)
7. [BoatNerd](http://ais.boatnerd.com/)
8. [Cargo Loop](http://www.cargoloop.com)
9. [Container tracking](http://container-tracking.org/)
10. [Croatian register of shipping > Home](http://www.crs.hr/en-us/home.aspx)
11. [Cruise Ship Tracker / Tracking Map Live](http://www.cruisin.me/cruise-ship-tracker/)
12. [CruiseMapper](https://www.cruisemapper.com/)
13. [Equasis](http://www.equasis.org/EquasisWeb/public/HomePage)
14. [eShips World Shipping Register](http://eships.net/)
15. [FleetMon](https://www.fleetmon.com)
16. [Florida Hull Number Search](http://pas.fdle.state.fl.us/pas/restricted/PAS/StolenBoats.jsf)
17. [Free AIS Ship Tracking of Marine Traffic - VesselFinder](https://www.vesselfinder.com/)
18. [FreightMetrix](http://www.freightmetrics.com.au/Marine/ ShipTracking/tabid/483/Default.aspx)
19. [http://www.gibraltarship.com/](http://www.gibraltarship.com/)
20. [IOC Live Piracy Map](https://www.icc-ccs.org/piracy-reporting-centre/live-piracy-map)
21. [Live Cruise Ship Tracker](https://www.livecruiseshiptracker.com/)
22. [LocusTraxx](http://www.locustraxx.com)
23. [Marine MAN Ltd ®](https://ships.jobmarineman.com/)
24. [Maritime-connector.com/ships](http://maritime-connector.com/ships/)
25. [Maritime-database.com](https://www.maritime-database.com/)
26. [MaritimeConnector](http://maritime-connector.com)
27. [myShipTracking](https://www.myshiptracking.com/)
28. [Navipedia](http://www.navipedia.net/index.php/Package_and_Container_Tracking)
29. [NOAA Vessel Search](https://www.st.nmfs.noaa.gov/st1/CoastGuard/VesselByName.html)
30. [OpenSeaMap](http://map.openseamap.org)
31. [OSP Maritime VHF](http://www.oldskoolphreak.com/tfiles/hack/fcc_radio.pdf)
32. [Panjiva Shipping Info](https://panjiva.com/search)
33. [PortFocus Port/Marina Info](http://portfocus.com/)
34. [Roose+Partners](http://rooselaw.co.uk/index.html)
35. [SailWX Cruise Ship and Ocean Liner Tracker](https://www.sailwx.info/shiptrack/cruiseships.phtml)
36. [SailWX Great Lakes Vessel Tracker](https://www.sailwx.info/shiptrack/greatlakesandsea.phtml)
37. [ScannerNet](http://www.scannernet.nl/maritiem/live-ais)
38. [ShipAIS](http://www.shipais.com/)
39. [ShipmentLink](https://www.shipmentlink.com/servlet/TDB1_CargoTracking.do)
40. [ShipSpotting](http://www.shipspotting.com/ais/index.php)
41. [Shodan ShipTracker](https://shiptracker.shodan.io/)
42. [SkunkTracker](https://www.mobilegeographics.com/skunktracker/)
43. [Terrestrial & Satellite AIS Tracking Service in Realtime](http://www.vesseltracker.com/app)
44. [TrackTrace Bill of Lading Tracking](https://www.track-trace.com/bol)
45. [Vessel Finder](https://www.vesselfinder.com/)
46. [World seaports catalogue, marine and seaports marketplace](http://ports.com/)
47. [VesselsValue](https://www.vesselsvalue.com/)
48. [Nok-schiffsbilder.de](https://nok-schiffsbilder.de/)
49. [Maritime Satellite Services | Marine Satellite System Provider](https://www.inmarsat.com/en/solutions-services/maritime.html)
----

### Aviation Assets
1. [ADS-B Exchange](https://global.adsbexchange.com/)
2. [Airlinecodes](http://www.airlinecodes.co.uk/aptcodesearch.asp)
3. [AIRLIVE.net](http://www.airlive.net/)
4. [AirNav Airport Information](http://www.airnav.com/airports/)
5. [AirNav RadarBox](https://www.radarbox24.com/flight/AA2097#)
6. [Airport Code Database Search](http://www.airlinecodes.co.uk/aptcodesearch.asp)
7. [Airport Webcams](http://airportwebcams.net/)
8. [Blackbookonline.info/aviation-public-records.aspx](http://www.blackbookonline.info/aviation-public-records.aspx)
9. [Counterfeit Aircraft Parts – Data about Fake Parts in Airplanes](https://www.havocscope.com/tag/counterfeit-aircraft-parts/)
10. [FAA Aircraft Registry](https://www.faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/)
11. [FAA Registry](https://registry.faa.gov/aircraftinquiry/)
12. [FAA Registry N-Number Search](https://registry.faa.gov/aircraftinquiry/NNum_Inquiry.aspx)
13. [FAA Registry Name Search](https://registry.faa.gov/aircraftinquiry/Name_Inquiry.aspx)
14. [FAA Unapproved Parts Notifications (UPN) (Airplane)](https://www.faa.gov/aircraft/safety/programs/sups/upn/)
15. [Faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/interactive_aircraft_inquiry](https://www.faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/interactive_aircraft_inquiry/)
16. [FlightAware - Tracker](https://flightaware.com/)
17. [FlightAware UK](https://uk.flightaware.com/)
18. [FlightRadar24](https://www.flightradar24.com/60,15/6)
19. [Jeppesen – Transforming the Way the World Moves](http://ww1.jeppesen.com/index.jsp)
20. [Landings Pilot License Search](http://www.landings.com/evird.acgi$pass*193800885!_h-www.landings.com/_landings/pages/search/search_namd_full.html)
21. [List of civil aircraft registration prefixes](https://en.wikipedia.org/wiki/List_of_aircraft_registration_prefixes)
22. [Listen to Live ATC (Air Traffic Control) Communications](https://www.liveatc.net/)
23. [Minnesota DOT N-Number Search](https://dotapp7.dot.state.mn.us/aeroreg-public/lookupAircraft.jsp)
24. [NRD Aviation Radio Frequencies](http://nationalradiodata.com/aviation-airport-scanner-frequencies-home.jsp)
25. [NTSB Aviation Accident Database](https://www.ntsb.gov/_layouts/ntsb.aviation/index.aspx)
26. [Page Not Found](https://www.ihs.com/products/janes-airports-handling-agents.html)
27. [Passenger Airlines Wiki](https://en.wikipedia.org/wiki/List_of_passenger_airlines#List_of_passenger_airlines)
28. [PHOTOS Airliners.net | Aviation Photography, Discussion Forums & News](https://www.airliners.net/)
29. [PHOTOS JetPhotos](https://www.jetphotos.com/)
30. [PHOTOS Planespotters.net](https://www.planespotters.net/)
31. [Planefinder](https://planefinder.net/)
32. [PlaneSpotters Aviation ID](https://www.planespotters.net/)
33. [radarbox24-pro](https://www.radarbox24.com/)
34. [RadarVirtuel.com](http://www.radarvirtuel.com/)
35. [Tracked Aircraft - PlaneLogger registration database](http://www.planelogger.com/Aircraft)
36. [Travel By Drone](http://travelbydrone.com/)
37. [Welcome to Flightwise!](http://flightwise.com/)
38. [World Aeronautical Database](http://worldaerodata.com/)
39. [xBlog: bellingcat - A Beginner's Guide To Flight Tracking - bellingcat](https://www.bellingcat.com/resources/how-tos/2019/10/15/a-beginners-guide-to-flight-tracking/)
40. [xBlog: bellingcat - A Beginner's Guide To Flight Tracking - bellingcat](https://www.bellingcat.com/resources/how-tos/2019/10/15/a-beginners-guide-to-flight-tracking/)
41. [xBlog: Rareplanes-an-introduction](https://medium.com/the-downlinq/rareplanes-an-introduction-b28449222ca4)
----

### Cargo Container Bill of Lading | Tracking
1. [ContainerTracking](http://container-tracking.org/)
2. [IHS Markit Tools](https://ihsmarkit.com/products/maritime-global-trade-atlas.html)
3. [ImportGenius](https://www.importgenius.com/)
4. [PIERS Bill of lading](https://ihsmarkit.com/products/piers.html)
5. [Searates Container Tracking](https://www.searates.com/container/tracking)
6. [TrackTrace](https://www.track-trace.com/)
7. [TrackTrace Container Tracking](https://www.track-trace.com/container)
8. [SECURESYSTEM (Sattelite, GSM & AIS)](https://www.securesystem.net/)
9. [Zillionsource](http://www.zillionsource.com/)
10. [Bosch Service Solutions](https://www.boschservicesolutions.com/)
11. [Incoterms 2020 - UPDATE: Definition, Bedeutung, Funktion [Spickzettel]](https://www.sendcloud.de/incoterms-definition-bedeutung-funktion/)
----

### Rail Assets
1. [ACW Railway Maps](http://www.acwr.com/economic-development/rail-maps)
2. [Archive Freight Rail Works](http://archive.freightrailworks.org)
3. [Association of American Railroads](https://www.aar.org)
4. [Trainline](https://www.thetrainline.com)
5. [Nederlandse Spoorwegen](https://www.ns.nl/)
6. [Data.gov Railway](https://catalog.data.gov/dataset?tags=railroad)
7. [DOT Railroads](https://www.transportation.gov/railroads)
8. [OpenRailwayMap](https://www.openrailwaymap.org/)
9. [Federal Railroad Administration](https://www.fra.dot.gov/Page/P0001)
10. [International Association of Railway Operations](http://www.iaror.org)
11. [OpenRailwayMap](https://www.openrailwaymap.org/)
12. [Rail Delivery Group](https://www.raildeliverygroup.com/our-services/rail-data.html)
13. [RailWebcams](http://railwebcams.net)
14. [World By Map Railways](http://world.bymap.org/Railways.html)
----

### QGIS
1. [QGIS - Wikipedia](https://en.wikipedia.org/wiki/QGIS)
2. [Download QGIS](https://qgis.org/en/site/forusers/download.html)
3. [#WoInDeutschland - bear hunt - Medium](https://medium.com/@bearhunt38/woindeutschland-5365844819ff)
4. [QGIS for OSINT use.](https://medium.com/@bearhunt38/qgis-for-osint-use-e53cfd0dfba8)
5. [Geo locating photo’s with OSM overpass API - bear hunt - Medium](https://medium.com/@bearhunt38/geo-locating-photos-with-osm-overpass-api-10ed35b95f11)
----

### Metadata GEOINT
1. [I Know Where Your Cat Lives: About](https://iknowwhereyourcatlives.com/about/)
----

### Logistics Supply Chain
1. [Logistics Glossary](https://www.logisticsglossary.com/)
2. [Panjiva Track Shipping Information](https://panjiva.com/)
----

### COUNTRY CODES
1. [2-Letter, 3-Letter, Country Codes for All Countries in the World](http://www.worldatlas.com/aatlas/ctycodes.htm)
2. [Passport Index](https://www.passportindex.org/)
3. [GeoNames Geo Database of Names](https://www.geonames.org/)
4. [PRADO, an online public register of authentic identification and travel documents](https://www.consilium.europa.eu/prado/en/prado-start-page.html)
----

### Crisis Alerts & Travel Security | Incident Disaster
1. [ACLED (Conflict Data)](https://www.acleddata.com/)
2. [BBC Country Profiles](http://news.bbc.co.uk/2/hi/country_profiles/default.stm)
3. [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
4. [Crime Terror Nexus](https://crimeterrornexus.com/)
5. [Global Incident Map](http://www.globalincidentmap.com/threatmatrix.php)
6. [GWU Project on Extremism](https://extremism.gwu.edu/)
7. [Incidents Map  –  ACT Emergency Services Agency](http://esa.act.gov.au/community-information/incidents-map/)
8. [Islamism Map](https://islamism-map.com/#/)
9. [Live Earthquakes Map](http://quakes.globalincidentmap.com/)
10. [LiveUA Map](https://liveuamap.com/)
11. [Mapping Militant Organizations](http://web.stanford.edu/group/mappingmilitants/cgi-bin/)
12. [RSOE EDIS](http://hisz.rsoe.hu/)
13. [SPLC Hate Map](https://www.splcenter.org/hate-map)
14. [The Aviation Herald](http://www.avherald.com/)
15. [Travel Risk Map | US State Department](https://travelmaps.state.gov/TSGMap/)
16. [UMD Global Terrorism Database](https://www.start.umd.edu/gtd/)
17. [Real-time Crisis Alerts | SAM](https://www.samdesk.io/)
18. [Water, Peace and Security Global warning tool](https://waterpeacesecurity.org/map)
----

### Travel Advisory | Reisewarnungen | Sicherheit | Kriminalität
1. [Auswaertiges Amt Reisewarnungen](http://www.auswaertiges-amt.de/DE/Laenderinformationen/01-Reisewarnungen-Liste_node.html)
2. [Foreign travel advice](https://www.gov.uk/foreign-travel-advice)
3. [Countries & Areas Archive](https://www.state.gov/misc/list/index.htm)
4. [United Nations Office on Drugs and Crime](http://www.unodc.org/)
5. [Crime](https://www.numbeo.com/crime/)
6. [Europol](https://www.europol.europa.eu/)
7. [GlobalSecurity.org](http://www.globalsecurity.org/)
8. [Striving For A Safer World Since 1945](https://fas.org/)
9. [Airban -   European Commission](https://ec.europa.eu/transport/modes/air/safety/air-ban_de)
10. [World-map-of-encryption](https://www.gp-digital.org/world-map-of-encryption/)
----

### Country Infrastructure | Expat Guide
1. [Countries](http://reliefweb.int/countries)
2. [The World Factbook — Central Intelligence Agency](https://www.cia.gov/library/publications/the-world-factbook/)
3. [Expatriates](https://www.justlanded.com/)
----

### Power Plug Guide
1. [power plug adapter needed?](https://www.power-plugs-sockets.com/)
----

### Culture & Customs | Kultur
1. [Tim Marshall The What & The Why](http://www.thewhatandthewhy.com/)
2. [Language Translation from #1 UK Online Translation Agency](http://www.kwintessential.co.uk/)
3. [International Business Etiquette and Manners for Global Travelers
cultural diversity, cross cultural communication, and intercultural business
relationships for success](http://www.cyborlink.com/besite/)
4. [Guides to Culture, Customs and Etiquette for 80+ Countries](https://www.commisceo-global.com/resources/country-guides)
5. [Localingual](https://localingual.com/)
----

### Medical Health Pandemic | Medizinische Informationen
1. [Countries](http://reliefweb.int/countries)
2. [World Health Organization](http://www.who.int/en/)
3. [Home](https://www.ncbi.nlm.nih.gov/pubmed/)
4. [CDC Works 24/7](https://www.cdc.gov/)
----

### Consulate Embassies | Konsulats Informationen
1. [Embassy List, Embassies and Consulates Around the World](http://www.embassyworld.org/)
2. [Helpline Database > Embassy Database](http://www.helplinedatabase.com/embassy-database/)
----

### Press Freedom
1. [2016 World Press Freedom Index](https://rsf.org/en/ranking)
----

### SIGINT | Radio
1. [Bearcat Family Radio Frequency Database](http://www.bearcat1.com/family.htm)
2. [Bearcat Fast Food Radio Frequency Database](http://www.bearcat1.com/fastfood.htm)
3. [Bearcat Military Radio Frequency Database](http://www.bearcat1.com/feds.htm)
4. [Bearcat Mr. Scanner Radio Frequency Database](http://www.bearcat1.com/subscription.htm)
5. [Bearcat Sport Radio Frequency Database](http://www.bearcat1.com/sports.htm)
6. [Broadcastify](http://www.broadcastify.com/)
7. [Digital Radio Frequency Search](https://digitalfrequencysearch.com/index.php)
8. [DXing National Radio Scanner Frequency Database](https://www.dxing.com/scanfreq.htm)
9. [Motorola Business Radio Frequency Database](https://www.buytwowayradios.com/blog/2015/05/default_frequencies_for_motorola_business_radios.html)
10. [N4JRI's Security Scanning Blog](https://www.qsl.net/n4jri/index.htm)
11. [NatCom Shortwave Radio Frequency Database](http://www.nat-com.org/swfreq.htm)
12. [National Radio Data Scanner Frequency Database](http://nationalradiodata.com/scanner-frequencies-home.jsp)
13. [NRD Amateur Radio Call Signs](http://nationalradiodata.com/amateur-radio-home.jsp)
14. [Old Skool Phreak FCC Radio Frequency Database](http://www.oldskoolphreak.com/tfiles/hack/fcc_radio.pdf)
15. [OldSkoolPhreak Radio Frequency Database](http://www.oldskoolphreak.com/tfiles/hack/fcc_radio.pdf)
16. [PoliceFrequencies Police Radio Frequency Database](http://www.police-frequencies.com/)
17. [RadioReference OSIT](http://radioreference.com)
18. [RadioReference Scanner Radio Frequency Database](https://www.radioreference.com/apps/db/)
19. [Rollanet Scanner Radio Frequency Database](http://www.rollanet.org/~n0klu/Ham_Radio/1000's%20-%20SCANNER%20&%20HAM%20Radio%20Frequencies%20-%20Ebook.pdf)
20. [ScannerFrequencies Scanner Radio Frequency Database](https://www.scannerfrequencies.com/)
21. [ScannerMaster Scanning Blog](http://www.scannermasterblog.com/tag/scanner-radio)
22. [Signal Identification Wiki](https://www.sigidwiki.com/wiki/Signal_Identification_Guide)
23. [such indexx](http://www.funkfrequenzen01.de/indexx.htm)
24. [www.safetybasement.com/Spy-Listening-Recording-Devices-s/389.htm](http://www.safetybasement.com/Spy-Listening-Recording-Devices-s/389.htm)
25. [ZipScanners Scanner Dealer](https://www.zipscanners.com/)
26. [United States Frequency Allocation Chart | National Telecommunications and Information Administration](https://www.ntia.doc.gov/page/2011/united-states-frequency-allocation-chart)
27. [Radio Garden](https://radio.garden/)
28. [radiooooo](https://radiooooo.com/)
----

### SIGINT II
1. [CellReception Tower Map](http://www.cellreception.com/towers)
2. [GitHub SIGINT Tools ravens/awesome-telco](https://github.com/ravens/awesome-telco)
3. [About RTL-SDR](https://www.rtl-sdr.com/about-rtl-sdr/)
----

### Pay Telephones
1. [El Jefe's Payphone Directory (US)](https://www.payphone-directory.org/)
----

### Webcams
1. [123Cam](http://123cam.com/)
2. [Airport Webcams](http://airportwebcams.net/)
3. [CamVista](http://www.camvista.com/)
4. [EarthCam](https://www.earthcam.com/)
5. [Fisgonia](http://www.fisgonia.com/)
6. [Insecam](https://www.insecam.org/)
7. [Krooz Cams](http://www.kroooz-cams.com)
8. [Lookr](https://www.lookr.com/)
9. [OpenStreetCam](https://www.openstreetcam.org/map/@40.728397037445035,-73.99206161499025,12z)
10. [Opentopia](http://www.opentopia.com/)
11. [SeeAllTheThings](https://github.com/baywolf88/seeallthethings)
12. [SurveillanceUnderSurveillance](https://kamba4.crux.uberspace.de/)
13. [The Webcam Network](http://www.the-webcam-network.com/)
14. [Webcams.Travel](https://www.webcams.travel/)
15. [Unsecured IP Camera List](https://reolink.com/unsecured-ip-camera-list/)
16. [WorldWebcams](http://world-webcams.nsspot.net/?m=Webcam_map)
----

