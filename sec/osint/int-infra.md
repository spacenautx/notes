### Cached | Archive | Wayback History
1. [@Archive.org/](http://www.archive.org/)
2. [@CachedView Archive Tool](https://cachedview.nl/)
3. [Archivbestände des Reichssicherheitshauptamts](https://archive.org/details/ss-rsha/page/n35/mode/2up)
4. [Archive Today Archive.is (DE) Deleted Social Media + Screenshot](https://archive.fo/)
5. [Archive-it.org/](http://www.archive-it.org/public/all_collections)
6. [Archive.is](http://archive.is/)
7. [Cached Pages](http://www.cachedpages.com/)
8. [Internet Archive Datasets](https://archive.org/details/LIUsers.7z)
9. [Pandora.nla.gov.au](http://pandora.nla.gov.au)
10. [Time Travel](http://timetravel.mementoweb.org/)
11. [UKWA Home](http://www.webarchive.org.uk/ukwa/)
12. [Viewcached.com/](http://www.viewcached.com/)
13. [WebCite](http://www.webcitation.org/query)
14. [World Imagery Wayback Livingatlas](https://livingatlas.arcgis.com/wayback/)
----

### Web Screenshot by Proxy
1. [Snapito Take a screenshot of a full web page](https://snapito.com/)
2. [Web Screenshots](http://ctrlq.org/screenshots/)
3. [Web-capture](https://web-capture.net/)
----

### Domain IP Search | MyIP
1. [@Robtex!](https://www.robtex.com/)
2. [DNS Spy Monitor, validate and verify your DNS configurations](https://dnsspy.io/)
3. [Find other sites hosted on a web server](https://www.yougetsignal.com/tools/web-sites-on-web-server/)
4. [Free IP address tools for IPv4 and IPv6 | IPAddressGuide](https://www.ipaddressguide.com/)
5. [Freegeoip.app  search the geolocation of IP addresses](https://freegeoip.app/)
6. [Info:Domain Recon Top 10 Tools](https://medium.com/@dalvikbytecode/top-10-osint-tools-to-help-you-do-recon-a-domain-53d3af8b1ad2)
7. [Ipv6 Locator](http://ipv6locator.net/)
8. [Network-tools.com ***](https://network-tools.com/)
9. [TOOL: IPNetInfo: Retrieve IP Address Information from WHOIS servers](https://www.nirsoft.net/utils/ipnetinfo.html)
10. [ViewDNS.info***](http://viewdns.info/)
11. [Who is hosting This](https://www.whoishostingthis.com/)
12. [xBlog: Geolocating Mobile Phones With An IP – NixIntel](https://nixintel.info/osint/geolocating-mobile-phones-with-an-ip/)
13. [xBlog: How Cartographers for the U.S. Military Inadvertently Created a House of Horrors in South Africa](https://gizmodo.com/how-cartographers-for-the-u-s-military-inadvertently-c-1830758394)
14. [xBlog: Investigating Infrastructure Links with Passive DNS and Whois Data –  Citizen Evidence Lab – Amnesty International](https://citizenevidence.org/2020/06/26/investigating-infrastructure-links-with-passive-dns-and-whois-data/)
----

### Detect Technologies on Websites
1. [Wappalyzer](https://www.wappalyzer.com/)
2. [SpiderFoot](https://www.spiderfoot.net/)
----

### Discovery
1. [@Shodan](https://www.shodan.io/)
2. [@SpiderFoot](https://www.spiderfoot.net/)
3. [Copyscape Plagiarism Checker](http://www.copyscape.com/)
4. [Daily DNS Changes & Web Hosting Activity](http://dailychanges.domaintools.com/)
5. [GitHub: random-robbie/My-Shodan-Scripts](https://github.com/random-robbie/My-Shodan-Scripts)
6. [GitHub: sa7mon/S3Scanner (Scan for open S3 buckets)](https://github.com/sa7mon/S3Scanner)
7. [GitHub: techgaun/github-dorks](https://github.com/techgaun/github-dorks)
8. [NetBlocks mapping internet freedom](https://netblocks.org/)
9. [Punkspider Punk.sh](https://www.punkspider.org/)
10. [Redirect Detective](http://redirectdetective.com/)
11. [Registered Domain Names Search](https://domainsdb.info/)
12. [URL and website scanner](https://urlscan.io/)
13. [xBlog: DNS Records, QAnon, and How To Handle Uncertainty In OSINT. – NixIntel](https://nixintel.info/osint/do-dns-records-prove-that-jim-watkins-is-q/)
14. [xBlog: Don’t leak sensitive data via security scanning tools by John Opdenakker](https://www.peerlyst.com/posts/don-t-leak-sensitive-data-via-security-scanning-tools-john-opdenakker?)
15. [xBlog: Mining DNS MX Records for Fun and Profit – covert.io](http://www.covert.io/mining-mx-records-for-fun-and-profit/)
16. [zoomeye.org/ Shodan Clone](https://www.zoomeye.org/)
----

### Visualise  Domains
1. [Visual Site Mapper ***](http://www.visualsitemapper.com/)
2. [GeoPeeker | See how a site appears to the rest of the world](https://geopeeker.com/)
----

### Search Backlinks
1. [Backlink Checker](http://smallseotools.com/backlink-checker/)
2. [Backlink link analysis tool - Link research ***](https://openlinkprofiler.org/)
3. [Backlinks Checker Tool](http://www.backlinkwatch.com/)
4. [Majestic](https://de.majestic.com/)
5. [Mangools SEO](https://mangools.com/)
6. [Open Site Explorer: Link Research & Backlink Checker](https://moz.com/researchtools/ose/)
7. [Ahrefs SEO](https://ahrefs.com/)
8. [Screaming Frog SEO Spider](https://www.screamingfrog.co.uk/seo-spider/)
----

### Enumerate Subdomains
1. [Assetnote.io](https://assetnote.io/)
2. [Cloudflare IP Ranges](https://www.cloudflare.com/ips/)
3. [Find Subdomains - Online Penetration Testing Tools](https://pentest-tools.com/information-gathering/find-subdomains-of-domain)
4. [Find subdomains online — FindSubDomains ***](https://findsubdomains.com/)
5. [GitHub - samyoyo/Bluto: Recon, Subdomain Bruting, Zone Transfers](https://github.com/samyoyo/Bluto)
6. [GitHub: darkoperator/dnsrecon](https://github.com/darkoperator/dnsrecon)
7. [GitHub: knock](https://github.com/guelfoweb/knock)
8. [GitHub: KnockPy](https://github.com/santiko/KnockPy/blob/master/knock.py)
9. [GitHub: subbrute](https://github.com/TheRook/subbrute/blob/master/subbrute.py)
10. [GitHub: Sublist3r](https://github.com/aboul3la/Sublist3r/)
11. [lanmaster53/recon-ng](https://github.com/lanmaster53/recon-ng)
12. [laramies/theHarvester](https://github.com/laramies/theHarvester)
13. [SecLists/Discovery/DNS at master · danielmiessler/SecLists · GitHub](https://github.com/danielmiessler/SecLists/tree/master/Discovery/DNS)
14. [VirusTotal](https://www.virustotal.com/)
----

### Domain Health
1. [Is It Down? Website Down Or Offline For You? Check It Here!](http://downforme.org)
2. [Domain Health Check](https://mxtoolbox.com/domain/)
3. [IP Blacklist Check by WebSitePulse](https://www.websitepulse.com/help/testtools.dbl-test.html)
4. [Free Online Tools for Looking up Potentially Malicious Websites](https://zeltser.com/lookup-malicious-websites/)
----

### Domain Name Finder/Generator
1. [https://brainstorm.domains/](https://brainstorm.domains/)
2. [Namesh Domain Name Generator](https://www.namemesh.com/)
3. [Checkdomain Domaincheck](https://www.checkdomain.de/)
4. [Domain Check: Kostenlos prüfen und freie Domains finden](https://www.ionos.de/domains/domain-check)
----

### Build with
1. [BuiltWith Technologie Lookupquote-leftquote-right](https://builtwith.com/de/)
2. [What WordPress Theme Is That?](https://whatwpthemeisthat.com/)
----

### Torrent
1. [Torrent downloads and distributions for IP 3.87.253.130](https://iknowwhatyoudownload.com/en/peer/)
----

### SHORTCUTS
1. [ViewDNS.info](https://viewdns.info/)
2. [Network Tools From MxToolBox](https://mxtoolbox.com/NetworkTools.aspx)
3. [Pulsedive Cyber Threat Intelligence](https://pulsedive.com)
----

### WHOIS info
1. [@Domain Dossier](https://centralops.net/co/domaindossier.aspx)
2. [@DomainBigData.com (registrant)](https://domainbigdata.com/)
3. [@ViewDNS.info](http://viewdns.info/)
4. [*_Website Attribution Without WhoIs – Reverse IP Lookup – NixIntel](https://nixintel.info/osint/website-attribution-without-whois-reverse-ip-lookup/)
5. [Dnslytics.com](https://dnslytics.com/)
6. [domaintools.com IRIS](https://www.domaintools.com/products/iris/)
7. [Reverse Whois API demo by Whoxy.com](https://www.whoxy.com/reverse-whois/demo.php)
8. [Website Informer](https://website.informer.com/)
9. [Whoisology.com Reverse WhoIs Lookup](https://whoisology.com/)
----

### WHOIS Info pre GDPR
1. [@DomainBigData.com (Not functioning)](https://domainbigdata.com/)
2. [domainIQ (needs Registering)](https://www.domainiq.com/)
3. [easyWhois: Lookup Domain Whois Records and Research DNS](https://www.easywhois.com/)
4. [WHOIS Search, Domain Name, Website, and IP Tools](https://who.is/)
5. [Whois XML API - Whois Lookup - Domain Name Search](https://www.whoisxmlapi.com/)
6. [Whowas](https://www.apnic.net/static/whowas-ui/)
----

### Registration Data Access protocoll (RDAP)
1. [RDAP Web Client - LACNIC](https://rdap-web.lacnic.net/)
2. [RDAP Web Client -RDAP.org](https://client.rdap.org/)
3. [GitHub: Download jq - read JSON](https://stedolan.github.io/jq/download/)
----

### At same IP
1. [atsameip.intercode.ca - Find all websites hosted on the same IP-address](http://atsameip.intercode.ca)
2. [Find websites hosted on the same ip](http://www.sameip.org)
----

### Reverse analytics | Sourcecode
1. [PubDB traffic stats of websites](http://pub-db.com/)
2. [NerdyData Search for Source Code](https://nerdydata.com/search)
3. [SameID.net](http://sameid.net/)
4. [Analyze ID](http://analyzeid.com/)
5. [Search Engine for Source Code](https://publicwww.com/)
----

### Threat | Phish | Honey
1. [Censys](https://censys.io)
2. [HoneyDB](https://riskdiscovery.com/honeydb/)
3. [IBM X-Force Exchange](https://exchange.xforce.ibmcloud.com/)
4. [PhishTank](https://www.phishtank.com/)
5. [Threat Crowd](https://www.threatcrowd.org)
6. [Threat Intelligence Platform](https://threatintelligenceplatform.com/)
7. [VirusTotal](https://www.virustotal.com/gui/home/upload)
----

### Networktools
1. [Microsoft DOS pathping command](https://www.computerhope.com/pathping.htm)
2. [Network Tools](https://network-tools.com/)
3. [heise.de/tools/](https://www.heise.de/tools/)
4. [TOOL: Traceroute and Visual Route tool](http://www.visualroute.com/)
5. [G Suite Toolbox](https://toolbox.googleapps.com/apps/main/)
----

### SSL Certificate Search
1. [CertDB — SSL certificates search engine](http://certdb.com/)
2. [crt.sh SSL Certificate Search](https://crt.sh/)
3. [crt.sh - Identity (Domain Name, Organization Name) Certificate Fingerprint  crt.sh ID](https://crt.sh/)
4. [GitHub-Certgraph](https://lanrat.com/certgraph/)
5. [Google Certificate Transparency Report](https://transparencyreport.google.com/https/certificates)
----

### Passive DNS
1. [@DNSdumpster.com](https://dnsdumpster.com/)
2. [@RiskIQ Community](https://community.riskiq.com)
3. [mnemonic](https://passivedns.mnemonic.no/)
4. [PTRarchive.com Historical DNS records](http://ptrarchive.com/)
5. [SecurityTrails Free Search](https://securitytrails.com/)
6. [VirusTotal Passive DNS](https://www.virustotal.com)
----

### http Status
1. [Bulk URL HTTP Status Code, Header & Redirect Checker](https://httpstatus.io/)
2. [urlchecker.com](http://urlchecker.com/)
3. [HTTP Status Codes](https://www.restapitutorial.com/httpstatuscodes.html)
4. [Analyse your HTTP response headers](https://securityheaders.com/)
5. [List of HTTP status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
6. [Hypertext Transfer Protocol (HTTP) Status Code Registry](https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml#http-status-codes-1)
----

### Website Reputation
1. [urlvoid](https://www.urlvoid.com)
2. [Quttera Online Website Malware Scanner](https://quttera.com/home)
3. [site check sucuri](https://sitecheck.sucuri.net)
4. [SimilarWeb](https://www.similarweb.com/)
5. [Calculate Website Traffic Worth, Revenue and Pageviews with SiteWorthTraffic](http://www.siteworthtraffic.com/)
6. [Alexa](https://www.alexa.com)
----

### Find WIFI Network
1. [WiGLE: Wireless Network Mapping](https://wigle.net/)
2. [Find-wifi.mylnikov.org](https://find-wifi.mylnikov.org/)
----

### NIC
1. [Deutschland DENIC eG: Wir sind .de](https://www.denic.de/)
2. [Dnsbelgium.be](https://www.dnsbelgium.be/)
3. [EU Whois.eurid.eu/de/](https://whois.eurid.eu/de/)
4. [Finland Viestintävirasto Fi-domain](https://www.viestintavirasto.fi/en/fidomain.html)
5. [Frankreich Afnic.fr/](https://www.afnic.fr/)
6. [InterNIC](http://www.internic.net/whois.html)
7. [Iran .ir whois info query](http://whois.iranic.com/)
8. [Italien Registro .it](http://www.nic.it/?set_language=en)
9. [Litauen Domreg.lt/](https://www.domreg.lt/)
10. [Nic.ru/en](https://www.nic.ru/en/)
11. [Niederlände SIDN](https://www.sidn.nl/)
12. [Norwegen Norid.no/en/](https://www.norid.no/en/)
13. [Österreich Nic.at/index/](https://www.nic.at/index/)
14. [Pir.org](http://pir.org/)
15. [Portugal DNS.PT](https://www.dns.pt/pt/)
16. [Registro.br](https://registro.br/2/whois#/lresp)
17. [Russland Nic.ru/en/index.html](https://www.nic.ru/en/index.html)
18. [Schweiz Switch.ch/](https://www.switch.ch/)
19. [Spanien Dominios.es |](http://www.dominios.es/dominios/)
20. [UK Nominet.uk/](http://www.nominet.uk/)
21. [Verisign .com, .net, .name, .cc, .tv, .edu](https://www.verisign.com/en_US/domain-names/whois/index.xhtml?inc=www.verisigninc.com)
22. [Whois lookup](http://whois.afilias.net/)
----

### IANA ICAN
1. [AFRINIC](http://www.afrinic.net/)
2. [American Registry for Internet Numbers (ARIN)](https://www.arin.net/index.html)
3. [CCNSO](https://ccnso.icann.org/)
4. [Country Code Names Supporting Organisation](https://ccnso.icann.org/en)
5. [Download ccTLD domain name lists and zone files](http://viewdns.info/data/)
6. [Home](https://www.apnic.net/)
7. [IANA - Internet Assigned Numbers Authority](http://www.iana.org/)
8. [ICANN](https://www.icann.org/)
9. [Inicio](http://www.lacnic.net/)
10. [LACNIC Inicio](http://www.lacnic.net/)
11. [Registry Operator by IANA — Root Zone Database](https://www.iana.org/domains/root/db)
12. [Registry-Liste für gTLD bei ICANN](https://www.icann.org/resources/pages/listing-2012-02-25-en)
13. [RIPE Network Coordination Centre](https://www.ripe.net/)
14. [Whois Inaccuracy Complaint Form](https://forms.icann.org/en/resources/compliance/complaints/whois/inaccuracy-form)
----

### HTML
1. [Encode and decode a piece of text to its HTML equivalent](https://www.web2generators.com/html-based-tools/online-html-entities-encoder-and-decoder)
----

### Hardware
1. [Pinouts - Handbook of hardware schemes](http://pinouts.ru/)
----

### URL Decoder
1. [URL Decoding](https://ascii.cl/url-decoding.htm)
2. [SecurityTrails DNS History](https://securitytrails.com/dns-trails)
----

### Training
1. [Julia Evans wizard zines](https://wizardzines.com/)
----

### Cyberorg
1. [ENISA](https://www.enisa.europa.eu/)
2. [PhishStats](https://phishstats.info/)
----

### Law Enforcement (LE)
1. [ISP LE Guide](https://www.search.org/resources/isp-list/)
----

### Robots txt
1. [https://seositecheckup.com/seo-audit/robotstxt-test](https://seositecheckup.com/seo-audit/robotstxt-test)
----

