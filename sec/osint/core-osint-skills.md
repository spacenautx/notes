### OSINT Frameworks
1. [Bellingcat's Online Investigation Toolkit [bit.ly/bcattools]](https://docs.google.com/spreadsheets/d/18rtqh8EG2q1xBo2cLNyhIDuK9jrPGwYr9DI2UncoqJQ/edit#gid=930747607)
2. [Dating apps and hook-up sites for investigators](https://start.me/p/VRxaj5/dating-apps-and-hook-up-sites-for-investigators)
3. [Online Extremism/Terrorism](https://start.me/p/DPNyrl/online-extremismterrorism)
4. [OSINT Framework - Bruno Mortier](https://start.me/p/ZME8nR/osint)
5. [technisette.com](https://technisette.com)
6. [The Ultimate OSINT Collection](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
----

### Harvesting Web Data
1. [Mobile-Friendly Test](https://search.google.com/test/mobile-friendly)
2. [URL and website scanner](https://urlscan.io)
----

### Image Analysis
1. [MoonCalc moon position- and moon phases calculator](https://mooncalc.org)
2. [SunCalc sun position- und sun phases calculator](https://suncalc.org)
----

### Search Engines & Leveraging
1. [Baidu](https://baidu.com)
2. [Bing](https://bing.com)
3. [Duck Duck Go](https://duckduckgo.com/)
4. [Google](https://google.com)
5. [Google Trends](https://trends.google.com/trends/explore)
6. [Offensive Security’s Exploit Database Archive](https://www.exploit-db.com/google-hacking-database)
7. [Yandex](https://yandex.com)
----

### Language Translation
1. [Bing Microsoft Translator](https://www.bing.com/translator/)
2. [DeepL Translator](https://deepl.com)
3. [Google Translate](https://translate.google.com)
4. [Yandex.Translate](https://translate.yandex.com/)
----

### File Metadata Analysis
1. [ExifTool by Phil Harvey](https://github.com/exiftool/exiftool)
2. [FotoForensics](https://fotoforensics.com/)
3. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
----

### Reverse Image Searching
1. [Bing Image Search](https://www.bing.com/images/trending?FORM=ILPTRD)
2. [Google Images](https://images.google.com/?gws_rd=ssl)
3. [Yandex Image search](https://yandex.com/images/)
----

### Imagery and Maps
1. [BBBike.org](https://www.bbbike.org/)
2. [Bing Maps](https://www.bing.com/maps/)
3. [Google Earth](https://earth.google.com/web/)
4. [Google Maps](https://www.google.com/maps)
5. [Insecam](http://www.insecam.org/)
6. [Mapillary](https://www.mapillary.com/app/)
7. [PeakFinder](https://www.peakfinder.org/)
8. [TfL JamCams](https://www.tfljamcams.net/)
9. [World Imagery Wayback](https://livingatlas.arcgis.com/wayback/?active=29260&ext=-115.34940,36.03992,-115.24760,36.08807)
10. [Yandex Maps](https://yandex.com/maps/?)
11. [Webcams from around the World](https://worldcam.eu/)
----

