### OpSec & InfoSec in Crypto
1. [GitHub](https://github.com/OffcierCia/Crypto-OpSec-SelfGuard-RoadMap)
2. [Rob Braxman Tech](https://www.youtube.com/channel/UCYVU6rModlGxvJbszCclGGw)
3. [7 ways to level up your Bitcoin OpSec](https://blog.keys.casa/7-ways-to-level-up-your-bitcoin-opsec/)
4. [Fundamentals of OpSec in Crypto. Taken from GoodOpsec.org | by nobody | The Business Of Crypto | Medium](https://medium.com/the-business-of-crypto/fundamentals-of-opsec-in-crypto-7844ba701b1d)
5. [Here's how to protect your bitcoin and ethereum from hacking](https://www.cnbc.com/2017/11/02/heres-how-to-protect-your-bitcoin-and-ethereum-from-hacking.html)
6. [usenix](https://www.usenix.org/system/files/1401_08-12_mickens.pdf)
7. [Crypto WiFi Hack - How it Works | Ledger](https://www.ledger.com/academy/security/hack-wifi)
8. [The Hitchhiker’s Guide to Online Anonymity](https://anonymousplanet.org/guide.html)
9. [Security First In DeFi: How to keep crypto safe as a crypto beginner - YouTube](https://www.youtube.com/watch?v=hxHqE2W8scQy)
10. [Beginner’s Guide #12: Bitcoin Privacy & OpSec with Jameson Lopp - YouTube](https://www.youtube.com/watch?v=0aSQMeoz9ow)
11. [Bitcoin Security](https://www.lopp.net/bitcoin-information/security.html)
12. [5 Tips to Prevent Hackers From Stealing Your Crypto Assets | HackerNoon](https://hackernoon.com/5-tips-to-prevent-hackers-from-stealing-your-crypto-assets-e2243zig)
13. [Airgapcomputer.com](https://airgapcomputer.com/)
14. [How to Stay Safe on the Internet: A Guide for Crypto Users](https://trustwallet.com/blog/how-to-stay-safe-on-the-internet-crypto-guide)
15. [DEFI Hacks - How To Protect Your MetaMask Wallet From Getting Hacked (Protect Your Crypto And DEFI) - YouTube](https://www.youtube.com/watch?v=ixLuRvYlrlw)
16. [How to keep your smartphone safe from spying | by Joel Samuel | Medium](https://joelgsamuel.medium.com/how-to-keep-your-smartphone-safe-from-spying-d7d50fbed817)
17. [physical-bitcoin-attacks/README.md at master · jlopp/physical-bitcoin-attacks](https://github.com/jlopp/physical-bitcoin-attacks/blob/master/README.md)
18. [Cryptocurrency Security Checklist - CryptoSec](https://cryptosec.info/checklist/)
19. [GitHub - OffcierCia/Crypto-OpSec-SelfGuard-RoadMap: Here we collect and discuss the best DeFi,Blockchain and crypto-related OpSec researches and data terminals - contributions are welcome.](https://github.com/OffcierCia/Crypto-OpSec-SelfGuard-RoadMap#table-of-contents)
----

### Investigations
1. [Read this thread](https://twitter.com/officer_cia/status/1493395239905734667)
2. [How I investigate crypto hacks and security incidents: A-Z — CIA Officer's Blog](https://mirror.xyz/officercia.eth/BFzv17UwH6QG4q711NAljtSiP8eKR17daLjTdmAgbHw)
3. [TX Analysis tools – Telegraph](https://graph.org/TX-Analysis-tools-04-19)
4. [Save this list](https://github.com/OffcierCia/DeFi-Developer-Road-Map#transaction-visualization-scoring--tracking)
5. [MALTEGO (ПО)](https://www.maltego.com/downloads/)
6. [CRYSTAL](https://explorer.crystalblockchain.com/)
7. [BLOCKPATH](https://blockpath.com/)
8. [OXT](https://oxt.me/)
9. [SICP](http://sicp.ueba.su/)
10. [GRAPHSENSE](https://graphsense.info/)
11. [CRYPTO HOUND](https://c-hound.ai/)
12. [ORBIT](https://github.com/s0md3v/Orbit)
13. [ETHTECTIVE](https://ethtective.com/)
14. [Breadcrumbs.app](https://www.breadcrumbs.app)
15. [Tenderly](https://tenderly.co)
16. [CipherTrace](https://ciphertrace.com)
17. [Elliptic](https://www.elliptic.co)
18. [Bloxy](https://bloxy.info)
19. [txstreet.com/v/eth](https://txstreet.com/v/eth)
20. [Docs.blockscout.com](https://docs.blockscout.com)
21. [AMLBot](https://amlbot.com/)
----

### OSINT Collection
1. [OSINT pack](https://start.me/p/rxRbpo/ti?locale=en)
2. [ItIsMeCall911/Awesome-Telegram-OSINT](https://github.com/ItIsMeCall911/Awesome-Telegram-OSINT)
3. [GitHub](https://github.com/Coordinate-Cat/OSINT-TOOLS-CLI)
4. [API-s-for-OSINT/README.md at main · cipher387/API-s-for-OSINT](https://github.com/cipher387/API-s-for-OSINT/blob/main/README.md)
5. [GitHub - xploitstech/Xteam: Xteam All in one Instagram,Android,phishing osint and wifi hacking tool available](https://github.com/xploitstech/Xteam)
6. [GitHub](https://github.com/danieldurnea/FBI-tools)
7. [What's in Your Digital Fingerprint? - YouTube](https://youtu.be/hZlT3bn_gfo)
----

### Sec
1. [Crypto-OpSec-SelfGuard-RoadMap/README.md at main · OffcierCia/Crypto-OpSec-SelfGuard-RoadMap · GitHub](https://github.com/OffcierCia/Crypto-OpSec-SelfGuard-RoadMap/blob/main/README.md)
2. [drduh/macOS-Security-and-Privacy-Guide](https://github.com/drduh/macOS-Security-and-Privacy-Guide)
3. [physical-bitcoin-attacks/README.md at master · jlopp/physical-bitcoin-attacks](https://github.com/jlopp/physical-bitcoin-attacks/blob/master/README.md)
4. [The Hitchhiker’s Guide to Online Anonymity](https://anonymousplanet.org/guide.html)
5. [DeFi Security: Best Practices + Security Q&ACommentShare](https://defieducation.substack.com/p/defi-security-best-practices-security?s=r)
6. [6 Ways a Site Can Attack your MetaMask](https://bloom.co/blog/6-ways-a-site-can-attack-your-metamask/)
7. [What’s in Your Wallet?](https://arxiv.org/pdf/2109.06836.pdf)
8. [All known smart contract-side and user-side attacks and vulnerabilities in Web3.0, DeFi, NFT and Metaverse + Bonus – Telegraph](https://graph.org/All-known-smart-contract-side-and-user-side-attacks-and-vulnerabilities-in-Web30--DeFi-03-31)
9. [Key principles of storing crypto + Bonus – Telegraph](https://graph.org/Key-principles-of-storing-crypto-cold-wallet-attacks-defense-methods-best-practices--Bonus-04-23)
10. [Key principles of storing crypto + Bonus — CIA Officer's Blog](https://mirror.xyz/officercia.eth/GtKNkmRDR_hhCqrnSENjqfPDHHb0W1M2SVeXDp4swCQ)
----

### Cheatsheets
1. [Solidity Cheatsheets Pack + Bonus – Telegraph](https://graph.org/Solidity-Cheatsheets-Pack-03-20)
2. [Data: All known smart contract attacks and vulnerabilities – Telegraph](https://graph.org/Data-02-14)
3. [ETHSec Tools – Telegraph](https://graph.org/ETHSec-Tools-02-13)
4. [Blockchain Scaling Researches – Telegraph](https://graph.org/Blockchain-Scaling-Researches-03-13)
5. [All known smart contract-side and user-side attacks and vulnerabilities in Web3.0, DeFi, NFT and Metaverse + Bonus – Telegraph](https://graph.org/All-known-smart-contract-side-and-user-side-attacks-and-vulnerabilities-in-Web30--DeFi-03-31)
6. [TX Analysis tools – Telegraph](https://graph.org/TX-Analysis-tools-04-19)
7. [Key principles of storing crypto + Bonus – Telegraph](https://graph.org/Key-principles-of-storing-crypto-cold-wallet-attacks-defense-methods-best-practices--Bonus-04-23)
8. [Key principles of storing crypto + Bonus — CIA Officer's Blog](https://mirror.xyz/officercia.eth/GtKNkmRDR_hhCqrnSENjqfPDHHb0W1M2SVeXDp4swCQ)
----

### Awesome Blogs
1. [Aniccaresearch.tech](http://aniccaresearch.tech)
2. [Posts | Zefram Lou](https://zefram.xyz/posts/)
3. [Home - Alin Tomescu](https://alinush.github.io//)
4. [Cmichel.io](https://cmichel.io)
5. [DeFi Education](https://defieducation.substack.com)
6. [Femboy.capital](https://femboy.capital)
7. [0xrishabh](https://blog.0xrishabh.dev//)
8. [Alkimiya](https://aniccaresearch.tech)
9. [Based.builders](https://based.builders)
10. [Pentacle](https://pentacle.xyz/projects/security)
11. [Useweb3.xyz](https://useweb3.xyz)
12. [Web3.smsunarto.com](https://web3.smsunarto.com)
13. [Notonlyowner | Introduction to smart contract security and hacking in Ethereum](https://www.notonlyowner.com/learn/intro-security-hacking-smart-contracts-ethereum)
14. [Genesis 0x01: Simplified Roadmap for Blockchain Security](https://devansh.xyz/blockchain-security/2021/09/17/genesis-0x01.html)
15. [So you want to be a blockchain developer? :: tr3y.io](https://tr3y.io/articles/crypto/how2bloccchain.html)
16. [Solidity Tutorial](https://cryptodevhub.io/wiki/blockchain-development-tutorial)
17. [Sov's Compendium](https://sovs.notion.site/sovs/Sov-s-Compendium-41f097d28dae4d09801f10cde1b2d03b)
18. [she256](https://she256.org/guide/)
19. [Immersionden.xyz](https://immersionden.xyz)
20. [The 411 on Web3 101's: a Collective List of Web3 Learning Experi… — alli](https://alli.mirror.xyz/wMk-VhWDBVq0Oq-DbmIe3AX4Sz6eYWuYbiDswXPHQJE)
21. [Zero To Hero: Web3.0 and Solidity Development Roadmap](https://vitto.cc/web3-and-solidity-smart-contracts-development-roadmap/)
22. [GitHub](https://github.com/dcbuild3r/blockchain-development-guide)
23. [Kyrian Alex's Newsletter](https://kyrianalex.substack.com)
24. [Cryptoengineer.notion.site](https://cryptoengineer.notion.site)
25. [Notion – The all-in-one workspace for your notes, tasks, wikis, and databases.](https://noxx3xxon.notion.site/The-EVM-Handbook-bb38e175cc404111a391907c4975426d)
26. [Going the distance](https://jeiwan.net/)
27. [Notion – The all-in-one workspace for your notes, tasks, wikis, and databases.](https://naderdabit.notion.site/Nader-s-web3-Learning-Resources-for-Developers-a200ed2ef21c4d578dc158df2b882c63)
28. [DeTools](https://joshcs.xyz/detools)
29. [ZK Learning Resources](https://learn.0xparc.org)
30. [Understand EVM bytecode – Part 1](https://blog.trustlook.com/understand-evm-bytecode-part-1/)
31. [MEV Bots & Uniswap Implicit Assumptions](https://blog.alphaventuredao.io/mev-bots-uniswap-implicit-assumptions/)
32. [Notion – The all-in-one workspace for your notes, tasks, wikis, and databases.](https://ripe-blossom-fb2.notion.site/Ethereum-DeFi-Knowledge-pack-437812a972f14d888ea3b0dc500ff6b2)
----

### Malware - File Analysis
1. [Any.Run](https://app.any.run/)
2. [CRITs](https://github.com/crits/crits)
3. [CrowdStrike Feed Management System](https://github.com/CrowdStrike/CrowdFMS)
4. [cuckoosandbox/cuckoo](https://github.com/cuckoosandbox/cuckoo)
5. [Hybrid Analysis](https://www.hybrid-analysis.com/)
6. [Intezer Analyze](https://analyze.intezer.com/)
7. [IRIS-H Digital Forensics](https://iris-h.services/#/pages/dashboard)
8. [Joe Sandbox](https://www.joesandbox.com/)
9. [Jotti's malware scan](https://virusscan.jotti.org/en)
10. [Malfrats/xeuledoc](https://github.com/Malfrats/xeuledoc)
11. [Malicious PDF Analysis](https://zeltser.com/tools-for-malicious-pdf-analysis/)
12. [malwaretracker.com](https://malwaretracker.com/)
13. [OPSWAT MetaDefender File Analysis](https://metadefender.opswat.com/#!/)
14. [PacketTotal](https://packettotal.com/)
15. [PDF Analysis Tools](https://blog.didierstevens.com/programs/pdf-tools/)
16. [PDF Explorer](https://www.rttsoftware.com/pdfe.html)
17. [PDF Stream Dumper](http://sandsprite.com/blogs/index.php?uid=7&pid=57)
18. [PDFExaminer: pdf malware analysis](https://www.pdfexaminer.com/)
19. [PEframe](https://github.com/guelfoweb/peframe)
20. [PPEE](https://www.mzrst.com/)
21. [Quicksand.io Document Analyzer](https://www.quicksand.io/)
22. [radare](https://www.radare.org/r/)
23. [radareorg/cutter](https://github.com/radareorg/cutter)
24. [Totalhash - Malware File Analysis](https://totalhash.cymru.com/upload/)
25. [VirSCAN.org](http://www.virscan.org/)
26. [VirusShare-related archives](https://a4lg.com/downloads/vxshare/)
27. [VirusShare.com](https://virusshare.com/)
28. [VirusTotal - File Analysis](https://www.virustotal.com/)
29. [Ghidra](https://ghidra-sre.org/)
30. [VirusTotal](https://t.me/VirusTotalAV_bot)
31. [Dr.Web](https://t.me/DrWebBot)
32. [firstlookmedia/dangerzone](https://github.com/firstlookmedia/dangerzone)
----

