### Instagram
1. [Internet Archive: Wayback Machine](https://archive.org/web/)
2. [Social Searcher](https://www.social-searcher.com/)
3. [www.uvrx.com/social.html](http://www.uvrx.com/social.html)
4. [Desktopif](https://chrome.google.com/webstore/detail/desktop-for-instagram/nlhjgcligpbnjphflfdbmabbmjidnmek)
5. [Skimagram](https://skimagram.com/)
6. [Username Search](https://www.idcrawl.com/username)
----

### Facebook Search Tools
1. [Intelligence X](https://intelx.io/tools?tab=facebook)
2. [Who posted what?](https://whopostedwhat.com/)
3. [searchisback](https://searchisback.com/)
4. [Find my Facebook ID](https://findmyfbid.in/)
5. [Fb-search.com](https://fb-search.com/)
6. [Facebook Search](https://sowdust.github.io/fb-search/)
7. [Download FB Album](https://chrome.google.com/webstore/detail/downalbum/cgjnhhjpfcdhbhlcmmjppicjmgfkppok)
8. [Imageye](https://chrome.google.com/webstore/detail/imageye-image-downloader/agionbommeaifngbhincahgmoflcikhm)
9. [new-fb-search.md](https://gist.github.com/nemec/2ba8afa589032f20e2d6509512381114)
10. [Graph Tips](https://graph.tips/beta/)
11. [CyberChef](https://gchq.github.io/CyberChef/)
12. [The New Facebook Graph Search Part 1](https://webcache.googleusercontent.com/search?q=cache:XxDih5v6kNsJ:https://osintcurio.us/2019/08/22/the-new-facebook-graph-search-part-1/+&cd=1&hl=en&ct=clnk&gl=us)
13. [The New Facebook Graph Search Part 2](https://webcache.googleusercontent.com/search?q=cache:ITEeCVYFgLUJ:https://osintcurio.us/2019/08/22/the-new-facebook-graph-search-part-2/+&cd=2&hl=en&ct=clnk&gl=us)
14. [social-searcher.com](https://www.social-searcher.com/)
15. [SocialMediaMineR: A Social Media Search and Analytic Tool version 0.4 from CRAN](https://rdrr.io/cran/SocialMediaMineR/)
16. [Floridaresidentsdirectory.com](https://www.floridaresidentsdirectory.com/)
----

### Snap Chat
1. [Snap Map](https://map.snapchat.com)
2. [Snapdex](https://www.snapdex.com/)
----

### People Search Engines
1. [Data Removal Workbook](https://inteltechniques.com/data/workbook.pdf)
2. [www.spokeo.com](https://www.spokeo.com/)
3. [Online Background Checks](https://www.beenverified.com/)
4. [PeekYou](https://www.peekyou.com/)
5. [Thats them (Age, lat/long,)](https://thatsthem.com/)
6. [White Pages](https://www.whitepages.com/)
7. [Tools](https://intelx.io/tools?tab=person)
8. [FamilyTree](https://www.familytreenow.com/)
9. [Yellowbook](https://www.yellowbook.com/)
10. [peoplesearchnow](http://www.peoplesearchnow.com/)
11. [Search Social Media Profiles](https://www.peoplelooker.com/)
12. [Background Checks and Public Records Search](https://www.truthfinder.com/)
13. [fastpeoplesearch (relatives, cell, age, past address, akas)](https://www.fastpeoplesearch.com/)
14. [voterrecords.com/voters](https://voterrecords.com/voters/)
15. [Social Searcher](https://www.social-searcher.com/)
16. [www.mylife.com/site/wsfy.view](https://www.mylife.com/site/wsfy.view)
17. [User Name Search](https://intelx.io/tools?tab=username)
18. [AmIUnique](https://amiunique.org/)
19. [Clustrmaps](https://clustrmaps.com/)
20. [Privacy and Security Podcast](https://inteltechniques.com/podcast.html)
21. [IDCrawl](https://www.idcrawl.com/)
22. [Unmask.com](https://unmask.com/)
----

### Video Tools
1. [Geo Search Tool](http://youtube.github.io/geo-search-tool/search.html)
2. [SaveFrom.net helper](http://en.savefrom.net/savefrom-helper-for-google-chrome.php)
3. [Social Searcher](https://www.social-searcher.com/)
4. [LiveLeak.com](http://www.liveleak.com/index.php)
5. [File2HD.com](http://file2hd.com/)
6. [Tools](https://intelx.io/tools?tab=youtube)
7. [Download Save Youtube](https://ytoffline.net/en1/)
8. [Search for Yotube video comment](https://ytcomment.kmcat.uk/)
----

### Police Watchdog Sites
1. [Cop Block](https://www.copblock.org/)
2. [MuckRock](https://www.muckrock.com/)
3. [Plainviewproject.org](https://www.plainviewproject.org/)
----

### Twitch and Discord
1. [Twitch](https://www.twitch.tv/directory/game/The%20Search/videos/all)
2. [https://www.twitch.tv/](https://www.twitch.tv/)
3. [Search for a Twitch channel - SullyGnome](https://sullygnome.com/channelsearch)
4. [All Twitch Streamers Search](https://twitchstats.net/allstreamers)
5. [DiscordHub](https://discordhub.com/user/search)
6. [Search Discord Servers](https://disboard.org/search)
7. [Discord](https://discordapp.com/)
----

### Documents Metadata Extract or Search
1. [Magic File Tool](https://intelx.io/tools?tab=filetool)
2. [Document Search](https://intelx.io/tools?tab=file)
----

### Open Source Prison Searches
1. [Florida Prison Search](http://www.dc.state.fl.us/offendersearch/)
2. [Federal Inmate Locator](https://www.bop.gov/inmateloc/)
3. [National Sex Offender](https://www.nsopw.gov/en/Search/Results)
----

### Court Records
1. [Polk Court Records](https://pro.polkcountyclerk.net/PRO/Home/PublicLogin)
2. [Florida Probation Search](http://www.dc.state.fl.us/OffenderSearch/Search.aspx)
3. [Polk County Ordinances](https://library.municode.com/fl/polk_county/codes/code_of_ordinances)
4. [Search Polk County Ordinance](https://apps.polk-county.net/ordinances/)
----

### Report Abuse
1. [Removing Photo/Video Facebook](https://www.facebook.com/help/1561472897490627/?helpref=hc_fnav)
2. [Hacked/Fake Facebook Accounts](https://www.facebook.com/help/1216349518398524/?helpref=hc_fnav)
3. [Reporting Abuse on Instagram](https://help.instagram.com/192435014247952)
4. [Report Child Sexual Exploitation](https://report.cybertip.org/)
5. [Report Abuse Snapchat](https://support.snapchat.com/en-US/i-need-help)
6. [Report Abuse on FB Messenger](https://www.facebook.com/help/messenger-app/1165699260192280/?helpref=hc_fnav&bc[0]=691363354276356)
7. [Report Abuse to Discord](https://support.discord.com/hc/en-us/articles/360000291932-How-to-Properly-Report-Issues-to-Trust-Safety)
8. [Report Abuse - Houseparty](https://houseparty.com/faq/)
9. [Report Abuse on WhatsApp](https://faq.whatsapp.com/21197244/)
10. [Report to Yee](https://yee.chat/community)
11. [Report Cyberbullying](https://cyberbullying.org/report)
12. [Report Abuse on Google Hangout/Products](https://support.google.com/families/contact/report_child_grooming)
13. [Report Abuse on YUBO](https://safety.yubo.live/safetytools)
14. [Report Abuse YouTube](https://www.youtube.com/reportabuse)
15. [Report Abuse on Twitch](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=en_US)
16. [Report Abuse on Steam](https://help.steampowered.com/en/faqs/view/4F8E-AE41-B6B1-F836)
----

### Reporting in Florida
1. [FDLE](https://www.fdle.state.fl.us/FCCC/Report-a-Computer-Crime.aspx)
2. [FBI Internet Crime Complaint Center (IC3)](https://www.ic3.gov/default.aspx)
3. [National Center for Missing Exploited Children CyberTipline](https://www.missingkids.org/gethelpnow/cybertipline)
----

### Open Source Maps for Time
1. [Dual Maps](http://data.mapchannels.com/mm/dual2/map.htm)
2. [Geo Search Tool](http://youtube.github.io/geo-search-tool/search.html)
3. [Snap Map](https://map.snapchat.com)
4. [Create a Map](https://batchgeo.com/)
5. [tweetmap](https://www.omnisci.com/demos/tweetmap)
6. [Lat/Long Search](https://intelx.io/tools?tab=location)
7. [Mapillary](https://mapillary.com)
8. [Trendsmap](https://www.trendsmap.com/)
9. [what3words](https://what3words.com/clip.apples.leap)
10. [Clustrmaps](https://clustrmaps.com/)
11. [Tweepsmap](https://tweepsmap.com/dash/)
12. [Google Maps](https://www.google.com/maps)
----

### Phone Number Searches
1. [usphonebook](https://www.usphonebook.com/)
2. [PeopleByName Directory - Search People By Phone Number](https://www.peoplebyname.com/callerid/)
3. [Truecaller.com](https://www.truecaller.com)
4. [Tools - Intelligence X](https://intelx.io/tools?tab=telephone)
5. [WhoCallsMe](https://whocallsme.com/)
6. [TrapCall](https://www.trapcall.com)
7. [Reveal Name](https://www.revealname.com/)
8. [Phone Carrier Check](https://phonecarriercheck.com/)
9. [Thats Them](https://thatsthem.com/reverse-phone-lookup)
10. [Spydialer (Reverse Lookup)](https://www.spydialer.com/)
11. [Reverse Phone Lookup](https://www.idcrawl.com/phone)
12. [https://unmask.com/phone/](https://unmask.com/phone/)
----

### Classified and Auction Sites
1. [Buy and sell used stuff in the United States](http://us.letgo.com/en)
2. [OfferUp](https://offerup.com/)
3. [craigslist > sites](https://www.craigslist.org/about/sites#US)
4. [https://www.facebook.com/marketplace/](https://www.facebook.com/marketplace/)
5. [Bunz.com](https://bunz.com/)
6. [Mercari](https://www.mercari.com/)
7. [Double List](https://doublelist.com/login/)
----

### Twitter Tools
1. [Tools](https://intelx.io/tools?tab=twitter)
2. [Twitter Search](https://twitter.com/search-home)
3. [All My Tweets - View all your tweets on one page.](https://allmytweets.net/connect/)
4. [Mentionmapp Analytics](http://mentionmapp.com/)
5. [Sign in // Twitonomy](https://www.twitonomy.com/dashboard.php)
6. [Followerwonk](https://followerwonk.com/)
7. [geosocial footprint](http://geosocialfootprint.com/)
8. [App.teachingprivacy.org](http://app.teachingprivacy.org/)
9. [Analyze Twitter Followers](https://followerwonk.com/analyze/)
10. [Tweetdeck](https://tweetdeck.twitter.com/#)
11. [Spoonbill](https://spoonbill.io/data/)
12. [Twitterfall](https://twitterfall.com/)
13. [Twitter Advanced Search](https://twitter.com/search-advanced)
14. [Social Searcher](https://www.social-searcher.com/)
15. [www.uvrx.com/social.html](http://www.uvrx.com/social.html)
16. [www.omnisci.com/demos/tweetmap](https://www.omnisci.com/demos/tweetmap)
17. [Tweetdeck](https://tweetdeck.twitter.com/)
18. [Twitter Trending Hashtags and Topics](https://www.trendsmap.com/)
19. [Tweepsmap](https://tweepsmap.com/dash/)
20. [IDCrawl](https://www.idcrawl.com/)
----

### IP Address Search
1. [GeoIP2 Database Demo](https://www.maxmind.com/en/geoip-demo)
2. [IP2Location](https://www.ip2location.com)
3. [Tools - Intelligence X](https://intelx.io/tools?tab=ip)
4. [Tools](https://intelx.io/tools?tab=geoip)
5. [IP-Adress](https://www.ip-adress.com/)
----

### Neighborhood Social Media Apps/Sites
1. [Nextdoor](https://nextdoor.com/)
2. [Neighbors App by Ring](https://shop.ring.com/pages/neighbors)
----

### YouTube
1. [YouTube Metadata](https://intelx.io/tools?tab=youtube)
2. [YouTube Video Download](http://en.savefrom.net/savefrom-helper-for-google-chrome.php)
3. [YouTube Downloader](http://file2hd.com/)
4. [Download Save Youtube](https://ytoffline.net/en1/)
5. [Youtube Comment Search](https://ytoffline.net/en1/)
6. [https://socialblade.com/youtube/top/trending/top-500-channels-1-day/most-subscribed](https://socialblade.com/youtube/top/trending/top-500-channels-1-day/most-subscribed)
7. [Trending](https://www.youtube.com/feed/trending)
8. [IDCrawl](https://www.idcrawl.com/)
----

### Top Apps & Websites
1. [Data AI](https://www.appannie.com/en/apps/ios/top/)
2. [Google Trends](https://trends.google.com/trends/?geo=US)
----

### Human Trafficking Sites
1. [bedpage](https://www.bedpage.com/)
2. [Yesbackpage.com](https://www.yesbackpage.com/)
3. [cityxguide.photo](https://cityxguide.photo/)
4. [Backpage.co](https://backpage.co/)
5. [https://www.adultlook.com/l/usa](https://www.adultlook.com/l/usa)
6. [Escorts Massages and Independent Adult Call Girls](https://www.escortdirectory.com/)
7. [Tryst.link](https://tryst.link/)
8. [Backpage-escorts.net](https://backpage-escorts.net/)
9. [Ibackpage.com](https://www.ibackpage.com/)
10. [How to Find People on Onlyfans - Scamfish - A Consumer Protection Publication - SocialCatfish.com](https://socialcatfish.com/scamfish/how-to-find-out-if-someone-has-an-onlyfans-account/)
----

### Password Managers
1. [KeePassXC Password Manager](https://keepassxc.org/)
2. [KeePassXC Browser](https://addons.mozilla.org/firefox/addon/keepassxc-browser/)
3. [Strongbox – Own Your Secrets](https://strongboxsafe.com/)
4. [Bitwarden](https://bitwarden.com/)
----

### Information Sites
1. [WikiLeaks](https://wikileaks.org/)
2. [LiveLeak](https://www.liveleak.com/)
3. [Osintcurio](https://osintcurio.us/)
4. [OSINT Links](https://osint.link/)
5. [Privacy and Security Podcast](https://inteltechniques.com/podcast.html)
----

### Meme Boards
1. [9Gag](https://9gag.com/)
2. [4chan](https://4chan.org)
3. [http://8ch.net/](http://8ch.net/)
----

### Subpoena for All Websites
1. [SEARCH |   ISP List](https://search.org/resources/isp-list/)
2. [Facebook](https://www.facebook.com/records/login/)
3. [Google | Law Enforcement Request System](https://support.google.com/legal-investigations/contact/LERS)
4. [OfferUp](http://records@offerup.com)
5. [Uber](http:// lert@uber.com)
6. [Discord](https://webcache.googleusercontent.com/search?q=cache:Xn366cRmiBMJ:https://www.search.org/resources/isp-list/+&cd=1&hl=en&ct=clnk&gl=us)
7. [IP address](https://www.maxmind.com/en/geoip-demo)
8. [Snap Chat Exigent](https://lawenforcement.snapchat.com/en-US/emergency)
9. [Instagram and Facebook Exigent](https://www.facebook.com/records/login/)
10. [Apple Exigent](https://www.apple.com/legal/privacy/le-emergencyrequest.pdf)
----

### Livestream Feeds
1. [https://www.facebook.com/watch/](https://www.facebook.com/watch/)
----

### Images
1. [Google Images](https://www.google.com/imghp)
2. [Y-Vladimir/SmartDeblur](https://github.com/Y-Vladimir/SmartDeblur)
3. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
4. [Stolen Camera Finder](http://www.stolencamerafinder.com/)
5. [Tools - Intelligence X](https://intelx.io/tools?tab=image)
6. [TinEye](https://tineye.com/)
7. [FotoForensics](https://fotoforensics.com/)
8. [Magic File](https://intelx.io/tools?tab=filetool)
----

### Rental Cars
1. [Turo car](https://turo.com/)
2. [Enterprise Receipts](https://www.enterprise.com/car_rental/ticketReceiptRequest.do)
3. [Hertz Request a Receipt](https://www.hertz.com/rentacar/receipts/request-receipts.do)
4. [Budget Receipts](https://www.budget.com/en/reservation/get-e-receipt)
5. [Avis Receipt](https://www.avis.com/en/reservation/get-e-receipt)
6. [Sixt Invoice](http://car-rental.sixt.com/php/customerservice/invcopy)
7. [Dollar Receipts](http://www.dollar.com/reservations/receipt.aspx)
8. [Thrifty Receipts](https://www.thrifty.com/Reservations/OnlineReceipts.aspx)
9. [Advantage Rental](https://receipts.advantage.com/HTMLClient/)
10. [National Rental Car](https://www.htallc.com/tollpass/invoice/search?agency=NationalCar)
11. [Alamo Rent A Car](https://legacy.alamo.com/en_US/car-rental/receipts.html)
----

### Location Based Apps
1. [Life360](https://www.life360.com/)
2. [Google Location](https://myaccount.google.com/intro/dashboard)
3. [iCloud](https://www.icloud.com/)
4. [Hulahoop](https://hulahoop.appheroes.com.tr/)
5. [Family Locator](https://www.family-locator.com/)
----

### Email
1. [Tools - Intelligence X](https://intelx.io/tools?tab=emailvalidate)
2. [LeekPeek](https://leakpeek.com)
----

### Dating Sites
1. [MocoSpace](https://www.mocospace.com/html/profile-search_results.jsp)
2. [Pof](https://www.pof.com/)
3. [Badoo](https://badoo.com/)
4. [Tagged](https://www.tagged.com/)
5. [Yubo](https://yubo.live/en)
6. [SKOUT](https://www.skout.com/)
7. [Grindr](https://www.grindr.com/)
----

### Text Storage Sites
1. [Pastebin](https://pastebin.com/)
2. [Tools - Intelligence X](https://intelx.io/tools?tab=file)
3. [Scribd](http://scribd.com)
4. [Ghostbin.co](https://ghostbin.co/)
5. [https://pastebin.is/](https://pastebin.is/)
----

### LinkedIn
1. [Intelligence X](https://intelx.io/tools?tab=linkedin)
2. [SocialMediaMineR: A Social Media Search and Analytic Tool version 0.4 from CRAN](https://rdrr.io/cran/SocialMediaMineR/)
3. [IDCrawl](https://www.idcrawl.com/)
----

### Blogging Sites
1. [Trending](https://www.tumblr.com/explore/trending)
2. [Blog Search Engine](http://www.blogsearchengine.org/)
3. [Search blogspot](https://www.searchblogspot.com/)
----

### Website Research
1. [Similar Sites](https://www.similarsites.com/)
2. [Find a grave](https://www.findagrave.com/)
3. [carrot2](https://search.carrot2.org/#/web)
4. [Snopes](https://www.snopes.com/)
5. [Tools](https://intelx.io/tools?tab=analytics)
6. [Intelligence X](https://intelx.io/tools)
7. [Public Records Search](https://aleph.occrp.org/)
8. [Internet of Insecurity](https://internetofinsecurity.com/)
9. [SimilarWeb](https://www.similarweb.com/)
10. [https://unmask.com/address/](https://unmask.com/address/)
----

### White Hat
1. [Shodan](https://www.shodan.io/)
----

### Tik Tok
1. [Trending Videos on TikTok](https://www.tiktok.com/trending/?lang=en)
2. [Discover](https://www.tiktok.com/discover?lang=en)
3. [Tik Tok Online Viewer](https://ttonlineviewer.com/)
4. [Tik Tok Video Downloader](https://vidnice.com/download/)
5. [IDCrawl](https://www.idcrawl.com/)
6. [IDCrawl](https://www.idcrawl.com/)
7. [IDCrawl](https://www.idcrawl.com/)
8. [IDCrawl](https://www.idcrawl.com/)
9. [IDCrawl](https://www.idcrawl.com/)
10. [IDCrawl](https://www.idcrawl.com/)
11. [IDCrawl](http://idcrawl.com)
12. [IDCrawl](https://www.idcrawl.com/)
----

### Social Discovery Apps
1. [YUBO LEO Guide](https://safety.yubo.live/law)
2. [Digital Identity Verifier](https://www.yoti.com/)
3. [Hoop.photo](https://hoop.photo/terms/)
4. [Wink.social](https://wink.social/)
5. [YOLO](https://onyolo.com/)
6. [YOLO Contact](https://onyolo.com/privacy)
----

### RSS Feeds
1. [Google Alerts](https://www.google.com/alerts)
----

### Private and Secure Emails
1. [ProtonMail](https://protonmail.com)
2. [Tutanota](https://tutanota.com)
3. [33mail](https://33mail.com)
4. [AnonAddy](https://anonaddy.com)
5. [Thunderbird — Make Email Easier. — Thunderbird](https://www.thunderbird.net/en-US/)
6. [Mail.edison.tech](https://mail.edison.tech/)
----

### Scanner Apps and Links
1. [Broadcastify.com](https://www.broadcastify.com/)
2. [RadioReference.com](https://www.radioreference.com/)
3. [Interceptradio.com](https://interceptradio.com/)
4. [The DXZone](https://dxzone.com/)
5. [Repeaterbook.com](https://repeaterbook.com/)
6. [Scanner Radio: Listen to live police and fire radio](https://scannerradio.app/index.html)
----

### Vehicle Searches
1. [Decode This](https://www.decodethis.com/)
2. [How to Quickly Decode Your VIN](https://www.edmunds.com/how-to/how-to-quickly-decode-your-vin.html)
3. [Vehicle Identification Number](https://vehicleidentificationnumber.com/)
4. [Free VIN number decoder for any cars](https://www.vindecoder.net/)
5. [CARFAX for Police](https://www.carfaxforpolice.com/)
6. [Berla.co](https://berla.co/)
----

### Teaching Tools
1. [Kahoot](https://kahoot.com/schools-u/)
----

### Free Alerts for Spoof Calls
1. [Security Apps to Protect You & Your Phone - AT&TAT&T homeMenu IconMenu IconMenu IconMenu IconMenu Iconclose iconsearch iconAT&T homesearch iconsearch iconsearch iconCart IconProfile icon](https://www.att.com/features/security-apps/)
2. [Mobile Security - Blocking Scams & Unwanted Calls | T-Mobile](https://www.t-mobile.com/customers/mobile-security)
3. [Block, restrict or allow voice calls](https://www.sprint.com/en/support/solutions/services/block-restrict-or-allow-voice-calls-using-my-sprint.html)
4. [Verizon Call Filter | Verizon Wireless](https://www.verizonwireless.com/solutions-and-services/call-filter/)
5. [Hiya.com](https://hiya.com/)
6. [YouMail Visual Voicemail](https://www.youmail.com/)
7. [Robokiller.com](https://www.robokiller.com/)
8. [Truecaller.com](https://www.truecaller.com/)
9. [Nomorobo Only internet VOIP free](https://www.nomorobo.com/)
10. [National Do Not Call Registry](https://www.donotcall.gov/)
11. [Voice](https://voice.google.com/about)
12. [Spoofcard](https://www.spoofcard.com/)
----

