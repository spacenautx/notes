### Check Username | Username enumeration
1. [@ WhatsMyName Web-enumerate usernames across  websites](https://whatsmyname.app/)
2. [@ NameChk](http://namechk.com/)
3. [@ Namevine](https://namevine.com/)
4. [@ Social Searcher](https://www.social-searcher.com/)
5. [@ Username Search](http://usersearch.org/)
6. [GitHub: WebBreacher/WhatsMyName: This repository has the unified data required to perform user enumeration on various websites. Content is in a JSON file and can easily be used in other projects.](https://github.com/WebBreacher/WhatsMyName?source=post_page---------------------------)
7. [Instant Username Search](https://instantusername.com/)
8. [KnowEm](http://knowem.com/)
9. [Namecheckup.com](https://namecheckup.com/)
10. [PGP Public Key Server](https://pgp.key-server.io/)
11. [@NAMINT plus enumeration](https://seintpl.github.io/NAMINT/)
----

### Email assumptions |  COMPANY
1. [Email Format OSIT](http://email-format.com)
2. [SalesRox – schlaue Tools für erfolgreiche Verkäufer](http://emailformat.de/)
3. [peepmail OSIT](http://samy.pl/peepmail)
4. [Toofr OSIT](http://toofr.com)
5. [Rapportive](http://rapportive.com/)
6. [ProspectLinked](http://prospectlinked.com/)
7. [Find email addresses in seconds Hunter (Email Hunter)](https://hunter.io/)
8. [Find verified company emails in seconds — Anymail finder](https://anymailfinder.com/)
9. [x_Blog: Domain Email OSINT](https://www.secjuice.com/domain-email-address-osint/)
----

### Email assumptions | PERSONAL
1. [@ Email Permutator](http://metricsparrow.com/toolkit/email-permutator/)
2. [Email Permutator Excel Tabelle](https://docs.google.com/spreadsheet/ccc?key=0AoW7aksoVU98dGNFSUtfeXg4akpNTWM0Z2pHWjJzZUE#gid=0)
3. [SalesRox – schlaue Tools für erfolgreiche Verkäufer](http://emailformat.de/)
4. [WhatsMyName Web](https://whatsmyname.app/)
5. [Namecombiner.com](https://namecombiner.com/)
----

### E-Mail assumptions | Lists of Webmail providers
1. [emailtester.de](http://www.emailtester.de/freemail/anbieter.php)
2. [Free Email Providers Guide](http://www.fepg.net/index.html)
3. [Übersicht](http://www.wolfgang-frank.eu/mailserver.php)
4. [Liste mit POP3- und SMTP-Servern](http://www.patshaping.de/hilfen_ta/pop3_smtp.htm)
5. [CEO Email](https://ceoemail.com/)
----

### Verify E-Mail
1. [Aboutip Überprüfen des Vorhandenseins einer E-Mail-Adresse](http://www.aboutip.de/mail.php)
2. [@ CentralOps Free online network tools](https://centralops.net/co/)
3. [@ Gravatar Check Email](https://en.gravatar.com/site/check/)
4. [Email Address Verifier](https://tools.verifyemailaddress.io/)
5. [Email Verifier](https://hunter.io/email-verifier)
6. [Free e-Mail Validator Tool OSIT](http://e-mailvalidator.com)
7. [MailTester OSIT](http://mailtester.com/testmail.php)
8. [Referenz für SMTP Fehler](https://support.google.com/a/answer/3726730?hl=de)
9. [Verify Email Address Online](http://verify-email.org/)
10. [Verify Email Address with Proofy (needs Login)](https://proofy.io)
11. [Google Finder](https://tools.epieos.com/google-account.php)
----

### Check Email | Email2web | Email2Social
1. [Simple Email Reputation](https://emailrep.io/)
2. [DarkSearch Search email on Darkweb](https://darksearch.io/)
3. [Searchmy.bio Email in Instagram bio](https://www.searchmy.bio/)
4. [truepeoplesearch.com Search by Name, shows Email](https://www.truepeoplesearch.com/)
5. [Email usage finder](https://tools.epieos.com/holehe.php)
----

### Email Header
1. [@ Email Header Analyzer](http://www.mxtoolbox.com/EmailHeaders.aspx)
2. [@ Trace Email Online](https://dnschecker.org/email-header-analyzer.php)
3. [Email Trace OSIT](http://www.ip-adress.com/trace_email/)
4. [SpamCop Reveal the full, unmodified email](http://www.spamcop.net/fom-serve/cache/19.html)
5. [Trace Email Address Source](http://whatismyipaddress.com/trace-email)
6. [xBlog: E-Mail-Header lesen und verstehen](https://th-h.de/net/usenet/faqs/headerfaq/)
7. [xBlog: How to Get Email Headers – A Guide from MxToolBox](https://mxtoolbox.com/Public/Content/EmailHeaders/)
8. [xBlog: How to Read and Analyze the Email Header Fields and Information aboutSPF, DKIM, SpamAssassin](https://www.arclab.com/en/kb/email/how-to-read-and-analyze-the-email-header-fields-spf-dkim.html)
9. [xBlog: What Can You Find in an Email Header?](https://www.howtogeek.com/108205/htg-explains-what-can-you-find-in-an-email-header/)
10. [xBlog: Email spoofing best practices -](https://docs.rackspace.com/support/how-to/email-spoofing-best-practices/)
----

### Reset Password | can also function as Email Verification
1. [Twitter](https://twitter.com/account/begin_password_reset)
2. [Facebook](https://www.facebook.com/login/identify?ctx=recover)
3. [LinkedIn](https://www.linkedin.com/uas/request-password-reset?trk=help-feature-launcher)
4. [Pinterest](https://www.pinterest.de/password/reset/)
5. [Gravatar](https://en.gravatar.com/site/check/)
6. [Yahoo](https://login.yahoo.com/account/challenge/username?authMechanism=secondary&authCredentialsType=cookies&yid=&done=https%253A%252F%252Fwww.yahoo.com&s=QQ--&c=eyJ2IjoyLCJjIjp0cnVlfQ--~QKnw44O2LgW1_928.AAG1ljpa9DtBIFoJDoAQ0F6UdY6VOZG4SpnaI11iYMXOwBeQAel0XxrYOAehQZ83GCPmILEIfLC15A4L59v2yKXgPrDO4CyEb7DClEMujxwpIz1BeZzkuv1_OsgbzdIb45JSjP519E0lh2T3gsJEGtQ7vlzVkphv2CGp_x_AHr2dBs2p8INaxOhGANOIs0a603bBdBZsYTxweukMRqpoarkJaYaPJGOH.HwzF2Txjo-&crumb=kZreXjkHJtV)
7. [VK](https://vk.com/restore)
8. [Instagram](https://www.instagram.com/accounts/password/reset/)
9. [Microsoft](https://account.live.com/ResetPassword.aspx?)
10. [Ebay](https://fyp.ebay.de/EnterUserInfo?)
11. [Web.de](https://passwort.web.de/passwordrecovery/)
12. [YouTube](https://www.youtube.com/account_recovery)
13. [GMX](https://passwort.gmx.net/passwordrecovery)
14. [Skype](https://a.login.skype.com/diagnostics)
15. [Ello.co](https://ello.co/forgot-password)
16. [Dropbox](https://www.dropbox.com/forgot)
17. [Github](https://github.com/password_reset)
----

### E-Mail to Profile + Maltego Social Links
1. [Manycontacts.com/en/mail-check](https://www.manycontacts.com/en/mail-check)
2. [Reverse Genie Email Search](http://reversegenie.com/email.php)
3. [Lullar Com](http://com.lullar.com/)
4. [Whois AMPed!](http://www.whoismind.com/)
5. [SecurityTrails](https://securitytrails.com/dns-trails)
----

### E-Mail Search
1. [Api.trumail.io](https://api.trumail.io/v2/lookups/json?email=)
2. [Spytox Email Search](https://www.spytox.com/email-search/)
3. [https://pipl.com/search/?q=](https://pipl.com/search/?q=)
4. [Intelx.io](https://intelx.io/?s=)
----

### Direct to Voice Messaging
1. [Ringless Voicemail Messaging App](https://www.slydial.com/)
----

### Telephone Directories
1. [telefonbuch.com](http://www.telefonbuch.com/de)
2. [InfoBel International](http://www.infobel.com/en/world/)
3. [11880.com (ehemals klicktel)](https://www.11880.com/)
4. [Das Telefonbuch >> Mehr als Adressen und Telefonnummern!](http://www.dastelefonbuch.de/)
5. [Das Örtliche Telefonbuch mit Telefonnummern, Telefonauskunft mit Stadtplan](http://www.dasoertliche.de/)
6. [Namendb.com/ Telefonbuch Handelsregistereinträge Ahnenforschung](http://namendb.com/)
7. [Telefonbuch für Österreich - Adressen & Telefonnummern | HEROLD](https://www.herold.at/telefonbuch/)
----

### Dialling and ZIP Code | Info zu Vorwahlen und PLZ |
1. [Telefonvorwahlen: Suche nach Vorwahlen in Deutschland](http://www.telefonvorwahlen.net/de/)
2. [Vorwahl.de :: Nationale & Internationale Telefonvorwahlen / Vorwahlen](http://www.vorwahl.de/)
3. [2-Letter, 3-Letter, Country Codes for All Countries in the World](http://www.worldatlas.com/aatlas/ctycodes.htm)
4. [Handyvorwahlen für alle Länder weltweit](http://www.telespiegel.de/vorwahl/handyvorwahl-ausland.php)
5. [Postleitzahl finden](http://www.postleitzahl-finden.de)
6. [Wtng.info](http://wtng.info/)
----

### Yellow Pages | Gelbe Seiten | Firmensuche | Wer Liefert Was
1. [„Wer liefert was“](https://www.wlw.de/)
2. [Branchenbuch Deutschland](http://branchenbuch.meinestadt.de/)
3. [Branchenbuch für Deutschland](http://www.yellowmap.de/)
4. [Cylex-Branchenbuch Deutschland](https://web2.cylex.de/)
5. [dialo.de Startseite](https://www.dialo.de/)
6. [Gelbe Seiten](http://www.gelbeseiten.de/)
7. [International White & Yellow Pages](http://www.wayp.com/)
8. [Telefonbuch](https://www.goyellow.de/)
9. [User-Verifikation](http://www.wogibtes.info/)
10. [WaWoSu.de](http://www.waswosuchen.de/)
11. [Willkommen bei Hotfrog](http://www.hotfrog.de/)
----

### Inverse Phone Search | Telefonnummer Rückwärtssuche |
1. [Anrufer.INFO](https://anrufer.info/)
2. [Rückwärtssuche über Telefonnummern >> Das Telefonbuch](http://www.dastelefonbuch.de/R%C3%BCckw%C3%A4rts-Suche)
3. [Klicktel ist jetzt 11880.com: Rückwärtssuche, Inverssuche, Telefonnummer suchen](https://www.11880.com/rueckwaertssuche)
4. [GoYellow Rückwärtssuche](https://www.goyellow.de/stichwortverzeichnis/rueckwaertssuche)
5. [Praxistipps.chip.de](https://praxistipps.chip.de/rueckwaertssuche-von-faxnummern-die-besten-tipps_28842)
6. [Anrufercheck.de](https://anrufercheck.de/)
7. [GitHub - sundowndev/PhoneInfoga: Advanced information gathering & OSINT tool for phone numbers.](https://github.com/sundowndev/PhoneInfoga?source=post_page---------------------------)
8. [NumberVille](https://numberville.com/)
----

### Carrier Lookup
1. [Free Carrier Lookup](https://freecarrierlookup.com/)
2. [carrierlookup.com/](https://www.carrierlookup.com/)
3. [Carrier Lookup: Identify & Organize Numbers by Carrier](https://www.textmagic.com/free-tools/carrier-lookup)
4. [FastBackgroundCheck via site operator google](http://fastbackgroundcheck.com/)
5. [Hlrlookup.com](https://www.hlrlookup.com/)
----

