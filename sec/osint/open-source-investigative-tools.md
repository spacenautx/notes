### Social Search and Monitoring Tools
1. [SAMDesk](http://app.samdesk.io/)
2. [Dataminr](https://app.dataminr.com/login)
3. [Crowdtangle](https://apps.crowdtangle.com/nytimes)
----

### Facebook
1. [Facebook Signal](https://signal.fb.com/#/dashboard)
2. [Advanced Facebook ID Tool](https://inteltechniques.com/intel/osint/facebook.html)
3. [FB Livemap](https://www.facebook.com/livemap/)
4. [Facebook Graph Shortcuts](http://graph.tips/)
5. [peoplefindThor](http://peoplefindthor.dk)
6. [Facebook Page Archive](https://www.facebook.com/PAGENAME/timeline/YYYY/MM)
7. [Graph search tips](http://www.researchclinic.net/graph.html)
----

### YouTube
1. [YouTube data viewer](http://www.amnestyusa.org/citizenevidence/)
2. [Watch Frame by Frame](http://www.watchframebyframe.com/)
3. [Rotate video - replace xs with video code](http://deturl.com/rotate.asp?v=xxxxxxxxxx)
4. [YT Geo Search](http://youtube.github.io/geo-search-tool/search.html)
5. [Deturl](http://deturl.com/)
6. [Montage - Historical YouTube Search](https://montage.storyful.com/)
----

### Scraping
1. [Import.io scraper](https://app.import.io/dash)
2. [Python console](https://www.pythonanywhere.com)
3. [Scrapy](https://scrapy.org/)
----

### Twitter
1. [Tweetdeck](https://web.tweetdeck.com/)
2. [Twitter / Advanced Search](https://twitter.com/search-advanced)
3. [Google search Twitter lists](https://www.google.ie/search?q=site%3Atwitter.com+inurl%3Alists+niger&rlz=1CASMAE_enIE635IE635&oq=site%3Atwitter.com+inurl%3Alists+niger&aqs=chrome..69i57j69i58.13185j0j1&sourceid=chrome&es_sm=0&ie=UTF-8#q=site:twitter.com+inurl:lists)
4. [List Copy](http://projects.noahliebman.net/listcopy/index.php)
5. [Download Twitter Videos](http://www.downloadtwittervideo.com/)
6. [twXplorer](https://twxplorer.knightlab.com/)
7. [TweetBeaver](http://tweetbeaver.com)
8. [First Tweet](https://discover.twitter.com/first-tweet)
9. [Search Operators](https://twitter.com/search-home#)
----

### Outreach and finding people
1. [Infobel](http://www.infobel.com/en/world/)
2. [Spokeo](http://www.spokeo.com/)
3. [Hunter](http://hunter.io)
4. [People Finder](https://pipl.com/)
5. [Research Clinic - Online Sleuthing Tips](http://researchclinic.net)
----

### Metadata
1. [Metapicz](http://metapicz.com/#landing)
2. [Jeffrey's Exif Viewer](http://exif.regex.info/exif.cgi)
3. [VerExif](http://www.verexif.com/en/)
4. [EXIF Data Viewer](http://exifdata.com/)
5. [izitru](https://www.izitru.com/)
----

### Extensions (install and synch to Chrome profile)
1. [RevEye Reverse Image Search](https://chrome.google.com/webstore/detail/reveye-reverse-image-sear/keaaclcjhehbbapnphnmpiklalfhelgf?hl=en)
2. [Wayback Machine](https://chrome.google.com/webstore/detail/wayback-machine/fpnmgdkabkmnadcjpehmlllkndpkmiak)
3. [First Draft News CheckList](https://chrome.google.com/webstore/detail/firstdraftnewscheck/japockpeaaanknlkhagilkgcledilbfk)
4. [Session Buddy](https://chrome.google.com/webstore/detail/session-buddy/edacconmaakjimmfgnblocblbcdcpbko?hl=en)
5. [Google Translate](https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb)
6. [INVID](http://www.invid-project.eu/tools-and-services/invid-verification-plugin/?utm_content=buffereda10&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
7. [SAMDesk](https://chrome.google.com/webstore/detail/sam/ohbloeckkmohmmiphcgebjlhchacjaib?hl=en)
8. [Storyful Multisearch](https://chrome.google.com/webstore/detail/storyful-multisearch/hkglibabhninbjmaccpajiakojeacnaf?hl=en)
----

### Video download
1. [Twitter Downloader](http://www.downloadtwittervideo.com/)
2. [YouTube, Facebook Downloader](http://en.savefrom.net/)
3. [Instagram Downloader](http://www.downloadvideosfrom.com/Instagram.php)
4. [Download YouTube, Twitch.Tv, YOUKU, Tudou](https://keepvid.com/)
5. [Periscope Downloader](http://downloadperiscopevideos.com/)
6. [Download Facebook, Twitter, Vimeo, Ustream](http://savevideo.me/)
----

### Maps
1. [Google Maps](http://maps.google.com/)
2. [Google Earth](https://www.google.com/earth/)
3. [www.bing.com/maps/](http://www.bing.com/maps/)
4. [Wikimapia](http://wikimapia.org/#lat=46.587385&lon=1.8136916&z=2&l=0&m=b)
5. [Yandex](https://yandex.ru/maps)
6. [SunCalc](http://suncalc.org/)
7. [Global Gazzetteer](http://www.fallingrain.com/world/index.html)
8. [Power Grid](https://nadoloni.com/power.html)
9. [OpenRailwayMap](http://www.openrailwaymap.org/)
10. [OpenStreetMap](http://www.openstreetmap.org/)
11. [Map convert](http://andrew.hedges.name/experiments/convert_lat_long/)
12. [UNITAR](http://www.unitar.org/unosat/)
----

### Satellite
1. [Terraserver](https://www.terraserver.com/)
2. [Planet Explorer](https://www.planet.com/explorer)
3. [DigitalGlobe - Search & Discovery](https://discover.digitalglobe.com/)
----

### Monitoring
1. [Trendsmap](https://www.trendsmap.com/)
2. [Broadcastify](https://www.broadcastify.com/listen/)
3. [Scanner audio](https://www.radioreference.com/)
----

### Maritime and Aviation
1. [Maritime Connector](http://maritime-connector.com/ships/)
2. [Marine Traffic](http://www.marinetraffic.com/)
3. [VesselFinder](https://www.vesselfinder.com/)
4. [Flightradar24](http://www.flightradar24.com/51.47,7.14/7)
5. [FlightAware](http://uk.flightaware.com/)
6. [Live ATC](http://www.liveatc.net/)
7. [Bureau International des Containers](https://www.bic-code.org/)
----

### Other tools and reading
1. [Google Advanced Search](https://www.google.com/advanced_search)
2. [Wolfram Alpha: Knowledge Engine](http://www.wolframalpha.com/)
3. [Trint - Transcription](https://trint.com/)
4. [Database of Hotel Room Pics](https://traffickcam.com/)
5. [Verification Handbook](http://verificationhandbook.com)
6. [First Draft News](http://firstdraftnews.com)
7. [Global Investigation Journalists Tipsheets](https://gijc2017.org/tipsheets/)
8. [OSINT Framework](http://osintframework.com/)
----

