### Über diese Seite
1. [@SebMeineck](https://twitter.com/sebmeineck)
2. [Kontakt](https://sebastianmeineck.wordpress.com/kontakt/)
3. [Newsletter abonnieren](https://newsletter.sebmeineck.de/home)
----

### Datenbanken
1. [AlternativeTo: database for alternatives](https://alternativeto.net/)
2. [Lumen: Content Removal Request Database](https://lumendatabase.org/)
3. [VirusTotal malware library](https://www.virustotal.com/gui/home/upload)
4. [Sourcewatch: Interessenverbände abchecken](https://www.sourcewatch.org)
5. [DWDS: Wortschatz-Suche](https://www.dwds.de/d/ressources#wortverlauf)
6. [ADL: Hate Symbols Database](https://www.adl.org/hate-symbols)
7. [tatortrechts: Rechte Straftaten in Deutschland](https://tatortrechts.de/)
8. [ddosecrets: Distributed Denial of Secrets](https://ddosecrets.com/wiki/Distributed_Denial_of_Secrets)
9. [Mobilsicher: App-Check](https://appcheck.mobilsicher.de/)
10. [PolitPro - Politik, News & Wahltrends](https://politpro.eu/de)
----

### Suchmaschinen
1. [Source Code Search](https://publicwww.com/)
2. [Tor: Ahmia Search](https://ahmia.fi/)
3. [Torwhois.com](https://torwhois.com/)
4. [Tor: Dargle](http://www.dargle.net/about)
5. [Millionshort: Search excluding poupular sites](https://millionshort.com/)
6. [Playphrase.me: movie quote search](https://playphrase.me/#/search)
7. [Blockpath: Bitcoin search](https://blockpath.com/)
----

### Google
1. [Google Scholar](https://scholar.google.de/)
2. [Google Fact Check Explorer](https://toolbox.google.com/factcheck/explorer)
3. [Google Hacking Exploit Database](https://www.exploit-db.com/google-hacking-database/)
4. [Profilr: Dorks for job profiles](https://profilr.co/)
5. [xeuledoc: Google Docs OSINT](https://github.com/Malfrats/xeuledoc)
6. [Google Programmable Search Engine](https://programmablesearchengine.google.com/intl/de_de/about/)
7. [Intelx Google-Dorks](https://intelx.io/dorks)
8. [Nützliche Operatoren für die Google-Suche](https://www.vice.com/de/article/9k895y/google-suche-elf-profi-tricks-bessere-suchergebnisse-operatoren-maximalfunktion)
----

### Wissenschaft
1. [AWMF: Medizinische Leitlinien](https://www.awmf.org/leitlinien/)
2. [Pubmed: Klinische Studien](https://pubmed.ncbi.nlm.nih.gov/)
3. [MedienDoktor: Portal für Medizin- & Science-Journalismus](http://www.medien-doktor.de/gesundheit/)
4. [WolframAlpha](https://www.wolframalpha.com/)
5. [BASE scientific search](https://www.base-search.net)
6. [Unpaywall: find scientific papers for free](https://chrome.google.com/webstore/detail/unpaywall/iplffkdpngmdjhlpjmppncnlhomiipha)
7. [z-lib](https://z-lib.org/)
8. [Our World in Data](https://ourworldindata.org/)
9. [Libgen.gs](https://libgen.gs/)
----

### KARTEN-TOOLS
1. [Google Earth Pro](https://www.google.de/earth/download/gep/agree.html)
2. [Sentinel Hub](https://www.sentinel-hub.com/)
3. [Planet Labs](https://www.planet.com/products/planet-imagery/)
4. [Mapillary: Alternate Google Street View](https://www.mapillary.com/app/)
5. [Map Switcher](https://chrome.google.com/webstore/detail/map-switcher/fanpjcbgdinjeknjikpfnldfpnnpkelb)
6. [Overpass turbo: OSM search](https://overpass-turbo.eu/)
7. [Soar.earth](https://soar.earth/?)
8. [newspaper map](https://newspapermap.com/)
9. [Shademap: Shadow Simulator](https://shademap.app/)
----

### Social Media
1. [CrowdTangle: Who posted a link on social](https://apps.crowdtangle.com/chrome-extension)
2. [Nindo: Social Media Charts](https://nindo.de/)
3. [Bellingcat: TikTok investigation](https://www.bellingcat.com/resources/2020/05/25/investigate-tiktok-like-a-pro/)
4. [TubeOffline: Universal Video Downloader](https://www.tubeoffline.com/)
5. [Subredditstats.com](https://subredditstats.com/)
6. [4chan search](https://4chansearch.com/)
7. [Snapchat Map](https://map.snapchat.com/)
8. [TwitchTracker](https://twitchtracker.com/)
9. [Skypli: search Skype users](https://www.skypli.com/)
10. [SteamID: account database](https://steamid.uk/)
11. [DISBOARD: Discord-Server-Suche](https://disboard.org/de)
----

### Facebook
1. [Facebook Video Downloader](https://www.fbdown.net/index.php)
2. [Facebook Political Ads Search](https://ad.watch/)
3. [Graph Tips: Facebook Advanced Search](https://graph.tips/beta/)
4. [Whopostedwhat: Fb search + time indication](https://whopostedwhat.com/)
5. [Osint Support: FB + LinkedIn Email Lookup](https://osint.support/chrome-extensions/)
----

### YouTube
1. [YouTube Comment Scraper](http://ytcomments.klostermann.ca/)
2. [Hadzy: YouTube comment search](https://hadzy.com/)
3. [YouTube Channel Crawler](https://www.channelcrawler.com/)
4. [Savesubs: download subtitles from YouTube](https://savesubs.com)
5. [downsub: subtitle downloader](https://downsub.com/)
6. [Social Blade: YouTube Statistics](https://socialblade.com/)
7. [AddOn: Display video transcript](https://chrome.google.com/webstore/detail/invideo-for-youtube/iacbjlffnpbhgkgknabhkfmlcpdcigab?hl=de)
8. [YouTube_Tool: Comment Scraper](https://github.com/nlitsme/youtube_tool)
9. [Youtube-dl: Downloader](https://youtube-dl.org/)
----

### Instagram
1. [Gramly Instagram Interaction Analysis](https://gramfly.com/)
2. [Spatulah: Instagram comment scraper](https://spatulah.com/)
3. [Likeometer: Instagram Influencer Ranking](https://likeometer.co/weltweit/alle/)
4. [Instadp: Instagram downloader](https://instadp.org/#r)
5. [StoriesIG: Instagram Stories Downloader](https://storiesig.com/)
6. [Picuki: Instagram search](https://www.picuki.com/)
7. [Instadp: Profile pic in full resolution](https://www.instadp.com/)
8. [Dumpor: Instagram search](https://dumpor.com/)
9. [Datalux/Osintgram](https://github.com/Datalux/Osintgram)
10. [Sterra](https://github.com/novitae/sterraxcyl)
----

### Telegram
1. [Telegram OSINT List by ItsMeCall911](https://github.com/ItIsMeCall911/Awesome-Telegram-OSINT)
2. [Telegram OSINT list by sector035](https://sector035.nl/articles/2021-05)
3. [xtea: Telegram Search Engine](https://xtea.io/ts_en.html#gsc.tab=0)
4. [Telegram Nearby Map](https://github.com/tejado/telegram-nearby-map)
----

### Reddit
1. [redditlist](http://redditlist.com)
2. [Subredditstats.com](https://subredditstats.com/)
3. [Map of Reddit](https://anvaka.github.io/map-of-reddit/?x=255000&y=381000&z=1846873.3752154177)
----

### Twitter
1. [Twitter Advanced Search](https://twitter.com/search-advanced)
2. [Twitter: Who said it first](https://ctrlq.org/first/)
3. [TweetBeaver Twitter account analysis](https://tweetbeaver.com/)
4. [Allmytweets: All tweets from any user](https://www.allmytweets.net/connect/)
5. [Twitter Account Analysis](https://accountanalysis.app/)
6. [Twitter Search Operator Tutorial](https://freshvanroot.com/blog/2019/twitter-search-guide-by-luca/)
7. [TWINT: scraping](https://github.com/twintproject/twint)
8. [Nitter: Browse Twitter without Login](https://nitter.net/)
9. [TweepDiff](https://tweepdiff.com)
----

### Wikipedia
1. [Who Wrote That: check Wikipedia changes fast](https://chrome.google.com/webstore/detail/who-wrote-that/ekkbnedhfelfaidbpaedaecjiokkionn?hl=de)
2. [Wikipedia xTools](https://xtools.wmflabs.org/authorship)
3. [Edit Counter - XTools](https://xtools.wmflabs.org/ec?sections=all)
4. [Bundesedit.de](http://bundesedit.de/)
----

### Behörden-Recherche
1. [Frag den Staat](https://fragdenstaat.de/)
2. [AskTheEU.org](https://www.asktheeu.org)
3. [Dokukratie: Antworten auf Kleine Anfragen finden](https://www.dokukratie.de/)
4. [Bundestagwatch.de](https://bundestagwatch.de/)
5. [abgeordnetenwatch.de](https://www.abgeordnetenwatch.de/)
6. [Open Parliament TV](https://de.openparliament.tv/)
7. [Open Discourse](https://opendiscourse.de/)
8. [Govdata: Deutschsprachige Verwaltungsdaten](https://www.govdata.de/)
9. [Gesetze im Internet](http://www.gesetze-im-internet.de)
10. [Verfassungsschutzberichte durchsuchen](https://verfassungsschutzberichte.de/)
11. [Bund.dev: API-Portal des Bundes](https://bund.dev/)
12. [CourtListener](https://www.courtlistener.com/)
13. [bundesAPI](https://github.com/bundesAPI/deutschland)
----

### Firmen-Recherche
1. [OpenCorporates: find companies](https://opencorporates.com/)
2. [northdata.de: Firmendaten](https://www.northdata.de/)
3. [Handelsregister.de](https://www.handelsregister.de/rp_web/welcome.xhtml)
4. [unternehmensregister.de](https://www.unternehmensregister.de/ureg/)
5. [occrp: companies, files, names](https://data.occrp.org/)
6. [Bundesanzeiger: gratis Geschäftsberichte](https://www.bundesanzeiger.de/ebanzwww/wexsservlet)
7. [LEI: company search](https://search.gleif.org/#/search/)
8. [Official company registers](http://www.rba.co.uk/sources/registers.htm)
9. [theorg: Organizations](https://theorg.com/organizations)
10. [DSGVO Bußgeld Datenbank](https://www.dsgvo-portal.de/dsgvo-bussgeld-datenbank.php)
----

### Website-Recherche
1. [OSINT.SH Website Toolkit](https://osint.sh/)
2. [IP Checker](https://ipinfo.info/html/ip_checker.php)
3. [Don't FingerPrint Me](https://chrome.google.com/webstore/detail/dont-fingerprint-me/nhbedikkbkakbjipijipejfojanppbfg)
4. [domainbigdata whois analysis](https://domainbigdata.com/)
5. [Spyonweb: find IP, Google-ID, more](http://spyonweb.com/)
6. [Reverse IP Lookup: Find Hosts Sharing an IP](https://hackertarget.com/reverse-ip-lookup/)
7. [ViewDNS.info](https://viewdns.info/)
8. [SecurityTrails: subdomains, DNS records, more](https://securitytrails.com/)
9. [DNS History Search](https://completedns.com/)
10. [Openlinkprofiler: find backlinks](https://www.openlinkprofiler.org/)
11. [MoonSearch website analysis](http://moonsearch.com)
12. [Whoisology](https://whoisology.com/)
13. [URL Fuzzer: find hidden directories](https://pentest-tools.com/website-vulnerability-scanning/discover-hidden-directories-and-files)
14. [Hackertarget.com: extract links from any website](https://hackertarget.com/extract-links/)
15. [urlscan.io: scan a website before opening](https://urlscan.io/)
16. [Wappalyzer: Website technology lookup](https://www.wappalyzer.com/)
17. [Visual Site Mapper: Websites überfliegen](http://www.visualsitemapper.com/)
18. [DNSlytics: Google Analytics Reverse Search](https://dnslytics.com/reverse-analytics)
19. [Blacklight: website privacy inspector](https://themarkup.org/blacklight)
20. [WhereGoes: URL Redirect Checker](https://wheregoes.com/trace/)
21. [Domain Name Disputes](https://www.dndisputes.com/)
22. [iphistory.ch: historische IP-Adressen](https://www.iphistory.ch/de/)
23. [archive.is: go archive any website](http://archive.is)
24. [Wayback Machine: Speed up Archive.org search](https://chrome.google.com/webstore/detail/wayback-machine/nbkhjdgdpjdebcniehkeigmdfeiokegh/related)
25. [Blacklight: Tracking Checker](https://themarkup.org/blacklight)
26. [Website-Investigator](https://abhijithb200.github.io/investigator/)
27. [Host Spider](https://github.com/h3x0crypt/HostSpider)
28. [CRT: Certificate Search](https://crt.sh/)
----

### ACCOUNT-SUCHE
1. [Sherlock: search accounts by nickname](https://github.com/sherlock-project/sherlock)
2. [Maigret: search accounts by username](https://github.com/soxoj/maigret)
3. [WhatsMyName: Search accounts by nickname](https://whatsmyname.app/)
4. [Webresolver: find skype users by email](https://webresolver.nl/tools/skype_to_email)
5. [Spiderfoot: OSINT Search Engine](https://www.spiderfoot.net/)
6. [BreachDirectory](https://breachdirectory.org/)
7. [Intelx.io: search leaks, more](https://intelx.io/)
8. [Have I been pwned: password leak library](https://haveibeenpwned.com)
9. [Snusbase Database Search Engine](https://snusbase.com/)
10. [Ekultek/WhatBreach](https://github.com/Ekultek/WhatBreach)
----

### E-Mail-Tools
1. [Holehe: search accounts by mail](https://github.com/megadose/holehe)
2. [Emailrep: Check mail adresses](https://emailrep.io/)
3. [Burner Mail](https://burnermail.io)
4. [Epieos: Email Lookup](https://tools.epieos.com/email.php)
5. [Hunter.io: verify e-mail contacts](https://hunter.io/verify)
6. [Hunter.io: find e-mail contacts](https://hunter.io/)
----

### Browser Tools
1. [donotlink.it: create anti-SEO-backlinks](https://donotlink.it)
2. [t1p: sichere Shortlinks](https://t1p.de/)
3. [RSS-Bridge](https://rss.nixnet.services/)
4. [Cyber Chef](https://www.gchq.gov.uk/news/cyberchef-cyber-swiss-army-knife)
5. [OneTab: Instant tab organizer](https://chrome.google.com/webstore/detail/onetab/chphlpgkkbolifaimnlloiipkdnihall?hl=de)
----

### Text-Tools
1. [PDF24.org: Online PDF Toolkit](https://de.pdf24.org/)
2. [OCRmyPDF](https://github.com/jbarlow83/OCRmyPDF)
3. [PDFStudioViewer: bulk search PDFs](https://www.qoppa.com/pdfstudioviewer/download/)
4. [pd3f – PDF Text Extractor](https://pd3f.com/)
5. [Draftable: compare documents](https://draftable.com/)
6. [Diffchecker: compare almost identical texts](https://www.diffchecker.com/)
----

### Bild-Recherche
1. [Guide To Using Reverse Image Search](https://www.bellingcat.com/resources/how-tos/2019/12/26/guide-to-using-reverse-image-search-for-investigations/)
2. [Exif Viewer](https://chrome.google.com/webstore/detail/exif-viewer/mmbhfeiddhndihdjeganjggkmjapkffm/related?hl=de)
3. [Reveal: Image Verification Assistant](http://reveal-mklab.iti.gr/reveal/index.html)
4. [Add-on: Search by Image](https://github.com/dessant/search-by-image)
5. [TinEye Reverse Image Search](https://www.tineye.com)
6. [Yandex Reverse Image Search](https://yandex.com/images/)
7. [Bulk Media Downloader: Detect & copy media](https://chrome.google.com/webstore/detail/bulk-media-downloader/ehfdcgbfcboceiclmjaofdannmjdeaoi/related?hl=de)
8. [Beta.flim.ai: movie screenshot search](https://beta.flim.ai/)
9. [Fake news debunker by InVID & WeVerify](https://chrome.google.com/webstore/detail/fake-news-debunker-by-inv/mhccpoafgdgbhnjfhkcmgknndkeenfhe)
----

### Bild-Bearbeitung
1. [Photopea: Online-Foto-Editor](https://www.photopea.com/)
2. [Pixlr: Online-Foto-Editor](https://pixlr.com/de/e/)
3. [Exif Purge](http://www.exifpurge.com/)
4. [Image Scrubber: Blur Images Offline](https://everestpipkin.github.io/image-scrubber/)
5. [EZGif: Online Gif-& Video-Toolkit](https://ezgif.com/)
6. [InPixio: Image Background Remover](https://www.inpixio.com/remove-background/)
7. [Image Colorizer](https://imagecolorizer.com/colorize.html)
8. [Artbreeder: Gesichter generieren](https://www.artbreeder.com/)
9. [Removal AI: Hintergrund -Entferner](https://removal.ai/de/)
10. [Cleanup.pictures](https://cleanup.pictures/)
11. [Giphy Gif Maker](https://giphy.com/create/gifmaker)
12. [PurePNG: CC0 transparent library](https://purepng.com/)
13. [Undraw: open-source illustrations](https://undraw.co/)
14. [CC search](https://ccsearch.creativecommons.org/)
15. [‎iOS: Anonymous Camera](https://apps.apple.com/us/app/anonymous-camera/id1504102584)
16. [AI Image Upscaler](https://imgupscaler.com/)
----

### Visualisierungen
1. [KnightLab Storytelling Tools](https://knightlab.northwestern.edu/projects/)
2. [Datawrapper: Create maps for free](https://app.datawrapper.de/create/map)
3. [Mediacloud: analyze worldwide media coverage](https://tools.mediacloud.org/#/home)
4. [SankeyMATIC: Diagramm-Baukasten](http://sankeymatic.com/build/)
5. [Headliner: Audio to Video viz](https://www.headliner.app/)
6. [Mayxgda: Wortwolken](https://www.maxqda.de/hilfe-mx20/visual-tools/wortwolke)
7. [wortwolken.com](https://www.wortwolken.com/)
8. [diagrams.net: Flowchart Maker](https://www.diagrams.net/)
----

### Dokumentieren
1. [FireShot: Atomshot-Alternative](https://chrome.google.com/webstore/detail/take-webpage-screenshots/mcbpblocgmgfnpjjppndjkmgjaogfceg?hl=de)
2. [Link Gopher: extract links from website](https://chrome.google.com/webstore/detail/link-gopher/bpjdkodgnbfalgghnbeggfbfjpcfamkf/related)
3. [Chrono Download Manager](https://chrome.google.com/webstore/detail/chrono-download-manager/mciiogijehkdemklbdcbfkefimifhecn)
4. [Atomshot: Screen- & Scrollshots + timestamp](https://chrome.google.com/webstore/detail/atomshot/pjfmllbdhacnbnjgenkeflcmklpkjdcn)
5. [Copy selected links](https://chrome.google.com/webstore/detail/copy-selected-links/kddpiojgkjnpmgiegglncafdpnigcbij/related?hl=de)
6. [CopyTables: Tabellen von Websites extrahieren](https://chrome.google.com/webstore/detail/copytables/ekdpkppgmlalfkphpibadldikjimijon)
7. [DataMiner.io: easy scraping](https://data-miner.io/)
8. [Distill.io: website change tracker](https://distill.io/)
9. [HTTrack Website Copier](https://www.httrack.com/)
10. [ICIJ: Datashare](https://datashare.icij.org/)
----

### Krypto-Tools
1. [E-Mail: ProtonMail](https://protonmail.com/de/)
2. [VPN: MullvadVPN](https://mullvad.net/de/)
3. [Cloudspeicher: Tresorit](https://tresorit.com/de)
4. [Messenger: Signal](https://signal.org/de/)
5. [TextEditor: Cryptpad](https://cryptpad.fr/)
6. [Videocalls: Jitsi](https://meet.jit.si/)
7. [OS: Tails](https://tails.boum.org/)
8. [Passwort-Manager: KeePassXC](https://keepassxc.org/)
9. [Encryption: Veracrypt (Tutorial)](https://gabrielebrandhuber.at/externe-festplatte-verschluesseln-veracrypt/)
10. [Datei-Transfer Tresorit Send](https://send.tresorit.com/)
11. [Datei-Transfer: OnionShare](https://onionshare.org/)
12. [Nixnet: collection](https://nixnet.services/)
----

### Expert:innen finden
1. [Speakerinnen.org](https://speakerinnen.org/de)
2. [AlgoEthik Expertinnen-Liste](https://algorithmenethik.de/2019/03/08/keine-zukunft-ohne-frauen-mehr-sichtbarkeit-den-algorithmen-expertinnen/)
3. [Frauendomäne.at](https://www.frauendomaene.at/members/)
4. [Toolbox gegen Rechts](https://www.toolbox-gegen-rechts.de/)
5. [Vielfaltfinder](https://www.vielfaltfinder.de/)
----

### AUDIO-TOOLS
1. [Fyyd: Google für Audio](https://fyyd.de/)
2. [Spaactor: Google für Audio](https://www.spaactor.com/)
3. [Listen Notes: Google für Audio](https://www.listennotes.com/de/)
4. [Multritrack Editor im Browser: Beautiful Audio Editor](https://beautifulaudioeditor.appspot.com/app)
5. [Audio-Editor im Browser: Audiomass](https://audiomass.co/)
6. [Zencastr: podcast recording online](https://zencastr.com/)
7. [Radio Garden](http://radio.garden/)
----

### Tutorials
1. [Command Line for Reporters](https://github.com/AJVicens/command-line-for-reporters)
2. [Bildschirmaufnahmen erstellen](https://mailchi.mp/88aada1e1ffb/online-recherche-newsletter-no13-tatort-rechts-atomshot-hunchly)
----

### Weitere Sammlungen
1. [TraceLabs OSINT VM](https://www.tracelabs.org/initiatives/osint-vm)
2. [OSINT Cheatsheets](https://cheatsheet.haax.fr/open-source-intelligence-osint/)
3. [Insanely Huge Toolkit by Technisette](https://start.me/p/m6XQ08/osint)
4. [Bellingcat's Online Investigation Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/mobilebasic)
5. [SmallSEOtools](https://smallseotools.com/de/)
6. [Toolbox by Intelx.io](https://intelx.io/tools)
7. [social-media-hacker-list](https://github.com/MobileFirstLLC/social-media-hacker-list)
8. [Toddington Cheat Sheets](https://www.toddington.com/resources/cheat-sheets/)
9. [NYT: Anti-Doxxing Training Overview](https://docs.google.com/document/d/1AyDO5QDp9UUbHHGT3d9JpcTni94xpY66q657a8rDz4I/edit)
10. [First Draft Toolkit](https://start.me/p/wMv5b7/first-draft-advanced-toolkit)
11. [Verification Toolset](https://start.me/p/ZGAzN7/verification-toolset)
12. [V3nari Bookmarks](https://start.me/p/1kxyw9/v3nari-bookmarks)
13. [Nixintel's OSINT Resource List](https://start.me/p/rx6Qj8/nixintel-s-osint-resource-list)
----

### Medien-Analyse
1. [cOWIDplus-Viewer: Medieninteresse-Barometer](https://www.owid.de/plus/cowidplusviewer2020/)
2. [Media Cloud](https://mediacloud.org/)
3. [newspaper map](https://newspapermap.com/)
4. [Deutsches Zeitungsportal](https://www.deutsche-digitale-bibliothek.de/newspaper)
----

