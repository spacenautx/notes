### People Search
1. [192.com](https://www.192.com/)
2. [BritishPhoneBook.com](http://www.britishphonebook.com/)
3. [The Phone Book from BT](https://www.thephonebook.bt.com/person/)
4. [Individual Insolvency Register - Search](https://www.insolvencydirect.bis.gov.uk/eiir/IIRRegisterNameInput.asp?option=NAME&court=ALL)
5. [Search for a Trainee - Pharmaceutical Society of Northern Ireland](https://www.psni.org.uk/search-register/trainee-results/)
6. [CIMA - Find a CIMA Accountant](https://www.cimaglobal.com/About-us/Find-a-CIMA-Accountant/)
7. [PeopleLookup.co.uk - 41 Million Electoral Roll 
Records UK GB
United Kingdom England Scotland Wales Northern Ireland Great 
Britain
Electoral Role](http://www.peoplelookup.co.uk/dob/)
----

### Businesses
1. [Companies House](https://beta.companieshouse.gov.uk)
2. [yell.com](https://www.yell.com/)
3. [Pubs Galore - The UK pub guide](https://www.pubsgalore.co.uk/)
4. [FCA](https://register.fca.org.uk/)
5. [Company Check](https://companycheck.co.uk/)
6. [endole.co.uk](https://www.endole.co.uk/)
7. [UKAS :   Search Accredited Organisations](https://www.ukas.com/search-accredited-organisations/)
8. [Big Red Directory](https://www.bigreddirectory.com/)
9. [BizDb](https://www.bizdb.co.uk/)
10. [Yably – the place where ratings muster](https://yably.co.uk/)
11. [Food Safety Scores](https://www.eatible.co.uk/)
12. [Company Search, Company Credit Check, Director Search](https://www.companysearchesmadesimple.com/)
13. [Advanced Search - Monster.co.uk](https://www.monster.co.uk/jobs/advanced-search/?intcid=swoop_Hero_Find_Jobs_Advanced_Search)
14. [Business Search - Monster.co.uk](https://www.monster.co.uk/company/)
15. [Find a job](https://findajob.dwp.gov.uk/search)
16. [Buyer Search - Public Contracts Scotland](https://www.publiccontractsscotland.gov.uk/Search/search_Auth.aspx)
17. [Online medicines seller registry](https://medicine-seller-register.mhra.gov.uk/search-registry/)
18. [Get information about schools - GOV.UK](https://get-information-schools.service.gov.uk/?SelectedTab=Establishments)
19. [Scores on the Doors - Official Food Hygiene Ratings](https://www.scoresonthedoors.org.uk/)
20. [Stallfinder | Find an Event or Stallholder](https://www.stallfinder.com/)
21. [storageplusmovers.co.uk](https://www.storageplusmovers.co.uk/Search/GetCompaniesByPostcode)
22. [Listings Archive - Tradesmen Corner](https://www.tradesmencorner.co.uk/listings/?ls=)
23. [TrustMark Tradesmen](https://www.trustmark.org.uk/)
24. [Virtual Office Directory: England, Scotland, N Ireland, Wales](http://www.virtualofficedirectory.co.uk/)
25. [UK Pub Guide • whatpub.com](https://whatpub.com/)
26. [Find UK Accountant: A Directory of Accountants](https://www.find-uk-accountant.co.uk/)
27. [TendersDirect](https://www.tendersdirect.co.uk/)
28. [ABTA | Travel Advice & Holiday Information | UK Travel Industry](https://www.abta.com/)
29. [Ukdirectory.co.uk](https://www.ukdirectory.co.uk/)
30. [Extractives.company-information.service.gov.uk](https://extractives.company-information.service.gov.uk/)
31. [Search for published payment practice reports](https://check-payment-practices.service.gov.uk/search)
----

### Court Records
1. [Court Cases Results](https://www.thelawpages.com/court-cases/court-case-search.php?mode=1)
2. [Courts and Tribunals Judiciary](https://www.judiciary.uk/judgments/)
3. [Case tracker – Justice UK](https://casetracker.justice.gov.uk/search.jsp)
4. [supremecourt.uk](https://www.supremecourt.uk/current-cases/index.html)
5. [Causelist](http://causelist.org/)
----

### Planning Permissions
1. [Planning Registers Site Search](https://www.gov.uk/search-register-planning-decisions)
2. [Planning Appeals England & Wales](https://acp.planninginspectorate.gov.uk/CaseSearch.aspx)
3. [Planning Appeals Scotland](https://www.dpea.scotland.gov.uk/CaseSearch.aspx?T=2)
4. [Planning Permission | Mayor of London](https://apps.london.gov.uk/ldd/)
----

### REGISTRIES
1. [Amazon Baby Wishlist](https://www.amazon.co.uk/baby-reg/homepage/)
----

### VESSELS
1. [https://www.fqaregister.service.gov.uk/browse#tabs=0](https://www.fqaregister.service.gov.uk/browse#tabs=0)
----

### Address
1. [Scotland Landlord Search](https://www.landlordregistrationscotland.gov.uk/search/start)
2. [Land Registry - Property Search](https://eservices.landregistry.gov.uk/eservices/FindAProperty/view/QuickEnquiryInit.do)
3. [Land Registry - Map Enquiry](https://eservices.landregistry.gov.uk/eservices/FindAProperty/view/MapEnquiryInit.do)
4. [FixMyStreet](https://www.fixmystreet.com/)
----

### Main Public Records
1. [genesreunited.co.uk](https://www.genesreunited.co.uk/search)
2. [BMD Records from England & Wales](https://www.ancestry.co.uk/search/categories/34/)
3. [UK Government Data](https://data.gov.uk/search)
4. [Office for National Statistics](https://www.ons.gov.uk/search?q=)
5. [JNCC - Search](https://jncc.gov.uk/search?q=)
6. [Search For Notices - Public Contracts Scotland](https://www.publiccontractsscotland.gov.uk/search/search_mainpage.aspx)
7. [Search | Food Standards Agency](https://www.food.gov.uk/search?keywords=&fax=)
8. [Search | UK Sport](http://www.uksport.gov.uk/search?q=)
9. [Flood map for planning - GOV.UK](https://flood-map-for-planning.service.gov.uk/)
10. [Search for a trade mark](https://trademarks.ipo.gov.uk/ipo-tmtext)
11. [Find a register office - GOV.UK](https://www.gov.uk/register-offices)
12. [Prison finder - find a prison in England and Wales](http://www.justice.gov.uk/contacts/prison-finder)
13. [FreeCEN - UK Census Records (England, Scotland, Wales)](https://www.freecen.org.uk/)
14. [Non Conformist BMD Register Search](https://bmdregisters.co.uk/)
15. [Historical Directories](http://specialcollections.le.ac.uk/digital/collection/p16445coll4)
16. [The British Library](https://www.bl.uk/)
17. [Newspaper Index](https://one-name.org/modern-newspaper-index/)
18. [London's public transport history pictures](https://www.ltmuseum.co.uk/collections/collections-online/photographs)
19. [Legislation.gov.uk](http://www.legislation.gov.uk/)
20. [Press Complaints Commission >> Advanced Search](http://www.pcc.org.uk/advanced_search.html)
21. [Find an authority - WhatDoTheyKnow](https://www.whatdotheyknow.com/select_authority?query=)
22. [Rent Smart Wales - Public Register](https://www.rentsmart.gov.wales/en/check-register/)
----

### Birth Records
1. [The England & Wales birth records](https://search.findmypast.co.uk/search-world-Records/england-and-wales-births-1837-2006)
2. [England and Wales Birth Registration Index, 1837-2008](https://www.familysearch.org/search/collection/2285338?collectionNameFilter=true)
----

### Marriage Records
1. [Amazon Wedding Wishlist](https://www.amazon.co.uk/gp/registry/search.html?ie=UTF8&type=wedding)
----

### Education
1. [Find an Ofsted inspection report](https://reports.ofsted.gov.uk/)
2. [Inspection report search | Estyn](https://www.estyn.gov.wales/inspection/search)
3. [Nurseries UK](https://www.daynurseries.co.uk/)
4. [Directory of Open Access Repositories - v2.sherpa](http://v2.sherpa.ac.uk/opendoar/)
5. [Get information about schools - GOV.UK](https://get-information-schools.service.gov.uk/)
6. [The Student Room](https://www.thestudentroom.co.uk/)
7. [Teacher misconduct](https://www.gov.uk/government/collections/teacher-misconduct)
8. [NASUWT |  The Teachers' Union](https://www.nasuwt.org.uk/)
----

### Crime
1. [StAR - Stolen Asset Recovery Initiative - Corruption Cases](https://star.worldbank.org/corruption-cases/?db=All)
2. [Who are Britain’s jihadists?](https://www.bbc.co.uk/news/uk-32026985)
3. [Catch a Thief UK](https://www.catchathief.co.uk/)
4. [Research Map | What Works Centre for Crime Reduction](https://whatworks.college.police.uk/Research/Research-Map/Pages/Research-Map.aspx)
5. [Action Fraud](https://www.actionfraud.police.uk/)
6. [UK Burglary Hot Spots | MoneySuperMarket](https://www.moneysupermarket.com/home-insurance/burglary-hotspots/)
7. [Crime Map - police.uk](https://www.police.uk/)
8. [Most Wanted - CrimeStoppers](https://crimestoppers-uk.org/give-information/most-wanted)
9. [Stats and data | The Met](https://www.met.police.uk/sd/stats-and-data/)
10. [FCA Warnings](https://www.fca.org.uk/news/search-results?n_search_term=&start=1&np_category=warnings)
11. [Friends Against Scams](https://www.friendsagainstscams.org.uk/)
12. [CrimeStoppers - Most Wanted](https://crimestoppers-uk.org/give-information/most-wanted)
13. [Most Wanted - National Crime Agency](https://www.nationalcrimeagency.gov.uk/most-wanted-search)
14. [NHS Counter Fraud Authority](https://cfa.nhs.uk/)
15. [Data downloads | data.police.uk](https://data.police.uk/data/)
16. [Proscribed terrorist groups or organisations](https://www.gov.uk/government/publications/proscribed-terror-groups-or-organisations--2)
17. [Financial sanctions targets: list of all asset freeze targets](https://www.gov.uk/government/publications/financial-sanctions-consolidated-list-of-targets/consolidated-list-of-targets)
18. [Home - Serious Fraud Office](https://www.sfo.gov.uk/)
19. [Fraud news from the travel industry](https://www.profit.uk.com/travel-fraud-news/)
20. [BTP stations](http://www.btp.police.uk/contact_us/btp_stations.aspx)
21. [Missing Kids](https://www.missingpeople.org.uk/missingkids)
22. [Old Bailey Online - The Proceedings of the Old Bailey, 1674-1913](https://www.oldbaileyonline.org/)
23. [London homicide map 2019 • murdermap](https://www.murdermap.co.uk/london-homicide-map-2019/)
----

### Death Records
1. [Burial records & Cremation records](https://www.deceasedonline.com/servlet/GSDOSearch)
2. [Ireland Historical Will Search](https://apps.proni.gov.uk/WillsCalendar_IE/WillsSearch.aspx)
3. [Find a will | GOV.UK](https://probatesearch.service.gov.uk/#wills)
4. [Funeral Notices](https://funeral-notices.co.uk/national)
5. [www.rip.ie](https://rip.ie/)
----

### Adoption Records
1. [UK Birth Adoption Register](http://www.ukbirthadoptionregister.com/search.php)
----

### Vehicles
1. [askMID](https://ownvehicle.askmid.com/)
2. [Trade Car Checks & Vehicle Valuation Services](https://cazana.com/uk)
3. [Check if a vehicle is taxed and has an MOT](https://vehicleenquiry.service.gov.uk/)
4. [MOT History](https://www.check-mot.service.gov.uk/)
5. [Cars for sale in the UK - NewsNow Classifieds](https://www.newsnow.co.uk/classifieds/cars-vans-for-sale/)
6. [CAR CHECK](https://carcheck.app/vehicle/check)
7. [Caranalytics.co.uk](https://www.caranalytics.co.uk/)
8. [Freecarcheck.co.uk](https://www.freecarcheck.co.uk/)
9. [Check your Vehicle](https://tfl.gov.uk/modes/driving/check-your-vehicle/)
10. [Partial Number Plate Search](https://www.partialnumberplate.co.uk/)
----

### Military
1. [Forces Reunited](https://www.forcesreunited.co.uk/search/)
2. [Today's birthdays | Army Rumour Service](https://www.arrse.co.uk/community/members/?key=todays_birthdays)
----

### Classified Ads
1. [Gumtree](https://www.gumtree.com/)
2. [Free Classified Ads, Free Ads, UK Classifieds](https://www.ukclassifieds.co.uk/)
3. [Free Ads](https://www.freeads.co.uk/)
4. [Find Property, Jobs and Cars and much much more - NewsNow Classifieds](https://www.newsnow.co.uk/classifieds/)
5. [Loot - Free Classified Ads in the UK since 1985](https://loot.com/)
----

### Financial
1. [HMRC: Help](https://www.tax.service.gov.uk/information/help)
2. [Current list of deliberate tax defaulters - GOV.UK](https://www.gov.uk/government/publications/publishing-details-of-deliberate-tax-defaulters-pddd/current-list-of-deliberate-tax-defaulters)
3. [Sort Code Checker](http://www.fasterpayments.org.uk/consumers/sort-code-checker)
4. [Sort Code Checker](https://www.sortcodes.co.uk/checker.html)
5. [VAT lookup](http://www.vat-lookup.co.uk/)
6. [VAT (Value Added Tax) IDs - Numbers Search Lookup](https://vat-search.co.uk/)
----

### Forums
1. [Anime UK News Forums](https://forums.animeuknews.net/)
2. [Taxi Driver Online • Index page](https://www.taxi-driver.co.uk/phpBB2/index.php)
----

### Domains
1. [UKWA Home](https://www.webarchive.org.uk/ukwa/)
----

### Interactive Maps
1. [Cornwall Interactive Map](https://map.cornwall.gov.uk/website/ccmap/)
2. [Devon Interactive Map](http://map.devon.gov.uk/DCCViewer/)
3. [Surrey Interactive Map](http://surreymaps.surreycc.gov.uk/public/viewer.asp)
4. [London Interactive Map](https://maps.london.gov.uk/cim/index.html)
5. [Dartmoor Camping Map](https://www.dartmoor.gov.uk/about-us/about-us-maps/new-camping-map)
6. [Northern Ireland Interactive Map](https://dfcgis.maps.arcgis.com/apps/webappviewer/index.html?id=6887ca0873b446e39d2f82c80c8a9337)
7. [Islington Interactive Map](https://mapapp.islington.gov.uk/mapthatv3/Default.aspx)
8. [Manchester Interactive Map](https://www.gmwildlife.org.uk/mapapp/)
9. [Stockport Interactive Map](https://www.stockport.gov.uk/con29r-map)
10. [Ireland Interactive Map](https://apps2.spatialni.gov.uk/EduSocial/PRONIApplication/index.html)
11. [Air Monitoring](https://uk-air.defra.gov.uk/interactive-map)
12. [secret-bases.co.uk](https://www.secret-bases.co.uk/google-earth.htm)
13. [Who owns England? Land ownership map](http://map.whoownsengland.org/)
14. [Public Land Registry | Mayor of London](https://apps.london.gov.uk/public-land/)
15. [London Development Database (v2.01b)](https://apps.london.gov.uk/land-development-database/)
16. [London Workspaces](https://apps.london.gov.uk/workspaces/)
17. [London Schools Atlas](https://apps.london.gov.uk/schools/)
18. [London Rents Map](https://apps.london.gov.uk/private-rents/)
19. [Batch geocoding](https://www.doogal.co.uk/BatchGeocoding.php)
20. [UK phone codes](https://www.doogal.co.uk/UKPhoneCodes.php#google_vignette)
----

### Misc
1. [BBC Motion Gallery](https://www.gettyimages.co.uk/footage/bbcmotiongallery)
2. [Plant Database](https://www.rhs.org.uk/plants/search-form)
----

### Patents
1. [Intellectual Property Office](https://www.ipo.gov.uk/p-ipsum.htm)
2. [Intellectual Property Office - Results of past patent decisions](https://www.ipo.gov.uk/patent/p-decisionmaking/p-challenge/p-challenge-decision-results.htm)
----

