### Readme
1. [Be careful what you OSINT with – Key Findings](https://keyfindings.blog/2020/03/23/be-careful-what-you-osint-with/)
----

### Other Toolsets
1. [Set it on 🔥child -OSINT for Finding People](https://docs.google.com/spreadsheets/d/1JxBbMt4JvGr--G0Pkl3jP9VDTBunR2uD3_faZXDvhxc/edit#gid=603724104)
----

### Bazzel's OSINT Techniques
1. [Bazzell's OSINT Techniques Book](https://inteltechniques.com/book1.html)
2. [Book Resources](https://inteltechniques.com/osintbook/)
----

### Disposable Contact Information
1. [10 Minute Mail](https://10minutemail.com/)
2. [Guerrilla Mail](https://www.guerrillamail.com/)
3. [Burner Mail - Fast, Easy, Disposable Email Addresses](https://burnermail.io/)
4. [Firefox Extension -  Relay.firefox.com](https://relay.firefox.com/)
5. [Burner - Phone Number](https://www.burnerapp.com/)
----

