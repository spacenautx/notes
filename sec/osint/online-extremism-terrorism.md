### Go back to main dashboard
1. [Go Back (click here)](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
----

### 1. Safeguarding Anonymity & Privacy
1. [One click and you're out: UK makes it an offence to view terrorist propaganda even once • The Register](https://www.theregister.co.uk/AMP/2019/02/13/uk_counter_terror_act_royal_assent/?__twitter_impression=true)
2. [Is your VPN secure?](http://theconversation.com/is-your-vpn-secure-109130?utm_source=twitter&utm_medium=twitterbutton)
3. [Basic Opsec](https://medium.com/@JMartinMcFly/basic-opsec-for-the-uninitiated-77d0839f7bce)
4. [Browser Uniqueness & Fingerprinting](https://medium.com/@ravielakshmanan/web-browser-uniqueness-and-fingerprinting-7eac3c381805)
5. [Am I unique?](https://amiunique.org/)
6. [Privacy Tools](https://amiunique.org/tools)
7. [Digital Security Resources](https://medium.com/@mshelton/current-digital-security-resources-5c88ba40ce5c)
8. [Identity Protection - CIFAS](https://www.cifas.org.uk/services/identity-protection)
9. [Web Tracking: What You Should Know About Your Privacy Online](https://medium.freecodecamp.org/what-you-should-know-about-web-tracking-and-how-it-affects-your-online-privacy-42935355525)
10. [25 Best Chrome Extensions For Penetration Testing Can Keep You Out of Trouble](http://techblogcorner.com/2017/10/27/best-chrome-extensions-for-penetration-testing/)
11. [Why Do Some Websites Block VPNs?](https://www.howtogeek.com/403771/why-do-some-websites-block-vpns/)
12. [MyBrowserInfo](http://mybrowserinfo.com/)
----

### 2. Legal & Ethical Issues
1. [BPS - Ethics Guidelines for Internet-Med Research (2017)](https://www.bps.org.uk/news-and-policy/ethics-guidelines-internet-mediated-research-2017)
2. [Case study - use of deception](https://esrc.ukri.org/funding/guidance-for-applicants/research-ethics/ethics-case-studies/case-study-use-of-deception/)
3. [Core principles (ESRC)](https://esrc.ukri.org/funding/guidance-for-applicants/research-ethics/our-core-principles/)
4. [Ethics case studies](https://esrc.ukri.org/funding/guidance-for-applicants/research-ethics/ethics-case-studies/)
5. [Internet-mediated research](https://esrc.ukri.org/funding/guidance-for-applicants/research-ethics/frequently-raised-topics/internet-mediated-research/)
6. [Stanford - Internet Research Ethics](https://plato.stanford.edu/entries/ethics-internet-research/)
7. [Terrorism | The Crown Prosecution Service](https://www.cps.gov.uk/terrorism)
8. [Terrorism / Counterterrorism](https://www.hrw.org/topic/terrorism-counterterrorism)
9. [Bellingcat -  prevent trauma how-to guide](https://www.bellingcat.com/resources/how-tos/2018/10/18/prevent-identify-address-vicarious-trauma-conducting-open-source-investigations-middle-east/)
10. [VOX-Pol - Building Resilience for Terrorism Researchers](https://www.voxpol.eu/building-resilience-for-terrorism-researchers/)
----

### Violent Online Political Extremism (Research and Resources)
1. [Library - VOX - Pol](https://www.voxpol.eu/library/)
2. [Blog Archives - VOX - Pol](https://www.voxpol.eu/category/blog/)
3. [VOX-Pol Publications - VOX - Pol](https://www.voxpol.eu/publications/)
4. [War of keywords - how extremists are exploiting the internet and what to do about it](https://institute.global/sites/default/files/inline-files/IGC_War%20of%20Keywords_23.08.17_0.pdf)
5. [Third-Party Research - Tech Against Terrorism](https://www.techagainstterrorism.org/news/global/)
6. [GNET](https://gnet-research.org/)
7. [Researcher Resources - VOX - Pol](https://www.voxpol.eu/researcher-resources/)
----

### 4. Countering Violent Extremism (CVE)
1. [Masar](https://secure.masar.tech/login)
2. [Educate Against Hate](https://educateagainsthate.com/)
3. [ICCT - “That is what the terrorists want”: Media as amplifier or disrupter of violent extremist propaganda](https://icct.nl/publication/that-is-what-the-terrorists-want-media-as-amplifier-or-disrupter-of-violent-extremist-propaganda/)
4. [ICCT - A Brief History of Propaganda During Conflict](https://icct.nl/publication/a-brief-history-of-propaganda-during-conflict-a-lesson-for-counter-terrorism-strategic-communications/)
5. [ICCT - Countering Terrorist Narratives](https://icct.nl/publication/countering-terrorist-narratives/)
6. [ICCT - Making CVE Work: A Focused Approach Based on Process Disruption](https://icct.nl/publication/making-cve-work-a-focused-approach-based-on-process-disruption/)
7. [ICSVE - ISIS Defector Memes](http://www.icsve.org/our-work/memes/)
8. [Institute for Strategic Dialogue](https://www.isdglobal.org/)
9. [ISD - Education](https://www.isdglobal.org/programmes/education/)
10. [ISD - The Counter Narrative Handbook](http://www.isdglobal.org/wp-content/uploads/2016/06/Counter-narrative-Handbook_1.pdf)
11. [RAN Issue Paper - Counter Narratives & Alternative Narratives](https://ec.europa.eu/home-affairs/sites/homeaffairs/files/what-we-do/networks/radicalisation_awareness_network/ran-papers/docs/issue_paper_cn_oct2015_en.pdf)
12. [https://www.unodc.org/e4j/en/tertiary/counter-terrorism.html](https://www.unodc.org/e4j/en/tertiary/counter-terrorism.html)
----

### 5. CVE Training Courses
1. [Community led action to VE](http://www.start.umd.edu/training/community-led-action-response-violent-extremism)
2. [Countering VE Narratives](http://www.start.umd.edu/countering-violent-extremist-narratives)
3. [Mental Health & Education Approaches into CVE](http://www.start.umd.edu/training/integrating-mental-health-and-education-approaches-cve)
4. [Coalition Building, CVE & Community Resilience](http://www.start.umd.edu/coalition-building-countering-violent-extremism-and-community-resilience-0)
5. [Counter-Terrorism](https://www.unodc.org/e4j/en/tertiary/counter-terrorism.html)
----

### 6. IS Media System
1. [Daesh Propaganda, Before and After its Collapse | StratCom](https://stratcomcoe.org/daesh-propaganda-and-after-its-collapse)
2. [From Battlefront to Cyberspace: Demystifying the Islamic State’s Propaganda Machine – Combating Terrorism Center at West Point](https://ctc.usma.edu/battlefront-cyberspace-demystifying-islamic-states-propaganda-machine/)
3. [Encrypted Extremism - GW](https://extremism.gwu.edu/sites/g/files/zaxdzs2191/f/EncryptedExtremism.pdf)
4. [Terrorist Use of the Internet – ICTRP](https://www.ictrp.org/terrorist-use-of-the-internet/)
5. [IS Media System](https://www.ict.org.il/Article/2235/The_Media_System_of_the_Islamic_State-On_the_Road_to_Recovery#gsc.tab=0)
6. [Media Groups](https://ent.siteintelgroup.com/mediagroups.html)
7. [https://www.dhs.gov/sites/default/files/publications/15-271-IA%20-%20Media%20Arms%20-%20Maniglia_update_v11_accessible.pdf](https://www.dhs.gov/sites/default/files/publications/15-271-IA%20-%20Media%20Arms%20-%20Maniglia_update_v11_accessible.pdf)
8. [The Cloud Caliphate](https://ctc.usma.edu/the-cloud-caliphate-archiving-the-islamic-state-in-real-time)
----

### 7. Arabic names
1. [The Names of Jihad - Majalla Magazine](http://eng.majalla.com/2017/07/article55254103/the-names-of-jihad)
2. [Behind the Name: Arabic Names](https://www.behindthename.com/names/usage/arabic)
3. [Arabic name structure - Wikipedia](https://en.wikipedia.org/wiki/Arabic_name)
----

### 8. Logos & Symbols
1. [Emblems :: Jihad Intel](http://jihadintel.meforum.org/identifiers/20/emblems)
2. [Hate on Display™ Hate Symbols Database](https://www.adl.org/education-and-resources/resource-knowledge-base/hate-symbols)
3. [Images Archive – Combating Terrorism Center at West Point](https://ctc.usma.edu/militant-imagery-project/)
4. [https://www.dhs.gov/sites/default/files/publications/15-271-IA%20-%20Media%20Arms%20-%20Maniglia_update_v11_accessible.pdf](https://www.dhs.gov/sites/default/files/publications/15-271-IA%20-%20Media%20Arms%20-%20Maniglia_update_v11_accessible.pdf)
----

### 9. Tracing arms & ammunition
1. [Weapons of ISIS](http://www.conflictarm.com/reports/weapons-of-the-islamic-state/)
2. [SIPRI Arms Transfers Database](https://www.sipri.org/databases/armstransfers)
3. [A-Z Databases](http://www.natolibguides.info/az.php)
4. [The Weapons ID Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
5. [Search 3d models - sketchfab](https://sketchfab.com/search?sort_by=-pertinence&type=models)
6. [Arms embargoes](https://www.sipri.org/databases/embargoes)
7. [Identification and Support Cards](http://www.smallarmssurvey.org/weapons-and-markets/tools/id-cards.html)
8. [Ammunition-Tracing-Kit.pdf](http://www.smallarmssurvey.org/fileadmin/docs/D-Book-series/book-06-ATK/SAS-Ammunition-Tracing-Kit.pdf)
9. [Loading Mapping Arms Data (MAD) 1992-2014. Please wait as application loads.
                    Note: The graphics used in this visualization are not supported by older versions of 			Internet Explorer. It is best viewed using an up-to-date version of Chrome, Firefox or Internet Explorer (IE11).
		N.B. If you are using Internet Explorer, this must NOT be set to run in 'Compatibility Mode'! (Tools > Compatibility View Settings)](http://nisatapps.prio.org/armsglobe/index.php)
10. [Global Firearms Holdings](http://www.smallarmssurvey.org/weapons-and-markets/tools/global-firearms-holdings.html)
11. [UEMS tools](http://www.smallarmssurvey.org/weapons-and-markets/stockpiles/unplanned-explosions-at-munitions-sites/uems-tools.html)
12. [HotGunz Stolen Gun Search Results](https://www.hotgunz.com/search.php)
----

### 10. Identifying combatant affiliation
1. [How to Digitally Verify Combatant Affiliation in Middle East Conflicts - bellingcat](https://www.bellingcat.com/resources/how-tos/2018/07/09/digitally-verify-middle-east-conflicts/)
2. [Camopedia](http://camopedia.org/index.php?title=Main_Page)
3. [Camouflageindex.camouflagesociety.org/index-2.html](http://camouflageindex.camouflagesociety.org/index-2.html)
4. [Uniforminsignia.org](http://www.uniforminsignia.org/)
5. [List of comparative military ranks](https://en.wikipedia.org/wiki/List_of_comparative_military_ranks)
6. [All Badges site for collectors](http://allbadges.net/en)
----

### 11. CYBINT
1. [Bitdefender Threat Map](https://threatmap.bitdefender.com/)
2. [Deep Web Research and Discovery Resources 2018](http://whitepapers.virtualprivatelibrary.net/DeepWeb.pdf)
3. [Cryptography - intro, papers, tutorials & more](https://www.peerlyst.com/posts/a-curated-list-of-cryptography-papers-articles-tutorials-and-howtos-for-non-cryptographers-dawid-balut?utm_source=twitter&utm_medium=social&utm_content=peerlyst_post&utm_campaign=peerlyst_shared_post)
4. [MEMRI - Cyber & Jihad Lab](http://cjlab.memri.org/)
5. [Hacking for ISIS](https://fortunascorner.com/wp-content/uploads/2016/05/Flashpoint_HackingForISIS_April2016-1.pdf)
6. [Hacking database](https://www.zone-h.org/archive?hz=1)
7. [An Analysis of Islamic State Propaganda Distribution](https://go.flashpoint-intel.com/docs/an-analysis-of-islamic-state-propaganda-distribution)
8. [Best practices guide - Threat Intelligence & Incident Forensics](http://docs.apwg.org/sponsors_technical_papers/DomainTools-Guide-Cybercrime-Investigation.pdf)
9. [Operative Framework - tutorial](https://null-byte.wonderhowto.com/how-to/recon-research-person-organization-using-operative-framework-0176323/)
10. [how-to use buscador osint vm for conducting online investigations](https://null-byte.wonderhowto.com/how-to/use-buscador-osint-vm-for-conducting-online-investigations-0186611/)
----

### 12. ICTRP - Regulations & Policies
1. [Regulations & Policies](https://www.ictrp.org/regulations-and-policies/)
----

### 13. Gaming & Extremism
1. [Extremists’ use of gaming (adjacent) platforms ‒ Insights regarding primary and secondary prevention measures, August 2021](https://ec.europa.eu/home-affairs/networks/radicalisation-awareness-network-ran/publications/extremists-use-gaming-adjacent-platforms-insights-regarding-primary-and-secondary-prevention_en)
2. [The gamification of violent extremism & lessons for P/CVE, 2021](https://ec.europa.eu/home-affairs/networks/radicalisation-awareness-network-ran/publications/gamification-violent-extremism-lessons_en)
3. [Extremism and Gaming Research Network | Royal United Services Institutemenuplussearchuseruserchevron-rightplusminusplusminusplusminusplusminusplusminusplusminusplusminusplusminusplusminusclockchevron-rightclockchevron-rightquotequotelong-arrowfacebooktwitteryoutubelinkedin](https://rusi.org/explore-our-research/projects/extremism-and-gaming-research-network#publication-outputs)
4. [Gamers Who Hate: An Introduction to ISD’s Gaming and Extremism Series - ISD](https://www.isdglobal.org/isd-publications/gamers-who-hate-an-introduction-to-isds-gaming-and-extremism-series/)
5. [Gaming and Extremism: The Extreme Right on Twitch - ISD](https://www.isdglobal.org/isd-publications/gaming-and-extremism-the-extreme-right-on-twitch/)
6. [Gaming and Extremism: The Extreme Right on Discord - ISD](https://www.isdglobal.org/isd-publications/gaming-and-extremism-the-extreme-right-on-discord/)
7. [Gaming and Extremism: The Extreme Right on DLive - ISD](https://www.isdglobal.org/isd-publications/gaming-and-extremism-the-extreme-right-on-dlive/)
8. [Gaming and Extremism: The Extreme Right on Steam - ISD](https://www.isdglobal.org/isd-publications/gaming-and-extremism-the-extreme-right-on-steam/)
9. [Gaming and Extremism - Homeland Security Digital Library](https://www.hsdl.org/c/gaming-and-extremism/)
10. [Only Playing: Extreme-Right GamificationCrest ResearchSubmit search queryToggle menuClose menuOpen submenuOpen submenuBack to top](https://crestresearch.ac.uk/resources/only-playing-extreme-right-gamification/)
----

