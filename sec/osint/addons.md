### Addons - Archive
1. [Archive.org (Chrome)](https://docs.google.com/document/d/1uoQr5TPswmVFUyeQ4lhI2qoCAqw6XZU4cutDNfBMFps/edit)
2. [Go Back in Time (Chrome)](https://chrome.google.com/webstore/detail/go-back-in-time/hgdahcpipmgehmaaankiglanlgljlakj)
3. [Resurrect Pages (FF)](https://addons.mozilla.org/nl/firefox/addon/resurrect-pages/)
----

### Addons - Documents
1. [Unpaywall (Chrome)](https://chrome.google.com/webstore/detail/unpaywall/iplffkdpngmdjhlpjmppncnlhomiipha)
2. [Unpaywall (FF)](https://addons.mozilla.org/en-GB/firefox/addon/unpaywall/?src=search)
----

### Addons - Email
1. [Name2Email](https://chrome.google.com/webstore/detail/name2email-by-reply/mnbdclgaeiapdnhfpbfalfjfcjddfaii?hl=en)
----

### Addons - Corporate
1. [Ocean.io](https://chrome.google.com/webstore/detail/oceanio/gkocieepdpljclejnkijcpicoccgnkhd)
----

### Addons - PGP
1. [Mailvelope](https://www.mailvelope.com/en/)
----

### Addons - Image/video
1. [Exif viewer (FF)](https://addons.mozilla.org/en-US/firefox/addon/exif-viewer/)
2. [Google 'View Image' button (Chrome)](https://chrome.google.com/webstore/detail/google-search-view-image/hgngncnljacgakaiifjcgdnknaglfipo?hl=nl)
3. [Hover Zoom (Chrome)](https://chrome.google.com/webstore/detail/hover-zoom/nonjdcjchghhkdoolnlbekcfllmednbl?hl=en)
4. [NooBox (Chrome)](https://chrome.google.com/webstore/detail/noobox/kidibbfcblfbbafhnlanccjjdehoahep?hl=nl)
5. [Google 'View Image' button (FF)](https://addons.mozilla.org/en-US/firefox/addon/view-image/)
6. [Send to Exif Viewer (Chrome)](https://chrome.google.com/webstore/detail/send-to-exif-viewer/gogiienhpamfmodmlnhdljokkjiapfck?hl=nl)
7. [Video DownloadHelper (Chrome)](https://chrome.google.com/webstore/detail/video-downloadhelper/lmjnegcaeklhafolokijcfjliaokphfk?hl=nl)
8. [Video DownloadHelper (FF)](https://addons.mozilla.org/nl/firefox/addon/video-downloadhelper/?src=search)
9. [Video verification InVid (FF and Chrome)](http://www.invid-project.eu/tools-and-services/invid-verification-plugin/)
10. [Who Stole My Pictures? (FF)](https://addons.mozilla.org/en-US/firefox/addon/who-stole-my-pictures/)
----

### Addons - Maps
1. [Mapswitcher (Chrome)](https://chrome.google.com/webstore/detail/map-switcher/fanpjcbgdinjeknjikpfnldfpnnpkelb)
----

### Addons - OSINT
1. [Auto Refresh Page (Chrome)](https://chrome.google.com/webstore/detail/auto-refresh/ifooldnmmcmlbdennkpdnlnbgbmfalko?hl=nl)
2. [Data Scraper (Chrome)](https://chrome.google.com/webstore/detail/data-scraper-easy-web-scr/nndknepjnldbdbepjfgmncbggmopgden?hl=nl)
3. [Distill (Chrome)](https://chrome.google.com/webstore/detail/distill-web-monitor/inlikjemeeknofckkjolnjbpehgadgge?hl=en-US)
4. [Distill (FF)](https://addons.mozilla.org/en-US/firefox/addon/distill-web-monitor-ff/)
5. [Fireshot (Chrome)](https://chrome.google.com/webstore/detail/take-webpage-screenshots/mcbpblocgmgfnpjjppndjkmgjaogfceg?hl=nl)
6. [Fireshot (FF)](https://addons.mozilla.org/nl/firefox/addon/fireshot/?src=search)
7. [Google translate (Chrome)](https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb?hl=nl)
8. [Nimbus Screenshot (Chrome)](https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj?hl=en)
9. [Nimbus Screenshot (FF)](https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/)
10. [OSINT Browser (FF)](https://addons.mozilla.org/en-US/firefox/addon/osint-browser-extension/)
11. [Practical Startpage (Chrome)](https://chrome.google.com/webstore/detail/practical-startpage/ikjalccfdoghanieehppljppanjlmkcf?hl=nl)
12. [Screengrab! (Chrome)](https://chrome.google.com/webstore/detail/screengrab/fccdiabakoglkihagkjmaomipdeegbpk?hl=nl)
13. [Scroll++ (FF)](https://addons.mozilla.org/en-US/firefox/addon/scroll/)
14. [Scroll to bottom (Chrome)](https://chrome.google.com/webstore/detail/scroll-to-bottom/filgdcpknaklibnfoejliopebdipligb)
15. [Translate Selected Text (Chrome)](https://chrome.google.com/webstore/detail/translate-selected-text/fbimffnjoeobhjhochngikepgfejjmgj?hl=nl)
----

### Addons - Phone numbers
1. [WhatsAllApp](https://github.com/LoranKloeze/WhatsAllApp)
2. [Whatsfoto](https://github.com/zoutepopcorn/whatsfoto)
----

### Addons - Privacy/security
1. [Adblock Plus (Chrome)](https://chrome.google.com/webstore/detail/adblock-plus/cfhdojbkjhnklbpkdaibdccddilifddb)
2. [Canvas Defender (Chrome)](https://chrome.google.com/webstore/detail/canvas-defender/obdbgnebcljmgkoljcdddaopadkifnpm?hl=en)
3. [Canvas Defender (FF)](https://addons.mozilla.org/en-US/firefox/addon/no-canvas-fingerprinting/)
4. [Click&Clean (Chrome)](https://chrome.google.com/webstore/detail/clickclean/ghgabhipcejejjmhhchfonmamedcbeod?hl=en)
5. [Copy All Links (Chrome)](https://chrome.google.com/webstore/detail/copy-links/kcpoommnneaebpfgaoejklgemonkmjpc?hl=nl)
6. [Ghostery (Chrome)](https://chrome.google.com/webstore/detail/ghostery/mlomiejdfkolichcflejclcbmpeaniij?hl=en)
7. [Ghostery (FF)](https://addons.mozilla.org/en-US/firefox/addon/ghostery/)
8. [Facebook - Disconnect (Chrome)](https://chrome.google.com/webstore/detail/disconnect-facebook-pixel/nnkndeagapifodhlebifbgbonbfmlnfm?hl=en)
9. [Facebook - Disconnect (FF)](https://addons.mozilla.org/en-GB/firefox/addon/facebook-disconnect-ii/?src=search)
10. [Fea Keylogger](https://chrome.google.com/webstore/detail/fea-keylogger/fgkghpghjcbfcflhoklkcincndlpobja?hl=en)
11. [Dataselfie (FF & Chrome)](https://dataselfie.it/#/)
12. [Don't track me Google](https://chrome.google.com/webstore/detail/dont-track-me-google/gdbofhhdmcladcmmfjolgndfkpobecpg?hl=en)
13. [HTTPS Everywhere (Chrome)](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp?hl=en)
14. [HTTPS Everywhere (FF)](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)
15. [Hola VPN (Chrome)](https://chrome.google.com/webstore/detail/unlimited-free-vpn-hola/gkojfkhlekighikafcpjkiklfbnlmeio?hl=en)
16. [Hola VPN (FF)](https://hola.org/firefox)
17. [Hoxx VPN (Chrome)](https://chrome.google.com/webstore/detail/hoxx-vpn-proxy/nbcojefnccbanplpoffopkoepjmhgdgh)
18. [Hoxx VPN (FF)](https://addons.mozilla.org/en-US/firefox/addon/hoxx-vpn-proxy/)
19. [Privacy Badger (Chrome)](https://chrome.google.com/webstore/detail/privacy-badger/pkehgijcmpdhfbdbbnkijodmdjhbjlgp)
20. [Privacy Badger (FF)](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)
21. [Referer Button (Chrome)](https://chrome.google.com/webstore/detail/referer-control/hnkcfpcejkafcihlgbojoidoihckciin?hl=en)
22. [Referer Button (FF)](https://addons.mozilla.org/en-GB/firefox/addon/referrer-switch/)
23. [Resurrect Pages (FF)](https://addons.mozilla.org/en-US/firefox/addon/resurrect-pages/)
24. [Tampermonkey (Chrome)](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=nl)
----

### Addons - Social Media
1. [Facebook - Advanced Facebook Search (Chrome)](https://chrome.google.com/webstore/detail/afs/gckmgjhcejhnfenfbippohhnfjkeaapj?hl=de-DE)
2. [Facebook - Tagged photos (Chrome)](https://chrome.google.com/webstore/detail/picturemate-view-tagged-f/khmlalkcjmglpgdkmkmmgjcajahkoigj)
3. [Facebook - Video Downloader (Chrome)](https://chrome.google.com/webstore/detail/fbdown-video-downloader/fhplmmllnpjjlncfjpbbpjadoeijkogc)
4. [General - Download photo Albums (Chrome)](https://chrome.google.com/webstore/detail/downalbum/cgjnhhjpfcdhbhlcmmjppicjmgfkppok?hl=nl)
5. [General - Intelligence search (Chrome)](https://chrome.google.com/webstore/detail/intelligence-search/dipfggodcibdmflidbceoaanadclgomm?hl=nl)
6. [General - Prophet (Chrome)](https://chrome.google.com/webstore/detail/prophet/alikckkmddkoooodkchoheabgakpopmg?hl=nl)
7. [Instagram - Downloader for Instagram + Direct Message](https://chrome.google.com/webstore/detail/downloader-for-instagram/olkpikmlhoaojbbmmpejnimiglejmboe?hl=en)
8. [Instagram - Helper Tools for Instagram (Chrome)](https://chrome.google.com/webstore/detail/helper-tools-for-instagra/hcdbfckhdcpepllecbkaaojfgipnpbpb)
9. [LinkedIn - Contact info](https://chrome.google.com/webstore/detail/find-anyones-email-contac/jjdemeiffadmmjhkbbpglgnlgeafomjo)
10. [LinkedIn - Guest browser (FF)](https://addons.mozilla.org/en-US/firefox/addon/linkedin-guest-browser/)
11. [Osint.support (Facebook, Instagram, LinkedIn, Twitter & TikTok)](https://osint.support/)
12. [Pinterest - Guest browser (FF)](https://addons.mozilla.org/en-US/firefox/addon/pinterest-guest/)
13. [Reddit - All comments viewer](https://chrome.google.com/webstore/detail/reddit-all-comments-viewe/hgpgeobpjpchpmkecjppgcnekmbibgbi?hl=en-GB)
14. [Reddit - MostlyHarmless (Chrome)](http://kerrick.github.io/Mostly-Harmless/#features)
15. [Twitter - Treeverse](https://chrome.google.com/webstore/detail/treeverse/aahmjdadniahaicebomlagekkcnlcila?hl=en)
16. [Twitter - Twlets (Chrome)](https://chrome.google.com/webstore/detail/twlets-twitter-to-excel/glmadnnfibhnhgboophnodnhbjdogiec)
----

### Addons - Spoofing
1. [Geo location spoofing (Chrome)](https://chrome.google.com/webstore/detail/manual-geolocation/jpiefjlgcjmciajdcinaejedejjfjgki)
2. [Location Guard (Chrome)](https://chrome.google.com/webstore/detail/location-guard/cfohepagpmnodfdmjliccbbigdkfcgia?hl=en)
3. [Location Guard (FF)](https://addons.mozilla.org/en-GB/firefox/addon/change-geolocation-locguard/)
4. [User-Agent Switcher (Chrome)](https://chrome.google.com/webstore/detail/user-agent-switcher-for-c/djflhoibgkdhkhhcedjiklpkjnoahfmg)
5. [User-Agent Swticher (FF)](https://addons.mozilla.org/en-GB/firefox/addon/user-agent-string-switcher/?src=search)
----

### Addons - Who.is/hosting/technical information
1. [IP Address and Domain Information (Chrome)](https://chrome.google.com/webstore/detail/ip-address-and-domain-inf/lhgkegeccnckoiliokondpaaalbhafoa?hl=nl)
2. [Webrank Whois Lookup (Chrome)](https://chrome.google.com/webstore/detail/webrank-whois-lookup/ajebiiiaphmfaiookbhpepoongjejihh?hl=en)
----

