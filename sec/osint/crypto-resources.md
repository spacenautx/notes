### Crypto & Web3 - Page Links
1. [(Go to Page 2)](https://start.me/p/5v5v7n/additional-crytp-resources)
2. [(Go to Page 3)](https://start.me/p/RMxEvD/crypto-resources-page-3)
3. [Regulation Sources](https://start.me/p/L1lE28/regulatory-considerations)
----

### Select Crypto 🚀
1. [Bitcoin](https://bitcoin.org/en/)
2. [[Bitcoin Whitepaper]](https://storage.googleapis.com/wzukusers/user-35906463/documents/89fc2d0d841642bf8c90d4a9feefdfd0/bitcoin-WP.pdf)
3. [Ethereum](https://ethereum.org/en/)
4. [Ethereum 2.0](https://ethereum.org/en/upgrades/)
5. [[Ethereum Whitepaper]](https://ethereum.org/en/whitepaper/)
6. [Radixdlt](https://www.radixdlt.com/)
7. [- Radix Knowledge Base](https://learn.radixdlt.com/)
8. [[Radix Whitepaper RE DeFi]](https://storage.googleapis.com/wzukusers/user-35906463/documents/6f23c25470db44489a809d0659e81544/60870dd57116a30d877abe57_DeFi-Whitepaper-v1.0-3.pdf)
9. [[Radix Whitepaper RE Atomic Cross-shard Consensus]](https://arxiv.org/abs/2008.04450)
10. [Ripple](https://ripple.com/)
11. [[Ripple Whitepaper]](https://storage.googleapis.com/wzukusers/user-35906463/documents/f8cb5b48dfea45d98593ac0c8268cf87/ripple_consensus_whitepaper.pdf)
12. [Elrond](https://elrond.com/)
13. [[Elrond Whitepaper]](https://storage.googleapis.com/wzukusers/user-35906463/documents/a8b44a79d957467daf4417023657a465/elrond-whitepaper.pdf)
14. [Binance Smart Chain](https://www.bnbchain.world/en/smartChain)
15. [[Binance Documentation]](https://github.com/bnb-chain/whitepaper/blob/master/WHITEPAPER.md)
16. [Dogecoin](https://dogecoin.com/)
17. [[Dogecoin Whitepaper]](https://storage.googleapis.com/wzukusers/user-35906463/documents/aa40e36362da475e98ed34811fe1e653/dogecoin-whitepaper.pdf)
18. [Iota](https://www.iota.org/)
19. [[Iota Papers]](https://www.iota.org/foundation/research-papers)
20. [Cardano]([https://cardano.org/])
21. [[Cardano Documentation]](https://docs.cardano.org/)
22. [Solana](https://solana.com/)
23. [[Solana Documentation]](https://docs.solana.com/)
24. [Polkadot](https://polkadot.network/)
25. [Polkadot (Dot) Paper](https://polkadot.network/PolkaDotPaper.pdf)
26. [NEAR Protocol](https://near.org/)
27. [NEAR Documentation](https://docs.near.org/)
28. [Avalanche - AVA Labs](https://www.avax.network/)
29. [Avalanche Whitepapers](https://www.avalabs.org/whitepapers)
30. [Cosmos](https://cosmos.network/)
31. [Cosmos Whitepaper](https://v1.cosmos.network/resources/whitepaper)
----

### Radix DLT
1. [Radixdlt.com](https://www.radixdlt.com/)
2. [GoodFi](https://www.goodfi.com/)
3. [Radixdlt.store](https://radixdlt.store/)
4. [The Radix Knowledge Base](https://learn.radixdlt.com/?_gl=1*zgmmvk*_ga*MTY3NDY3MzM3Ni4xNjEzODIzMzEw*_ga_MZBXX3HP5Q*MTY1MjIyNTY0Ny4xMTEuMC4xNjUyMjI1NjQ3LjA.)
5. [The Radix Blog | Radix DLT](https://www.radixdlt.com/blog)
6. [Podcast | Radix DLT - Decentralized Ledger Technology](https://www.radixdlt.com/podcast)
7. [Telegram: Contact @radix_dlt](https://t.me/radix_dlt)
8. [Olympia main quest](https://radixverse.art/port)
9. [The Biggest Innovation in Crypto Since Smart Contracts - Obsidian Publish](https://publish.obsidian.md/jake-mai/The+Biggest+Innovation+in+Crypto+Since+Smart+Contracts)
10. [DefiPlaza](https://defiplaza.net/)
11. [radixdlt.shop](https://radixdlt.shop/)
----

### Radix Papers
1. [White Paper: How Radix is building the future of DeFi](https://radixdlt.com/whitepapers/defi)
2. [White Paper: Atomic cross-shard consensus](https://arxiv.org/abs/2008.04450)
3. [White Paper: A Breakthrough in Consensus Theory](https://radixdlt.com/whitepapers/consensus)
4. [Radix Tokenomics](https://learn.radixdlt.com/categories/radix-tokens-tokenomics?_gl=1*bnchxe*_ga*MTY3NDY3MzM3Ni4xNjEzODIzMzEw*_ga_MZBXX3HP5Q*MTY1MjIyNTY0Ny4xMTEuMS4xNjUyMjI1ODQzLjA.)
----

### Conferences
1. [Consensus 2022 Presented by CoinDesk | June 9-12, 2022](https://www.coindesk.com/consensus2022/)
2. [The North American Bitcoin Conference (TNABC)](https://www.btcmiami.com/)
3. [Bitcoin 2023](https://b.tc/conference/)
4. [Permissionless - Blockworks](https://blockworks.co/events/permissionless/)
5. [Future Blockchain Summit 2022](https://www.futureblockchainsummit.com/home)
6. [Token2049](https://www.token2049.com/)
7. [Linq | Digital Business Cards For Modern Networking](https://buy.linqapp.com/discount/Bunn?redirect=%2F%3Fafmc%3DBunn)
----

### Web3 Jobs
1. [Polychain Capital Talent Network](https://jobs.polychain.capital/companies)
2. [Proof of Talent](https://proofoftalent.co/home/crypto-recruiters)
3. [Blockchain and Cryptocurrency Jobs](https://crypto.jobs/)
4. [Cryptocurrency Jobs](https://cryptocurrencyjobs.co/)
5. [Crypto Jobs List](https://cryptojobslist.com/)
6. [Pomp Crypto Jobs](https://pompcryptojobs.com/)
7. [CryptoJobs.Careers](https://cryptojobs.careers/)
8. [🎙 Work in Crypto - Jobs List - Audible](https://www.audible.com?source_code=ASSORAP0511160006)
9. [Bitcoiner Jobs](https://bitcoinerjobs.com/)
----

### Real Estate
1. [BHR - Blockchain Home Registry](https://bhr.fyi/)
2. [Milo Credit - crypto mortgaged](https://www.milocredit.com/)
3. [USDC.homes (Texas only)](https://usdc.homes/)
4. [Figure - crypto mortgages](https://www.figure.com/mortgage/crypto-mortgage/)
5. [Mortgage.ledn.io](https://mortgage.ledn.io/)
----

### Social
1. [Subsocial](https://subsocial.network/)
----

### Legal ⚖
1. [Morrisoncohen.com](https://www.morrisoncohen.com/)
2. [Andersonkill.com](https://www.andersonkill.com/)
3. [Blockchain Law Offices – Blockchain Law Offices](https://www.blockchainlawoffices.com/)
4. [Perkins Coie LLP - International Law Firm | Perkins Coie](https://www.perkinscoie.com/en/)
5. [Willkie Farr & Gallagher LLP](https://www.willkie.com/)
6. [Reed Smith LLP](http://www.reedsmith.com/)
7. [Clifford Chance](https://www.cliffordchance.com/home.html)
8. [Alan Cohn - Partner | Steptoe & Johnson LLP](https://www.steptoe.com/en/lawyers/alan-cohn.html)
9. [Evan Abrams - Associate | Steptoe & Johnson LLP](https://www.steptoe.com/en/lawyers/evan-abrams.html)
----

### Crypto Research Tools
1. [CoinGecko](https://www.coingecko.com/)
2. [Luna Crush](https://lunarcrush.com/)
3. [CoinMarketCal](https://coinmarketcal.com/en/)
4. [Crypto Rating Council](https://www.cryptoratingcouncil.com/)
5. [Coin Dance](https://coin.dance/)
6. [Altcoin Season Index: Is it Altseason right now?](https://www.blockchaincenter.net/altcoin-season-index/)
7. [CryptoQuant](https://cryptoquant.com/)
8. [Glassnode](https://glassnode.com/)
9. [3Commas Trading Bot | Coin Bureau](https://guy.coinbureau.com/3commas/)
10. [Coinglass](https://www.bybt.com/)
11. [Crypto Panic](https://cryptopanic.com/)
12. [Messari](https://messari.io/)
13. [TradingView](https://www.tradingview.com/)
14. [Skew.](https://skew.com/)
15. [Santiment](https://santiment.net/)
16. [CryptoMiso](https://www.cryptomiso.com/)
17. [Blockchaincenter](https://www.blockchaincenter.net/en/)
18. [CryptoCompare (by Kraken)](https://www.cryptocompare.com/)
----

### Free Educational Resources
1. [The Crypto Glossary](https://www.thecryptoglossary.com/)
2. [taxes.law (email robertbunn@duck.com for access)](https://taxes.law/)
3. [Coin Bureau](https://www.coinbureau.com/)
4. [Learn about Cryptocurrency](https://coinmarketcap.com/alexandria)
5. [EDX](https://www.edx.org/)
6. [Decrypt](https://decrypt.co/learn)
7. [Kraken](https://www.kraken.com/learn)
8. [RADAR](https://radar.tech/learn)
9. [Cryptopedia](https://www.gemini.com/cryptopedia)
10. [Crypto Glossary](https://coinmarketcap.com/alexandria/glossary)
----

### Free
1. [Candy Rewards - CoinGecko](https://www.coingecko.com/account/rewards?locale=en)
2. [Sweatco.in](https://sweatco.in/)
3. [KuCoin Rewards](https://www.kucoin.com/land/task-center?spm=kcWeb.B1homepage.Header1.8)
4. [Campuslegends.com](https://campuslegends.com/)
5. [Coinbase - earn while you learn](https://www.coinbase.com/earn)
----

### Music
1. [Audius](https://audius.co/)
----

### Insurance
1. [bridgemutual](https://bridgemutual.io/)
2. [inSure](https://insuretoken.net/)
----

### New to DeFi
1. [GoodFi](https://www.goodfi.com/)
----

### YouTube
1. [Coin Bureau](https://www.youtube.com/channel/UCqK_GSMbpiV8spgD3ZGloSw)
2. [Crypto Banter](https://www.youtube.com/channel/UCN9Nj4tjXbVTLYWN0EKly_Q)
3. [RadixDLT](https://www.youtube.com/c/RadixDLT)
4. [EatTheBlocks](https://www.youtube.com/channel/UCZM8XQjNOyG2ElPpEUtNasA)
----

### Podcasts
1. [The DeFi Download](https://radixdlt.buzzsprout.com/)
2. [The Encrypted Economy](https://theencryptedeconomy.com)
3. [Unchained Podcast](https://unchainedpodcast.com)
4. [The Bad Crypto Podcast](https://badcryptopodcast.com/)
5. [Blockcrunch](https://castbox.fm/channel/id4823944?country=us)
6. [Into the Ether](https://podcast.ethhub.io/website/category/povcryptopod.libsyn.com/)
7. [Wyre Talks](https://blog.sendwyre.com/wyretalks/home)
8. [Chris Blec on DeFi](https://odysee.com/@ChrisBlec:8)
9. [Bankless - YouTube](https://youtube.com/c/Bankless)
10. [Coinstack - For Smart Crypto Investors - Bitcoin, Ethereum, DeFi & The Future of Money • A podcast on AnchorGlobe IconFacebook LogoTwitter LogoInstagram Logo](https://anchor.fm/coinstack)
11. [Real Vision Finance - YouTube](https://youtube.com/c/RealVisionFinance)
12. [Nathaniel Whittemore - YouTube](https://youtube.com/c/NathanielWhittemoreCrypto)
13. [DeFi, NFT & Web3 Insights - The Defiant - YouTube](https://youtube.com/c/TheDefiant)
----

### Space
1. [Crypto Space Agency](https://www.csa.xyz/)
2. [Relativity Space](https://luxcapital.com/companies/relativity-space/)
----

### Vehicles 🚘
1. [Carnomaly](https://carnomaly.io/)
----

### Banks/Finance
1. [Custodia Bank](https://www.custodiabank.com/)
2. [FV Bank](https://www.f bank.us)
3. [Signature Bank](https://www.signet.com/)
4. [Western Alliance Bank](https://westernalliancebank.com/blockchain)
----

### Play-to-earn and Gaming
1. [Galaxy Fight Club](https://galaxyfightclub.com/)
2. [Axie Infinity](https://axieinfinity.com/)
3. [The Sandbox Game](https://www.sandbox.game/en/)
----

### The Metaverse  (virtual worlds)
1. [Gala](https://gala.com/)
2. [Decentraland](https://decentraland.org/)
3. [Horizon Worlds on Oculus Quest](https://www.oculus.com/experiences/quest/2532035600194083/?utm_source=gg&utm_medium=ps&utm_campaign=17154134368&utm_term=horizon+facebook&utm_content&gclsrc=aw.ds&gclid=Cj0KCQjwn4qWBhCvARIsAFNAMihVeBj9fVLpe-ogCaxn_CwPYT0DbcAlep8R_yMjW6X5RRi_fuhEQQoaAnR_EALw_wcB)
4. [Bloktopia](https://www.bloktopia.com/)
5. [Pax.world](https://pax.world/)
6. [Somnium Space](https://www.somniumspace.com/)
7. [Enjin.io](https://enjin.io/)
8. [Star Atlas](https://staratlas.com/)
9. [My Meta MMO](http://My Meta MMO.com)
10. [ActiveWorlds](https://www.activeworlds.com/)
----

### Newsletters
1. [Bankless](https://bankless.substack.com/)
2. [Bankless Level-Up Guide](https://newsletter.banklesshq.com/p/bankless-level-up-guide?s=r)
3. [Kermankohli](https://kermankohli.substack.com/)
4. [Dose of DeFi](https://doseofdefi.substack.com/)
5. [EthHub](https://ethhub.substack.com/)
6. [My Two Gwei](https://mytwogwei.substack.com/)
7. [The Defiant](https://newsletter.thedefiant.io/)
8. [Week in Ethereum News](https://www.weekinethereumnews.com/)
9. [DeFi Tutorials](https://defitutorials.substack.com/)
----

### Mobile
1. [Celo.org](https://celo.org/)
----

### Decentralized Exchanges
1. [Orca (DEX for people, not programs)](https://www.orca.so/)
2. [Uniswap](https://app.uniswap.org/#/swap?chain=mainnet)
3. [1inch](https://app.1inch.io/#/1/swap/ETH/DAI)
4. [AirSwap](https://www.airswap.io/#/)
5. [Balancer](https://balancer.exchange/)
6. [Bancor](https://app.bancor.network/)
7. [dYdX](https://dydx.exchange/)
8. [KyberSwap](https://kyberswap.com/#/about)
9. [Matcha](https://matcha.xyz/)
10. [Tokenlon](https://tokenlon.im/#/)
11. [Totle](https://www.totle.com/)
12. [Curve](https://curve.fi/)
----

### Additional Exchanges
1. [Binance.US](https://www.binance.us/en/home)
2. [Binance (Non-US)](https://www.binance.com/en)
3. [Coinbase](https://www.coinbase.com/)
4. [Coinbase Pro](https://pro.coinbase.com/)
5. [Kraken](https://www.kraken.com/)
6. [KuCoin](https://www.kucoin.com/)
7. [paraswap](https://www.paraswap.io/)
----

### Decentralized Finance Platforms
1. [Aave](https://aave.com/)
2. [Compound](https://app.compound.finance/)
3. [DeFi Saver](https://defisaver.com/)
4. [Oasis](https://oasis.app/)
5. [MakerDAO](https://makerdao.com/en/)
6. [Yearn Finance](https://yearn.finance/#/portfolio)
7. [Zapper](https://zapper.fi/)
----

### Other Financial Platforms
1. [depository.money (Yield Farming for Landlords)](https://unitedsecuritydeposit.com/)
2. [Public Mint](https://publicmint.com/)
3. [NEXO (& Get $25 in BTC)](https://nexo.io/ref/kgyk0dzp2z?src=ios-link)
4. [Maple.finance (undercollateralized loans for institutional borrowers)](https://maple.finance/)
5. [dharma](https://www.dharma.io/)
6. [MoonPay](https://www.moonpay.com/)
----

### Predictive Markets
1. [Hedgehog Markets](https://hedgehog.markets/)
2. [Augur](https://www.augur.net/)
----

### Dashboard Interfaces
1. [Portfolio.defiprime.com](https://portfolio.defiprime.com/)
2. [Instadapp](https://instadapp.io/)
3. [Zerion](https://zerion.io/)
4. [Dune](https://dune.com/home)
----

### Lab
1. [Protocol Labs](https://protocol.ai/)
----

### Other
1. [Y Combinator](https://www.ycombinator.com/)
2. [Global Income Coin](https://www.globalincomecoin.com/)
3. [Bitrefill (crypto to buy gift cards)](https://www.bitrefill.com/buy/worldwide/?hl=en)
4. [Top 100 People In Blockchain and Crypto 2021](https://cointelegraph.com/top-people-in-crypto-and-blockchain-2021)
5. [YouHodler](https://www.youhodler.com/)
6. [Blockcerts (MIT blockchain verifiable Diploma)](https://www.blockcerts.org/)
7. [Helium](https://www.helium.com/)
8. [BitGo](https://www.bitgo.com/)
9. [Crypto Woodstock](https://worldnewsera.com/news/entrepreneurs/inside-crypto-woodstock-where-technologists-plot-a-utopian-future/)
----

### DeFi Information
1. [DeFi Prime](https://defiprime.com/)
2. [DeFi Pulse](https://defipulse.com/)
3. [LoanScan: Compare High Interest Accounts](https://loanscan.io/)
----

### Privacy
1. [Proton](https://proton.me/)
2. [Duck Duck Go](https://duckduckgo.com/)
3. [Threema](https://threema.ch/en)
4. [The Tor Project](https://www.torproject.org/download/)
5. [Onion Sites](https://www.expressvpn.com/blog/best-onion-sites-on-dark-web/)
----

### Indexing Web3
1. [The Graph](https://thegraph.com/en/)
----

### Web3 Identity
1. [SelfKey](https://selfkey.org/)
----

### Crypto Cards
1. [Switch - Black Card](https://switchrewardcard.com/black-card/)
2. [Crypto.com Visa Card](https://crypto.com/cards)
3. [Abra](https://www.abra.com/)
4. [Fold](https://foldapp.com/)
----

### Exercise
1. [Genopets - Move-to-Earn NFT game](https://www.genopets.me/)
2. [Sweatcoin - rewards for steps](https://sweatco.in/)
3. [STEPN is a Web3 lifestyle app with Social-Fi and Game-Fi elements](https://stepn.com/)
----

### DAO
1. [LexDAO](https://www.lexdao.coop/)
2. [DAO4DAOs](https://dao4dao.webflow.io/)
3. [OrangeDAO.xyz](https://www.orangedao.xyz/)
4. [Orange DAO - Twitter](https://twitter.com/OrangeDAOxyz?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor)
5. [LinksDAO](https://linksdao.io/)
6. [CityDAO](https://email.mg2.substack.com/c/eJwlkEluhTAMQE9DligjwyKLdtFroAyGRoUEJU4RPX3D_5LlhWX7-dkZhC3lW5-pIHnSgvcJOsJVdkCETGqBvASvhRQzZUwRr6Vnk5pIKMuaAQ4Tdo25Ajmr3YMzGFJ8TQglh4l8az-oVU5tgRcgB-M5s-Nq13ESXI0rp2-wqT5AdKDhF_KdIhCXjgMiPrvIrr8Rz9KJj45_tbiuq3cBb29SH1IrkKA55ZyqlgWd1dyLnk6Tc4ZKcNYZOatO0mPjfam2oHE_fQOQrHOykNHWGDv-6WFH0_q2R-vV0KyW55IaG26BaOwO7SB8_-ylv2wQIbdf-sWgZoMSjcyZZPP4dmsGSg2zGqUije5Tm4p6N393XLH8A2SBgv0)
7. [LobsterDAO](https://email.mg2.substack.com/c/eJwlUMluwyAQ_Zpwq4VZbHzg0Et_w2KZOKgYLBgSuV9fnEhopNHwVmcQtlxOfeSK5BorngfoBK8aAREKaRXKGrzmgi-CjTPxWvhRSUVCXe8FYDchaiwNyNFsDM5gyOmN4FJMijz0RLnwVEhrlAM6T0rMCub74qkV08jZR9g0HyA50PCEcuYExOV9h4QXF4n6gXjUG_--sZ_-toCPZof-pS8x29q9fnmT-5Y7_hngRYJmlDEq--R0kcvAB6qUc4YKcNYZsciboPvGhto6gXG_Fx8pumQLBfutYrkaCi6a15DLdkVcL1stBTxXSMZG6O7wU-C7i3WDBBfMrwb1OEneRdkolaSfoD2OlNMiZyFJF_a5o5KO5u9Md6z_mwCILA)
8. [Flamingo](https://flamingodao.xyz/)
9. [Introduction to the Maker Protocol](https://docs.makerdao.com/)
10. [Bankless.community](https://www.bankless.community/)
11. [Developer DAO](https://www.developerdao.com/)
12. [DAO Haus](http://DAOHaus.com)
13. [Moralis](http://Moralis.com)
14. [Maker DAO](https://makerdao.com/)
15. [Moloch DAO](https://molochdao.com/)
16. [Raid Guild](https://www.raidguild.org/)
17. [MANTRA DAO Community Governed DeFi Platform for Staking and Lending](https://www.mantradao.com/)
18. [Aeraforce](https://www.aeraforce.xyz/)
19. [Twire.io](https://www.twire.io/)
20. [Friends With Benefits](https://www.fwb.help/join)
----

### Sheldon the Sniper 🎯
1. [How Crypto "Changed My Life Forever" with Sheldon The Sniper!](https://www.youtube.com/watch?v=YGBLIYXXoOA&t=2041s)
2. [CoinPanel](https://app.coinpanel.com/login)
3. [Sheldon The Sniper - Twitter](https://twitter.com/Sheldon_Sniper?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor)
4. [TNABC - Sniper 1](https://1drv.ms/v/s!AhlCXLk4e2T2gpVmHeKxZQDR049U8g?e=QT0iWZ)
5. [TNABC - Sniper 2](https://1drv.ms/v/s!AhlCXLk4e2T2gpVnSBudMkVfodUXRg?e=YdECcy)
----

### Recommended Books 📚
1. [Digital Assets and Blockchain Technology (I know the author; he is an industry guru)](https://www.amazon.com/Digital-Assets-Blockchain-Technology-Regulation/dp/1789907454/ref=mp_s_a_1_1?crid=2UXEW5NIH5LDT&keywords=digital+assets+and+blockchain+technology&qid=1652213815&s=books&sprefix=digital+assets+%2Caps%2C157&sr=1-1)
2. [How to Bitcoin](https://www.amazon.com/How-Bitcoin-CoinGecko-ebook/dp/B08XVZLKHT/ref=sr_1_3?dchild=1&keywords=How+to+Bitcoin&qid=1633260453&sr=8-3)
3. [How to DeFi (Beginner)](https://www.amazon.com/How-DeFi-Beginner-Coin-Gecko-ebook/dp/B098KMWZG7/ref=sr_1_3?dchild=1&keywords=How+to+DeFi&qid=1633260490&sr=8-3)
4. [How to DeFi (Advanced)](https://www.amazon.com/How-DeFi-Advanced-Coin-Gecko-ebook/dp/B098J81GMK/ref=sr_1_4?dchild=1&keywords=How+to+DeFi&qid=1633260532&sr=8-4)
5. [Chart Logic (I know the author, he is brilliant)](https://www.amazon.com/Chart-Logic-Comprehensive-Cryptocurrencies-Outperform-ebook/dp/B08DG84X92/ref=sr_1_4?dchild=1&keywords=Chart+Logic&qid=1633260625&sr=8-4)
6. [DeFi Investment Made Easy](https://www.amazon.com/DEFI-Investment-Made-Easy-Decentralized-ebook/dp/B08RZCYBX1/ref=sr_1_3?dchild=1&keywords=DEfi+Investment+made+easy&qid=1633260902&sr=8-3)
7. [Legal Tech, Smart Contracts and Blockchain](https://www.amazon.com/Contracts-Blockchain-Perspectives-Business-Innovation-ebook-dp-B07NH8FCL1/dp/B07NH8FCL1/ref=mt_other?_encoding=UTF8&me=&qid=1633261713)
8. [Token Economy: How the WEb3 reinvents the Internet](https://www.amazon.com/Token-Economy-Web3-reinvents-Internet-ebook/dp/B08BKSY3QZ/ref=sr_1_1?dchild=1&keywords=Token+Economy&qid=1633260669&sr=8-1)
9. [Economics and Math of Token Engineering and DeFi: Fundamentals of Token Economics](https://www.amazon.com/Economics-Math-Token-Engineering-DeFi-ebook/dp/B08NCP7K7S/ref=sr_1_3?crid=ZP2FCRGAW1PF&dchild=1&keywords=economics+and+math+of+token+engineering+and+defi&qid=1633260941&sprefix=Economics+and+Math+of+T%2Caps%2C168&sr=8-3)
10. [DeFi and the Future of Finance](https://www.amazon.com/DeFi-Future-Finance-Campbell-Harvey-ebook/dp/B09DJV2QLC/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=1652015654&sr=8-1)
----

### Select Wallets
1. [MetaMask](https://metamask.io/)
2. [Ledger](https://www.ledger.com/)
3. [trezor](https://trezor.io/)
4. [Phantom.app](https://phantom.app/)
5. [Trust Wallet](https://trustwallet.com/)
6. [Atomic Wallet](https://atomicwallet.io/)
7. [Bitcoin.com](https://www.bitcoin.com/)
8. [Coinomi](https://www.coinomi.com/en/)
9. [Exodus](https://www.exodus.com/)
10. [Gnosis-safe](https://gnosis-safe.io/)
11. [Monolith](https://monolith.xyz/)
12. [Bitcoin (Core)](https://bitcoin.org/en/)
13. [Cake Wallet](https://cakewallet.com/)
14. [Arculus](https://www.getarculus.com/products/cold-storage-wallet/)
----

### Blockchain Domains
1. [Code is Law](https://codeis.law/)
2. [TheMetaverse.law](https://themetaverse.law/)
3. [Unstoppable Domains](https://unstoppabledomains.com/)
4. [Ethereum Name Service](https://ens.domains/)
----

### Taxes
1. [Cointracking](https://cointracking.info/)
2. [Cryptocurrency Tax Software](https://tokentax.co/)
3. [Cryptoworth](https://cryptoworth.io/)
4. [TaxBit](http://www.taxbit.com/)
5. [Crypto Tax Calculator](https://cryptotaxcalculator.io/)
----

### Cryptocurrency Search
1. [bitcoin Whos Who](https://bitcoinwhoswho.com/)
2. [blockchain.com/explorer](https://www.blockchain.com/explorer)
3. [BlockSeer](https://www.blockseer.com/)
4. [oxt.me/](https://oxt.me/)
5. [WalletExplorer.com: smart Bitcoin block explorer](https://www.walletexplorer.com/)
6. [cryptocurrencyfacts.com/](https://cryptocurrencyfacts.com/)
7. [Radix Explorer](https://explorer.radixdlt.com/#/)
----

### NFT (General)
1. [Lazy](https://lazy.com/)
2. [Tapistrii](https://www.tapistrii.com/)
3. [Metasill](https://www.metasill.io/)
4. [Infiniteobjects](https://infiniteobjects.com/)
5. [Chain2frame](https://chain2frame.com/)
6. [Aeternals.io](https://www.aeternals.io/)
7. [Flips.Finance](https://www.flips.finance/)
8. [NFT scoring](https://nftscoring.com/trending)
9. [NFT Calendar](https://nftcalendar.io/)
10. [Compass.art](https://compass.art/)
11. [icy.tools](https://icy.tools/)
12. [Moby](https://moby.gg/)
13. [Nftnerds](https://nftnerds.ai/)
14. [Raritysniffer.com](https://raritysniffer.com/)
15. [Intermezzo.tools](https://intermezzo.tools/)
16. [Nansen](https://www.nansen.ai/)
17. [NFTBank](http://nftbank.ai/)
18. [Flooredape](https://flooredape.io/)
19. [CryptoSlam!](https://cryptoslam.io/)
20. [Context](https://context.app/trending)
21. [Coniun (New mints)](https://coniun.io/new-mints)
22. [Drops (NFT loans)](https://drops.co/)
23. [The Crypto Space Agency](https://email.mg2.substack.com/c/eJwlUMuOhCAQ_JrhaBBoxQOHvexvGITWIatgoF3X-frFmaRTSadTXQ9nCZeUL7OnQuyGka4dTcSzrEiEmR0F8xi8kUoOSrQ980b5VoNmoYxzRtxsWA3lA9l-TGtwlkKKb4YE1Wn2NHOLWis_C7DtDIOUc9fxnk9910PnBX6E7eEDRocGfzFfKSJzadsw0v2LreZJtJeH_HqI7zrneTau2ObvetWNBSO4EBwqSj7A0MiGa-2c5Qrd5Kwa4KH4toimHFMh636a-p1lk9OEmeqtUL7LCG61Z5PycqcZbwdHDHSNGO20YjVCn67esccFI940P1oybQeyiooWNPBPpuocoBugV8CqsE-VFc1qX1ecqfwDSgyAaw)
24. [WAX](https://on.wax.io/wax-io/)
25. [VeVe Digital Collectibles](https://www.veve.me/)
----

### NFT (Ethereum)
1. [Rarible.com](https://rarible.com/)
2. [Opensea.io](https://opensea.io/)
3. [rarity.tools](https://rarity.tools/)
----

### NFT (Solana)
1. [Phantom](https://phantom.app/)
2. [Solanart](https://solanart.io/)
3. [HowRare.is](https://howrare.is/)
4. [DigitalEyes Market](https://digitaleyes.market/)
5. [Solsea.io](https://solsea.io/)
6. [Magic Eden](https://magiceden.io/)
7. [Moonrank.app](https://moonrank.app/)
----

### NFT (Radix)
1. [abandoned scorpions](https://radstrike.com/scorpions/)
2. [Cats Doing Good Deeds](https://cdgd.org/)
3. [Hoard Token](https://hoardtoken.com/)
4. [Nerds Republic](https://nerdsrepublic.net/)
5. [Roidboiz](https://roidboiz.io/)
6. [Radchix](https://radchix.com/)
7. [Radlads](https://radladsxrd.com/)
8. [Radical Penguins](https://www.radicalpenguins.com/)
9. [Artistizen - Radix Team Tributjects](https://www.artistizen.com/)
10. [Natty Radishes](https://nattyradishes.com/)
11. [Root Materials](https://www.rootmaterials.com/)
----

### 🔥 Select Content
1. [Select Specific Content](https://start.me/p/b5zG5m/crypto-content)
2. [Crypto Laws & Regulations](https://taxes.law/crypto-laws-regulations)
----

### Helpful Relevant Non-Web3 Links
1. [12ft – Hop any paywall](https://12ft.io/)
----

