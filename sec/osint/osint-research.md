### Organisations
1. [Influence Operations | Centre for Information Resilience](https://www.info-res.org)
----

### Companies
1. [UBO registers in the EU – Blockint](https://www.blockint.nl/kyc/ubo-registers-in-the-eu/)
2. [The Company Database | aiHit](https://www.aihitdata.com)
----

### People
1. [Home | Locate Family | Find people for FREE!](https://locatefamily.com)
----

### Monitor
1. [Deck for Reddit](https://rdddeck.com)
2. [TweetDeck](https://tweetdeck.twitter.com)
----

### Geo
1. [Copernicus and the free & open source software community | Copernicus](https://www.copernicus.eu/en/copernicus-and-free-open-source-software-community)
2. [Open Access Hub](https://scihub.copernicus.eu/)
3. [Convert Coordinates](https://www.earthpoint.us/convert.aspx)
4. [GPS coordinate converter](https://www.gps-coordinates.net/gps-coordinates-converter)
5. [Free Map Tools](https://www.freemaptools.com/)
6. [Geographic data mining and visualisation for beginners - Overpass turbo tutorial by Mateusz Koniecz...](https://mapsaregreat.com/geographic-data-mining-and-visualisation-for-beginners/overpass-turbo-tutorial.html)
7. [Map Developers - we build custom google map api solutions](https://www.mapdevelopers.com/)
8. [GPS Visualizer](https://www.gpsvisualizer.com/)
9. [IPVM Camera Calculator V3](https://calculator.ipvm.com/)
10. [15 Free Satellite Imagery Data Sources - GIS Geography](https://gisgeography.com/free-satellite-imagery-data-list/)
11. [Generate a panorama](https://www.udeuschle.de/panoramas/makepanoramas_en.htm)
12. [SunCalc - sunrise, sunset, shadow length, solar eclipse, sun position, sun phase, sun height, sun c...](https://www.suncalc.org/#/52.5107,13.4272,17/2017.07.19/19:44/1/1)
13. [Calculation of sun’s position in the sky for each location on the earth at any time of day [en]](https://www.sunearthtools.com/dp/tools/pos_sun.php?lang=en)
14. [PeakFinder - Finsteraarhorn](https://www.peakfinder.org/?lat=46.5376&lng=8.1257&azi=90&zoom=5&ele=4274&name=Finsteraarhorn)
15. [PeakVisor Identify Mountains](https://peakvisor.com/)
16. [overpass turbo](https://overpass-turbo.eu/)
17. [datadesk/overpass-turbo-tutorial: Code examples and tutorial on getting data out of OpenStreetMap](https://github.com/datadesk/overpass-turbo-tutorial)
18. [Descartes Labs: Search](https://search.descarteslabs.com/?layer=naip_v2_rgb_2014-2015#lat=39.2322531&lng=-100.8544921&zoom=5)
19. [GeoTips – Tips and tricks for Geoguessr](https://geotips.net/)
20. [Geohack](https://geohack.toolforge.org/)
21. [People Search / OSINT Investigations Tool by Bob Brasich @goFindWho](https://gofindwho.com/)
22. [Ukraine Interactive map - Ukraine Latest news on live map - liveuamap.com](https://liveuamap.com/)
23. [Soar | Discover your Earth](https://soar.earth/)
24. [3G / 4G / 5G coverage in United States - nPerf.com](https://www.nperf.com/en/map/US/-/-/signal/?ll=47.93335932610921&lg=-123.45000000000002&zoom=3)
25. [I Search From: custom location, language, device & personalization Google Search tool to preview ad...](http://isearchfrom.com)
26. [Home Page](https://www.mapchannels.com/Home.aspx)
27. [MapHub · Create interactive maps](https://maphub.net)
28. [OpenStreetMap Taginfo](https://taginfo.openstreetmap.org)
29. [cartographia/geospatial-intelligence-library: 🛰 Your geospatial intelligence tool belt for digital...](https://github.com/cartographia/geospatial-intelligence-library)
30. [GeoNames](https://www.geonames.org)
31. [Spectator | Real-Time Satellite Monitoring](https://spectator.earth)
----

### Flight
1. [Virtual Radar (3809)](http://www.virtualradar.nl/virtualradar/desktop.html)
2. [Icarus Flights – Flight Intelligence for Investigators](https://icarus.flights)
3. [ADS-B Exchange - tracking 13160 aircraft](https://globe.adsbexchange.com)
4. [USASC-USAAS-USAAC-USAAF-USAF Military Aircraft Serial Numbers--1908 to Present](http://www.joebaugher.com/usaf_serials/usafserials.html)
----

### Photo
1. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
2. [ExifTool by Phil Harvey](https://exiftool.org/)
3. [IPVM Camera Calculator V3](https://calculator.ipvm.com/)
4. [Remove Unwanted Objects & Fix Imperfections with Inpaint Online!](https://theinpaint.com/)
5. [ImgOps - Image Operations - 画](https://imgops.com/)
6. [Remove Background from Image – remove.bg](https://www.remove.bg/)
7. [Forensically, free online photo forensics tools - 29a.ch](https://29a.ch/photo-forensics/#forensic-magnifier)
8. [dessant/search-by-image: Browser extension for reverse image search, available for Edge, Chrome and...](https://github.com/dessant/search-by-image)
9. [Rotate image online](https://pinetools.com/rotate-image)
10. [Gesichtserkennung | Microsoft Azure](https://azure.microsoft.com/de-de/services/cognitive-services/face/#features)
11. [Remove Background from Image – remove.bg](https://www.remove.bg)
12. [Am I Real?](https://seintpl.github.io/AmIReal)
13. [tanaikech/goris: This is a CLI tool to search for images with Google Reverse Image Search (goris).](https://github.com/tanaikech/goris)
----

### Characters
1. [Type ° degree symbol, © copyright symbol, and other symbols online](https://symbols.typeit.org/)
2. [Search form - searching for Unicode characters by name](https://unicode-search.net/)
3. [List of Unicode characters - Wikipedia](https://en.wikipedia.org/wiki/List_of_Unicode_characters)
----

### Monitoring
1. [CrowdTangle | Content Discovery and Social Monitoring Made Easy](https://www.crowdtangle.com/)
2. [Echosec Systems | Threat Intelligence Solutions](https://www.echosec.net/)
----

### Cipher-Stego
1. [Online Steganography tool (embed/hide secret messages or images within a host-image)](https://manytools.org/hacker-tools/steganography-encode-text-into-image)
2. [Steghide - Documentation](http://steghide.sourceforge.net/documentation.php)
3. [Code-Breaking, Cipher and Logic Puzzle solving tools | Boxentriq](https://www.boxentriq.com/code-breaking)
----

### Hacking
1. [Ettercap Home Page](https://www.ettercap-project.org/index.html)
----

### Reporting
1. [GitHub - NUKnightLab/TimelineJS3: TimelineJS v3: A Storytelling Timeline built in JavaScript. http:...](https://github.com/NUKnightLab/TimelineJS3)
----

### China
1. [Home - What's on Weibo](https://www.whatsonweibo.com)
2. [Pleco Software – The #1 Chinese dictionary app for iOS and Android](https://www.pleco.com)
3. [Weibo](https://weibo.com/login.php)
4. [LiuChan | A Chinese mouseover dictionary extension for Chrome](https://paperfeed.github.io/LiuChan)
5. [LiuChan Chinese Popup Dictionary - Chrome Web Store](https://chrome.google.com/webstore/detail/liuchan-chinese-popup-dic/hjpjmkjmkgedphipmbnmejlnfndjcgdf/related?hl=en)
----

### Leaks
1. [20+ OSINT resources for breach data research – osintme.com](https://www.osintme.com/index.php/2021/04/18/20-osint-resources-for-breach-data-research/)
2. [Fuck Facebook (TM)](http://4wbwa6vcpvcr3vvf4qkhppgy56urmjcj2vagu2iqgp3z656xcmfdbiqd.onion.pet)
3. [Have I Been Pwned: Check if your email has been compromised in a data breach](https://haveibeenpwned.com)
4. [Just a moment...](https://dehashed.com)
----

### Genealogy
1. [Home • Verein für Computergenealogie e.V. (CompGen)](https://www.compgen.de/)
2. [FamilySearch • Free Family Trees and Genealogy Archives — FamilySearch.org](https://www.familysearch.org/en/)
----

### Skype
1. [8C/skypehunt: OSINT Framework for Skype](https://github.com/8C/skypehunt)
----

### Maps
1. [Fire Map - NASA | LANCE | FIRMS](https://firms.modaps.eosdis.nasa.gov/map/#d:24hrs;@32.1,45.7,8z)
----

### Tools
1. [IntelTechniques OSINT Online Search Tool](https://inteltechniques.com/tools)
2. [CyberChef](https://gchq.github.io/CyberChef/)
3. [Online Tools for Software Engineers](https://www.online-toolz.com/)
4. [@cyb_detective OSINT-tools collection](https://cipher387.github.io/osint_stuff_tool_collection)
5. [Your online toolshed: many tools for web developers, system admins & webworkers (a.o. Network confi...](https://manytools.org)
6. [Free Online Tools For Developers - to Beautify, Validate, Minify, Analyse, Convert JSON, XML, JavaS...](https://codebeautify.org/?__cf_chl_captcha_tk__=46a6845b710e1b0b793fd6dcf31bec60e561288f-1607423361-0-ASDqDXi-jCUM8MbJ9TXPaK23d0yjqzUIVYNUMJs_g4ZJhxLlAs6tkd7GBoqQh13UUKm8jrGtZlS6q9zvDT5eZ59nZF0RA7OjpnEQfvMnEJkK_rL3uNRZfZ4an3UQ4QnIw62GYBxnt5jfWsGhfCLx2pXx-BOWG1om0-ex67T1WKjtTl_IFIr-TW1pXIrUltGrdSHDP4oAwfSkvYhvmAB4Q7SkLTixujqoWCSyrntbXBj2wKjBAL8VgDSJfX4O6NBTRwQQojLoNVomwjUFVZEVtS0zPnUUol3rIoABUr_jnTc6AxnLkeNqEEJ9fQUfuV5rca30q1KEogqjIbC4uJqBI2NBpP1XN6SBFwVAV7zIxrZhGBl13l-JC34X3FwOkDuH2nn_xvdM4nZ9F_ej7zqIx1oJ8O4EWOWOFdvJy7QiaLP868U1th4u_lUKN8DmOmEKtplJu_1oWKrn3x6qZ03VQ5Wq-rkJDu3QlKdc2ApJQ0OuL75VERv3a8P30tlyuA78h1AADHPKbPSPFVDVaHmuvzg)
7. [Tools - Intelligence X](https://intelx.io/tools)
8. [Free Online Epoch And Unix Timestamp Converter - FreeFormatter.com](https://www.freeformatter.com/epoch-timestamp-to-date-converter.html)
9. [Ultimate Hashing and Anonymity toolkit](https://md5hashing.net/)
10. [Free Tools | OSINT Combine](https://www.osintcombine.com/tools)
11. [Free Online Tools For Developers - FreeFormatter.com](https://freeformatter.com/)
12. [OSINT Toolkit](https://one-plus.github.io/index.html)
13. [Readium](https://sugoidesune.github.io/readium/)
14. [Lyzem Search](https://lyzem.com/)
15. [Mobile-Friendly Test – Google Search Console](https://search.google.com/test/mobile-friendly)
16. [CTI & OSINT Online Resources - Google Drive](https://docs.google.com/spreadsheets/d/1klugQqw6POlBtuzon8S0b18-gpsDwX-5OYRrB7TyNEw/htmlview#)
17. [Skype Resolver | WebResolver](https://webresolver.nl)
18. [shot-scraper: automated screenshots for documentation, built on Playwright](https://simonwillison.net/2022/Mar/10/shot-scraper)
19. [Home - SpiderFoot](https://www.spiderfoot.net)
20. [OSINT Tools - Practical OSINT Tools - free of charge - Aware Online](https://www.aware-online.com/en/osint-tools)
----

### Tools - specific
1. [Hunchly - Darkweb OSINT - Free Daily Dark Web Reports](https://www.hunch.ly/darkweb-osint/)
2. [jq](https://stedolan.github.io/jq/)
3. [securisec/chepy: Chepy is a python lib/cli equivalent of the awesome CyberChef tool.](https://github.com/securisec/chepy)
4. [ANY.RUN - Interactive Online Malware Sandbox](https://any.run/)
----

### Collection tools
1. [Home - SpiderFoot](https://www.spiderfoot.net/)
2. [Infoooze | Infoooze is an Information collection tool (OSINT) in NodeJs. It provides various module...](https://infoooze.js.org)
3. [intelowlproject/IntelOwl: Intel Owl: analyze files, domains, IPs in multiple ways from a single API...](https://github.com/intelowlproject/IntelOwl)
----

### Download
1. [Download social media videos without tools](https://www.osintcombine.com/post/download-social-media-videos-without-tools)
2. [Download Youtube video fast, convert high quality youtube to mp3 | YT5s.com](https://yt5s.com/en12)
3. [Yout.com](https://yout.com)
----

### Github
1. [darkoperator/dnsrecon: DNS Enumeration Script](https://github.com/darkoperator/dnsrecon)
2. [GitHub - s0md3v/ote: Generate Email, Register for anything, Get the OTP/Link](https://github.com/s0md3v/ote)
3. [GitHub - FortyNorthSecurity/EyeWitness: EyeWitness is designed to take screenshots of websites, pro...](https://github.com/FortyNorthSecurity/EyeWitness)
4. [megadose/holehe: holehe allows you to check if the mail is used on different sites like twitter, in...](https://github.com/megadose/holehe)
5. [evilsocket/ditto: A tool for IDN homograph attacks and detection.](https://github.com/evilsocket/ditto)
6. [GitHub - nixintel/o365chk: Simple Python tool to check if there is an Office 365 instance linked to...](https://github.com/nixintel/o365chk/)
7. [GitHub - laramies/theHarvester: E-mails, subdomains and names Harvester - OSINT](https://github.com/laramies/theHarvester)
8. [GitHub - lanmaster53/recon-ng: Open Source Intelligence gathering tool aimed at reducing the time s...](https://github.com/lanmaster53/recon-ng)
9. [GitHub - projectdiscovery/httpx: httpx is a fast and multi-purpose HTTP toolkit allows to run multi...](https://github.com/projectdiscovery/httpx)
10. [GitHub - iamadamdev/bypass-paywalls-chrome: Bypass Paywalls web browser extension for Chrome and Fi...](https://github.com/iamadamdev/bypass-paywalls-chrome)
11. [GitHub - flameshot-org/flameshot: Powerful yet simple to use screenshot software](https://github.com/flameshot-org/flameshot#installation)
12. [GitHub - BurntSushi/ripgrep: ripgrep recursively searches directories for a regex pattern while res...](https://github.com/BurntSushi/ripgrep)
13. [GitHub - sinwindie/OSINT: Collections of tools and methods created to aid in OSINT collection](https://github.com/sinwindie/OSINT)
14. [GitHub - B0sintBlanc/Osint-Bookmarklets: This is a place for all things OSINT & Bookmarklets!](https://github.com/B0sintBlanc/Osint-Bookmarklets)
15. [cipher387/osint_stuff_tool_collection: A collection of several hundred online tools for OSINT](https://github.com/cipher387/osint_stuff_tool_collection)
16. [amitrajputfff/Profil3r: Profil3r is an OSINT tool that allows you to find potential profiles of a p...](https://github.com/amitrajputfff/Profil3r)
17. [7ORP3DO/infoooze: Infoooze is an Open-source intelligence (OSINT) tool in NodeJs. It provides vario...](https://github.com/7orp3do/infoooze)
18. [Malfrats/OSINT-Map: 🗺 A map of OSINT tools.](https://github.com/Malfrats/OSINT-Map)
19. [blacklanternsecurity/bbot: OSINT automation for hackers.](https://github.com/blacklanternsecurity/bbot)
----

### Resources
1. [OSINT Dojo](https://www.osintdojo.com/resources/)
2. [OH SHINT! Welcome Aboard - OH SHINT! It's A Blog!](https://ohshint.gitbook.io/oh-shint-its-a-blog)
3. [GitHub - cipher387/osint_stuff_tool_collection: A collection of several hundred online tools for OS...](https://github.com/cipher387/osint_stuff_tool_collection)
4. [Full Results Table | MetaOSINT.github.io](https://metaosint.github.io/table)
5. [MetaOSINT | Jumpstart Your OSINT Investigation | Home](https://metaosint.github.io)
6. [OSINT Open Source Intelligence tools resources methods techniques](https://rr.reuser.biz)
----

### VM
1. [Virtual Machines for OSINT — Learn All The Things](https://www.learnallthethings.net/blog/2020/7/23/rage-against-the-virtual-machines)
2. [Build Your Own Custom OSINT Machine – Part 5 – NixIntel](https://nixintel.info/linux/build-your-own-custom-osint-machine-part-5/)
----

### CLI
1. [Wget Wizard - WhatIsMyBrowser.com](https://www.whatismybrowser.com/developers/tools/wget-wizard/)
2. [GNU Grep 3.5](https://www.gnu.org/software/grep/manual/grep.html)
3. [explainshell.com - match command-line arguments to their help text](https://explainshell.com)
4. [C3n7ral051nt4g3ncy/cURL_for_OSINT: cURL Tool Usage for OSINT (Open-Source Intelligence)](https://github.com/C3n7ral051nt4g3ncy/cURL_for_OSINT)
----

### Devices
1. [Shodan](https://www.shodan.io/)
2. [WiGLE: Wireless Network Mapping](https://wigle.net/)
3. [Shodan Images](https://images.shodan.io/)
4. [jakejarvis/awesome-shodan-queries: 🔍 A collection of interesting, funny, and depressing search que...](https://github.com/jakejarvis/awesome-shodan-queries)
5. [What is my IP address, country, operating system, browser? Lookup MAC addresses](https://aruljohn.com/)
----

### Mobile
1. [OpenCelliD - Largest Open Database of Cell Towers & Geolocation - by Unwired Labs](https://opencellid.org/#zoom=16&lat=37.77889&lon=-122.41942)
2. [azmatt/Anaximander: Python Code to Map Cell Towers From a Cellebrite Android Dump](https://github.com/azmatt/Anaximander)
3. [Genymotion For Fun – Free Android Emulator (For Personal Use)](https://www.genymotion.com/fun-zone/)
----

### Text
1. [GitHub - tesseract-ocr/tesseract: Tesseract Open Source OCR Engine (main repository)](https://github.com/tesseract-ocr/tesseract)
2. [Free Online OCR - Image to text or PDF to Doc converter](https://www.onlineocr.net)
----

### Malware
1. [ANY.RUN - Interactive Online Malware Sandbox](https://any.run/)
----

### Metadata
1. [YouTube Metadata](https://mattw.io/youtube-metadata/)
2. [InVID Verification Plugin - InVID project](https://www.invid-project.eu/tools-and-services/invid-verification-plugin/)
----

### Software
1. [Homepage - Maltego](https://www.maltego.com/)
----

### API
1. [Community API](https://developer.greynoise.io/reference/community-api?utm_medium=email&_hsmi=117811933&utm_content=117811933&utm_source=hs_email#)
2. [public-apis/public-apis: A collective list of free APIs](https://github.com/public-apis/public-apis)
3. [Zip Code API - Free Access to Worldwide Postal Code Data](https://zipcodebase.com/)
4. [Postman | The Collaboration Platform for API Development](https://orange-firefly-886125.postman.co/home)
----

### Linux
1. [Home | Linux Journey](https://linuxjourney.com)
2. [Ryan's Tutorials - A bunch of Technology Tutorials](https://ryanstutorials.net)
----

### SearchEngine
1. [OSINT Resources — Learn All The Things](https://www.learnallthethings.net/osint-resources)
2. [Google Hacking Database (GHDB) - Google Dorks, OSINT, Recon](https://www.exploit-db.com/google-hacking-database)
3. [Search Engine Colossus : Directory of Search Engines](https://www.searchenginecolossus.com/)
4. [Wolfram|Alpha: Computational Intelligence](https://www.wolframalpha.com/)
5. [Search Engine for Source Code - PublicWWW.com](https://publicwww.com/)
6. [Searx - Privacy-respecting metasearch engine](https://searx.me/)
7. [eTools.ch - The Transparent Metasearch Engine from Switzerland](https://www.etools.ch/)
8. [Search Engine for Source Code - PublicWWW.com](https://publicwww.com/)
9. [Home - YaCy](https://yacy.net/)
10. [searx.info](https://searx.info/)
11. [Searx - Privacy-respecting metasearch engine](https://searx.me/)
12. [eTools.ch - The Transparent Metasearch Engine from Switzerland](https://www.etools.ch/)
13. [The YouTube Channel Crawler](https://www.channelcrawler.com/eng)
14. [People Search / OSINT Investigations Tool by Bob Brasich @goFindWho](https://gofindwho.com/)
15. [Lyzem Search](https://lyzem.com/)
16. [CSE Utopia - start.me](https://start.me/p/EL84Km/cse-utopia)
17. [World Social Media Platforms | OSINT Combine](https://www.osintcombine.com/world-social-media-platforms)
18. [Trending Hashtags Instagram Twitter Facebook | HashAtIt](https://www.hashatit.com)
19. [I Search From: custom location, language, device & personalization Google Search tool to preview ad...](http://isearchfrom.com)
20. [eTools.ch - Information about Federated Search](https://www.etools.ch/searchInfo.do)
21. [I Search From: custom location, language, device & personalization Google Search tool to preview ad...](http://isearchfrom.com)
22. [People Search / OSINT Investigations Tool by Bob Brasich @goFindWho](https://gofindwho.com)
23. [DorkSearch - Speed up your Dorking](https://dorksearch.com)
24. [Stefanie Proto's Custom Search Engines - Google Sheets](https://docs.google.com/spreadsheets/d/1fBPz6KHsFXryhu6JNrj1l-Rl04bEKLfinyCgCIqTyzU/edit#gid=436019663)
----

### Databases
1. [Catalogue of Research Databases | OCCRP ID](https://id.occrp.org/databases/)
2. [Dataset Search](https://datasetsearch.research.google.com/)
----

### Social Media
1. [Trending Hashtags Instagram Twitter Facebook | HashAtIt](https://www.hashatit.com)
2. [Followerwonk: Tools for Twitter Analytics, Bio Search and More](https://followerwonk.com)
----

### Facebook
1. [Fuck Facebook (TM)](https://4wbwa6vcpvcr3vvf4qkhppgy56urmjcj2vagu2iqgp3z656xcmfdbiqd.onion.pet/search?i=&f=&l=Hennecken&t=&w=&o=&s=187882000&r=*any*&g=*any*)
2. [Facebook Tools - Best Free Tools for Facebook](https://commentpicker.com/facebook-tools.php)
3. [Who posted what?](https://whopostedwhat.com)
4. [Facebook Search](https://www.sowsearch.info)
----

### Instagram
1. [Bibliogram](https://bibliogram.pussthecat.org/)
2. [Instagram Tools - Best Free Tools for Instagram](https://commentpicker.com/instagram-tools.php)
3. [download instagram stories highlights, photos and videos online - ImgInn.com](https://imginn.com)
4. [Instagram Photo, Video, Stories & Reels Downloader](https://instabig.net)
----

### Twitter
1. [nitter](https://nitter.net/)
2. [GitHub - twintproject/twint: An advanced Twitter scraping & OSINT tool written in Python that doesn...](https://github.com/twintproject/twint)
3. [TweetBeaver - Home of Really Useful Twitter Tools](https://tweetbeaver.com/)
4. [GitHub - igorbrigadir/twitter-advanced-search: Advanced Search for Twitter.](https://github.com/igorbrigadir/twitter-advanced-search)
5. [All My Tweets - View all your tweets on one page.](https://www.allmytweets.net/connect/)
6. [Analysis of Twitter Accounts | accountanalysis](https://accountanalysis.app/)
7. [Twitonomy: Twitter #analytics and much more...](https://www.twitonomy.com/)
8. [Spoonbill](http://spoonbill.io/)
9. [Mentionmapp Analytics | Social Network Analysis & Insights](https://mentionmapp.com/)
10. [#onemilliontweetmap](https://onemilliontweetmap.com/?center=25.505,-0.09&zoom=2&search=&timeStep=0&timeSelector=0&hashtag1=&hashtag2=sad&sidebar=yes&hashtagBattle=0&timeRange=0&timeRange=25&heatmap=0&sun=0&cluster=1)
11. [BirdHunt | Find Tweets By Location](https://birdhunt.co)
12. [HEAVY.AI | Tweetmap](https://www.heavy.ai/demos/tweetmap)
13. [nitter](https://nitter.net)
14. [Instances · zedeus/nitter Wiki · GitHub](https://github.com/zedeus/nitter/wiki/Instances)
15. [memory.lol/tw/username](https://memory.lol/tw/username)
16. [TweeterID - Twitter ID and username converter](https://tweeterid.com)
17. [Twitter Tools - Best Free Tools for Twitter](https://commentpicker.com/twitter-tools.php)
18. [Tweet Hunter | Get More Twitter Followers | Tweets, Threads, Scheduler, Analytics](https://tweethunter.io)
19. [Twitter Analytics for Tweets, Timelines & Twitter Maps | Social Bearing](https://socialbearing.com)
20. [falkensmz/tw1tter0s1nt: Python tool that automates the process of Twitter OSiNT investigation using...](https://github.com/falkensmz/tw1tter0s1nt)
----

### Telegram
1. [fabledowl’s gists · GitHub](https://gist.github.com/fabledowl)
2. [ИССЛЕДОВАНИЕ TELEGRAM - start.me](https://start.me/p/NxkOl7/telegram)
3. [TELEGRAM OSINT - start.me](https://start.me/p/YaOYnJ/telegram-osint)
4. [estebanpdl/tg-api: It connects to Telegram's API. It generates JSON files containing channel's data...](https://github.com/estebanpdl/tg-api)
5. [Alb-310/Geogramint: An OSINT Geolocalization tool for Telegram that find nearby users and groups 📡...](https://github.com/Alb-310/Geogramint)
6. [TELEGRAM OSINT - start.me](https://start.me/p/YaOYnJ/telegram-osint)
----

### Archive
1. [GitHub - dessant/web-archives: Browser extension for viewing archived and cached versions of web pa...](https://github.com/dessant/web-archives)
2. [Wayback Machine](https://web.archive.org/)
3. [GitHub - ArchiveBox/ArchiveBox: 🗃 Open source self-hosted web archiving. Takes URLs/browser histor...](https://github.com/ArchiveBox/ArchiveBox)
----

### Analytics
1. [Reverse Google Analytics](https://osint.sh/analytics)
2. [Find Other Websites Owned By The Same Person](https://analyzeid.com)
----

### Sites
1. [We are OSINTCurio.us – Helping the OSINT community stay curious](https://osintcurio.us/)
2. [Home - Technisette website](https://www.technisette.com/p/home)
3. [OSINT Techniques - Home](https://www.osinttechniques.com/)
4. [OSINT Open Source Intelligence tools resources methods techniques](http://rr.reuser.biz/)
----

### Certificates
1. [Introducing another free CA as an alternative to Let's Encrypt](https://scotthelme.co.uk/introducing-another-free-ca-as-an-alternative-to-lets-encrypt/)
2. [crt.sh | Certificate Search](https://crt.sh/)
3. [Entrust](https://www.entrust.com/)
4. [Home - Censys](https://censys.io/)
----

### IP
1. [ViewDNS.info - Your one source for DNS related tools!](https://viewdns.info/)
2. [DNSdumpster.com - dns recon and research, find and lookup dns records](https://dnsdumpster.com/)
3. [SpyOnWeb.com Research Tool - Internet Competitive Intelligence](https://spyonweb.com/)
4. [URL and website scanner - urlscan.io](https://urlscan.io/)
5. [Welcome to Robtex!](https://www.robtex.com/)
6. [Dashboard - host.io](https://host.io/dashboard)
7. [#1 Domain, Whois, IP, DNS, and Cyber Threat Intelligence Provider | WhoisXML API](https://main.whoisxmlapi.com/)
8. [Locate IP Address Lookup Show on Map City of the IP 159.180.48.55](https://infosniper.net/index.php)
9. [Spyse — Internet Assets Search Engine](https://spyse.com/)
10. [MX Lookup Tool - Check your DNS MX Records online - MxToolbox](https://mxtoolbox.com)
11. [Network-Tools.com: Ping, Traceroute, WHOIS & More](https://network-tools.com)
12. [Netlas](https://app.netlas.io/responses)
13. [iphostinfo Cloudflare resolv](https://iphostinfo.com)
----

### Phone
1. [Welcome to FreeCarrierLookup.com](https://freecarrierlookup.com/)
2. [Phonebook.cz - Intelligence X](https://phonebook.cz/)
3. [Telefoonnummers zoeken | ACM.nl](https://www.acm.nl/nl/onderwerpen/telecommunicatie/telefoonnummers/nummers-doorzoeken)
4. [Download Profile Picture - WhatsApp Tools](https://watools.io/download-profile-picture)
5. [megadose/ignorant: ignorant allows you to check if a phone number is used on different sites like s...](https://github.com/megadose/ignorant)
----

### Username
1. [WhatsMyName Web](https://whatsmyname.app/)
2. [GitHub - sherlock-project/sherlock: 🔎 Hunt down social media accounts by username across social ne...](https://github.com/sherlock-project/sherlock)
3. [qeeqbox/social-analyzer: API, CLI & Web App for analyzing & finding a person's profile across socia...](https://github.com/qeeqbox/social-analyzer)
4. [GitHub - thewhiteh4t/nexfil: OSINT tool for finding profiles by username](https://github.com/thewhiteh4t/nexfil)
5. [soxoj/maigret: 🕵️‍♂️ Collect a dossier on a person by username from thousands of sites](https://github.com/soxoj/maigret)
----

### Email
1. [Email Permutator | Metric Sparrow Toolkit](http://metricsparrow.com/toolkit/email-permutator/)
2. [Find email addresses in seconds • Hunter (Email Hunter)](https://hunter.io/)
3. [Simple Email Reputation](https://emailrep.io/)
4. [Email Checker | Hunter Email | Bounce Checker | Email Deliverability - MailTester](https://mailtester.com/en/)
5. [megadose/holehe: holehe allows you to check if the mail is used on different sites like twitter, in...](https://github.com/megadose/holehe)
6. [GitHub - Malfrats/xeuledoc: Fetch information about a public Google document.](https://github.com/Malfrats/xeuledoc)
7. [mxrch/GHunt: 🕵️‍♂️ Offensive Google framework.](https://github.com/mxrch/GHunt)
8. [GitHub - C3n7ral051nt4g3ncy/Prot1ntelligence: Prot1ntelligence is a Python script for the OSINT & C...](https://github.com/C3n7ral051nt4g3ncy/Prot1ntelligence)
9. [Simple Email Reputation](https://emailrep.io)
10. [GetEmail.io – We use Big Data & AI to Find the Email of Anyone on Earth](https://getemail.io)
----

### OPSEC
1. [Wget Wizard - WhatIsMyBrowser.com](https://www.whatismybrowser.com/developers/tools/wget-wizard/)
2. [10 Best Antivirus Software [2020]: Windows, Android, iOS & Mac](https://www.safetydetectives.com/)
3. [NixNet](https://nixnet.services/)
4. [OPSEC is for everyone, not just those with something to hide-Pt 3](https://www.tripwire.com/state-of-security/security-awareness/opsec-everyone-people-something-hide-part-3/)
5. [Epic Privacy Browser, a secure chromium-based web browser with Encrypted Proxy and AdBlock | a free...](https://www.epicbrowser.com)
6. [Browserling – Online cross-browser testing](https://www.browserling.com)
7. [Easy Web Browsing From Multiple Locations | LocaBrowser](https://locabrowser.com)
----

### Sock puppet
1. [What will my baby look like? Morph Faces and Celebrities Online For Free - MorphThing.com](https://www.morphthing.com/?)
2. [Generated Photos | Unique, worry-free model photos](https://generated.photos)
3. [This Person Does Not Exist](https://thispersondoesnotexist.com)
4. [This Baseball Player Does Not Exist](https://thisbaseballplayerdoesnotexist.com)
5. [This X Does Not Exist](https://thisxdoesnotexist.com)
----

### Training
1. [Sign In - My OSINT Training (MOT)](https://www.myosint.training/users/sign_in)
----

### Webpages
1. [BuiltWith Pro](https://pro.builtwith.com/)
2. [Home - Censys](https://censys.io/)
3. [Mobile-Friendly Test – Google Search Console](https://search.google.com/test/mobile-friendly)
4. [Monitor Website Changes for Free ← Versionista](https://versionista.com/)
5. [Netcraft | Internet Research, Cybercrime Disruption and PCI Security Services](https://www.netcraft.com/)
6. [Threat Intelligence - Pulsedive](https://pulsedive.com/)
7. [Visualping: #1 Website change detection, monitoring and alerts](https://visualping.io/)
8. [Technology lookup - Wappalyzer](https://www.wappalyzer.com/)
9. [Blacklight – The Markup](https://themarkup.org/blacklight)
10. [FouAnalytics - Ads By Domain](https://pagexray.fouanalytics.com/)
11. [Attention Required! | Cloudflare](https://codebeautify.org/)
12. [LargerIO Technology Lookup](https://www.larger.io/)
13. [Technology Lookup Online - Know The Website Technology and More!](https://tools.cmlabs.co/en/technology-lookup)
14. [URL and website scanner - urlscan.io](https://urlscan.io)
15. [FouAnalytics - Ads By Domain](https://pagexray.fouanalytics.com)
16. [NerdyData.com - Search The Web's Source Code for Technologies](https://www.nerdydata.com)
17. [Search Engine for Source Code - PublicWWW.com](https://publicwww.com)
18. [Reverse Analytics Search](https://hackertarget.com/reverse-analytics-search)
19. [simonw/shot-scraper: Tools for taking automated screenshots of websites](https://github.com/simonw/shot-scraper)
----

### Blog
1. [@hatless1der | Blog – Open Source Intelligence For Everyone](https://hatless1der.com/)
2. [NixIntel – OSINT, Linux, Digital Forensics, and InfoSec](https://nixintel.info/)
3. [On Scene – OSINT Blog by AthenasOwl_97](https://athenasowl97.com)
4. [UNREDACTED magazine](https://unredactedmagazine.com/download.html)
5. [Articles](https://sector035.nl/articles/category:week-in-osint)
----

### Communities
1. [Searchlight Community](https://searchlight.community/)
2. [Searchlight](https://discord.com/invite/BtFpNJ2)
3. [Offensive Security](https://discord.com/invite/ctBUcPuakZ?utm_content=167993147&utm_medium=social&utm_source=twitter&hss_channel=tw-134994790)
4. [OSINT Dojo](https://www.osintdojo.com/resources)
----

### Learning
1. [post_fact's OSINT primer - Google Docs](https://docs.google.com/document/d/1wnEbVu_hrLdfDFIeG5LPeSvxxkMsu6IP0lW1XWRNDRU/edit)
2. [A Beginner's Guide to Social Media Verification - bellingcat](https://www.bellingcat.com/resources/2021/11/01/a-beginners-guide-to-social-media-verification)
3. [First Steps to Getting Started in Open Source Research - bellingcat](https://www.bellingcat.com/resources/2021/11/09/first-steps-to-getting-started-in-open-source-research)
4. [OSINT Dojo - YouTube](https://www.youtube.com/c/OSINTDojo/about)
----

### Tutorials
1. [Advocacy Assembly](https://advocacyassembly.org/en)
2. [Moving Past Just Googling It: Harvesting and Using OSINT | SANS@MIC Talk - YouTube](https://www.youtube.com/watch?v=BVFhMqktkeE)
3. [OSINT At Home - Tutorials on Digital Research, Verification and Open Source Investigations - YouTube](https://www.youtube.com/playlist?list=PLrFPX1Vfqk3ehZKSFeb9pVIHqxqrNW8Sy)
----

### OSINT-Info
1. [OSINT Dojo](https://www.osintdojo.com/resources/)
2. [Bellingcat's Online Investigation Toolkit [bit.ly/bcattools] - Google Sheets](https://docs.google.com/spreadsheets/d/18rtqh8EG2q1xBo2cLNyhIDuK9jrPGwYr9DI2UncoqJQ/edit)
3. [We are OSINTCurio.us – Helping the OSINT community stay curious](https://osintcurio.us/)
4. [Free Tools | OSINT Combine](https://www.osintcombine.com/tools)
5. [Links | Sector035](https://sector035.nl/links)
6. [Technisette](https://www.technisette.com/p/home)
7. [Open-Source Intelligence Summit 2021 - Shared Resources | SANS Institute](https://www.sans.org/blog/list-of-resource-links-from-open-source-intelligence-summit-2021/)
8. [Malfrat's OSINT Map](https://map.malfrats.industries)
9. [Recommended OSINT Tools and Processes | OS2INT](https://os2int.com/toolbox)
10. [Home - OSINT Foundation](https://www.osintfoundation.com/osint/default.asp)
11. [OSINT Dojo](https://www.osintdojo.com)
----

### OSINT-StartMe
1. [OSINT INCEPTION 🔍 - start.me](https://start.me/p/Pwy0X4/osint-inception)
2. [OSINT4ALL - start.me](https://start.me/p/L1rEYQ/osint4all)
3. [Nixintel's OSINT Resource List - start.me](https://start.me/p/rx6Qj8/nixintel-s-osint-resource-list)
4. [The Ultimate OSINT Collection - start.me](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
5. [commandergirl's suggestions - start.me](https://start.me/p/1kJKR9/commandergirl-s-suggestions)
6. [OSINT-GLOBAL (Non-US) - start.me](https://start.me/p/jj2XEr/osint-global-non-us)
7. [Deepweb en tools - start.me](https://start.me/p/ydJLqE/deepweb-en-tools)
8. [OSINT - start.me](https://start.me/p/b56xX8/osint)
9. [OSINT - start.me Digintel](https://start.me/p/ZME8nR/osint)
10. [01 NCSO - start.me](https://start.me/p/BnrMKd/01-ncso)
11. [OsintNOR - start.me](https://start.me/p/0PM7bl/osintnor)
12. [TELEGRAM OSINT - start.me](https://start.me/p/YaOYnJ/telegram-osint)
13. [AsINT_Collection - start.me](https://start.me/p/b5Aow7/asint_collection)
14. [Verification Toolset - start.me](https://start.me/p/ZGAzN7/verification-toolset)
15. [Online-Recherche Newsletter - start.me](https://start.me/p/m65arv/online-recherche-newsletter)
----

### Bookmarks
1. [Install and use our bookmarklet – start.me Help Centre](https://support.start.me/hc/en-us/articles/200964881-Install-and-use-our-bookmarklet)
2. [Making Bookmarklets](https://gist.github.com/caseywatts/c0cec1f89ccdb8b469b1)
3. [Bookmarklet Creator with Script Includer - Peter Coles](https://mrcoles.com/bookmarklet)
4. [OSINT Bookmarks | OSINT Combine](https://www.osintcombine.com/osint-bookmarks)
5. [Bellingcat's Online Investigation Toolkit [bit.ly/bcattools] - Google Sheets](https://docs.google.com/spreadsheets/d/18rtqh8EG2q1xBo2cLNyhIDuK9jrPGwYr9DI2UncoqJQ/edit#gid=930747607)
6. [Dating apps and hook-up sites for investigators - start.me](https://start.me/p/VRxaj5/dating-apps-and-hook-up-sites-for-investigators)
7. [01 OSINT Hub - start.me](https://start.me/p/BnrMKd/01-osint-hub)
----

