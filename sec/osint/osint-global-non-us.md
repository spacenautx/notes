### Albania
1. [Albania REGISTRY](http://www.qkr.gov.al/)
----

### Algeria
1. [Accueil](https://sidjilcom.cnrc.dz/)
----

### Antigua & Barbuda
1. [Antigua and Barbuda Intellectual Property & Commerce | 
            ABIPCO](https://abipco.gov.ag/)
----

### Argentina
1. [Argentina.gob.ar](http://www.argentina.gov.ar)
2. [Cnv.gov.ar/](http://www.cnv.gov.ar/)
----

### Australia
1. [ABN Lookup](http://www.abr.business.gov.au/)
2. [ASIC ALerts](http://www.search.asic.gov.au/misccgi/alert.html)
3. [ASIC Home](http://www.asic.gov.au/asic/asic.nsf)
4. [Australia Public Record Search](http://australiapublicrecord.com/)
5. [Business names](http://www.asic.gov.au/asic/asic.nsf/byheadline/Introduction+to+ASIC+Connect?openDocument)
6. [Business Register](http://www.abr.business.gov.au)
7. [eSearch Pty Ltd](http://www.esearch.net.au/)
8. [Free People Search](https://www.nettrace.com.au/resource/search/people/)
9. [Humanrights.gov.au/publications/human-rights-record-recruitment-chapter-5](https://www.humanrights.gov.au/publications/human-rights-record-recruitment-chapter-5)
10. [legal: Searches for prosecutions of companies and directors](http://www.fido.gov.au/asic/asic.nsf/byheadline/Registers?openDocument#companies)
11. [Publicrecords.com.au](https://www.publicrecords.com.au/)
12. [Publicrecords.searchsystems.net/Other_Nations/Australia_Free_Public_Records/Western-Australia-Records](http://publicrecords.searchsystems.net/Other_Nations/Australia_Free_Public_Records/Western-Australia-Records/)
13. [Registers](http://www.fido.gov.au/asic/asic.nsf/byheadline/Registers?openDocument#companies)
14. [Reverse Australia](http://www.matespotter.com)
----

### Austria
1. [AT: FMA - Aktuelle Investorenwarnungen](https://www.fma.gv.at/category/news/?cat=42&filter-dropdown-year=&filter-dropdown-order=date_desc)
2. [Beneficial Owner AT](https://www.bmf.gv.at/finanzmarkt/WiEReG.html)
3. [Die Östereichische Justiz](https://www.justiz.gv.at)
4. [Firmenbuch (Handelsregister)](https://firmenbuch.at/)
5. [Herold.at Gelbe Seiten](https://www.herold.at/)
6. [Imd Information Medien Datenverarbeitung](https://www.imd.at/)
7. [Jusline.at Rechtsportal](https://www.jusline.at/)
8. [Justiz.gv.at Firmenbuch](https://www.justiz.gv.at/web2013/html/default/8ab4a8a422985de30122a90fc2ca620b.de.html)
9. [KSV](https://www.myksv.at)
10. [KSW](https://portal.ksw.or.at)
11. [Lobbying](http://lobbyreg.justiz.gv.at)
12. [PVP Standardportal - Stammportal - Login-Seite](https://bportal.zmr.register.gv.at/at.gv.bmi.fnsweb-p/zmr/checked/Meldeauskunft)
13. [UID Suche](http://uid-suche.at/)
----

### Belgium
1. [BE Roularta Eurodb](http://www.eurodb.be/)
2. [BELGIUM Finanzen.belgium.be](https://finanzen.belgium.be/de/E-services/registre-ubo)
3. [Check Passports](https://www.checkdoc.be)
----

### Bosnia
1. [bizreg.pravosudje.ba](https://bizreg.pravosudje.ba)
2. [Bosnia Herzegovina REGISTRY Privredna / Gospodarska Komora Federacije Bosne i Hercegovine](http://www.kfbih.com/)
----

### Brazil
1. [3rd federal Regional Court](http://web.trf3.jus.br/consultas/Internet/ConsultaProcessual)
2. [Acompanhamento Processual :: STF Supreme Court](http://www.stf.jus.br/portal/processo/pesquisarProcesso.asp)
3. [Atestado de antecedentes criminais Sao Paulo (SSP)](http://www.ssp.sp.gov.br/servicos/atestado.aspx)
4. [Boa Vista SCPC](https://www.boavistaservicos.com.br/)
5. [Certidão de Antecedentes Criminais](https://servicos.dpf.gov.br/antecedentes-criminais/certidao)
6. [CNDT Certidão Negativa de Débitos Trabalhistas](http://www.tst.jus.br/certidao)
7. [Cnpj.info](http://cnpj.info)
8. [COAF Ministry of Finance](https://comprot.fazenda.gov.br/comprotegov/site/index.html#ajax/processo-consulta.html)
9. [Commercial Registry of Paraíba](http://www.redesim.pb.gov.br/)
10. [Company Registry in Brazil Explained](https://thebrazilbusiness.com/article/company-register-in-brazil)
11. [Consulta à Certidão Negativa de Débito (CND)](http://cnd.dataprev.gov.br/cws/contexto/cnd/cnd.html)
12. [Consulta.procob.com Commercial](http://consulta.procob.com)
13. [Divulgação de Candidaturas e Contas Eleitorais Electoral Court](http://divulgacandcontas.tse.jus.br/divulga/#/consulta/doadores-fornecedores/2)
14. [Emissão de Comprovante de Inscrição e de Situação Cadastral](http://www.receita.fazenda.gov.br/PessoaJuridica/CNPJ/cnpjreva/Cnpjreva_Solicitacao.asp)
15. [FGTS  Federal Savings Bank](https://www.sifge.caixa.gov.br/Cidadao/Crf/FgeCfSCriteriosPesquisa.asp)
16. [Google br Media Review](https://www.google.com.br/)
17. [IBAMA SICAFI  Brazilian Institute of Environment and Natural Resources](https://servicos.ibama.gov.br/sicafiext/)
18. [Jusbrasil. Conectando Pessoas à Justiça](https://www.jusbrasil.com.br/home)
19. [Jucees Commercial Registry of Espírito Santo](https://www.jucees.es.gov.br/)
20. [Jucerja Commercial Registry of Rio de Janeiro](https://www.jucerja.rj.gov.br/JucerjaPortalWeb/default.aspx)
21. [JUCEMG Portal de Serviços](http://portalservicos.jucemg.mg.gov.br/Portal/login.jsp?josso_back_to=http://portalservicos.jucemg.mg.gov.br/Portal/josso_security_check)
22. [Jucesp Commercial Registry of Sao Paulo](http://www.institucional.jucesp.sp.gov.br/homepage.php)
23. [Pesquisa de NIRE](http://www.juntacomercial.pr.gov.br/modules/conteudo/conteudo.php?conteudo=240)
24. [Pesquisa do Diário Oficial de Minas Gerais](http://jornal.iof.mg.gov.br/xmlui)
25. [Portal da Polícia Federal](http://www.dpf.gov.br)
26. [Portal da Transparência CEIS Transparency Portal](http://www.portaldatransparencia.gov.br/)
27. [Receita Federal do Brasil / Brazilian Federal Revenue Office](http://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/cnpjreva_solicitacao.asp)
28. [Receita Federal IRS](https://cav.receita.fazenda.gov.br/eCAC/publico/login.aspx?sistema=00015)
29. [Registro.br WHOIS Ponto BR](https://registro.br/2/whois#/lresp)
30. [Serasa Experian Brasil ($)](https://www.serasaexperian.com.br/)
31. [Significado de Órgão Expedidor](https://www.significados.com.br/orgao-expedidor/)
32. [SINTEGRA ICMS (Taxpayer  Registration on Value-added Tax on Sales and Services](http://www.sintegra.gov.br/)
33. [Superior Tribunal  Superior Court of Justice](http://www.stj.jus.br/portal/site/STJ)
34. [Tribunal de Justiça de São Paulo Court of Justice](http://www.tjsp.jus.br/)
35. [Tribunal Superior Eleitoral Electoral Court Main page](http://inter01.tse.jus.br)
----

### British Virgin Islands
1. [Bvifsc.vg](http://www.bvifsc.vg/)
----

### Burkina Faso
1. [Annonces Légales | me.bf](http://www.me.bf/en/annonces-legales)
2. [Rechercher une société | CCI BF](http://www.cci.bf/?q=fr/entreprise)
----

### Cameroon
1. [Ccima.cm/phocadownload/LISTES%20PRINCIPALES%20ENTREPRISES%20CAMEROUNAISES%20DGE.pdf](http://www.ccima.cm/phocadownload/LISTES%20PRINCIPALES%20ENTREPRISES%20CAMEROUNAISES%20DGE.pdf)
----

### Canada
1. [Home](http://corporationscanada.ic.gc.ca/eic/site/cd-dgc.nsf/eng/home)
----

### Cayman Islands
1. [Cayman Compass](https://www.caymancompass.com/)
2. [CI Registry](https://www.ciregistry.ky/)
3. [https://www.cogencyglobal.com/blog/what-information-is-available-for-a-cayman-islands-company](https://www.cogencyglobal.com/blog/what-information-is-available-for-a-cayman-islands-company)
4. [http://online.gov.ky](http://online.gov.ky)
----

### Costa Rica
1. [Bolsacr.com](http://bolsacr.com/)
2. [Inicio](http://amcham.cr/)
3. [personas_juridicas_consultas](https://www.rnpdigital.com/personas_juridicas/index.htm)
4. [Portal Imprenta Nacional](http://www.gaceta.go.cr)
----

### China (PRC)
1. [Baidu Map (Social Media)](http://image.baidu.com/?fr=shitu)
2. [Renren (Social Media)](http://www.renren.com/)
3. [QZone (Social Media)](http://www.qq.com/)
4. [QZone / QQ (Social Media)](http://qzone.qq.com/)
5. [Weibo (Social Media)](http://weibo.com/)
6. [Xing (Social Media)](https://www.xing.com/)
----

### Croatia
1. [Biznet Corporate Registry](http://www.biznet.hr/)
2. [Croatia REGISTRY sudreg.pravosudje.hr](https://sudreg.pravosudje.hr)
3. [Croatian Companies House (Companies Registry) | Venture Croatia](http://assono.hr/blog/croatian-companies-house-companies-registry-Croatia)
4. [Croatian register of shipping > Home](http://www.crs.hr/en-us/home.aspx)
5. [CompanyWall Business - Bonitetne informacije tvrtki](http://www.companywall.hr)
----

### Cyprus
1. [Department of Registrar of Companies and Official Receiver](http://www.mcit.gov.cy/mcit/drcor/drcor.nsf/index_en/index_en)
----

### Czech Republic
1. [ČÚZK - Cadastral Offices](http://www.cuzk.cz/English/Offices/Cadastral-offices/Cadastral-offices.aspx)
2. [Czech-Republic Business/Business-entities](https://www.justlanded.com/english/Czech-Republic/Czech-Republic-Guide/Business/Business-entities)
3. [Czech telephone directory](http://phone.fin.cz/)
4. [Frequenz tschechischer Namen](http://tschechischblog.de/kurioses/die-top-ten-tschechischer-nachnamen/)
5. [Gesellschaftsformen Tschechien](https://www.company-worldwide.com/leistungen/gesellschaften/tschechische-gesellschaften/)
6. [Justice.cz Firmendaten und Insolvenzen](http://portal.justice.cz/Justice2/Uvod/uvod.aspx)
7. [Obchodní rejstřík firem Handelsregister](http://obchodnirejstrik.cz/)
8. [Prijmeni.unas.cz/index.htm Top 100 Nachnamen](http://prijmeni.unas.cz/index.htm)
9. [Protimonopolný úrad Slovenskej republiky | Protimonopolný úrad SR](http://www.antimon.gov.sk)
10. [Rechtsformen-der-unternehmen-der-tschechischen-republik](http://www.tschechien-wirtschaft.de/article/rechtsformen-der-unternehmen-der-tschechischen-republik)
11. [Rzp.cz Trade Ministry Personensuche](http://www.rzp.cz)
12. [Seznam](https://www.seznam.cz/)
13. [Seznam všech organizací na jednom místě Czech Organisations](http://www.sorganizace.cz/)
14. [Veřejný rejstřík a Sbírka listin Registerdaten](https://or.justice.cz/ias/ui/rejstrik)
15. [Vizuální obchodní rejstřík, vizualizace vztahů firem a osob](https://www.podnikani.cz/)
16. [Zlaté stránky](http://www.zlatestranky.cz/)
----

### Denmark
1. [DENMARK Datacvr.virk.dk](https://datacvr.virk.dk)
2. [mit Virk](https://datacvr.virk.dk/data/)
3. [Registrering af ejerforhold](https://erhvervsstyrelsen.dk/det-offentlige-ejerregister)
----

### Dominica (Antillen)
1. [Dominica Companies: Dominica Company/Dominica IBC/Corporation Formation](https://dominica-corporations-ibc-incorporate-in-dominica.offshore-companies.co.uk/)
2. [Home](http://www.cipo.gov.dm/)
----

### France
1. [french-leader](http://french-leader.com/)
2. [Infogreffe](https://www.infogreffe.fr/)
3. [OPen Data France www.data.gouv.fr](https://www.data.gouv.fr/fr/search/?q=geolocalisation)
4. [PagesJaunes Gelbe Seiten](http://www.pagesjaunes.fr/)
5. [Property in France cadastre.gouv.fr](https://cadastre.gouv.fr/scpc/accueil.do)
6. [www.societe.com](http://www.societe.com)
7. [Yahoo Search France](http://fr.search.yahoo.com/?fr=altavista)
----

### Germany
1. [Aleph.occrp.org](https://aleph.occrp.org/)
2. [Anti-Money Laundering](https://www.anti-gw.de/)
3. [BaFin](http://www.bafin.de)
4. [Bankruptcy Announcements](https://www.insolvenzbekanntmachungen.de/)
5. [Bankruptcy Records](https://insolvex.de/)
6. [Business Register](https://www.unternehmensregister.de/ureg/)
7. [Commercial Register (Handelsregister)](https://www.handelsregister.de/rp_web/welcome-members.do;jsessionid=BE0A60EEEE0BD1140C00402C22957467-n1.tc032n01)
8. [Commercial Register Announcements (Handelsregister)](http://www.handelsregisterbekanntmachungen.de/?aktion=suche)
9. [Commercial Register Data](https://offeneregister.de/)
10. [Compliance Digital](https://www.compliancedigital.de/)
11. [Corruption Perception Index](https://www.wikiwand.com/de/Korruptionswahrnehmungsindex)
12. [Crunchbase (Business Lookup)](https://www.crunchbase.com/#/home/index)
13. [Debtor Register](https://www.ra-micro.de/erlaeuterungen-zu-negativmerkmalen-aus-dem-schuldnerverzeichnis/)
14. [Deutsche-boerse](http://deutsche-boerse.com)
15. [Deutschen Bundesbank - Restrictions on Capital & Payments](http://www.bundesbank.de)
16. [DICO e.V.](http://www.dico-ev.de/)
17. [e-justice.europa](https://e-justice.europa.eu)
18. [Federal Gazette](https://www.bundesanzeiger.de/ebanzwww/wexsservlet)
19. [Federal Ministry for Economics and Technology (BMWi), Referat V B 2](http://www.bwmi.bund.de)
20. [Federal Office of Economics and Export Control (BAFA)](https://www.bafa.de/DE/Aussenwirtschaft/aussenwirtschaft_node.html)
21. [FMF - Anti Money Laundering](https://www.bundesfinanzministerium.de/Content/DE/Downloads/Broschueren_Bestellservice/2019-10-19-erste-nationale-risikoanalyse_2018-2019.html)
22. [German Tax Offices](https://www.liste-finanzamt.de/)
23. [Germany Embargo & Sanction Lists](http://www.zolltarifnummern.de/embargo/overview.php)
24. [GS1 Standards](https://www.gs1-germany.de/gs1-standards/)
25. [info4c Compliance Info broker](https://www.info4c.net/de/)
26. [ING-DiBa](https://www.ing-diba.de/)
27. [InsolNet](http://www.insolnet.de/)
28. [Insolvenz-Portal](https://app.insolvenz-portal.de)
29. [LEI # Search  – GLEIF](https://www.gleif.org/de)
30. [Market Visual Search](http://www.marketvisual.com/)
31. [Melderegister Berlin (GER)](https://olmera.verwalt-berlin.de/std/Login/start.do)
32. [Northdata (GER)](https://www.northdata.de/)
33. [Obituaries - Aachen, Düren and Heinsberg region](https://www.aachen-gedenkt.de/)
34. [Obituaries - Stuttgart Gedenkt](https://www.stuttgart-gedenkt.de/)
35. [Obituaries - Trauer Niederrhein-nachrichten](https://www.trauer.niederrhein-nachrichten.de/)
36. [Obituaries - Trauer RP](https://trauer.rp-online.de/)
37. [Obituaries - Trauer WochenSpiegel](https://trauer.wochenspiegellive.de/)
38. [Obituaries - Wir Trauern](https://www.wirtrauern.de/)
39. [Open Ownership](https://www.openownership.org/)
40. [OpenCorporates : German Registry information](https://opencorporates.com/companies/de)
41. [RLP](https://rechnungshof.rlp.de/de/veroeffentlichungen/gutachten-und-stellungnahmen/gutachtliche-aeusserung-flughafen-frankfurt-hahn-gmbh-2017/)
42. [Site Leaks](http://www.siteleaks.com)
43. [Stay Friends (Germany School Connections)](https://www.stayfriends.de/login)
44. [Tax ID Check](https://ust-id-pruefen.de/)
45. [The Company Database AIHITDATA](https://www.aihitdata.com/)
46. [Transparency International Deutschland e.V.](https://www.transparency.de/)
47. [Transparency Register](https://www.transparenzregister.de)
48. [Transparency register (beneficial owners UBE)](https://www.transparenzregister.de)
----

### Gibraltar
1. [https://www.gibraltar.gov.gi/new/civil-status-registration-office](https://www.gibraltar.gov.gi/new/civil-status-registration-office)
2. [http://www.companieshouse.gi/publications/C0005.pdf](http://www.companieshouse.gi/publications/C0005.pdf)
3. [http://www.companieshouse.gi/](http://www.companieshouse.gi/)
4. [https://www.gibraltar.gov.gi/new/companies-house](https://www.gibraltar.gov.gi/new/companies-house)
5. [http://www.gibraltarship.com/](http://www.gibraltarship.com/)
6. [http://www.gibraltarport.com/ship-registry](http://www.gibraltarport.com/ship-registry)
----

### Hungary
1. [Bírósági Határozatok Gyűjteménye](http://birosag.hu/ugyfelkapcsolati-portal/anonim-hatarozatok-tara)
2. [Cégszolgálat Ingyenes Céginformáció Corporate Registry](http://www.e-cegjegyzek.hu/?cegkereses)
3. [Elektronikus Beszámoló Portál](http://e-beszamolo.im.gov.hu/oldal/kezdolap)
----

### India
1. [all Indian Newspapers](http://allindiannewspapers.com/)
2. [Check Company Name](http://www.mca.gov.in/mcafoportal/showCheckCompanyName.do)
3. [CIBIL Credit Information](https://www.cibil.com/)
4. [Easyhindityping.com](http://www.easyhindityping.com/)
5. [Fiai-india.org Financial intermediaries of India Association](http://www.fiai-india.org/)
6. [Indian Naming Conventions: Family Names and Given Names](http://learningindia.in/indian-naming-conventions/)
7. [MCA Login](http://www.mca.gov.in/mcafoportal/login.do)
8. [NBFC non-banking-financial-companies-in-india](http://www.yourarticlelibrary.com/economics/non-banking-financial-companies-in-india-types-and-supervision/23512/)
9. [Sam Richter India News Search](http://www.yougotthenews.com/india/)
10. [Securities and Exchange Board of India](http://www.sebi.gov.in)
11. [Verify DIN/DPIN-PAN Details Of Director](http://www.mca.gov.in/mcafoportal/showVerifyDIN.do)
12. [View Company Master Data](http://www.mca.gov.in/mcafoportal/viewCompanyMasterData.do)
----

### Indonesia
1. [Ahu.go.id/ Registry](https://ahu.go.id/)
2. [Arti kata  - Kamus Besar Bahasa Indonesia (KBBI) Online](https://www.kbbi.web.id/)
3. [Data Center Collection Ministry of Trade Republic of Indonesia](http://www.kemendag.go.id/en/perdagangan-kita/company-directory/data-center-collection/)
4. [Google Indonesia](https://www.google.co.id/)
5. [Indonesia Companies and Industry Market Research](http://www.disb2b.com/front/indonesiacompaniesindustyreports.php)
6. [Indonesian Chamber of Commerce & Industry](http://kadin-indonesia.or.id/anggota/anggota)
7. [Indonesia.searchenginessite.com](http://indonesia.searchenginessite.com/)
8. [Kementerian Perindustrian](http://kemenperin.go.id/direktori-perusahaan)
9. [List of Indonesian acronyms and abbreviations - Wikipedia](https://en.wikipedia.org/wiki/List_of_Indonesian_acronyms_and_abbreviations)
10. [Livinginindonesia.info/item/acronyms-abbreviations](https://livinginindonesia.info/item/acronyms-abbreviations)
11. [Shareholder Register](http://www.indonesiacompanylaw.com/tag/shareholder-register/)
12. [Yahoo Indonesia](https://id.yahoo.com/)
----

### Ireland
1. [Beneficial Ownership Status in Ireland - RBO](https://rbo.gov.ie/how-to.html)
----

### Islamic Republic of Iran
1. [موتور جستجو یوز - yooz Search Engine](https://yooz.ir/)
2. [صفحه نخست | موتور جستجوی ریسمون Rismoon Search Engine](http://www.rismoon.com/)
3. [سلام Salam Meta Search Engine](http://salam.ir/)
4. [Standard Chartered Bank Amended Deferred Prosecution Agreement](https://www.iranwatch.org/sites/default/files/sxb_amended_dpa_-_filed_0.pdf)
5. [پارسی جو: جستجوگر ایرانی Parsijoo Search Engine and e-commerce](http://parsijoo.ir/)
6. [Find list of Iran search engines with Search Engine Colossus](https://www.searchenginecolossus.com/Iran.html)
7. [Iranmehr.com](http://iranmehr.com/)
8. [Find Target Company ID](http://www.ilenc.ir/)
9. [روزنامه رسمی جمهوری اسلامی ایران Iranian Gazette](http://www.rrk.ir/)
10. [Search - Tehran Times](http://www.tehrantimes.com/search)
11. [Iran Daily Search](http://www.iran-daily.com/Search/Quick?s=news)
12. [Farsi Translator : English to Persian Dictionary](http://aryanpour.com/translator.php)
13. [Online Persian English Translator](http://www.etranslator.ro/persian-english-online-translator.php)
14. [English / Farsi Text Translator](http://www.dictionary-farsi.com/dynamic_english_farsi_translator.asp)
15. [Islamic Parliament of Iran (English)](http://en.parliran.ir/)
16. [Obtaining land deeds & registration](http://mobasher.ir/proceedings-denotative/)
17. [Tender جستجو پیشرفته مناقصه ها](http://www.ariatender.com/beta/tender-advanced-search.php)
18. [Iran Chamber Society: Iranian Calendar Converter](http://www.iranchamber.com/calendar/converter/iranian_calendar_converter.php)
19. [Parseek Search Engine](http://www.parseek.com/)
20. [Alphabetical List of Iranian Entities Iranwatch](https://www.iranwatch.org/iranian-entities)
21. [facenama (Social Media)](http://facenama.com/)
----

### Israel
1. [Engineer registry](https://apps.moital.gov.il/WebServicesHandlers/EngineersAndArchitects.aspx)
2. [Israeli Government Databases](https://data.gov.il/)
----

### Italy
1. [AGCM - Autorita' Garante della Concorrenza e del Mercato](https://www.agcm.it)
2. [Cerved: report aziende, recupero crediti e studi di settore](https://www.cerved.com/)
3. [Registro ImpreseVai all'assistenzaVai all'assistenza](http://www.registroimprese.it/)
4. [Visura Catastale Online - La ricevi in pochi secondi!](http://www.pratiche.it/visura_catastale)
----

### Ivory Coast
1. [CEPICI](https://www.cepici.ci/?tmp=annonces_legales&p=annonces-legales)
----

### Japan
1. [Mixi (Social Media)](https://mixi.jp/)
2. [Newsmap](http://newsmap.jp/)
----

### Kazachstan
1. [Almaty chamber of commerce and investments](http://www.alcci.kz/)
2. [База данных юридических лиц | Ministry of Justice of the Republic of Kazakhstan](http://www.adilet.gov.kz/en/base)
3. [Electronic government of the Republic of Kazakhstan](http://egov.kz/cms/en)
4. [Главная - Казахстанская фондовая биржа (KASE)](http://www.kase.kz/)
5. [gosreestr.kz/ru/ KAZACHSTAN Property Register](https://www.gosreestr.kz/ru/)
6. [Gr5.gosreestr.kz/p/ru/gr-search/search-objects](https://gr5.gosreestr.kz/p/ru/gr-search/search-objects)
7. [Казахстан - Маркетинговый бизнес-справочник Казахстана. Базы данных для маркетинговых исследований: импорт и экспорт, предприятия и организации в РК](https://kazdata.kz/)
8. [Kgd.gov.kz/en/services/taxpayer_search](http://kgd.gov.kz/en/services/taxpayer_search)
9. [Костанайский областной суд](http://kst.sud.kz/rus)
10. [nap](http://nap.kz/)
11. [ОПИ](https://www.dfo.kz/)
12. [Поиск человека (налогоплательщика) по ИИН в базе налоговой РК, Салык РК](http://serj.ws/salyk)
13. [Stat.gov.kz/faces/mobileHomePage](http://stat.gov.kz/faces/mobileHomePage?_adf.ctrl-state=s2abo56ym_4&_afrLoop=9633062985782082)
14. [Торговая палата Карагандинской области](http://www.karcci.kz/)
15. [Заңды тұлғалардың, филиалдардың және өкілдіктердің құрылтай құжаттарына енгізілген өзгертулер және толықтыруларды мемлекеттік тіркеу | Қазақстан Республикасының Электрондық үкіметі](http://egov.kz/cms/kk/services/business_registration/pass_mj?lipi=urn%253Ali%253Apage%253Ad_flagship3_messaging%253BzJLAMI5kSDW8T0NJzDsy9w%253D%253D)
16. [3klik.kz - Unternehmen suchen, finden und klicken.](http://www.3klik.kz/)
----

### Kosovo
1. [Kosovo REGISTRY ARBK](https://arbk.rks-gov.net)
----

### Kuwait
1. [Kuwait-toplist Firmensuche](http://www.kuwait-toplist.com/)
2. [Kuwaitchamber.org Handelskammer](http://www.kuwaitchamber.org.kw/echamber/website/index.jsp)
----

### Latvia
1. [draugiem (Social Media)](https://www.draugiem.lv/)
----

### Luxemburg
1. [LUX Legilux Luxemburg Journal Officiel](http://www.legilux.public.lu)
2. [Luxembourg Business Registers](https://www.lbr.lu)
3. [Luxemburg UBO Registry Www.lbr.lu](https://www.lbr.lu/mjrcs-rbe)
----

### Macedonia
1. [Macedonia REGISTRY](https://www.crm.com.mk/namereservation/)
----

### Malta
1. [www.registry.mfsa.com.mt/](http://www.registry.mfsa.com.mt/)
----

### Mexico
1. [AFP.com (News Agency)](https://www.afp.com/es)
2. [Amarillas México | AmarillasMexico.mx](https://www.amarillasmexico.mx/)
3. [Aplicaciones.sat.gob.mx](https://aplicaciones.sat.gob.mx/PTSC/ADC/resources/pages/operaciones/generarContrasena/ingresarRfc.xhtml)
4. [BSNS Business Data Directory](https://www.digmap.com/platform/mexico-business-data/)
5. [Buho Legal Consult](http://www.buholegal.com/consultasep/)
6. [Directorio Empresarial México](https://www.directorioempresarialmexico.asesorensistemas.mx)
7. [El Financiero (Newspaper)](https://www.elfinanciero.com.mx/)
8. [El Universal (Newspaper)](https://www.eluniversal.com.mx/)
9. [Excelsior (Newspaper)](https://www.excelsior.com.mx/)
10. [Direccion general de Estadistica Judicial](http://www.dgepj.cjf.gob.mx/paginas/serviciosTramites.htm?pageName=servicios%2Fexpedientes.htm)
11. [http://laht.com/index.asp](http://laht.com/index.asp)
12. [LaJornada (Newspaper)](https://www.jornada.com.mx)
13. [Latin American Herald Tribune (Newspaper)](http://laht.com/index.asp)
14. [Lta.reuters.com (News Agency)](https://lta.reuters.com/)
15. [Mexico.paginasamarillas.com (Bsns Directory)](https://mexico.paginasamarillas.com/)
16. [OEM (Newspaper)](https://www.oem.com.mx/oem/)
17. [Piservices.mx (Mexico PI)](http://piservices.mx/about-us)
18. [Reforma (Newspaper)](https://www.reforma.com/)
19. [Siempre (Newspaper)](http://www.siempre.mx/)
20. [Suprema Corte de Justicia de la Nacion](http://www2.scjn.gob.mx/ConsultaTematica/PaginasPub/TematicaPub.aspx)
----

### Montenegro
1. [Montenegro REGISTRY](http://www.pretraga.crps.me:8083/)
----

### Morocco
1. [Directinfo.ma](https://www.directinfo.ma/)
----

### Netherlands
1. [Algemeen Dagblad](http://www.ad.nl/)
2. [Datenschutzbehörde Personeninfo](https://autoriteitpersoonsgegevens.nl/nl)
3. [De Telefoongids Gouden Gids](http://www.detelefoongids.nl/)
4. [De Telegraaf](http://www.telegraaf.nl/)
5. [De Volkskrant](http://www.volkskrant.nl/)
6. [Elsevier](http://www.elsevier.nl/)
7. [Faillissementen.com Insolvencies](http://www.faillissementen.com)
8. [Informatie van de AFM voor professionals](http://www.afm.nl/corporate/)
9. [Justis.nl/binaries/WEB_112365_Lijst_WPBR%20nw_tcm34-324710.pdf](https://www.justis.nl/binaries/WEB_112365_Lijst_WPBR%20nw_tcm34-324710.pdf)
10. [Kamer van Koophandel](http://www.kvk.nl/handelsregister/zoekenframeset.asp?url=https://server.db.kvk.nl/wwwsrvu/html/zoek.htm)
11. [KVK](http://www.kvk.nl)
12. [NE Kvk.nl/english/business-register/](https://www.kvk.nl/english/business-register/)
13. [Nobel](http://www.nobelinf.nl/)
14. [NRC Handelsblad](http://www.nrc.nl/)
15. [NVH - Leden](http://www.nvhinfo.nl/htm/nvh_leden.htm?40)
16. [Overheid Government](http://www.overheid.nl)
17. [Rechtspraak Litigation](http://www.rechtspraak.nl)
18. [Wipo.int/pctdb/en/search-adv.jsp](http://www.wipo.int/pctdb/en/search-adv.jsp)
----

### Pakistan
1. [Campus.pk Social network of Pakistani students](http://www.campus.pk/)
2. [Circle.pk Social Media Site](https://circle.pk/)
3. [Fbr.gov.pk Federal Board of Revenue Pakistan](http://www.fbr.gov.pk/)
4. [Fpcci.org.pk/ Chambers of Commerce](http://fpcci.org.pk/)
5. [Federal Board of Revenue, Government of Pakistan Search by NTN Number](http://www.fbr.gov.pk/)
6. [Naseeb.com Pakistan Dating](http://www.naseeb.com/)
7. [PakAlumni Worldwide: The Global Social Network](http://www.pakalumni.com/)
8. [Psx.com.pk/ Pakistan Stock Exchange](https://www.psx.com.pk/)
9. [Reffra social networking, blogging and video sharing](http://www.reffra.com/)
10. [Sarhad Chamber of Commerce](http://kpcci.org.pk/)
11. [SECP Company Name Search](https://www.secp.gov.pk/company-name-search/)
12. [Secp Securities and Exchange Comission of Pakistan](https://www.secp.gov.pk/)
13. [Stop.com.pk SOcial Site, Store, Classifieds and Forum](http://stop.com.pk/)
14. [Verify Companies](http://jamapunji.pk/verify/incorporated-entities)
----

### Poland
1. [Business Registry](https://bip.ms.gov.pl/)
2. [www.krs-online.com.pl/](http://www.krs-online.com.pl/)
3. [Ministerstwo Sprawiedliwości Justice Ministry](https://www.ms.gov.pl/)
4. [Phone Book of Poland Google CSE](http://pbof.com/poland/)
5. [pkt.pl/ Directory](https://www.pkt.pl/)
6. [Registry Infornmation Company Search](https://ekrs.ms.gov.pl/web/wyszukiwarka-krs/strona-glowna)
7. [Statistical Office Poland](http://stat.gov.pl/en/)
8. [Wie kann man die REGON Nummer, Statistiknummer in Polen verifizieren ? - Unterlagen aus Polen, Beschaffung von Abschriften von Gerichtsurteilen, Geburtsurkunden, Heiratsurkunden, Sterbeurkunden, Taufurkunden, Grundbuchauszüge und Handelsregisterauszüge.](http://www.urkundenauspolen.de/wie-kann-man-die-regon-nummer-statistiknummer-in-polen-verifizieren/)
9. [Yellow Pages Poland](https://www.yellowpages.pl/)
----

### Portugal
1. [Instituto dos Registos e Notariado: Início](http://www.irn.mj.pt/sections/inicio/)
2. [Portal das FinanÃ§as](http://www.portaldasfinancas.gov.pt/at/html/index.html)
3. [Portal da Habitação - Página Inicial](http://www.portaldahabitacao.pt)
----

### Romania
1. [Top Romanian Companies Database by turnover, profit, employees](https://www.romanian-companies.eu/top_companies.asp)
2. [Verify Romanian Passport ID](http://valideaza.ro/valideaza-cnp.php)
----

### Russia
1. [List-Org (Russian Companies)](https://www.list-org.com/)
2. [OK.RU](https://ok.ru/)
3. [People search results | VK](https://vk.com/people)
4. [Поиск сообществ (Community Search)](https://vk.com/communities)
5. [Поиск людей (People Search)](https://vk.com/people)
6. [Social Media - VK FindFace](https://findface.ru/)
7. [SocialStats.ru](http://socialstats.ru/)
8. [Spotlight](http://spotlight.svezet.ru/)
9. [Spy VKontakte](http://vk5.city4me.com/)
10. [TargetoLog](http://targetolog.com/)
11. [VK](http://vk.com/)
12. [Vk.barkov.net](https://vk.barkov.net/)
13. [VK Contact Catalog](https://вконтакте24.рф/catalog.html)
14. [VK Faces (without login)](https://vkfaces.com/)
15. [VK Parser](http://vkparser.ru/)
16. [VK Post Tree](http://dcpu.ru/vk_repost_tree.php)
17. [VK - Search Communities](https://vk.com/communities)
18. [ВКонтакте RSS](http://vk-to-rss.appspot.com/)
19. [ВКонтакте RSS VK to RSS](https://vk-to-rss.appspot.com/)
20. [vMetaDate.sh](https://gist.github.com/cryptolok/8a023875b47e20bc5e64ba8e27294261)
21. [Yandex.Images Search](https://yandex.com/images/)
----

### Senegal
1. [Rechercher une société](http://creationdentreprise.sn/rechercher-une-societe)
----

### Serbia
1. [Bizreg.esrpska.com One Stop Shop Registry Info](http://bizreg.esrpska.com)
2. [Geschäftsbericht (Nur mit Unternehmensnummer)](http://pretraga3.apr.gov.rs)
3. [Народна банка Србије National Bank of Serbia](http://www.nbs.rs/internet/cirilica/index.html)
4. [Serbia REGISTRY APR Агенција за привредне регистре](http://www.apr.gov.rs/eng/Home.aspx)
----

### Seychelles
1. [Seychelles Revenue Commission | Welcome](https://www.src.gov.sc/)
2. [http://www.egov.sc/Business/Licenses.aspx](http://www.egov.sc/Business/Licenses.aspx)
----

### Singapore
1. [ACRA Singapore](https://www.acra.gov.sg/home/)
2. [eCitizen](http://www.ecitizen.gov.sg/Pages/default.aspx)
3. [Egazette Electronic Gazette](http://www.egazette.com.sg)
4. [Chamber of Commerce](http://www.sbf.org.sg/)
5. [MASNET](https://masnetsvc.mas.gov.sg/Ffid.html)
6. [myTax Portal](https://mytax.iras.gov.sg/ESVWeb/default.aspx?target=GSTListingSearch)
7. [Sgx.com Stock Exchange](http://sgx.com)
----

### Slowakia
1. [Business Register of the Slovak Republic on Internet](http://www.orsr.sk/Default.asp?lan=en)
2. [Finstat](https://www.finstat.sk)
3. [Rozšírené vyhľadávanie - rpvs Beneficial Owner](https://rpvs.gov.sk/rpvs/Partner/Partner/VyhladavaniePartnera)
4. [Slowakei-Net.de](http://www.slowakei-net.de/deutsch/slowakei_preview.html?body_extras-links.html)
5. [Vyhľadávanie](http://www.registeruz.sk/cruz-public/domain/accountingentity/simplesearch)
6. [ŽIVNOSTENSKÝ REGISTER SLOVENSKEJ REPUBLIKY](http://www.zrsr.sk/zr_fo.aspx)
----

### Slowenia
1. [Slovenia REGISTRY AJPES](https://www.ajpes.si/Default.asp?language=german)
----

### South Africa
1. [CIPC Handelskammer](http://www.cipc.co.za/)
2. [Deutsche industrie und Handelskammer für Südafrika](http://suedafrika.ahk.de/)
3. [Eservices.cipc.co.za](https://eservices.cipc.co.za/)
4. [Experian](http://www.experian.co.za/)
5. [GUICHET UNIQUE DE FORMALITES DES ENTREPRISES EN CENTRAFRIQUE](http://gufe-rca.org/)
6. [www.saflii.org/content/databases](http://www.saflii.org/content/databases)
7. [saflii.org/content/databases](http://www.saflii.org/content/databases)
8. [Transunion](https://www.transunion.co.za/)
----

### Spain
1. [AbcTelefonos.com](http://abctelefonos.com)
----

### Switzerland
1. [AllYouCanRead: Swiss newspapers](https://www.allyoucanread.com/swiss-newspapers/)
2. [Base de donnés des arrêts TAF](http://www.bvger.ch/publiws/?lang=fr)
3. [Base de donnés des arrêts TAF Patent Court](http://www.bvger.ch/publiws/?lang=fr)
4. [Bj.admin.ch Federal Office of Justice](http://www.bj.admin.ch/)
5. [branchenbuch](http://branchenbuch.ch/)
6. [Bstger.weblaw Federal Criminal Court](http://bstger.weblaw.ch/index.php?method=search)
7. [Bundesamt für Polizei](https://www.fedpol.admin.ch/fedpol/de/home.html)
8. [Bundesamt für Polizei AML](http://www.fedpol.admin.ch/)
9. [Bundesblatt](https://www.admin.ch/gov/de/start/bundesrecht/bundesblatt.html)
10. [CH: FINMA - Aufsicht über direkt unterstellte Finanzintermediäre](https://www.finma.ch/de/ueberwachung/direkt-unterstellte-finanzintermediaere-dufi/)
11. [CH: FINMA - Liste der von der FINMA bewilligten Banken und Effektenhändler](https://www.finma.ch/de/finma-public/bewilligte-institute-personen-und-produkte/)
12. [CH: FINMA - SRO-Mitglieder-Suche](https://www.finma.ch/de/bewilligung/selbstregulierungsorganisationen-sro/sro-mitglieder-suche/)
13. [CH: Finma: Warnung vor möglicherweise unerlaubt tätigen Anbietern](https://www.finma.ch/de/finma-public/warnliste/#Order=1)
14. [Competition Commission](https://www.weko.admin.ch/weko/en/home.html)
15. [Courts and cases Switzerland (Lexadin, last update 2010)](https://www.lexadin.nl/wlg/courts/nofr/eur/lxctzwi.htm)
16. [Easymonitoring](https://www.easymonitoring.ch/)
17. [finanzen.ch: Realtimekurse](http://www.finanzen.ch/)
18. [FINMA - Startpage](http://www.finma.ch)
19. [Handelsregisteramt](http://www.hra.zh.ch/internet/justiz_inneres/hra/de/home.html)
20. [Hrc Internet](https://ge.ch/hrcintapp/companySearch.action?lang=FR&showHeader=false)
21. [The official Swiss phone directory](https://tel.search.ch/)
22. [Offizielles Telefonbuch der Schweiz – local.ch](https://tel.local.ch/de)
23. [Schweiz: Adressen + Firmenverzeichnis](http://www.firmendb.de/schweiz/index.php)
24. [Search - Eidgenössische Finanzkontrolle Whistleblower](http://www.efk.admin.ch)
25. [SHAB Schweizerisches Handelsamtsblatt](https://www.shab.ch)
26. [Staatssekretariat für Migration](https://www.sem.admin.ch/sem/de/home.html)
27. [Swissdox (€) Medienbeobachtung in der Schweiz](http://swissdox.ch/Swissdox2/)
28. [SwissBanking](http://www.swissbanking.org/)
29. [SIX Swiss Exchange](http://www.six-swiss-exchange.com/)
30. [Übernahmekommission - Übernahmekommission](http://www.takeover.ch/)
31. [zefix](https://www.zefix.ch)
----

### Thailand
1. [Dbd.go.th Corporate Data Search English (dates according to the Buddhist calendar)](http://www.dbd.go.th/Applications/cds/)
2. [.dbd.go.th Corporate Data Thai, only with Thai ID & Address](http://datawarehouse.dbd.go.th/bdw/search/search1.html)
----

### Tunesia
1. [Centre National Universitaire Cnudst.rnrt.tn](http://www.cnudst.rnrt.tn)
2. [Legislative and regulatory texts | legislation.tn](http://www.legislation.tn/en/recherche/legislatifs-reglementaires)
3. [Official Printing Office of the Republic of Tunisia](http://www.iort.gov.tn)
4. [Phonebook of Tunisia.com  +216     by Phonebook of the World.com](http://phonebookoftheworld.com/tunisia/)
5. [Registre National des Entreprises](http://www.registre-commerce.tn)
6. [The official press of the Republic of Tunisia](http://www.iort.gov.tn/WD120AWP/WD120Awp.exe/CONNECT/SITEIORT)
----

### Ukraine
1. [Investigating Ultimate Beneficial Ownership in Ukraine | Global Risk Affairs](http://www.globalriskaffairs.com/2017/12/investigating-ultimate-beneficial-ownership-in-ukraine/?utm_campaign=shareaholic&utm_medium=twitter&utm_source=socialnetwork)
2. [Opendatabot — захист бізнесу від рейдерських захоплень](https://opendatabot.com/)
3. [Youcontrol.com.ua/en](https://youcontrol.com.ua/en/)
4. [https://clarity-project.info Tender Information](https://clarity-project.info)
5. [UBI - Business Information](https://ubi.agency/)
----

### United Arab Emirates UAE
1. [B2B Marketplace](http://www.exportersindia.com)
2. [Cedar-rose.com/ DD Dienstleister](http://www.cedar-rose.com/Index)
3. [Clarifiedby.com/ Diligencia / DD Dienstleister](https://www.clarifiedby.com/)
4. [Complete list of Dubai Interior Design Companies](https://topdubaidesigners.com/complete-list-of-dubai-interior-design-companies/)
5. [Difc Dubai International Financial Centre | Registrar of Companies](https://www.difc.ae/public-register)
6. [Dcciinfo.com Commercial Directory](http://www.dcciinfo.com/)
7. [Dubai Chamber](http://www.dubaichamber.com/)
8. [Dubai Department of Economic Development](http://dubaided.gov.ae/English/)
9. [Dubai Free Zones client directory](http://eservices.dubaitrade.ae/clientdirectory/clientsSearch.do)
10. [Cbls.economy.gov.ae ver 1](http://cbls.economy.gov.ae/)
11. [Cbls.economy.gov.ae ver 2](http://cbls.economy.gov.ae/BASearch.aspx)
12. [Dfsa.ae/Public-Register Dubai Financial Services Authority](http://www.dfsa.ae/Public-Register/Firm)
13. [Dubai Airport Freezone](http://www.dafz.ae/en)
14. [Dubai Stock Exchange](http://www.dfm.ae/)
15. [Gulf Malayali.directory](http://malayali.directory/)
16. [Localsearch AE](http://www.localsearch.ae)
17. [Nasdaq Dubai](http://www.nasdaqdubai.com/)
18. [Sharjah Chamber](http://www.sharjah.gov.ae/)
19. [UAE B2B companies directory:.](http://dubai.companies-uae.com/)
20. [UAE Job Site](https://www.bayt.com/en/uae/)
21. [United Arab Emirates Pages](http://www.uaeresults.com)
----

### United Kingdom (UK)
1. [192.com](http://www.192.com/)
2. [BBC News](https://www.bbc.co.uk/news/world/europe)
3. [Briteverify](http://www.briteverify.co.uk/)
4. [Companies House](https://www.gov.uk/government/organisations/companies-house)
5. [Companies House service (Free)](https://beta.companieshouse.gov.uk/)
6. [Core.ac](https://core.ac.uk/search)
7. [Data Scotland](https://www.datascotland.com)
8. [Data.gov.uk](https://data.gov.uk/)
9. [Discoverandromeda.com](https://discoverandromeda.com/)
10. [Companies House (UK)](https://beta.companieshouse.gov.uk/)
11. [Europages](http://www.europages.co.uk/)
12. [Companies House UK](https://www.gov.uk/get-information-about-a-company)
13. [Company Check](https://companycheck.co.uk/)
14. [Experian UK](http://www.experian.co.uk/)
15. [Find my past UK](https://search.findmypast.co.uk/search-world-Records/england-and-wales-births-1837-2006)
16. [Fish4 Job Search](http://www.fish4.co.uk/)
17. [Hersay](http://www.hersay.co.uk/)
18. [HM Land Registry](http://www.landregistry.gov.uk/home)
19. [Hoovers.com](http://www.hoovers.com/)
20. [Investigative Dashboard.UK](https://investigativedashboard.org/databases/europe/#united-kingdom)
21. [Financial Conduct Authority](https://register.fca.org.uk/)
22. [Jobsite](http://www.jobsite.co.uk/)
23. [Locate Family](https://www.locatefamily.com/Street-Lists/index.html)
24. [NewsNow UK](http://www.newsnow.co.uk/)
25. [Financial Conduct Authority - UK](http://www.fca.org.uk/)
26. [Oscobo Search the Web](https://oscobo.co.uk/)
27. [QWARIE: UK-OSINT](http://uk-osint.net/favorites.html)
28. [RBA Registers](http://www.rba.co.uk/sources/registers.htm)
29. [Reed Jobs](http://www.reed.co.uk/)
30. [Scoot](http://www.scoot.co.uk/)
31. [The National Archives](http://www.nationalarchives.gov.uk/)
32. [Welcome to the Jersey  JFSC Companies Registry](http://www.jerseyfsc.org/registry/)
33. [WTNG](http://www.wtng.info/wtng-44-uk.html)
34. [Overseas registries](https://www.gov.uk/government/publications/overseas-registries/overseas-registries)
35. [Xrayz](http://www.xrayz.co.uk/)
36. [Zetoc](http://zetoc.jisc.ac.uk/)
37. [JMLSG Guidance](http://www.jmlsg.org.uk/industry-guidance/article/jmlsg-guidance-current)
38. [Joint Money Laundering Steering Group Home Page](http://www.jmlsg.org.uk/what-is-jmlsg)
----

### Turkey
1. [Turkish Chamber of Commerce](http://www.tobb.org.tr/)
2. [Turkish Company register](http://www.ticaretsicilgazetesi.gov.tr/)
3. [Turkish Data](http://yalta.etu.edu.tr/turkish-data-sources-en.html)
----

### Turks and Caicos
1. [Turks and Caicos Government Offices](http://tcimall.tc/turks-and-caicos-government-offices/)
----

### Surveillance
1. [A Very Brief Introduction To Surveillance Detection](https://protectioncircle.org/2014/01/03/a-very-brief-introduction-to-surveillance-detection/)
2. [Detecting Terrorist Surveillance Email](https://worldview.stratfor.com/article/detecting-terrorist-surveillance)
3. [How to Determine When You Are Under Physical Surveillance](https://escapethewolf.com/2296/when-you-under-physical-surveillance/)
----

### Culture & Customs
1. [Countries](http://reliefweb.int/countries)
2. [Expatriates](https://www.justlanded.com/)
3. [International Business Etiquette and Manners for Global Travelers
cultural diversity, cross cultural communication, and intercultural business
relationships for success](http://www.cyborlink.com/besite/)
4. [Language Translation from #1 UK Online Translation Agency](http://www.kwintessential.co.uk/)
5. [Tim Marshall The What & The Why](http://www.thewhatandthewhy.com/)
----

### Crisis Alerts & Travel Security | Incident Disaster
1. [ACLED (Conflict Data)](https://www.acleddata.com/)
2. [Airban -   European Commission](https://ec.europa.eu/transport/modes/air/safety/air-ban_de)
3. [Auswaertiges Amt Reisewarnungen](http://www.auswaertiges-amt.de/DE/Laenderinformationen/01-Reisewarnungen-Liste_node.html)
4. [BBC Country Profiles](http://news.bbc.co.uk/2/hi/country_profiles/default.stm)
5. [CDC Works 24/7](https://www.cdc.gov/)
6. [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
7. [Countries & Areas Archive](https://www.state.gov/misc/list/index.htm)
8. [Crime](https://www.numbeo.com/crime/)
9. [Crime Terror Nexus](https://crimeterrornexus.com/)
10. [Embassy List, Embassies and Consulates Around the World](http://www.embassyworld.org/)
11. [Europol](https://www.europol.europa.eu/)
12. [Foreign travel advice](https://www.gov.uk/foreign-travel-advice)
13. [Global Incident Map](http://www.globalincidentmap.com/threatmatrix.php)
14. [GlobalSecurity.org](http://www.globalsecurity.org/)
15. [GWU Project on Extremism](https://extremism.gwu.edu/)
16. [Helpline Database > Embassy Database](http://www.helplinedatabase.com/embassy-database/)
17. [Incidents Map  –  ACT Emergency Services Agency](http://esa.act.gov.au/community-information/incidents-map/)
18. [Islamism Map](https://islamism-map.com/#/)
19. [Live Earthquakes Map](http://quakes.globalincidentmap.com/)
20. [LiveUA Map](https://liveuamap.com/)
21. [Mapping Militant Organizations](http://web.stanford.edu/group/mappingmilitants/cgi-bin/)
22. [PubMed](https://www.ncbi.nlm.nih.gov/pubmed/)
23. [Real-time Crisis Alerts | SAM](https://www.samdesk.io/)
24. [ReliefWeb](http://reliefweb.int/countries)
25. [RSOE EDIS](http://hisz.rsoe.hu/)
26. [SPLC Hate Map](https://www.splcenter.org/hate-map)
27. [Striving For A Safer World Since 1945](https://fas.org/)
28. [The Aviation Herald](http://www.avherald.com/)
29. [Travel Risk Map | US State Department](https://travelmaps.state.gov/TSGMap/)
30. [UMD Global Terrorism Database](https://www.start.umd.edu/gtd/)
31. [United Nations Office on Drugs and Crime](http://www.unodc.org/)
32. [Water, Peace and Security Global warning tool](https://waterpeacesecurity.org/map)
33. [World Health Organization](http://www.who.int/en/)
34. [World-map-of-encryption](https://www.gp-digital.org/world-map-of-encryption/)
----

### Politically Exposed Person PEP Sources
1. [Chiefs of State & Cabinet Members of Foreign Governments](https://www.cia.gov/library/publications/world-leaders-1/)
2. [CIDOB Biografías Líderes Políticos](https://www.cidob.org/en/biografias_lideres_politicos_only_in_spanish)
3. [EveryPolitician](http://everypolitician.org/)
4. [LittleSis watchdog network](https://littlesis.org/)
5. [OpenSecrets](https://www.opensecrets.org/)
6. [Rulers](http://rulers.org/index.html)
----

### Sanctions List Watchlist
1. [Alphabetical List of Iranian Entities | Iran Watch](https://www.iranwatch.org/iranian-entities)
2. [bigger312/Sanctions-Lists](https://github.com/bigger312/Sanctions-Lists)
3. [Common Foreign and Security Policy (CFSP)](https://eeas.europa.eu/topics/common-foreign-security-policy-cfsp_en)
4. [Global Sanctionlist Profiles](https://sanctions-intelligence.com/global-lists/)
5. [INTERPOL](https://www.interpol.int/notice/search/wanted)
6. [Mr. Watchlist](https://mrwatchlist.com/)
7. [OPEN SANCTIONS](http://pudo.org/material/opennames/)
8. [Open Sanctions & Organized Crime Database](https://www.opensanctions.org/)
9. [Specially Designated Nationals And Blocked Persons List (SDN)](https://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/default.aspx)
10. [Star Stolen Asset Recovery Worldbank Corruption Cases Search](http://star.worldbank.org/corruption-cases/)
11. [US BIS Denied-Persons List](https://www.bis.doc.gov/index.php/policy-guidance/lists-of-parties-of-concern/denied-persons-list)
12. [US Denied Persons List](https://2016.export.gov/ecr/eg_main_023148.asp)
13. [US Export Gov Consolidated Screening List](https://sanctionssearch.ofac.treas.gov/)
14. [US OFAC - Open Sanctions](https://data.world/datasets/opensanctions)
----

### GEO Located News
1. [newspaper map](http://newspapermap.com/)
----

### GPS Coordinates GEO Locate
1. [ADDRESS LOOKUP Find the address of any place on Google Maps](https://ctrlq.org/maps/address/)
2. [Address to GPS Conversion](http://www.mapseasy.com/adress-to-gps-coordinates.php)
3. [Coordinate conversion](https://www.doogal.co.uk/BatchReverseGeocoding.php)
4. [GeolocateThis!](http://geolocatethis.site/)
5. [GPS Coordinates .DE](https://www.gpskoordinaten.de/)
6. [GPS Visualizer: Quick Geocoder](http://www.gpsvisualizer.com/geocode)
7. [Latitude and Longitude Finder on Map Get Coordinates](http://www.latlong.net/)
8. [Meet Me](https://maper.info)
9. [MyGeoPosition.com](http://mygeoposition.com/)
10. [ShadowCalculator - Show sun shadows on google maps](http://shadowcalculator.eu/#/lat/50.08/lng/19.899999999999977)
----

### Satellite Data
1. [Bhuvan NRSC (ISRO IN)](http://bhuvan.nrsc.gov.in/data/download/index.php)
2. [Citizen Evidence Lab](https://citizenevidence.org/2020/03/20/magery/)
3. [CLASS (NOAA US)](http://www.class.noaa.gov/)
4. [Copernicus](https://scihub.copernicus.eu/dhus/)
5. [CORONA (UARK US)](http://corona.cast.uark.edu/)
6. [DGI-INPE (BR)](http://www.dgi.inpe.br/CDSR/)
7. [Digital Coast Home](https://coast.noaa.gov/digitalcoast/)
8. [DigitalGlobe](http://www.digitalglobe.com/)
9. [Earth Explorer (USGS)](https://earthexplorer.usgs.gov/)
10. [EOLi-SA  (ESA)](https://earth.esa.int/web/guest/eoli)
11. [EORC (JP)](http://www.eorc.jaxa.jp/ALOS/en/aw3d30/)
12. [EOSDATA](https://lv.eosda.com/)
13. [Flash Earth](http://www.flashearth.com)
14. [GLCF: Welcome](http://landcover.org/)
15. [Land Viewer | EOS](https://eos.com/landviewer/)
16. [COAST (NOAA US)](https://coast.noaa.gov/dataviewer/#)
17. [REVERB ECHO (NASA US)](https://reverb.echo.nasa.gov/reverb/)
18. [SASGIS (RU)](http://sasgis.ru/sasplaneta/)
19. [Satellite Imagery Course Online](https://learn-new-tools.org/)
20. [Terrapattern](http://www.terrapattern.com/)
21. [Terraserver](https://www.terraserver.com)
22. [VITO Earth Observation (BE)](http://www.vito-eodata.be/PDF/portal/Application.html#Home)
23. [Zoom Earth](https://zoom.earth/)
----

### SIGINT
1. [awesome-telco](https://github.com/ravens/awesome-telco)
2. [Broadcastify](http://www.broadcastify.com/)
3. [Funk Frequenzen 01 (DE)](http://www.funkfrequenzen01.de/indexx.htm)
4. [Motorola Business Radio Frequency Database](https://www.buytwowayradios.com/blog/2015/05/default_frequencies_for_motorola_business_radios.html)
5. [N4JRI's Security Scanning Blog](https://www.qsl.net/n4jri/index.htm)
6. [Rollanet Frequency Database (PDF)](http://www.rollanet.org/~n0klu/Ham_Radio/1000's%20-%20SCANNER%20&%20HAM%20Radio%20Frequencies%20-%20Ebook.pdf)
7. [Signal Identification Wiki](https://www.sigidwiki.com/wiki/Signal_Identification_Guide)
8. [ZipScanners](https://www.zipscanners.com/)
----

### Geolocating IP Addresses
1. [What Is the Best API for Geolocating an IP Address?](https://ahmadawais.com/best-api-geolocating-an-ip-address/)
2. [IP Address API and Data Solutions](https://ipinfo.io/)
3. [ipapi - IP Address Location API](https://ipapi.co/)
----

### Webcam Search
1. [123cam](http://123cam.com/)
2. [Airport Webcams](http://airportwebcams.net/)
3. [CamVista](http://www.camvista.com/)
4. [Censys](https://censys.io/)
5. [EarthCam](https://www.earthcam.com/)
6. [Fisgonia](http://www.fisgonia.com/)
7. [Insecam](https://www.insecam.org/)
8. [Krooz Cams](http://www.kroooz-cams.com)
9. [Lookr](https://www.lookr.com/)
10. [Míla (Iceland)](https://www.livefromiceland.is/webcams/geysir)
11. [nsspot](http://world-webcams.nsspot.net/)
12. [OpenStreetCam](http://openstreetcam.org/map/@40.73125888744284,-73.99618148803712,12z)
13. [OpenTopia Web Cam Streaming](http://www.opentopia.com/)
14. [Pictimo](https://www.pictimo.com/)
15. [Round Shot](http://www.roundshot.com/default.cfm?DomainID=1&TreeID=172&language=en)
16. [SeeAllTheThings](https://github.com/baywolf88/seeallthethings)
17. [The Webcam Network](http://www.the-webcam-network.com/)
18. [thingful](https://www.thingful.net/)
19. [Traffic Cameras and Reports Search Directory](https://publicrecords.onlinesearches.com/Traffic-Cameras-and-Reports.htm)
20. [UberSpace](https://kamba4.crux.uberspace.de/)
21. [Unsecured IP Camera List](https://reolink.com/unsecured-ip-camera-list/)
22. [WebCam.NL](https://webcam.nl/live_streaming/)
23. [Webcams Travel](https://www.webcams.travel/)
24. [Worldcam](https://worldcam.eu/)
----

### Metadata
1. [I Know Where Your Cat Lives](https://iknowwhereyourcatlives.com/about/)
2. [QGIS](https://qgis.org/en/site/forusers/download.html)
----

### COUNTRY CODES
1. [2-Letter, 3-Letter, Country Codes for All Countries in the World](http://www.worldatlas.com/aatlas/ctycodes.htm)
2. [GeoNames Geo Database of Names](https://www.geonames.org/)
3. [Mobile Phone Country Codes and Providers](https://en.wikipedia.org/wiki/List_of_mobile_telephone_prefixes_by_country)
4. [Passport Index](https://www.passportindex.org/)
5. [PRADO, an online public register of authentic identification and travel documents](https://www.consilium.europa.eu/prado/en/prado-start-page.html)
----

### Anti Money Laundering AML | Geldwäsche GwG | Transparency
1. [Basel AML Index | Basel Institute on Governance](https://www.baselgovernance.org/basel-aml-index)
2. [Basel Committee on Banking Supervision](https://www.bis.org/bcbs/publ/d353.htm)
3. [Basel Governance ICAR](https://www.baselgovernance.org/)
4. [Centre for Global Development (CGD)](https://www.cgdev.org/)
5. [Corporate Tax Haven Index](https://www.corporatetaxhavenindex.org/)
6. [Countries - Financial Action Task Force (FATF)](http://www.fatf-gafi.org/countries/#high-risk)
7. [Corporate Tax Haven Index (Tax Justice Network)](https://www.corporatetaxhavenindex.org/)
8. [Country AML Reports (Know Your Country)](https://www.knowyourcountry.com/copy-of-country-reports)
9. [Country Public AML Ranking | Basel Institute on Governance](https://www.baselgovernance.org/basel-aml-index/public-ranking)
10. [CSO INternational Centre for Asset Recovery](http://https://cso.assetrecovery.org/)
11. [CSR Research quality Standard](http://www.aristastandard.org/content/home.html)
12. [DB Nomics World Economic Data](https://db.nomics.world/)
13. [Europa.eu High Risk Countries](https://europa.eu/rapid/press-release_IP-19-781_en.htm)
14. [Europa.eu Q&A Risk Countries](https://europa.eu/rapid/press-release_MEMO-19-782_en.htm)
15. [European Anti-Fraud Office](http://ec.europa.eu/anti-fraud//home_en)
16. [European Bank for Reconstruction and Development (EBRD)](http://ebrd.com/home)
17. [Europol](https://www.europol.europa.eu/)
18. [Federal Register Customer Due Diligence Requirements for Financial Institutions (US)](https://www.federalregister.gov/documents/2016/05/11/2016-10567/customer-due-diligence-requirements-for-financial-institutions)
19. [FFIEC BSA/AML Bank Secrecy Act/ Anti-Money Laundering Examination Manual](https://www.occ.treas.gov/publications/publications-by-type/other-publications-reports/ffiec-bsa-aml-examination-manual.pdf)
20. [Financial Action Task Force (FATF)](http://www.fatf-gafi.org/)
21. [Financial Crimes Enforcement Network (FinCEN) US Dept. of Treasury](https://www.fincen.gov/)
22. [GAN Business Anti-Corruption Portal](https://www.business-anti-corruption.com/)
23. [Geldwäschereiverordnung FINMA (Schweiz)](http://www.forum-sro.ch/d/finma-eb-gwv-finma.pdf)
24. [Global Financial Integrity](https://www.gfintegrity.org/)
25. [Global Witness](http://www.globalwitness.org/)
26. [Group of States against Corruption (GRECO)](https://www.coe.int/en/web/greco)
27. [Guernsey Financial-Services Commission Handbook ) GFSC)](https://www.gfsc.gg/sites/default/files/Handbook-for-Financial-Services-Businesses-on-Countering-Financial-Crime-and-Terrorist-Financing-July-2016_0.pdf)
28. [ICC Commercial Crime Services Fraudnet](https://icc-ccs.org/)
29. [IMF Homepage](http://www.imf.org/external/)
30. [IMOLIN International Money Laundering Information Network](http://www.imolin.org/imolin/en/index.html)
31. [Intergrity Sanctions](https://www.ebrd.com/ineligible-entities.html)
32. [INTERPOL](http://www.interpol.int/en)
33. [Isle of Man Financial Supervisory Authority (IoMFSA)](https://www.iomfsa.im/designated-business/amlcft/)
34. [OECD Tax Transparency](https://www.oecd.org/tax/transparency/)
35. [Openownership - Beneficial Ownership Register](https://www.openownership.org/)
36. [Paul Renner](http://kycmap.com/)
37. [SUERF Policy Notes](https://www.suerf.org/policynotes)
38. [Tax Justice Network Financial Secrecy Index](https://www.financialsecrecyindex.com/en/)
39. [Tax Justice Network: What is a secrecy jurisdiction?](https://www.financialsecrecyindex.com/faq/what-is-a-secrecy-jurisdiction)
40. [The Egmont Group of Financial Intelligence Units (FIUs)](http://www.egmontgroup.org/)
41. [The World Bank – Stolen Asset Recovery Initiative](https://star.worldbank.org/star/)
42. [TI Corruption Perceptions Index 2018](https://www.transparency.org/cpi2018)
43. [TIAG - Forensic Information Litigation Support Group](https://www.tiagnet.com/)
44. [Transparency.org CPI Corruption Perception Index](https://www.transparency.org/research/cpi/overview)
45. [United Nations Convention Against Corruption](https://www.unodc.org/unodc/en/treaties/CAC/)
46. [United Nations Office on Drugs & Crime (UNODC)](https://www.unodc.org/)
47. [WEF Partnering Against Corruption Initiative](https://www.weforum.org/communities/partnering-against-corruption-initiative)
48. [Wolfsberg Group CBDDQ Capacity Building Materials](https://www.wolfsberg-principles.com/cbddq-capacity-building-materials)
49. [Zoll online - Drittländer mit erhöhtem Risiko](https://www.zoll.de/DE/Fachthemen/FIU/Fachliche-Informationen/Drittlaender/drittlaender_node.html)
----

### Credit rating & Insolvency Info | Bonitäts Informationen
1. [Indat.info - Insolvenzen (€)](http://www.indat.info/)
2. [Global Credit Reports](http://www.global-creditreports.com/)
----

### Brand IP | Supply Chain | Anti-Counterfeit | Illi
1. [Coalition Against Piracy (CAP)](http://www.casbaa.com/about-us/cap/)
2. [EU IPO Enforcement Database EDB](https://euipo.europa.eu/ohimportal/de/web/observatory/home)
3. [Eurosint Forum](https://www.eurosint.eu/)
4. [GIRP European Healthcare Distribution Association](http://girp.eu/)
5. [INTA International Trademark Association](http://www.inta.org)
6. [INTCEN (PDF)](http://rieas.gr/images/editorial/NomikosEUintelligence15.pdf)
7. [UNICRI United Nations Interregional Crime and Justice Research](http://www.unicri.it/)
8. [USCIB Anti-Ilicit Trade](https://www.uscib.org/uscib-sets-up-new-anti-illicit-trade-committee/)
9. [World Customs and Piracy (CAP) Group](http://www.wcoomd.org/en/about-us/wco-working-bodies/enforcement_and_compliance/wco_counterfeiting_and_piracy_group.aspx)
----

### Legal Identifier LEI
1. [Legal Entity Identifier | Search LEI code](https://lei.info/)
2. [LEI ROC List](https://www.leiroc.org/publications/gls/lou_20131003_2.pdf)
3. [Legal Entity Identifier LEI leiroc.org](https://www.leiroc.org/)
----

### Finance
1. [Definition of Public Interest Entities (PIEs)](https://www.accountancyeurope.eu/publications/definition-public-interest-entities-europe/)
2. [ESMA Register European Regulatory Framework](https://registers.esma.europa.eu/publication/)
3. [Google finance](https://www.google.com/finance)
4. [Investopedia](http://www.investopedia.com/)
5. [List of European stock exchanges](https://en.wikipedia.org/wiki/List_of_European_stock_exchanges)
6. [SIX Swiss Exchange](https://www.six-group.com/exchanges/index.html)
7. [Steuern in Europa](http://europa.eu/youreurope/business/vat-customs/buy-sell/index_de.htm)
8. [World Finance](http://finance.mapsofworld.com/)
----

### IBAN | BIC | IIN | CC Validation
1. [BIC Search](https://www2.swift.com/bsl/)
2. [BIN/IIN Lookup](https://binlist.net/)
3. [BINDB - Issuer ID Numbers](https://www.bindb.com/bin-database.html)
4. [CC Number Validator](https://www.freeformatter.com/credit-card-number-generator-validator.html#howToValidate)
5. [CC Validator](https://www.fakenamegenerator.com/credit-card-validator.php)
6. [CC Validator](https://www.validcreditcardnumber.com/)
7. [DC - Luhn (Mod10) Algorithm](https://www.dcode.fr/luhn-algorithm)
8. [DFG - Luhn (Mod10) Validator](http://www.datafakegenerator.com/valitar.php)
9. [IBAN Check](http://www.tbg5-finance.org/?ibancheck.shtml)
10. [IBAN Validator](https://www.iban.com/)
11. [Ibancalculator Iban validieren](http://www.ibancalculator.com/iban_validieren.html)
12. [Inquiry IID](https://www.six-interbank-clearing.com/en/home/bank-master-data/inquiry-bc-number.html)
13. [International Bank Account Numbers](https://en.wikipedia.org/wiki/International_Bank_Account_Number)
14. [Mindestlohnrechner](https://www.shiftjuggler.com/de/mindestlohnrechner/)
15. [PC - Luhn (Mod10) Algorithm](https://planetcalc.com/2464/)
16. [SM - Issuer ID Numbers](https://stevemorse.org/ssn/List_of_Bank_Identification_Numbers.html)
17. [Währungscode ISO_4217](https://en.wikipedia.org/wiki/ISO_4217)
18. [XE IBAN Calculator](http://www.xe.com/ibancalculator/)
----

### Currency Converter
1. [Historical Currency Conversions](http://fxtop.com/de/vergangene-rechner.php)
2. [Historical Currency Converter](https://www.oanda.com/solutions-for-business/historical-rates/main.html)
3. [Oanda](https://www.oanda.com/lang/de/currency/converter/)
4. [Rechner](https://web2.0rechner.de/)
5. [XE Currency Converter](http://www.xe.com/de/currencyconverter/)
----

### Company Information
1. [Baselland.ch Handelsregisterämter  im Ausland](https://www.baselland.ch/politik-und-behorden/direktionen/sicherheitsdirektion/zivilrechtsverwaltung/handelsregister/links-ausland)
2. [Business Registers in Member States EUR](https://e-justice.europa.eu/content_business_registers_in_member_states-106-en.do)
3. [Company Check Orbis](https://orbisdirectory.bvdinfo.com/version-20171213/OrbisDirectory/Companies)
4. [Corporation Wiki (USA) D&B Data](https://www.corporationwiki.com/)
5. [DocumentCloud Search](https://www.documentcloud.org/public/search/)
6. [E-justice.Find a Company](https://e-justice.europa.eu/content_find_a_company-489-de.do)
7. [EU National Registries](https://e-justice.europa.eu/content_business_registers_in_member_states-106-de-en.do?member=1)
8. [FinCen MSB Database](https://www.fincen.gov/msb-registrant-search)
9. [Find VAT #](https://tva-recherche.lu/)
10. [fortune.com/fortune500/](http://fortune.com/fortune500/)
11. [GCRD International](https://www.gcrd.info/)
12. [Global Incorporation Guide](https://www.lowtax.net/g/jurisdictions/)
13. [Global Open Data Index](http://index.okfn.org/)
14. [Gov UK Overseas registries](https://www.gov.uk/government/publications/overseas-registries/overseas-registries)
15. [HSBC Deferred Prosecution Agreement](https://www.justice.gov/archives/opa/documents-and-resources-december-11-2012hsbc-press-conference)
16. [ICP - International Credit Status](http://www.icpcredit.com/index.asp)
17. [Investigative Dashboard](https://investigativedashboard.org/databases/)
18. [International Franchise Association - Over 1,200 franchise opportunities](https://www.franchise.org/)
19. [International Monetary Fund (IMF): Staff Country Reports](https://globaledge.msu.edu/global-resources/resource/1250)
20. [Kompass - Business Directory - (Company / Industry)](https://www.kompass.com/b/business-directory/)
21. [list-org (Russian)](https://www.list-org.com/)
22. [NACE Codes](https://www.euregiolocator.eu/downloads/NACE-Codes.pdf)
23. [NAICS Association](https://www.naics.com/)
24. [Nonprofit Explorer NGO Search](https://projects.propublica.org/nonprofits/)
25. [Offshore Jurisdictions](https://ioserv.com/en/jurisdictions/jurisdlist/)
26. [OpenCorporates](https://opencorporates.com/)
27. [Report on the Non-Resident Portfolio at Danske Bank’s Estonian branch](https://danskebank.com/-/media/danske-bank-com/file-cloud/2018/9/report-on-the-non-resident-portfolio-at-danske-banks-estonian-branch.pdf?rev=56b16dfddae94480bb8cdcaebeaddc9b&hash=B7D825F2639326A3BBBC7D524C5E341E)
28. [SEC.gov Company Search](https://www.sec.gov/edgar/searchedgar/companysearch.html)
29. [US DOJ Country Links](https://www.justice.gov/jmd/ls/countries-d-k#I)
30. [Validate TAX/VAT ID - Validierung UST Nr.](http://ec.europa.eu/taxation_customs/vies/vieshome.do?selectedLanguage=de)
31. [Wikipedia List of company registers](https://en.wikipedia.org/wiki/List_of_company_registers)
----

### Cryptocurrency
1. [Bitcoin Abuse Database](https://www.bitcoinabuse.com/)
2. [Bitcoin Address Lookup, Checker and Alerts](https://bitcoinwhoswho.com/)
3. [Bitcoin ATM Map – Find Bitcoin ATM](https://coinatmradar.com/)
4. [Bitcoin Block Explorer](https://blockexplorer.com)
5. [Bitcoin Block Reward Halving Countdown](https://bitcoinblockhalf.com/)
6. [Bitcoinity.org](https://bitcoinity.org/)
7. [BitcoinWhosWho Bitcoin Address Search](https://bitcoinwhoswho.com/)
8. [BitRef Bitcoin Address Balance Tool](https://bitref.com/)
9. [Blockchain](http://blockchain.com)
10. [Blockchain.com/learning-portal/how-to-get-bitcoins](https://www.blockchain.com/learning-portal/how-to-get-bitcoins)
11. [Blockchaininfo Bitcoin Block Explorer](https://blockchain.info/)
12. [Blockchain Block Explorer Search](https://www.blockchain.com/explorer)
13. [BlockCypher](https://www.blockcypher.com/)
14. [BlockExplorer Bitcoin Block Explorer](https://blockexplorer.com/)
15. [Blockonomics Bitcoin Transaction Search](https://www.blockonomics.co/)
16. [blockseer](http://blockseer.com)
17. [Browserling Web Developer Tools](https://www.browserling.com/tools)
18. [ChainRadar Monero Block Explorer](https://chainradar.com/xmr/blocks)
19. [ChipMixer Bitcoin Tumbler](https://chipmixer.com/)
20. [Coinatmradar](http://coinatmradar.com)
21. [Coinbase Cryptocurrency Market](https://www.coinbase.com/)
22. [DASH Digital Cash](https://www.dash.org/)
23. [EtherChain Ethereum Block Explorer](https://www.etherchain.org/accounts/)
24. [Etherscan Ethereum Block Explorer](https://etherscan.io/)
25. [Financial Underground Kingdom](https://fuk.io/)
26. [Follow the Bitcoin | Automating OSINT BLOGPOST](http://www.automatingosint.com/blog/2017/09/follow-the-bitcoin-with-python-blockexplorer-and-webhose-io/)
27. [Fried Cryptocurrency Scam Checker](https://fried.com/crypto-scam-checker/)
28. [Learn Me A Bitcoin](http://learnmeabitcoin.com/tools/path/)
29. [LiveCoinWatch Cryptocurrency Market Index](https://www.livecoinwatch.com/)
30. [MoneroBlocks Monero Block Explorer](https://moneroblocks.info/)
31. [https://twitter.com/chainalysis TOOL](https://twitter.com/chainalysis)
32. [Random.org Bytes](https://www.random.org/bytes/)
33. [SHA-256 hash calculator | Xorbin](https://xorbin.com/tools/sha256-hash-calculator)
34. [Wallet Explorer](http://walletexplorer.com)
35. [WalletExplorer Bitcoin Block Explorer](https://www.walletexplorer.com/)
36. [WalletExplorer.com: smart Bitcoin block explorer](https://www.walletexplorer.com/)
37. [XMRChain Block Explorer](https://xmrchain.net/)
----

### Software
1. [Accuity](https://accuity.com/)
2. [Thomson Reuters World-Check One](https://www.refinitiv.com/en/products/world-check-kyc-screening/world-check-one-kyc-verification)
----

### Aviation Assets
1. [ADS-B Exchange](https://global.adsbexchange.com/VirtualRadar/desktop.html)
2. [Airlinecodes](http://www.airlinecodes.co.uk/aptcodesearch.asp)
3. [Airliners](https://www.airliners.net/)
4. [AIRLIVE.net](http://www.airlive.net/)
5. [AirNav Airport Information](http://www.airnav.com/airports/)
6. [Airport Code Database Search](http://www.airlinecodes.co.uk/aptcodesearch.asp)
7. [Airport Webcams](http://airportwebcams.net/)
8. [bellingcat - A Beginner's Guide To Flight Tracking - bellingcat](https://www.bellingcat.com/resources/how-tos/2019/10/15/a-beginners-guide-to-flight-tracking/)
9. [Black Book - Aviation Records](http://www.blackbookonline.info/aviation-public-records.aspx)
10. [Counterfeit Aircraft Parts](https://www.havocscope.com/tag/counterfeit-aircraft-parts/)
11. [FAA Aircraft Registry](https://www.faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/)
12. [FAA Registry](https://registry.faa.gov/aircraftinquiry/)
13. [FAA Registry N-Number Search](https://registry.faa.gov/aircraftinquiry/NNum_Inquiry.aspx)
14. [FAA Registry Name Search](https://registry.faa.gov/aircraftinquiry/Name_Inquiry.aspx)
15. [FAA Unapproved Parts Notifications (UPN) (Airplane)](https://www.faa.gov/aircraft/safety/programs/sups/upn/)
16. [Faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/interactive_aircraft_inquiry](https://www.faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/interactive_aircraft_inquiry/)
17. [FlightAware (UK)](https://uk.flightaware.com/)
18. [FlightAware (US)](https://flightaware.com/)
19. [FlightRadar24](https://www.flightradar24.com/60,15/6)
20. [Flight Wise](http://flightwise.com/)
21. [Jeppesen](http://ww1.jeppesen.com/index.jsp)
22. [Jet Photos](https://www.jetphotos.com/)
23. [Landings Pilot License Search](http://www.landings.com/evird.acgi$pass*193800885!_h-www.landings.com/_landings/pages/search/search_namd_full.html)
24. [List of civil aircraft registration prefixes](https://en.wikipedia.org/wiki/List_of_aircraft_registration_prefixes)
25. [Listen to Live ATC (Air Traffic Control) Communications](https://www.liveatc.net/)
26. [Minnesota DOT N-Number Search](https://dotapp7.dot.state.mn.us/aeroreg-public/lookupAircraft.jsp)
27. [NRD Aviation Radio Frequencies](http://nationalradiodata.com/aviation-airport-scanner-frequencies-home.jsp)
28. [NTSB Aviation Accident Database](https://www.ntsb.gov/_layouts/ntsb.aviation/index.aspx)
29. [Passenger Airlines Wiki](https://en.wikipedia.org/wiki/List_of_passenger_airlines#List_of_passenger_airlines)
30. [Planefinder](https://planefinder.net/)
31. [Plane Logger](http://www.planelogger.com/Aircraft)
32. [PlaneSpotters Aviation ID](https://www.planespotters.net/)
33. [Plane Spotters](https://www.planespotters.net/)
34. [Radar Box 24](https://www.radarbox24.com/flight/AA2097#)
35. [Radar Virtuel](http://www.radarvirtuel.com/)
36. [Travel By Drone](http://travelbydrone.com/)
37. [World Aeronautical Database](http://worldaerodata.com/)
38. [Virtual Radar](https://global.adsbexchange.com/VirtualRadar/desktop.html)
----

### Rail Assets
1. [ACW Railway Maps](http://www.acwr.com/economic-development/rail-maps)
2. [Archive Freight Rail Works](http://archive.freightrailworks.org)
3. [Association of American Railroads](https://www.aar.org)
4. [Trainline](https://www.thetrainline.com)
5. [Nederlandse Spoorwegen](https://www.ns.nl/)
6. [Data.gov Railway](https://catalog.data.gov/dataset?tags=railroad)
7. [DOT Railroads](https://www.transportation.gov/railroads)
8. [OpenRailwayMap](https://www.openrailwaymap.org/)
9. [Federal Railroad Administration](https://www.fra.dot.gov/Page/P0001)
10. [International Association of Railway Operations](http://www.iaror.org)
11. [OpenRailwayMap](https://www.openrailwaymap.org/)
12. [Rail Delivery Group](https://www.raildeliverygroup.com/our-services/rail-data.html)
13. [RailWebcams](http://railwebcams.net)
14. [World By Map Railways](http://world.bymap.org/Railways.html)
----

### Maritime Assets
1. [AIS-Schiffspositionen/-verkehr in Echtzeit](https://www.marinetraffic.com/de/)
2. [BIC: Bureau of International Containers](https://www.bic-code.org/)
3. [BoatFax Hull ID Number (HIN) Search](https://www.boatfax.com/index.php?option=com_bfx_checkit&Itemid=184)
4. [BoatInfoWorld Documented Vessels](https://www.boatinfoworld.com/)
5. [BoatInfoWorld.com - Search, View &amp; Download Vessel Information](https://www.boatinfoworld.com/)
6. [BoatNerd](http://ais.boatnerd.com/)
7. [Cargo Loop](http://www.cargoloop.com)
8. [Container tracking](http://container-tracking.org/)
9. [Croatian register of shipping > Home](http://www.crs.hr/en-us/home.aspx)
10. [Cruise Ship Tracker / Tracking Map Live](http://www.cruisin.me/cruise-ship-tracker/)
11. [CruiseMapper](https://www.cruisemapper.com/)
12. [Equasis](http://www.equasis.org/EquasisWeb/public/HomePage)
13. [eShips World Shipping Register](http://eships.net/)
14. [FleetMon](https://www.fleetmon.com)
15. [Florida Hull Number Search](http://pas.fdle.state.fl.us/pas/restricted/PAS/StolenBoats.jsf)
16. [Free AIS Ship Tracking of Marine Traffic - VesselFinder](https://www.vesselfinder.com/)
17. [FreightMetrix](http://www.freightmetrics.com.au/Marine/ ShipTracking/tabid/483/Default.aspx)
18. [http://www.gibraltarship.com/](http://www.gibraltarship.com/)
19. [Inmarsat.com/wp-content/uploads/2013/10/ships_directory.jpg](https://www.inmarsat.com/wp-content/uploads/2013/10/ships_directory.jpg)
20. [IOC Live Piracy Map](https://www.icc-ccs.org/piracy-reporting-centre/live-piracy-map)
21. [Live Cruise Ship Tracker](https://www.livecruiseshiptracker.com/)
22. [LocusTraxx](http://www.locustraxx.com)
23. [Maritime-connector.com/ships](http://maritime-connector.com/ships/)
24. [Maritime-database.com](https://www.maritime-database.com/)
25. [MaritimeConnector](http://maritime-connector.com)
26. [myShipTracking](https://www.myshiptracking.com/)
27. [Navipedia](http://www.navipedia.net/index.php/Package_and_Container_Tracking)
28. [NOAA Vessel Search](https://www.st.nmfs.noaa.gov/st1/CoastGuard/VesselByName.html)
29. [OpenSeaMap](http://map.openseamap.org)
30. [OSP Maritime VHF](http://www.oldskoolphreak.com/tfiles/hack/fcc_radio.pdf)
31. [Panjiva](https://panjiva.com/search)
32. [PortFocus Port/Marina Info](http://portfocus.com/)
33. [Roose+Partners](http://rooselaw.co.uk/index.html)
34. [SailWX Cruise Ship and Ocean Liner Tracker](https://www.sailwx.info/shiptrack/cruiseships.phtml)
35. [SailWX Great Lakes Vessel Tracker](https://www.sailwx.info/shiptrack/greatlakesandsea.phtml)
36. [ScannerNet](http://www.scannernet.nl/maritiem/live-ais)
37. [ShipAIS](http://www.shipais.com/)
38. [ShipmentLink](https://www.shipmentlink.com/servlet/TDB1_CargoTracking.do)
39. [ShipSpotting](http://www.shipspotting.com/ais/index.php)
40. [Shodan ShipTracker](https://shiptracker.shodan.io/)
41. [SkunkTracker](https://www.mobilegeographics.com/skunktracker/)
42. [Terrestrial & Satellite AIS Tracking Service in Realtime](http://www.vesseltracker.com/app)
43. [TrackTrace Bill of Lading Tracking](https://www.track-trace.com/bol)
44. [Vessel Finder](https://www.vesselfinder.com/)
45. [World seaports catalogue, marine and seaports marketplace](http://ports.com/)
----

### Cargo Container Bill of Lading | Tracking
1. [Bosch Service Solutions](https://www.boschservicesolutions.com/)
2. [ContainerTracking](http://container-tracking.org/)
3. [IHS Markit Tools](https://ihsmarkit.com/products/maritime-global-trade-atlas.html)
4. [ImportGenius](https://www.importgenius.com/)
5. [PIERS Bill of lading](https://ihsmarkit.com/products/piers.html)
6. [Searates Container Tracking](https://www.searates.com/container/tracking)
7. [TrackTrace](https://www.track-trace.com/)
8. [TrackTrace Container Tracking](https://www.track-trace.com/container)
9. [SECURESYSTEM (Sattelite, GSM & AIS)](https://www.securesystem.net/)
10. [Zillionsource](http://www.zillionsource.com/)
----

### OSINT FRAMEWORK -SOURCES CTY-
1. [Business Registers in EU Member States](https://e-justice.europa.eu/content_business_registers_in_member_states-106-en.do)
----

### Maps
1. [Arcgis](https://www.arcgis.com)
2. [Baidu Maps 百度地图](http://map.baidu.com/)
3. [BatchGeo: Erstellen Sie Karten aus Ihren Daten](https://de.batchgeo.com/)
4. [ctrlq.org Where am I](https://ctrlq.org/maps/where/)
5. [Custom Map Creator & Map Maker | Maptive](https://www.maptive.com/)
6. [Daum 지도 Korean Maps](http://map.daum.net/)
7. [De.batchgeo.com](Batchgeo)
8. [Descartes GeoVisual Search](https://search.descarteslabs.com/)
9. [DevCity Buildings in the Netherlands by year of construction](http://dev.citysdk.waag.org/buildings/)
10. [Every Disputed Territory in the World [Interactive Map]](http://metrocosm.com/disputed-territories-map.html)
11. [Flickr: Explore everyone's photos on a Map](http://www.flickr.com/map/)
12. [Free Map Tools](https://www.freemaptools.com/)
13. [gmapgis Draw on Google Maps](https://www.gmapgis.com/)
14. [Google My Maps](https://www.google.com/maps/d/)
15. [gosur Interaktive Karte Online Map](http://www.gosur.com/map/)
16. [HERE WeGo](https://wego.here.com/?x=ep&map=49.8044,9.977,10,normal)
17. [Kepler Geospatial Datamining](http://kepler.gl/#/)
18. [Map Tools: List of Google maps tools](https://www.mapdevelopers.com/map_tools.php)
19. [Mapify.us OSIT](http://mapify.us)
20. [Mapillary](https://www.mapillary.com/)
21. [Mapillary Crowdsourced Street-level Photos](https://www.mapillary.com/)
22. [MapMachine - National Geographic](http://maps.nationalgeographic.com/map-machine#s=r&c=43.74999999999998,%20-99.71000000000001&z=4)
23. [MapQuest](http://www.mapquest.com/)
24. [Maps - Sahel and West Africa Club Secretariat](https://www.oecd.org/swac/maps/)
25. [Maps | West Africa Gateway | Portail de l'Afrique de l'Ouest](http://www.west-africa-brief.org/maps)
26. [Metrocosm  Disputed Territory](http://metrocosm.com/disputed-territories-map.html)
27. [Naver Korean Maps  네이버 지도](http://map.naver.com/)
28. [Official MapQuest](https://www.mapquest.com/)
29. [OpenStreetCam](http://openstreetcam.org/map/)
30. [Perry-Castañeda Library Map Collection](http://www.lib.utexas.edu/maps/index.html)
31. [Photo-Interpretation-Student-Handbook.-Photo-Interpretation-Principles.pdf](http://sites.miis.edu/geospatialtools2012/files/2012/07/Photo-Interpretation-Student-Handbook.-Photo-Interpretation-Principles.pdf)
32. [Scribble Draw On Maps and Make Them Easily.](https://www.scribblemaps.com)
33. [Scribble Maps OSIT](http://scribblemaps.com)
34. [Snap Map](https://map.snapchat.com/)
35. [TRAVIC TRACKER GEOPS](https://tracker.geops.ch)
36. [Truesize Compare Countries With This Simple Tool](http://thetruesize.com/)
37. [Washington Post- 40 maps that explain the world](http://www.washingtonpost.com/blogs/worldviews/wp/2013/08/12/40-maps-that-explain-the-world/)
38. [Wikimapia](http://wikimapia.org/)
39. [World Aeronautical Database](http://worldaerodata.com/)
40. [www.arcgis.com](https://www.arcgis.com)
41. [Yandex.Maps a detailed world map](https://yandex.com/maps/)
----

