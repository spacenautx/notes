### Tools - Abuse
1. [Cyberbullying.org/report](https://cyberbullying.org/report)
2. [Datacentrumgids (Benelux)](https://www.datacentrumgids.nl/)
3. [Gethuman (USA)](http://gethuman.com)
4. [Meldknop.nl - report abuse (NL)](https://meldknop.nl)
5. [Veiliginternetten.nl (NL)](https://veiliginternette.nl)
----

### Addons - Archive
1. [Go Back in Time (Chrome)](https://chrome.google.com/webstore/detail/go-back-in-time/hgdahcpipmgehmaaankiglanlgljlakj)
2. [Passive Cache (FF)](https://addons.mozilla.org/en-US/firefox/addon/passive-cache/)
3. [Resurrect Pages (FF)](https://addons.mozilla.org/nl/firefox/addon/resurrect-pages/)
----

### Tools - Android emulators
1. [Bluestacks](https://www.bluestacks.com/nl/index.html)
2. [Genymotion](https://www.genymotion.com/)
3. [Leapdroid](https://leapdroid.en.softonic.com/)
4. [Nox](https://www.bignox.com/)
----

### Addons - Documents
1. [Unpaywall (Chrome)](https://chrome.google.com/webstore/detail/unpaywall/iplffkdpngmdjhlpjmppncnlhomiipha)
2. [Unpaywall (FF)](https://addons.mozilla.org/en-GB/firefox/addon/unpaywall/?src=search)
----

### Hunchly
1. [Hunchly](https://www.hunch.ly/)
2. [Huchkly Webinar on YouTube](https://www.youtube.com/watch?v=wA1ec0dPYhw)
3. [Hunchly Downloads](https://automatingosint.us10.list-manage.com/track/click?u=c132a47e7c8b127d7654a6233&id=126f2c3a3b&e=d2b647903a)
4. [Installing Hunchly on Windows - Hunchly Knowledge Base](https://support.hunch.ly/article/14-installing-hunchly-on-windows)
5. [Using-the-hunchly-maltego-transforms](https://support.hunch.ly/article/47-1-using-the-hunchly-maltego-transforms)
----

### Addons - Email
1. [Name2Email](https://chrome.google.com/webstore/detail/name2email-by-reply/mnbdclgaeiapdnhfpbfalfjfcjddfaii?hl=en)
----

### Addons - Corporate
1. [Ocean.io](https://chrome.google.com/webstore/detail/oceanio/gkocieepdpljclejnkijcpicoccgnkhd)
----

### Tools - Darkweb
1. [Deepdotweb - List of most popular darkmarkets](https://www.deepdotweb.com/2013/10/28/updated-llist-of-hidden-marketplaces-tor-i2p/)
2. [DeepWeb - Deeplinks](https://www.deepweb-sites.com/deep-web-links-2015/)
3. [DeepWeb - Top 50 onion-sites](https://www.deepweb-sites.com/top-50-dark-web-onion-domains-pagerank/)
4. [Hunchly's Darkweb report](https://www.dropbox.com/s/dvm23v5dhoe1cz4/HiddenServices.xlsx?dl=0&utm_content=buffer7f07c&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
5. [i2p (download)](https://geti2p.net/en/)
6. [Ichidan (visit via Tor)](http://ichidanv34wrx7m7.onion)
7. [Onionscan](https://onionscan.org/)
8. [OnionShare](https://blog.torproject.org/tor-heart-onionshare)
9. [Tor (download)](https://www.torproject.org/index.html.en)
10. [TorNodes](https://www.dan.me.uk/tornodes)
11. [Zeronet (download)](https://zeronet.io/)
----

### Tools - Email
1. [Email-format](https://email-format.com/)
2. [Gmail](https://gmail.com)
3. [Guerillamail](http://www.guerrillamail.com/)
4. [Hunter.io](http://hunter.io)
5. [Hushmail](http://hushmail.com)
6. [Inboxbear - Passwordless email addresses](http://inboxbear.com)
7. [IntelTechniques - Email](https://inteltechniques.com/osint/email.search.html)
8. [Mail.com - free email creations](https://mail.com)
9. [Mailinator](https://www.mailinator.com/)
10. [Manycontacts](https://www.manycontacts.com/)
11. [Protonmail](http://protonmail.com)
12. [Verify an email address - Hunter.io](https://hunter.io/email-verifier)
13. [Verify an email address - Mailhippo](https://tools.verifyemailaddress.io/)
14. [Verify an email address - Mailtester](http://www.mailtester.com)
15. [Verify an email address - Verifalia](http://verifalia.com/validate-email)
----

### MindMup Mindmap Tool
1. [MindMup: Zero-Friction Free Mind Mapping Software Online](https://www.mindmup.com/)
2. [MindMup 2.0 For Google Drive](https://drive.mindmup.com/)
3. [MindMup](https://blog.mindmup.com/)
4. [Sign in](https://plus.google.com/communities/112831595986131146219)
5. [Mindmup](https://www.facebook.com/mindmupapp)
6. [MindMup](https://github.com/mindmup)
7. [MindMup 2.0 Preview](https://www.youtube.com/watch?v=--v7ZfTHNJ8)
----

### Timeline
1. [Office Timeline](https://www.officetimeline.com)
2. [Preceden Timeline Maker](https://www.preceden.com/)
3. [TimeGraphics](https://time.graphics/de/)
4. [Timeline JS](https://timeline.knightlab.com/)
5. [Sutori](https://www.sutori.com/)
6. [Myhistro](http://www.myhistro.com)
7. [SmartDraw](https://www.smartdraw.com/)
8. [Custom timelines in TweetDeck](https://blog.twitter.com/official/en_us/a/2013/custom-timelines-in-tweetdeck.html?utm_campaign=elearningindustry.com&utm_source=%252Ftop-10-free-timeline-creation-tools-for-elearning-professionals&utm_medium=link)
9. [Free-Timeline.com](http://free-timeline.com)
10. [Timeglider: web-based timeline software](https://timeglider.com)
11. [Timetoast](https://www.timetoast.com)
12. [Zotero](https://www.zotero.org/)
13. [Aeontimeline](http://www.aeontimeline.com/)
14. [Thetimelineproject](http://thetimelineproj.sourceforge.net/)
----

### Tools - Github
1. [Zen](https://github.com/s0md3v/Zen)
----

### Addons - PGP
1. [Mailvelope](https://www.mailvelope.com/en/)
----

### Tools  - Images and Documents
1. [Exif viewer tool](http://owl.phy.queensu.ca/~phil/exiftool/)
2. [File Extensions](https://www.file-extensions.org)
3. [Foca](https://www.elevenpaths.com/labstools/foca/index.html)
4. [Forensically](https://29a.ch/photo-forensics/#level-sweep)
5. [Get-metadata.com](https://www.get-metadata.com/)
6. [Ghiro](http://www.getghiro.org/)
7. [ImageIdentify](https://www.imageidentify.com)
8. [InVid - Verify fake videos](http://www.invid-project.eu/tools-and-services/invid-verification-plugin/)
9. [Izitru.com](http://izitru.com)
10. [Online convert](https://www.online-convert.com/)
11. [PDF Redact Tools](https://github.com/firstlookmedia/pdf-redact-tools)
12. [Spiderpig - metadata on documents](https://github.com/hatlord/Spiderpig)
13. [SunCallculation](http://suncalc.net/)
14. [Text-compare](https://text-compare.com/)
15. [ThoughtfulDev/EagleEye](https://github.com/ThoughtfulDev/EagleEye)
----

### Addons - Image/video
1. [Exif viewer (FF)](https://addons.mozilla.org/en-US/firefox/addon/exif-viewer/)
2. [Exif viewer (Chrome)](https://chrome.google.com/webstore/detail/exif-viewer/nafpfdcmppffipmhcpkbplhkoiekndck?hl=en)
3. [Google 'View Image' button (Chrome)](https://chrome.google.com/webstore/detail/google-search-view-image/hgngncnljacgakaiifjcgdnknaglfipo?hl=nl)
4. [Hover Zoom (Chrome)](https://chrome.google.com/webstore/detail/hover-zoom/nonjdcjchghhkdoolnlbekcfllmednbl?hl=en)
5. [Hover Zoom (FF)](https://addons.mozilla.org/en-GB/firefox/addon/hoverzoom-plus/?src=search)
6. [NooBox (Chrome)](https://chrome.google.com/webstore/detail/noobox/kidibbfcblfbbafhnlanccjjdehoahep?hl=nl)
7. [Google 'View Image' button (FF)](https://addons.mozilla.org/en-US/firefox/addon/view-image/)
8. [RightSpeed for YouTube (Chrome)](https://chrome.google.com/webstore/detail/rightspeed-for-youtube/flibmeaimaamdoldglmbcooncgjedblo?hl=nl)
9. [Save All Images (Chrome)](https://chrome.google.com/webstore/detail/save-all-images/jmolegopjlipmoedaoijpjaddhjjckal?hl=nl)
10. [Send to Exif Viewer (Chrome)](https://chrome.google.com/webstore/detail/send-to-exif-viewer/gogiienhpamfmodmlnhdljokkjiapfck?hl=nl)
11. [Video DownloadHelper (Chrome)](https://chrome.google.com/webstore/detail/video-downloadhelper/lmjnegcaeklhafolokijcfjliaokphfk?hl=nl)
12. [Video DownloadHelper (FF)](https://addons.mozilla.org/nl/firefox/addon/video-downloadhelper/?src=search)
13. [Video Downloader professional (Chrome)](https://chrome.google.com/webstore/detail/video-downloader-professi/kmdldgcmokdpmacblnehppgkjphcbpnn?hl=nl)
14. [Video verification InVid (FF and Chrome)](http://www.invid-project.eu/tools-and-services/invid-verification-plugin/)
15. [Who Stole My Pictures? (FF)](https://addons.mozilla.org/en-US/firefox/addon/who-stole-my-pictures/)
----

### Tools - Leaks
1. [LeakScraper](https://github.com/Acceis/leakScraper/wiki/leakScraper)
----

### Addons - OSINT
1. [Auto Refresh Page (Chrome)](https://chrome.google.com/webstore/detail/auto-refresh/ifooldnmmcmlbdennkpdnlnbgbmfalko?hl=nl)
2. [Data Scraper (Chrome)](https://chrome.google.com/webstore/detail/data-scraper-easy-web-scr/nndknepjnldbdbepjfgmncbggmopgden?hl=nl)
3. [Distill (Chrome)](https://chrome.google.com/webstore/detail/distill-web-monitor/inlikjemeeknofckkjolnjbpehgadgge?hl=en-US)
4. [Distill (FF)](https://addons.mozilla.org/en-US/firefox/addon/distill-web-monitor-ff/)
5. [DownloadMaster 2016 (Chrome)](https://chrome.google.com/webstore/detail/download-master-2016/ghgbnmhbcbbgoalbahdpehncclfhhkge?hl=nl)
6. [Fireshot (Chrome)](https://chrome.google.com/webstore/detail/take-webpage-screenshots/mcbpblocgmgfnpjjppndjkmgjaogfceg?hl=nl)
7. [Fireshot (FF)](https://addons.mozilla.org/nl/firefox/addon/fireshot/?src=search)
8. [Google translate (Chrome)](https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb?hl=nl)
9. [Nimbus Screenshot (Chrome)](https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj?hl=en)
10. [Nimbus Screenshot (FF)](https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/)
11. [OSINT browser (Chrome)](https://chrome.google.com/webstore/detail/open-source-intelligence/bclnaepfegjimpinlmgnipebbknlmmbh/related?hl=en-US)
12. [OSINT Browser (FF)](https://addons.mozilla.org/en-US/firefox/addon/osint-browser-extension/)
13. [Practical Startpage (Chrome)](https://chrome.google.com/webstore/detail/practical-startpage/ikjalccfdoghanieehppljppanjlmkcf?hl=nl)
14. [Screengrab! (Chrome)](https://chrome.google.com/webstore/detail/screengrab/fccdiabakoglkihagkjmaomipdeegbpk?hl=nl)
15. [Translate Selected Text (Chrome)](https://chrome.google.com/webstore/detail/translate-selected-text/fbimffnjoeobhjhochngikepgfejjmgj?hl=nl)
----

### Firefox Extensions Productivity
1. [Google Translate for Firefox](https://addons.mozilla.org/en-US/firefox/addon/google-translator-for-firefox/)
2. [Exif Viewer](https://addons.mozilla.org/en-US/firefox/addon/exif-viewer/?)
3. [Image Search Options](https://addons.mozilla.org/en-US/firefox/addon/image-search-options/?)
4. [Resurrect Pages](https://addons.mozilla.org/en-US/firefox/addon/resurrect-pages/?)
5. [Nimbus Screen Capture](https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/?)
6. [Copy Plaintext](https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/?)
7. [JSON XML Viewer](https://addons.mozilla.org/en-US/firefox/addon/mjsonviewer/?)
8. [Copy selected URL](https://addons.mozilla.org/en-US/firefox/addon/copy-selected-links/?)
9. [Video Download Helper](https://addons.mozilla.org/en-US/firefox/addon/video-downloadhelper/?)
10. [Bulk Media Downloader](https://addons.mozilla.org/en-US/firefox/addon/bulk-media-downloader/?)
11. [IP Domain Info](https://addons.mozilla.org/en-US/firefox/addon/bulk-media-downloader/?)
12. [Multi Account Container](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/?)
13. [Download Star](https://addons.mozilla.org/en-US/firefox/addon/download-star/)
----

### Tools - Maps
1. [FoodnDrink](https://github.com/WebBreacher/foodndrink)
2. [Microsoft/USBuildingFootprints](https://github.com/Microsoft/USBuildingFootprints/)
3. [RoboSat](https://github.com/mapbox/robosat)
----

### Tools - OSINT
1. [101+ OSINT resources](https://i-sight.com/resources/101-osint-resources-for-investigators/)
2. [AnswerThePublic](https://answerthepublic.com/)
3. [Bellingcat - OSINT Links](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
4. [Browsershots](http://browsershots.org)
5. [Browsershots (screenshot via mobile device)](http://browsershots.net/)
6. [Cheatsheet Bing search (NL)](https://drive.google.com/file/d/0B7IvVbyx3btlajl2aDJrVVJIOUE/view)
7. [Cheatsheet Duckduckgo search (NL)](https://drive.google.com/file/d/0B7IvVbyx3btldkpYYmRySndVQTA/view)
8. [Cheatsheet Yandex search(NL)](https://drive.google.com/file/d/0B7IvVbyx3btldktiNC1YelZaXzA/view)
9. [CloudFail - find IPs behind Cloudflare](https://github.com/m0rtem/CloudFail)
10. [Databasic](http://databasic.io)
11. [Datawrapper](http://datawrapper.de)
12. [DFIR Training - Various tools](https://www.dfir.training/tools/osint/)
13. [Digital Methods Initiative Tool database](https://wiki.digitalmethods.net/Dmi/ToolDatabase)
14. [Fakenamegenerator](http://www.fakenamegenerator.com)
15. [FakeSpot - identify fake reviews](https://www.fakespot.com/)
16. [Flowchart Domains](https://inteltechniques.com/data/Domain.png)
17. [Flowchart Location search](https://inteltechniques.com/data/location.png)
18. [Flowchart OSINT](https://inteltechniques.com/menu.links.html)
19. [Followthatpage.com](https://followthatpage.com/)
20. [Hunchly](http://hunch.ly)
21. [Keep Google - Google notes](https://keep.google.com)
22. [Listly](https://listly.io/)
23. [Maltego - CaseFile](https://www.paterva.com/web7/buy/maltego-clients/casefile.php)
24. [Microsoft Steps Recorder](https://support.microsoft.com/en-us/help/22878/windows-10-record-steps)
25. [MidaSearch OSINT websites](https://midasearch.org/osint-websites/)
26. [Mindmap OSINT Digital Intelligence](https://atlas.mindmup.com/digintel/digital_intelligence_training/index.html)
27. [MindMup](http://mindmup.com)
28. [Morphing photos](http://www.morphthing.com/)
29. [OSINTframework](https://osintframework.com/)
30. [OSINTframework by @CryptoCypher](https://pastebin.com/raw/4WFNefEi)
31. [PDFmyURL](http://pdfmyurl.com)
32. [Pentest-tools](https://pentest-tools.com/home)
33. [ResearchClinic](http://www.researchclinic.net/)
34. [SANS - Sec487 - Blue teaming wiki](https://github.com/sans-blue-team/sec487-wiki/blob/master/index.md)
35. [Sken.io](https://www.sken.io/)
36. [Spiderfoot](http://spiderfoot.net)
37. [Station](https://getstation.com/)
38. [UK OSINT - helpfull tools](http://www.uk-osint.net/)
39. [Visualize your investigation](https://vis.occrp.org/)
40. [Xray OSINT Gathering tool](https://n0where.net/network-osint-gathering-tool-xray)
----

### Tools - Phone numbers
1. [Receive SMS (free)](http://receive-sms-online.com/)
2. [Receive-SMS (free)](https://receive-sms.com/)
3. [Receive SMS (free)](http://sms.sellaite.com/)
4. [Receive SMS Online](https://www.receivesmsonline.net/)
----

### Web Archiving Mirroring
1. [Archive.is](http://archive.is/)
2. [Webrecorder](https://webrecorder.io/)
3. [PageFreezer | Legal](https://legal.pagefreezer.com/)
4. [Wget - GNU Project - Free Software Foundation](https://www.gnu.org/software/wget/)
5. [HTTrack Website Copier](http://www.httrack.com/)
6. [Website Downloader](https://websitedownloader.io/)
----

### Crawl Links
1. [LinkChecker](https://wummel.github.io/linkchecker/)
----

### Metadata Tools
1. [11Paths MetashieldCleanup Metadata Manager](https://metashieldclean-up.elevenpaths.com/)
2. [Anonymouth Metadata Stripper](https://github.com/psal/anonymouth)
3. [CameraSummary Metadata Analyzer](https://camerasummary.com/)
4. [Doc Scrubber™](https://www.brightfort.com/docscrubber.html)
5. [EXIF Data Viewer Metadata Analyzer](http://exifdata.com/)
6. [ExifPro Metadata Editor](http://www.exifpro.com/)
7. [ExtractMetadata Metadata Analyzer](https://www.extractmetadata.com/)
8. [FindExif Metadata Analyzer](http://www.findexif.com/)
9. [FOCA](https://www.elevenpaths.com/labstools/foca/)
10. [Get-Metadata EXIF Data Viewer](https://www.get-metadata.com/)
11. [Jeffrey's Image Viewer Metadata Analyzer](http://exif.regex.info/exif.cgi)
12. [Jose's Image Metadata Analyzer](https://metadataviewer.herokuapp.com/)
13. [JPEG Snoop Metadata Analyzer](https://github.com/ImpulseAdventure/JPEGsnoop)
14. [MAT: Metadata Anonymisation Toolkit](https://mat.boum.org/)
15. [Metagoofil Metadata Collector](http://www.edge-security.com/metagoofil.php)
16. [Metashield Analyzer Online, a service that analyzes the metadata in office documents](https://metashieldanalyzer.elevenpaths.com/)
17. [OOMetaExtractor](https://oometaextractor.codeplex.com/)
18. [PDF Redact Metadata Stripper](https://github.com/firstlookmedia/pdf-redact-tools)
19. [Phil Harvey Exif Tool Metadata Editor](http://owl.phy.queensu.ca/~phil/exiftool/)
20. [Spiderpig Metadata and Document Harvester](https://github.com/hatlord/Spiderpig)
21. [Thexifer Metadata Editor](https://www.thexifer.net/)
22. [Verexif Metadata Remover](http://www.verexif.com/en/)
----

### Addons - Phone numbers
1. [WhatsAllApp](https://github.com/LoranKloeze/WhatsAllApp)
2. [Whatsfoto](https://github.com/zoutepopcorn/whatsfoto)
----

### Addons - Privacy/security
1. [Adblock Plus (Chrome)](https://chrome.google.com/webstore/detail/adblock-plus/cfhdojbkjhnklbpkdaibdccddilifddb)
2. [Copy All Links (FF)](https://addons.mozilla.org/nl/firefox/addon/copy-all-links/?src=search)
3. [Canvas Defender (Chrome)](https://chrome.google.com/webstore/detail/canvas-defender/obdbgnebcljmgkoljcdddaopadkifnpm?hl=en)
4. [Canvas Defender (FF)](https://addons.mozilla.org/en-US/firefox/addon/no-canvas-fingerprinting/)
5. [Click&Clean (Chrome)](https://chrome.google.com/webstore/detail/clickclean/ghgabhipcejejjmhhchfonmamedcbeod?hl=en)
6. [Copy All Links (Chrome)](https://chrome.google.com/webstore/detail/copy-links/kcpoommnneaebpfgaoejklgemonkmjpc?hl=nl)
7. [Ghostery (Chrome)](https://chrome.google.com/webstore/detail/ghostery/mlomiejdfkolichcflejclcbmpeaniij?hl=en)
8. [Ghostery (FF)](https://addons.mozilla.org/en-US/firefox/addon/ghostery/)
9. [Facebook - Disconnect (Chrome)](https://chrome.google.com/webstore/detail/disconnect-facebook-pixel/nnkndeagapifodhlebifbgbonbfmlnfm?hl=en)
10. [Facebook - Disconnect (FF)](https://addons.mozilla.org/en-GB/firefox/addon/facebook-disconnect-ii/?src=search)
11. [Fea Keylogger](https://chrome.google.com/webstore/detail/fea-keylogger/fgkghpghjcbfcflhoklkcincndlpobja?hl=en)
12. [Dataselfie (FF & Chrome)](https://dataselfie.it/#/)
13. [Don't track me Google](https://chrome.google.com/webstore/detail/dont-track-me-google/gdbofhhdmcladcmmfjolgndfkpobecpg?hl=en)
14. [HTTPS Everywhere (Chrome)](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp?hl=en)
15. [HTTPS Everywhere (FF)](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)
16. [Hola VPN (Chrome)](https://chrome.google.com/webstore/detail/unlimited-free-vpn-hola/gkojfkhlekighikafcpjkiklfbnlmeio?hl=en)
17. [Hola VPN (FF)](https://hola.org/firefox)
18. [Hoxx VPN (Chrome)](https://chrome.google.com/webstore/detail/hoxx-vpn-proxy/nbcojefnccbanplpoffopkoepjmhgdgh)
19. [Hoxx VPN (FF)](https://addons.mozilla.org/en-US/firefox/addon/hoxx-vpn-proxy/)
20. [Panda Safe Web (Chrome)](https://chrome.google.com/webstore/detail/panda-safe-web/fagakgcelolinfnkfgekcnedpaklfcok?hl=nl)
21. [Privacy Badger (Chrome)](https://chrome.google.com/webstore/detail/privacy-badger/pkehgijcmpdhfbdbbnkijodmdjhbjlgp)
22. [Privacy Badger (FF)](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)
23. [Referer Button (Chrome)](https://chrome.google.com/webstore/detail/referer-control/hnkcfpcejkafcihlgbojoidoihckciin?hl=en)
24. [Referer Button (FF)](https://addons.mozilla.org/en-US/firefox/addon/change-referer-button/)
25. [Resurrect Pages (FF)](https://addons.mozilla.org/en-US/firefox/addon/resurrect-pages/)
26. [Tampermonkey (Chrome)](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=nl)
----

### Tools - Privacy
1. [BrowserLeaks.com](https://browserleaks.com/)
2. [Downforeveryoneorjustme](http://downforeveryoneorjustme.com/)
3. [ipleak.net](https://ipleak.net/)
4. [Mijnonlineidentiteit (NL)](https://www.mijnonlineidentiteit.nl/social-media-privacy-instellingen/)
5. [OptOut Doc by @OsintNinja](https://docs.google.com/spreadsheets/d/1UY9U2CJ8Rnz0CBrNu2iGV3yoG0nLR8mLINcnz44XESI/edit#gid=1864750866)
6. [Pribot (AI-powered Privacy/Policies)](https://pribot.org/)
7. [Privacy Tools](https://epic.org/privacy/tools.html)
8. [Private Internet Access](https://www.privateinternetaccess.com)
9. [ProtonVPN](https://protonvpn.com/)
10. [Proxysnel](http://proxysnel.nl)
11. [Safeweb.norton.com](http://safeweb.norton.com)
12. [Should I Trust](https://github.com/ericalexanderorg/should-i-trust)
13. [Sucuri Security](http://sitecheck.sucuri.net)
14. [Terms of Service; Didn't Read](https://tosdr.org/)
15. [TwoFactorAuthentication](https://twofactorauth.org/)
16. [Verexif - remove exif data from pictures](http://www.verexif.com/en/)
17. [What's my User Agent?](http://www.whatsmyua.info/)
----

### Data Visualisation / Mapping
1. [Google fusion tables](http://www.google.com/fusiontables/Home/)
2. [Google spreadsheet charts](http://www.google.com/google-d-s/spreadsheets/)
3. [TouchGraph.com](http://www.touchgraph.com/seo)
4. [Leaflet](http://leafletjs.com/)
5. [CartoDB](http://cartodb.com/)
6. [Quadrigram](http://www.quadrigram.com/)
7. [GeoCommons](http://geocommons.com/)
8. [Processing](http://processingjs.org/)
9. [Visual.ly](http://visual.ly/)
10. [Wolfram|Alpha](http://www.wolframalpha.com/)
11. [Spatial.ly](http://spatial.ly/)
12. [De.batchgeo.com](https://de.batchgeo.com/)
13. [Google Maps Streetview Player](http://brianfolts.com/driver/)
14. [ScribbleMaps](https://www.scribblemaps.com/)
----

### Tools - Social Media
1. [Facebook - FindMyFBid](https://findmyfbid.com/)
2. [Facebook - ExtractFace](http://le-tools.com/ExtractFace.html#download)
3. [Facebook - PeopleYouMayKnow](https://gizmodo.com/keep-track-of-who-facebook-thinks-you-know-with-this-ni-1819422352)
4. [Facebook - Video Downloader](https://fbdown.net/)
5. [General - Meetfranz](https://meetfranz.com/)
6. [Instagram - GramSpy](http://gramspy.com/)
7. [Instagram - Instagram downloader](https://downloadgram.com/)
8. [Instagram - Instadp.site](https://instadp.site/)
9. [Instagram - Instaloader](https://github.com/instaloader/instaloader)
10. [Instagram - Instalooter](https://github.com/althonos/InstaLooter)
11. [Instagram - InstaSave](https://github.com/alshf89/InstaSave)
12. [Instagram - izuum](http://izuum.com)
13. [Instagram - SaveIG](https://saveig.com/)
14. [Instagram - StoriesIG](https://storiesig.com/)
15. [LinkedIn - LinkedIn2Username](https://github.com/initstring/linkedin2username)
16. [LinkedIn - Socilab search](http://www.socilab.com)
17. [LinkedIn - Search by email](https://www.linkedin.com/sales/gmail/profile/viewByEmail/ReplaceThis@byEmailDomain.com)
18. [Periscope - Download periscope videos](http://downloadperiscopevideos.com)
19. [Reddit - PushShift](http://files.pushshift.io/reddit/)
20. [Reddit - Reditr](http://reditr.com/)
21. [Snapchat - Grab Snaps](https://github.com/CaliAlec/snap-map-private-api)
22. [Telegram - Save history](https://github.com/pigpagnet/save-telegram-chat-history)
23. [Tumblr - Show original post](http://studiomoh.com/fun/tumblr_originals/)
24. [Twitter - Allmytweets](https://www.allmytweets.net/connect/)
25. [Twitter - Bio changed of following](http://spoonbill.io/)
26. [Twitter - Deadbird](https://keitharm.me/project/deadbird)
27. [Twitter - Download Twitter Video](http://www.downloadtwittervideo.com/)
28. [Twitter - Email Test](https://pdevesian.eu/tet)
29. [Twitter - Fakers](https://fakers.statuspeople.com)
30. [Twitter - Geotweets](http://geosocialfootprint.com/)
31. [Twitter - How far did your tweet travel?](https://tweetreach.com/)
32. [Twitter - OneMillionTweetMap](http://onemilliontweetmap.com)
33. [Twitter - Snapbird twitter history](https://snapbird.org/)
34. [Twitter - Socialbearing](https://socialbearing.com/)
35. [Twitter - Spoonbill](http://spoonbill.io/)
36. [Twitter - Tinfoleak](https://tinfoleak.com/)
37. [Twitter - Trendmap](https://www.trendsmap.com/)
38. [Twitter - Tweet Mapper](https://keitharm.me/project/tweet-mapper)
39. [Twitter - Tweetanalyzer](https://github.com/x0rz/tweets_analyzer)
40. [Twitter - Tweetbeaver](http://tweetbeaver.com/)
41. [Twitter - Tweetmap](https://www.mapd.com/demos/tweetmap/)
42. [Twitter - Tweets to Excel](http://twlets.com/)
43. [Twitter - Twiangulate](http://twiangulate.com/search/)
44. [Twitter - TwiMap](https://twimap.com/)
45. [Twitter - Twitonomy](http://www.twitonomy.com)
46. [Vkontakte - FindFace](https://findface.ru/)
47. [Vkontakte - Retreive metadata](https://gist.github.com/cryptolok/8a023875b47e20bc5e64ba8e27294261)
48. [WhatsApp - Retreive data](https://github.com/LoranKloeze/WhatsAllApp)
49. [WhatsApp - Create fake WhatsApp messages](http://www.fakewhats.com/generator)
----

### Infographics
1. [7 Tools for Building an Infographic in an Afternoon (Design Skills or Not)](https://blog.bufferapp.com/infographic-makers)
2. [Vizualize.me](http://vizualize.me/)
----

### Addons - Social Media
1. [Facebook - Advanced Facebook Search (Chrome)](https://chrome.google.com/webstore/detail/afs/gckmgjhcejhnfenfbippohhnfjkeaapj?hl=de-DE)
2. [Facebook - Secret Revealer Social Advanced Search Engine (Chrome)](https://chrome.google.com/webstore/detail/secret-revealer-social-ad/necinlbdnnabaagfgnamifjjpglgheba?hl=nl)
3. [Facebook - Tagged photos (Chrome)](https://chrome.google.com/webstore/detail/picturemate-view-tagged-f/khmlalkcjmglpgdkmkmmgjcajahkoigj)
4. [Facebook - Video Downloader (Chrome)](https://chrome.google.com/webstore/detail/fbdown-video-downloader/fhplmmllnpjjlncfjpbbpjadoeijkogc)
5. [General - Download photo Albums (Chrome)](https://chrome.google.com/webstore/detail/downalbum/cgjnhhjpfcdhbhlcmmjppicjmgfkppok?hl=nl)
6. [General - Intelligence search (Chrome)](https://chrome.google.com/webstore/detail/intelligence-search/dipfggodcibdmflidbceoaanadclgomm?hl=nl)
7. [General - Passive Recon](https://addons.mozilla.org/nl/firefox/addon/passiverecon/)
8. [General - Prophet (Chrome)](https://chrome.google.com/webstore/detail/prophet/alikckkmddkoooodkchoheabgakpopmg?hl=nl)
9. [Instagram - Instagram Story Viewer (Chrome)](https://chrome.google.com/webstore/detail/chrome-ig-story/bojgejgifofondahckoaahkilneffhmf)
10. [Instagram - Helper Tools for Instagram (Chrome)](https://chrome.google.com/webstore/detail/helper-tools-for-instagra/hcdbfckhdcpepllecbkaaojfgipnpbpb)
11. [LinkedIn - Contact info](https://chrome.google.com/webstore/detail/find-anyones-email-contac/jjdemeiffadmmjhkbbpglgnlgeafomjo)
12. [LinkedIn - Guest browser (FF)](https://addons.mozilla.org/en-US/firefox/addon/linkedin-guest-browser/)
13. [Pinterest - Guest browser (FF)](https://addons.mozilla.org/en-US/firefox/addon/pinterest-guest/)
14. [Reddit - All comments viewer](https://chrome.google.com/webstore/detail/reddit-all-comments-viewe/hgpgeobpjpchpmkecjppgcnekmbibgbi?hl=en-GB)
15. [Reddit - MostlyHarmless (Chrome)](http://kerrick.github.io/Mostly-Harmless/#features)
16. [Twitter - Treeverse](https://chrome.google.com/webstore/detail/treeverse/aahmjdadniahaicebomlagekkcnlcila?hl=en)
----

### Tools - Translation/Language
1. [Cyrcillic decoder](https://2cyr.com/decode/?lang=en)
2. [Ethnologue](https://www.ethnologue.com/)
3. [DeepL Translator](https://www.deepl.com/translator)
4. [FlockWatch](https://github.com/sjacks26/FlockWatch)
5. [Google Translate](http://translate.google.com)
6. [Know Your Meme](http://knowyourmeme.com/)
7. [NewOCR](https://www.newocr.com/)
8. [TagDef](https://tagdef.com)
9. [Urban Dictionairy](https://www.urbandictionary.com/)
10. [Yamii](https://www.yamli.com/arabic-keyboard/)
----

### Addons - Spoofing
1. [Geo location spoofing (Chrome)](https://chrome.google.com/webstore/detail/manual-geolocation/jpiefjlgcjmciajdcinaejedejjfjgki)
2. [User-Agent Switcher (Chrome)](https://chrome.google.com/webstore/detail/user-agent-switcher-for-c/djflhoibgkdhkhhcedjiklpkjnoahfmg)
3. [User-Agent Switcher (FF)](https://addons.mozilla.org/en-US/firefox/addon/user-agent-switcher/)
----

### Tools - URL Shortners
1. [Bit.do](https://bit.do)
2. [Bit.ly](http://bit.ly)
3. [CheckShortUrl](http://checkshorturl.com/)
4. [Grabify IP Logger](https://grabify.link/)
5. [Unfurlr](https://unfurlr.com/)
6. [Unshorten.it](http://unshorten.it)
7. [Unshorten.link](https://unshorten.link/)
----

### Addons - Who.is/hosting/technical information
1. [IP Address and Domain Information (Chrome)](https://chrome.google.com/webstore/detail/ip-address-and-domain-inf/lhgkegeccnckoiliokondpaaalbhafoa?hl=nl)
2. [Webrank Whois Lookup (Chrome)](https://chrome.google.com/webstore/detail/webrank-whois-lookup/ajebiiiaphmfaiookbhpepoongjejihh?hl=en)
----

### Tools - Verification
1. [Research Wikipedia edits](https://www.engadget.com/2018/05/06/facebook-friend-suggestions-helped-connect-extremists/?guccounter=1)
2. [Reviews - Fakespot](http://fakespot.com)
----

### Tools - Video
1. [TubeOffline](https://www.tubeoffline.com/)
----

### Tools - Virtual Machine
1. [Buscador OSINT VM](http://inteltechniques.com/buscador)
2. [Kali Linux](https://www.kali.org/)
3. [Linux Mint](https://linuxmint.com/download.php)
4. [Linux Ubuntu](https://www.ubuntu.com/download/desktop)
5. [VirtualBox (free)](https://www.virtualbox.org/wiki/Downloads)
6. [VMWare (paid)](https://www.vmware.com/)
----

### Tools- Who.is/domain information
1. [Subdomains - CTFR (Pyhton)](https://securityonline.info/ctfr-get-the-subdomains-from-a-https-website-in-a-few-seconds/)
----

### Visualization
1. [Draw.io](https://www.draw.io/)
2. [Datawrapper](https://www.datawrapper.de/)
3. [Kibana](https://www.elastic.co/products/kibana)
4. [Data Viz Catalogue](https://datavizcatalogue.com/)
5. [RAWGraphs](http://app.rawgraphs.io/)
6. [Visual Investigative Scenarios](https://vis.occrp.org/)
----

### Tools - YouTube
1. [HookTube](https://hooktube.com/)
2. [Keepvid](https://keepvid.com/)
3. [WatchFrameByFrame](http://www.watchframebyframe.com/)
4. [Yasiv](https://yasiv.com/youtube)
5. [Yout.com](https://yout.com)
6. [Youtube-DL](https://rg3.github.io/youtube-dl/)
7. [YTcomments](http://ytcomments.klostermann.ca/)
----

### Visualisation
1. [Flowchart Maker & Online Diagram Software](https://www.draw.io/)
2. [yEd Graph Editor](https://www.yworks.com/products/yed?)
3. [422 South OSIT](http://422.com)
4. [Open Data Showroom](http://opendata-showroom.org/en/)
5. [WebSequenceDiagrams](https://www.websequencediagrams.com/)
6. [VIS. Visual Investigative Scenarios platform:
		Pages](https://vis.occrp.org/)
7. [Gephi](https://gephi.org/)
8. [ORA-LITE: Software](http://www.casos.cs.cmu.edu/projects/ora/software.php)
9. [NodeExcel](https://nodexl.codeplex.com/)
10. [Sentinel Visualiser](http://www.fmsasg.com/)
11. [CodePlex Archive](https://archive.codeplex.com/?p=nodexl)
12. [The Linking Open Data cloud diagram](http://lod-cloud.net/)
13. [Danger Zone](https://hackernoon.com/osint-tool-for-visualizing-relationships-between-domains-ips-and-email-addresses-94377aa1f20a)
14. [Lucidchart](https://www.lucidchart.com/)
----

### Tools - Start.me-links
1. [Asint collection](https://start.me/p/b5Aow7/asint_collection)
2. [Bruno Mortier (DEU)](https://start.me/p/3g0aKK/sources)
3. [Country sorted OSINT links](https://start.me/p/W2kwBd/sources-cnty)
4. [Verification set for datingsites](https://start.me/p/VRxaj5/dating-apps-and-sites-for-investigators)
5. [Verification Toolset](https://start.me/p/ZGAzN7/verification-toolset)
----

### Tools - Hash
1. [Online Password Hash Crack](http://www.onlinehashcrack.com)
----

### Screenshot
1. [greenshot/greenshot](https://github.com/greenshot/greenshot)
2. [Snagit Einfache Bildschirmaufnahme und Videoaufzeichnung](https://www.techsmith.de/snagit.html)
3. [Shutter - Feature-rich Screenshot Tool (Linux)](http://shutter-project.org/)
4. [ShareX](https://getsharex.com/)
----

