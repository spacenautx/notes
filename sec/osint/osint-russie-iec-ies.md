### S'initier à l'OSINT (sélection)
1. [bellingcat](https://bellingcat.com)
2. [OpenFacto](https://openfacto.fr)
3. [Sector035](https://sector035.nl)
4. [Global Investigative Journalism Network](https://gijn.org)
5. [Boolean Strings](https://booleanstrings.com)
6. [IntelTechniques](https://inteltechniques.com)
7. [Osint Combine](https://osint.combine)
8. [We are OSINTCurio.us](https://osintcurio.us/)
9. [Secjuice](https://www.secjuice.com/)
----

### Presse et médias
1. [Роскомнадзор (agence gouv de contrôle)](https://rkn.gov.ru/)
2. [Blacklist sites web](https://reestr.rublacklist.net/)
3. [Classements des médias par popularité (différents supports)](https://www.mlg.ru/ratings/)
4. [Liste chaînes TV classées par détenteurs](http://www.tvenfrance.com/TVinWorld/TVRussia/tvRussia.html)
5. [Liste de radios russophones (Russie et au-delà)](https://en.wikipedia.org/wiki/List_of_Russian-language_radio_stations)
6. [Rapports/médias de masse étrangers qualifiés d'agents d'influence](http://unro.minjust.ru/InoSMIReports.aspx)
7. [Liste de titres presse écrite avec sensibilité politique](https://www.eurotopics.net/fr/176694/russie-des-medias-sous-contrle-de-letat#)
8. [Importante liste de titres de presse russes](https://www.newspapersglobal.com/EN/europe/Russia/russia.htm)
9. [Russia Today (chaîne d'influence internationale)](https://www.rt.com/)
10. [Sputnik International (média d'influence internationale)](https://sputniknews.com/)
11. [Carto médias/propriétaires](https://web.archive.org/web/20170620063054/https://russie.fr/wp-content/uploads/2011/12/Medias-business-russie.pdf)
----

### Loi et Justice
1. [Portail officiel d'informations juridiques](http://pravo.gov.ru/)
2. [Consultant: base de données législative (+ergonomique)](http://www.consultant.ru/)
3. [Portail officiel règlementation](https://regulation.gov.ru/)
4. [Portail vie judiciaire](https://sudrf.ru/)
5. [Portail Tribunaux de commerce](https://kad.arbitr.ru/)
6. [Base de données Procédures d'exécution (dettes)](https://fssp.gov.ru/)
7. [Inspections du Procureur](https://proverki.gov.ru/portal)
----

### Vie publique
1. [Gosulugi (portail web des services publics)](https://www.gosuslugi.ru/)
2. [Deklarator (base de données déclarations d'intérêts)](https://declarator.org/)
3. [Ministère de la Justice: infos sur ONGs, associations...](http://unro.minjust.ru/)
4. [Openngo.ru (base de données sur ONGs)](https://openngo.ru/)
5. [ClearSpending (base de données dépense publique)](https://clearspending.ru/)
----

### Personnes physiques
1. [Base de données des numéros de passeport invalides](http://xn--b1afk4ade4e.xn--b1ab2a0a.xn--b1aew.xn--p1ai/info-service.htm?sid=2000)
2. [Déterminer l'INN d'une personne physique](https://service.nalog.ru/static/personal-data.html?svc=inn&from=%2Finn.do)
3. [Base de données des INN invalides (personnes physiques)](https://service.nalog.ru/invalid-inn-fl.html)
4. [Pages blanches (cyrillique)](http://spra.vkaru.net/)
5. [Pages blanches (alphabet latin)](http://english.spravkaru.net/)
6. [Namechk (checker nom d'utilisateur)](https://namechk.com/)
7. [Search4faces (reconnaissance faciale sur Vk et Ok)](https://search4faces.com/)
8. [Findclone (reconnaissance faciale - sur inscription)](https://findclone.ru/)
----

### Réseaux sociaux
1. [VKontakte (Vk)](https://vk.com/)
2. [VKDIA (outil de recherche sur un profil)](https://vkdia.com/)
3. [Vk.watch (idem)](https://vk.watch/)
4. [220vk (trouve informations dissimulées sur un profil)](https://220vk.com/)
5. [Regvk.com (trouve date d'inscription)](https://regvk.com/)
6. [Oleg Nechiporenko (idem)](https://valery.shostak.ru/vk/nfwels)
7. [Vk.barkov.net (plusieurs outils de recherche)](https://vk.barkov.net/)
8. [Arborescence de publications d'un post](http://dcpu.ru/vk_repost_tree.php)
9. [Photo-map.ru (recherche images géolocalisées sur Vk)](http://photo-map.ru/)
10. [Odnoklassniki (Ok)](https://ok.ru/)
11. [Where-you.com (recherche profils sur Vk, Ok, Mail.ru)](https://where-you.com/)
12. [Kribrum.io (recherche posts par mots clés Vk, Ok...)](https://kribrum.io/)
13. [Moï Mir](https://my.mail.ru/)
14. [Habr Career (réseau professionnel dans l'informatique)](https://career.habr.com/)
----

### Telegram
1. [Prévisualiser une chaîne](https://xn--r1a.website/)
2. [Telegram Channels (recherche chaînes, groupes, bots)](https://telegramchannels.me/)
3. [Lenta.tg (recherche de publications)](https://lenta.tg/)
4. [Google CSE pour Telegram](https://intelx.io/tools?tab=telegram)
5. [Search.buzz.im (recherche de posts ouverts)](https://search.buzz.im/)
6. [Tgstat: recherche de chaînes et statistiques](https://tgstat.ru/en)
7. [Recherche groupes et chaînes (par pays/langue)](https://www.hottg.com/)
8. [Top Telegram Groups](https://combot.org/telegram/top/groups?lng=fr&page=1)
9. [Ru.tgchannels.org (recherche/monitoring de chaînes)](https://ru.tgchannels.org/)
10. [Lyzem (recherches chaînes, groupes, posts)](https://lyzem.com/)
11. [Telegramic.org (recherche et statistiques)](https://telegramic.org/)
----

### Autres plateformes populaires (sélection)
1. [Mail.ru (service courriel de référence)](https://mail.ru/)
2. [ICQ (messagerie instantanée)](https://www.icq.com/)
3. [Yandex: services annexes](https://yandex.ru/all)
4. [LiveJournal (plateforme de blogging)](https://www.livejournal.com/)
5. [Ozon.ru (marketplace de référence)](https://www.ozon.ru/)
6. [Rutube (plateforme de streaming vidéo)](https://rutube.ru/)
----

### Listes de forums
1. [Listes de forums sur Infoselection](https://infoselection.ru/obshchenie-i-znakomstva2/item/599-katalog-populyarnykh-forumov)
2. [Top 100 des forums par Rambler](https://top100.rambler.ru/navi?period=day&typeId=8&sort=popularity)
3. [Forumrate.ru](http://forumrate.ru/)
----

### Personnes morales
1. [Registre Unifié des personnes morales (EGRUL)](https://egrul.nalog.ru/index.html)
2. [Registre des PME](https://rmsp.nalog.ru/)
3. [Unro.minjust.ru (associations, ONGs...)](http://unro.minjust.ru/)
4. [Base données des INN invalides (personnes morales)](https://service.nalog.ru/invalid-inn-ul.html)
----

### Vie des affaires: services officiels
1. [Service officiel de vérification des partenaires](https://pb.nalog.ru/)
2. [Tribunaux de commerce (décisions, audience...)](https://kad.arbitr.ru/)
3. [Entreprises défaillantes](https://bankrot.fedresurs.ru/?attempt=1)
4. [Institutions financières](https://www.cbr.ru/)
5. [Procédures d'exécution (dettes)](https://fssp.gov.ru/)
6. [Commande publique](https://zakupki.gov.ru/epz/main/public/home.html)
7. [Cadastre](https://rosreestr.gov.ru/site/)
8. [Propriété intellectuelle (brevets...)](https://rospatent.gov.ru/ru)
9. [Bases de données brevets](https://www.fips.ru/iiss/)
10. [Inspections et contrôles](https://proverki.gov.ru/)
11. [Exploitations géologiques](https://www.rfgf.ru/)
12. [Communauté des Etats Indépendants](https://e-cis.info/)
13. [Chambre de Commerce et d'Industrie](https://tpprf.ru/ru/)
----

### Recherche sur entreprises/entités: services privés
1. [List-org (meilleur service gratuit recherche/entité)](https://www.list-org.com/)
2. [Zachestnyibiznes.ru (freemium)](https://zachestnyibiznes.ru/)
3. [Fedresurs.ru (vie juridique des entreprises)](https://fedresurs.ru/)
4. [Spark-interfax.ru (freemium)](https://www.spark-interfax.ru/)
5. [rusprofile (freemium)](https://www.rusprofile.ru/)
6. [Testfirm.ru (freemium, orienté analyse financière)](https://www.testfirm.ru/)
7. [Kontur (freemium)](https://focus.kontur.ru/)
8. [Aleph (orienté investigation crim. financière)](https://aleph.occrp.org/)
9. [Pages jaunes](https://www.yp.ru/)
----

### Journaux d'annonces légales
1. [Disclosure.skrin.ru](https://disclosure.skrin.ru/)
2. [Раскрытие Информации](https://disclosure.1prime.ru/)
3. [E-disclosure.ru](https://e-disclosure.ru/)
4. [DISCLOSURE.RU - Система раскрытия информации на рынке ценных бумаг (фондовый рынок, акции, инвестиционная компания, ценные бумаги, рынок ценных бумаг, финансы, оценка, финансовый анализ, инвестиции, вексель, биржа, кредит, корпоративные новости, промышленные предприятия, компании, банки, финансовые компании, государственные и муниципальные облигации)](https://disclosure.ru/index.shtml)
5. [Seldon.Basis  — поиск и проверка контрагентов](https://basis.myseldon.com/ru/landing)
6. [E-disclosure.azipi.ru](https://e-disclosure.azipi.ru/)
----

### Moteurs de recherche (généralistes)
1. [Yandex](https://yandex.ru)
2. [Yandex: indications pour recherche avancée](https://yandex.com/support/search/)
3. [Mail.ru](https://mail.ru/)
4. [Рамблер (Rambler)](https://www.rambler.ru/)
5. [Search Engine Colossus : Russia](https://www.searchenginecolossus.com/Russia.html)
----

### Transports
1. [Yandex.Maps (transports en commun, trafic routier...)](https://yandex.com/maps/)
2. [AIS Tracker (suivi trafic maritime/fluvial)](http://aistracker.ru/main.asp)
----

### Informations sur les véhicules
1. [Décoder une immatriculation russe](https://en.wikipedia.org/wiki/Vehicle_registration_plates_of_Russia)
2. [Décoder un VIN](https://en.wikipedia.org/wiki/Vehicle_identification_number)
3. [Avtocod.ru (informations sur un véhicule)](https://avtocod.ru/)
4. [Autoteka.ru (idem)](https://autoteka.ru/)
5. [Nomerogram.ru (recherche d'images par immatriculation)](https://www.nomerogram.ru/)
----

### Cartographies
1. [Cadastre (carte)](https://pkk.rosreestr.ru/#/search)
2. [Yandex maps](https://yandex.ru/maps/)
3. [Wikimapia](http://wikimapia.org/#lang=en&lat=0.000000&lon=0.000000&z=12&m=w)
4. [LiveUAMap](https://russia.liveuamap.com/)
5. [Photo-map.ru (recherche photos RS par localisation)](http://photo-map.ru/)
----

### Production intellectuelle et scientifique
1. [Base de données brevets (officiel)](https://www.fips.ru/iiss/)
2. [Yandex brevets (plus facile d'utilisation)](https://yandex.ru/patents)
3. [eLibrary (bibliothèque en ligne de référence)](https://elibrary.ru/defaultx.asp?)
4. [Socionet.ru (indexation pub scientifique)](https://socionet.ru/)
5. [Cyberleninka (base de données pub scientifique)](https://cyberleninka.ru/)
6. [Rusneb (bibliothèque en ligne)](https://rusneb.ru/)
7. [RSL (bibliothèque nationale)](https://www.rsl.ru/)
8. [Travaux de thèses depuis 1946](http://diss.rsl.ru/)
9. [Rusarchives.ru (portail des archives)](https://rusarchives.ru/)
----

### Adresses postales
1. [Carte administrative de la Russie](https://www.diplomatie.gouv.fr/IMG/pdf/russie_carte_administrative__cle8a182a.pdf)
2. [Russia Postcode ✉️](https://rus.postcodebase.com/fr)
----

### Outils linguistiques
1. [Translitt. norme ISO 9](https://www.translitteration.com/translitteration/fr/russe/iso-9/)
2. [Translit.ru (translittération+traduction)](https://translit.ru)
3. [Gate2Home (translittération+traduction avec clavier virtuel)](https://gate2home.com/Russian-Keyboard/Translate#lang=fr&t=)
4. [LEXILOGOS (translittération+traduction)](https://lexilogos.com)
5. [2cyr.com (translittération)](https://2cyr.com)
6. [Translate.ru (traducteur puissant)](https://www.translate.ru/)
7. [Google Traduction](https://translate.google.fr/?hl=fr)
8. [Yandex Traduction](https://translate.yandex.com/)
9. [Lingvo Live (dictionnaire réputé)](https://www.lingvolive.com/ru-ru)
10. [Gramota (dictionnaire)](http://gramota.ru/)
11. [Sokr.ru (dictionnaire d'abrévations, sigles...)](http://sokr.ru/)
12. [Ruski-Mat (dictionnaire argot et jargons)](http://www.russki-mat.net/home.php)
13. [Slovonovo.ru (dictionnaire argot et jargons)](https://www.slovonovo.ru/)
14. [Jargon.ru (dictionnaire jargons)](http://jargon.ru/)
----

### Outils techniques
1. [Concernant les numéros de téléphone (indicatifs...)](https://en.wikipedia.org//wiki/Telephone_numbers_in_Russia)
2. [Pinger un numéro de téléphone](https://smsc.ru/testhlr/?SE=97418fea)
3. [Numéro virtuel russe](https://onlinesim.ru/)
4. [WhoIs gérant les domaines en cyrillique](https://www.nic.ru/)
5. [DNS Dumpster pour la recherche de sous-domaines](https://dnsdumpster.com/)
6. [Spyse pour la recherche de sous-domaines](https://spyse.com/)
7. [Netcraft pour la recherche de sous-domaines](https://www.netcraft.com/)
8. [Décoder texte cyrillique encodé](https://2cyr.com/decode/)
9. [Liste noire Roskmnadzor](https://2ip.ru/rkn-blacklist/)
----

### Autres guides OSINT / Russie
1. [Recherches en ouvert sur l'internet russe (archive)](https://ia801200.us.archive.org/16/items/OpenSourceGuidebook/Open%20Source%20Guidebook.pdf)
2. [Enjeux culturels des groupes criminels russophones](https://www.ege.fr/sites/ege.fr/files/uploads/2019/05/Rapport-MRSIC-Criminalite-Russophone.pdf)
3. [Legal Guide for Foreign Investors in Russia 2019 | Herbert Smith Freehills | Global law firm](https://www.herbertsmithfreehills.com/latest-thinking/legal-guide-for-foreign-investors-in-russia-2019)
----

