### Case Studies
1. [AML Toolbox](https://start.me/p/rxeRqr/aml-toolbox?embed=1)
2. [Financial Crimes OSINT Tools: Companies – Travis Birch – Medium](https://medium.com/@travb/financial-crimes-osint-tools-companies-b9ebc4ca1ace)
3. [Financial Crimes OSINT Tools: Banking – Travis Birch – Medium](https://medium.com/@travb/financial-crimes-osint-tools-banking-5ede7edbc14f)
----

### Company Information
1. [Escavador](https://www.escavador.com/)
2. [The KYC Registry](https://www.swift.com/our-solutions/compliance-and-shared-services/financial-crime-compliance/our-kyc-solutions/the-kyc-registry#topic-tabs-menu)
3. [Databases (Worldwide) - Investigative Dashboard](https://investigativedashboard.org/databases/)
4. [Opengazettes.com](http://opengazettes.com/)
5. [OpenCorporates :: The Open Database Of The Corporate World](https://opencorporates.com/)
6. [Aleph data search](https://data.occrp.org/)
7. [Overseas registries](https://www.gov.uk/government/publications/overseas-registries/overseas-registries)
8. [GCRD International | Welcome to GCRD.](https://www.gcrd.info/index.php?id=3814)
9. [ICIJ Offshore Leaks Database](https://offshoreleaks.icij.org/)
10. [Companies House UK](https://www.gov.uk/get-information-about-a-company)
11. [Company Check Orbis](https://orbisdirectory.bvdinfo.com/version-20171213/OrbisDirectory/Companies)
12. [Company Check Zauba](https://www.zaubacorp.com/)
13. [Company Check](https://companycheck.co.uk/)
14. [Financial Conduct Authority](https://register.fca.org.uk/)
15. [The Investigator’s Handbook: A Guide to Using OpenCorporates](https://blog.opencorporates.com/2017/10/31/the-investigators-handbook-a-guide-to-using-opencorporates/)
16. [Victims of Offshore: A Video Introduction to the Panama Papers - ICIJ](https://www.icij.org/investigations/panama-papers/video/)
17. [Business Monitor](https://business-monitor.ch/en)
18. [https://e-justice.europa.eu/content_business_registers_in_member_states-106-en.do?init=true](https://e-justice.europa.eu/content_business_registers_in_member_states-106-en.do?init=true)
19. [aleph.occrp.org](https://aleph.occrp.org/)
----

### Sanctions Lists
1. [Sanctions List Search Tool](https://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/fuzzy_logic.aspx)
2. [Other OFAC Sanctions Lists](https://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/Other-OFAC-Sanctions-Lists.aspx)
3. [Financial sanctions targets: list of all asset freeze targets](https://www.gov.uk/government/publications/financial-sanctions-consolidated-list-of-targets/consolidated-list-of-targets)
4. [Consolidated list of persons, groups and entities subject to EU financial sanctions](https://data.europa.eu/euodp/data/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions)
5. [EU terrorist list - Consilium](http://www.consilium.europa.eu/en/policies/fight-against-terrorism/terrorist-list/)
6. [Free PEP Check](https://namescan.io/FreePEPCheck.aspx)
7. [The UK sanctions list - GOV.UK](https://www.gov.uk/government/publications/the-uk-sanctions-list)
----

### Trade Data
1. [Free access to detailed global trade data](https://comtrade.un.org/)
2. [World Bank Open Data](https://data.worldbank.org/)
3. [Container tracking](http://www.track-trace.com/container)
4. [Transport - Freight transport - OECD Data](https://data.oecd.org/transport/freight-transport.htm)
----

### Stolen Property
1. [Art and Culture crime database](http://news.culturecrime.org/all)
2. [Hotgunz (USA)](http://www.hotgunz.com/search.php)
3. [NMPR - The National Mobile Property Register](https://thenmpr.com/home)
----

### People Search
1. [PEP Checks and Sanction Screening](https://namescan.io/)
2. [Everypolitician.org](http://everypolitician.org/)
3. [CVgadget.com](http://www.cvgadget.com/)
4. [CV Searcher - Google Custom Search](https://cse.google.com/cse/publicurl?cx=009462381166450434430:0aq_5piun68)
5. [Certificated Bailiff Register – Justice UK](https://certificatedbailiffs.justice.gov.uk/)
6. [Spokeo](https://www.spokeo.com/)
7. [Find People for Free](https://thatsthem.com/)
8. [Identify the owner of any phone number!](http://www.peoplebyname.com/)
9. [Public Records Directory](https://publicrecords.directory/)
10. [No.1 Free People Search](http://www.yasni.com/)
11. [Free People Search Engine](https://www.zabasearch.com/)
12. [Peekyou](https://www.peekyou.com/)
13. [How Many of Me](http://howmanyofme.com/search/)
14. [Find A Grave](https://www.findagrave.com/)
----

### Credit Card Infomration
1. [Free BIN/IIN Lookup Web Service](https://binlist.net/)
2. [Validate, Verify & Check Credit Card or Debit Card Number](https://www.bincodes.com/creditcard-checker/)
3. [Pastebin](https://pastebin.com/)
4. [kahunalu/pwnbin](https://github.com/kahunalu/pwnbin)
----

### Bitcoin
1. [Bitcoin Block Explorer](https://blockchain.info/)
2. [OSINT-SPY | Search using OSINT](https://osint-spy.com/ip-logger)
3. [BTC (Bitcoin API)](https://osint-spy.com/btc)
4. [SharadKumar97/OSINT-SPY](https://github.com/SharadKumar97/OSINT-SPY)
5. [Follow the Bitcoin With Python, BlockExplorer and Webhose.io | Automating OSINT Blog](http://www.automatingosint.com/blog/2017/09/follow-the-bitcoin-with-python-blockexplorer-and-webhose-io/)
6. [Neutrino - Solutions for monitoring, analyzing and tracking cryptocurrency flows](https://neutrino.nu/)
7. [Elliptic | Preventing and detecting criminal activity in cryptocurrencies](https://www.elliptic.co/)
8. [Chainalysis](https://www.chainalysis.com/)
9. [Best Bitcoin Trading Bots &#8211; Top Cryptocurrency Earning Software?](https://bitcoinexchangeguide.com/best-bitcoin-trading-bots/)
10. [Bitcoin Address Lookup, Checker and Alerts](https://bitcoinwhoswho.com/)
11. [WalletExplorer.com: smart Bitcoin block explorer](https://www.walletexplorer.com/)
12. [Bitcoin Abuse Database](https://www.bitcoinabuse.com/)
13. [Blockchain](https://www.blockchain.com/)
----

### Automating (F)OSINT
1. [Justin Seitz's Automating OSINT](https://register.automatingosint.com/)
2. [Follow the Money with Python | Automating OSINT Blog](http://www.automatingosint.com/blog/2015/09/follow-the-money-with-python/)
3. [Follow the Bitcoin With Python, BlockExplorer and Webhose.io | Automating OSINT Blog](http://www.automatingosint.com/blog/2017/09/follow-the-bitcoin-with-python-blockexplorer-and-webhose-io/)
----

### Other
1. [Visual Tools for Investigators.](https://vis.occrp.org/)
2. [Islamism World Map](https://islamism-map.com/#/)
3. [West Africa Leaks - ICIJ](https://www.icij.org/investigations/west-africa-leaks/)
4. [Datawrapper](https://datawrapper.de)
----

### Country Profile
1. [BBC NEWS](http://news.bbc.co.uk/1/hi/country_profiles/default.stm)
2. [Country - Libya](https://data2.unhcr.org/en/country/lby)
3. [Refworld | Libya](https://www.refworld.org/country,,UNHCR,,LBY,,,,0.html)
4. [Country Anti Money Laundering Reports](https://www.knowyourcountry.com/copy-of-country-reports)
5. [The World Factbook](https://www.cia.gov/the-world-factbook/)
6. [Country Profiles | Amnesty International](https://www.amnesty.org/en/countries/)
7. [World Media Directory Maintained by EIN Presswire](https://www.einpresswire.com/world-media-directory/)
8. [Get World News by RSS - World News Report - EIN News](https://world.einnews.com/all_rss)
9. [Foreign travel advice](https://www.gov.uk/foreign-travel-advice)
10. [European Country of Origin Information Network](https://www.ecoi.net/)
11. [Country Profiles](https://www.ecoi.net/en/countries/congo/country-profiles/)
12. [Libya Immigration Detention Profile | Global Detention Project | Mapping immigration detention around the world](https://www.globaldetentionproject.org/countries/africa/libya)
13. [Passport Index](https://www.passportindex.org/?country=vc)
14. [Publicintelligence.net](https://publicintelligence.net/)
15. [International - U.S. Energy Information Administration (EIA)](https://www.eia.gov/international/overview/world?fips=wotc&trk=p3)
----

### Aviation Movements
1. [ADS-B Exchange – World's largest co-op of unfiltered flight data](https://www.adsbexchange.com/)
2. [radar.freedar.uk/VirtualRadar/desktop.html](https://radar.freedar.uk/VirtualRadar/desktop.html)
3. [Flightrader24](https://www.flightradar24.com/)
4. [Live Flight Tracking](https://uk.flightaware.com/live/)
5. [Listen to Live Air Traffic Control](https://www.liveatc.net/)
6. [GeoGuessr- The Top Tips, Tricks and Techniques | Random Stuff](https://web.archive.org/web/20210212090412/https://somerandomstuff1.wordpress.com/2019/02/08/geoguessr-the-top-tips-tricks-and-techniques/)
7. [Historical Flight Viewer](https://flight-data.adsbexchange.com/)
8. [GVA Dictator Alert](https://github.com/lefranz/geneva-dictators)
9. [Planespotters](https://www.planespotters.net/)
10. [Military Aircraft Photos/Search](http://www.airfighters.com/)
11. [LocalizaTodo HTML5](http://www.localizatodo.com/html5/)
12. [FlightConnections](https://www.flightconnections.com/)
13. [United States NATO Military Aircraft Tracker | Plane Flight Tracker](http://www.planeflighttracker.com/2014/04/united-states-military-aircraft-in.html)
----

### Maps & Satellites
1. [Resources for Finding and Using Satellite Images](https://gijn.org/resources-for-finding-and-using-satellite-images/)
2. [🗺 BBBike Map Compare 🛰](https://mc.bbbike.org/mc/#)
3. [Using World Imagery Wayback](https://www.esri.com/arcgis-blog/products/arcgis-living-atlas/mapping/using-world-imagery-wayback/)
4. [Google Maps](http://maps.google.com)
5. [Google Earth](http://google.com/earth)
6. [Bing Maps](https://www.bing.com/maps)
7. [Apple Maps](https://mapsconnect.apple.com)
8. [Instant Google Street View](https://www.instantstreetview.com/)
9. [DualMaps](http://www.dualmaps.com/)
10. [Animated Route Map by www.mapchannels.com](http://data.mapchannels.com/routemaps2/routemap200.htm?saddr=17706%20Seattle%20Ferry%20Terminal,%20Seattle,%20WA%2098104,%20USA&daddr=400%20Broad%20St,%20Seattle,%20WA%2098109,%20USA&maptype=2&units=2&z=15&fi=50&fs=1&refresh=3&timeout=300&draggable=0&sw=240&svc=0&svz=2&atw=160&fgc=000000&bgc=CCCCCC&rc=FF0000&rw=3&ro=0.7)
11. [OpenStreetMap](https://www.openstreetmap.org/#map=6/54.910/-3.432)
12. [Dashcam Map - Nextbase](https://www.nextbase.co.uk/video-map/)
13. [Midasearch - webcam viewers](https://midasearch.org/midasearch-webcam-viewers/)
14. [Mapillary](https://www.mapillary.com/)
15. [Maps.me](http://maps.me)
16. [Wikimapia](http://wikimapia.org)
17. [Openrailwaymap](https://www.openrailwaymap.org)
18. [Terra Server](https://www.terraserver.com/)
19. [GeoNames](http://www.geonames.org/)
20. [sentinel-hub/custom-scripts](https://github.com/sentinel-hub/custom-scripts)
21. [Shadowcalculator.eu](http://shadowcalculator.eu/#/lat/50.08/lng/19.9)
22. [Free GIS Data](https://freegisdata.rtwilson.com/)
23. [Zoom.earth](https://zoom.earth)
24. [Harris](http://www.mapmart.com/)
25. [Planet](https://www.planet.com/)
26. [Radiant Earth](https://www.radiant.earth/)
27. [Find an Address, Business, or Location](https://www.mapquest.com/search)
28. [OpenStreetMap](https://www.openstreetmap.org/#map=6/54.910/-3.432)
29. [Using World Imagery Wayback](https://www.esri.com/arcgis-blog/products/arcgis-living-atlas/mapping/using-world-imagery-wayback/)
30. [Geology](https://geology.com/)
31. [TRAVEL with DRONE](https://travelwithdrone.com/)
32. [Satellite map of the World : Google&trade; &mdash; share any place, address search; cities, countries, regions](https://satellites.pro/)
33. [Open Data Program | Maxar](https://www.maxar.com/open-data)
34. [Overpassturbu.eu](https://overpassturbu.eu)
35. [ebird.org/home](https://ebird.org/home)
36. [air-pollution-app](https://sandersson0013.users.earthengine.app/view/air-pollution-app)
37. [Identify.plantnet.org](https://identify.plantnet.org/)
38. [Satellite Viewer](https://maps.esri.com/rc/sat/index.html)
39. [Smapshot.heig-vd.ch](https://smapshot.heig-vd.ch/)
40. [EarthExplorer](https://earthexplorer.usgs.gov/)
----

### Geolocation support sources/tips/tricks
1. [https://web.archive.org/web/https://somerandomstuff1.wordpress.com/2019/02/08/geoguessr-the-top-tips-tricks-and-techniques/](https://web.archive.org/web/https://somerandomstuff1.wordpress.com/2019/02/08/geoguessr-the-top-tips-tricks-and-techniques/)
2. [EarthExplorer](https://earthexplorer.usgs.gov/)
3. [https://i.redd.it/lxtu47hf5ll51.jpg](https://i.redd.it/lxtu47hf5ll51.jpg)
4. [https://i.redd.it/16rncxw7w1j51.png](https://i.redd.it/16rncxw7w1j51.png)
5. [https://i.redd.it/s27wx8fxkyi51.jpg](https://i.redd.it/s27wx8fxkyi51.jpg)
6. [Symbols by Alphabetical order: C](https://www.symbols.com/symbols/C)
7. [backgroundLayer 1](https://upload.wikimedia.org/wikipedia/commons/2/29/World_Speed_Limits.svg)
8. [Brands of the World™](https://www.brandsoftheworld.com/)
9. [FindMyShadow.com](https://www.findmyshadow.com/)
10. [Shadowcalculator.eu](http://shadowcalculator.eu/#/lat/50.08/lng/19.9)
11. [SunCalc](http://suncalc.net/#/51.508,-0.125,2/2021.02.18/11:59)
12. [| AllTrails](https://www.alltrails.com/explore?b_tl_lat=23.168858546478376&b_tl_lng=57.515607706268355&b_br_lat=23.09718300821517&b_br_lng=57.634727364855735)
13. [Bikku Bitti](https://peakvisor.com/peak/bikku-bitti-5n70eyss.html?yaw=-141.37&pitch=-7.49&hfov=60.00)
14. [GeoTips – Tips and tricks for Geoguessr](https://geotips.net/)
15. [How Nature May Reveal Secrets in Geolocation | by wondersmith_rae | Medium](https://wondersmithrae.medium.com/how-nature-may-reveal-secrets-in-geolocation-e34dc9f42ed)
----

### Drones & webcams databases
1. [TRAVEL with DRONE](https://travelwithdrone.com/)
2. [Dronestagram](http://www.dronestagr.am/)
3. [Soar.earth](https://soar.earth/)
4. [Insecam](http://www.insecam.org/)
5. [Webcams from around the World](https://worldcam.eu/)
6. [SkylineWebcams](https://www.skylinewebcams.com/)
7. [Datasets - senseFlyDatasets - senseFlyLinkedinTwitterFacebookYouTubeNewLinkedinTwitterFacebookYouTubeNewic-search-03IcnCircleCloseIcnCircleCloseLinkedinTwitterFacebookYouTubeNewSketchfabSlideshare](https://www.sensefly.com/education/datasets/)
8. [Aerialphotosearch.com](https://www.aerialphotosearch.com/)
9. [Aktuelle Luftbilder, Luftaufnahmen aus Deutschland & Europa](https://www.euroluftbild.de/)
----

### Maritime Movements
1. [Marine Traffic](https://www.marinetraffic.com/)
2. [ShipmentLink](https://www.shipmentlink.com/servlet/TDB1_CargoTracking.do)
3. [Chinese Navy Surface Units – Current and Retired – The Searchers](https://warsearcher.com/shipsearcher/chinese-navy-surface-units-current-and-retired-under-construction/)
4. [Vessel Tracker](https://www.vesseltracker.com/)
5. [Vessel Finder](https://www.vesselfinder.com/)
6. [Shipfinder](http://www.shipfinder.com/)
7. [Shipfinder](http://shipfinder.co/)
8. [Ship Spotting](http://www.shipspotting.com/)
9. [ShipAIS](http://www.shipais.com/)
10. [Ais Decoder](http://arundale.com/docs/ais/ais_decoder.html)
11. [TankerTrackers](http://tankertrackers.com/#/)
12. [LocalizaTodo HTML5](http://www.localizatodo.com/html5/)
13. [Fleetmon](https://www.fleetmon.com/services/live-tracking/fleetmon-explorer/)
14. [Equasis](http://www.equasis.org/EquasisWeb/public/HomePage)
15. [Crewdata.com](https://crewdata.com)
16. [Jobs At Sea](http://maritime-connector.com/)
17. [Military Ship Tracker](https://www.marinevesseltraffic.com/2013/02/military-ship-track.html)
----

### Train Movements
1. [Raildar](https://raildar.co.uk/radar.html)
----

### Other Resources
1. [Resources for Finding and Using Satellite Images - Global Investigative Journalism Network](https://gijn.org/resources-for-finding-and-using-satellite-images/)
----

### Training & Tutorials
1. [LibGuides: Intelligence Studies: Open-Source Intelligence (OSINT)](https://usnwc.libguides.com/c.php?g=494120&p=3420732)
2. [OSINT: How to find information on anyone](https://medium.com/@Peter_UXer/osint-how-to-find-information-on-anyone-5029a3c7fd56)
3. [Magma.lavafeld.org](https://magma.lavafeld.org/)
4. [Learn – Google Earth OutreachGroupfacebooktwitter](https://www.google.com/earth/outreach/learn/)
5. [Investigations KIT](https://kit.exposingtheinvisible.org/)
6. [BIRD - BIRN Investigative Resource Desk](https://bird.tools/)
7. [Bird.tools](https://bird.tools/)
8. [Security Education Companion](https://sec.eff.org/)
9. [Detecting Fake Viral Stories Before They Become Viral Using FB API - By Florin Badita](https://hackernoon.com/detecting-fake-viral-stories-before-they-become-viral-using-fb-api-f67a18cd4f0a?source=rss)
10. [Citizen Investigation Guide](https://gijn.org/citizen-investigation-guide/)
11. [Intro to I2P](https://medium.com/@mhatta/have-fun-with-i2p-the-2018-edition-6ed61af3ad79)
12. [We are OSINTCurio.us](https://osintcurio.us)
13. [OSINT Handbook 2018](https://www.i-intelligence.eu/wp-content/uploads/2018/06/OSINT_Handbook_June-2018_Final.pdf)
14. [Exposing the Invisible](https://exposingtheinvisible.org/)
15. [CaseFile – Visualization of Information](https://corma.de/en/casefile-visualization-of-information-investigation/)
16. [Verification Handbook: homepage](https://verificationhandbook.com/)
17. [comp3321 NSA Python Training Document : NSA : Free Download, Borrow, and Streaming : Internet Archive](https://archive.org/details/comp3321/page/n5/mode/2up/search/device)
18. [Research Clinic](http://www.researchclinic.net/)
19. [CNS New Tools Courseware Portfolio – Learn. Investigate. Discover.](https://learn-new-tools.org/)
20. [Dark Web Tools](https://iaca-darkweb-tools.com/)
21. [GIJN - Toolbox](https://gijn.org/2018/11/07/gijn-toolbox/)
22. [European Country of Origin Information Network](https://www.ecoi.net/en/coi-resources/training/)
23. [Coronavirus: Resources for reporters - First DraftCoronavirus: Resources for reporters - First Draft](https://firstdraftnews.org/long-form-article/coronavirus-resources-for-reporters/)
24. [VIS](https://vis.occrp.org/)
25. [Kumu](https://kumu.io/)
26. [bellingcat - A Beginner's Guide To Flight Tracking - bellingcat](https://www.bellingcat.com/resources/how-tos/2019/10/15/a-beginners-guide-to-flight-tracking/)
27. [bellingcat - How To Tell Stories: A Beginner's Guide For Open Source Researchers - bellingcat](https://www.bellingcat.com/resources/2019/07/12/how-to-tell-stories-a-beginners-guide-for-open-source-researchers/)
28. [Open-Source Intelligence (OSINT) - Intelligence Studies - LibGuides at Naval War College](https://usnwc.libguides.com/c.php?g=494120&p=3420732)
29. [The Most Comprehensive TweetDeck Research Guide In Existence (Probably)](https://www.bellingcat.com/resources/how-tos/2019/06/21/the-most-comprehensive-tweetdeck-research-guide-in-existence-probably/)
30. [https://firstdraftnews.org/wp-content/uploads/2019/10/Verifying_Online_Information_Digital_AW.pdf](https://firstdraftnews.org/wp-content/uploads/2019/10/Verifying_Online_Information_Digital_AW.pdf)
31. [Getting Started With Spiderfoot – A Beginner’s Guide – NixIntel](https://nixintel.info/osint-tools/getting-started-with-spiderfoot/)
32. [OSINT on the Ocean: Maritime Intelligence Gathering Techniques – Rae BakerInvite to discussScroll Top](https://www.peerlyst.com/posts/osint-on-the-ocean-maritime-intelligence-gathering-techniques-rae-baker)
33. [https://corporatewatch.org/wp-content/uploads/2017/09/Investigating-Companies-Corporate-Watch.pdf](https://corporatewatch.org/wp-content/uploads/2017/09/Investigating-Companies-Corporate-Watch.pdf)
34. [https://www.journaliststoolbox.org/2020/01/22/online_journalism/](https://www.journaliststoolbox.org/2020/01/22/online_journalism/)
35. [My Favorite Tools: Lionel Faull - Global Investigative Journalism Network](https://gijn.org/2020/01/21/my-favorite-tools-lionel-faull/)
36. [Data Journalism : / Help Desk](https://helpdesk.gijn.org/support/solutions/articles/14000036505-data-journalism)
37. [Education | Sentinel Hub](https://www.sentinel-hub.com/explore/education)
38. [Draw.io](https://www.draw.io/)
39. [The Data Visualisation Catalogue](https://datavizcatalogue.com/)
40. [Learn – Google Earth OutreachGroupfacebooktwitter](https://www.google.com/earth/outreach/learn/)
41. [Learn – Google Earth OutreachGroupfacebooktwitter](https://www.google.com/earth/outreach/learn/)
42. [Osinteditor.com](http://www.osinteditor.com/)
43. [Violations de l’embargo sur les armes en Libye – Guide méthodologique | OpenFacto](https://t.co/0NG2oyTpAw?amp=1)
44. [Problem-Solving Techniques](https://www.mindtools.com/pages/main/newMN_TMC.htm)
45. [Country & Area Assessments](https://www.osintcombine.com/post/country-area-assessments)
46. [OSINT Collection Schema](https://www.osintcombine.com/post/osint-collection-schema)
47. [Corporate Profiling](https://www.osintcombine.com/post/corporate-profiling-advanced-linkedin-searching-more)
48. [Naming conventions internationally](https://www.fbiic.gov/public/2008/nov/Naming_practice_guide_UK_2006.pdf)
49. [Walkthrough: Tracking the spread of the debunked Plandemic video](https://dfrlab.github.io/Plandemic/)
50. [https://www.social-engineer.org/framework/general-discussion/](https://www.social-engineer.org/framework/general-discussion/)
51. [https://medium.com/@zacherytysonbrown/write-like-an-intelligence-analyst-34d06738d2ef](https://medium.com/@zacherytysonbrown/write-like-an-intelligence-analyst-34d06738d2ef)
52. [Intelligence Writing - Writing - LibGuides at Embry-Riddle Aeronautical University - Prescott Campus](https://erau.libguides.com/c.php?g=331035&p=2222278)
53. [Sources And Methods](https://sourcesandmethods.blogspot.com/)
54. [Tutorial  - Maltego](https://www.maltego.com/categories/tutorial/)
55. [US 2020 Dashboard: Live insights on information disorderUS 2020 Dashboard: Live insights on information disorder](https://firstdraftnews.org/dashboard-insights/)
56. [Military Wiki](https://military.wikia.org/wiki/Main_Page)
57. [Intelligence Online](https://www.intelligenceonline.com)
58. [OSINT Iran: passive reconnaissance of Iranian IP space, IoT, WiFi networks, social media and more – osintme.com](https://web.archive.org/web/20210206085713/https://www.osintme.com/index.php/2020/12/23/osint-iran-passive-reconnaissance-of-iranian-ip-space-iot-wifi-networks-social-media-and-more/)
59. [Overview of Corporate Research Articles - OSINTEditor](https://www.osinteditor.com/general/overview-of-corporate-research-articles/)
60. [bird.tools/wp-content/uploads/2019/12/13082019_Osint-guide_v2fin.1.pdf](https://bird.tools/wp-content/uploads/2019/12/13082019_Osint-guide_v2fin.1.pdf)
61. [GeoGuessr- The Top Tips, Tricks and Techniques | Random Stuff](https://web.archive.org/web/20210212090412/https://somerandomstuff1.wordpress.com/2019/02/08/geoguessr-the-top-tips-tricks-and-techniques/)
62. [https://github.com/matteo-grella/tweetdeck-scraper](https://github.com/matteo-grella/tweetdeck-scraper)
63. [Sponsor @anvaka on GitHub Sponsors · GitHub](https://github.com/sponsors/anvaka)
64. [www.rand.org/content/dam/rand/pubs/research_reports/RR1400/RR1408/RAND_RR1408.pdf](https://www.rand.org/content/dam/rand/pubs/research_reports/RR1400/RR1408/RAND_RR1408.pdf)
65. [www.cia.gov/static/9a5f1162fd0932c29bfed1c030edf4ae/Pyschology-of-Intelligence-Analysis.pdf](https://www.cia.gov/static/9a5f1162fd0932c29bfed1c030edf4ae/Pyschology-of-Intelligence-Analysis.pdf)
66. [D3Js](https://d3js.org/)
67. [Critical Thinking](https://www.criticalthinking.org/)
68. [Google's Python Class  |  Python Education
       |  Google Developers](https://developers.google.com/edu/python/)
69. [Statistics and Probability](https://www.khanacademy.org/math/statistics-probability)
70. [Static and dynamic network visualization with R](https://kateto.net/network-visualization)
71. [DataJournalism.com](https://datajournalism.com/)
----

### Mis- & Disinformation
1. [Manipulated Media](https://www.reuters.com/manipulatedmedia)
2. [Audio Fakes](https://motherboard.vice.com/en_us/article/3k7mgn/baidu-deep-voice-software-can-clone-anyones-voice-with-just-37-seconds-of-audio)
3. [Magma.lavafeld.org](https://magma.lavafeld.org/)
4. [Deepfakes Club](https://www.deepfakes.club/)
5. [GitHub - DFRLab/Dichotomies-of-Disinformation](https://github.com/DFRLab/Dichotomies-of-Disinformation)
6. [Project Voco](https://www.bbc.co.uk/news/technology-37899902)
7. [Data Visualizer | Jigsaw](http://jigsaw.google.com/the-current/disinformation/dataviz/)
8. [Fake news. It's complicated.Fake news. It's complicated.](https://firstdraftnews.org/latest/fake-news-complicated/)
9. [OSoMe: Social Media Observatory](https://osome.iuni.iu.edu/tools/)
10. [Hamilton Monitored Accounts on Twitter – Alliance For Securing Democracy](https://securingdemocracy.gmfus.org/hamilton-monitored-accounts-on-twitter/)
11. [Hamilton 2.0 Dashboard](https://securingdemocracy.gmfus.org/hamilton-dashboard/)
12. [FSI - New White Paper on GRU Online Operations Puts Spotlight on Pseudo-Think Tanks and Personas](https://fsi.stanford.edu/news/potemkin-pages-personas-blog)
13. [Open-Source Intelligence (OSINT) - Intelligence Studies - LibGuides at Naval War College](https://usnwc.libguides.com/c.php?g=494120&p=3420732)
14. [Walkthrough: Tracking the spread of the debunked Plandemic video](https://dfrlab.github.io/Plandemic/)
15. [https://thisxdoesnotexist.com/](https://thisxdoesnotexist.com/)
16. [GeoGuessr- The Top Tips, Tricks and Techniques](https://somerandomstuff1.wordpress.com/2019/02/08/geoguessr-the-top-tips-tricks-and-techniques/)
17. [What's Being Done to Fight Disinformation Online](https://www.rand.org/research/projects/truth-decay/fighting-disinformation.html)
18. [Tools That Fight Disinformation Online](https://www.rand.org/research/projects/truth-decay/fighting-disinformation/search.html)
19. [Newsguardtech.com](https://www.newsguardtech.com/)
20. [https://iffy.news/iffy-plus/](https://iffy.news/iffy-plus/)
----

### Digital Crypto Currencies
1. [Blockchain](https://blockchain.com)
2. [Blockpath.com](https://blockpath.com)
----

### Safeguarding Anonymity & Privacy
1. [Is your VPN secure?](http://theconversation.com/is-your-vpn-secure-109130?utm_source=twitter&utm_medium=twitterbutton)
2. [Digital Security Resources](https://medium.com/@mshelton/current-digital-security-resources-5c88ba40ce5c)
3. [Basic Opsec](https://medium.com/@JMartinMcFly/basic-opsec-for-the-uninitiated-77d0839f7bce)
4. [Browser Uniqueness & Fingerprinting](https://medium.com/@ravielakshmanan/web-browser-uniqueness-and-fingerprinting-7eac3c381805)
5. [Web Tracking: What You Should Know About Your Privacy Online](https://medium.freecodecamp.org/what-you-should-know-about-web-tracking-and-how-it-affects-your-online-privacy-42935355525)
6. [Am I unique?](https://amiunique.org/)
7. [Privacy Tools](https://amiunique.org/tools)
8. [BrowserLeaks.com](https://browserleaks.com/)
9. [Online Guide to Privacy](https://epic.org/privacy/tools.html)
10. [What is a VPN - NORD VPN](https://nordvpn.com/what-is-a-vpn/)
11. [Identity Protection - CIFAS](https://www.cifas.org.uk/services/identity-protection)
12. [25 Best Chrome Extensions For Penetration Testing Can Keep You Out of Trouble](http://techblogcorner.com/2017/10/27/best-chrome-extensions-for-penetration-testing/)
13. [Why Do Some Websites Block VPNs?](https://www.howtogeek.com/403771/why-do-some-websites-block-vpns/)
14. [Watch Your Hack](https://watchyourhack.com/)
15. [Security Planner](https://securityplanner.org/#/)
16. [How Cartographers for the U.S. Military Inadvertently Created a House of Horrors in South Africa](https://gizmodo.com/how-cartographers-for-the-u-s-military-inadvertently-c-1830758394)
17. [Surveillance Self-Defense](https://ssd.eff.org/)
18. [BrowserSpy.dk](http://browserspy.dk/)
19. [Cover Your Tracks](https://coveryourtracks.eff.org/)
20. [Electronic Frontier Foundation](http://eff.org/)
21. [AmIUnique](https://amiunique.org/)
22. [go-incognito/sources.md at master · techlore-official/go-incognito · GitHub](https://github.com/techlore-official/go-incognito/blob/master/sources.md#section-3)
----

### Delete Online Footprints
1. [https://searchenginesindex.com/](https://searchenginesindex.com/)
2. [Quickly Delete Years of Facebook Posts With This Chrome Extension](https://lifehacker.com/quickly-delete-years-of-facebook-posts-with-this-chrome-1828148008)
3. [How to delete old tweets before they come back to haunt you.](https://www.nbcnews.com/news/amp/ncna896546)
4. [Tweetdeleter – Delete tweets with a few clicks | Premium](https://www.tweetdeleter.com/en/offers)
5. [TweetEraser - Delete All Your Tweets And Favorites](https://www.tweeteraser.com/)
6. [Automatically delete your old tweets with TweetDelete.net](https://www.tweetdelete.net/)
7. [Social Book Post Manager](https://chrome.google.com/webstore/detail/social-book-post-manager/ljfidlkcmdmmibngdfikhffffdmphjae)
8. [How to Delete Your Old Tweets So Your Enemies Can&#39;t Get to Them](https://lifehacker.com/how-to-delete-your-old-tweets-and-favs-before-your-enem-1821062277)
----

### Directories
1. [2020 Directory of Directories – LLRX](https://www.llrx.com/2020/06/2020-directory-of-directories/)
----

### Search Engines (General)
1. [Searchenginesindex.com](https://searchenginesindex.com/)
2. [The Search Engine Map](https://www.searchenginemap.com/)
3. [Google Factcheck Explorer](https://toolbox.google.com/factcheck/explorer)
4. [Interactive online Google tutorial and references](http://www.googleguide.com/)
5. [I Search From](http://isearchfrom.com/)
6. [Google.com](https://google.com)
7. [The Importance of Excluding Words When Setting Up Google Alerts](https://researchbuzz.me/2018/02/28/the-importance-of-excluding-words-when-setting-up-google-alerts/)
8. [DuckDuckGo](http://duckduckgo.com)
9. [Wolfram|Alpha: Making the world’s knowledge computable](https://www.wolframalpha.com/)
10. [Baidu (China)](http://www.baidu.com/)
11. [Bing](https://bing.start.me/?a=gsb_startme_00_00_ssg01)
12. [Bing vs. Google](http://bvsg.org)
13. [Dogpile Web Search](http://www.dogpile.com/)
14. [Dataset Search](https://toolbox.google.com/datasetsearch)
15. [eTools.ch - multiple search engines at once](http://www.etools.ch/)
16. [Image Identify Project](https://www.imageidentify.com/)
17. [Internet Archive: Digital Library of Free & Borrowable Books, Movies, Music & Wayback Machine](https://archive.org/)
18. [Qwant](http://qwant.com)
19. [Yamii (Arabic)](https://www.yamli.com/arabic-keyboard/)
20. [Yandex (RU)](http://yandex.ru)
21. [Yippy.com](http://www.yippy.com/)
22. [Internet Map](https://internet-map.net/)
23. [Snapito](https://snapito.com/)
24. [web-capture.net](https://web-capture.net/)
25. [carrot2](https://search.carrot2.org/#/search/web)
26. [NAPALM FTP Indexer](https://www.searchftps.net/)
27. [Unpaywall](https://unpaywall.org/)
28. [MetaGer](https://metager.de/)
----

### Academic Search Engines
1. [scholar.google.co](https://scholar.google.com)
2. [Dimensions](https://app.dimensions.ai/discover/publication)
3. [Talk to Books](https://books.google.com/talktobooks/)
4. [Text Analyzer Beta, from JSTOR Labs](https://www.jstor.org/analyze/)
5. [CORE](https://core.ac.uk/)
6. [The Library of Congress](https://www.loc.gov/)
7. [Open Knowledge Maps](https://openknowledgemaps.org/)
8. [BASE (Bielefeld Academic Search Engine): Basic Search](https://www.base-search.net/)
9. [Unpaywall](https://unpaywall.org/)
----

### Dark Web Search Engines
1. [Daily Dark Web Monitoring Reports from Hunchly](https://darkweb.hunch.ly/)
2. [Ahmia.fi](https://ahmia.fi/)
3. [Torscan](http://www.torscan.io/)
4. [Onion.link](http://onion.link/faq.html)
5. [http:// http://tor66sezptuu2nta[.]onion.com](http:// http://tor66sezptuu2nta[.]onion.com)
6. [best practice for dark web investigations](https://www.justice.gov/criminal-ccips/page/file/1252341/download)
7. [LuckP47 Casual Dark Web Investigation Livestream - YouTube](https://www.youtube.com/watch?v=YvV947T-F90)
----

### List of darkweb places
1. [misterch0c/CrimeBoards](https://github.com/misterch0c/CrimeBoards)
2. [List of darknet markets for investigators #2 – osintme.com](https://www.osintme.com/index.php/2021/02/20/list-of-darknet-markets-for-investigators-2/)
3. [Crowdstrike Adversary Universe](https://adversary.crowdstrike.com/)
----

### IoT - Search Engines
1. [Shodan.com](https://shodan.io)
2. [Thingful](https://www.thingful.net/)
3. [Worldcam](https://worldcam.eu/)
4. [Earthcam](http://www.earthcam.com/)
5. [Openstreetcam](https://www.openstreetcam.org/map/)
6. [Webcams.travel](https://www.webcams.travel/)
7. [Airport webcams](http://airportwebcams.net/)
8. [Insecam](http://www.insecam.org/)
9. [Opentopia](http://www.opentopia.com/)
10. [The webcam network](http://www.the-webcam-network.com/)
----

### Tracing arms & ammunition
1. [Weapons of ISIS](http://www.conflictarm.com/reports/weapons-of-the-islamic-state/)
2. [SIPRI Arms Transfers Database](https://www.sipri.org/databases/armstransfers)
3. [A-Z Databases](http://www.natolibguides.info/az.php)
4. [Major Arms Sales | The Official Home of the Defense Security Cooperation Agency](https://www.dsca.mil/major-arms-sales)
5. [The Weapons ID Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
6. [Search 3d models - sketchfab](https://sketchfab.com/search?sort_by=-pertinence&type=models)
7. [Arms embargoes](https://www.sipri.org/databases/embargoes)
8. [Identification and Support Cards](http://www.smallarmssurvey.org/weapons-and-markets/tools/id-cards.html)
9. [Ammunition-Tracing-Kit.pdf](http://www.smallarmssurvey.org/fileadmin/docs/D-Book-series/book-06-ATK/SAS-Ammunition-Tracing-Kit.pdf)
10. [Loading Mapping Arms Data (MAD) 1992-2014. Please wait as application loads.
                    Note: The graphics used in this visualization are not supported by older versions of 			Internet Explorer. It is best viewed using an up-to-date version of Chrome, Firefox or Internet Explorer (IE11).
		N.B. If you are using Internet Explorer, this must NOT be set to run in 'Compatibility Mode'! (Tools > Compatibility View Settings)](http://nisatapps.prio.org/armsglobe/index.php)
11. [Global Firearms Holdings](http://www.smallarmssurvey.org/weapons-and-markets/tools/global-firearms-holdings.html)
12. [UEMS tools](http://www.smallarmssurvey.org/weapons-and-markets/stockpiles/unplanned-explosions-at-munitions-sites/uems-tools.html)
13. [HotGunz Stolen Gun Search Results](https://www.hotgunz.com/search.php)
14. [ArcGIS Dashboards](https://itrace.maps.arcgis.com/apps/opsdashboard/index.html#/71e05fa765964469a7f02d010d59a247)
----

### Identifying combatant affiliatio
1. [How to Digitally Verify Combatant Affiliation in Middle East Conflicts - bellingcat](https://www.bellingcat.com/resources/how-tos/2018/07/09/digitally-verify-middle-east-conflicts/)
2. [bellingcat - Syrian Opposition Factions in the Syrian Civil War - bellingcat](https://www.bellingcat.com/news/mena/2016/08/13/syrian-opposition-factions-in-the-syrian-civil-war/)
3. [Camopedia](http://camopedia.org/index.php?title=Main_Page)
4. [Camouflageindex.camouflagesociety.org/index-2.html](http://camouflageindex.camouflagesociety.org/index-2.html)
5. [Uniforminsignia.org](http://www.uniforminsignia.org/)
6. [List of comparative military ranks](https://en.wikipedia.org/wiki/List_of_comparative_military_ranks)
7. [All Badges site for collectors](http://allbadges.net/en)
----

### Intelligence related sites
1. [Snowden Doc Search](https://search.edwardsnowden.com/)
2. [Projects | Transparency Toolkit](https://transparencytoolkit.org/projects/)
3. [https://theintercept.com/snowden-sidtoday/?orderBy=publishedTime&orderDirection=desc#archive](https://theintercept.com/snowden-sidtoday/?orderBy=publishedTime&orderDirection=desc#archive)
----

### Translation/Language
1. [Learn Languages](http://mylanguages.org/)
2. [Translate Languages in Spreadsheet](https://lifehacker.com/how-to-use-google-sheets-to-translate-languages-for-you-1833717291)
3. [Lexilogos](https://www.lexilogos.com/)
4. [Cyrcillic decoder](https://2cyr.com/decode/?lang=en)
5. [Google Translate](http://translate.google.com)
6. [Know Your Meme](http://knowyourmeme.com/)
7. [TagDef](https://tagdef.com)
8. [Urban Dictionairy](https://www.urbandictionary.com/)
9. [Yamii](https://www.yamli.com/arabic-keyboard/)
10. [DeepL](https://www.deepl.com/translator)
11. [Voyant Tools](https://voyant-tools.org/)
----

### Monitoring News/Conflicts/Web
1. [Liveuamap.com](https://liveuamap.com)
2. [NewsBrief](http://emm.newsbrief.eu/NewsBrief/clusteredition/en/latest.html)
3. [Google Alerts](https://www.google.co.uk/alerts)
4. [Google Alerts alternative. The best and free alerts service with Twitter results](https://www.talkwalker.com/alerts)
5. [IBM News Explorer](http://news-explorer.mybluemix.net/?query=IRAQ&type=unconstrained)
6. [IRIS](https://iris.wcoomd.org/?locale=en)
7. [Live Piracy Map](https://www.icc-ccs.org/piracy-reporting-centre/live-piracy-map)
8. [NewsNow: World News news](https://www.newsnow.co.uk/h/World+News)
9. [Airwars Homepage (Redesign)](https://airwars.org/)
----

### Domain Searches
1. [Technisette - After the GDPR WHOIS Search](https://osintcurio.us/2019/01/08/after-the-gdpr-researching-domain-name-registrations/)
2. [SynapsInt](https://synapsint.com/)
3. [Whois Lookup, Domain Availability & IP Search](https://whois.domaintools.com/)
4. [DomainBigData.com](https://domainbigdata.com/)
5. [WhoWas](https://www.apnic.net/static/whowas-ui/)
6. [Free online network tools](https://centralops.net/co/)
7. [ViewDNS.info](https://viewdns.info/)
8. [Analyze ID](http://analyzeid.com/)
9. [Intelligence X](https://intelx.io/)
10. [Whois Lookup & IP](https://www.whois.net/)
11. [Instant Domain Search](https://instantdomainsearch.com/)
12. [DataSploit (Advanced)](https://github.com/DataSploit/datasploit)
13. [Whois API](https://www.whoxy.com/)
14. [Netbootcamp - Overview](https://netbootcamp.org/whoishistory/)
15. [Cyber Threat Intelligence](https://pulsedive.com/)
16. [https://www.apnic.net/static/whowas-ui/](https://www.apnic.net/static/whowas-ui/)
17. [https://www.domaintools.com/products/iris/](https://www.domaintools.com/products/iris/)
18. [Search Engine for Source Code](https://publicwww.com/)
19. [NerdyData.com](https://nerdydata.com/)
20. [Hexillion: Whois API and Internet tools for cybersecurity investigations, troubleshooting, competitive intelligence](https://hexillion.com/)
21. [Host.io](https://host.io/)
22. [Spyse.com](https://spyse.com/)
----

### Facial Recognition Search Engines
1. [Baidu Image Search](https://image.baidu.com/)
2. [Betaface | Advanced face recognition](https://www.betaface.com/wpa/)
3. [Face detection & recognition](http://facedetection.kuznech.com/)
4. [Google Images](https://www.google.com/imghp)
5. [Innovative graphic search engine Picwiser - photos fast matching, image matching, image recognition, photos, picture, search engine](http://picwiser.com/)
6. [pictriev, Gesichts-Suchmaschine](http://www.pictriev.com/)
7. [Twins or Not?](http://www.twinsornot.com/)
8. [Yandex Image Search](https://yandex.com/images/)
----

### Email Search/Checker
1. [Online Email Validator & Email Verification Services | Byteplant](https://www.email-validator.net/)
2. [Email Format](https://email-format.com/)
3. [Hunter](https://hunter.io/)
4. [Email Permutator](http://metricsparrow.com/toolkit/email-permutator/)
5. [Have I Been Pwned: Check if your email has been compromised in a data breach](https://haveibeenpwned.com/)
----

### Phone numbers
1. [Receive SMS (free)](http://receive-sms-online.com/)
2. [Receive-SMS (free)](https://receive-sms.com/)
3. [Receive SMS (free)](http://sms.sellaite.com/)
4. [Receive SMS Online](https://www.receivesmsonline.net/)
----

### Phone Number Lookup
1. [FREE to Lookup Unknown Callers - ReversePhoneLookup.com](http://www.reversephonelookup.com/)
2. [Free Reverse Phone Number Lookup](https://www.spydialer.com/default.aspx)
3. [Reverse Phone Lookup | Cell Phone Search | BeenVerified](https://www.beenverified.com/reverse-phone/)
4. [Who is calling you? Do a confidential reverse phone lookup to find out!](https://www.intelius.com/reverse-phone-lookup)
5. [Reverse Phone Lookup | Cell Phone Search | BeenVerified](https://www.beenverified.com/reverse-phone/)
----

### People Search
1. [Pipl](https://pipl.com/)
2. [PeekYou.com](https://www.peekyou.com/)
3. [People search - Find people over the internet for free - White Pages](http://www.ppfinder.com/)
4. [Everypolitician.org](http://everypolitician.org/)
5. [CVgadget.com](http://www.cvgadget.com/)
6. [CV Searcher - Google Custom Search](https://cse.google.com/cse/publicurl?cx=009462381166450434430:0aq_5piun68)
7. [PEP Checks and Sanction Screening](https://namescan.io/)
8. [Free Family History and Genealogy Records — FamilySearch.org](https://www.familysearch.org/)
9. [Find People, Phone Numbers, Addresses & More](https://www.whitepages.com/)
10. [Find People, Phone Numbers, Addresses & More | Whitepages](https://www.411.com/)
11. [192.com](http://www.192.com/)
12. [RecruitEm](https://recruitin.net/)
13. [Over 50,000 Genealogy Links for US, Canada, UK, Ireland, Australia, NZ - created & maintained by Ian Mold](http://www.genealogylinks.net/)
14. [Ancestry® | Genealogy, Family Trees & Family History Records](https://www.ancestry.co.uk/?geo_a=t&geo_s=us&geo_t=uk&geo_v=2.0.0&o_iid=41013&o_lid=41013&o_sch=Web+Property)
15. [Alumni.NET](http://www.alumni.net/)
16. [100% Free Genealogy - FamilyTreeNow.com](https://www.familytreenow.com/)
----

### Digital Forensics
1. [InVid](http://www.invid-project.eu/)
2. [Get-metadata](https://www.get-metadata.com/)
3. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
4. [List of digital forensics tools](https://en.wikipedia.org/wiki/List_of_digital_forensics_tools)
5. [Forensically](https://29a.ch/photo-forensics/#forensic-magnifier)
6. [Fotoforensics](http://fotoforensics.com/)
7. [IRFANVIEW](http://www.irfanview.com/)
8. [hatlord/Spiderpig](https://github.com/hatlord/Spiderpig)
9. [Metapicz](http://metapicz.com/#landing)
10. [Exifdata](http://exifdata.com/)
11. [ExifTool](https://www.sno.phy.queensu.ca/~phil/exiftool/)
12. [5 Tools to Detect Digitally Altered Images](https://medium.com/@raebaker/5-tools-to-detect-digitally-altered-images-719db4015b5d)
----

### Translation
1. [Google Translate](https://translate.google.co.uk/)
2. [Yandex.Translate – dictionary and online translation between English and over 90 other languages.](https://translate.yandex.com/)
3. [Baidu Translate](https://fanyi.baidu.com/)
4. [DeepL Translator](https://www.deepl.com/translator)
5. [Translate by Babylon - Free Online Translation](http://translation.babylon-software.com/)
6. [Free Online Translation](http://imtranslator.net/translation/)
----

### Free/mium OSINT Software
1. [Hunchly - Better Online Investigations](https://hunch.ly/)
2. [Story Maps](http://storymaps.arcgis.com/en/)
3. [OSINT Browser for Law Enforcement](http://osirtbrowser.com/)
4. [People Tracker](https://trape.co/)
5. [laramies/theHarvester](https://github.com/laramies/theHarvester)
6. [hslatman/awesome-threat-intelligence](https://github.com/hslatman/awesome-threat-intelligence)
7. [Paliscope](https://www.paliscope.com/)
8. [Maltego](https://www.paterva.com/web7/downloads.php)
9. [Video: How to Use Maltego to Research & Mine Data Like an Analyst](https://null-byte.wonderhowto.com/how-to/video-use-maltego-research-mine-data-like-analyst-0180985/)
10. [Open Source Intelligence](https://secapps.com/market/recon)
----

### Tools - Virtual Machine
1. [Buscador OSINT VM](http://inteltechniques.com/buscador)
2. [Kali Linux](https://www.kali.org/)
3. [Linux Mint](https://linuxmint.com/download.php)
4. [Linux Ubuntu](https://www.ubuntu.com/download/desktop)
5. [VirtualBox (free)](https://www.virtualbox.org/wiki/Downloads)
6. [VMWare (paid)](https://www.vmware.com/)
----

### Useful Addons
1. [Addons](https://start.me/p/nRQNRb/addons)
----

### Scraping Data
1. [Data Scraper](https://chrome.google.com/webstore/detail/data-scraper-easy-web-scr/nndknepjnldbdbepjfgmncbggmopgden)
2. [Web Scraper](http://webscraper.io/)
3. [Agenty - Advanced Web Scraper](https://chrome.google.com/webstore/detail/agenty-advanced-web-scrap/gpolcofcjjiooogejfbaamdgmgfehgff/related)
----

### Pastebins
1. [Pastebins (Google Custom Search)](https://inteltechniques.com/osint/menu.pastebins.html)
----

### Newspapers
1. [Newspaperarchive.com](https://newspaperarchive.com/)
2. [newspaper map](https://newspapermap.com/)
----

### Verification
1. [Verification Toolset](https://start.me/p/ZGAzN7/verification-toolset?utm_content=buffer84b1d&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
2. [First Draft News - Verification Dashboard](https://start.me/p/Wrrzk0/tools)
----

### Internet Archives
1. [Archive.today](http://archive.today)
2. [Wayback Machine](https://web.archive.org/)
3. [Freemium solution](https://www.apowersoft.com/video-download-capture.html)
4. [How to Archive Open Source Materials - bellingcat](https://www.bellingcat.com/resources/how-tos/2018/02/22/archive-open-source-materials/)
----

### Time Zone Converter
1. [Worldtime Buddy](https://www.worldtimebuddy.com/)
----

### Other OSINT lists
1. [First Draft](https://start.me/p/vjv80b/first-draft-basic-toolkit)
2. [AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
3. [OSINT Framework](http://osintframework.com/)
4. [Ph055a/awesome_osint](https://github.com/Ph055a/awesome_osint)
5. [IVMachiavelli/OSINT_Team_Links](https://github.com/IVMachiavelli/OSINT_Team_Links)
6. [The Ultimate OSINT Collection](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
7. [jivoi/awesome-osint](https://github.com/jivoi/awesome-osint)
8. [Tools](https://start.me/p/Wrrzk0/tools)
9. [MidaSearch](https://midasearch.org/)
10. [Uk-osint.net](https://www.uk-osint.net)
11. [OSINTessentials](https://www.osintessentials.com/)
12. [https://www.osintessentials.com](https://www.osintessentials.com)
13. [Advanced Digital Toolkit - OSINT - Andy Black Associates](https://www.andyblackassociates.co.uk/resources-andy-black-associates/advanced-digital-toolkit/)
14. [AML Toolbox](https://start.me/p/rxeRqr/aml-toolbox)
15. [Bruno Mortier](https://start.me/p/ZME8nR/osint)
16. [i-intelligence.eu/uploads/public-documents/OSINT_Handbook_2020.pdf](https://i-intelligence.eu/uploads/public-documents/OSINT_Handbook_2020.pdf)
17. [OSINT Tools](https://www.osinttechniques.com/osint-tools.html)
18. [https://github.com/B0sintBlanc/Osint-Bookmarklets](https://github.com/B0sintBlanc/Osint-Bookmarklets)
19. [101+ OSINT Resources for Investigators [2020]](https://i-sight.com/resources/101-osint-resources-for-investigators/)
20. [Welcome to @Ivan30394639 OSINT tools collection](https://cipher387.github.io/osint_stuff_tool_collection/)
21. [The Top 132 Osint Open Source Projects](https://awesomeopensource.com/projects/osint)
----

### Timeline builder
1. [Timeline JS](https://timeline.knightlab.com/)
2. [Time Graphics](https://time.graphics/de/)
3. [Diagram.Codes](https://www.diagram.codes/)
----

### Visualisations
1. [Search for Charts by Data Visualization Functions](https://datavizcatalogue.com/search.html)
2. [The Data Visualisation Catalogue](https://datavizcatalogue.com/)
3. [Data Viz Project](https://datavizproject.com/)
4. [Flowchart Maker & Online Diagram Software](https://app.diagrams.net/)
5. [All Charts – The Python Graph Gallery](https://python-graph-gallery.com/all-charts/)
6. [Datawrapper](https://datawrapper.de)
7. [Flourish.studio](https://flourish.studio/)
----

### Live Social
1. [CrowdTangle](https://apps.crowdtangle.com/public-hub)
----

### Blogging, forum & other communities
1. [Strava](https://labs.strava.com/heatmap/)
2. [Tumblr](https://www.tumblr.com/)
3. [LiveJournal](https://www.livejournal.com/)
4. [Classmates](http://www.classmates.com/)
5. [Wordpress](http://wordpress.org)
6. [Blogger](https://www.blogger.com/)
7. [Wix](http://wix.com)
8. [Medium](http://medium.com)
9. [ProBoards](https://www.proboards.com/)
10. [SquareSpace](https://www.squarespace.com/?)
11. [Joomla](https://www.joomla.org/)
12. [Ghost](https://ghost.org/)
13. [Weebly](https://www.weebly.com/?lang=en)
----

### Dating Apps & Sites
1. [Dating apps and sites for investigators](https://start.me/p/VRxaj5/dating-apps-and-sites-for-investigators)
----

### Discord
1. [DISBOARD](https://disboard.org)
2. [https://tracr.co/](https://tracr.co/)
3. [https://discordservers.com/](https://discordservers.com/)
4. [Discord Center](https://discord.center/)
5. [Discord Me](https://discord.me)
6. [Discord Servers](https://top.gg/servers)
7. [Million Short](https://millionshort.com)
8. [Dutchosintguy/OSINT-Discord-resources](https://github.com/Dutchosintguy/OSINT-Discord-resources)
9. [OSINT Tools (curated by Lorand Bodo)](https://start.me/p/7kxyy2/osint-tools-curated-by-lorand-bodo)
----

### Facebook
1. [Facebook Matrix — Plessas Experts Network](https://plessas.net/facebookmatrix)
2. [Help Center](https://www.facebook.com/help/460711197281324/)
3. [Facebook Matrix](https://docs.google.com/spreadsheets/d/15dD0qSCDYDwVtWKC9zfZk1j8RzmvZARXTu9hrM34DnU/edit#gid=0)
4. [GitHub - Dutchosintguy/Ultimate-Facebook-Scraper: 🤖 A bot which scrapes almost everything about a Facebook user's profile including all public posts/statuses available on the user's timeline, uploaded photos, tagged photos, videos, friends list and their profile photos (including Followers, Following, Work Friends, College Friends etc).](https://github.com/Dutchosintguy/Ultimate-Facebook-Scraper)
5. [Who posted what?](https://whopostedwhat.com/)
6. [Pitoolbox.com.au/facebook-tool](http://pitoolbox.com.au/facebook-tool/)
7. [Facebook Search Tool by Bob Brasich @NetBootCamp](http://netbootcamp.org/facebook.html)
8. [Find my Facebook ID](https://findmyfbid.com/)
9. [Bulk Facebook ID Finder | Seo Tool Station](https://seotoolstation.com/bulk-facebook-id-finder)
10. [Facebook Page Analytics](https://barometer.agorapulse.com/)
11. [Facebook Graph Search Generator by Henk van Ess and Torment Gerhardsen](http://graph.tips/beta/)
12. [Facebook Video Downloader Online](https://www.fbdown.net/index.php)
----

### Flickr
1. [Flickr: Explore everyone's photos on a Map](https://www.flickr.com/map/)
2. [idGettr — Find your Flickr ID](https://www.webpagefx.com/tools/idgettr/)
3. [Free Username Search - Lookup any username on UserSherlock.com](http://usersherlock.com/)
----

### Gab
1. [lyndsysimon/gab](https://github.com/lyndsysimon/gab)
----

### GitHub
1. [Welcome to nginx!](https://grep.app/)
----

### Google+
1. [Google+ User Feed](http://plusfeed.frosas.net/)
----

### Instagram
1. [arc298/instagram-scraper](https://github.com/arc298/instagram-scraper)
2. [https://downloadgram.com/](https://downloadgram.com/)
3. [instaloader/instaloader](https://github.com/instaloader/instaloader)
4. [althonos/InstaLooter](https://github.com/althonos/InstaLooter)
5. [GitHub - realsirjoe/instagram-scraper: scrapes medias, likes, followers, tags and all metadata. Inspired by instagram-php-scraper,bot](https://github.com/realsirjoe/instagram-scraper)
6. [GitHub - drawrowfly/instagram-scraper: Instagram Scraper. Scrape useful data/posts from instagram users, hashtag and locations pages. Comments and people who liked specific posts and soon more. No login or API keys are required](https://github.com/drawrowfly/instagram-scraper)
7. [https://github.com/huaying/instagram-crawler](https://github.com/huaying/instagram-crawler)
8. [https://github.com/ScriptSmith/instamancer](https://github.com/ScriptSmith/instamancer)
----

### LinkedIn
1. [Lusha](https://chrome.google.com/webstore/detail/lusha/mcebeofpilippmndlpcghpmghcljajna/related?hl=en)
2. [Searching on LinkedIn | LinkedIn Help](https://www.linkedin.com/help/linkedin/answer/302/searching-on-linkedin?lang=en)
3. [Michael Bazzell's Blog  » Blog Archive   » LinkedIn Profiles by Email](https://inteltechniques.com/blog/2018/04/25/linkedin-profiles-by-email/)
4. [Socilab - LinkedIn Social Network Visualization, Analysis, and Education](http://www.socilab.com/#home)
5. [initstring/linkedin2username](https://github.com/initstring/linkedin2username)
----

### Pinterest
1. [PinGroupie](http://pingroupie.com/)
2. [DownAlbum](https://chrome.google.com/webstore/detail/downalbum/cgjnhhjpfcdhbhlcmmjppicjmgfkppok)
----

### Reddit
1. [Reddit Investigator](http://www.redditinvestigator.com/)
2. [reddit metrics](http://redditmetrics.com/)
3. [Reddit User Analyser](https://atomiks.github.io/reddit-user-analyser/)
4. [Reddit Archive](http://www.redditarchive.com/)
5. [Files.pushshift.io/reddit](http://files.pushshift.io/reddit/)
6. [Reditr](http://reditr.com/)
7. [Imgur](https://imgur.com/search?q=)
8. [Mostly Harmless: A Google Chrome extension for awesome redditors](http://kerrick.github.io/Mostly-Harmless/#features)
9. [Reddit Enhancement Suite](https://chrome.google.com/webstore/detail/reddit-enhancement-suite/kbmfpngjjgdllneeigpgjifpgocmfgmb)
10. [Redditcommentsearch.com](https://redditcommentsearch.com/)
11. [Reddit Insight](https://www.redditinsight.com/)
12. [TrackReddit](https://www.trackreddit.com/)
13. [Map of Reddit](https://anvaka.github.io/map-of-reddit/?x=255000&y=381000&z=1231248.9168102785)
14. [Rdddeck.com](https://rdddeck.com/)
15. [reddit visualization - YASIV](http://yasiv.com/reddit#/Search?q=osint)
----

### Snapchat
1. [Snapdex: The Best Snapchat Names Index](https://www.snapdex.com/)
2. [CaliAlec/snap-map-private-api](https://github.com/CaliAlec/snap-map-private-api)
3. [Snap Map](https://map.snapchat.com/)
4. [Snapvip.io](https://snapvip.io/)
5. [Find My Snap -  Check if your SnapChat account was leaked on the 2013  hack.](http://findmysnap.com/)
----

### Telegram
1. [nickoala/telepot](https://github.com/nickoala/telepot)
2. [GitHub - expectocode/telegram-export: Export Telegram chat data and history](https://github.com/expectocode/telegram-export)
3. [Github.com/Skarlso/rscrap](https://github.com/Skarlso/rscrap)
4. [Telegram API for OSINT – Part 1 – Users](https://fabledowlblog.wordpress.com/2017/07/10/telegram-api-for-osint-part-1-users/)
5. [Telegram API for OSINT – Part 2 – Messages](https://fabledowlblog.wordpress.com/2017/09/09/telegram-api-for-osint-part-2-messages/)
6. [TelegramDB.org](https://telegramdb.org/)
7. [GitHub - gwu-libraries/uriscrape: Scrape URIs from Telegram channel transcripts in PDF files](https://github.com/gwu-libraries/uriscrape)
8. [Exploring The Jihadi Telegram World: A Brief Overview - European Eye on Radicalization](https://eeradicalization.com/exploring-the-jihadi-telegram-world-a-brief-overview/)
9. [Jihadi Telegram Tutorial Channels: The Lone Wolf’s Lair - European Eye on Radicalization](https://eeradicalization.com/jihadi-telegram-tutorial-channels-the-lone-wolfs-lair/)
10. [Far Right extremism on Telegram: A brief overview - European Eye on Radicalization](https://eeradicalization.com/far-right-extremism-on-telegram-a-brief-overview/)
11. [Welcome to Telethon’s documentation! — Telethon 1.7.7 documentation](https://arabic-telethon.readthedocs.io/en/stable/)
12. [TChannels.me](https://telegramchannels.me)
13. [Largest catalog of Telegram channels. Statistics, analytics, TOP chart. Telegram Analytics.](https://tgstat.com/)
14. [Telegago](https://cse.google.com/cse?&cx=006368593537057042503:efxu7xprihg#gsc.tab=0)
15. [Google Custom Search](https://cse.google.com/cse?cx=004805129374225513871:p8lhfo0g3hg&q=)
16. [Search.buzz.im](https://search.buzz.im/)
17. [Lyzem Blog](https://lyzem.com/)
18. [2200+ Telegram Channels, Groups, Bots and Stickers List](https://telegramchannels.me/)
19. [Telegram Channels List: Discover interesting channels for your Telegram](https://tlgrm.eu/channels)
20. [TelegramDB.org](https://telegramdb.org/)
21. [Week in OSINT 202105](https://sector035.nl/articles/2021-05)
22. [ItIsMeCall911/Awesome-Telegram-OSINT](https://github.com/ItIsMeCall911/Awesome-Telegram-OSINT)
23. [Telegram Network Visualization — Tracing Forwards and Mentions](https://medium.com/dataseries/telegram-network-visualization-tracing-forwards-and-mentions-f75746712fcf)
24. [Building graph for Telegram chats, channels and their neighbors | Ntwrk Today](https://ntwrk.today/2020/04/09/building-telegram-graph.html)
----

### TikTok
1. [sc1341/TikTokOSINT](https://github.com/sc1341/TikTokOSINT)
2. [Open Source Intelligence](https://www.osintcombine.com/tiktok-quick-search)
3. [OSINT Investigations on TikTok](https://www.secjuice.com/osint-investigations-on-tiktok/)
4. [https://raw.githubusercontent.com/sinwindie/OSINT/master/TikTok/Bookmarklet%20Tools](https://raw.githubusercontent.com/sinwindie/OSINT/master/TikTok/Bookmarklet%20Tools)
5. [TikTok Web Viewer Online and Analytics](https://vidnice.com/)
6. [GitHub - Dutchosintguy/tiktok-scraper: TikTok Scraper & Downloader. Scrape information from User, Trending and HashTag pages and download video posts](https://github.com/Dutchosintguy/tiktok-scraper)
----

### Tumblr
1. [Trending](https://www.tumblr.com/explore/trending)
----

### Twitter
1. [Twitter.com/search-advanced](https://twitter.com/search-advanced?lang=en-gb)
2. [Twitter Cheat Sheet](https://thenative.com/wp-content/uploads/twitter-cheat-sheet-PDF.pdf)
3. [TweetDeck](https://tweetdeck.twitter.com/)
4. [Followerwonk: Tools for Twitter Analytics, Bio Search and More](https://moz.com/followerwonk/)
5. [digitalmethodsinitiative/dmi-tcat](https://github.com/digitalmethodsinitiative/dmi-tcat/wiki)
6. [Twitter Analytics by Foller.me](https://foller.me/)
7. [Real-time Twitter Trending Hashtags and Topics](https://www.trendsmap.com/)
8. [TweetBeaver - Home of Really Useful Twitter Tools](https://tweetbeaver.com/)
9. [Twlets](http://twlets.com/)
10. [Mapd.com/demos/tweetmap](https://www.mapd.com/demos/tweetmap/)
11. [Tweet Mapper](https://keitharm.me/project/tweet-mapper)
12. [vaguileradiaz/tinfoleak](https://github.com/vaguileradiaz/tinfoleak)
13. [tinfoleak](https://tinfoleak.com/)
14. [The one million tweet map](https://onemilliontweetmap.com/?center=25.505,-0.09&zoom=2&search=&timeStep=0&timeSelector=0&hashtag1=&hashtag2=&hashtagBattle=0&timeRange=0&timeRange=25&heatmap=0&sun=0&cluster=1)
15. [GeoSocial Footprint](http://geosocialfootprint.com/)
16. [Download Twitter Videos to MP4 & MP3! Online Easy & Free](https://www.downloadtwittervideo.com/)
17. [Botcheck.me](https://botcheck.me/)
18. [TwitterAudit](https://www.twitteraudit.com/)
19. [Search.whotwi.com](https://search.whotwi.com/)
20. [Bot Sentinel Dashboard ‹ Bot Sentinel](https://botsentinel.com/)
21. [Twelve Ways to Spot a Bot](https://medium.com/dfrlab/botspot-twelve-ways-to-spot-a-bot-aedc7d9c110c)
22. [How To Spot Russian Trolls](https://medium.com/dfrlab/trolltracker-how-to-spot-russian-trolls-2f6d3d287eaa)
23. [Understanding bots, botnets and trolls](https://ijnet.org/en/story/understanding-bots-botnets-and-trolls)
24. [Tracking 'Russian bots in real-time' Hamilton68](https://dashboard.securingdemocracy.org/)
25. [The Twitter Twayback Machine by staringispolite](http://staringispolite.github.io/twayback-machine/)
26. [Mentiomapp](http://mentionmapp.com/)
27. [Twitter List Finder](https://cse.google.com/cse/publicurl?cx=016621447308871563343:u4r_fupvs-e)
28. [Twiangulate: analyzing the connections between friends and followers](http://twiangulate.com/search/)
29. [Twitter Search — BackTweets](http://backtweets.com/)
30. [Scopedown](http://downloadperiscopevideos.com/)
31. [Periscope](http://periscope.tv/)
32. [Scraping & Visualizing Twitter mentions – Dutch Osint Guy – Medium](https://medium.com/@dutchosintguy/scraping-visualizing-twitter-mentions-520a6277eb8b)
33. [Tweelpers](http://tweeplers.com/?cc=WORLD)
34. [https://spoonbill.io/](https://spoonbill.io/)
35. [Home page](http://twopcharts.com)
36. [twintproject/twint](https://github.com/twintproject/twint)
37. [Tweet Sentiment Visualization App](https://www.csc2.ncsu.edu/faculty/healey/tweet_viz/tweet_app/)
38. [Accountanalysis.app](https://accountanalysis.app/)
39. [atmoner/TwitWork](https://github.com/atmoner/TwitWork)
40. [TruthNest](https://app.truthnest.com/)
41. [SparkToro](https://sparktoro.com/)
42. [BotSight - Chrome Web Store](https://chrome.google.com/webstore/detail/botsight/kahiiipphdmmkkamddaiiddmidmhbbil/related)
43. [GetOldTweets3 · PyPI](https://pypi.org/project/GetOldTweets3/)
44. [Twitter tools](https://twitter.jeffprod.com/)
45. [igorbrigadir/twitter-advanced-search](https://github.com/igorbrigadir/twitter-advanced-search)
----

### VKontakte
1. [FindFace](https://findface.ru/)
2. [small tool to retreive vk.com (vkontakte) users hidden metadata (state, access, dates, counts, etc) anonymously (without login)](https://gist.github.com/cryptolok/8a023875b47e20bc5e64ba8e27294261)
3. [Поиск сообществ](https://vk.com/communities)
4. [Поиск людей](https://vk.com/people)
5. [ВКонтакте RSS](http://vk-to-rss.appspot.com/)
6. [Фонарик. Таргетинг Вконтакте. Аналог Церебро-таргет.](http://spotlight.svezet.ru/)
7. [ТаргетоLOG — уникальный инструмент таргетирования](http://targetolog.com/)
8. [Шпион ВКонтакте](http://vk5.city4me.com/)
----

### WhatsApp
1. [Fake WhatsApp Chat Generator](http://www.fakewhats.com/generator)
2. [WhatsApp tools of all kind](http://wassame.com/tools/)
3. [Retrieve Metadata](https://github.com/LoranKloeze/WhatsAllApp)
4. [LoranKloeze/WhatsAllApp](https://github.com/LoranKloeze/WhatsAllApp)
----

### YouTube
1. [Savevideo.me](https://savevideo.me/)
2. [Geo Search Tool](http://youtube.github.io/geo-search-tool/search.html)
3. [YouTube Data Tools](https://tools.digitalmethods.net/netvizz/youtube/)
4. [HookTube](https://hooktube.com/)
5. [Keepvid](https://keepvid.com/)
6. [WatchFrameByFrame](http://www.watchframebyframe.com/)
7. [Yout.com](https://yout.com)
8. [Youtube-DL](https://rg3.github.io/youtube-dl/)
9. [YTcomments](http://ytcomments.klostermann.ca/)
10. [Fram by frame](http://www.watchframebyframe.com/)
----

### Tools - Android emulators
1. [Andy - Android Emulator for PC & Mac](https://andyroid.net)
2. [Bluestacks](https://www.bluestacks.com/nl/index.html)
3. [Genymotion](https://www.genymotion.com/)
4. [Leapdroid](https://leapdroid.en.softonic.com/)
5. [Nox](https://www.bignox.com/)
----

### Scrape/Download Data
1. [Downlodo.com](http://www.downlodo.com/)
2. [Data Scraper](https://chrome.google.com/webstore/detail/data-scraper-easy-web-scr/nndknepjnldbdbepjfgmncbggmopgden)
3. [Web Scraper](http://webscraper.io/)
4. [Agenty](https://chrome.google.com/webstore/detail/agenty-advanced-web-scrap/gpolcofcjjiooogejfbaamdgmgfehgff/related)
----

### Other
1. [SMAT - Social Media Analysis Toolkit](https://www.smat-app.com/)
2. [ThoughtfulDev/EagleEye](https://github.com/ThoughtfulDev/EagleEye)
----

### Radio
1. [Listen to Live ATC (Air Traffic Control) Communications](https://www.liveatc.net/)
2. [Radioreference](https://www.radioreference.com/)
3. [Broadcastify](https://www.broadcastify.com/)
4. [Radio.garden](http://radio.garden/)
5. [SDR.hu](http://sdr.hu/)
6. [GlobalTuners](https://www.globaltuners.com/)
7. [ProScan](https://www.proscan.org/)
8. [MilScanners](https://www.milscanners.org/)
9. [TuneIn](https://tunein.com/)
10. [Live Online Radio](https://www.liveonlineradio.net/category/syria)
11. [Radio Garden](http://radio.garden/)
----

### TV
1. [Vidgrid.tk.gg](https://vidgrid.tk.gg/)
----

### Hacking resources
1. [Adversary.crowdstrike.com](https://adversary.crowdstrike.com/)
----

