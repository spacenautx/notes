### Search
1. [Adverse Media Entity Search](https://www.google.com/search?q="ENTITYNAME" AND (arrest | convict | sentence | bribe | embezzle | traffick | seizure | forfeit | suspicion | criminal | plead | fraud | fine | lawsuit | corrupt | launder | ponzi | terror | violation | "Panama papers" | "offshore leak" | “Unexplained Wealth” | bankrupt | FCA | OFAC | OFSI))
2. [OCCRP Database](https://data.occrp.org/)
3. [Google Search Operators](https://ahrefs.com/blog/google-advanced-search-operators/)
4. [Google Translate](https://translate.google.com/)
----

### Professional Registers
1. [UK Financial Services Register Search](https://register.fca.org.uk/)
2. [Jersey Regulated Financial Entity Search](https://www.jerseyfsc.org/registry/)
3. [Irish Financial Services Register](http://registers.centralbank.ie/)
4. [Guernsey Regulated Financial Entity Search](https://www.gfsc.gg/commission/regulated-entities)
5. [UK Solicitors Law Firm Search](https://www.sra.org.uk/consumers/using-solicitor/law-firm-search.page)
6. [UK Solicitor Individual Search](https://solicitors.lawsociety.org.uk/)
7. [UK Barrister Individual Search](https://www.barstandardsboard.org.uk/regulatory-requirements/the-barristers'-register/)
8. [UK Chartered Legal Executives Search](https://www.cilex.org.uk/about_cilex/about-cilex-lawyers/cilex-practitioners-directory)
9. [Irish Solicitor and firm Search](https://www.lawsociety.ie/Find-a-Solicitor/Solicitor-Firm-Search/)
10. [UK Chartered Accountant Search](https://find.icaew.com/)
11. [South Africa Chartered Accountant Search](https://www.saica.co.za/Members/VerifyaMember/tabid/1017/language/en-US/Default.aspx)
12. [UK Licensed Conveyancer Search](https://www.clc-uk.org.uk/cms/cms.jsp?menu_id=19871)
13. [ACCA Accountant Register](https://www.accaglobal.com/pk/en/member/find-an-accountant/directory-of-member.html)
----

### Financial Search Tools
1. [SWIFT BIC Search](https://www2.swift.com/bsl/facelets/bicsearch.faces)
2. [Sort Code Checker](http://www.fasterpayments.org.uk/consumers/sort-code-checker)
3. [Country Codes ISO 2 and 3](https://www.iban.com/country-codes)
----

### Company OSINT
1. [World Company Registers List](https://en.wikipedia.org/wiki/List_of_company_registers)
2. [World Company Registers Information Database](https://beta.e-justice.europa.eu/106/EN/business_registers_in_eu_countries)
3. [OpenCorporates](https://opencorporates.com/)
4. [IOM Country Register](https://services.gov.im/ded/services/companiesregistry/companysearch.iom)
5. [UK Companies House](https://beta.companieshouse.gov.uk/)
6. [UK Companies House (old)](http://wck2.companieshouse.gov.uk/)
7. [UK Insolvency Register](https://www.insolvencydirect.bis.gov.uk/eiir/)
8. [UK Company Search](https://www.companieslist.co.uk/)
9. [The Gazette Company Search](https://www.thegazette.co.uk/companies)
10. [Russian Company Information](https://www.list-org.com/)
11. [List of legal entity types by country](https://en.wikipedia.org/wiki/List_of_legal_entity_types_by_country)
12. [ICIJ offshore leaks](https://offshoreleaks.icij.org/)
13. [BankTrack - Bank Ownership Data](https://www.banktrack.org/search#category=banks)
14. [Global Company Incorporation Information and Data](https://www.lowtax.net/g/jurisdictions/)
15. [World Bank Ineligible Firms & Individuals](https://projects-beta.worldbank.org/en/projects-operations/procurement/debarred-firms)
----

### Individual OSINT
1. [MRZ (Passport) Calculator](http://www.emvlab.org/mrz/)
2. [World ID Database](https://www.consilium.europa.eu/prado/en/search-by-document-country.html)
3. [UK GRO Office (Birth and Death Certs)](https://www.gro.gov.uk/gro/content/certificates/login.asp)
4. [Scottish People Register (Birth, death)](https://www.scotlandspeople.gov.uk/)
5. [Unofficial UK Birth Marriage and Death Certs](https://www.freebmd.org.uk/)
6. [OSINT Framework](https://osintframework.com/)
7. [Postcode Finder UK](https://www.royalmail.com/find-a-postcode)
----

### Charity OSINT
1. [UK Charity Search| BETA](https://beta.charitycommission.gov.uk/)
2. [UK Charity Search](http://apps.charitycommission.gov.uk/ShowCharity/RegisterOfCharities/AdvancedSearch.aspx)
3. [Republic of Ireland Charities Register](https://www.charitiesregulator.ie/en/information-for-the-public/search-the-charities-register)
4. [Scottish Charity Search](https://www.oscr.org.uk/about-charities/search-the-register/register-search/)
----

### Sanctions
1. [UK Consolidated Sanctions List](https://www.gov.uk/government/publications/financial-sanctions-consolidated-list-of-targets/consolidated-list-of-targets)
2. [OFAC List Search](https://sanctionssearch.ofac.treas.gov/)
3. [EU Consolidated Sanctions List](https://data.europa.eu/euodp/en/data/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions)
4. [Individual Sanctions Search](https://namescan.io/FreeSanctionsCheck.aspx)
5. [Sign Up for OFAC Alerts](https://public.govdelivery.com/accounts/USTREAS/subscriber/new?topic_id=USTREAS_61)
6. [Sign Up for OFSI Alerts](https://www.gov.uk/email/subscriptions/new?frequency=immediately&topic_id=office-of-financial-sanctions-implementation)
7. [EU Sanctions Map](https://www.sanctionsmap.eu/#/main)
----

### Financial Crime Guidance
1. [JMLSG Guidance](http://www.jmlsg.org.uk/)
2. [PWC Global Financial Crime Comparison](https://citt.pwc.de/gfc/submodule_selection/)
3. [FCA Guide to Financial Crime](https://www.handbook.fca.org.uk/handbook/FCG.pdf)
4. [REP-CRIM Guidance](https://www.handbook.fca.org.uk/handbook/SUP/16/Annex42B.html?date=2018-01-03)
5. [NCA publications](https://www.nationalcrimeagency.gov.uk/who-we-are/publications?limit=100&sort=created_on&direction=desc)
----

### Financial Crime Risk Guidance
1. [Basel AML Risk list](https://www.baselgovernance.org/asset-recovery/basel-aml-index/public-ranking)
2. [Corruption Perceptions Index 2018](https://www.transparency.org/cpi2018)
3. [FATF Recommendations](https://www.fatf-gafi.org/media/fatf/documents/recommendations/pdfs/FATF%20Recommendations%202012.pdf)
4. [Know Your Country](https://www.knowyourcountry.com/copy-of-country-reports)
5. [CIA - World Factbook](https://www.cia.gov/library/publications/resources/the-world-factbook/)
----

### UK Financial Crime Legislation
1. [Bribery Act 2010](https://www.legislation.gov.uk/ukpga/2010/23)
2. [Criminal Finances Act 2017](http://www.legislation.gov.uk/ukpga/2017/22/contents/enacted)
3. [Fraud Act 2006](http://www.legislation.gov.uk/ukpga/2006/35/contents)
4. [Money Laundering and Terrorist Financing (Amendment) Regulations 2019](http://www.legislation.gov.uk/uksi/2019/1511/contents/made)
5. [Money Laundering Regulations 2007](http://www.legislation.gov.uk/uksi/2007/2157/contents/made)
6. [Money Laundering Regulations 2017](https://www.legislation.gov.uk/uksi/2017/692/contents)
7. [Proceeds of Crime Act 2002](http://www.legislation.gov.uk/ukpga/2002/29/contents)
8. [Sanctions and Anti-Money Laundering Act 2018](http://www.legislation.gov.uk/ukpga/2018/13/contents/enacted)
9. [Terrorism Act 2000](http://www.legislation.gov.uk/ukpga/2000/11/contents)
----

### News
1. [Thinking about Crime Limited: News](http://www.thinkingaboutcrime.com/newsroom.htm)
2. [National Crime Agency - News](https://www.nationalcrimeagency.gov.uk/news)
3. [National Crime Agency - Publications](https://www.nationalcrimeagency.gov.uk/who-we-are/publications?sort=created_on&direction=desc)
4. [I hate money laundering | For those who have a love/hate relationship with the world's biggest financial crime](https://ihatemoneylaundering.wordpress.com/)
5. [KYC360](https://www.riskscreen.com/kyc360/news/)
6. [Money Laundering Watch](https://www.moneylaunderingnews.com/)
7. [OCCRP](https://www.occrp.org/en/)
8. [Thefinancialcrimenews.com](https://thefinancialcrimenews.com/)
----

