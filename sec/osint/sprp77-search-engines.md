### OSINT/ Hacking Search
1. [Public Bookmarks Search](https://cse.google.com/cse?cx=005797772976587943970:3tu7im1-rdg)
2. [WordPress Content Snatcher](https://cse.google.com/cse?cx=011081986282915606282:w8bndhohpi0)
3. [The Ethical Hacker's Search Engine](https://cse.google.com/cse?cx=009049714591083331396:dm4qfriqf3a)
4. [Hacking Docs Search Engine](https://cse.google.com/cse?cx=013991603413798772546:ekjmizm8vus)
5. [OSINT Tools, Resources & News Search Engine](https://cse.google.com/cse?cx=006290531980334157382:qcaf4enph7i)
6. [Top Level Domains Hacker](https://cse.google.com/cse?cx=013991603413798772546:ku75d_g_s6a)
7. [Amazon Cloud Search Engine](https://cse.google.com/cse?cx=005797772976587943970:g-6ohngosio)
8. [US Government Intel CSE](https://cse.google.com/cse?cx=009049714591083331396:i7cetsiiqru)
9. [Short URL Search Engine](https://cse.google.com/cse?cx=017261104271573007538:magh-vr6t6g)
10. [Infosec-institute CSE](https://cse.google.com/cse?cx=013991603413798772546:8c1g6f0frp8)
11. [Amazon Bucket Hacker](https://cse.google.com/cse?cx=018215287813575168593:c17elzg384a)
----

### OSINT Resources Folder
1. [Sprp77 Awesome OSINT Folder](http://bit.ly/2E8Tml6)
----

### Google Search Engines
1. [Google Alerts Search Engine](https://cse.google.com/cse?cx=013991603413798772546:rs7bbm8kdsg)
2. [Google Docs Search Engine](https://cse.google.com/cse?cx=013991603413798772546:rse-4irjrn8)
3. [Google+ Collections & Communities Search](https://cse.google.com/cse?cx=013991603413798772546:g5vjn23udla)
4. [Google Plus Communities CSE](https://cse.google.com/cse?cx=009238533315723793042:osjzdvijbbe)
5. [The Googlier Search Index](https://cse.google.com/cse?cx=005797772976587943970:nixxa0prgrw)
6. [Google CSE Resources Search Engine](https://cse.google.com/cse?cx=013991603413798772546:dhoqafcmphk)
7. [Google CSE Finder](https://cse.google.com/cse?cx=011081986282915606282:fa52ldjw5to)
8. [Chrome Web Store Search Engine](https://cse.google.com/cse?cx=006205189065513216365:pn3lumi80ne)
9. [Google Drive Folder Search Engine](https://cse.google.com/cse?cx=013991603413798772546:nwzqlcysx_w)
10. [Dorky Google Search Engine](https://cse.google.com/cse?cx=017648920863780530960:lddgpbzqgoi)
11. [The Custom Search Engine CSE](https://cse.google.com/cse?cx=001691553474536676049:op4j-wn6tq4)
12. [GOO.GL Search Engine](https://cse.google.com/cse?cx=001691553474536676049:vpcby_api4y)
13. [Google Domain Hacker](https://cse.google.com/cse?cx=005797772976587943970:ca2hiy6hmri)
----

### TV Video Search Engines
1. [Periscope/PSCP TV Search Engine](https://cse.google.com/cse?cx=005797772976587943970:audysx4758e)
2. [Webcam Custom Search Engine](https://cse.google.com/cse?cx=013991603413798772546:gjcdtyiytey)
3. [Live Broadcasts Search Engine](https://cse.google.com/cse?cx=013991603413798772546:ke-odhnab38)
----

### Photo Search Engines
1. [The Photo Album Finder](https://cse.google.com/cse?cx=013991603413798772546:bldnx392j6u)
2. [Twitter Image Search Engine](https://cse.google.com/cse?cx=006205189065513216365:vltpvp4_gyo)
3. [Beautiful Photo Album Finder](https://cse.google.com/cse?cx=013991603413798772546:bt8ybjlsnok)
4. [Google Photo Archives Hacker](https://cse.google.com/cse?cx=006205189065513216365:vp0ly0owiou)
5. [The Photastic Search Engine](https://cse.google.com/cse?cx=017261104271573007538:vmpv6nt8dc4)
6. [Wallpaper Search Engine](https://cse.google.com/cse?cx=006205189065513216365:zorwyd7ztvk)
7. [Google+ Photos Search Engine](https://cse.google.com/cse?cx=006205189065513216365:uo99tr1fxjq)
8. [GooglePlus Photo Albums Search Engine](https://cse.google.com/cse?cx=013991603413798772546:5h_z8fh4eyy)
----

### SEO Search Engines
1. [SEO Graphic Resources](https://cse.google.com/cse?cx=006290531980334157382:3x8i6ydquuc)
2. [SEO Resources Search Engine](https://cse.google.com/cse?cx=005797772976587943970:i7q6z1kjm1w)
3. [Super SEO Search Engine](https://cse.google.com/cse?cx=005797772976587943970:ddxth6fexqw)
4. [Super IFTTT Applet Finder](https://cse.google.com/cse?cx=000501358716561852263:xzfiqchwcj8)
5. [The Search Engine Finder](https://cse.google.com/cse?cx=013991603413798772546:csa-hd4a4dk)
----

### Code Search Engines
1. [The Code Chaser](https://cse.google.com/cse?cx=013991603413798772546:xbbb31a0ecw)
2. [CODE WITH THE FLOW](https://cse.google.com/cse?cx=013991603413798772546:gejhsvqignk)
3. [Raw Git Hacker](https://cse.google.com/cse?cx=007791543817084091905:vmwkk8ksx9k)
----

### Technology Custom Search Engines
1. [Tech, Non-Tech & Diversity Search Engine](https://cse.google.com/cse/publicurl?cx=012020006043586000305:w5dhsgzmkeg#gsc.tab=0 https://cse.google.com/cse/publicurl?cx=015211855213760009025:zpqcxcycah8 https://cse.google.com/cse/publicurl?cx=012236071480267108189:0y1g3vhxpoe https://cse.google.com/cse/publicurl?cx=008789176703646299637:mubfrybi2ja)
2. [Technical Search Engine](https://cse.google.com/cse/publicurl?cx=000826244820084663955:6wz9grqlj6e)
3. [Machine Learning](https://cse.google.com/cse/publicurl?cx=016964911540212529382:9j83vmmllem)
4. [GoToMeeting Video Conference](https://cse.google.com/cse?cx=011444696387487602669:isjgvad4bmi)
----

### Social Media Search
1. [Social Media Search Engine](https://cse.google.com/cse?cx=016621447308871563343:0p9cd3f8p-k)
2. [Google Domain Hacker](https://cse.google.com/cse?cx=005797772976587943970:ca2hiy6hmri)
3. [Meetup Custom Search Engine](https://cse.google.com/cse?cx=013991603413798772546:ego1pf6emq8)
4. [Reddit Search Engine](https://cse.google.com/cse?cx=017261104271573007538:bbzhlah6n4o)
5. [Github Awesome Search Engine](https://cse.google.com/cse?cx=017261104271573007538:fqn_jyftcdq)
6. [Tweet Archive Search Engine](https://cse.google.com/cse?cx=013991603413798772546:h8jz6ooyjkk)
7. [Twitter List Finder](https://cse.google.com/cse?cx=016621447308871563343:u4r_fupvs-e)
8. [Twitter Moments Search Engine](https://cse.google.com/cse?cx=016621447308871563343:nuitgl_de4k)
9. [Facebook Custom Search Engine](https://cse.google.com/cse/publicurl?cx=016621447308871563343:vylfmzjmlti)
10. [Trello Search](https://cse.google.com/cse?cx=013991603413798772546:kwtsivvtnby)
11. [Wordpress Blog Search](https://cse.google.com/cse?cx=017261104271573007538:ffk_jpt64gy)
12. [TikTok Search Engine](https://cse.google.com/cse?cx=011444696387487602669:aqf7d9w73om)
----

### Infomation Search Engines
1. [The Researcher's Vault](https://cse.google.com/cse?cx=013991603413798772546:fjfpayt0bje)
2. [Visual Concepts CSE](https://cse.google.com/cse?cx=013991603413798772546:gj6rx9spox8)
3. [Livebinders E-Leaning Resources](https://cse.google.com/cse?cx=013991603413798772546:65pngvxl3e4)
4. [SEARCH BY FILETYPE](https://cse.google.com/cse?cx=013991603413798772546:mu-oio3a980)
5. [Find Pasted Text CSE](https://cse.google.com/cse?cx=013991603413798772546:nxs552dhq8k)
6. [Mailing List Archives Search Engine](https://cse.google.com/cse?cx=013991603413798772546:sipriovnbxq)
7. [PBWorks Search Engine](https://cse.google.com/cse?cx=017261104271573007538:xhguhddcxuk)
8. [Wikidot Search Engine](https://cse.google.com/cse?cx=013991603413798772546:1tl6ugi8jja)
9. [The Search Engine Search Engine](https://cse.google.com/cse/publicurl?cx=013991603413798772546:hvkibqdijhe)
10. [DocumentCloud.org File Search Engine](https://cse.google.com/cse?cx=011444696387487602669:o962nsq0qgk)
11. [Google Form Responses CSE](https://cse.google.com/cse?cx=011444696387487602669:ctaqbesethw)
----

### Job/Recruiting Search Engines
1. [Job Search - US [ALL]](https://cse.google.com/cse/publicurl?cx=013207573749533308742:ura3u1olqiu)
2. [Job Search](https://cse.google.com/cse/publicurl?cx=partner-pub-7088803035940952:ynwgxo8bxh8)
3. [Recruiters and Sourcers](https://cse.google.com/cse/publicurl?cx=009462381166450434430:1w9uqgljuvq)
4. [LinkedIn X-Ray](https://cse.google.com/cse/home?cx=002879889969213338875:ykfcyju2xe8)
5. [CareerSpace | Job Search..](https://cse.google.com/cse/publicurl?cx=partner-pub-4067593612301221:1623481997)
----

