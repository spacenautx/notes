### Start here
1. [Top 5 Steps to Immerse yourself into the Cyber Security field | SANS](https://www.sans.org/blog/top-5-steps-to-immerse-yourself-into-the-cybersecurity-field/)
----

### Communities
1. [Discord: The Cyber Mentor](https://discord.gg/REfpPJB/)
2. [Discord: The Many Hats Club](https://themanyhats.club/invite/)
3. [Net Sec Focus](https://www.netsecfocus.com/)
4. [Slack: OWASP](https://owasp.slack.com/)
5. [Slack: VeteranSec](https://veteransec.com/)
----

### Training
1. [Best websites to help you build your hacking skills](https://medium.com/@rgreenhagen/best-websites-to-help-you-build-your-hacking-skills-75f4246a9ea1)
2. [Cybrary](https://www.cybrary.it/)
3. [ENISA CSIRT Training Resources](https://www.enisa.europa.eu/topics/trainings-for-cybersecurity-specialists/online-training-material)
4. [Free Learning - Your daily programming eBook from Packt](https://www.packtpub.com/eu/free-learning)
5. [Ir0nin.com - Hacker's Resources](http://ir0nin.com)
6. [Learn Cybersecurity | Free Online Courses | Class Central](https://www.classcentral.com/subject/cybersecurity)
7. [National Cyber Security Alliance](https://staysafeonline.org/)
8. [TryHackMe](https://tryhackme.com/)
9. [VulnHub](https://www.vulnhub.com/)
10. [Free Training – DFIR Diva](https://freetraining.dfirdiva.com/)
----

### News
1. [.:: Phrack Magazine ::.](http://phrack.org/index.html)
----

### List of Computer Security Certifications
1. [List of computer security certifications](https://en.m.wikipedia.org/wiki/List_of_computer_security_certifications)
2. [Security Certification Progression Chart](https://pauljerimy.com/security-certification-roadmap/)
----

### Documentation
1. [Github: public-pentesting-reports](https://github.com/juliocesarfort/public-pentesting-reports)
2. [GitHub: the-book-of-secret-knowledge](https://github.com/trimstray/the-book-of-secret-knowledge)
3. [Jupyter Notebooks for BloodHound Analytics and Alternative Visualizations 📊 !](https://medium.com/threat-hunters-forge/jupyter-notebooks-for-bloodhound-analytics-and-alternative-visualizations-9543c2df576a)
----

### Virtualization
1. [USB physical port mapping - Proxmox VE](https://pve.proxmox.com/wiki/USB_physical_port_mapping)
----

### Firewalls
1. [OPNsense® - Fully Open Sourced](https://opnsense.org/)
2. [pfSense - Your Next-generation Secure NetworkNetgate Logo](https://www.netgate.com/solutions/pfsense/)
----

### Lab Docs
1. [Active Directory Lab for Penetration Testing](https://medium.com/@browninfosecguy/active-directory-lab-for-penetration-testing-5d7ac393c0c4)
2. [clong/DetectionLab](https://github.com/clong/DetectionLab)
3. [GitHub: APT-Lab-Terraform: Purple Teaming Attack & Hunt Lab - Terraform](https://github.com/DefensiveOrigins/APT-Lab-Terraform)
4. [Installing a Fake Internet with INetSim and PolarProxy](https://www.netresec.com/?page=Blog&month=2019-12&post=Installing-a-Fake-Internet-with-INetSim-and-PolarProxy)
5. [splunk/attack_range: A tool that allows you to create vulnerable instrumented local or cloud environments to simulate attacks against and collect the data into Splunk](https://github.com/splunk/attack_range)
----

### Defense/Blue Team
1. [Blue Team Training Toolkit](https://www.bt3.no/)
2. [Github: DetectionLab](https://github.com/clong/DetectionLab)
3. [Github: Great List of Resources to Build an Enterprise Grade Home Lab](https://github.com/aboutsecurity/blueteam_homelabs)
4. [GitHub: leonidas - Automated Attack Simulation](https://github.com/fsecurelabs/leonidas)
5. [Github: SecLists](https://github.com/danielmiessler/SecLists)
6. [GitHub: tsunami-security-scanner-plugins](https://github.com/google/tsunami-security-scanner-plugins)
----

### Infosec Jypyterthon 2020
1. [Infosec Jupyterthon 2020!](https://www.youtube.com/watch?v=QCVd4Svtaa8)
2. [Infosec Jupyterthon 2020! Interactive Resources](https://infosecjupyterthon.com/notebooks/0_banner.html)
----

### Documentation
1. [The Penetration Testing Execution Standard](http://www.pentest-standard.org/index.php/Main_Page)
2. [PTES Technical Guidelines](http://www.pentest-standard.org/index.php/PTES_Technical_Guidelines)
----

### Hacking Tools
1. [Basecrack - Best Decoder Tool For Base Encoding Schemes](https://www.kitploit.com/2020/06/basecrack-best-decoder-tool-for-base.html?utm_source=dlvr.it&utm_medium=twitter)
2. [Cryptii](https://cryptii.com/)
3. [GitHub - jthuraisamy/SysWhispers: AV/EDR evasion via direct system calls.](https://github.com/jthuraisamy/SysWhispers)
4. [Github: Ciphey - automated decryption/decoding/cracking tool](https://github.com/Ciphey/Ciphey)
5. [Github: streisand - VPN VM](https://github.com/StreisandEffect/streisand)
6. [Living Off The Land Binaries and Scripts (and now also Libraries)](https://lolbas-project.github.io/)
7. [Metasploit Unleashed](https://www.offensive-security.com/metasploit-unleashed/)
8. [Nishang](https://github.com/samratashok/nishang)
9. [Penetration Testers’ Guide to Windows 10 Privacy & Security](https://hackernoon.com/the-2017-pentester-guide-to-windows-10-privacy-security-cf734c510b8d)
10. [Pentest Tools Framework : A Database Tools For Penetration Testing](https://kalilinuxtutorials.com/pentest-tools-framework/)
11. [Pentester Land · Offensive Infosec](https://pentester.land/)
12. [PowerShell Empire | Building an Empire with PowerShell](https://www.powershellempire.com/)
13. [PowerSploit](https://github.com/PowerShellMafia/PowerSploit)
14. [Responder](https://github.com/SpiderLabs/Responder)
15. [Spyse – A Cyber Security Search Engine | Penetration Testing Lab](https://pentestlab.blog/2020/06/15/spyse-a-cyber-security-search-engine/)
16. [The C2 Matrix](https://www.thec2matrix.com/)
----

