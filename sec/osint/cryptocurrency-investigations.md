### Abuse Databases
1. [Bitcoin Abuse Database - Scam Database](https://www.bitcoinabuse.com/)
2. [Scam Alert - Live Scam Checking](https://scam-alert.io/)
3. [Whale-alert.io - Live Transaction Alerting](https://whale-alert.io/)
4. [RansomWatch - Known Ransom Crypto Addresses](https://ransomwatch.telemetry.ltd/#/INDEX)
5. [Cryptscam.com - Scam Database](http://cryptscam.com/)
6. [Bitcoin Whos Who - Address History Lookup](https://bitcoinwhoswho.com/)
7. [Know Your Coin Privacy - Address Re-use and Deterministic Links](https://www.kycp.org/)
8. [BitRank Verified - BTC Transaction Trust Ranking](https://bitrankverified.com/home)
9. [Cryptocurrency Alerting - Wallet Transaction Alerting](https://cryptocurrencyalerting.com/wallet-watch.html)
----

### DeFi Exchanges
1. [List of Decentralized Exchanges](https://defiprime.com/exchanges#aggregators)
2. [Uniswap](https://app.uniswap.org/#/swap)
3. [SUSHI](https://app.sushi.com/en/swap)
4. [1inch](https://app.1inch.io/#/1/swap/DAI/ETH)
5. [Aave](https://app.aave.com/#/asset-swap)
6. [PancakeSwap](https://pancakeswap.finance/swap)
7. [dYdX](https://trade.dydx.exchange/r/defiprime)
8. [Kyberswap](https://kyberswap.com/#/swap)
9. [Raydium](https://raydium.io/swap/)
10. [DexGuru](https://dex.guru/token/0xe9e7cea3dedca5984780bafc599bd69add087d56-bsc)
11. [Multichain](https://app.multichain.org/#/multi)
----

### Atomic Swaps
1. [ChangeNOW](https://changenow.io/)
2. [Changelly](https://changelly.com/)
3. [StealthEX](https://stealthex.io/)
4. [Simpleswap.io](https://simpleswap.io/)
5. [Swapspace.co](https://swapspace.co/?to=eth&amount=0.1)
6. [Beam4.me](https://beam4.me/)
7. [Swapzone](https://swapzone.io/)
8. [FixedFloat](https://fixedfloat.com/)
----

### Data Visualization
1. [BitInfoCharts](https://bitinfocharts.com/)
2. [OSINT Combine - DataVis Tool](https://osintcombine.tools/)
3. [Gephi Visualization Tool](https://gephi.org/)
4. [Maltego Visualization Tool](https://www.maltego.com/downloads/)
5. [Lampyre Visualization Tool](https://lampyre.io/)
6. [Blockpath - BTC Transaction Charting](https://blockpath.com/)
7. [BTC Transaction Pathing](https://learnmeabitcoin.com/tools/path/)
8. [Bitcoin Big Bang - Elliptic](https://info.elliptic.co/hubfs/big-bang/bigbang-v1.html)
9. [Whale Alert Transaction Map](https://whale-alert.io/transaction-map)
----

### Locate Nodes
1. [Bitaps.com](https://bitaps.com/)
2. [https://min-api.cryptocompare.com/documentation](https://min-api.cryptocompare.com/documentation)
3. [BitNodes.io](https://bitnodes.io/)
----

### Crypto Investigation Platforms
1. [Breadcrumbs - Crypto Investigation Platform](https://www.breadcrumbs.app/)
2. [Graphsense - Cryptoasset Analytics](https://graphsense.info/)
3. [Qlue [**PAID**]](https://qlue.io/)
4. [ATII - Project Hades [**PAID**]](https://followmoneyfightslavery.org/hades/)
----

### Specialized Analytic Services
1. [Crystalblockchain.com](https://crystalblockchain.com/)
2. [CipherTrace](https://ciphertrace.com/)
3. [Chainalysis](https://www.chainalysis.com/)
4. [AMLBot](https://amlbot.com/)
5. [Elliptic](https://www.elliptic.co/)
6. [Coinfirm](https://www.coinfirm.com/)
----

### Web3 Browsers and Extensions
1. [Comparison of Top 3 Blockchain Web Browsers (Part 2) | by Acent | Acent tech | Medium](https://medium.com/acent-tech/comparison-of-top-3-blockchain-web-browsers-part-2-10698b02722)
2. [Osiris](https://browseosiris.com/)
3. [Opera](https://www.opera.com/download)
4. [Brave Browser](https://brave.com/)
5. [Beaker Browser](https://beakerbrowser.com/)
----

### Certifications
1. [Certified Cryptocurrency Expert™ (CCE) - Blockchain Council](https://www.blockchain-council.org/certifications/certified-cryptocurrency-expert/)
2. [Certified Cryptocurrency Auditor™ (CCA) - Blockchain Council](https://www.blockchain-council.org/certifications/certified-cryptocurrency-auditor-instructor-led-training/)
3. [Certified Cryptocurrency Forensic Investigator - McAfee Institute](https://www.mcafeeinstitute.com/products/certified-cryptocurrency-forensic-investigator-ccfi)
4. [Certified Cryptocurrency Investigator (CCI) - Blockchain Intelligence Group](https://www.cryptoinvestigatortraining.com/)
5. [FIU Connect (Cryptoassets) - ManchesterCF](https://www.manchestercf.com/cryptoassets-2/)
6. [Cryptocurrency Tracing Certified Examiner (CTCE) - CipherTrace](https://ciphertrace.com/certified-examiner-training/#1639596452769-9be0d67b-ba88)
7. [Virtual Currency and Blockchain Certificate - ACAMS](https://www.acams.org/en/training/certificates/virtual-currency-and-blockchain)
8. [Cellebrite Learning Center: Cellebrite Cryptocurrency Investigative Techniques (CCIT)](https://www.cellebritelearningcenter.com/mod/page/view.php?id=61967)
9. [Chainalysis KYT Certification - Chainalysis](https://www.chainalysis.com/chainalysis-kyt-certification/)
10. [Chainalysis
        CCFC - Chainalysis](https://www.chainalysis.com/ccfc/)
11. [Chainalysis
        CISC - Chainalysis](https://www.chainalysis.com/cisc/)
12. [Chainalysis Reactor Certification - Chainalysis](https://www.chainalysis.com/chainalysis-reactor-certification/)
13. [Crypto Currency Security Standard Auditor (CCSSA)bitcoinyoutubefacebooklinkedinmailtwitter](https://cryptoconsortium.org/certifications/ccssa/)
14. [Cryptocurrency Asset Investigation Certification (CAIC) - Akademie K.](https://www.karhrman.com/?gclid=Cj0KCQiA6NOPBhCPARIsAHAy2zAxvRLjVDLmsoqJShsSvxiOOZIxyZ1lH7hPIoTKGRlb1DzJDUsOsxQaAqlIEALw_wcB)
----

