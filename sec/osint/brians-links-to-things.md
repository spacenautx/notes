### Professional Bodies
1. [ISC(2)](https://www.isc2.org/)
2. [ISACA](https://www.isaca.org/)
3. [BCS, The Chartered Institute for IT](https://www.bcs.org/)
----

### Cloud
1. [Compare Cloud.in](https://comparecloud.in/)
----

### Property
1. [HM Land Registry - Property Alert](https://propertyalert.landregistry.gov.uk/propertyalert/)
2. [HM Land Registry](https://www.gov.uk/government/organisations/land-registry)
3. [Offshore Property Ownership Checker](https://www.private-eye.co.uk/registry)
----

### Government Local & National
1. [Bidstats - Gov Contract Awards](https://bidstats.uk/)
2. [Local Information - Keep It In The Community](https://www.keepitinthecommunity.org/)
3. [MPs' Expenses](https://www.mpsexpenses.info/)
4. [Open Police data sets](https://data.police.uk/)
5. [They Work For You - Find Your MP](https://www.theyworkforyou.com/)
6. [UK Parliament Votes and Proceedings](https://www.parliament.uk/business/publications/business-papers/commons/votes-and-proceedings)
7. [UnHerd Britain](https://election.unherd.com/)
8. [What Do They Know - FOI Requests](https://www.whatdotheyknow.com/)
9. [WriteToThem - Email your Councillor, MP, MEP, MSP or Welsh, NI, or London Assembly Member for free](https://www.writetothem.com/)
----

### Auditing
1. [AuditScripts.com - Audit Questionaires](https://www.auditscripts.com/free-resources/critical-security-controls/)
2. [FedRAMP.gov Templates](https://www.fedramp.gov/templates/)
----

### Shopping
1. [eBay](http://ebay.co.uk)
2. [Eventbrite](https://www.eventbrite.co.uk)
3. [Hak5](https://shop.hak5.org/)
4. [Madbob Lockpicks](https://www.madbobpicks.co.uk/)
5. [Maltronics](https://maltronics.com/)
6. [Werd](https://www.werd.com/)
7. [Get Console for Cisco - iPhone and iPad Serial Cables](https://www.get-console.com/shop/)
8. [Amazon](https://www.amazon.co.uk)
----

### Tools to Download
1. [Basecrack - Best Decoder Tool For Base Encoding Schemes](https://www.kitploit.com/2020/06/basecrack-best-decoder-tool-for-base.html)
2. [BruteShark Network Forensic Analysis Tool — SkyNet Tools](https://skynettools.com/bruteshark-network-forensic-analysis-tool)
3. [Creepy by ilektrojohn](http://www.geocreepy.com)
4. [EvilNet - Network Attack Wifi Attack Vlan Attack Arp Attack Mac Attack Attack Revealed Etc...](https://www.kitploit.com/2020/06/evilnet-network-attack-wifi-attack-vlan.html)
5. [GitHub - 0xInfection/SIPTorch: A (WIP) "SIP Torture" testing suite.](https://github.com/0xInfection/SIPTorch)
6. [GitHub - Ciphey/Ciphey: Automated decryption tool](https://github.com/Ciphey/Ciphey)
7. [GitHub - megadose/holehe: holehe allows you to check if the mail is used on different sites like twitter, instagram and will retrieve information on sites with the forgotten password function.](https://github.com/megadose/holehe?goal=0_f50a9c9026-1371fa1ea1-1285120861&mc_cid=1371fa1ea1&mc_eid=8b90efa453)
8. [GitHub - Mr-xn/Penetration_Testing_POC: 渗透测试有关的POC、EXP、脚本、提权、小工具等，欢迎补充、完善---About penetration-testing python-script poc getshell csrf xss cms php-getshell domainmod-xss penetration-testing-poc csrf-webshell cobub-razor cve rce sql sql-poc poc-exp bypass oa-getshell cve-cms](https://github.com/Mr-xn/Penetration_Testing_POC)
9. [GitHub - sensepost/routopsy](https://github.com/Sensepost/routopsy)
10. [GitHub - spyx/monkey-shell](https://github.com/spyx/monkey-shell?goal=0_f50a9c9026-39ce6d9b36-1285120861&mc_cid=39ce6d9b36&mc_eid=8b90efa453)
11. [GitHub - trustedsec/spoonmap](https://github.com/trustedsec/spoonmap)
12. [GRecon - Your Google Recon Is Now Automated](https://www.kitploit.com/2020/12/grecon-your-google-recon-is-now.html)
13. [IPFuscator](https://github.com/vysecurity/IPFuscator)
14. [Keytap2 - acoustic keyboard eavesdropping based on language n-gram frequencies · Discussion #31 · ggerganov/kbd-audio · GitHub](https://github.com/ggerganov/kbd-audio/discussions/31)
15. [Luckystrike: An Evil Office Document Generator.](https://www.shellntel.com/blog/2016/9/13/luckystrike-a-database-backed-evil-macro-generator)
16. [NTLMRawUnHide: parse network packet capture files and extract NTLMv2 hashes](https://securityonline.info/ntlmrawunhide)
17. [Osintgram - A OSINT Tool On Instagram](https://www.kitploit.com/2020/08/osintgram-osint-tool-on-instagram.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+PentestTools+%28PenTest+Tools%29)
18. [RustScan: Faster Nmap Scanning with Rust](https://github.com/brandonskerritt/RustScan)
19. [uk.liveuamap.com](https://uk.liveuamap.com)
20. [Vulmap - Web Vulnerability Scanning And Verification Tools](https://www.kitploit.com/2020/12/vulmap-web-vulnerability-scanning-and.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+PentestTools+%28PenTest+Tools%29)
21. [FisherMan - CLI Program That Collects Information From Facebook User Profiles Via Selenium](https://www.kitploit.com/2021/08/fisherman-cli-program-that-collects.html)
22. [Portspoof -  A new approach to fight back port and service scanners.](http://drk1wi.github.io/portspoof/)
23. [GitHub - GlennPegden2/probe-stalker: Analyses wifi probes (from mobile phones etc) and looks them up on wigle to provide possibly commonly visited locations of people near you](https://github.com/GlennPegden2/probe-stalker)
24. [in9uz · GitHub](https://github.com/in9uz)
25. [GitHub - DarkCoderSc/SubSeven: SubSeven Legacy Official Source Code Repository](https://github.com/DarkCoderSc/SubSeven)
----

### Legal
1. [Infolaw.co.uk](https://www.infolaw.co.uk/)
----

### Financial
1. [IBAN checker](https://www.iban.com/)
2. [Swift Code & BIC Code for all Banks in the World](https://www.theswiftcodes.com/)
3. [BIN - Bank Identification Number checks](https://www.bincodes.com/)
----

### Purchasing
1. [Cisco Transceiver Module Group Compatibility Matrix](https://tmgmatrix.cisco.com/)
2. [Cisco 10GBASE SFP+ Modules Data Sheet](https://www.cisco.com/c/en_in/products/collateral/interfaces-modules/transceiver-modules/data_sheet_c78-455693.html)
3. [Cisco Catalyst 9300 Series Switches Data Sheet](https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-9300-series-switches/nb-06-cat9300-ser-data-sheet-cte-en.html)
4. [Cisco Catalyst 9300 Series Switches Hardware Installation Guide](https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst9300/hardware/install/b_c9300_hig/b_c9300_hig_chapter_011.html)
5. [Cisco Switch Selector](https://www.cisco.com/c/en/us/products/switches/switch-selector.html?guide=enterprise-e)
----

### Mobile Things
1. [εxodus - Android Privacy Apps Audit](https://reports.exodus-privacy.eu.org/en)
----

### Blogs
1. [Decoded Legal](https://decoded.legal/blog/)
2. [Didier Stevens](https://blog.didierstevens.com/)
3. [Hacker On Two Wheels](https://anotherhackerblog.com/)
4. [Hackster](https://www.hackster.io/)
5. [NSA - Limiting Data Exposure](https://media.defense.gov/2020/Aug/04/2002469874/-1/-1/0/CSI_LIMITING_LOCATION_DATA_EXPOSURE_FINAL.PDF?fbclid=IwAR2XL7jEi98haPqqO4xfvq6YUxptVq9d21rMuJMS4HMIMj3PKxNqgs-dfvI)
6. [OlderGeeks.com](https://www.oldergeeks.com/index.html)
7. [samy kamkar - home](https://samy.pl)
8. [Security Basics: XSS Explained. I’m sure you’ve heard of XSS… | by Andrew Long | The Startup | Sep, 2020 | Medium](https://medium.com/swlh/security-basics-xss-explained-3ade8071aaa1)
9. [Spy On Windows Machines Using Metasploit | by Jamie Pegg | Medium](https://medium.com/@jamiepegg/spy-on-windows-machines-using-metasploit-758dbf72bb90)
10. [Understanding Binary and Data Representation with CyberChef](https://cybergibbons.com/reverse-engineering-2/understanding-binary-and-data-representation-with-cyberchef)
11. [Penetration Testing Lab](https://pentestlab.blog/)
12. [FuzzySecurity](http://www.fuzzysecurity.com/index.html)
13. [Networking With Fish](https://www.networkingwithfish.com/)
----

### Bluetooth
1. [MHC Traffic Ltd - Bluetooth Pedestrian and Vehicle Tracking](https://www.mhctraffic.com/bluetooth-tracking)
----

### Ukraine
1. [Attack On Europe: Documenting Equipment Losses During The 2022 Russian Invasion Of Ukraine](https://www.oryxspioenkop.com/2022/02/attack-on-europe-documenting-equipment.html)
2. [Russian War Crimes](https://www.russiancrimes.in.ua/)
3. [Russia's losses](https://www.minusrus.com/en)
4. [2022 Russian invasion of Ukraine](https://en.wikipedia.org/wiki/2022_Russian_invasion_of_Ukraine)
5. [Ukr.warspotting.net](https://ukr.warspotting.net/)
----

### Fun Stuff
1. [Asciiflow](https://asciiflow.com/#/)
2. [b3ta.com challenges](https://b3ta.com/challenge/)
3. [Bongo Cat](https://bongo.cat/)
4. [Bullshit Generator](https://www.bullshitgenerator.com/)
5. [Buzzword Bingo Game - Build Your Own](https://www.buzzwordbingogame.com/byo)
6. [Connect4.gamesolver.org](http://connect4.gamesolver.org/?pos=12226552556272)
7. [Famous TV Shows Floor Plans](https://blog.drawbotics.com/2017/01/31/your-favorite-tv-shows-brought-to-life-with-amazing-3d-floor-plans/)
8. [Find the Invisible Cow](https://findtheinvisiblecow.com)
9. [Font Generator (Fancy text)](https://www.fontgeneratoronline.com/)
10. [FreddieMeter](https://freddiemeter.withyoutube.com)
11. [Hackers Dont Give A Shit](https://romelsan.github.io/hackers-dont-give-a-shit/html/#)
12. [Http.cat](https://http.cat/)
13. [INMATE Classified Penpals in Prison](https://www.inmate.com/)
14. [Octagon Simulator](https://greem.co.uk/octagonsimulator)
15. [Parli-N-Grams](http://parli-n-grams.puntofisso.net)
16. [quakejs.com](http://www.quakejs.com/?utm_source=dlvr.it&utm_medium=twitter)
17. [Radio Garden](http://radio.garden/)
18. [Software Library: MS-DOS Games](https://archive.org/details/softwarelibrary_msdos_games)
19. [Crazy GIFs](https://jn3008.tumblr.com/archive)
20. [Spurious Correlations](https://tylervigen.com/spurious-correlations)
21. [T E X T F I L E S D O T C O M](http://textfiles.com/)
22. [The Splat Calculator](https://www.angio.net/personal/climb/speed.html)
23. [Whythefuckwasibreached.com](https://whythefuckwasibreached.com/)
24. [Yaytext](https://yaytext.com)
25. [Absurd Trolley Problems](https://neal.fun/absurd-trolley-problems/)
26. [ISO - ISO 3103:2019 - Tea — Preparation of liquor for use in sensory tests](https://www.iso.org/standard/73224.html)
27. [Is It Read Only Friday?](https://isitreadonlyfriday.com/)
28. [My Retro TVs](https://myretrotvs.com/)
----

### To Categorise!
1. [Coronavirus Dashboard](https://coronavirus.data.gov.uk)
2. [Digital Forensics and Incident Response : Jai Minton](https://www.jaiminton.com/cheatsheet/DFIR/#)
3. [What2Log - The Sawmill](https://what2log.com/)
4. [Free Cybersecurity Resources | SANS Institute](https://www.sans.org/free?utm_medium=Email&utm_source=HL-NA&utm_content=682027+Free+Resources+Second+Link&utm_campaign=SANS+Free+Resources)
5. [DNS exfiltration of data: step-by-step simple guide](https://hinty.io/devforth/dns-exfiltration-of-data-step-by-step-simple-guide/?goal=0_f50a9c9026-0d30293821-1285120861&mc_cid=0d30293821&mc_eid=8b90efa453)
6. [GitHub - koutto/pi-pwnbox-rogueap: Homemade Pwnbox / Rogue AP based on Raspberry Pi — WiFi Hacking Cheatsheets + MindMap](https://github.com/koutto/pi-pwnbox-rogueap?goal=0_f50a9c9026-fc916fcecd-1285120861&mc_cid=fc916fcecd&mc_eid=8b90efa453)
7. [Linksys Online Simulators](https://ui.linksys.com)
8. [Reddit 🔥 Nature Is Fucking Lit](https://www.reddit.com/r/NatureIsFuckingLit)
9. [SYDI - Document Your Network!](http://sydiproject.com/document-your-network)
10. [Telephones UK](https://telephonesuk.org.uk/)
11. [that_gai_gai Pinterest - Cable Porn](https://www.pinterest.co.uk/that_gai_gai/servers/?invite_code=2063d9b17c304d448f74360661f0b9e6&sender=779615522902788308)
12. [Weberblog.net | IT-Security, Networks, IPv6, DNSSEC, NTP, Monitoring, DIY](https://weberblog.net)
13. [World’s Biggest Data Breaches & Hacks — Information is Beautiful](https://www.informationisbeautiful.net/visualizations/worlds-biggest-data-breaches-hacks)
14. [https://thenewstack.io/the-lost-worlds-of-telnet/](https://thenewstack.io/the-lost-worlds-of-telnet/)
15. [Cheat-Sheets — Malware Archaeology](https://www.malwarearchaeology.com/cheat-sheets)
16. [Device Hunt - PCI and USB](https://devicehunt.com/)
17. [FuzzySecurity](http://www.fuzzysecurity.com/tutorials/16.html)
18. [DeepL Translator](https://www.deepl.com/translator)
19. [GitHub - GoVanguard/list-infosec-encyclopedia: A list of information security related awesome lists and other resources.](https://github.com/GoVanguard/list-infosec-encyclopedia)
20. [Malware-IR-TH-TI-Resources/Malware-IR-TH-TI-Resources.md at main · ShilpeshTrivedi/Malware-IR-TH-TI-Resources · GitHub](https://github.com/ShilpeshTrivedi/Malware-IR-TH-TI-Resources/blob/main/Malware-IR-TH-TI-Resources.md)
21. [https://github.com/archanchoudhury/Davy-Jones-Locker](https://github.com/archanchoudhury/Davy-Jones-Locker)
22. [GitHub - gfek/Real-CyberSecurity-Datasets: Public datasets to help you address various cyber security problems.](https://github.com/gfek/Real-CyberSecurity-Datasets)
23. [GitHub - pascalschulz/Infosec-Resources: just a little treasure chest of stuff I need to watch / read later](https://github.com/pascalschulz/Infosec-Resources)
24. [Socrative Testing](https://www.socrative.com/)
25. [Wiki - List of security hacking incidents](https://en.wikipedia.org/wiki/List_of_security_hacking_incidents)
26. [mitre-attack/attack-datasources](https://github.com/mitre-attack/attack-datasources)
27. [Who Has Access to My Google Stuff](https://www.whohasaccess.com/)
28. [Enigma machine / Tom MacWright](https://observablehq.com/@tmcw/enigma-machine)
29. [Information is Beautiful](https://informationisbeautiful.net/)
30. [Character Biography Generator](https://www.character-generator.org.uk/bio/)
31. [explainshell.com](https://explainshell.com/)
32. [Obey the Bend, Calculating Wire Bend Radius](https://www.truecable.com/blogs/cable-academy/minimum-bend-radius)
33. [Digital Forensic Challenge Images (Datasets)](https://www.ashemery.com/dfir.html)
34. [House of Commons Library](https://commonslibrary.parliament.uk/?utm_source=twitter&utm_medium=social&utm_campaign=cl-promotion_website-211221)
35. [https://www.tunnelsup.com/hash-analyzer/](https://www.tunnelsup.com/hash-analyzer/)
36. [RITA - Black Hills Information Security](https://www.blackhillsinfosec.com/projects/rita/)
37. [CYBERSECURITY JOB HUNTING GUIDE](https://www.cyberhuntingguide.net/)
38. [The Ultimate OSINT Collection](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
39. [Dns.toys](https://www.dns.toys/)
40. [UDP vs. TCP: A Quick Comparison | Caseyis Blog](https://casey.is/blogging/udpvstcp)
41. [Understanding DNS—anatomy of a BIND zone file | Ars Technica](https://arstechnica.com/gadgets/2020/08/understanding-dns-anatomy-of-a-bind-zone-file)
42. [Windows 10 Online Emulator](https://www.onworks.net/runos/create-os.html)
43. [AlternativeTo (Replacement Recommendations)](https://alternativeto.net/)
44. [Free PDF, Video, Image & Other Online Tools](https://tinywow.com/)
45. [Ventoy](https://www.ventoy.net/en/index.html)
46. [Enigma machine](https://observablehq.com/@tmcw/enigma-machine)
47. [the 5-in-1 network admin's cable](http://www.ossmann.com/5-in-1.html)
48. [OSINT Handbook June 2018 (pdf)](https://www.i-intelligence.eu/wp-content/uploads/2018/06/OSINT_Handbook_June-2018_Final.pdf)
49. [Torrent download list](http://iknowwhatyoudownload.com/en/peer/)
----

