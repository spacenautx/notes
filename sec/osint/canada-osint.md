### People Search
1. [RecordsFinder - People Search (CA)](https://recordsfinder.com/canada/)
2. [Canada411 Advanced Search - WhitePages (CA)](https://www.canada411.ca/search/advanced.html)
3. [411directoryassistance.ca](https://www.411directoryassistance.ca/)
4. [Classmates (Canada) - Alumni Lookup](https://www.classmates.com/registration/state.jsp?cType=school&canada=true)
5. [Canada Post - Find a Postal Code](https://www.canadapost-postescanada.ca/info/mc/personal/postalcode/fpc.jsf)
----

### Telephony Lookups
1. [FreeCarrierLookup - Mobile Carrier Lookup](https://freecarrierlookup.com/)
2. [White Pages](https://www.whitepages.com/)
3. [Canada411](https://www.canada411.ca/)
4. [411.ca](https://411.ca/)
5. [Cell Tower Map](https://www.ertyu.org/steven_nikkel/cancellsites.html?lat=52.000000&lng=-97.000000&zoom=4&type=Roadmap&layers=a&pid=0)
6. [Cellular Tower and Signal Map](https://www.cellmapper.net/map)
7. [OpenCelliD - Cell Tower Lookup](https://opencellid.org/#zoom=16&lat=43.65348&lon=-79.38394)
8. [Cellular Coverage Maps Directory](https://www.comparecellular.ca/coverage-maps/)
9. [LocalCallingGuide - Rate Center, Switch, and Interconnect Lookup](https://localcallingguide.com/)
10. [Toll-Free RespOrg Lookup](https://www.800forall.com/SearchWhoOwns.aspx)
11. [NANPA - Carrier Identification Codes (CICs)](https://nationalnanpa.com/number_resource_info/carrier_id_codes.html)
12. [NANPA - NPA (Area) Codes](https://nationalnanpa.com/area_codes/index.html)
13. [CNAC - Canadian Numbering and Dialling Plan](https://cnac.ca/canadian_dial_plan/Canadian_Numbering_and_Dialling_Plan.pdf)
14. [CNAC - Canadian Area Codes](https://cnac.ca/area_code_maps/canadian_area_codes.htm)
15. [CNAC - CO/NXX Status Codes](https://cnac.ca/co_codes/co_code_status.htm)
----

### FEDERAL GOVERNMENT
1. [Security Screening/Clearance Resources](https://www.canada.ca/en/services/defence/nationalsecurity/screening.html)
2. [Security Screening/Clearance Definitions](https://www.canada.ca/en/security-intelligence-service/services/government-security-screening.html)
3. [Security Screening/Clearance Standards](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=28115#appB)
4. [Canada Civil Servants Directory](https://geds-sage.gc.ca/en/GEDS?pgid=002)
5. [ArchivesCanada - Search](https://archivescanada.accesstomemory.ca/actor/browse)
6. [Library and Archives Canada - Search](https://bac-lac.on.worldcat.org/v2)
7. [OPENGOVCA - Open Government Data](https://opengovca.com/)
8. [Elections Canada - Political Donations](https://www.elections.ca/wpapps/WPF/EN/CCS/Index?returntype=1)
----

### Corporate/Charity Affiliations
1. [Federal Corporation Search](https://www.ic.gc.ca/app/scr/cc/CorporationsCanada/fdrlCrpSrch.html)
2. [Federal Charity Search](https://apps.cra-arc.gc.ca/ebci/hacc/srch/pub/dsplyBscSrch?request_locale=en)
3. [NUANS - Corporate Name and Trademarks](https://www.nuans.com/eic/site/075.nsf/eng/home)
4. [MRAS Business Registry Search (BETA)](https://beta.canadasbusinessregistries.ca/search)
5. [OpenCorporates](https://opencorporates.com/)
6. [D&B Hoovers](https://www.dnb.com/products/marketing-sales/dnb-hoovers.html)
7. [ICIJ Offshore Leaks Database](https://offshoreleaks.icij.org/)
8. [OpenOwnership Database](https://register.openownership.org/)
9. [OpenGazettes](http://opengazettes.com/)
10. [OCCRP ID - Government Databases](https://id.occrp.org/databases/)
----

### Intellectual Property and Trademarks
1. [Canadian Trademarks Database](https://www.ic.gc.ca/app/opic-cipo/trdmrks/srch/home?lang=eng)
2. [Canadian Patents Database](https://www.ic.gc.ca/opic-cipo/cpd/eng/search/basic.html?wt_src=cipo-patent-main)
3. [Canadian Copyrights Database](https://www.ic.gc.ca/app/opic-cipo/cpyrghts/dsplySrch.do?lang=eng&wt_src=cipo-cpyrght-main&wt_cxt=toptask)
4. [Canadian Industrial Designs Database](https://www.ic.gc.ca/app/opic-cipo/id/bscSrch.do?lang=eng&wt_src=cipo-id-main&wt_cxt=toptask)
----

### Legal Databases
1. [Canada - Judicial Structure Map](https://www.justice.gc.ca/eng/csj-sjc/just/07.html)
2. [Supreme Court of Canada (SCC) - Decisions and Appeals Search](https://scc-csc.lexum.com/scc-csc/en/nav.do)
3. [Federal Court of Appeal - Decision Search](https://decisions.fca-caf.gc.ca/fca-caf/en/d/s/index.do?col=53)
4. [Federal Court - Cases, Decisions, and Procedural Charts](https://www.fct-cf.gc.ca/en/home)
5. [Tax Court of Canada - Decision Search](https://decision.tcc-cci.gc.ca/tcc-cci/en/nav.do)
6. [Tax Court of Canada - File Search](https://www.tcc-cci.gc.ca/en/pages/find-a-court-file)
7. [Court Martial Appeal Court of Canada (Military Appeals Court)](https://www.cmac-cacm.ca/en/home)
8. [Office of the Chief Military Judge (Military Courts)](https://www.canada.ca/en/chief-military-judge.html)
9. [Canadian Legal Information Institute (CanLII)](https://www.canlii.org/en/)
10. [World Legal Information Institute (WorldLII)](http://worldlii.org/)
11. [Bankrupcty/Insolvency - Records Search](https://www.ic.gc.ca/app/scr/bsf-osb/ins/login.html?lang=eng)
12. [Bankrupcty/Insolvency - Unclaimed Dividends Search](https://www.ic.gc.ca/app/scr/osb-bsf/ufd/search.html)
13. [Bankruptcy/Insolvency - CCAA Records List](https://www.ic.gc.ca/eic/site/bsf-osb.nsf/eng/h_br02281.html)
----

### FOSINT - FINTRAC
1. [SEDAR Filings - Canada's EDGAR](https://www.thebalance.com/how-to-use-canada-s-sedar-1979205)
2. [Canadian Securities Administrators (CSA) - Canada's SEC](https://www.securities-administrators.ca/)
3. [CSA - National Registration Search (Securities License)](https://www.securities-administrators.ca/nrs/nrsearchprep.aspx)
4. [CSA - Disciplined List Search](https://www.securities-administrators.ca/disciplinedpersons.aspx?id=74)
5. [CSA - Cease Trade Orders (CTO) Search](https://cto-iov.csa-acvm.ca/SearchArticles.asp?Instance=101&Form=1)
6. [SEDAR - Company Filings](https://www.sedar.com/search/search_form_pc_en.htm)
7. [SEDAR - Investment Fund Filings](https://www.sedar.com/search/search_form_mf_en.htm)
8. [FINTRAC - Money Services Business (MSB) Search](https://www10.fintrac-canafe.gc.ca/msb-esm/public/msb-search/search-by-name/)
9. [BIN Codes - BIN, CC, IP Checker/Validator](https://www.bincodes.com/creditcard-checker/)
10. [BINLIST - BIN/IIN Lookup API](https://binlist.net/)
11. [IIROC - Search](https://www.iiroc.ca/search?query=)
----

### British Columbia
1. [The Courts of British Columbia](https://www.bccourts.ca/index.aspx)
2. [Civil Court Cases and Filings (BC)](https://justice.gov.bc.ca/cso/esearch/civil/partySearch.do)
3. [Appeal Court Cases and Filings (BC)](https://justice.gov.bc.ca/cso/esearch/appeal/caseSearch.do;jsessionid=xVJA8w4nQipYWUUdohvQyD9y.0e1ba411-53d7-3b0d-8e0a-d5b7d8cafbab)
4. [Criminal Court Cases and Charges (BC)](https://justice.gov.bc.ca/cso/esearch/criminal/partySearch.do)
5. [Business Registry Search (BC)](https://www.bcregistry.ca/business/auth/home/decide-business)
6. [CDSBC Dental Surgeons License Lookup (BC)](https://www.cdsbc.org/home/registrant-lookup#/lookup/dentists)
7. [BC Traffic Ticket Lookup](https://justice.gov.bc.ca/cso/esearch/criminal/partySearch.do)
8. [ARELLO - Real Estate Licensee Verification Database](https://www.arello.com/)
9. [BC Assessment - Property Assessment Search](https://www.bcassessment.ca/)
----

### Alberta
1. [Business License Search (AB)](https://www.servicealberta.ca/find-if-business-is-licenced.cfm)
2. [Alberta Power Providers Directory](https://poweroutage.us/area/state/alberta)
3. [Alberta.ca - Municipal Property Assessments](https://www.alberta.ca/municipal-property-assessment-overview.aspx)
----

### Saskatchewan
1. [Business Registry Search (SK)](https://www.isc.ca/CorporateRegistry/Pages/default.aspx)
2. [Saskatchewan Power Providers Directory](https://poweroutage.us/area/state/saskatchewan)
3. [ARELLO - Real Estate Licensee Verification Database](https://www.arello.com/)
4. [SAMA - Municipal Property Assessments](https://www.sama.sk.ca/)
----

### Manitoba
1. [Business Registry Search (MB)](https://companiesoffice.gov.mb.ca/online.html)
2. [Manitoba Power Providers Directory](https://poweroutage.us/area/state/manitoba)
3. [ARELLO - Real Estate Licensee Verification Database](https://www.arello.com/)
4. [Manitoba Courts - Search](https://web43.gov.mb.ca/Registry/NameSearch)
----

### Ontario
1. [Driver's Licence Check (ON)](https://www.dlc.rus.mto.gov.on.ca/dlc/enter-details)
2. [Ontario MTO - IDF Curve Lookup](https://www.eng.uwaterloo.ca/~dprincz/mto_site/terms.shtml)
3. [Ontario POA Office - Court Lookup](https://www.ontario.ca/page/check-status-traffic-tickets-and-fines-online-or-request-meeting-resolve-your-case)
4. [Toronto POA Offence - Court Lookup](https://secure.toronto.ca/CourtCaseLookUp/button.jsf)
5. [ESC Corporate Services - Public Records Search (ON) **PAID](https://www.eservicecorp.ca/public-record-search-services/)
6. [CyberBahn Name Searching (ON) **PAID](https://cyberbahngroup.ca/entity-management-solutions/name-searching/)
7. [Ontario Energy/Electricity Provider Map](https://www.ieso.ca/localContent/ontarioenergymap/index.html)
8. [Ontario Power Providers Directory](https://poweroutage.us/area/state/ontario)
9. [HydroOne Outage Map](https://d8bkcndcv6jca.cloudfront.net/)
10. [Toronto Hydro Outage Map](https://www.torontohydro.com/outage-map)
11. [OSC - Investor Warnings and Alerts](https://www.osc.ca/en/investors/investor-warnings-and-alerts)
12. [MPAC - Municipal Property Assessments](https://www.mpac.ca/en)
13. [Collection, Debt, Loan, Consumer Reporting, and Lenders Registry](https://www.ontario.ca/page/search-business-licence-registration-or-appointment)
----

### Quebec
1. [Business Registry Search (QC)](https://www.quebec.ca/en/businesses-and-self-employed-workers/public-registries)
2. [Provincial Land Registry (QC)](https://appli.mern.gouv.qc.ca/infolot/)
3. [Civil Records Registry (marriages, divorces, birth, etc) (QC)](https://numerique.banq.qc.ca/resultats)
4. [Provincial Construction License (RBQ) Registry (QC)](https://www.rbq.gouv.qc.ca/services-en-ligne/licence/registre-des-detenteurs-de-licence.html)
5. [Provincial Liquor Permit Registry (QC)](https://www.racj.gouv.qc.ca/en/registres-publics/alcool.html)
6. [Financial Professionals License Registry (QC)](https://lautorite.qc.ca/grand-public/registres/registre-des-entreprises-et-des-individus-autorises-a-exercer)
7. [Quebec Power Providers Directory](https://poweroutage.us/area/state/quebec)
8. [Transportation Company Registry (QC)](https://www.pes.ctq.gouv.qc.ca/pes/faces/dossierclient/recherche.jsp)
9. [Court Decisions Archive (QC)](https://soquij.qc.ca/fr)
10. [Open Government Data (QC)](https://www.donneesquebec.ca/organisation/gouvernement-du-quebec/)
11. [Driver's Licence Check (QC)](https://saaq.gouv.qc.ca/services-en-ligne/citoyens/verifier-validite-permis-conduire/)
----

### New Brunswick
1. [Business Registry Search (NB)](https://www.pxw2.snb.ca/card_online/cardsearch.aspx)
2. [NB Courts](https://www1.gnb.ca/nota/Default.aspx?lang=en-US)
----

### Nova Scotia
1. [Business Registry Search (NS)](https://beta.novascotia.ca/programs-and-services/registry-joint-stock-companies)
----

### Prince Edward Island (PEI)
1. [Business Registry Search (PEI)](https://www.princeedwardisland.ca/en/feature/pei-business-corporate-registry-original#/service/LegacyBusiness/LegacyBusinessSearch)
----

### Newfoundland & Labrador
1. [Business Registry Search (NL)](https://cado.eservices.gov.nl.ca/CADOInternet/Main.aspx)
2. [Municipal Assessment Agency (MAA) - Property Assessment Search](https://maa.ca/assessments/assessment-search/)
----

### Northwest Territories
1. [Business Registry Search (NWT)](https://www.justice.gov.nt.ca/app/cros-rsel/search)
----

### Nunavut Territory
1. [Business Registry Search (NU)](https://nni.gov.nu.ca/business/search/name)
----

### Yukon Territory
1. [Business Registry Search (YK)](https://ycor-reey.gov.yk.ca/search)
----

### Public Safety
1. [Canadian Police Information Centre (CPIC) - Stolen Items Database](https://www.cpic-cipc.ca/index-eng.htm)
2. [Background Checks - Sterling Backcheck ***PAID](https://www.sterlingbackcheck.ca/backcheck-canada/)
3. [Public Safety Canada - Listed Terrorist Entities](https://www.publicsafety.gc.ca/cnt/ntnl-scrt/cntr-trrrsm/lstd-ntts/crrnt-lstd-ntts-en.aspx)
4. [FireSmoke - Wildland fire weather and smoke reporting real-time DB](https://firesmoke.ca)
----

### Transporation
1. [Criminal Record Checks - Royal Canadian Mounted Police (RCMP)](https://www.rcmp-grc.gc.ca/en/criminal-record-checks)
2. [Transport Canada - Flight Crew Licensing Search](https://wwwapps.tc.gc.ca/saf-sec-sur/2/CAS-SAC/apfcls.aspx)
3. [SearchSystems.net - Canada Public Records Search](https://publicrecords.searchsystems.net/Canada_Free_Public_Records/)
4. [Transport Canada - Air Operator Search](https://wwwapps.tc.gc.ca/saf-sec-sur/2/CAS-SAC/olsrles.aspx?lang=eng)
5. [Transport Canada CCARCS - Current Registered Aircraft Search](https://wwwapps.tc.gc.ca/Saf-Sec-Sur/2/CCARCS-RIACC/RchAvc.aspx)
6. [Transport Canada CCARCS - Historical Registered Aircraft Search](https://wwwapps.tc.gc.ca/Saf-Sec-Sur/2/CCARCS-RIACC/RchHs.aspx)
7. [Transport Canada - Vessel Registration Query System (Maritime)](https://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/vrqs-srib/eng/vessel-registrations)
8. [Nauticapedia - Vessel Database Query (Maritime)](https://www.nauticapedia.ca/dbase/Query/dbsubmit_Vessel.php)
9. [Armalytics - Canadian Firearms Reference Table (RCMP)](https://www.armalytics.ca/)
----

### Sex Offenders
1. [Sex Offender Information Registration Act](https://laws-lois.justice.gc.ca/eng/acts/S-8.7/FullText.html)
2. [National Sex Offender Registry (NSOR) | Royal Canadian Mounted Police](https://www.rcmp-grc.gc.ca/en/privacy-impact-assessment-national-sex-offender-registry-nsor?wbdisable=true)
3. [Ontario Sex Offender Registry](https://www.csc-scc.gc.ca/atip/007006-0018-en.shtml)
4. [Sex Offender Registry - Province of British Columbia](https://www2.gov.bc.ca/gov/content/justice/criminal-justice/sex-offender-registry)
----

