### Shooting and Editing tools
1. [lumen5.com](https://lumen5.com/)
2. [iMovie](https://www.apple.com/imovie/)
3. [Splice Video Editor by Bending Spoons](http://spliceapp.com/)
4. [Keepvid](https://keepvid.com/)
5. [Lightworks: The professional editor for everyone](https://www.lwks.com/)
6. [OpenShot Video Editor](https://www.openshot.org/)
7. [Shotcut - Home](https://shotcut.org/)
8. [HitFilm Express: Free editing & VFX software](https://fxhome.com/hitfilm-express)
9. [DaVinci Resolve 16](https://www.blackmagicdesign.com/products/davinciresolve/)
10. [3d animation software | Learn how to make animations | Moviestorm for filmmakers](https://www.moviestorm.co.uk/)
11. [OpenToonz](https://opentoonz.github.io/e/)
12. [‎Shot Designer on the App Store](https://apps.apple.com/us/app/shot-designer/id556342711)
13. [Zubtitle | Automatically Add Captions to Any Video Effortlessly](https://zubtitle.com/)
14. [Www.transcriptionstar.com](https://www.transcriptionstar.com/free-captioning-tool/)
15. [Altru- (paid employer brand video service)](https://www.altrulabs.com/)
16. [www.loom.com/](https://www.loom.com/)
----

### Video Hosting Platforms
1. [Loom](https://chrome.google.com/webstore/detail/loom-video-recorder-scree/liecbddmkiiihnedobmlmillhodjkdmb?hl=en-US)
2. [Community Video Archive](https://archive.org/details/opensource_movies)
3. [YouTube](https://www.youtube.com/)
4. [Vimeo](https://vimeo.com/)
5. [Dailymotion](https://www.dailymotion.com/us)
6. [Wix Video Overview | WIX App Market | Wix.comWix.comFacebookTwitterYouTubePinterestInstagramLinkedIn](https://www.wix.com/app-market/wix-video/overview)
7. [Video Hosting for Business](https://sproutvideo.com/)
8. [Online Video Platform](https://www.brightcove.com/en/online-video-platform)
----

### Video Editing Communities and Forums
1. [NewTubers](https://www.reddit.com/r/NewTubers/)
2. [r/videography](https://www.reddit.com/r/videography/)
3. [Video Editing (non professionals)](https://www.reddit.com/r/VideoEditing/)
4. [Video Editing Community](https://discordapp.com/invite/9WhN8bp)
5. [The DV Info Net Forum](http://www.dvinfo.net/forum/)
6. [Video Editing Forums: Digital Director](https://www.videoforums.co.uk/)
----

### Royalty Free Video and Images and music
1. [Reddit.com/r/NatureStockVideos](https://www.reddit.com/r/NatureStockVideos/)
2. [Download Aerials Stock Footage, Sfx, Texture](https://www.cutestockfootage.com/search/aerials)
3. [Finding Footage Video Searching - Foundboxesvideos](http://www.findingfootage.com/videogridengine/index.php/foundboxesvideos/index/538)
4. [Stock Photos, Stock Photography, and Royalty Free Images](https://www.shutterstock.com/photos)
5. [Audio Library — Music for content creators](https://www.youtube.com/channel/UCht8qITGkBvXKsR1Byln-wA)
6. [Vlog No Copyright Music](https://www.youtube.com/channel/UCEickjZj99-JJIU8_IJ7J-Q)
7. [NoCopyrightSounds](https://www.youtube.com/channel/UC_aEa8K-EOJ3D6gOs7HcyNg)
8. [Stock Footage for Free](https://www.stockfootageforfree.com/)
9. [Pixabay](https://pixabay.com/)
10. [Download Free Stock Footage](https://www.videvo.net/)
11. [Free HD Video Clips & Stock Video Footage at Videezy!](https://www.videezy.com/)
12. [www.lifeofvids.com/](https://www.lifeofvids.com/)
13. [Distill](http://www.wedistill.io/)
14. [Video Archives - SplitShire](https://www.splitshire.com/category/video-2/)
15. [Totally Free Stock Footage Downloads HD Video](https://www.vidsplay.com/)
----

### SourceCon Blogs about video
1. [Integrating Video into Outreach - Greg Hawkes](https://www.sourcecon.com/integrating-video-into-outreach/)
2. [I finally created my own video messaging for sourcing- Erin Mathew](https://www.sourcecon.com/i-finally-created-my-own-video-messaging-for-sourcing-heres-what-happened/)
3. [DIY Sourcing like those Cheesy Make your own Slime Videos but way Nerdier- Greg Hawkes](https://www.sourcecon.com/diy-sourcing-like-those-cheesy-make-your-own-slime-videos-but-way-nerdier/)
4. [Using Videos to Source - Josef Kadlec](https://www.sourcecon.com/video-sourcing/)
5. [Evolve of Die- Arron Daniels](https://www.sourcecon.com/evolve-or-die/)
----

### Tutorials and youtube channels
1. [Creative COW - Tutorials and Articles](https://www.creativecow.net/)
2. [TechGumbo](https://www.youtube.com/channel/UCaSM4GqhbaVmRT7fmmFmR1w)
3. [Cinecom.net](https://www.youtube.com/user/YapperDesign/videos)
4. [DSLRguide](https://www.youtube.com/channel/UCzQ1L-wzA_1qmLf49ey9iTQ)
5. [Movavi Vlog
 - YouTube](https://www.youtube.com/channel/UCCv2d4YxZ4vrFuZou0W9L4A)
6. [Kriscoart](https://www.youtube.com/user/KriscoartProductions/videos)
7. [Corel VideoStudio
 - YouTube](https://www.youtube.com/user/VideoStudioPro)
8. [Learn How To Edit Stuff
 - YouTube](https://www.youtube.com/channel/UCsXMr0Gg0dHrMD_I8KlYmwA)
9. [PinnacleStudioLife
 - YouTube](https://www.youtube.com/user/PinnacleStudioLife)
----

### Script/storyboard/planning tools
1. [Storyboard That: The World's Best FREE Online Storyboard Creator](https://www.storyboardthat.com/)
2. [Plot](https://theplot.io/)
3. [StudioBinder: Free Shot List & Storyboard Software for Creatives](https://www.studiobinder.com/shot-list-storyboard/)
4. [WriterDuet | Professional Screenwriting Software You'll Love](https://writerduet.com/?link=QJF56RRE)
5. [Hollywood Camera Work :: Causality Story Sequencer :: Main](https://www.hollywoodcamerawork.com/causality.html)
6. [Page 2 Stage](http://page2stage.sourceforge.net/)
----

### Lighting and Equipment resources
1. [Shooting Solo: A Low Budget Filmmaker's Equipment List](https://www.desktop-documentaries.com/low-budget-filmmakers-equipment-list.html)
2. [Home - VER](https://www.ver.com/)
3. [Video Equipment Rental | Video Camera Rental | BorrowLenses](https://www.borrowlenses.com/video)
4. [Rent Cameras, Lenses, and Video Gear • LensProToGo](https://www.lensprotogo.com/)
5. [10 Best Video Equipment Rental Options for Filmmakers](https://www.studiobinder.com/blog/video-equipment-rental/)
----

### Useful Chrome Extensions
1. [Ultimate Video Saver - Chrome Web Store](https://chrome.google.com/webstore/detail/ultimate-video-saver/afkpfjljjhhonjehpkmgonimjjgaheap?hl=en-US)
2. [Video Downloader professional - Chrome Web Store](https://chrome.google.com/webstore/detail/video-downloader-professi/jpaglkhbmbmhlnpnehlffkgaaapoicnk?hl=en-US)
3. [The Flash Video Dоwnlоader - Chrome Web Store](https://chrome.google.com/webstore/detail/the-flash-video-d%D0%BEwnl%D0%BEade/amjcoehkcacocffpmhnefgoeanepjfkf?hl=en-US)
4. [Pro Mode for YouTube Video Editor - Chrome Web Store](https://chrome.google.com/webstore/detail/pro-mode-for-youtube-vide/aenmbapdfjdkanhfppdmmdipakgacanp?hl=en-US)
5. [Video editor OpenShot online - Chrome Web Store](https://chrome.google.com/webstore/detail/video-editor-openshot-onl/kdfinbdncekfhibpbnkjedmdofkjghjj?hl=en-US)
6. [Video editor online Avidemux with SeaMonkey - Chrome Web Store](https://chrome.google.com/webstore/detail/video-editor-online-avide/mncaenllngkhkjfecnelhohdfnaaiond?hl=en-US)
7. [Subtitles For YouTube - Chrome Web Store](https://chrome.google.com/webstore/detail/subtitles-for-youtube/oanhbddbfkjaphdibnebkklpplclomal?hl=en-US)
8. [DIY Captions Launcher for YouTube - Chrome Web Store](https://chrome.google.com/webstore/detail/diy-captions-launcher-for/nfmohppoonnamhhboojkgffnjmbekckl?hl=en-US)
9. [YouTube Video Effects And Audio Enhancer - Chrome Web Store](https://chrome.google.com/webstore/detail/youtube-video-effects-and/jdjldbengpgdcfkljfdmakdgmfpneldd?hl=en-US)
10. [Movie maker MovieStudio video editor - Chrome Web Store](https://chrome.google.com/webstore/detail/movie-maker-moviestudio-v/ohgkmilcibaoempgifldidkidnbkbeii?hl=en-US)
11. [ScreenLife - Chrome Web Store](https://chrome.google.com/webstore/detail/screenlife/gfokfhnimibpcgkgcpblbajenencilob?hl=en-US)
12. [Movie Maker 10 - Chrome Web Store](https://chrome.google.com/webstore/detail/movie-maker-10/pekjnkmjnceiahmhajolbjhlcbagdhde?hl=en-US)
13. [WeVideo Movie & Video Editor - Chrome Web Store](https://chrome.google.com/webstore/detail/wevideo-movie-video-edito/hhgogcoghcnacfjfdamiicmapmoglkdo?hl=en-US)
14. [Video2Edit - Video Editor & Video Converter - Chrome Web Store](https://chrome.google.com/webstore/detail/video2edit-video-editor-v/djanbckaomofhalpklkcbimkmlaocddd?hl=en-US)
15. [IG Downloader - Chrome Web Store](https://chrome.google.com/webstore/detail/ig-downloader/daeljdgmllhgmbdkpgnaojldjkdgkbjg?hl=en-US)
16. [Bass Booster - Chrome Web Store](https://chrome.google.com/webstore/detail/bass-booster/ekeainfdlkfjgomgdoiamdlheeolffdi?hl=en-US)
17. [Screencastify](https://chrome.google.com/webstore/detail/screencastify-screen-vide/mmeijimgabbpbgpdklnllpncmdofkcpn)
18. [ScreenLife](https://chrome.google.com/webstore/detail/screenlife/gfokfhnimibpcgkgcpblbajenencilob?hl=en-US)
----

