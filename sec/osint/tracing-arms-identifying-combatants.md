### Copy of Go back to main dashboard
1. [Go Back (click here)](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
----

### 1. Tracing arms & ammunition
1. [Weapons of ISIS](http://www.conflictarm.com/reports/weapons-of-the-islamic-state/)
2. [Small Arms Survey - Home](http://www.smallarmssurvey.org/)
3. [SIPRI Arms Transfers Database](https://www.sipri.org/databases/armstransfers)
4. [A-Z Databases](http://www.natolibguides.info/az.php)
5. [The Weapons ID Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
6. [Search 3d models - sketchfab](https://sketchfab.com/search?sort_by=-pertinence&type=models)
7. [Arms embargoes](https://www.sipri.org/databases/embargoes)
8. [Identification and Support Cards](http://www.smallarmssurvey.org/weapons-and-markets/tools/id-cards.html)
9. [Ammunition-Tracing-Kit.pdf](http://www.smallarmssurvey.org/fileadmin/docs/D-Book-series/book-06-ATK/SAS-Ammunition-Tracing-Kit.pdf)
10. [Loading Mapping Arms Data (MAD) 1992-2014. Please wait as application loads.
                    Note: The graphics used in this visualization are not supported by older versions of 			Internet Explorer. It is best viewed using an up-to-date version of Chrome, Firefox or Internet Explorer (IE11).
		N.B. If you are using Internet Explorer, this must NOT be set to run in 'Compatibility Mode'! (Tools > Compatibility View Settings)](http://nisatapps.prio.org/armsglobe/index.php)
11. [2013 Army Weapons Systems](https://fas.org/man/dod-101/sys/land/wsh2013/index.html)
12. [Global Firearms Holdings](http://www.smallarmssurvey.org/weapons-and-markets/tools/global-firearms-holdings.html)
13. [UEMS tools](http://www.smallarmssurvey.org/weapons-and-markets/stockpiles/unplanned-explosions-at-munitions-sites/uems-tools.html)
14. [HotGunz Stolen Gun Search Results](https://www.hotgunz.com/search.php)
15. [Small Arms Survey - Interactive Map & Charts on Armed ViolenceInteractive Map & Charts on Armed Violence](http://www.smallarmssurvey.org/tools/interactive-map-charts-on-armed-violence.html)
16. [Small Arms Survey - PODSPODS](http://www.smallarmssurvey.org/mpome/pods-dynamic-map.html)
17. [Home](https://bulletpicker.com/index.html)
18. [Forgotten Weapons](https://www.forgottenweapons.com/)
19. [Armyrecognition.com](https://www.armyrecognition.com/)
20. [Qlik Sense](https://webgate.ec.europa.eu/eeasqap/sense/app/75fd8e6e-68ac-42dd-a078-f616633118bb/sheet/24ca368f-a36e-4cdb-94c6-00596b50c5ba/state/analysis)
21. [https://conflict.id/home](https://conflict.id/home)
22. [SEESAC Developed SALW Tools - SEESAC](https://www.seesac.org/SEESAC-Developed-SALW-Tools/)
23. [Att-assistance.org](https://att-assistance.org/)
24. [Conflict Armament Research](https://www.conflictarm.com/)
25. [Airrecognition.com](https://www.airrecognition.com/)
26. [Navyrecognition.com](https://www.navyrecognition.com/)
27. [MilitaryRussia.Ru — отечественная военная техника (после 1945г.) | Статьи](http://militaryrussia.ru/blog/)
28. [Wisconsinproject.org](https://www.wisconsinproject.org/)
29. [Iran Watch](https://www.iranwatch.org/)
30. [Riskreport.org](https://www.riskreport.org/)
31. [Patarames: May 2020](https://patarames.blogspot.com/2020/05/)
32. [Aboyandhis.blog](https://www.aboyandhis.blog/)
33. [Military Database](https://www.scramble.nl/database/military)
34. [HotGunz Stolen Gun Search Results](https://www.hotgunz.com/search.php)
35. [Firearms Guide](https://www.firearmsguide.com/)
36. [Data & Statistics](https://www.atf.gov/resource-center/data-statistics)
37. [ODIN](https://odin.tradoc.army.mil/)
38. [Military and Conflict GIJN Database  - Google Drive](https://docs.google.com/spreadsheets/u/0/d/1wiIVKdvn8QSBQ1LGB9kiGrB28SYFblbZ7uM4uKSWFfU/htmlview)
----

### 2. Identifying combatant affiliation
1. [How to Digitally Verify Combatant Affiliation in Middle East Conflicts - bellingcat](https://www.bellingcat.com/resources/how-tos/2018/07/09/digitally-verify-middle-east-conflicts/)
2. [bellingcat - Syrian Opposition Factions in the Syrian Civil War - bellingcat](https://www.bellingcat.com/news/mena/2016/08/13/syrian-opposition-factions-in-the-syrian-civil-war/)
3. [Camopedia](http://camopedia.org/index.php?title=Main_Page)
4. [Camouflageindex.camouflagesociety.org/index-2.html](http://camouflageindex.camouflagesociety.org/index-2.html)
5. [Uniforminsignia.org](http://www.uniforminsignia.org/)
6. [List of comparative military ranks](https://en.wikipedia.org/wiki/List_of_comparative_military_ranks)
7. [All Badges site for collectors](http://allbadges.net/en)
8. [Military Wiki](https://military.wikia.org/wiki/Main_Page)
9. [World military equipment](https://www.armyrecognition.com/individual_page/world_military_equipment.html)
----

### 3. Tracing equipment
1. [Army Guide](http://www.army-guide.com/)
2. [Armyrecognition.com](https://www.armyrecognition.com/)
3. [Military Factory](https://www.militaryfactory.com/)
4. [Military-today.com](https://www.military-today.com/)
5. [Anatomy of a Tank in World of Tanks | AllGamers](https://ag.hyperxgaming.com/article/5256/anatomy-of-a-tank-in-world-of-tanks)
6. [Anatomy of a ship](https://nmssanctuaries.blob.core.windows.net/sanctuaries-prod/media/archive/education/pdfs/shipparts.pdf)
7. [Parts of Airplane](https://www.grc.nasa.gov/www/k-12/airplane/airplane.html)
8. [Military Wiki](https://military.wikia.org/wiki/Main_Page)
----

### 4. Intelligence Sources
1. [Publicintelligence.net](https://publicintelligence.net/)
2. [Worldview.stratfor.com](https://worldview.stratfor.com)
----

