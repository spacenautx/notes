### People Search
1. [Australian Person Lookup](https://personlookup.com.au/)
2. [Australian Public Records](https://australiapublicrecord.com/people-finder/)
3. [Australian Census Records](https://guides.slv.vic.gov.au/earlycensus/findpeople)
4. [National Archives Of Australia](https://recordsearch.naa.gov.au)
5. [Missing Persons Australia](https://missingpersons.gov.au/view-all-profiles)
6. [White Pages® (AU)](https://www.whitepages.com.au/residential)
7. [People Search Australia](http://www.peoplesearch.com.au)
8. [Ancestry Australia](https://www.ancestry.com.au/)
9. [Australian Military Service Records](http://www.naa.gov.au/collection/explore/defence/service-records/)
10. [Researching a person | Australian War Memorial](https://www.awm.gov.au/collection/understanding-the-memorials-collection/researching-a-person)
----

### Business Search
1. [ABN Lookup](https://abr.business.gov.au/)
2. [ASIC Connect](https://connectonline.asic.gov.au/)
3. [White Pages (AU)](https://www.whitepages.com.au/)
4. [Yellow Pages (AU)](https://www.yellowpages.com.au/)
5. [Reverse Australia](https://www.reverseaustralia.com/)
6. [SearchFrog](https://searchfrog.com.au/)
7. [True Local](https://www.truelocal.com.au/)
----

### Obituary Search
1. [The Ryerson Index](http://ryersonindex.org/search.php)
2. [Herald Sun MyTributes](https://www.heraldsun.com.au/tributes)
3. [Australian Cemeteries Index](https://www.austcemindex.com/)
4. [Rookwood Cemetary](http://www.rookwoodcemetery.com.au/)
----

### Property Search
1. [Planning Alerts](https://www.planningalerts.org.au/)
2. [OnTheHouse](https://www.onthehouse.com.au/)
3. [NSW Planning Portal](https://www.planningportal.nsw.gov.au/spatialviewer/#/find-a-property/address)
----

### Public Records
1. [Public Records Australia](https://www.publicrecords.com.au/)
2. [Unpaid Wages Australia](https://www.fairwork.gov.au/how-we-will-help/helping-the-community/search-for-unpaid-wages)
3. [NSW Unclaimed Money](https://www.apps09.revenue.nsw.gov.au/erevenue/ucm/ucm_search.php)
4. [VIC Unclaimed Money](https://www.e-business.sro.vic.gov.au/eBusiness/UnclaimedMoneys/#/)
5. [SA Unclaimed Money](https://www.treasury.sa.gov.au/Our-services/unclaimed-money/search-for-unclaimed-money)
6. [WA Unclaimed Money](https://unclaimedmonies.treasury.wa.gov.au/ums/)
7. [ACT Unclaimed Money](https://www.ptg.act.gov.au/unclaimed-money)
8. [Australian Court Data](https://www.courtdata.com.au/)
9. [WA CheckMyCrime](ttps://onlineservices.police.wa.gov.au/Home/CheckMyCrime)
10. [VIC Wanted Persons](https://www.crimestoppersvic.com.au/help-solve-crime/wanted-persons)
11. [Court Judgements QLD](https://www.sclqld.org.au/caselaw)
12. [Birth, Death and Marriage Records](https://www.nla.gov.au/research-guides/australian-birth-death-and-marriage-records)
----

### Archives & Newspapers
1. [National Archives Australia](http://National Archives Australia.com)
2. [NSW State Archives](https://www.records.nsw.gov.au/)
3. [VIC Government Open Data](https://www.data.vic.gov.au/)
4. [Public Data Sets](https://data.gov.au/)
5. [Research Data Australia](https://researchdata.ands.org.au/search/#!/q=/class=collection/)
6. [National Archive of Australia](https://trove.nla.gov.au/)
7. [National Archive of Australia Websites](https://trove.nla.gov.au/website)
8. [Our Digital Island](https://linctas.ent.sirsidynix.net.au/client/en_AU/library/search/results?qf=LOCAL_FORMAT%09Format+(Specific)%09archived+website%09archived+website)
9. [Newspapers.com](https://www.newspapers.com/)
----

### Vehicle Search
1. [NSW License Plate Search](https://www.service.nsw.gov.au/transaction/check-vehicle-registration)
2. [VIC License Plate Search](https://www.vicroads.vic.gov.au/registration/buy-sell-or-transfer-a-vehicle/check-vehicle-registration)
3. [QLD License Plate Search](https://www.service.transport.qld.gov.au/checkrego/public/Welcome.xhtml)
4. [WA License Plate Search](https://www.transport.wa.gov.au/dotdirect/dotdirect.asp)
5. [SA License Plate Search](https://www.sa.gov.au/topics/driving-and-transport/registration/vehicle-registration/check-a-vehicles-registration-expiry-date)
6. [NT License Plate Search](https://nt.gov.au/driving/rego/check,-renew-or-transfer-your-registration/rego-check)
7. [ACT License Plate Search](https://rego.act.gov.au/regosoawicket/public/reg/FindRegistrationPage)
8. [TAS License Plate Search](https://www.transport.tas.gov.au/MRSWebInterface/public/regoLookup/registrationLookup.jsf)
----

### Registered Professions
1. [LicensedTrades](https://www.licensedtrades.com.au/)
2. [OneGov Mobile | Public Register](https://www.onegov.nsw.gov.au/publicregister/#/publicregister/categories)
3. [Tax Practitioners (1)](https://www.tpb.gov.au/registrations_search)
4. [Tax Practitioners (2)](https://www.cpaaustralia.com.au/FindACpa/Locate.mvc/Index)
5. [Finance Registers](https://connectonline.asic.gov.au/RegistrySearch/faces/landing/ProfessionalRegisters.jspx)
6. [Medical Board of Australia - Register of practitioners](https://www.medicalboard.gov.au/~/link.aspx?_id=b19bed3ca4d34c86a4e692db96f77eb8&_z=z)
7. [Australian Health Practitioner Regulation Agency - Register of practitioners](https://www.ahpra.gov.au/Registration/Registers-of-Practitioners.aspx)
8. [Family Court Judgements](http://www6.austlii.edu.au/cgi-bin/viewdb/au/cases/cth/FamCA/)
9. [Conveyancers licence check | NSW Fair Trading](https://www.fairtrading.nsw.gov.au/help-centre/online-tools/conveyancers-licence-check)
10. [ARC License Check](https://www.lookforthetick.com.au/licence-check)
11. [Tasmania License Check](https://occupationallicensing.justice.tas.gov.au/Search/onlinesearch.aspx)
12. [Occupational License Search (WA)](https://www.commerce.wa.gov.au/consumer-protection/consumer-protection-licence-and-registration-search)
13. [Building Professionals (ACT)](https://www.accesscanberra.act.gov.au/app/answers/detail/a_id/2918/~/licensed-building-professionals)
14. [Builders Register (VIC)](https://consumer.etoolbox.buildingcommission.com.au/Pages/Search.aspx)
15. [Tradespeople License Check (NSW)](https://www.onegov.nsw.gov.au/publicregister/#/publicregister/search/Trades)
16. [Real Estate Agents Register (VIC)](https://registers.consumer.vic.gov.au/EAsearch)
----

### Maps & Aerial Images
1. [NationalMap Australia](https://nationalmap.gov.au/)
2. [Aerial Maps of Sydney (1943)](https://maps.six.nsw.gov.au/)
3. [ACT Maps and Aerial Images](https://actmapi.act.gov.au/)
----

### Live Camera Feeds
1. [Traffic Live NSW](http://m.livetraffic.rta.nsw.gov.au/CameraList.aspx?r=ALL)
2. [QLD Traffic Cameras](https://qldtraffic.qld.gov.au/cameras.html)
3. [Traffic Cameras](https://straya.io/traffic-cameras/)
4. [Live View of Sydney Harbour](https://webcamsydney.com/)
5. [Live View of Quay West Sydney](https://www.quaywestsuitessydney.com.au/webcam-en.html)
6. [Sydney Cameras Live](https://www.webcams.travel/webcam/1179853135-Weather-Sydney-Harbour%2C-Australia-Sydney-CBD)
7. [Live Survelliance Australia](https://www.insecam.org/en/bycountry/AU/)
8. [EarthCam Australia](https://www.earthcam.com/search/ft_search.php?term=australia)
----

