### Articles / Tutorials (Articles / Tutoriels)
1. [i-intelligence.eu/ Handbook PDF](https://i-intelligence.eu/uploads/public-documents/OSINT_Handbook_June-2018_Final.pdf)
2. [Osint FR](https://osint-fr.net/articles/)
3. [Articles | OpenFacto](https://openfacto.fr/articles/)
4. [Slides.nothing2hide.org](https://slides.nothing2hide.org/)
5. [SecurityTrails](https://securitytrails.com/blog/osint-tools)
6. [Trouble Fake](https://troublefake.blogspot.com/)
7. [Facebook Tips](https://osintcurio.us/2020/04/02/facebook-tips/)
8. [PhoneInfoga - Advanced OSINT framework for phone numbers](https://hakin9.org/phoneinfoga-advanced-information-gathering-osint-framework-for-phone-numbers/)
9. [Beautiful Soup: Build a Web Scraper With Python – Real Python](https://realpython.com/beautiful-soup-web-scraper-python/)
10. [How to scrape a website that requires login with Python – Isaac Vidas – This is not for you](https://kazuar.github.io/scraping-tutorial/)
11. [Linux Backdoor RedXOR Likely Operated by Chinese Nation-State](https://www.intezer.com/blog/malware-analysis/new-linux-backdoor-redxor-likely-operated-by-chinese-nation-state-actor/)
12. [http://How to receive SMS otp verification online in 5 min. Guide.com](http://How to receive SMS otp verification online in 5 min. Guide.com)
----

### Tool lists (Listes d'outils)
1. [OSINT Essentials](https://www.osintessentials.com/)
2. [Bellingcat's Online Investigation Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/mobilebasic)
3. [SANS Faculty Free Tools Index](https://www.sans.org/img/free-faculty-tools.pdf?msc=sans-free-lp)
4. [QWARIE: UK-OSINT](https://www.uk-osint.net/)
5. [OSINT Tools](https://www.osinttechniques.com/osint-tools.html)
6. [MIDASearch](https://midasearch.org/)
7. [Best OSINT Tools & Software for Passive & Active Recon & Security!](https://www.pcwdld.com/osint-tools-and-software)
8. [inteltechniques OSINT Packet 2019](https://inteltechniques.com/JE/OSINT_Packet_2019.pdf)
9. [KitPloit - OSINT Tools](https://www.kitploit.com/search/label/OSINT)
10. [OSINT-SPY](https://www.kitploit.com/2019/02/osint-spy-search-using-osint-open.html)
11. [JohnHammond/ctf-katana](https://github.com/JohnHammond/ctf-katana)
----

### Guides
1. [OSINT Framework](https://osintframework.com)
2. [OSINT YOGA](https://yoga.osint.ninja)
3. [WebBreacher/osinttools](https://github.com/WebBreacher/osinttools)
4. [inteltechniques.com/data/Email.png](https://inteltechniques.com/data/Email.png)
5. [inteltechniques.com/data/Domain.png](https://inteltechniques.com/data/Domain.png)
6. [inteltechniques.com/data/Real%20Name.png](https://inteltechniques.com/data/Real%20Name.png)
7. [inteltechniques.com/data/Telephone.png](https://inteltechniques.com/data/Telephone.png)
8. [inteltechniques.com/data/location.png](https://inteltechniques.com/data/location.png)
9. [inteltechniques.com/data/Username.png](https://inteltechniques.com/data/Username.png)
10. [What is OSINT? - SearchLight](https://searchlight.community/definitions/what-is-osint/)
11. [A Guide To Social Media Intelligence Gathering (SOCMINT)](https://www.secjuice.com/social-media-intelligence-socmint/)
12. [Guide To Using Reverse Image Search For Investigations](https://www.bellingcat.com/resources/how-tos/2019/12/26/guide-to-using-reverse-image-search-for-investigations/)
13. [Tips and Tricks on Reverse Image Searches – We are OSINTCurio.us](https://osintcurio.us/2020/04/12/tips-and-tricks-on-reverse-image-searches/)
14. [Using FFMPEG To Grab Stills and Audio For OSINT – NixIntel](https://nixintel.info/osint-tools/using-ffmpeg-to-grab-stills-and-audio-for-osint/)
----

### Search Engines Syntax
1. [Hack The Box Blog - What is Google Dorking?](https://www.hackthebox.com/blog/What-Is-Google-Dorking)
2. [Google Search Operators: The Complete List (42 Advanced Operators)](https://ahrefs.com/blog/google-advanced-search-operators/)
3. [Offensive Security’s Exploit Database Archive](https://www.exploit-db.com/google-hacking-database)
4. [Listing of a number of useful Google dorks.](https://gist.github.com/stevenswafford/393c6ec7b5375d5e8cdc)
5. [Refine web searches](https://support.google.com/websearch/answer/2466433)
6. [Google Hacking](https://pentest-tools.com/information-gathering/google-hacking)
7. [DuckDuckGo Search Syntax](https://help.duckduckgo.com/duckduckgo-help-pages/results/syntax/)
8. [Advanced search options](https://help.bing.microsoft.com/apex/index/18/en-US/10002)
9. [madhavmehndiratta/dorkScanner](https://github.com/madhavmehndiratta/dorkScanner)
----

### Twitter
1. [twintproject/twint](https://github.com/twintproject/twint)
2. [Twitter Advanced Search](https://twitter.com/search-advanced)
3. [Tweetdeck](https://tweetdeck.twitter.com/)
4. [Tweetanalyzer.net](https://tweetanalyzer.net/)
5. [Hashatit](https://www.hashatit.com)
6. [Twitter Trending Hashtags and Topics](https://www.trendsmap.com/)
7. [Twitter Analytics by Foller.me](https://foller.me/)
8. [Socialbearing](https://socialbearing.com/)
9. [Sleeping Time](http://sleepingtime.org/)
10. [tinfoleak](https://tinfoleak.com/)
11. [Top 10 Twitter Tracking Tools | Reshift Media Inc.](https://www.reshiftmedia.com/top-10-twitter-tracking-tools/)
12. [burrrd](https://burrrd.com/)
13. [Socialbearing](https://socialbearing.com/)
14. [Twchat.com](http://twchat.com/)
15. [TwitScoop.com](https://www.twitscoop.com/)
16. [Twiends](https://twiends.com/)
17. [Find Twitter Followers](http://searchfortweets1.com/)
18. [Twitter Shadowban Test](https://shadowban.eu/)
19. [Twitjobseek.com](http://twitjobseek.com/)
20. [Twitter search exporter and keyword monitor](https://birdiq.net/twitter-search)
21. [Twitter search tool – Twitter Investigations – Aware Online Academy](http://www.aware-online.com/en/osint-tools/twitter-search-tool/)
22. [Tweeplesearch.com](https://tweeplesearch.com/?bio=&name=&location=&frmn=&frmx=&flmn=&flmx=&is_hash_search=true&is_url_search=false&is_profile=true&type=profiles&sort_by=followers_count&page=1&lang=&order=desc)
23. [Itrended.com](https://itrended.com/)
24. [Tweeplers](https://www.tweeplers.com/?cc=WORLD)
25. [Seekatweet v2](https://www.seekatweet.com/)
26. [Tweet Mapper :: Index](https://keitharm.me/projects/tweet/)
27. [geosocial footprint](http://geosocialfootprint.com/)
28. [App.teachingprivacy.org](http://app.teachingprivacy.org/)
29. [The one million tweet map](https://onemilliontweetmap.com/?center=25.505,-0.09&zoom=2&search=&timeStep=0&timeSelector=0&hashtag1=&hashtag2=sad&sidebar=yes&hashtagBattle=0&timeRange=0&timeRange=25&heatmap=0&sun=0&cluster=1)
30. [Twitter Trending Hashtags and Topics](https://www.trendsmap.com/)
31. [Tweepsmap.com](https://tweepsmap.com/)
----

### Reddit
1. [Reddit Search](https://camas.github.io/reddit-search/)
2. [Reddit search tool - Reddit investigations - Aware Online Academy](http://www.aware-online.com/en/osint-tools/reddit-search-tool/)
3. [Reddit Investigator](https://www.redditinvestigator.com/)
4. [Finddit](https://viralharia.github.io/finddit/)
5. [Findit-search.herokuapp.com](https://findit-search.herokuapp.com/)
6. [Reddit OSINT Techniques](https://www.secjuice.com/reddit-osint-techniques/)
7. [JosephLai241/URS](https://github.com/JosephLai241/URS)
8. [similar reddits](https://anvaka.github.io/redsim/)
9. [reddit.guide](https://reddit.guide/)
10. [i.rarchives.com/](https://i.rarchives.com/)
11. [Search Reddit Comments by User](https://redditcommentsearch.com/)
12. [Redective](http://redective.com/)
13. [Redditery.com](http://www.redditery.com/)
14. [Redditbrowser.com](https://redditbrowser.com/)
15. [redditlist](http://redditlist.com/)
16. [redditP](https://www.redditp.com/)
----

### LinkedIn
1. [leapsecurity/InSpy](https://github.com/leapsecurity/InSpy)
2. [vysecurity/LinkedInt](https://github.com/vysecurity/LinkedInt)
3. [dchrastil/ScrapedIn](https://github.com/dchrastil/ScrapedIn)
4. [FREE LinkedIn Xray Search Tool — Recruitment Geek](https://recruitmentgeek.com/tools/linkedin/)
5. [free people search tool](https://freepeoplesearchtool.com/)
6. [LinkedIn X-Ray Search Tool |  SourcinglabServer Error](https://sourcinglab.io/search/linkedin)
7. [LinkedIn Xray Search Tool](https://www.anantha.co.in/linkedin/)
8. [Trevisan LinkedIn Boolean Search Generator](http://trevisansocial.com/linkedtool/)
----

### Facebook
1. [Facebook Directory People](https://www.facebook.com/directory/people/)
2. [DumpItBlue download | SourceForge.net](https://sourceforge.net/projects/extractface/)
3. [sqren/fb-sleep-stats](https://github.com/sqren/fb-sleep-stats)
4. [Find my Facebook ID](https://lookup-id.com/)
5. [Tools](https://intelx.io/tools?tab=facebook)
6. [Facebook Search – Find Posts and Person Profiles](https://www.social-searcher.com/facebook-search/)
7. [Fb-search.com](https://fb-search.com/)
8. [Facebook Search Tool - Een effectieve OSINT Tool - Aware Online](http://www.aware-online.com/osint-tools/facebook-search-tool/)
9. [Find my Facebook ID](https://lookup-id.com/)
10. [Gofindwho.com](https://gofindwho.com/#ggsearch)
11. [On Facebook Searching — Plessas Experts Network](https://plessas.net/blog/2019/8/14/facebook-searching)
----

### Instagram
1. [Picodash](https://www.picodash.com/)
2. [Websta](https://web.stagram.com/)
3. [Find Instagram User ID](https://codeofaninja.com/tools/find-instagram-user-id)
4. [Websta](https://web.stagram.com/)
5. [sometag](https://sometag.org/)
----

### Persons (Personnes)
1. [Free People Search](https://www.peoplefinder.com/)
2. [FamilyTree](https://familytreenow.com)
3. [Spokeo](https://www.spokeo.com)
4. [Thats them](https://thatsthem.com)
5. [Online Background Checks](https://www.beenverified.com)
6. [fastpeoplesearch](https://www.fastpeoplesearch.com)
7. [TruePeopleSearch](https://www.truepeoplesearch.com)
8. [Яндекс](https://people.yandex.ru/)
9. [Intelius](https://www.intelius.com/)
10. [PeekYou](https://www.peekyou.com/)
11. [People Search](https://www.instantcheckmate.com/)
12. [Who's A Rat](https://whosarat.com/)
----

### Sexual Crimes
1. [NSOPW](https://www.nsopw.gov/)
----

### Usernames (Noms d'utilisateurs)
1. [sherlock-project/sherlock](https://github.com/sherlock-project/sherlock)
2. [KnowEm](https://knowem.com)
3. [Username Search](https://usersearch.org)
4. [Instant Username Search](https://instantusername.com/)
5. [Namechk](https://namechk.com/)
6. [NameCheckup](https://namecheckup.com/)
7. [WhatsMyName Web](https://whatsmyname.app/)
8. [WebBreacher/WhatsMyName](https://github.com/WebBreacher/WhatsMyName)
----

### Companies (Entreprises)
1. [https://us-info.com/en/usa](https://us-info.com/en/usa)
2. [Business Entities :: California Secretary of State](https://www.sos.ca.gov/business-programs/business-entities/)
3. [OpenCorporates](https://opencorporates.com/)
4. [Corporation Wiki](https://www.corporationwiki.com/)
5. [Companies House](https://www.gov.uk/government/organisations/companies-house)
6. [LittleSis](https://littlesis.org/)
7. [Track Hedge Funds Using 13F Filings](https://whalewisdom.com/)
8. [Crosslinked - LinkedIn Enumeration Tool in Kali Linux - GeeksforGeeks](https://www.geeksforgeeks.org/crosslinked-linkedin-enumeration-tool-in-kali-linux/)
9. [inspy | Kali Linux Tools](https://www.kali.org/tools/inspy/)
10. [SocialPwned](https://kalilinuxtutorials.com/socialpwned/)
11. [HM Revenue & Customs](https://www.gov.uk/government/organisations/hm-revenue-customs)
12. [Partnerpoint.com](https://www.partnerpoint.com/)
----

### Compromised databases (Bases de données compromises)
1. [Have I been pwned?](https://haveibeenpwned.com)
2. [Find out if you’ve been part of a data breach](https://monitor.firefox.com/)
3. [Have I been sold?](https://haveibeensold.app)
4. [DeHashed — #FreeThePassword](https://dehashed.com/)
5. [burnermail.io](https://burnermail.io)
6. [https://www.exploit-db.com/ghdb/5255](https://www.exploit-db.com/ghdb/5255)
----

### Website analysis (Analyse de site web)
1. [URL and website scanner](https://urlscan.io/)
2. [Intelligence X](https://intelx.io/)
3. [Who.is](https://who.is/)
4. [Web technologies used by a site](https://w3techs.com/sites)
5. [What's that site running?](https://sitereport.netcraft.com/)
6. [BuiltWith Technology Lookup](https://builtwith.com/)
7. [Scamdoc.com](https://www.scamdoc.com/)
8. [Signal-Arnaques](https://www.signal-arnaques.com)
9. [ViewDNS.info](https://viewdns.info/)
10. [https://talosintelligence.com/reputation](https://talosintelligence.com/reputation)
----

### Web Scraping
1. [Download ParseHub | Our Quickstart Guide](https://parsehub.com/quickstart)
----

### Email
1. [Hunter](https://hunter.io/)
2. [Email Permutator | Metric Sparrow Toolkit](http://metricsparrow.com/toolkit/email-permutator/#)
3. [Proofy](https://proofy.io/)
4. [Free email address validator](https://verifalia.com/validate-email)
5. [khast3x/h8mail](https://github.com/khast3x/h8mail)
6. [m4ll0k/Infoga](https://github.com/m4ll0k/Infoga)
7. [Hashcast.axur.com](https://hashcast.axur.com/)
8. [Spybot Identity Monitor](https://www.safer-networking.org/products/spybot-identity-monitor/)
9. [GHunt](https://github.com/mxrch/GHunt)
----

### Phone (Téléphone)
1. [Who Called Me](https://www.whocalledme.com/)
2. [PrivacyStar](https://privacystar.com/index.html)
3. [Getcontact](https://www.getcontact.com/)
4. [EveryCaller](http://www.everycaller.com/)
5. [Truecaller.com](https://www.truecaller.com/)
----

### DTMF (dual tone multi frequency / double tonalité multifréquence)
1. [What is DTMF (dual tone multi frequency)? - Definition from WhatIs.com](https://searchnetworking.techtarget.com/definition/DTMF#:~:text=DTMF%20(dual%20tone%20multi%20frequency)%20is%20the%20signal%20to%20the,(%22pulse%22)%20dialling.)
2. [Dialabc.com | Tools for Phone Numbers](http://dialabc.com/)
3. [T9 Converter (Text Message) - Phone SMS Decoder, Encoder, Translator](https://www.dcode.fr/t9-cipher)
----

### "Swiss knives" ("Couteaux Suisses")
1. [am0nt31r0/OSINT-Search](https://github.com/am0nt31r0/OSINT-Search)
2. [GitHub - saeeddhqan/Maryam: Maryam : Open-source Intelligence(OSINT) Framework](https://github.com/saeeddhqan/Maryam)
3. [Install from Scratch - REMnux Documentation](https://docs.remnux.org/install-distro/install-from-scratch)
4. [FLARE VM: The Windows Malware Analysis Distribution You’ve Always Needed! « FLARE VM: The Windows Malware Analysis Distribution You’ve Always Needed!](https://www.fireeye.com/blog/threat-research/2017/07/flare-vm-the-windows-malware.html)
----

### Source code search (Recherche de code)
1. [searchcode](https://searchcode.com/)
----

### Bug Bounty Tools (Outils de chasseur de bugs)
1. [How To Get Started as a Bug Bounty Hunter - Hack Ware News](https://hackwarenews.com/how-to-get-started-as-a-bug-bounty-hunter/)
----

### Games (Jeux)
1. [GeoGuessr](https://www.geoguessr.com/)
2. [OsintDojo.github.io](https://www.osintdojo.com/)
----

### Food
1. [Open Food Facts](https://world.openfoodfacts.org/)
2. [INAO - Institut National de l'Origine et de la Qualité](https://www.inao.gouv.fr/)
----

### Reverse image search (Recherche inversée d'images )
1. [TinEye](https://tineye.com/)
2. [Google Images](https://images.google.com/)
3. [Yandex.Images: search for images on the internet, search by image](https://yandex.com/images/)
4. [Freeze frame](http://www.bing.com/images/discover?FORM=ILPMFT)
5. [Baidu](http://image.baidu.com/)
6. [Findclone](https://findclone.ru/)
7. [Findmevk.com (VKontakte)](https://findmevk.com/)
8. [Karma Decay (Reddit)](http://karmadecay.com/)
9. [RevEye Reverse Image Search](https://chrome.google.com/webstore/detail/reveye-reverse-image-sear/keaaclcjhehbbapnphnmpiklalfhelgf?hl=en)
10. [Image Search Options – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/en-US/firefox/addon/image-search-options/)
11. [Pictriev - Searching faces on the web](http://pictriev.com/)
----

### Image analysis (Analyse d'images)
1. [Forensically, free online photo forensics tools - 29a.ch](https://29a.ch/photo-forensics/#forensic-magnifier)
2. [FotoForensics](http://fotoforensics.com/)
3. [ebemunk/phoenix](https://github.com/ebemunk/phoenix)
4. [Ghiro](http://www.getghiro.org/)
----

### Blur processing (Traitement des flous)
1. [SmartDeblur](http://smartdeblur.net/)
2. [Blurity!](https://www.blurity.com/)
3. [Neural network image super-resolution and enhancement](https://letsenhance.io/)
----

### EXIF
1. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
2. [ExifTool by Phil Harvey](https://exiftool.org/)
3. [EXIF Data Viewer](http://exifdata.com/)
4. [Viewexifdata.com](http://www.viewexifdata.com/)
5. [Exifpurge.com](http://www.exifpurge.com/)
6. [View Exif data online, remove Exif online](https://www.verexif.com/en/)
----

### Photo/video downloads (Téléchargement photos / vidéos)
1. [Youtube-dl.org](https://youtube-dl.org/)
2. [DownloadHelper](https://www.downloadhelper.net/)
3. [Download All Images – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/en-US/firefox/addon/save-all-images-webextension/)
4. [The Stream Detector – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/en-US/firefox/addon/hls-stream-detector/)
----

### Maps (Cartes)
1. [Google Maps](http://maps.google.com/)
2. [OpenStreetMap](https://www.openstreetmap.org/)
3. [Snap Map](https://map.snapchat.com/)
4. [WiGLE: Wireless Network Mapping](https://wigle.net/)
5. [Mapillary](https://www.mapillary.com/)
----

### Webcams
1. [Webcamtaxi Search Engine of Live World Webcams](https://www.webcamtaxi.com/en/search.html?searchword=paris&searchphrase=all)
2. [https://www.exploit-db.com/ghdb/4508](https://www.exploit-db.com/ghdb/4508)
----

### Weather (Météo)
1. [OpenWeatherMap](https://openweathermap.org/)
2. [Weather History & Data Archive](https://www.wunderground.com/history)
3. [World Temperatures — Weather Around The World](https://www.timeanddate.com/weather/)
4. [AccuWeather](https://www.accuweather.com)
----

### Miscellaneous (Divers)
1. [Webinaire investigation en ligne](https://nothing2hide.org/fr/2020-05-20-webinaire-investigation-en-ligne/)
2. [List of fact-checking websites - Wikipedia](https://en.wikipedia.org/wiki/List_of_fact-checking_websites)
3. [Use of social network websites in investigations](https://en.wikipedia.org/wiki/Use_of_social_network_websites_in_investigations)
----

### Steganography ()
1. [SilentEye - Steganography is yours](https://achorein.github.io/silenteye/)
----

### Planes
1. [FlightAware](https://flightaware.com/)
----

