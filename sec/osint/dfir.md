### How to get started?
1. [3MinMax Series with Kevin Ripa](https://www.youtube.com/playlist?list=PLfouvuAjspTqyfrgYO76VOwvVqK1AsMwK)
2. [Category:Digital-Forensics - aldeid](https://www.aldeid.com/wiki/Category:Digital-Forensics)
3. [DFIR - The Definitive Compendium Project](https://aboutdfir.com/)
4. [Paralus LLC](https://paralus.co/)
5. [So you want to break into the field of Digital Forensics… | Smarter Forensics](https://smarterforensics.com/2016/08/so-you-want-to-break-into-the-field-of-digital-forensics/)
6. [The Easy Way to Learn DFIR - Brett's Ramblings](https://www.brettshavers.com/brett-s-blog/entry/the-easy-way-to-learn-dfir)
7. [What is Digital forensics?](https://www.open.edu/openlearn/science-maths-technology/digital-forensics/content-section-1)
----

### Reverse Engineering
1. [Introduction to Reverse Engineering with Ghidra](https://hackaday.io/course/172292-introduction-to-reverse-engineering-with-ghidra)
2. [Nightmare](https://guyinatuxedo.github.io/?mc_cid=d676bd61c6&mc_eid=c18a7def31)
3. [RE for Beginners](https://www.begin.re/assignment-1)
4. [Reverse Engineering 101](https://malwareunicorn.org/workshops/re101.html#0)
5. [Reverse Engineering Challenge | Lucky Numbers | Medium](https://jaybailey216.medium.com/reverse-engineering-challenge-lucky-numbers-84e342fd588b)
----

### Contact Me
1. [Ian the Innovator (@InnovatorIan)](https://twitter.com/InnovatorIan)
2. [More Infosec links (aimed at n00bs)](https://start.me/p/ADwq1n/getting-started-in-information-security)
----

### DFIR Cheatsheets
1. [Cheat Sheet for Analyzing Malicious Documents](https://sansorg.egnyte.com/dl/IQ3GhaH868/?)
2. [Malware Analysis and Reverse-Engineering Cheat Sheet](https://sansorg.egnyte.com/dl/GpsbKAhkQo/?)
3. [SIFT Workstation](https://sansorg.egnyte.com/dl/rcC7AFiUr0/?)
----

### Labs/Challengs/CTFs
1. [CTFtime.org / All about CTF (Capture The Flag)](https://ctftime.org/ctfs)
2. [FORENSIC CHALLENGES](https://www.amanhardikar.com/mindmaps/ForensicChallenges.html)
3. [Forensic Challenges - DFRWS](https://dfrws.org/forensic-challenges/)
4. [Hacked | CyberDefenders® | Blue Team CTF
        Challenges](https://cyberdefenders.org/labs/71)
5. [https://info-sec-box.ctfd.io/](https://info-sec-box.ctfd.io/)
6. [LGDroid | CyberDefenders® | Blue Team CTF
        Challenges](https://cyberdefenders.org/labs/69)
7. [The CFReDS Project](https://www.cfreds.nist.gov/)
----

### File Signatures
1. [List of file signatures](https://en.wikipedia.org/wiki/List_of_file_signatures)
2. [File Signature Database:: All File Signatures.](https://filesignatures.net/index.php?page=all)
3. [File Signatures](https://www.garykessler.net/library/file_sigs.html)
----

### DFIR Related Lists
1. [cugu/awesome-forensics](https://github.com/Cugu/awesome-forensics)
2. [paralax/awesome-honeypots](https://github.com/paralax/awesome-honeypots)
3. https://github.com/Drew-Alleman/dystopia
4. [COMPREHENSIVE DFIR Cheatsheet from Jai Minton](https://www.jaiminton.com/cheatsheet/DFIR/)
----

### Blogs
1. [This Week In 4n6](https://thisweekin4n6.com/)
2. [Threatintel.de](https://www.threatintel.de/)
3. [Discreterecovery.dk](https://www.discreterecovery.dk/)
----

### DFIR Courses
1. [Autopsy Training](https://dfir-training.basistech.com/)
2. [Computer Forensics](https://www.edx.org/course/computer-forensics)
3. [Digital Forensics | hackers-arise](https://www.hackers-arise.com/forensics)
4. [Digital Forensics and Incident Response (DFIR) Training, Courses, Certifications and Tools | SANS Institute](https://www.sans.org/digital-forensics-incident-response/)
5. [ENISA DFIR training](https://www.enisa.europa.eu/topics/trainings-for-cybersecurity-specialists/online-training-material/technical-operational)
6. [Free Course Content Archives - eForensics](https://eforensicsmag.com/category/free-course-content/)
7. [Introduction to Windows Forensics](https://www.youtube.com/playlist?list=PLlv3b9B16ZadqDQH0lTRO4kqn2P1g9Mve)
8. [Training – DFIR Diva](https://dfirdiva.com/training/)
9. [Reverse-Engineering Course from Kevin Thomas](https://github.com/mytechnotalent/Reverse-Engineering)
----

### Mobile Forensics
1. [den4uk/andriller](https://github.com/den4uk/andriller)
2. [abrignoni/ALEAPP](https://github.com/abrignoni/ALEAPP)
3. [GitHub - mac4n6/iOS-Frequent-Locations-Dumper: Dump the iOS Frequent Location binary plist files](https://github.com/mac4n6/iOS-Frequent-Locations-Dumper)
----

### Tools
1. [Ghiro - Image forensics](http://www.getghiro.org/)
2. [FOCA - find metadata](https://github.com/ElevenPaths/FOCA)
3. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
4. [Ghidra](https://ghidra-sre.org/)
5. [Ghidra Cheatsheet](https://htmlpreview.github.io/?https://github.com/NationalSecurityAgency/ghidra/blob/stable/GhidraDocs/CheatSheet.html)
6. [Sonic Visualiser - Audio Analysis](https://www.sonicvisualiser.org/)
----
### triage
1. https://github.com/travisfoley/dfirtriage
-----
### lab
https://github.com/StevenDias33/Lab-DFIR-SOC
