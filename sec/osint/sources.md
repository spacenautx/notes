### Privacy
1. [Center for Humane Technology](https://www.humanetech.com/)
2. [How to Destroy Surveillance Capitalism, a New Book by Cory Doctorow | OneZero](https://onezero.medium.com/how-to-destroy-surveillance-capitalism-8135e6744d59)
3. [Whistleblower Says Facebook Ignored Global Political ManipulationBuzzFeed News HomeMenu IconTwitterFacebookCopyBuzzFeed News LogoCloseTwitterFacebookCopyTwitterFacebookCopyFacebookTwitterInstagramBuzzFeed News HomeBuzzFeedClose](https://www.buzzfeednews.com/article/craigsilverman/facebook-ignore-political-manipulation-whistleblower-memo)
4. [Welt-Mädchenbericht 2020 zu digitaler Gewalt gegen Mädchen und Frauen | Plan International Deutschland](https://www.plan.de/presse/welt-maedchenbericht-2020-zu-digitaler-gewalt-gegen-maedchen-und-frauen.html)
----

### People Search Engines Non-US
1. [fastpeoplesearch.com](http://fastpeoplesearch.com/)
2. [Free People Search Engines](http://www.skipease.com/)
3. [lullar OSIT](http://lullar.com)
4. [nuwber](http://nuwber.com/)
5. [peoplecheck.de Personensuchmaschine](https://peoplecheck.de/)
6. [Person / Info zu Name](http://www.yasni.de/)
7. [Pipl - People Search OSIT](http://pipl.com/)
8. [truepeoplesearch.com](http://truepeoplesearch.com/)
9. [WebMii - Search for people](http://webmii.com/)
10. [Yatedo Semantic People Search](http://www.yatedo.com/)
----

### Import / Export Trade Data
1. [ImpExp.com - Import & Export Data](http://www.impexp.com/#!)
2. [ITA International Trade Administration US dep. Commerce (USA)](http://tse.export.gov/)
3. [USITC - United States International Trade Commission (USA)](http://dataweb.usitc.gov/)
4. [Datamyne - Import export data | International trade statistics](http://www.datamyne.com/)
----

### Annual Reports | Jahresbericht
1. [Annual Reports](http://www.annualreports.com/)
2. [ReportLinker.com](http://www.reportlinker.com/)
3. [G.U.B. Analyse](http://www.dfi-analyse.de/)
----

### Court Judgements Law | Gerichtsurteile |Recht
1. [BfJ Gewerbezentralregister](https://www.bundesjustizamt.de/DE/Themen/Buergerdienste/GZR/GZR_node.html)
2. [Bundesverfassungsgericht](https://www.bundesverfassungsgericht.de/SiteGlobals/Forms/Suche/Entscheidungensuche_Formular.html;jsessionid=66AD89A8FA65262836D0F4934184B7C5.2_cid392?language_=de)
3. [Casetext US - Free](https://casetext.com/)
4. [Deutsche Rechtsprechung Online](http://www.drsp.net/)
5. [Gerichte (Entscheidungen und Pressemitteilungen)](http://www.rewi.hu-berlin.de/ri/rs/)
6. [Gesetze in Deutschland - Buzer.de](http://www.buzer.de/)
7. [Home.heinonline.org/ US](https://home.heinonline.org/)
8. [Justizportal des Bundes und der Länder](http://www.justiz.de/onlinedienste/rechtsprechung/index.php)
9. [lexetius.com/](http://lexetius.com/?0)
10. [lexxion.eu](http://www.lexxion.de/)
11. [openJur](https://openjur.de/)
12. [Rechtsprechung NRW](https://www.justiz.nrw/BS/nrwe2/index.php)
13. [Startseite   |   juris Das Rechtsportal](http://www.juris.de)
14. [Urteile aus dem Strafrecht](http://www.rechtsindex.de/strafrecht)
15. [Urteile im Volltext finden > Gerichtsurteile kostenlos](http://www.urteilfinden.de/)
16. [Urteile suchen](http://www.rechtsindex.de/urteile)
17. [Urteile, Gerichtsurteile online finden](http://www.juraforum.de/urteile/)
18. [World Legal Information Institute (WorldLII)](http://www.worldlii.org/)
19. [GHlobal Regulation Database Coverage: 1,823,663 laws and counting](https://www.global-regulation.com/coverage.php)
----

### Staatsanwaltschaften
1. [Staatsanwaltschaften](http://www.deutschejustiz.de/staatsanwaltschaften.html)
2. [Suchen nach ORT! StA und Gerichtsverzeichnis](http://www.justizadressen.nrw.de/og.php)
3. [Zentrales Staatsanwaltschaftliches Verfahrensregister – Wikipedia](https://de.wikipedia.org/wiki/Zentrales_Staatsanwaltschaftliches_Verfahrensregister)
----

### Patent Search
1. [DPMAregister](https://register.dpma.de/DPMAregister/pat/einsteiger)
2. [DPMA Deutsches Patent und Markenamt](https://www.dpma.de/)
3. [EUIPO Amt der Europäischen Union für geistiges Eigentum](https://euipo.europa.eu/ohimportal/de/search-availability)
4. [Ipr-hub.eu IPR SME HELPDESK BrandIP Property Rights](http://www.ipr-hub.eu/)
5. [Markenchk.de](http://www.markenchk.de/)
6. [Willkommen im PIZnet](http://www.piznet.de/)
7. [Google Patent Suche 1](https://www.google.de/?tbm=pts&hl=en&gws_rd=cr,ssl&ei=udOSVOa9AY6xaYCkgdAJ)
8. [Google Patent Suche 2](https://www.google.de/search?tbm=pts&q=patent&gws_rd=cr&ei=0WU8VtSDD8aPsgHfsYXIAg)
9. [Google Patents Patent Suche 3](https://patents.google.com/)
10. [WIPO Hague Express](http://www.wipo.int/designdb/hague/en/)
11. [Smart Search](https://register.epo.org/regviewer?lng=de)
12. [euipo.europa.eu/eSearch/](https://euipo.europa.eu/eSearch/)
13. [United States Patent and Trademark Office](https://www.uspto.gov/)
14. [Search for a patent](https://www.gov.uk/search-for-patent)
15. [Japan Platform for Patent Information](https://www.j-platpat.inpit.go.jp/web/all/top/BTmTopEnglishPage)
16. [International Trademark Search Free Engine](http://trademark-search.marcaria.com/)
17. [Patent Genius](http://www.patentgenius.com/)
18. [CC Search](https://search.creativecommons.org/)
19. [Espacenet
		
			
			
				
					Advanced search](http://worldwide.espacenet.com/advancedSearch?locale=en_EP)
20. [Intellectual Property Office](https://www.ipo.gov.uk/p-pj)
21. [Instituto Nacional da Propriedade Industrial (BRAZIL)](http://www.inpi.gov.br/)
----

### Professional  Associations (GER) | Kammern und Verbände
1. [Kammern und Verbände](https://www.deutschland.de/de/topic/wirtschaft/globalisierung-welthandel/kammern-und-verbaende)
2. [Architektenkammern in Deutschland](http://www.architektenkammern.net/)
3. [Bundesrechtsanwaltskammer ~ Startseite](http://www.brak.de/)
4. [Handwerkskammer](http://www.handwerkskammer.de/)
5. [Wirtschaftsprüferkammer (WPK)](https://www.wpk.de/)
6. [WPK Berufsregister](https://www.wpk.de/register/)
7. [G-ba-qualitaetsberichte.de](https://www.g-ba-qualitaetsberichte.de/#/search)
8. [Informationsfreiheitsanfragen - FragDenStaat Behörden](https://fragdenstaat.de/behoerden/)
9. [Qualitätsberichte der Krankenhäuser     - Gemeinsamer BundesausschussLogo: Gemeinsamer Bundesausschuss](https://www.g-ba.de/themen/qualitaetssicherung/datenerhebung-zur-qualitaetssicherung/datenerhebung-qualitaetsbericht/)
----

### Presentations
1. [Slideshare](http://www.slideshare.net)
2. [authorSTREAM](http://www.authorstream.com)
3. [www.slidefinder.net](http://www.slidefinder.net)
4. [Slideworld.com](http://www.slideworld.com)
----

### Leak Database
1. [balkanleaks.eu/](https://balkanleaks.eu/)
2. [Buckets (Scrapes Amazon Buckets)](https://buckets.grayhatwarfare.com/)
3. [BuckHacker - The Bucket Search Engine for Hackers =-](https://thebuckhacker.com/)
4. [Cryptome](https://cryptome.org/)
5. [Distributed Denial of Secrets](https://ddosecrets.com/wiki/Distributed_Denial_of_Secrets)
6. [Ghostproject leaked Data and Password](https://ghostproject.fr/)
7. [GitHub Tool:Leak Scraper](https://github.com/Acceis/leakScraper)
8. [Google Custom Search Leaked Search](https://cse.google.com/cse/publicurl?cx=009154535448372285253:gytgv2xhyv0)
9. [ICIJ Offshore Leaks / Panama Papers Database](https://offshoreleaks.icij.org/)
10. [Leakedsource](https://www.leakedsource.com/)
11. [OffshoreAlert](https://www.offshorealert.com/home.aspx)
12. [Organized Crime Reporting and Corruption Project (OCCR) DATA](https://data.occrp.org/)
13. [site:cryptome.org "name" Custom Search](https://www.google.com/search?q=site:cryptome.org+%22name%22)
14. [Snusbase Database Search Engine](https://snusbase.com)
15. [TOOL: globaleaks.org/](https://www.globaleaks.org/)
16. [Weleakinfo.com/ Raw Data for Payment)](https://weleakinfo.com/)
17. [WikiLeaks](http://wikileaks.org/)
18. [WikiLeaks Full-text search](https://search.wikileaks.org/plusd/)
19. [Wikispooks](https://wikispooks.com/wiki/Main_Page)
20. [xBlog: Pastebin kills search - Medium.com](https://medium.com/zeroguard/pastebin-kills-search-and-thats-okay-no-really-621d03245267)
----

### Employer Reviews | Arbeitgeber Bewertungsplattformen
1. [kununu](https://www.kununu.com/)
2. [Arbeitgeber Bewertung auf Jobvoting](http://www.jobvoting.de/)
3. [Chef und Arbeitgeber bewerten auf meinChef.de!](http://www.meinchef.de/)
4. [Glassdoor](https://www.glassdoor.de)
5. [Corporates Review](http://corporates.review)
6. [Job-Reviews.co.uk](http://job-reviews.co.uk/)
7. [Companize Jobs · Gehälter · Firmen](http://www.companize.com/)
----

### Job CV Resume
1. [Arbeit Swiss](https://www.arbeit.swiss/secoalv/de/home.html)
2. [Facebook Jobs](https://www.facebook.jobs/)
3. [Gigajob Jobbörse](http://de.gigajob.com/index.html)
4. [Jobagent jobs der Schweiz](https://www.jobagent.ch/)
5. [JOBBÖRSE der Bundesagentur für Arbeit](http://jobboerse.arbeitsagentur.de/)
6. [Jobbörse Stellenanzeigen.de](http://www.stellenanzeigen.de/)
7. [Jobbörse StepStone − Jobs und Stellenangebote](https://www.stepstone.de/)
8. [JobRobot](http://www.jobrobot.de/)
9. [Jobs.ch Schweiz](https://www.jobs.ch/de/)
10. [Jobscout24](http://www.jobs.de/)
11. [Jobsuche - Indeed](http://de.indeed.com/)
12. [jobware](http://www.jobware.de/)
13. [MarketVisual Search](http://www.marketvisual.com/)
14. [Monster.de/](http://www.monster.de/)
15. [Publicjobs Schweiz](https://www.publicjobs.ch/de/)
16. [SMR Group](https://www.smrgroup.com/)
----

### Web Directory | Web Internet Verzeichnisse
1. [Aihitdata.com Firmendaten AI](https://www.aihitdata.com)
2. [INFOMINE](http://infomine.ucr.edu/)
3. [Wolfram|Alpha](http://www.wolframalpha.com/)
4. [ODP - Open Directory Project](http://www.dmoz.org/)
5. [The WWW Virtual Library](http://vlib.org/)
6. [Digital Librarian](http://digital-librarian.com/)
7. [WWWVirtualLibrary](http://vlib.org/)
8. [IEEE Xplore - Digital Library](http://ieeexplore.ieee.org/Xplore/home.jsp?reload=true)
9. [GreyNet](http://greynet.org/greysourceindex.html)
10. [Scout Archives - Browse Resources](https://scout.wisc.edu/archives/index.php?P=BrowseResources&StartingLetter=Aa&EndingLetter=Al&Editing=0)
11. [Infoplease](http://www.infoplease.com/index.html)
12. [TechXtra](http://www.techxtra.ac.uk/index.html)
13. [WorldCat:](http://oaister.worldcat.org/)
14. [RFE](http://rfe.org/)
15. [Archive-It - Web Archiving](http://www.archive-it.org/)
16. [Common Crawl](http://commoncrawl.org/)
----

### Stolen Goods Database
1. [The National Property Register (UK)](https://www.immobilise.com/)
2. [Tracechecker (USA)](https://www.tracechecker.com/)
3. [Red Lists Database (Museum)](http://icom.museum/resources/red-lists-database/)
4. [Gestohlen.org](http://www.gestohlen.org/)
5. [Securius.eu Datenbank für sichergestellte Wertgegenstände](http://www.securius.eu/)
----

### Grading Sources | Grading Intelligence
1. [Intelligence source and information reliability - Wikipedia](https://en.wikipedia.org/wiki/Intelligence_source_and_information_reliability)
2. [x_Blog: Improving Information Evaluation for Intelligence Productionresearchgate.net](https://www.researchgate.net/publication/328858953_Improving_Information_Evaluation_for_Intelligence_Production)
3. [xBlog: The 5-minute guide to the intelligence grading process and its application in OSINT](https://www.intelligencewithsteve.com/post/the-5-minute-guide-to-the-intelligence-grading-process-and-its-application-in-osint)
----

### Library Publication Database
1. [BASE (Bielefeld Academic Search Engine): Basic Search](https://www.base-search.net/)
2. [Bioline.org.br](http://www.bioline.org.br/)
3. [link.springer](https://link.springer.com/)
4. [PDF Drive](http://www.pdfdrive.com/)
5. [RefSeek](https://www.refseek.com/)
6. [RePEc: Research Papers in Economics](http://repec.org/)
7. [ResearchRabbit](https://www.researchrabbit.ai/)
8. [Science](https://www.science.gov/)
9. [WorldCat.org](https://www.worldcat.org/)
10. [Z-Library](https://de.z-lib.org/)
----

### News Aggregator | Nachrichten Verzeichnis | Nachrichtenagentur
1. [Newspapers & News Media](http://www.abyznewslinks.com/)
2. [Online Newspapers: World Directory](http://www.onlinenewspapers.com/)
3. [Google News Archive Search](https://news.google.com/newspapers)
4. [Bing News](https://www.bing.com/news)
5. [Yahoo News](https://www.yahoo.com/news/)
6. [Reuters](http://www.reuters.com/)
7. [The Associated Press](https://www.ap.org/en-gb/)
8. [newstral Vergleichen was die Presse schreibt](https://newstral.com/de)
9. [Rivva](http://rivva.de/)
10. [BBC World](http://www.bbc.com/news/world)
11. [AFP Accueil](https://www.afp.com/)
12. [IBM Watson News Explorer](http://news-explorer.mybluemix.net/)
13. [stellungnah.me/](https://stellungnah.me/)
14. [AllYouCanRead](https://www.allyoucanread.com/)
----

### Classified Ads | Anzeigen |
1. [Harmari Search](https://www.harmari.com/search/unified/)
2. [United States Free Classified Ads](http://adsglobe.com/)
----

### Marketplace Classifieds | Marktplätze
1. [Amazon.com OSIT](http://amazon.com)
2. [Amazon.com Wunschliste](https://www.amazon.com/gp/registry/search)
3. [Anonza](https://www.anonza.de/)
4. [backpageblacklist (USA)](http://backpageblacklist.com)
5. [camelcamelcamel.com Preisüberwachung Amazon](https://camelcamelcamel.com/)
6. [craigslist OSIT](http://craigslist.org)
7. [eBbay Advanced Search](http://www.ebay.de/sch/ebayadvsearch?_sofindtype=2)
8. [eBay Kleinanzeigen](https://www.ebay-kleinanzeigen.de/)
9. [eBay.com](http://www.ebay.com/)
10. [eBay.de](http://www.ebay.de/)
11. [eBay Flippity - browse](http://www.flippity.com/)
12. [eBay Tools Goofbid](http://www.goofbid.com/)
13. [https://www.meinestadt.de/](https://www.meinestadt.de/)
14. [kalaydo.de](https://www.kalaydo.de/)
15. [Kleinanzeigen kostenlos](https://www.markt.de/)
16. [dhd24 Kleinanzeigen kostenlos inserieren](https://www.dhd24.com/)
17. [Local (USA)](http://www.local.com/)
18. [mobile.de – Gebrauchtwagen und Neuwagen](http://www.mobile.de/)
19. [Quoka.de – kostenlose Kleinanzeigen](https://www.quoka.de/)
20. [TotalCraigSearch.com](http://www.totalcraigsearch.com/)
----

### News Monitoring
1. [Social Media Monitoring Wiki](http://wiki.kenburbary.com/social-meda-monitoring-wiki)
2. [Google News OSIT](http://www.news.google.com)
3. [NewspaperARCHIVE OSIT](http://newspaperarchive.com/)
4. [AllYouCanRead.com](http://www.allyoucanread.com/)
5. [EMM Newsbrief](http://emm.newsbrief.eu/overview.html)
6. [AFP - Agence France-Presse](http://www.afp.com/en/)
7. [Newslookup.com](http://www.newslookup.com/)
8. [Newspaper Map](http://newspapermap.com/)
9. [PressDisplay](http://www.pressdisplay.com/pressdisplay/homepage_v2.aspx)
10. [ABYZ News Links](http://www.abyznewslinks.com/)
11. [World News](http://wn.com/)
12. [News Clock](http://www.newsclock.com/)
13. [interceder](http://interceder.net/)
14. [Reuters International News](http://uk.reuters.com/news/world/)
15. [NewsEdge](http://www.newsedge.com/)
16. [MarketWatch: Stock Market News](http://www.marketwatch.com/?siteid=mktw)
17. [NewsNow.co.uk](http://www.newsnow.co.uk/h/)
18. [Bing News Alerts](https://www.bing.com/news)
----

### Fact Checking | Fake News | Scams
1. [Fact-Checking Resources By Mike Reilley](https://www.journaliststoolbox.org/2019/02/17/urban_legendsfact-checking/)
2. [Firstdraftnews.org Identify Misinformation](https://firstdraftnews.org/free-online-course-on-identifying-misinformation/)
3. [[Check & Report Scams, Frauds and Dishonest Individuals - ScamPulse.com](https://www.scampulse.com/)]([Check & Report Scams, Frauds and Dishonest Individuals - ScamPulse.com](https://www.scampulse.com/))
----

### Open Data | FOAI | Wobbing |Freedom of Information
1. [Europe and Open Data Portal](https://www.europeandataportal.eu/)
2. [Data Journalism Handbook](http://datajournalismhandbook.org/1.0/en/)
3. [OPen Knowledge Foundation (GER)](http://opendata-network.org/)
4. [Mission](http://www.opendataportal.at/mission/)
5. [Open Data Tools](http://opendata-tools.org/en/)
6. [Datenjournalist](http://datenjournalist.de/)
7. [Data Driven Journalism](http://datadrivenjournalism.net/)
8. [Datenjournalist | Open Data & data-driven-journalism](http://datenjournalist.de/)
9. [Open Data Showroom](http://opendata-showroom.org/de/)
10. [Mike Bostock Data Visualisation](http://bost.ocks.org/mike/)
11. [D3.js - Data-Driven Documents](http://d3js.org/)
12. [Data Journalism Handbook 2](https://datajournalismhandbook.org/handbook/two)
13. [La liste des Portails Open Data dans le Monde - OpenDataSoft - fr](https://www.opendatasoft.fr/ressource-liste-portails-open-data-dans-le-monde/)
14. [Home | re3data.org](https://www.re3data.org/)
----

### Open Access Journals
1. [Directory of Open Access Journals](https://doaj.org/)
2. [OSINT-y Goodness, №14 — Directory of Open Access Journals](https://medium.com/@InfoSecSherpa/osint-y-goodness-14-directory-of-open-access-journals-5eb94d673851)
----

### Wikipedia
1. [Manypedia - Comparing Linguistic Points Of View (LPOV) of different language Wikipedias!](http://manypedia.com)
2. [Wikipedia "What links here"](https://en.wikipedia.org/wiki/Help:What_links_here)
----

### TOR I2P Freenet  DarkNet
1. [Book: Dark Web](http://file.allitebooks.com/20151027/Dark%20Web-%20Exploring%20and%20Data%20Mining%20the%20Dark%20Side%20of%20the%20Web.pdf)
2. [Cock.li TOR Email](https://cock.li/)
3. [Dark Web News](https://darkwebnews.com)
4. [d﻿ark.fai﻿l: Which darknet sites are online?](https://dark.fail/)
5. [DarkNet Stats](https://dnstats.net)
6. [DarkSearch](https://darksearch.io/)
7. [Deep Dot Web](https://www.deepdotweb.com)
8. [Deep Onion Web - Deep Web News .Onion Directory](https://deeponionweb.com/)
9. [Deep Websites Links](https://www.deepwebsiteslinks.com)
10. [DeepWeb Sites](https://www.deepweb-sites.com)
11. [Exonera Tor](https://exonerator.torproject.org)
12. [Freenet Anonymes Peer to Peer Netzwerk](https://freenetproject.org/)
13. [Hidden Wiki Tor](http://hiddenwikitor.com)
14. [Hyperion Gray - Dark Web Map](https://www.hyperiongray.com/dark-web-map)
15. [i2p](https://geti2p.net)
16. [JonDo](https://anonymous-proxy-servers.net/index.html)
17. [MidaSearch Dark Web](https://midasearch.org/osint-websites/dark-web/)
18. [Onion Land Search Engine](https://onionlandsearchengine.com)
19. [Onion Link](http://onion.link)
20. [Onion Scan](https://onionscan.org)
21. [Onion Search Engine](http://onionsearchengine.com)
22. [Onion.Live - Find latest darknet and deepweb mirrors, Top deepweb scams and top deepweb sellers. Report deepweb scams and verify darknet mirrors.](https://onion.live/)
23. [Onionshare](https://onionshare.org/)
24. [prof7bit/TorChat](https://github.com/prof7bit/TorChat/wiki)
25. [Reddit DeepWeb](https://www.reddit.com/r/deepweb)
26. [Tails](https://tails.boum.org/)
27. [The Dark Websites](https://www.thedarkwebsites.com)
28. [The Hidden Wiki](https://thehiddenwiki.org/)
29. [TOOL: Gichidan](https://github.com/hIMEI29A/gichidan)
30. [Tor Network Status](https://torstatus.blutmagie.de)
31. [Tor Project](https://www.torproject.org)
32. [Tor Stack Exchange](https://tor.stackexchange.com/)
33. [Tor: Documentation](https://www.torproject.org/docs/documentation.html.en)
34. [Tor2Web](https://www.tor2web.org)
35. [TorMessenger](https://trac.torproject.org/projects/tor/wiki/doc/TorMessenger#Downloads)
36. [Torproject.org/](https://www.torproject.org/)
37. [Torscan](http://torscan.io)
38. [xBlog: EFF How HTTPS & Tor Work Together](https://www.eff.org/pages/tor-and-https)
39. [xBlog: EFF How To Use Tor on MacOS](https://ssd.eff.org/en/module/how-use-tor-macos)
40. [Zeronet](https://zeronet.io)
41. [tor.taxi](https://tor.taxi/)
----

