### Mac OS Basic Security Checklist
1. [BlockBlock Ransomware Protection (Beta)](https://objective-see.com/products/blockblock.html)
2. [EtreCheck](http://etrecheck.com/)
3. [Little Snitch](https://www.obdev.at/products/littlesnitch/index-de.html)
4. [Macupdater](https://www.corecode.io/macupdater/)
5. [Malwarebytes](https://de.malwarebytes.com/)
6. [OverSight Monitor Camera/Microphone on Mac](https://objective-see.com/products/oversight.html)
7. [ClamAV](https://www.clamav.net/)
8. [Homebrew Packet Manager](https://brew.sh/index_de)
----

### Windows 10 Basic Security Checklist
1. [@Bleachbit (Linux & Win)](https://www.bleachbit.org/)
2. [@GlassWire](https://www.glasswire.com/)
3. [@O&O ShutUp10: Antispy tool for Windows 10](https://www.oo-software.com/en/shutup10)
4. [BatchPurifier](http://www.digitalconfidence.com/batchpurifier.html)
5. [CHK CompressMe Checksum Utility](http://compressme.net/)
6. [CryptoPrevent Malware Prevention](https://www.foolishit.com/cryptoprevent-malware-prevention/)
7. [Malwarebytes](https://de.malwarebytes.com/)
8. [Revo Uninstaller](http://www.revouninstaller.com/)
9. [SUMo Update Check](http://www.kcsoftwares.com/?download)
10. [Windows 10 Security Wiki on Peerlyst](https://www.peerlyst.com/posts/windows-10-security-wiki-guurhart?)
----

### Router Security Basic Checklist
1. [WiGLE: Wireless Network Mapping](https://wigle.net/)
2. [Router Security](http://routersecurity.org)
3. [Router-FAQ.de](https://www.router-faq.de/)
4. [Control access point inclusion in Google's Location services - Google Maps Help](https://support.google.com/maps/answer/1725632?hl=en)
5. [GL-AR750S / Slate - GL.iNet](https://www.gl-inet.com/products/gl-ar750s/)
----

### Checksum verification
1. [Heise Software Verzeichnis (GER)](http://www.heise.de/download/windows-50001505000/)
2. [MajorGeeks.Com](http://www.majorgeeks.com/)
3. [AlternativeTo](https://alternativeto.net/)
4. [GitHub](https://github.com/)
----

### OPSEC PRIVACY
1. [EFF Evaluating your threat model](https://www.eff.org/keeping-your-site-alive/evaluating-your-threat-model)
2. [Security for Journalists, Part Two: Threat Modeling - Learning - Source: An OpenNews project](https://source.opennews.org/articles/security-journalists-part-two-threat-modeling/)
3. [Security for Journalists, Part One: The Basics - Learning - Source: An OpenNews project](https://source.opennews.org/articles/security-journalists-part-one-basics/)
4. [Why every journalist should have a threat model (with cats) | Online Journalism Blog](https://onlinejournalismblog.com/2014/07/16/why-every-journalist-should-have-a-threat-model-with-cats/)
5. [Know Your Enemy - An Introduction to Threat Modeling](https://www.netmeister.org/blog/threat-model-101.html)
6. [Think Before You Link | Public Website](https://www.cpni.gov.uk/security-campaigns/think-you-link)
7. [How private is your personal information? - YouTube](https://www.youtube.com/watch?v=yrjT8m0hcKU&feature=youtu.be)
8. [See how easily freaks can take over your life - YouTube](https://www.youtube.com/watch?v=Rn4Rupla11M&feature=youtu.be)
9. [Amazing mind reader reveals his 'gift'](https://www.youtube.com/watch?v=F7pYHN9iC9I&feature=youtu.be)
10. [Don’t Let Trojan Horses Inside Your Connected Home | F-Secure SENSE - YouTube](https://www.youtube.com/watch?v=qdVtUY_D4OI&feature=youtu.be)
11. [https://ptrace.fefe.de/Hackback/#17](https://ptrace.fefe.de/Hackback/#17)
12. [How to Protect Privacy While Protesting or Crossing Borders | Reviews by Wirecutter](https://www.nytimes.com/wirecutter/guides/simple-online-security-for-borders-and-protests/)
13. [Hacker OPSEC](https://grugq.github.io/)
14. [Underground Tradecraft — AlphraKing AMAA (again)](https://grugq.tumblr.com/post/157100716168/alphraking-amaa-again)
15. [The Paranoid’s Bible 2.0 - The Paranoid's Bible: An anti-dox effort.](https://paranoidsbible.tumblr.com/post/160173700334/the-paranoids-bible-20)
16. [Online Privacy Through OPSEC and Compartmentalization: Part 2](https://www.ivpn.net/privacy-guides/online-privacy-through-opsec-and-compartmentalization-part-2)
----

### Virtual Machines VM
1. [Oracle VM VirtualBox](https://www.virtualbox.org/)
2. [Downloads – Oracle VM VirtualBox](https://www.virtualbox.org/wiki/Downloads)
3. [VMware – Cloud, Mobility, Networking & Security Solutions](https://www.vmware.com/)
4. [Parallels: Mac- und Windows-Virtualisierung, Verwaltung von Mac-, VDI- und RDS-Lösungen](https://www.parallels.com/de/)
5. [xBlog: Windows 10 in VirtualBox nutzen - so geht's (GER)](https://www.heise.de/tipps-tricks/Windows-10-in-VirtualBox-nutzen-so-geht-s-4208932.html)
6. [Firefox VM Browser Appliance](http://browser.shell.tor.hu/)
----

### Browser Isolation
1. [Cloud Browser | Secure Web Browser | Authentic8 - Silo](https://www.authentic8.com/)
2. [Protect teams with Browser Isolation | Cloudflare](https://www.cloudflare.com/de-de/teams/browser-isolation/)
----

### Passwort Manager host based
1. [@Keepass XC (Host)](https://keepassxc.org/)
2. [1Password (Web)](https://agilebits.com/onepassword)
3. [Codebook Password (Host Mac Device Sync)](https://www.zetetic.net/codebook/)
4. [KeePass Password Safe (Host)](http://keepass.info/)
5. [MacPass (Host Mac)](https://github.com/mstarke/MacPass)
6. [Password Safe (Host Win)](https://pwsafe.org/)
7. [SESAM Password Manager (Host)](https://github.com/ola-ct/Qt-SESAM/wiki)
8. [‎Strongbox  on the App Store (KeePass DB)](https://apps.apple.com/us/app/strongbox-password-safe/id897283731)
----

### Passwort Manager web based
1. [@Bitwarden - Open Source Password Management](https://bitwarden.com/)
2. [1Password (Web)](https://agilebits.com/onepassword)
3. [@LastPass (Web)](https://lastpass.com/)
4. [Dashlane](https://www.dashlane.com/de)
5. [Enpass](https://www.enpass.io/)
6. [SplashID](https://www.splashid.com)
7. [Why am I being prompted for Multifactor Authentication when I set my device as trusted?](https://support.logmeininc.com/lastpass/help/why-am-i-being-prompted-for-multifactor-authentication-when-i-set-my-device-as-trusted-lp010145)
----

### Secure E-Mail provider (EUROPE)
1. [@ProtonMail (CH)](https://protonmail.com/)
2. [Mailbox.org (GER)](https://mailbox.org/)
3. [Posteo.de (GER)](https://posteo.de/de)
4. [Mailfence (BEL)](https://www.mailfence.com/)
5. [Tutanota (GER)](https://tutanota.com/de/)
----

### Enable 2FA two factor identification
1. [1Password as an authenticator](https://support.1password.com/one-time-passwords/)
2. [Amazon](https://www.amazon.com/gp/help/customer/display.html?nodeId=201962420)
3. [Apple](https://appleid.apple.com/)
4. [Authy](https://www.authy.com/)
5. [Dropbox](https://blogs.dropbox.com/dropbox/2014/10/have-you-enabled-two-step-verification/)
6. [EbaY Sicherheitsschlüssel GER](http://pages.ebay.de/help/account/account-protection-security-key.html)
7. [Evernote](https://www.evernote.com/secure/SecuritySettings.action)
8. [Facebook](https://www.facebook.com/note.php?note_id=10150172618258920)
9. [Google](https://www.google.com/landing/2step/)
10. [Lockdownyourlogin campaign (US)](https://www.lockdownyourlogin.com/)
11. [Microsoft](http://go.microsoft.com/fwlink/p/?LinkID=263779)
12. [Paypal](https://www.paypal.com/DE/cgi-bin/webscr?cmd=_register-security-key-mobile)
13. [Releases · victor-rds/KeeTrayTOTP · GitHub for KeePass](https://github.com/victor-rds/KeeTrayTOTP/releases)
14. [Twitter](https://twitter.com/settings/security)
15. [xBlog: Die Bestätigung in zwei Schritten einrichten - 1&1 IONOS Hilfe](https://www.ionos.de/hilfe//?id=3269&l=0)
16. [xBlog: ProtonMail Two-factor authentication (2FA)](https://protonmail.com/support/knowledge-base/two-factor-authentication/)
----

### Yubikey / OnlyKey Hardware Token
1. [Compare YubiKeys](https://www.yubico.com/products/yubikey-hardware/)
2. [Plugins](http://keepass.info/plugins.html#otpkeyprov)
3. [White Papers](https://www.yubico.com/support/whitepapers/)
4. [Yubico](https://www.yubico.com/)
5. [YubiKey Personalization Tools](https://www.yubico.com/support/knowledge-base/categories/articles/yubikey-personalization-tools/)
6. [CryptoTrust](https://crp.to/)
----

### Virtual Private Network VPN
1. [@ProtonVPN: Secure and Free VPN service for protecting your privacy ***](https://protonvpn.com/)
2. [@That One Privacy Site – VPN Comparison Chart](https://thatoneprivacysite.net/)
3. [AirVPN (IT) ***](https://airvpn.org/)
4. [Golden Frog (CH)](https://www.goldenfrog.com/DE)
5. [IPVanish](https://www.ipvanish.com/)
6. [Perfect Privacy (CH)](https://www.perfect-privacy.com/german/)
7. [Privateinternetaccess](https://deu.privateinternetaccess.com/)
8. [Purevpn](https://www.purevpn.com/)
9. [xBlog: Chaining VPN servers (or “Double VPN”)](https://www.bestvpn.com/chaining-vpn-servers-double-vpn/)
10. [xBlog: Heise Verlag VPN Themenseite (GER)](http://www.heise.de/thema/VPN)
11. [xBlog: How to choose a VPN provider – Skadligkod.se](https://www.skadligkod.se/vpn/how-to-choose-a-vpn-provider/)
12. [xBlog: The Best VPN Service for 2019: Reviews by Wirecutter | A New York Times Company](https://thewirecutter.com/reviews/best-vpn-service/)
----

### Encrypted Chat / Other
1. [Adium + OTR (Mac)](https://adium.im/)
2. [Chatsecure.org](https://chatsecure.org/)
3. [Keybase](https://keybase.io/)
4. [Discord (Gamer Chat)](https://discordapp.com/)
----

### Secure File Transfer | Encrypted File Share
1. [KPN Secure File Transfer](https://filetransfer.kpn.com/welcome/intro)
2. [Firefox send](https://send.firefox.com/)
3. [TransferNow](https://www.transfernow.net/en/)
4. [Securesha](https://securesha.re/)
5. [FileCap](https://filecap.com/en/)
----

### Cloud Storage Encryption
1. [@ cryptomator.org](https://cryptomator.org)
2. [@ ownCloud](https://owncloud.com/)
3. [@ Tresorit End-to-End Encrypted Cloud Storage for Businesses](https://tresorit.com/)
4. [Boxcryptor Cloud Encryption - User based](https://www.boxcryptor.com)
5. [IDGuard deutsche Cloud für Datenaustausch &amp; Datenräume](https://www.idgard.de/)
6. [Spider Oak](https://spideroak.com/)
----

### Data at Rest - Full Disk Encryption
1. [Bitlocker (Win)](https://technet.microsoft.com/en-us/itpro/windows/keep-secure/bitlocker-overview)
2. [CipherShed Truecrypt fork](https://www.ciphershed.org/)
3. [Curated list of cryptography papers, tutorials and howtos](https://www.peerlyst.com/posts/a-curated-list-of-cryptography-papers-articles-tutorials-and-howtos-for-non-cryptographers-dawid-balut?utm_source=twitter&utm_medium=social&utm_content=peerlyst_post&utm_campaign=peerlyst_shared_post)
4. [FileVault (Mac)](https://support.apple.com/en-us/HT204837)
5. [nextTrueCrypt](https://truecrypt.ch/)
6. [VeraCrypt](https://veracrypt.codeplex.com/)
----

### Secure Eraser / Encryption as Erasure
1. [Bleachbit (Linux & Win) Secure Deletion](https://www.bleachbit.org/)
2. [CCEnhancer (Win)](https://singularlabs.com/software/ccenhancer/)
3. [Darik's Boot And Nuke - Secure Deletion](http://www.dban.org/)
4. [H2testw Delete Flash drive](https://www.heise.de/download/product/h2testw-50539)
5. [Heidi Eraser (win)](https://eraser.heidi.ie/)
6. [Macpaw (Mac)](http://macpaw.com/how-to/secure-empty-trash-on-mac)
----

### Archive encryption
1. [Download » 7-zip.de](https://www.7-zip.de/download.html)
2. [7-Zip Portable | PortableApps.com](https://portableapps.com/de/apps/utilities/7-zip_portable)
----

### Encrypted Paste Bins
1. [HardBin](https://hardbin.com/ipfs/QmTpjgQHPj2XuBXTwsdR7UfmiY5DKS4sNiRmdJuJCFSPpD/)
2. [Ghostbin](https://ghostbin.com/)
3. [0Bin](https://0bin.net/)
----

### Backup utilities
1. [Bombich Carbon Copy Cloner (Mac)](https://bombich.com/de)
2. [CryptSync (Win)](http://stefanstools.sourceforge.net/CryptSync.html)
3. [Duplicati](http://www.duplicati.com/)
4. [Freefilesync (Win Mac Linux)](https://sourceforge.net/projects/freefilesync/)
----

### Online Backup Services
1. [Dropbox](https://www.dropbox.com/)
2. [iCloud](https://www.apple.com/icloud/)
3. [Online Data Backup](https://www.crashplan.com/en-us/)
4. [Sichere Dateifreigabe und -synchronisierung](https://www.box.com/de-de/file-sharing)
5. [Sicherungs- und Datenschutzlösungen](https://aws.amazon.com/de/backup-recovery/)
----

### Make a Bootable USB
1. [Rufus (Win)](https://rufus.akeo.ie/)
2. [Sevenbits.io/mlul/ (Mac)](https://www.sevenbits.io/mlul/)
3. [UNetbootin (Win)](https://unetbootin.github.io/)
----

### Docker
1. [https://hub.docker.com/r/haugene/transmission-openvpn/](https://hub.docker.com/r/haugene/transmission-openvpn/)
----

### PGP Manual End to End E-Mail Encryption (via @thegrugq)
1. [Gpg4win for Windows](http://www.gpg4win.de/)
2. [GPGSync for Organisations](https://github.com/firstlookmedia/gpgsync/wiki)
3. [GPGTools for Mac OSX](https://gpgtools.org/)
4. [Mailvelope](https://www.mailvelope.com/de/)
5. [PUBLIK KEY KRYPTO (IDEA)](https://idea-instructions.com/public-key/)
6. [The GNU Privacy Guard](https://gnupg.org/)
7. [Thunderbird – EMail](https://www.mozilla.org/de/thunderbird/?flang=de)
----

### IMSI Catcher
1. [ESD Overwatch](http://esdoverwatch.com/)
----

### Email Breached Data News Sources
1. [World’s Biggest Data Breaches & Hacks — Information is Beautiful](https://www.informationisbeautiful.net/visualizations/worlds-biggest-data-breaches-hacks/)
2. [Data Breach Roundup - Steve Turner](https://www.identityforce.com/author/steve-turner)
----

### Email Breached Data | Leverage Breached/Leaked  Data
1. [@ Dehashed pwnd Database](https://dehashed.com/)
2. [@ Have I been pwned? HIBP***](https://haveibeenpwned.com/)
3. [Ashley Madison hacked email checker](https://ashley.cynic.al/)
4. [Ghostproject leaked Data and Password](https://ghostproject.fr/)
5. [Github.com Pastebin Scraper](https://github.com/Critical-Start/pastebin_scraper)
6. [Intelligence X Pastebin Search](https://intelx.io/)
7. [IntelX Email/Paste Search](https://intelx.io/)
8. [LeakedSource](https://leakedsource.ru/)
9. [PSBDMP Pastebin Search](https://psbdmp.ws/)
10. [SpyCloud - Maltego](https://spycloud.com/products/fraud-investigations/)
11. [xBlog_Steve Turner | IdentityForce® Data Breach Roundup](https://www.identityforce.com/author/steve-turner)
12. [xBlog: World’s Biggest Data Breaches & Hacks — Information is Beautiful](http://www.informationisbeautiful.net/visualizations/worlds-biggest-data-breaches-hacks/)
----

### Search Hash | Recover Password | Calculate Hash
1. [@Hashes.org Discontinued - Search for Torrent database](https://hashes.org/search.php)
2. [@Sha1-online.com](http://www.sha1-online.com/)
3. [SHA-256 hash calculator](https://xorbin.com/tools/sha256-hash-calculator)
4. [Md5 Decrypt & Encrypt - More than 15.000.000.000 hashes](https://md5decrypt.net/en/#answer)
5. [CrackStation](https://crackstation.net/)
6. [QuickHash GUI](https://www.quickhash-gui.org/)
7. [Dehash.me](https://dehash.me/)
8. [HashPals/Search-That-Hash](https://github.com/HashPals/Search-That-Hash)
----

### Self Assessment: Email Breached
1. [@Have I been pwned? HIBP](https://haveibeenpwned.com/)
2. [MELANI Check Tool](https://www.checktool.ch)
3. [OWN EMAIL Breachalarm E-Mail/Password breach](https://breachalarm.com/)
4. [OWN EMAIL Has my email been hacked? (with link to Databases)](https://hacked-emails.com/)
5. [OWN EMAIL Have I been compromised?](https://haveibeencompromised.com)
6. [OWN EMAIL HPI Identity Leak Checker](https://sec.hpi.uni-potsdam.de/leak-checker/search;jsessionid=A94B3A03C67B00F3B0FADB4A8C08E6FA)
7. [OWN EMAIL spycloud.com](https://spycloud.com/)
8. [Worlds biggest data breaches hacks Datenreichtum](http://www.informationisbeautiful.net/visualizations/worlds-biggest-data-breaches-hacks/)
----

### URL Shortener
1. [@ Kurz URLs mit Malware Schutz & Dereferrer Funktion](https://t1p.de/)
2. [Bitly](https://bitly.com/)
3. [is.gd](https://is.gd/)
4. [Tinyurl](https://tinyurl.com)
5. [T2M URL Shortener](https://t2mio.com/)
6. [FRAMEWORK https://tinyurl.com/osintframework](https://tinyurl.com/osintframework)
7. [FRAMEWORK OSINT - start.me](https://t1p.de/osint)
----

### Unshorten Redirect Unfurl URL Expander
1. [Redirect Detective](http://redirectdetective.com)
2. [Unshorten that URL! - Unshorten.It!](https://unshorten.it/)
3. [Where Does This Link Go?](http://wheredoesthislinkgo.com/)
4. [CheckShortURL](https://checkshorturl.com/expand.php)
5. [Expand Shortened URLs](https://www.expandurl.net/)
6. [GetLinkInfo](http://www.getlinkinfo.com/)
7. [unfurl URL](https://dfir.blog/unfurl/)
----

### Self Assessment: IP Device & Internet Connection
1. [@Device Info](https://www.deviceinfo.me/)
2. [IP Locator](https://www.ipfingerprints.com/)
3. [IPalyzer](https://ipalyzer.com/)
4. [My IP](https://www.myip.com/)
5. [Netperf](https://netperf.tools/)
6. [WhatIsMyIP.com®](https://www.whatismyip.com/)
7. [WHOER](https://whoer.net/de)
----

### Self Assessment: Port | Browser Privacy test
1. [@Device Info](https://www.deviceinfo.me/)
2. [@Panopticlick](https://panopticlick.eff.org/)
3. [@Whoishostingthis What's My User Agent?](http://www.whoishostingthis.com/tools/user-agent/)
4. [Am I unique?](https://amiunique.org/)
5. [BrowserLeaks.com](https://browserleaks.com/)
6. [BrowserSpy.dk](http://browserspy.dk/)
7. [Check & Secure Port Browser + Win](https://www.check-and-secure.com/start/)
8. [Detectmybrowser.com](https://detectmybrowser.com/)
9. [How's My SSL/TLS?](https://www.howsmyssl.com/)
10. [JonDonym IP check](http://ip-check.info/?lang=en)
11. [PHP 7.3.6](http://www.canyoutrackme.com/)
12. [Qualys BrowserCheck (Also as a Chrome Add On)](https://browsercheck.qualys.com/)
13. [Raymondhill.net Are Cookies blocked Test](http://raymondhill.net/httpsb/httpsb-test-cookie-1.php)
14. [Raymondhill.net HAR-parser What is your blocker NOT blocking?](http://raymondhill.net/httpsb/har-parser.html)
15. [Robinlinus Your Social Media Fingerprint Check](https://robinlinus.github.io/socialmedia-leak/)
16. [Shields UP! — Internet Vulnerability Profiling](https://grc.com/x/ne.dll?bh0bkyd2)
----

### Self Assessment : JavaScript test
1. [JavaTester Javascript Test](http://www.javatester.org/javascript.html)
2. [Raymondhill Javascript Test](http://raymondhill.net/httpsb/httpsb-test-js-1.html)
3. [Sveinbjorn Javascript Test](http://sveinbjorn.org/check_if_javascript_is_enabled)
4. [Isjavascriptenabled Javascript Test](http://www.isjavascriptenabled.com/)
5. [Is JavaScript Enabled?](https://www.whatismybrowser.com/detect/is-javascript-enabled)
6. [Activatejavascript Javascript Test](http://www.activatejavascript.org/)
7. [Browserspy Javascript Test](http://browserspy.dk/javascript.php)
----

### Self Assessment: Adobe Flash and Java Script Version
1. [Check Adobe Flash Version](http://get.adobe.com/de/flashplayer/)
2. [Check Java Version](https://www.java.com/de/download/)
----

### Self Assessment: E-Mail Privacy test
1. [@Email Privacy Tester](https://www.emailprivacytester.com/)
2. [Email Leak Tests](http://emailipleak.com/)
3. [heise online](https://www.heise.de/security//dienste/emailcheck/html-mails/webbug/)
4. [TLS Tests and Tools](https://www.checktls.com/)
5. [xBlog: Click trackin in email -Clearinghouse.wallflux.com](https://clearinghouse.wallflux.com/#click-tracking)
----

### Self Assessment: VPN & Router Leak test
1. [DNS leak test](https://www.dnsleaktest.com/)
2. [Geolocate IP Address Lookup](http://www.iptrackeronline.com/locate-ip-on-map-mini.php?lang=1)
3. [How to disable ipv6 on Mac OSX](http://help.unc.edu/help/how-do-i-disable-ipv6-on-mac-os-x/)
4. [How to disable IPv6 or its components in Windows](https://support.microsoft.com/en-us/kb/929852)
5. [IP/DNS Detect ***](https://ipleak.net)
6. [WebRTC Leak Test](https://www.browserleaks.com/webrtc)
----

### Firewall Appliance & Online Virus Check
1. [Sophos UTM Essential Firewall](https://www.sophos.com/en-us/products/free-tools/sophos-utm-essential-firewall.aspx)
2. [VirusTotal ***](https://www.virustotal.com/de/)
3. [PhishTank](https://www.phishtank.com/)
4. [ThreatMiner](https://www.threatminer.org/)
----

### Self assesment: Password test & Password generator
1. [How Secure Is My Password?](https://howsecureismypassword.net/)
2. [Passwort-generator (GER)](http://www.umrechnung.org/passwort-generator/password.htm)
3. [Secure 1Password Metadata (GER)](http://www.schlosser.info/1password-dropbox-synchronisieren/)
4. [Strength Test](http://rumkin.com/tools/password/passchk.php)
5. [Strong Random Password Generator (ENG)](http://passwordsgenerator.net/)
----

### SSL test
1. [wrong.host.badssl.com/](https://wrong.host.badssl.com/)
2. [https://expired.badssl.com/](https://expired.badssl.com/)
3. [https://self-signed.badssl.com/](https://self-signed.badssl.com/)
4. [https://revoked.badssl.com/](https://revoked.badssl.com/)
5. [https://rc4.badssl.com/](https://rc4.badssl.com/)
----

### ZERO DAY
1. [ZERODIUM](https://www.zerodium.com/program.html)
----

### Privacy
1. [Netzpolitik.org](https://netzpolitik.org/)
2. [Privacyinternational.org](https://privacyinternational.org/)
3. [ENISA European Union Agency for Network and Information Security](https://www.enisa.europa.eu/)
4. [Federal Trade Commission](https://www.ftc.gov/)
5. [Privacy Enforcement and Protection | State of California - Department of Justice - Office of the Attorney General](https://oag.ca.gov/privacy)
6. [International Association of Privacy Professionals](https://iapp.org/)
7. [ICO](https://ico.org.uk/)
8. [Future of Privacy Forum](https://fpf.org/)
9. [Privacy | JD Supra](https://www.jdsupra.com/law-news/privacy/)
10. [EPIC](https://epic.org/)
11. [Privacy, Cybersecurity and Data Security Overview](http://www.ncsl.org/research/telecommunications-and-information-technology/privacy-and-security.aspx)
12. [Digital Safety Kit - Committee to Protect Journalists](https://cpj.org/2019/07/digital-safety-kit-journalists.php)
13. [privacytools.io](https://www.privacytools.io/)
14. [Surveillance Self-Defense](https://ssd.eff.org/en)
----

### Children Online Safety Resources (GER)
1. [Lexikon für Jugendliche und Kinder Kids Digital Genial](https://shop.digitalcourage.de/store-products.php?seo=kids-digital-genial)
2. [Sicheres Internet für Kinder – SCHAU HIN!Schau Hin!](https://www.schau-hin.info/tipps-regeln/sicheres-internet-fuer-kinder)
3. [Surfen ohne Risiko Netzregeln](https://www.surfen-ohne-risiko.net/#fancyboxOpen=netzregeln=https%3A%2F%2Fwww.surfen-ohne-risiko.net%2Fnetzregeln%2F)
4. [Keep It Real Online
 - YouTube](https://www.youtube.com/channel/UCtq-hgUo003d4kMjh40k2YA)
----

### Infosec Phish stats
1. [PhishStats](https://phishstats.info/)
2. [Phishing Gallery - Security Without Borders](https://t.co/hYDcuXFmGw?amp=1)
----

