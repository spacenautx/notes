### Reddits
1. [Open Source Intelligence - r/OSINT](https://www.reddit.com/r/OSINT/)
2. [Reddit Bureau of Investigation - r/RBI](https://www.reddit.com/r/rbi/)
3. [EverydayEspionage - r/EverydayEspionage](https://www.reddit.com/r/EverydayEspionage/)
4. [Espionage - r/espionage](https://www.reddit.com/r/espionage/)
5. [Counterterrorism - r/Counterterrorism](https://www.reddit.com/r/Counterterrorism/)
6. [ActLikeYouBelong - r/ActLikeYouBelong](https://www.reddit.com/r/ActLikeYouBelong/)
7. [Social Engineering - r/SocialEngineering](https://www.reddit.com/r/SocialEngineering/)
----

### Communities
1. [Searchlight Discord](https://discord.gg/BtFpNJ2)
2. [Trace Labs](https://tracelabs.org)
3. [OSINT TEAM](https://osint.team)
----

### Challenges
1. [Sourcing.Games](https://www.sourcing.games/)
2. [Verif!cation Quiz Bot](https://twitter.com/quiztime)
3. [PictureGame](https://www.reddit.com/r/PictureGame/)
4. [geoguessr](https://geoguessr.com/)
5. [TryHackMe](https://tryhackme.com/room/ohsint)
6. [WhatIsThisThing](https://www.reddit.com/r/whatisthisthing/)
7. [CyberSoc](https://ctf.cybersoc.wales/)
----

### Collaboration
1. [pad.riseup.net](https://pad.riseup.net/)
2. [Google Docs](https://docs.google.com/)
----

### Software & Tools
1. [Lampyre](https://lampyre.io/)
2. [Maltego](https://www.paterva.com/)
3. [hunch.ly](https://www.hunch.ly/)
----

### Other People's Pages
1. [The Ultimate OSINT Collection - start.me](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
2. [Technisette - OSINT](https://start.me/p/m6XQ08/osint)
3. [Lorand - Terrorism & Radicalisation Research Dashboard](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
4. [AsINT_INV - AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
5. [AML Toolbox](https://start.me/p/rxeRqr/aml-toolbox)
6. [Dating apps and hook-up sites for investigators](https://start.me/p/VRxaj5/dating-apps-and-hook-up-sites-for-investigators)
----

### Blogs
1. [Week in OSINT – Medium](https://medium.com/week-in-osint)
2. [NixIntel – OSINT, Linux, Digital Forensics, and InfoSec](https://nixintel.info/)
3. [Private Investigator Blog - Public Records, Internet Search - PI Buzz](https://pibuzz.com/)
4. [Key Findings](https://keyfindings.blog/)
5. [Exposing the Invisible](https://exposingtheinvisible.org/)
6. [Investigations ← Forensic Architecture](https://forensic-architecture.org)
7. [OSINTCurio.us](https://osintcurio.us/)
8. [einvestigator](https://www.einvestigator.com/)
9. [Bublup roll - What to Read Right Now](https://rolls.bublup.com/kirbstr/read)
10. [OSINT - Secjuice](https://www.secjuice.com/tag/osint/)
----

