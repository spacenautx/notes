### Tomoko Discovery Tools
1. [Cryptocurrency Tools](https://start.me/p/7k5RPp/tomoko-discovery-cryptocurrency)
2. [AML Tools](https://start.me/p/dl6Y9J/tomoko-discovery-aml)
3. [Antiterrorism Tools](https://start.me/p/1kmyDo/tomoko-discovery-terrorism)
4. [OSINT Tools](https://start.me/p/gyde2p/tomoko-discovery-osint)
----

### Twitter Tools
1. [Twitter Advanced Search](https://twitter.com/search-advanced)
2. [TweeterID](https://tweeterid.com)
3. [Twitter Analytics for Tweets](https://socialbearing.com)
4. [TweetBeaver](https://tweetbeaver.com)
5. [One Million Tweet Map](http://onemilliontweetmap.com/)
6. [Followerwonk](https://followerwonk.com/analyze)
7. [Mentionmapp](http://mentionmapp.com/)
8. [Bluenod](http://bluenod.com/)
9. [Foller.me Analytics for Twitter](http://foller.me/)
10. [Sleeping Time](http://sleepingtime.org/)
11. [All My Tweets](https://www.allmytweets.net/connect)
12. [TwimeMachine](http://www.twimemachine.com)
13. [Botcheck](https://botcheck.me)
14. [Tweet Topic](http://tweettopicexplorer.neoformix.com)
15. [Who Tweeted It First](http://ctrlq.org/first/)
16. [Tweet Mapper](https://keitharm.me/projects/tweet)
17. [Quarter Tweets](http://qtrtweets.com/twitter)
18. [Twitonomy](http://twitonomy.com)
19. [TweetReach](https://tweetreach.com)
20. [Treeverse Extension](https://github.com/paulgb/Treeverse/blob/master/README.md)
21. [Keyhole: Hashtag Tracking](http://keyhole.co)
22. [Fake Follower Check](http://fakers.statuspeople.com)
23. [Twitterfall](https://twitterfall.com)
24. [Twlets](http://www.twlets.com)
25. [World Leader Twitter Directory](https://worldleadertwitterdirectory.wordpress.com/)
----

### Delete Tweets Tools
1. [TweetEraser](https://www.tweeteraser.com/)
2. [twitwipe](http://twitwipe.com/)
3. [Tweetdelete.net](https://www.tweetdelete.net/)
4. [Cardigan: Delete old Tweets](https://www.gocardigan.com/)
5. [Tweetdeleter - Delete all of your tweets fast](https://www.tweetdeleter.com/en)
----

### IFTTT Applets / Receipes
1. [IFTTT Applet: Connect Twitter to Google Drive](https://ifttt.com/myrecipes/personal/21510133)
2. [IFTTT Applet: Track  search or #hashtag mentions on Twitter](https://ifttt.com/myrecipes/personal/41736800)
3. [IFTTT Applet: Monitor-tweets-in-a-specific-area](https://ifttt.com/recipes/312309-monitor-tweets-in-a-specific-area)
4. [IFTTT Applet: Track new tweet from user, add to spreadsheet](https://ifttt.com/recipes/280410-if-new-tweet-by-a-specific-user-then-add-a-row-to-google-drive-spreadsheet)
5. [IFTTT Applet: Tweet to Google Drive Spreadsheet](https://ifttt.com/applets/280410p-if-new-tweet-by-a-specific-user-then-add-a-row-to-google-drive-spreadsheet)
6. [IFTTT - Discover IFTTT and Applets](https://ifttt.com/discover)
----

### Acquisition: Twitter2RSS
1. [Feed Exileed](http://feed.exileed.com/)
2. [Inoreader Pro (€)](https://www.inoreader.com)
3. [Queryfeed (Freemium)](https://queryfeed.net/)
4. [Publicate.it](https://publicate.it/twitter-rss-feed-generator/)
5. [Rss.app (Freemium)](https://rss.app/)
6. [RSS Box (+ Self Hosting)](https://rssbox.herokuapp.com/)
7. [TwitRSS.me](http://twitrss.me/)
8. [Zapier (Twitter>RSS)](https://zapier.com/)
9. [GitHub: RSS-Bridge/rss-bridge (Self Hosting)](https://github.com/RSS-Bridge/rss-bridge)
----

### Int'l Social Media
1. [Badoo](https://badoo.com)
2. [VK - Russia](https://vk.com)
3. [OK.RU](https://ok.ru)
4. [Renren - China](http://renren.com)
5. [QZone - China](http://www.qq.com)
6. [Taringa! - Latin America](https://www.taringa.net)
7. [Draugiem - Latvia](https://www.draugiem.lv)
8. [Facenama - Iran](https://facenama.com/home)
9. [Mixi: Japan](https://mixi.jp)
10. [hi5 - Asia, E. Europe, Africa](https://secure.hi5.com)
11. [Tiktok - China, Asian Countries](https://www.tiktok.com)
----

### Social Content Sharing
1. [reddit](https://www.reddit.com)
2. [Quora](https://www.quora.com)
3. [Steemit](https://steemit.com)
----

### Email Address Tools
1. [Hunter](https://hunter.io)
2. [Voila Norbert](https://www.voilanorbert.com)
3. [DomainBigData](https://domainbigdata.com)
4. [MailTester](http://mailtester.com/testmail.php)
5. [Verifalia](http://verifalia.com/validate-email)
6. [Email Format](https://email-format.com)
7. [Have I Been Pwned](https://haveibeenpwned.com)
8. [Ashley Madison hacked email checker](https://ashley.cynic.al)
9. [Melissa Data Email Check](http://melissadata.com/lookups/emails.asp)
10. [Email Permutator](http://metricsparrow.com/toolkit/email-permutator)
11. [Infoga - GitHub](https://github.com/m4ll0k/infoga)
12. [FindAnyEmail](https://www.toofr.com)
----

### E-Mail Forwarding / Remailer
1. [Paranoici Remailer](http://remailer.paranoici.org/index.php)
2. [33Mail](http://www.33mail.com/)
3. [Fake Email](http://email-fake.com)
4. [Sneakemail](https://sneakemail.com/)
5. [inboxbear - temporary email](https://inboxbear.com/)
6. [Eliminate Spam with Cocoon](https://getcocoon.com/features/nospam)
7. [Quicksilvermail (Win Client)](https://www.quicksilvermail.net/)
----

### Verify E-Mail
1. [Email Verifier](https://hunter.io/email-verifier)
2. [MailTester OSIT](http://mailtester.com/testmail.php)
3. [Free e-Mail Validator Tool OSIT](http://e-mailvalidator.com)
4. [Verify Email Address Online](http://verify-email.org/)
5. [Email Address Verifier](https://tools.verifyemailaddress.io/)
6. [Referenz für SMTP Fehler](https://support.google.com/a/answer/3726730?hl=de)
----

### Email Tools: Header, Trace, etc.
1. [Email Header Analyzer](http://www.mxtoolbox.com/EmailHeaders.aspx)
2. [RUS-CERT E-Mail-Header richtig lesen](https://cert.uni-stuttgart.de/themen/spam/header.html)
3. [Trace Email Address Source](http://whatismyipaddress.com/trace-email)
4. [Email Trace OSIT](http://www.ip-adress.com/trace_email/)
5. [E-Mail-Header lesen und verstehen](https://th-h.de/net/usenet/faqs/headerfaq/)
6. [SpamCop Reveal the full, unmodified email](http://www.spamcop.net/fom-serve/cache/19.html)
7. [What Can You Find in an Email Header?](https://www.howtogeek.com/108205/htg-explains-what-can-you-find-in-an-email-header/)
8. [How to Read and Analyze the Email Header Fields and Information about
SPF, DKIM, SpamAssassin](https://www.arclab.com/en/kb/email/how-to-read-and-analyze-the-email-header-fields-spf-dkim.html)
----

### IP Logger Tools | Identify Target IP | Canary Token
1. [Webresolver](https://webresolver.nl/)
2. [Find someone's ip address » Tracking and logging IP adresses](http://iplogger.org/)
3. [Grabify](https://grabify.link/)
4. [Blasze IP Logger](http://blasze.tk/)
5. [Get Notify Email Tracking Service](https://www.getnotify.com/)
6. [ReadNotify](http://www.readnotify.com/)
7. [Google URL Shortener](https://goo.gl/)
8. [Monitor folders on Windows with Folder Monitor - gHacks Tech News](https://www.ghacks.net/2018/08/07/monitor-folders-on-windows-with-folder-monitor/)
9. [Yesware Email Tracking](https://www.yesware.com/email-tracking/)
10. [Honeybox Home - secXtreme GmbH](https://www.honeybox.de/home.html)
11. [Canary Token | Know.  Before it matters](https://canarytokens.org/generate)
----

### Spoof Email
1. [Emkei's Fake Mailer](https://emkei.cz/)
2. [Anonymousemail](https://anonymousemail.me)
3. [Send Email](http://send-email.org)
4. [Email-Fake](https://email-fake.com/)
----

### Spam
1. [SpamCop](https://www.spamcop.net/bl.shtml)
2. [SpamHaus](https://www.spamhaus.org/)
----

### Reset Password | Verify Email
1. [Twitter](https://twitter.com/account/begin_password_reset)
2. [Facebook](https://www.facebook.com/login/identify?ctx=recover)
3. [LinkedIn](https://www.linkedin.com/uas/request-password-reset?trk=help-feature-launcher)
4. [Pinterest](https://www.pinterest.de/password/reset/)
5. [Gravatar](https://en.gravatar.com/site/check/)
6. [Yahoo](https://login.yahoo.com/account/challenge/username?authMechanism=secondary&authCredentialsType=cookies&yid=&done=https%253A%252F%252Fwww.yahoo.com&s=QQ--&c=eyJ2IjoyLCJjIjp0cnVlfQ--~QKnw44O2LgW1_928.AAG1ljpa9DtBIFoJDoAQ0F6UdY6VOZG4SpnaI11iYMXOwBeQAel0XxrYOAehQZ83GCPmILEIfLC15A4L59v2yKXgPrDO4CyEb7DClEMujxwpIz1BeZzkuv1_OsgbzdIb45JSjP519E0lh2T3gsJEGtQ7vlzVkphv2CGp_x_AHr2dBs2p8INaxOhGANOIs0a603bBdBZsYTxweukMRqpoarkJaYaPJGOH.HwzF2Txjo-&crumb=kZreXjkHJtV)
7. [VK](https://vk.com/restore)
8. [Instagram](https://www.instagram.com/accounts/password/reset/)
9. [Microsoft](https://account.live.com/ResetPassword.aspx?)
10. [Ebay](https://fyp.ebay.de/EnterUserInfo?)
11. [Web.de](https://passwort.web.de/passwordrecovery/)
12. [YouTube](https://www.youtube.com/account_recovery)
13. [GMX](https://passwort.gmx.net/passwordrecovery)
14. [Skype](https://a.login.skype.com/diagnostics)
15. [Ello.co](https://ello.co/forgot-password)
16. [Dropbox](https://www.dropbox.com/forgot)
17. [Github](https://github.com/password_reset)
----

### Phone | Phone Number Tools
1. [CallerIDtest](https://www.calleridtest.com)
2. [CallerIDservice](https://secure.calleridservice.com)
3. [TrueCaller](https://www.truecaller.com)
4. [NumberGuru](https://www.numberguru.com)
5. [NumVerify](https://numverify.com/)
6. [CellRevealer](https://cellrevealer.com)
7. [OpenCellID](https://opencellid.org/#zoom=16&lat=37.77889&lon=-122.41942)
8. [TextMagic](https://www.textmagic.com/free-tools/carrier-lookup)
9. [Pipl](https://pipl.com)
10. [411.com](https://www.411.com/reverse_phone)
11. [That's Them Reverse Phone Lookup](https://thatsthem.com/reverse-phone-lookup)
12. [Receive SMS](http://receive-sms-online.com/)
13. [Receive-SMS](https://receive-sms.com/)
14. [Sellaite - Receive SMS](http://sms.sellaite.com/)
15. [Receive SMS](https://www.receivesmsonline.net/)
16. [US Phonebook](https://www.usphonebook.com)
17. [OpenCNAM](https://www.opencnam.com/)
18. [HLR-lookups](https://www.hlr-lookups.com)
19. [NextCaller](https://nextcaller.com/)
20. [Mr. Number](http://mrnumber.com/1-888-742-0000)
21. [Free Carrier Lookup](http://freecarrierlookup.com/)
22. [Phone Validator](http://www.phonevalidator.com/)
23. [SpyDialer](http://www.spydialer.com/default.aspx)
24. [Reverse Genie](http://www.reversegenie.com/phone.php)
25. [Fone Finder](http://www.fonefinder.net/)
26. [Dialing Codes](http://www.dialingcode.com/)
27. [DigCaller - UK](https://digcaller.co.uk/)
28. [Imei24](https://imei24.com/blacklist_check/)
29. [Make and Model Database](https://imei24.com/phone_base/)
30. [Mcc-Mnc](http://www.mcc-mnc.com/)
31. [Mobile Number Validation - UK](https://www.experian.co.uk/business/data-management/data-validation/mobile-validation/)
----

### Archives
1. [Internet Archive: Wayback Machine](https://archive.org/web)
2. [Archive.is](https://archive.is)
3. [WebCite](https://webcitation.org/query)
4. [Cached View](http://cachedview.com)
5. [T E X T F I L E S D O T C O M](http://textfiles.com)
6. [Webarchive.org.uk](https://www.webarchive.org.uk/ukwa)
7. [Common Crawl](https://commoncrawl.org)
8. [PDF My URL](http://pdfmyurl.com)
----

### Data Leaks
1. [WikiLeaks](https://wikileaks.org)
2. [Cryptome](http://cryptome.org)
3. [LeakScraper](https://github.com/Acceis/leakScraper/wiki/leakScraper)
----

### Public Datasets
1. [Labeled Faces in the Wild DB](http://vis-www.cs.umass.edu/lfw)
2. [Stanford SNAP Dataset](http://snap.stanford.edu/data)
----

### Media, Archives, News, CC
1. [Digitized Newspapers: 1826-1922](https://chroniclingamerica.loc.gov)
2. [Newspaper Directory: 1690-Present](https://chroniclingamerica.loc.gov/search/titles)
3. [TV Closed Caption Search](https://archive.org/details/tv)
4. [Newspaper Map](http://newspapermap.com/)
5. [NewsBot](https://getnewsbot.com/)
6. [NewsNow](http://www.newsnow.co.uk/h/)
7. [World News](https://wn.com/#/search)
8. [AllYouCanRead - Search](https://www.allyoucanread.com/)
9. [YouGotTheNews](https://www.yougotthenews.com/)
10. [ThePaperBoy](https://www.thepaperboy.com/)
----

### Financial / Tax Tools
1. [Credit Card BIN Numbers Database](http://www.binbase.com/search.html)
2. [BIN Database](https://www.bindb.com/bin-database.html)
3. [IBAN Info](http://www.tbg5-finance.org/?ibancheck.shtml)
4. [VAT Search](https://vat-search.eu/)
5. [NETROnline](https://publicrecords.netronline.com)
6. [US Tax Court](https://ustaxcourt.gov)
7. [VAT Number Validation](http://ec.europa.eu/taxation_customs/vies/?locale=en)
8. [Valid Credit Card Number](https://www.validcreditcardnumber.com/)
9. [Citizen Audit](https://www.citizenaudit.org/)
----

### Birth | Death Records
1. [Death Record Search](http://www.dobsearch.com/death-records)
2. [Public Records Search](http://publicrecords.searchsystems.net)
3. [Melissa Death Check](http://melissadata.com/lookups/deathcheck.asp)
4. [Find A Grave](https://www.findagrave.com)
5. [Grave Info](http://www.graveinfo.com)
6. [U.S. Social Security Death Index](https://www.familysearch.org/search/collection/1202535)
7. [Sorted By Birthdate](http://sortedbybirthdate.com)
8. [Birth Database](http://www.birthdatabase.com)
9. [Legacy.com](http://www.legacy.com)
10. [GEDmatch](https://www.gedmatch.com)
11. [NewspaperArchive®](http://newspaperarchive.com/)
12. [Genealogy.com](http://www.genealogy.com)
----

### Analytics
1. [BuiltWith Technology Lookup](https://builtwith.com)
2. [Netcraft](https://toolbar.netcraft.com/site_report?url=undefined#last_reboot)
3. [StatsCrop](http://www.statscrop.com)
4. [Moz Link Explorer](https://moz.com/link-explorer)
5. [SpyOnWeb Research Tool](http://www.spyonweb.com)
6. [Security Headers](https://securityheaders.com)
7. [Alexa](https://www.alexa.com/siteinfo)
8. [W3bin](http://w3bin.com)
9. [SameID](http://sameid.net)
10. [URL Scan](https://urlscan.io)
11. [Visual Site Mapper](http://www.visualsitemapper.com)
12. [Blue Backlinks](http://bluebacklinks.com)
13. [Backlink Checker](https://smallseotools.com/backlink-checker)
14. [PubDB](http://pub-db.com)
15. [Siteliner](http://siteliner.com)
16. [Timer4Web](https://www.timer4web.com)
17. [Visualping](https://visualping.io)
18. [Follow That Page](https://www.followthatpage.com)
19. [URLwatch - GitHub](https://github.com/thp/urlwatch)
20. [WatchThatPage](http://watchthatpage.com)
21. [ChangeDetect](http://www.changedetect.com)
22. [CaseFile](https://www.paterva.com/web7/buy/maltego-clients/casefile.php)
23. [SiteSleuth](https://www.sitesleuth.io)
----

### Government Records
1. [Free County Records](https://www.blackbookonline.info/USA-Counties.aspx)
2. [County Explorer](https://ce.naco.org/index.html)
3. [Service Member Record Request](https://scra.dmdc.osd.mil/scra/#/single-record)
4. [Fold3 - Military Records](https://www.fold3.com)
5. [SSN Validator](https://www.ssnvalidaTor.com/index.aspx)
6. [Government Records](https://www.govinfo.gov/)
7. [NC State Employee Salaries](https://www.newsobserver.com/news/databases/state-pay)
----

### Default Passwords
1. [Default-password](https://default-password.info/)
2. [Hashes.org](https://hashes.org)
3. [Popular Default Passwords](http://open-sez.me/)
4. [Default Router Passwords](http://routerpasswords.com)
5. [Default Password List](http://phenoelit.org/dpl/dpl.html)
6. [Default Password Lookup](http://www.fortypoundhead.com/tools_dpw.asp)
----

### Network Tools
1. [Microsoft DOS pathping command](https://www.computerhope.com/pathping.htm)
2. [Network Tools](https://network-tools.com/)
3. [heise.de/tools/](https://www.heise.de/tools/)
4. [Path Analyzer Pro - the ultimate traceroute](http://www.pathanalyzer.com/)
5. [TOOL: Traceroute and Visual Route tool](http://www.visualroute.com/)
6. [G Suite Toolbox](https://toolbox.googleapps.com/apps/main/)
----

### Government Records (Misc)
1. [Contract Database - US GOV](https://www.sam.gov/portal/SAM/##11)
2. [EPA EnviroMapper for Envirofacts](https://www.epa.gov/emefdata/em4ef.home)
3. [Federal Firearms License Search](https://fflezcheck.atf.gov/fflezcheck/)
4. [FOIA.gov - Freedom of Information Act: Find](https://www.foia.gov/search.html)
5. [Governmentattic.org - FOIA Collection](http://governmentattic.org/)
6. [govinfo | Next Generation Federal Digital System](https://www.govinfo.gov/)
7. [Govt Publications Catalog - Electronic Titles - Basic Search: Full Catalog](https://catalog.gpo.gov/F?RN=848528119)
8. [Household Products Database](https://householdproducts.nlm.nih.gov/)
9. [Labor Union Searches - DOL](https://www.dol.gov/olms/regs/compliance/rrlo/lmrda.htm)
10. [LobbyData - Lobbyist Database](http://lobbydata.com/Directory)
11. [NGO - Google Custom Search](https://cse.google.com/cse/publicurl?cx=012681683249965267634:q4g16p05-ao)
12. [Polution - Scorecard Home (Search by Zipcode)](http://scorecard.goodguide.com/)
13. [Publications Database - US GOV](https://catalog.gpo.gov/F?RN=416017196)
14. [Publishing Office - US GOV](https://www.gpo.gov/)
15. [Selective Service System > Registration > Check a Registration > Check a Registration Form](https://www.sss.gov/registration/check-a-registration/verification-form)
16. [SSN Validator | Free SSN Validation | Free SSN Verification](https://www.ssnvalidator.com/index.aspx)
17. [SSS.gov/home/verification](http://sss.gov/home/verification)
18. [State Adoption Resources](https://www.childwelfare.gov/)
19. [Traffic Cameras and Reports Search Directory](http://publicrecords.onlinesearches.com/Traffic-Cameras-and-Reports.htm)
20. [US Government Publishing Office - FDsys - Home](https://www.gpo.gov/fdsys/)
----

### Police Agencies (Warrants+ Most Wanted)
1. [ATF - Most Wanted](https://www.atf.gov/most-wanted)
2. [Crime Stoppers of Central Iowa](http://polkcountycrimestoppers.org/sitemenu.aspx?ID=772&)
3. [DEA.gov / Fugitive](https://www.dea.gov/fugitives.shtml)
4. [EPA Fugitives | Enforcement | US EPA](https://www.epa.gov/enforcement/epa-fugitives)
5. [FBI - Most Wanted](https://www.fbi.gov/wanted/topten)
6. [ice.gov/most-wanted#tab0](https://www.ice.gov/most-wanted#tab0)
7. [Iowa - Crime Stoppers of Central Iowa](http://www.crimestoppersofcentraliowa.com/sitemenu.aspx?ID=772&P=unsolved)
8. [Most Wanted State Sites](http://corrections.com/links/36615/link)
9. [U.S. Marshals Service, 15 Most Wanted, Index](https://www.usmarshals.gov/investigations/most_wanted/)
----

### IANA ICAN
1. [American Registry for Internet Numbers (ARIN)](https://www.arin.net/index.html)
2. [Country Code Names Supporting Organisation](https://ccnso.icann.org/)
3. [ICANN](https://www.icann.org/)
4. [Whois Inaccuracy Complaint Form](https://forms.icann.org/en/resources/compliance/complaints/whois/inaccuracy-form)
5. [CCNSO](https://ccnso.icann.org/)
6. [Internet Assigned Numbers Authority](https://www.iana.org/)
7. [AFRINIC](http://www.afrinic.net/)
8. [Home](https://www.apnic.net/)
9. [RIPE Network Coordination Centre](https://www.ripe.net/)
10. [Inicio](http://www.lacnic.net/)
11. [LACNIC Inicio](http://www.lacnic.net/)
12. [Country Code Names Supporting Organisation](https://ccnso.icann.org/en)
----

### Drug Trade
1. [International Narcotics Control Strategy Report](https://www.state.gov/j/inl/rls/nrcrpt/2017/)
2. [DEA Drug Slang Terms](https://ndews.umd.edu/sites/ndews.umd.edu/files/dea-drug-slang-terms-and-code-words-july2018.pdf)
3. [NIH Commonly Abused Drugs](https://www.drugabuse.gov/drugs-abuse/commonly-abused-drugs-charts#pcp)
4. [UN International Drug Prices](https://data.unodc.org/)
5. [InSight Crime](https://www.insightcrime.org/)
6. [US Gangs List](https://en.wikipedia.org/wiki/List_of_gangs_in_the_United_States)
----

### CyberCrime
1. [ActorTrackr](https://actortrackr.com/)
2. [APT Groups and Operations](https://docs.google.com/spreadsheets/u/1/d/1H9_xaxQHpWaa4O_Son4Gx0YOIzlcBWMsdvePFX68EKU/pubhtml)
3. [APT Groups, Operations and Malware Search Engine](https://cse.google.com/cse/publicurl?cx=003248445720253387346:turlh5vi4xc)
4. [Cyber Campaigns](http://cybercampaigns.net/)
5. [Shadowserver Foundation](https://shadowserver.org/wiki/)
----

### Darkweb
1. [DeepWeb - Deeplinks](https://www.deepweb-sites.com/deep-web-links-2015/)
2. [i2p (download)](https://geti2p.net/en/)
3. [Tails](https://tails.boum.org/)
4. [Subgraph OS (download)](https://subgraph.com/sgos/)
5. [Whonix](https://www.whonix.org/)
6. [Ichidan (visit via Tor)](http://ichidanv34wrx7m7.onion)
7. [Onionscan](https://onionscan.org/)
8. [OnionShare](https://blog.torproject.org/tor-heart-onionshare)
9. [Tor (download)](https://www.torproject.org/download/)
10. [TorNodes](https://www.dan.me.uk/tornodes)
11. [Zeronet (download)](https://zeronet.io/)
12. [Reddit r/darknet](https://www.reddit.com/r/darknet)
13. [IACA Dark Web Investigation Support](https://iaca-darkweb-tools.com/)
14. [Human Traffickers Caught on Hidden Internet - Scientific American](http://www.scientificamerican.com/article/human-traffickers-caught-on-hidden-internet/)
15. [My Brief Encounter with a Dark Web &#39;Human Trafficking&#39; Site | Motherboard](http://motherboard.vice.com/read/my-brief-encounter-with-a-dark-web-human-trafficking-site)
16. [BrightPlanetdeep web vs dark web Archives - BrightPlanet](http://www.brightplanet.com/tag/deep-web-vs-dark-web/)
----

### Sex Offender Registry
1. [Iowa Sex Offender Registry](http://www.iowasexoffender.com/)
2. [Offender Search | Iowa Department of Corrections](https://doc.iowa.gov/offender/search)
3. [Sex Offender Public - National - DOJ](https://www.nsopw.gov/?AspxAutoDetectCookieSupport=1)
4. [Sex Offender Records Search Directory](http://publicrecords.onlinesearches.com/Sex-Offender-Registration.htm)
5. [Sex Offender Registry | Local Sex Predators | Family Watchdog](https://www.familywatchdog.us/Default.asp)
6. [Sex Offender Search | Sex Offender Records](https://www.blackbookonline.info/USA-Sex-Offenders.aspx)
----

### Abuse Reporting
1. [Cyberbullying.org/report](https://cyberbullying.org/report)
2. [Datacentrumgids (Benelux)](https://www.datacentrumgids.nl/)
3. [Gethuman (USA)](http://gethuman.com)
4. [Meldknop.nl - report abuse (NL)](https://meldknop.nl)
5. [Veiliginternetten.nl (NL)](https://veiliginternette.nl)
----

### Databases - Animals
1. [Chipnummer.nl/databanken (NL)](http://chipnummer.nl/databanken)
----

### Reverse analytics | Sourcecode
1. [PubDB traffic stats of websites](http://pub-db.com/)
2. [NerdyData Search for Source Code](https://nerdydata.com/search)
3. [SameID.net](http://sameid.net/)
4. [Analyze ID](http://analyzeid.com/)
5. [Search Engine for Source Code](https://publicwww.com/)
----

### Chrome Extensions: Productivity
1. [Google Translate](https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb)
2. [Google Similar Pages](https://chrome.google.com/webstore/detail/google-similar-pages/pjnfggphgdjblhfjaphkjhfpiiekbbej)
3. [Save to google drive](https://chrome.google.com/webstore/detail/save-to-google-drive/gmbmikajjgmnabiglmofipeabaddhgne?hl=en)
4. [TXT Als reinen Text kopieren](https://chrome.google.com/webstore/detail/copy-as-plain-text-amazin/mkkcgjeddgdnikkeoinjgbocghokolck)
5. [IP Information](https://chrome.google.com/webstore/detail/ip-address-and-domain-inf/lhgkegeccnckoiliokondpaaalbhafoa)
6. [Capture Webpage Screenshot Entirely. FireShot](https://chrome.google.com/webstore/detail/capture-webpage-screensho/mcbpblocgmgfnpjjppndjkmgjaogfceg)
7. [Data Scraper](https://chrome.google.com/webstore/detail/data-scraper/nndknepjnldbdbepjfgmncbggmopgden?hl=de)
8. [WayBack Machine](https://chrome.google.com/webstore/detail/wayback-machine/fpnmgdkabkmnadcjpehmlllkndpkmiak?hl=de)
9. [Image Search options](https://chrome.google.com/webstore/detail/image-search-options/kljmejbpilkadikecejccebmccagifhl/reviews)
10. [JSONView](https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc)
11. [Cite This For Me: Web Citer](https://chrome.google.com/webstore/detail/cite-this-for-me-web-cite/nnnmhgkokpalnmbeighfomegjfkklkle)
12. [ZapInfo](https://chrome.google.com/webstore/search/zapinfo)
13. [Instant Data Scraper](https://chrome.google.com/webstore/detail/instant-data-scraper/ofaokhiedipichpaobibbnahnkdoiiah)
14. [Explain und Send](https://chrome.google.com/webstore/detail/explain-and-send-screensh/mdddabjhelpilpnpgondfmehhcplpiin?utm_source=chrome-ntp-icon)
15. [Chrome Themes Web Store](https://chrome.google.com/webstore/category/themes)
16. [Sputnik — An Open Source Intelligence Browser Extension](https://medium.com/bugbountywriteup/sputnik-an-open-source-intelligence-browser-extension-da2f2c22c8ec)
17. [CaseFile](https://www.paterva.com/web7/buy/maltego-clients/casefile.php)
----

### Firefox Extensions Productivity
1. [Google Translate for Firefox](https://addons.mozilla.org/en-US/firefox/addon/google-translator-for-firefox/)
2. [Exif Viewer](https://addons.mozilla.org/en-US/firefox/addon/exif-viewer/?)
3. [Image Search Options](https://addons.mozilla.org/en-US/firefox/addon/image-search-options/?)
4. [Resurrect Pages](https://addons.mozilla.org/en-US/firefox/addon/resurrect-pages/?)
5. [Nimbus Screen Capture](https://addons.mozilla.org/en-US/firefox/addon/nimbus-screenshot/?)
6. [Copy Plaintext](https://addons.mozilla.org/en-US/firefox/addon/copy-plaintext)
7. [JSON XML Viewer](https://addons.mozilla.org/en-US/firefox/addon/mjsonviewer/?)
8. [Copy selected URL](https://addons.mozilla.org/en-US/firefox/addon/copy-selected-links/?)
9. [Video Download Helper](https://addons.mozilla.org/en-US/firefox/addon/video-downloadhelper/?)
10. [Bulk Media Downloader](https://addons.mozilla.org/en-US/firefox/addon/bulk-media-downloader/?)
11. [IP Domain Info](https://addons.mozilla.org/en-US/firefox/addon/bulk-media-downloader/?)
12. [Multi Account Container](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/?)
13. [Download Star](https://addons.mozilla.org/en-US/firefox/addon/download-star/)
14. [unMHT](https://addons.mozilla.org/de/firefox/addon/save-page-we/?src=search)
----

### Investigation Reports
1. [Visualize Your Investigation](https://vis.occrp.org/)
2. [CaseFile](https://www.paterva.com/web7/buy/maltego-clients/casefile.php)
3. [eInvestigator](https://www.einvestigator.com/)
4. [Keep Google - Google notes](https://keep.google.com)
5. [Microsoft Steps Recorder](https://support.microsoft.com/en-us/help/22878/windows-10-record-steps)
----

### Acquisition: Facebook2RSS
1. [Inoreader Pro (€)](https://www.inoreader.com)
2. [Zapier (Facebook>RSS)](https://zapier.com/)
3. [RSS Generator](http://fetchrss.com/)
4. [RSS-Bridge](https://bridge.suumitsu.eu/)
5. [Feed Exileed](http://feed.exileed.com/)
6. [GitHub: khawkins98/facebook-json-to-rss (Self Hosting)](https://github.com/khawkins98/facebook-json-to-rss)
----

### Instagram Tools
1. [Instagram](https://instagram.com/)
2. [QuerryFeed](https://queryfeed.net/)
3. [Hashtagify](http://hashtagify.me/hashtag/smm)
4. [Tofo.me Instagram Viewer](https://tofo.me)
5. [Picodash](https://www.picodash.com)
6. [WEBSTAGRAM](https://websta.me)
7. [GramFly](https://gramfly.com/)
8. [DownloadGram](https://downloadgram.com/)
9. [Instadp](https://instadp.site/)
10. [Instagram Downloader](https://github.com/instaloader/instaloader)
11. [InstaSave - GitHub](https://github.com/alshf89/InstaSave)
12. [InstaLooter - GitHub](https://github.com/althonos/InstaLooter)
----

### Acquisition: Instagram2RSS
1. [RSS Box (Follow Username or Post) + (Self hosting)](https://rssbox.herokuapp.com/)
2. [Zapier (Instagram>RSS)](https://zapier.com/)
3. [Webstagram (Follow Accounts and #)](https://web.stagram.com/)
4. [RSS Feed Generator, Create RSS feeds from URL](https://rss.app/)
----

### People Finder Tools
1. [ThatsThem](https://thatsthem.com/)
2. [Pipl - People Search](https://pipl.com/)
3. [Recruit'em](https://recruitin.net)
4. [Username Search](https://usersearch.org)
5. [KnowEm](https://knowem.com)
6. [User Sherlock](http://www.usersherlock.com)
7. [TruePeopleSearch](https://www.truepeoplesearch.com)
8. [FamilyTree](http://www.familytreenow.com)
9. [Advanced Background Checks](http://www.advancedbackgroundchecks.com)
10. [ZoomInfo](http://www.zoominfo.com)
11. [FastPeopleSearch](https://www.fastpeoplesearch.com)
12. [Reverse Genie](http://www.reversegenie.com/)
13. [TruePeopleSearch](https://www.truepeoplesearch.com)
14. [People Search & Address](http://www.addresses.com/)
15. [AnyWho](https://www.anywho.com/whitepages)
16. [Nuwber](https://nuwber.com)
17. [Ancestry.com](http://search.ancestry.com/)
18. [FamilySearch](https://familysearch.org/search)
19. [How Many of Me](http://howmanyofme.com/search)
20. [FindMyPast](http://search.findmypast.com/search-world-records)
21. [WebMii](http://webmii.com)
22. [PeekYou](http://www.peekyou.com)
23. [Snoop Station](http://snoopstation.com/index.html)
24. [US Search](http://www.ussearch.com)
25. [Zaba Search](http://www.zabasearch.com)
----

### Genealogy Search
1. [Cyndi's List - List of Genealogical References](https://www.cyndislist.com/categories/)
2. [Legacy.com - Obituaries](http://www.legacy.com/)
3. [100% Free Family Tree and Genealogy Research - FamilyTreeNow.com](https://www.familytreenow.com/)
4. [Ancestry.com - Death Records, Certificates, Index &amp; Obituaries](https://www.archives.com/search/death)
5. [Ancestry.com - Obituary Collection](https://search.ancestry.com/search/obit/)
6. [Ancestry.com - SSDI Search - Social Security Death Index](https://search.ancestry.com/search/db.aspx?dbid=3693)
7. [Archives.com - Genealogy &amp; Ancestor Search | Family History Results](https://www.archives.com/search/ancestor)
8. [FamilySearch.org - SSDI Search](https://www.familysearch.org/search/collection/1202535)
9. [Genealogy Records Search Directory (by state)](http://publicrecords.onlinesearches.com/Genealogy-Records.htm)
10. [GenealogyBank - Social Security Death Index - Free Search](https://www.genealogybank.com/explore/ssdi/all)
11. [Iowa Genealogy Records Search Directory](https://publicrecords.onlinesearches.com/Iowa-Genealogy-Records.htm)
12. [Iowa Obits - Death Notices for 1992 – 2018](http://www.obitsarchive.com/obituaries/usa/iowa?)
13. [MyHeritageSearch - Vital Records and More!](https://www.myheritage.com/search-records?keyword=vital&utm_source=ppc_bing&utm_medium=cpc&utm_campaign=mh_bing-search_us_en_des_sup_broad_vital&utm_content=12470989103&utm_term=online+vital+statistics&tr_camp_id=53885626&tr_ad_group=online_vital_statistics&tr_ag_id=4959460691&tr_device=c&tr_account=+B004N5L2&msclkid=f59b5e5345911bfe448a04cf73d23c76&gclid=CPmrkdih09kCFeaPxQIdFjgKeQ&gclsrc=ds&dclid=CMrjodih09kCFcbIwAodiCoMfQ)
----

### Vehicle Tools
1. [VIN decoder](https://www.vindecoderz.com)
2. [AllVehicleData](http://www.allvehicledata.com)
3. [VINCheck®](https://www.nicb.org/how-we-help/vincheck)
4. [Cycle Vin](https://www.cyclevin.com/)
5. [RecordsFinder](https://recordsfinder.com/plate)
6. [TRAVIC](http://tracker.geops.ch/)
7. [Crashmap UK](https://www.crashmap.co.uk/)
8. [Roadcrash UK](https://www.roadcrash.co.uk/)
9. [My Car Check - UK](https://www.mycarcheck.com/)
10. [UKCampsite - UK](https://www.ukcampsite.co.uk/articles/view.asp?id=108)
----

### Classified Ads Search
1. [craigslist > sites](https://www.craigslist.org/about/sites)
2. [Search All Junk | Ever wanted to search multiple classifieds sites at once? | Home](http://www.searchalljunk.com/)
3. [site:craigslist.org &quot;Place name or number here&quot; - Google Search](https://www.google.com/search?q=site%3Acraigslist.org+%22Place+name+or+number+here%22&gws_rd=ssl)
4. [Harmari Search](https://www.harmari.com/search/unified/)
5. [expired craigslist ads | Harmari by LTAS Technologies](https://www.harmari.com/tag/expired-craigslist-ads/)
----

### SnapChat Tools
1. [SnapChat Maps](https://map.snapchat.com/@33.809200,-118.349000,12.00z)
2. [SnapCode](https://somesnapcode.com/)
3. [SoVIP - Warning: Illicit Content](https://sovip.io)
----

### Pinterest Tools
1. [PINGROUPIE](http://pingroupie.com)
----

### Acquisition: Pinterest2RSS
1. [https://www.pinterest.com/USERNAME/feed.rss](https://www.pinterest.com)
2. [www.pinterest.com/NAMEOFBOARD/feed.rss](https://www.pinterest.com)
3. [Zapier (Pinterest>RSS)](https://zapier.com/)
----

### LinkedIn Tools
1. [Raven - GitHub](https://github.com/0x09AL/raven)
----

### YouTube Tools
1. [YouTube Comment Scraper](http://ytcomments.klostermann.ca/)
2. [The YouTube Channel Crawler](http://channelcrawler.com/)
3. [YouTube Geo Search Tool](https://youtube.github.io/geo-search-tool/search.html)
4. [YouTube Related Videos Visualization](http://yasiv.com/youtube)
5. [YouTube DataViewer](https://citizenevidence.amnestyusa.org/)
----

### Periscope Tools
1. [Scopedown](http://downloadperiscopevideos.com)
2. [PeriSearch](http://www.perisearch.net/)
----

### Reddit Tools
1. [Reddit User Analyser](https://atomiks.github.io/reddit-user-analyser)
2. [SnoopSnoo](https://snoopsnoo.com)
3. [Reddit Archive](http://www.redditarchive.com)
4. [Mostly Harmless](http://kerrick.github.io/Mostly-Harmless/#features)
5. [Reddit Enhancement Suite](https://chrome.google.com/webstore/detail/reddit-enhancement-suite/kbmfpngjjgdllneeigpgjifpgocmfgmb)
6. [Reddit Metrics](http://redditmetrics.com)
7. [Imgur](https://imgur.com)
----

### SubReddits
1. [Subreddits](http://subreddits.org)
2. [Reddit Bureau of Investigation](https://www.reddit.com/r/RBI)
3. [What is This Thing?](https://www.reddit.com/r/whatisthisthing)
4. [Pic Requests](https://www.reddit.com/r/picrequests)
----

### WhatsApp Tools
1. [Fake WhatsApp Chat Generator](http://www.fakewhats.com/generator)
2. [WhatsAllApp - GitHub](https://github.com/LoranKloeze/WhatsAllApp)
----

### Misc IM Tools
1. [Yikmap](http://yikmap.com)
2. [Kik.me](http://kik.me/%3Cusername%3E)
----

### Telegram Tools
1. [Save Telegram Chat History](https://chrome.google.com/webstore/detail/save-telegram-chat-histor/kgldnbjoeinkkgpphaaljlfhfdbndfjc?hl=en)
2. [Skarlso/rscrap](https://github.com/Skarlso/rscrap)
3. [Telegram API for OSINT – Part 1 – Users](https://fabledowlblog.wordpress.com/2017/07/10/telegram-api-for-osint-part-1-users/)
4. [Telegram API for OSINT – Part 2 – Messages](https://fabledowlblog.wordpress.com/2017/09/09/telegram-api-for-osint-part-2-messages/)
----

### Tinder Tools
1. [Tinder](https://tinder.com/)
----

### Tumblr Tools
1. [Tumblr Originals](http://studiomoh.com/fun/tumblr_originals)
----

### VKontakte tools
1. [Retrieve Metadata - GitHub](https://gist.github.com/cryptolok/8a023875b47e20bc5e64ba8e27294261)
2. [Поиск сообществ](https://vk.com/communities)
3. [Поиск людей](https://vk.com/people)
4. [ВКонтакте RSS](http://vk-to-rss.appspot.com/)
5. [Фонарик. Таргетинг Вконтакте. Аналог Церебро-таргет.](http://spotlight.svezet.ru/)
6. [ТаргетоLOG — уникальный инструмент таргетирования](http://targetolog.com/)
7. [Шпион ВКонтакте](http://vk5.city4me.com/)
----

### Social Media Mgmt|Search
1. [Social Searcher](https://www.social-searcher.com)
2. [Social Mention](http://socialmention.com)
3. [Social-Searcher](http://www.social-searcher.com/google-social-search)
4. [BuzzSumo](https://app.buzzsumo.com/research/most-shared)
5. [SocialBlade](http://socialblade.com)
6. [TweetDeck](https://tweetdeck.twitter.com)
7. [Social Media Monitoring Wiki](http://wiki.kenburbary.com/social-meda-monitoring-wiki)
8. [Hoot Suite](https://hootsuite.com/)
----

### Public Records
1. [Public Records Online Search](https://publicrecords.onlinesearches.com/)
2. [OCCRP Data](https://data.occrp.org/)
3. [Free Public Records](http://publicrecords.searchsystems.net/)
4. [NETROnline](https://publicrecords.netronline.com/)
----

### Property Records
1. [Melissa Data Property Check](https://www.melissa.com/lookups/personaTorsearch.asp)
2. [Zillow](https://www.zillow.com)
3. [HomeFacts](https://www.homefacts.com)
4. [Neighbor.report](https://neighbor.report)
5. [Redfin](https://www.redfin.com)
6. [SkyscraperPage.com](http://skyscraperpage.com)
----

### Criminals | Crime Data
1. [Crime Data Explorer](https://crime-data-explorer.fr.cloud.gov)
2. [Family Watchdog](https://www.familywatchdog.us)
3. [Criminal Searches](https://www.criminalsearches.com)
4. [Felon Search](http://www.felonspy.com)
5. [FBI Most Wanted](https://www.fbi.gov/wanted)
6. [The Inmate Locator](http://theinmatelocator.com)
7. [Federal Bureau of Prisons](https://www.bop.gov/inmateloc)
8. [The Offender List](http://www.theoffenderlist.com)
9. [Crime Seen - U.S.](https://www.crimeseen.com/location/report_list)
10. [Culture Crime](https://www.anonymousswisscollector.com/culture-crime-news)
11. [CultureCrime Database](https://news.culturecrime.org/)
12. [Human Trafficking Search](http://humantraffickingsearch.org)
13. [Global Terrorism Database](http://www.start.umd.edu/gtd)
14. [Jihadists (UK Database)](https://www.bbc.com/news/uk-32026985)
15. [Blackbook Online](https://www.blackbookonline.info/criminalsearch.aspx)
16. [NSOPW](https://www.nsopw.gov/en/Search/Verification)
17. [VineLink](https://vinelink.com)
18. [CrimeReports™](https://www.crimereports.com)
19. [Mugshots.com](https://mugshots.com)
20. [Money Supermarket - UK](https://www.moneysupermarket.com/home-insurance/burglary-hotspots/)
21. [Sanctions Explorer](https://sanctionsexplorer.org/)
22. [Sanctions List - United Nations](https://www.un.org/securitycouncil/)
23. [Sanctions List - U.S.](https://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/fuzzy_logic.aspx)
24. [Sanctions List - Jersey](https://www.jerseyfsc.org/the-commission/general-information/public-statements/register-of-restricted-individuals/)
25. [Sanctions List - UK](https://www.gov.uk/government/publications/financial-sanctions-consolidated-list-of-targets/consolidated-list-of-targets)
26. [AIRO - Ireland](http://airo.maynoothuniversity.ie/mapping-resources/airo-research-maps/crime/recorded-crime-monitoring-tool)
27. [MissingPersons - UK](https://www.missingpersons.police.uk/en-gb/home)
28. [EU Most Wanted](https://eumostwanted.eu/)
29. [Interpol - Red Notices](https://www.interpol.int/How-we-work/Notices/View-Red-Notices)
30. [Mr. Watchlist](https://mrwatchlist.com/watchlists/law-enforcements-most-wanted/)
31. [Interpol - Missing Persons](https://www.interpol.int/How-we-work/Notices/View-Yellow-Notices)
32. [Help Us Find Someone - UK](https://www.missingpeople.org.uk/help-us-find)
33. [ActionFraud - UK](https://www.actionfraud.police.uk/news)
34. [Canadian Police Info Centre](http://www.cpic-cipc.ca/index-eng.htm)
35. [Catch A Thief - UK](https://www.catchathief.co.uk/)
36. [London Insurance Fraud Dept](https://www.cityoflondon.police.uk/advice-and-support/fraud-and-economic-crime/ifed/Pages/default.aspx)
37. [Financial Conduct Authority - UK](https://www.fca.org.uk/search-results?search_term=clone%20firm%20details&start=1&sort_by=dmetaZ)
38. [Conman Exposed - UK](https://www.conman-exposed.co.uk/)
39. [UK Crime Maps](https://www.police.uk/)
40. [Crime Prevention Inventory - Canada](https://www.publicsafety.gc.ca/cnt/cntrng-crm/crm-prvntn/index-en.aspx)
41. [Crime And Law - Australia](https://nt.gov.au/law)
42. [Crime Stoppers - Australia](https://crimestopperssa.com.au/)
43. [The CSSC - UK](https://www.thecssc.com/)
44. [Crime Stats And Data - UK](https://www.met.police.uk/sd/stats-and-data/)
45. [Fraud Help Desk - The Netherlands](https://www.fraudehelpdesk.nl/)
46. [Harass Map - Egypt](https://harassmap.org/en)
47. [Most Wanted - UK](https://crimestoppers-uk.org/give-information/most-wanted)
48. [National Crime Agency - UK](http://www.nationalcrimeagency.gov.uk/)
49. [NCA SAR Online System](https://www.ukciu.gov.uk/(nhmmuv55m5ci5vy2tdiybn2z)/saronline.aspx)
50. [Police Data - UK](https://data.police.uk/)
51. [Proscribed Terrorist Groups List - UK](https://www.gov.uk/government/publications/proscribed-terror-groups-or-organisations--2)
52. [Stolen Asset Recovery DB](https://star.worldbank.org/corruption-cases/?db=All)
----

### Reputation | Threat Intelligence
1. [URLquery](https://urlquery.net)
2. [RiskIQ Community](https://community.riskiq.com)
3. [URLvoid](https://www.urlvoid.com)
4. [Threat Crowd](https://www.threatcrowd.org)
5. [Malware Info Sharing Platform](http://www.misp-project.org)
6. [Malware Patrol](https://www.malwarepatrol.net)
7. [Project Honey Pot](https://www.projecthoneypot.org)
8. [Open Threat Intelligence](https://cymon.io)
9. [BotScout](http://botscout.com)
10. [Web Filter Lookup](https://fortiguard.com/webfilter)
11. [Trend Micro Site Safety Center](https://global.sitesafety.trendmicro.com)
12. [ReputationAuthority](http://www.reputationauthority.org)
13. [Sucuri Security](https://sitecheck.sucuri.net)
14. [IBM X-Force Exchange](https://exchange.xforce.ibmcloud.com)
15. [ThreatMiner](https://www.threatminer.org)
16. [Automated Malware Analysis](https://www.joesandbox.com)
17. [Cisco Talos Intelligence Group](https://talosintelligence.com)
18. [WebRoot BrightCloud](https://www.brightcloud.com/tools/url-ip-lookup.php)
19. [Malware Domain List](https://www.malwaredomainlist.com/mdl.php)
20. [Free Website Malware Scanner](https://app.webinspector.com)
21. [Spiderfoot](http://spiderfoot.net)
22. [Pentest Tools](https://pentest-tools.com/home)
23. [Threat Crowd](https://threatcrowd.org)
----

### Exploits & Advisories
1. [Flexera - Secunia Research](https://secuniaresearch.flexerasoftware.com/community/research/)
2. [CVECommon](https://cve.mitre.org/)
3. [CVE security vulnerability database](https://www.cvedetails.com/)
4. [National Vulnerability DB](https://nvd.nist.gov/)
5. [SecurityFocus](https://www.securityfocus.com/bid)
6. [Offensive Security](https://www.exploit-db.com/)
----

### Unclaimed Property
1. [Local Government GIS - Beacon](https://beacon.schneidercorp.com/)
2. [Unclaimed and Abandoned Property Search Directory](http://publicrecords.onlinesearches.com/Unclaimed-Property.htm)
3. [Unclaimed Property Administrators](https://www.unclaimed.org/)
4. [Unclaimed Property FREE SEARCH - Officially endorsed By The States, Provinces and Naupa](http://www.missingmoney.com/)
----

### Databases - Stolen properties
1. [Art and Culture crime database](http://news.culturecrime.org/all)
2. [Artloss](http://www.artloss.com/en)
3. [Check verkopergegevens (NL)](https://www.politie.nl/aangifte-of-melding-doen/controleer-handelspartij.html)
4. [Hotgunz (USA)](http://www.hotgunz.com/search.php)
5. [Iamstolen (UK)](http://www.iamstolen.com/)
----

### Resources / Hacks + Leaks
1. [Ashley Madison hacked email checker](https://ashley.cynic.al/)
2. [Data.Occrp](https://data.occrp.org/)
3. [Defacer](https://defacer.id/)
4. [DeHashed](https://dehashed.com)
5. [Have I been pwned](https://haveibeenpwned.com)
6. [Have I been pwned - password](https://haveibeenpwned.com/Passwords)
7. [The No More Ransom Project](https://www.nomoreransom.org/)
8. [Offshore Leaks - Panama Papers](https://offshoreleaks.icij.org/)
9. [Pastebin dump collection](https://psbdmp.ws//)
10. [Politie - check of uw gegevens gestolen zijn (NL)](https://www.politie.nl/themas/controleer-of-mijn-inloggegevens-zijn-gestolen.html)
11. [Scan files for Malware](https://virusscan.jotti.org/)
12. [ScatteredSecrets](https://scatteredsecrets.com/)
13. [Which ransomware infected me?](https://id-ransomware.malwarehunterteam.com/)
14. [WikiLeaks](https://wikileaks.org/)
15. [Zone-H - Archive](http://www.zone-h.org/archive)
----

### Court Records
1. [Court Locator | United States Courts](http://www.uscourts.gov/court-locator)
2. [Court Records Directory](http://www.courtreference.com/)
3. [Court Records Directory](http://publicrecords.searchsystems.net/Free_Public_Records_by_Type_of_Record/Court_Records/)
4. [Court Records Search Directory](http://publicrecords.onlinesearches.com/Courts.htm)
5. [Free County Court Records Search](https://www.blackbookonline.info/USA-County-Court-Records.aspx)
6. [Federal Court Locator](http://www.uscourts.gov/court-locator)
7. [Federal Court Opinions Decisions - BRB](http://www.brbpub.com/freeresources/pubrecsitesSearch.aspx?subcat=Federal+Courts%27+Opinions+%26+Decisions)
8. [Iowa Courts Online Search](https://www.iowacourts.state.ia.us/ESAWebApp/DefaultFrame)
9. [Iowa Judicial Branch](https://www.iowacourts.gov/)
10. [On Demand Court Records](http://www1.odcr.com/)
11. [PACER](https://www.pacer.gov/)
12. [Advanced RECAP Archive Search for PACER – CourtListener.com](https://www.courtlistener.com/recap/)
13. [Court Electronic Records PACER](https://www.pacer.gov/psco/cgi-bin/links.pl)
14. [State Appellate & Supreme Courts](http://www.brbpub.com/freeresources/pubrecsitesSearch.aspx?subcat=State+Appellate+%26+Supreme+Court)
15. [The Tribal Court Clearinghouse](http://www.tribal-institute.org/index.htm)
16. [US Tax Court](https://www.ustaxcourt.gov/UstcDockInq/Default.aspx?PartyName)
17. [courthousedirect.com/](http://www.courthousedirect.com/)
18. [State Public Records Laws](https://www.muckrock.com/place)
19. [Canadian Legal Information Institute](https://www.canlii.org/en)
20. [Causelist - UK](http://causelist.org/)
21. [World Legal Information Institute (WorldLII)](http://worldlii.org)
----

### Scams
1. [Scamdex](http://www.scamdex.com/)
2. [BBB Scam Tracker℠](https://www.bbb.org/scamtracker/us/)
----

### Honeypots
1. [HoneyDB](https://riskdiscovery.com/honeydb/)
2. [foospidy/HoneyPy](https://github.com/foospidy/HoneyPy)
3. [NoThink!](http://www.nothink.org/honeypots.php)
----

### Malware - Info
1. [Malware Forensic Field Guides](http://www.malwarefieldguide.com/)
2. [MalwareTips](https://malwaretips.com/)
----

### Malware - Ransomware
1. [Free Ransomware Decryptors - Kaspersky Lab](https://noransom.kaspersky.com/)
2. [Ransomware Tracker](https://ransomwaretracker.abuse.ch/)
3. [The No More Ransom Project](https://www.nomoreransom.org/en/index.html)
----

### Malware - Research
1. [#totalhash](https://totalhash.cymru.com/)
2. [abuse.ch | Fighting malware and botnets](https://abuse.ch/)
3. [AVCaesar (by Malware.lu)](https://avcaesar.malware.lu/)
4. [Barracuda Labs Threatglass](http://threatglass.com/)
5. [Digital Certificates Used by Malware](http://www.ccssforum.org/malware-certificates.php)
6. [DNS-BH](http://www.malwaredomains.com/?page_id=66)
7. [Exploitalert](https://exploitalert.com/)
8. [Feodo Tracker](http://feodotracker.abuse.ch)
9. [Fortinet Threat Encyclopedia](https://fortiguard.com/encyclopedia)
10. [Hybrid Analysis](https://www.hybrid-analysis.com/)
11. [Joe Sandbox](https://www.joesandbox.com/)
12. [Kaspersky Labs](https://threats.kaspersky.com/)
13. [malc0de DNS Sinkhole](http://malc0de.com/bl/)
14. [MalShare](https://malshare.com/)
15. [Maltiverse](https://www.maltiverse.com/)
16. [Malware Domain List](https://www.malwaredomainlist.com/)
17. [Malware.md](https://gist.github.com/rjmolesa/34c6a299c17a486ea80f)
18. [MalwareConfig](http://malwareconfig.com)
19. [Offensive Security’s Exploit Database Archive](https://www.exploit-db.com/)
20. [Open Threat Intelligence](https://cymon.io/)
21. [OWASP File Hash Repository](https://www.owasp.org/index.php/OWASP_File_Hash_Repository)
22. [Sucuri Security - malware entries](http://labs.sucuri.net/?malware)
23. [Sucuri Security - malware signatures](http://labs.sucuri.net/?malwaredb)
24. [Threat Descriptions](https://www.f-secure.com/en/web/labs_global/threat-descriptions)
25. [Threat Grid](http://www.threatgrid.com)
26. [VirusTotal](https://www.virustotal.com/)
27. [WSTNPHX Malware Email Addresses](https://raw.githubusercontent.com/WSTNPHX/scripts-n-tools/master/malware-email-addresses.txt)
28. [ZeuS Tracker](https://zeustracker.abuse.ch/)
----

### Malware - File Analysis
1. [Any Run](https://app.any.run/)
2. [Jotti's malware scan](https://virusscan.jotti.org/en)
3. [OPSWAT MetaDefender File Analysis](https://metadefender.opswat.com/#!/)
4. [PDFExaminer: pdf malware analysis](https://www.pdfexaminer.com/)
5. [Quicksand.io Document Analyzer](https://www.quicksand.io/)
6. [VirSCAN.org](http://www.virscan.org/)
7. [VirusShare-related archives](https://a4lg.com/downloads/vxshare/)
8. [VirusShare.com](https://virusshare.com/)
9. [Malicious PDF Analysis](https://zeltser.com/tools-for-malicious-pdf-analysis/)
----

### Malware - Blogs / Resources
1. [BAE Systems Threat Research Blog](http://baesystemsai.blogspot.de/)
2. [Blaze's Security Blog](https://bartblaze.blogspot.com)
3. [Cisco's Talos Intelligence Group Blog](http://blog.talosintel.com/)
4. [malware@prevenity blog](http://malware.prevenity.com/)
5. [Malwarebytes Forums](https://forums.malwarebytes.com/)
6. [malwaretracker.com](https://malwaretracker.com/)
7. [S!Ri.URZ](https://siri-urz.blogspot.com/)
8. [Malwarebytes Labs Blog](http://blog.malwarebytes.com/)
----

### Self Assessment: Port | Browser Privacy test
1. [Panopticlick](https://panopticlick.eff.org/)
2. [Device Info ***](https://www.deviceinfo.me/)
3. [BrowserAudit ***](https://browseraudit.com)
4. [BrowserLeaks.com](https://browserleaks.com/)
5. [Detectmybrowser.com](https://detectmybrowser.com/)
6. [Am I unique?](https://amiunique.org/)
7. [JonDonym IP check](http://ip-check.info/?lang=en)
8. [Check & Secure Port Browser + Win](https://www.check-and-secure.com/start/)
9. [Shields UP! — Internet Vulnerability Profiling](https://grc.com/x/ne.dll?bh0bkyd2)
10. [How's My SSL/TLS?](https://www.howsmyssl.com/)
11. [Robinlinus Your Social Media Fingerprint Check](https://robinlinus.github.io/socialmedia-leak/)
12. [BrowserSpy.dk](http://browserspy.dk/)
13. [Raymondhill.net Are Cookies blocked Test](http://raymondhill.net/httpsb/httpsb-test-cookie-1.php)
14. [Raymondhill.net HAR-parser What is your blocker NOT blocking?](http://raymondhill.net/httpsb/har-parser.html)
15. [Whoishostingthis What's My User Agent?](http://www.whoishostingthis.com/tools/user-agent/)
16. [Qualys BrowserCheck (Also as a Chrome Add On)](https://browsercheck.qualys.com/)
----

### Search Engines
1. [iSEEK - Web](http://www.iseek.com/iseek/home.page)
2. [Million Short - What haven&#39;t you found?](https://millionshort.com/)
3. [Social Searcher - Free Social Media Search Engine](https://www.social-searcher.com/)
4. [Welcome - Investigative Dashboard Search](https://data.occrp.org/)
5. [Google](http://google.com)
6. [DuckDuckGo](https://duckduckgo.com)
7. [Bing](https://bing.start.me/?a=gsb_startme_00_00_ssg01)
8. [Ask.com](https://www.ask.com)
9. [InfoSpace](http://infospace.com)
10. [Yahoo Search](https://yahoo.start.me/?a=wsp_startme_00_00_ssg02)
11. [Dothop](http://dothop.com/home)
12. [Surf Canyon Search](http://www.surfcanyon.com)
13. [GoodSearch](https://www.goodsearch.com)
14. [Lycos](http://www.lycos.com)
15. [Mojeek](https://www.mojeek.com)
16. [AOL Search](http://search.aol.com)
17. [Gigablast](http://gigablast.com)
18. [Advangle](http://advangle.com)
19. [Bing vs. Google](http://bvsg.org)
20. [iSeek](http://iseek.com)
21. [Carrot2 Clustering](http://search.carrot2.org/stable/search)
22. [NerdyData](https://nerdydata.com)
23. [Source Code Search](https://publicwww.com)
24. [Scribd](https://www.scribd.com)
----

### Int'l Search Engines
1. [Alleba Filipino](http://www.alleba.com)
2. [Ansearch - Australia](http://www.ansearch.com.au)
3. [Baidu - China](http://www.baidu.com)
4. [Daum - S. Korea](https://www.daum.net)
5. [Eniro - Sweden](https://www.eniro.se)
6. [Goo - Japan](https://www.goo.ne.jp)
7. [Najdi - Slovenia](http://www.najdi.si)
8. [Naver - S. Korea](https://www.naver.com)
9. [Onet – Poland](https://www.onet.pl)
10. [Orange - France](http://www.orange.fr)
11. [Parseek - Iran](http://www.parseek.com)
12. [SAPO - Portugal](http://www.sapo.pt)
13. [The Swiss Search Engine](https://www.search.ch)
14. [Walia - Israel](https://www.walla.co.il)
15. [Yandex - Russia](https://www.yandex.com)
----

### MetaSearch Engines
1. [All The Internet](https://www.alltheinternet.com)
2. [Biznar - Federated Search](https://biznar.com/biznar/desktop/en/search.html)
3. [SearX](https://searx.me)
4. [Yippy](http://yippy.com)
5. [Mamma](https://www.mamma.com/)
6. [iBoogie](http://iboogie.com)
7. [Vroosh](http://www.vroosh.com)
8. [Search](https://www.search.com)
9. [Unabot](http://unabot.com)
10. [Info.com](http://info.com)
11. [Ixquick](https://www.ixquick.com)
----

### Specialty Search Engines
1. [SEARCH | ISP List](http://www.search.org/resources/isp-list/)
2. [2lingual Google Search](https://www.2lingual.com)
3. [Citeseer - Computer Literature](http:// http://citeseer.ist.psu.edu/index)
4. [Harmari Ads Search](https://www.harmari.com/search/unified)
5. [Boardreader - Forums](http://boardreader.com)
6. [Internet Archive: Digital Library](https://archive.org)
7. [WorldWideScience](https://worldwidescience.org)
8. [Shodan](https://www.shodan.io)
9. [Urlscan.io](https://urlscan.io/search/#*)
10. [Redirect Detective](http://redirectdetective.com)
11. [Analyze ID](http://analyzeid.com)
12. [Think Tank Search](http://guides.library.harvard.edu/hks/think_tank_search)
----

### Search Tips
1. [16 Google Advanced Search Tricks - Easy To Use](http://www.searchtricks.net/google-advanced-search/)
2. [Searchtricks - Advanced Search Operators](https://www.facebook.com/searchtricks)
----

### Planes-Aviation Tools
1. [FlightAware](https://flightaware.com)
2. [JetPhotos](https://www.jetphotos.com)
3. [Flightradar24](https://www.flightradar24.com)
4. [Air Cargo Tracking](http://www.track-trace.com/aircargo)
5. [RadarBox](https://www.radarbox24.com)
6. [PlaneFinder](https://planefinder.net)
7. [WikiRoutes](https://wikiroutes.info/en)
8. [N2YO](http://www.n2yo.com)
9. [ADS-B Exchange](https://www.adsbexchange.com/)
10. [World Aeronautical Database](http://worldaerodata.com)
----

### Marine Tools
1. [Live Ships Map - AIS - Vessel Traffic and Positions - AIS Marine Traffic](http://www.marinetraffic.com/en/ais/home/centerx:-70/centery:45/zoom:6)
2. [BoatInfoWorld.com - Search, View &amp; Download Vessel Information](http://www.boatinfoworld.com/)
3. [MarineTraffic](http://www.marinetraffic.com)
4. [BoatInfoWorld](https://www.boatinfoworld.com)
5. [Live Cruise Ship Tracker](https://www.livecruiseshiptracker.com)
6. [Vessel Finder](https://www.vesselfinder.com)
7. [Container Tracking](http://www.track-trace.com/container)
8. [OpenSea Map](http://www.openseamap.org)
9. [Ship Tracker](https://shiptracker.shodan.io/)
10. [Vessel Tracker](http://www.vesseltracker.com/app)
----

### Trains
1. [OpenRailwayMap](https://www.openrailwaymap.org)
2. [Deutsche Bahn](http://data.deutschebahn.com)
----

### Maps, Geolocation Tools
1. [Google Earth](https://www.google.com/earth)
2. [Follow Your World](https://followyourworld.appspot.com)
3. [Create a map | BatchGeo](https://batchgeo.com/)
4. [Instant Google Street View](https://www.instantstreetview.com)
5. [Streetview Player](http://brianfolts.com/driver)
6. [LandsAtLook](https://landsatlook.usgs.gov)
7. [GPS Visualizer: Quick Geocoder](http://www.gpsvisualizer.com/geocode)
8. [Zoom.earth](https://zoom.earth/#0,0,3z,sat,pm,2018-01-22)
9. [OpenStreetCam](https://www.openstreetcam.org)
10. [GeoVisual: Search](https://search.descarteslabs.com)
11. [Dual Maps](http://data.mashedworld.com/dualmaps/map.htm)
12. [Terrapattern](http://www.terrapattern.com)
13. [TerraServer](https://www.terraserver.com)
14. [Land Viewer | EOS](https://eos.com/landviewer)
15. [Military Grid System](https://dominoc925-pages.appspot.com/mapplets/cs_mgrs.html)
16. [Radius Around a Point on a Map](https://www.freemaptools.com/radius-around-point.htm)
17. [Find Address of any Location](https://ctrlq.org/maps/address)
18. [Bing Maps](http://Bing.com/maps)
19. [All Trails](https://www.alltrails.com/)
20. [Coordinates Converter](http://synnatschke.de/geo-tools/coordinate-converter.php)
21. [DigitalGlobe](http://discover.digitalglobe.com)
22. [EarthExplorer](https://earthexplorer.usgs.gov)
23. [Geograph](http://geograph.org)
24. [Geonames](http://geonames.org)
25. [Satimaging | IKONOS](https://www.satimagingcorp.com/gallery/ikonos/)
26. [HERE WeGo](https://wego.here.com)
27. [Sentinel-hub Playground](http://apps.sentinel-hub.com/sentinel-playground)
28. [GetLatLong](https://getlatlong.net/)
29. [Wikimapia](http://wikimapia.org)
30. [Yandex.Maps](https://yandex.com/maps)
31. [US Naviguide](http://www.usnaviguide.com)
32. [YourNavigation](http://www.yournavigation.org)
33. [OpenRailwayMap](http://www.openrailwaymap.org)
34. [Official MapQuest](https://www.mapquest.com)
35. [Historic Aerials](http://www.historicaerials.com/?javascript=&)
36. [NEXRAD Data Inventory](https://www.ncdc.noaa.gov/nexradinv)
37. [Sentinel2Look Viewer](https://landsatlook.usgs.gov/sentinel2)
38. [HiveMapper](https://hivemapper.com)
39. [OpenStreetMap](http://www.openstreetmap.org/#map=5/51.500/-0.100)
40. [Corona @ CAST UA](http://corona.cast.uark.edu)
41. [Distance Between UK Postcodes](https://www.freemaptools.com/distance-between-uk-postcodes.htm)
42. [Naver - Korean](http://map.naver.com)
43. [Daum - Korean](http://map.daum.net)
44. [Baidu - Asian](http://map.baidu.com)
----

### Geo-Based Searches
1. [Warwire](http://warwire.net)
2. [Ukraine Interactive map](http://liveuamap.com)
----

### Dating Sites
1. [Match.com®](https://www.match.com)
2. [Tinder](https://tinder.com)
3. [FirstMet Online Dating](https://www.firstmet.com)
4. [Plenty of Fish](http://www.pof.com)
5. [eHarmony](https://www.eharmony.com)
6. [FarmersOnly](https://www.farmersonly.com)
7. [Zoosk](https://www.zoosk.com)
8. [OkCupid](https://www.okcupid.com)
9. [Wamba](https://wamba.com)
10. [AshleyMadison.com®](https://www.ashleymadison.com)
11. [BeautifulPeople](https://www.beautifulpeople.com)
12. [Badoo](https://badoo.us)
13. [BlackPeopleMeet](https://www.blackpeoplemeet.com)
14. [LatinAmericanCupid](https://www.latinamericancupid.com)
15. [LatinoPeopleMeet](https://www.latinopeoplemeet.com)
16. [Amigos](https://amigos.com)
17. [EastMeetEast](https://www.eastmeeteast.com)
18. [AsianDating](https://www.asiandating.com)
19. [2redbeans](https://www.2redbeans.com)
----

### POF (Plenty Of Fish) Tools
1. [POF Dating on the App Store](https://itunes.apple.com/app/apple-store/id389638243?mt=8)
2. [POF Free Dating App - Android Apps on Google Play](https://play.google.com/store/apps/details?id=com.pof.android&feature=search_result&hl=en&referrer=utm_source%253Dpofweb_link_1%2526utm_medium%253DCampaignID=0)
3. [Spokeo (searches POF, Match.com... by e-mail address)](https://www.spokeo.com/)
4. [POF advanced search](http://www.pof.com/advancedsearch.aspx)
5. [POF.com username search](http://www.pof.com/basicusersearch.aspx)
6. [Plenty Of Fish • r/POF](https://www.reddit.com/r/POF/)
7. [A POF search string that you can examine and customize](http://www.pof.com/advancedsearch.aspx?iama=f&seekinga=m&minage=47&maxage=49&searchtype=&interests=giants&country=1&state=40&City=West+New+York&miles=5&cmdSearch=Begin+Interest+Search&Profession=)
8. [Search by interests	(change "pickling")](http://www.pof.com/interests/pickling.aspx)
----

### OK Cupid Tools
1. [OkCupid Dating on the App Store](https://itunes.apple.com/app/apple-store/id338701294?mt=8)
2. [OkCupid Dating - Android Apps on Google Play](https://play.google.com/store/apps/details?id=com.okcupid.okcupid&referrer=utm_source%253Ddesktophome%2526utm_medium%253Dweb)
3. [OkCupid](https://www.okcupid.com/)
4. [OKCupid search string to customize](https://www.okcupid.com/online-dating/new-york/new-york/)
5. [OkCupid on reddit • r/OkCupid](https://www.reddit.com/r/OkCupid/)
6. [Using OkCupid's Search Filters - YouTube](https://www.youtube.com/watch?v=Qboq-I4PvGc)
7. [OKCupid Scams: All About Catfish Scams on OKCupid.com](https://socialcatfish.com/blog/okcupid-scams-everything-about-being-catfished-scams-on-okcupid-com/)
----

### Photo, Image Tools
1. [Google Images](https://images.google.com)
2. [TinEye Reverse Image Search](https://www.tineye.com)
3. [Stolen Camera Finder](https://www.stolencamerafinder.com/)
4. [YouTube DataViewer](https://citizenevidence.amnestyusa.org/)
5. [PicSearch](http://www.picsearch.com)
6. [Digital Image Forensic Analyzer](http://www.imageforensic.org)
7. [Karma Decay](http://karmadecay.com)
8. [Wolfram Language Image ID Project](https://www.imageidentify.com)
9. [Forensically](https://29a.ch/photo-forensics)
10. [FotoForensics](http://fotoforensics.com)
11. [RootAbout](http://rootabout.com)
12. [Pictriev, face search engine](http://www.pictriev.com)
13. [How-Old](https://www.how-old.net/#)
----

### Flickr
1. [Flickr](https://www.flickr.com/)
2. [Flickr Search](https://www.flickr.com/search/people/?username=)
3. [Flickr: Explore everyone&#39;s photos on a Map](https://www.flickr.com/map)
4. [Free Username Search - Lookup any username on UserSherlock.com](http://usersherlock.com/)
5. [idGettr — Find your Flickr ID](https://www.webpagefx.com/tools/idgettr/)
6. [Flickr Mapping - Flickr Mapping](http://www.mypicsmap.com/)
----

### Photo forensics
1. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
2. [FotoForensics OSIT](http://fotoforensics.com/)
3. [Digital Image Forensic Analyzer](http://www.imageforensic.org/)
4. [gbimg.org](http://gbimg.org/)
5. [Image Forensics](http://www.errorlevelanalysis.com/)
6. [ExifTool by Phil Harvey](http://www.sno.phy.queensu.ca/~phil/exiftool/)
7. [Paliscope](https://www.paliscope.com/)
8. [Find exif data](http://www.findexif.com/)
9. [Extract camera information from any Jpeg (Jpg) image file](https://camerasummary.com/)
10. [Forensically, free online photo forensics tools](https://29a.ch/photo-forensics/#error-level-analysis)
11. [Reveal - Image Verification Assistant](http://reveal-mklab.iti.gr/reveal/)
12. [InVID project Image Verification plus Chrome Extension](http://www.invid-project.eu/)
13. [SmartDeblur](http://smartdeblur.net/)
14. [PhotoScissors Background Removal Tool](https://online.photoscissors.com/)
15. [remove.bg Remove Image Background](https://www.remove.bg/)
16. [redaelli/imago-forensics](https://github.com/redaelli/imago-forensics)
17. [Metadataanalysis (Win)](https://www.metadataanalysis.com/)
----

### Video Tools
1. [DownSub](http://downsub.com)
2. [Deturl](http://deturl.com)
3. [KeepVid Video Download](https://keepvid.com/)
4. [Clip Converter](https://www.clipconverter.cc/de/)
5. [DownFacebook](http://www.downfacebook.com/)
6. [Scopedown](https://downloadperiscopevideos.com/index.php)
7. [Verify Eyewitness Video](https://library.witness.org/product/video-as-evidence-verifying-eyewitness-video/)
----

### Image, Video, Text Metadata Tools
1. [Amnesty YouTube Dataviewer](https://citizenevidence.amnestyusa.org/)
2. [ExifTool](https://www.sno.phy.queensu.ca/~phil/exiftool)
3. [FotoForensics](https://fotoforensics.com)
4. [Image Forensics](https://29a.ch/photo-forensics/#level-sweep)
5. [Invid-Project](http://www.invid-project.eu/tools-and-services/invid-verification-plugin)
6. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
7. [Reveal Image Verification](http://reveal-mklab.iti.gr/reveal/index.html)
8. [SmartDeblur](http://smartdeblur.net)
9. [FOCA](https://www.elevenpaths.com/labstools/foca/index.html)
10. [Verexif - remove exif data from pictures](http://www.verexif.com/en/)
11. [Spiderpig - GitHub](https://github.com/hatlord/Spiderpig)
12. [ExifTool by Phil Harvey](https://sno.phy.queensu.ca/~phil/exiftool)
13. [theXifer](https://www.thexifer.net)
----

### Documents
1. [Google Docs](https://www.google.com/?q=site:docs.google.com+%22keyword%22)
2. [Cryptome](https://cryptome.org)
3. [Free-OCR](http://www.free-ocr.com)
4. [File Converter — Convertio](https://convertio.co)
5. [CIA Declassified Archives](https://www.muckrock.com/news/archives/2017/sep/22/crest-search-guide)
----

### IP Addresses
1. [Geo IP Tool](https://geoiptool.com)
2. [IP Locator](https://www.ipfingerprints.com)
3. [IPverse](http://ipverse.net)
4. [IP2location](https://www.ip2location.com/demo/47.152.41.139)
5. [IP Geolocation API](https://db-ip.com)
6. [IPintel](https://ipintel.io)
7. [IP Location Finder](https://www.iplocation.net)
8. [Info Sniper](http://www.infosniper.net)
9. [Information by IP Address](https://www.infobyip.com)
10. [IP Tracker](https://www.iptrackeronline.com)
11. [IPV6locator](http://ipv6locator.net)
12. [Reverse IP Lookup](https://www.ipfingerprints.com/reverseip.php)
13. [Reverse IP](https://dnslytics.com/reverse-ip)
14. [Grabify IP Logger](https://grabify.link)
15. [IP Logger](https://iplogger.com)
16. [CloudFail - GitHub](https://github.com/m0rtem/CloudFail)
17. [Torrent downloads](https://iknowwhatyoudownload.com)
----

### PDF Search
1. [PDF SEARCH ENGINE](http://www.findpdfdoc.com/)
2. [PDF Search Engine 2](http://www.pdfsearchengine.info/)
3. [Free Full PDF - Scientific Publications](http://www.freefullpdf.com/#gsc.tab=0)
4. [PDF Search Engine 3](http://www.sopdf.com/)
----

### Who.is/domain/ip info
1. [Afrinic (Africa)](http://africnic.net)
2. [Apnic (Asia, Australia and New Zealand)](http://apnic.net)
3. [Apnic - WhoWas](https://www.apnic.net/static/whowas-ui/)
4. [Arin (North America)](http://arin.net)
5. [BuiltWith](https://builtwith.com/)
6. [Censys.io](http://censys.io)
7. [Centralops.net](http://centralops.net)
8. [CertDB (SSL certificates)](http://certdb.com)
9. [Cloudflare - Crimeflare](http://crimeflare.biz/)
10. [Cloudflare - IP resolver](https://iphostinfo.com/cloudflare/)
11. [Crt.sh (SSL certificates)](https://crt.sh/)
12. [DNSdumpster](https://dnsdumpster.com/)
13. [DNSlytics.com](https://dnslytics.com)
14. [DomainBigData.com - Online investigation tools](http://domainbigdata.com)
15. [Domain history](http://whoisrequest.com/history/)
16. [Domain History](http://domainhistory.net/)
17. [Domain History](https://completedns.com/dns-history/)
18. [DomainIQ](https://www.domainiq.com)
19. [Domaintools.com](https://domaintools.com)
20. [Eurid (.eu domains)](https://eurid.eu/en/)
21. [FindSubdomains.com](https://findsubdomains.com/)
22. [GeoIP](https://geoiptool.com/)
23. [I Know What You Download](https://iknowwhatyoudownload.com/en/peer/)
24. [IPVoid](http://www.ipvoid.com/)
25. [Lacnic (South America)](http://lacnic.net)
26. [Network-tools](http://network-tools.com)
27. [Ripe (Europe, Greenland, Russia and Middle East)](http://ripe.net/)
28. [Robtex](http://robtex.com/)
29. [Securitytrails.com](http://securitytrails.com/)
30. [Should I Trust](https://github.com/ericalexanderorg/should-i-trust)
31. [SIDN (.nl domains)](http://sidn.nl)
32. [SiteSleuth](https://www.sitesleuth.io)
33. [SpyOnWeb](http://spyonweb.com)
34. [Subdomain scanner](https://pentest-tools.com/information-gathering/find-subdomains-of-domain)
35. [URLScan.io](https://urlscan.io)
36. [ViewDNS.info](http://viewdns.info)
37. [W3Bin (hosting information)](https://w3bin.com/)
38. [Webotheek.nl - (NL)](https://webotheek.nl/)
39. [WHOIS Search](https://who.is)
40. [WHOIS Search (ICANN)](http://whois.icann.org/en)
41. [Whois.Easycounter (historical domain info)](https://whois.easycounter.com/)
42. [Who.is GoDaddy](https://ca.godaddy.com/whois)
43. [Who.is Tucows](https://www.tucowsdomains.com/whois-search/)
44. [WhyNoHTTPS](https://whynohttps.com/)
45. [VisualSiteMapper](http://www.visualsitemapper.com)
46. [Timer4Web (paid)](http://timer4web.com)
47. [Carbondate.cs.odu.edu](http://carbondate.cs.odu.edu/)
----

### Domain Archives
1. [Archive.org](https://archive.org/about)
2. [Cached Pages](http://www.cachedpages.com)
3. [Archive.is](http://archive.is)
4. [Visualping](https://visualping.io)
5. [FollowThatPage](https://followthatpage.com)
6. [ScreenShots](http://www.screenshots.com)
----

### PassiveDNS
1. [RiskIQ Community](https://community.riskiq.com)
2. [SecurityTrails](https://securitytrails.com)
3. [DNS History](https://dnshistory.org)
4. [PTRarchive](http://ptrarchive.com)
----

### Pastebins
1. [Pastebin Search by IntelTechniques](https://inteltechniques.com/osint/menu.pastebins.html)
2. [Pastebin](https://pastebin.com)
3. [PasteLert](https://andrewmohawk.com/pasteLert)
4. [Dump Monitor](https://twitter.com/dumpmon)
5. [Sniff-Paste](https://github.com/needmorecowbell/sniff-paste)
----

### RSS Searches
1. [RSS Search Engine](http://ctrlq.org/rss/)
2. [RSS Micro](http://www.rssmicro.com/)
----

### Social Networks RSS Searches
1. [Iconosquare](https://pro.iconosquare.com/)
2. [Queryfeed](http://www.queryfeed.net/)
----

### Certificate Search
1. [Certificate Transparency](https://www.certificate-transparency.org/known-logs)
2. [Censys](https://censys.io)
3. [crt.sh](https://crt.sh)
4. [Certgraph](https://github.com/lanrat/certgraph)
----

### Legal Tools
1. [Justice Manual](https://www.justice.gov/usam/united-states-attorneys-manual)
2. [World Legal Information Institute](http://worldlii.org)
3. [CanLII](https://www.canlii.org/en)
4. [CourtListener](https://www.courtlistener.com/recap)
----

### Domain Blacklists
1. [DNS-BH](http://www.malwaredomains.com/wordpress/?page_id=66)
2. [ZeusTracker](https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist)
3. [Blackweb - GitHub](https://github.com/maravento/blackweb)
4. [CriticalStack](https://intel.criticalstack.com)
----

### FTP & Code Search
1. [SearchCode](https://searchcode.com/)
2. [NAPALM FTP Indexer](http://www.searchftps.net/)
3. [Dorks - GitHub](https://github.com/techgaun/github-dorks)
4. [Gitrob - Git Hub](https://github.com/michenriksen/gitrob)
----

### Typo-squatting
1. [DNStwister](https://dnstwister.report)
2. [URLCrazy](https://www.morningstarsecurity.com/research/urlcrazy)
3. [DNStwist - GitHub](https://github.com/elceef/dnstwist)
4. [Catphish - GitHub](https://github.com/ring0lab/catphish)
----

### Privacy Tools
1. [ipleak.net](https://ipleak.net/)
2. [Sucuri Security](http://sitecheck.sucuri.net)
3. [BrowserLeaks.com](https://browserleaks.com/)
4. [OptOut Doc by @OsintNinja](https://docs.google.com/spreadsheets/d/1UY9U2CJ8Rnz0CBrNu2iGV3yoG0nLR8mLINcnz44XESI/edit#gid=1864750866)
5. [Privacy Tools](https://epic.org/privacy/tools.html)
6. [Proxysnel](http://proxysnel.nl)
7. [TwoFactorAuthentication](https://twofactorauth.org/)
8. [Safeweb.norton.com](http://safeweb.norton.com)
9. [Should I Trust](https://github.com/ericalexanderorg/should-i-trust)
10. [Terms of Service; Didn't Read](https://tosdr.org/)
11. [What's my User Agent?](http://www.whatsmyua.info/)
12. [Pribot (AI-powered Privacy/Policies)](https://pribot.org/)
13. [Downforeveryoneorjustme](http://downforeveryoneorjustme.com/)
14. [Mijnonlineidentiteit (NL)](https://www.mijnonlineidentiteit.nl/social-media-privacy-instellingen/)
----

### Router Security Basic Checklist
1. [WiGLE: Wireless Network Mapping](https://wigle.net/)
2. [Router Security](http://routersecurity.org)
3. [Router-FAQ.de](https://www.router-faq.de/)
----

### URL Shortners
1. [Bit.do](https://bit.do)
2. [Bit.ly](http://bit.ly)
3. [CheckShortUrl](http://checkshorturl.com/)
4. [Unfurlr](https://unfurlr.com/)
5. [Unshorten.it](http://unshorten.it)
6. [Unshorten.link](https://unshorten.link/)
7. [Grabify IP Logger](https://grabify.link/)
----

### Self Assessment: E-Mail Privacy test
1. [heise online](https://www.heise.de/security//dienste/emailcheck/html-mails/webbug/)
2. [Email Leak Tests](http://emailipleak.com/)
3. [TLS Tests and Tools](https://www.checktls.com/)
4. [Email Privacy Tester ***](https://www.emailprivacytester.com/)
----

### DNS & Sub-Domains
1. [Domain Dossier](https://centralops.net/co/DomainDossier.aspx)
2. [ViewDNS](https://viewdns.info)
3. [Whoisology](https://whoisology.com/#advanced)
4. [Reverse Whois](https://www.whoxy.com/reverse-whois/)
5. [WhoisAmped](https://whoisamped.com)
6. [WhoIsHostingThis](https://www.whoishostingthis.com)
7. [Whois-RWS](https://whois.arin.net/ui/advanced.jsp)
8. [DNSdumpster.com](https://dnsdumpster.com)
9. [DomainBigData](https://domainbigdata.com)
10. [Netintel](https://netintel.net)
11. [DomainTools](https://whois.domaintools.com/)
12. [CentralOps Free online network tools](https://centralops.net/co/)
13. [Domain Crawler](http://www.domaincrawler.com)
14. [PubDB](http://pub-db.com)
15. [DNS SecurityTrails](https://dnstrails.com)
16. [DomainsDB](https://domainsdb.info)
17. [TCPIPUTILS](https://www.tcpiputils.com)
18. [DailyChanges](http://dailychanges.domaintools.com)
19. [Robtex](https://www.robtex.com)
20. [EasyWhois](https://www.easywhois.com)
21. [WHOIS Search](https://who.is)
22. [DomainIQ](https://www.domainiq.com)
23. [Website Informer](https://website.informer.com)
24. [Find Subdomains](https://pentest-tools.com/information-gathering/find-subdomains-of-domain)
25. [theHarvester](http://www.edge-security.com/theharvester.php)
26. [Recon-ng](https://bitbucket.org/LaNMaSteR53/recon-ng)
27. [Sken](https://www.sken.io/)
28. [Aquatone - GitHub](https://github.com/michenriksen/aquatone)
29. [Xray](https://github.com/evilsocket/xray)
30. [DNSrecon](https://github.com/darkoperator/dnsrecon)
31. [GoBuster-GitHub](https://github.com/OJ/gobuster)
32. [Fierce-Domain-Scanner](https://github.com/davidpepper/fierce-domain-scanner)
----

### VPNs
1. [Express VPN](https://www.expressvpn.com)
2. [NordVPN](https://nordvpn.com)
3. [IPVanish](https://www.ipvanish.com)
4. [Hotspot Shield](https://www.hotspotshield.com)
5. [CyberGhost VPN](https://www.cyberghostvpn.com)
6. [TunnelBear VPN](https://www.tunnelbear.com)
7. [TunnelBear VPN](https://www.tunnelbear.com)
8. [VyprVPN](https://www.vyprvpn.com)
9. [ProtonVPN](https://protonvpn.com/)
10. [Private Internet Access](https://www.privateinternetaccess.com/)
----

### Self Assessment: VPN & Router Leak test
1. [DNS leak test](https://www.dnsleaktest.com/)
2. [WebRTC Leak Test](https://www.browserleaks.com/webrtc)
3. [IP/DNS Detect ***](https://ipleak.net)
4. [Geolocate IP Address Lookup](http://www.iptrackeronline.com/locate-ip-on-map-mini.php?lang=1)
5. [How to disable ipv6 on Mac OSX](http://help.unc.edu/help/how-do-i-disable-ipv6-on-mac-os-x/)
6. [How to disable IPv6 or its components in Windows](https://support.microsoft.com/en-us/kb/929852)
----

### Sock Puppet Tools
1. [Fake Name Generator](https://www.fakenamegenerator.com)
2. [Secure Fake Name Creator](https://fakena.me)
3. [Fake WhatsApp Chat Generator](http://www.fakewhats.com/generator)
4. [Fake Identity Generator](http://backgroundchecks.org/justdeleteme/fake-identity-generator)
5. [Random User Generator](https://randomuser.me)
6. [Email-Fake](https://email-fake.com/)
7. [Fiverr: Acquire Fake Followers](http://www.Fiverr.com)
8. [Namechk](https://namechk.com)
9. [Acquire Fake Likes](http://www.Bootlikes.com)
10. [NameFake](https://en.namefake.com/)
11. [MiniRandom](https://minirandom.com/)
12. [Secure Fake Name Creator](https://fakena.me/)
13. [Fake Identity Generator](http://www.datafakegenerator.com/generador.php)
14. [Online Name Generator](https://online-generator.com/index.php)
15. [Random User Generator](https://randomuser.me/)
16. [Avatar Maker](https://avatarmaker.com/)
17. [The Avatar Maker](http://www.theavatarmaker.com/)
18. [Avachara](https://avachara.com/avatar/)
19. [Kartunix](https://www.kartunix.com/)
20. [Cartoonize](http://www.cartoonize.net/avatar.htm)
21. [FaceMaker](http://facemaker.uvrg.org/)
22. [Random Profile Photos](https://randomuser.me/photos)
23. [Face Plus](https://www.faceplusplus.com/face-merging/#demo)
24. [Morph Thing](http://www.morphthing.com/morph)
25. [MySudo](https://mysudo.com/)
----

### Social Networks
1. [Facebook](https://www.facebook.com)
2. [Twitter](https://twitter.com)
3. [Instagram](https://www.instagram.com)
4. [Pinterest](https://www.pinterest.com)
5. [LinkedIn](https://www.linkedin.com)
6. [Tumblr](https://www.tumblr.com)
7. [reddit](https://www.reddit.com)
8. [Mastodon](https://mastodon.social/about)
9. [gab | Free Speech Social Network](https://gab.ai)
10. [Minds](https://www.minds.com)
11. [Crabgrass](https://crabgrass.riseup.net)
12. [Myspace](https://myspace.com)
13. [Voat](https://voat.co)
14. [4chan](http://www.4chan.org)
15. [LiveJournal](https://www.livejournal.com)
16. [Meetup](https://www.meetup.com)
17. [Vero](https://www.vero.co)
18. [Befilo](https://befilo.com)
19. [Twoo](https://www.twoo.com)
20. [Opportunity](https://myopportunity.com)
21. [Ello](https://ello.co)
22. [Woddal](https://www.woddal.com)
23. [Hello Network](https://www.hello.com/en/index.html)
24. [Oneway](https://oneway.com)
25. [EyeEm](https://eyeem.com)
26. [Care2](https://www.care2.com)
27. [BlackPlanet](http://www.blackplanet.com)
28. [My Muslim Friends](http://mymfb.com)
29. [World Map of Social Networks](https://vincos.it/world-map-of-social-networks/)
----

### Int'l Social Media Sites
1. [OK.RU](https://ok.ru/)
2. [VK - Russia](https://vk.com)
3. [Badoo](https://badoo.com/)
4. [RenRen - China](http://renren.com)
5. [QZone - China](http://www.qq.com)
6. [Taringa! - Latin America](https://www.taringa.net)
7. [Draugiem - Latvia](https://www.draugiem.lv)
8. [Facenama - Iran](https://facenama.com/home)
9. [Mixi - Japan](https://mixi.jp)
10. [hi5 - Asia, E. Europe, Africa](https://secure.hi5.com)
11. [TikTok - China, Asia](https://www.tiktok.com)
12. [My Muslim Friends](http://mymfb.com)
----

### Law Enforcement - ISP|Social Media
1. [Facebook Contacts & Procedures](https://www.facebook.com/safety/groups/law/guidelines/)
2. [Twitter - Law Enforcement](https://help.twitter.com/en/rules-and-policies/twitter-law-enforcement-support#)
3. [Google Law Enforcement FAQ](https://support.google.com/transparencyreport/answer/7381738/)
4. [Instagram Info For Law Enforcement](https://help.instagram.com/494561080557017/)
5. [Pinterest: Law Enforcement](https://help.pinterest.com/en/article/law-enforcement-guidelines)
6. [SnapChat - Law Enforcement](https://storage.googleapis.com/snap-inc/privacy/lawenforcement.pdf)
7. [MSN - Law Enforcement](https://info.publicintelligence.net/MSN-Compliance.pdf)
8. [WeChat - Law Enforcement](https://www.wechat.com/en/law_enforcement_data_request.html)
9. [Verizon Oath - Transparency Report](https://transparency.oath.com/?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cudWstb3NpbnQubmV0Lw&guce_referrer_sig=AQAAAHBuYTp_7PX1dUR0vJ8p4PRR5-olkuuCY4Hu5J6M84-eINaghytyCrW8XIoIcnvisEZZR6ChTF7J0LruQjOoxbJa2Xpg8_h-N0ChN7q090IuD_ebjKys6cl8MUjv8ci3Q_kj6ExKKI1UgVCjOZp3C1KHyI4FNFT4hc035_IdyCU2)
10. [ISP Law Enforcement Details - Search](https://www.search.org/resources/isp-list/)
----

### Forum|Blog|IRC Search
1. [Boardreader](http://boardreader.com)
2. [Omgili (Oh My God I Love It)](http://omgili.com)
3. [Craigslist Forums](https://forums.craigslist.org)
4. [Delphi Forums](https://www.delphiforums.com)
5. [Yahoo Groups](https://groups.yahoo.com/neo/search)
6. [Blog Search Engine](http://www.blogsearchengine.org)
7. [Mibbit IRC Search](https://search.mibbit.com)
8. [IRCsnapshot - GitHub](https://github.com/bwall/ircsnapshot)
9. [IRC Search](http://irc.netsplit.de/channels/search.php)
----

### Journalists
1. [Journalist's Toolbox](https://www.journaliststoolbox.org/)
2. [Gijn.org](https://gijn.org/2018/04/03/deep-web-journalists-7-tips-using-occrp-data/)
3. [MuckRock](https://www.muckrock.com)
4. [Search FOIA Requests](https://www.muckrock.com/foi/list/?q=antifa&status=done&user-autocomplete=&agency-autocomplete=&jurisdiction-autocomplete=&jurisdiction=52-True&projects-autocomplete=&tags-autocomplete=&has_embargo=&has_crowdfund=&minimum_pages=&date_range_0=&date_range_1=&file_types=&search_title=&utm_content=buffer82d57&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
5. [Collaborative Fact Checker](https://meedan.com/en/check/)
6. [Verification Handbook](http://verificationhandbook.com/)
7. [CIA Declassified Archives](https://www.muckrock.com/news/archives/2017/sep/22/crest-search-guide)
8. [Fakespot](http://fakespot.com)
9. [Plagiarism Checker](https://smallseotools.com/plagiarism-checker)
10. [Document Redactions - GitHub](https://github.com/firstlookmedia/pdf-redact-tools)
11. [Fact-checking resources](https://www.americanpressinstitute.org/training-tools/fact-checking-resource)
12. [Research Clinic](http://www.researchclinic.net/)
13. [Reporters Lab](http://reporterslab.org/fact-checking)
14. [Africa Check](https://africacheck.org)
15. [Hoaxy](https://hoaxy.iuni.iu.edu)
16. [Copyscape Plagiarism Checker](http://copyscape.com)
17. [Full Fact - UK](https://fullfact.org/)
18. [FactCheck – Channel 4 News UK](https://www.channel4.com/news/factcheck)
19. [Alt News](https://www.altnews.in/)
----

### Open Data | FOAI | Wobbing |Freedom of Information
1. [Data Journalism Handbook](http://datajournalismhandbook.org/1.0/en/)
2. [Europe and Open Data Portal](https://www.europeandataportal.eu/)
3. [OPen Knowledge Foundation (GER)](http://opendata-network.org/)
4. [The Weekly Online Betting Blog](http://www.wobbing.eu/)
5. [Mission](http://www.opendataportal.at/mission/)
6. [Open Data Tools](http://opendata-tools.org/en/)
7. [Datenjournalist](http://datenjournalist.de/)
8. [Data Driven Journalism Resources](http://datadrivenjournalism.net/resources)
9. [Datenjournalist | Open Data & data-driven-journalism](http://datenjournalist.de/)
10. [Open Data Showroom](http://opendata-showroom.org/de/)
11. [D3.js - Data-Driven Documents](http://d3js.org/)
12. [Mike Bostock Data Visualisation](http://bost.ocks.org/mike/)
----

### Business Info
1. [Bloomberg Stock Market Info](https://www.bloomberg.com/markets/stocks)
2. [Corporation Wiki](https://www.corporationwiki.com)
3. [Commercial Register - Worldwide](https://www.commercial-register.sg.ch/home/worldwide.html)
4. [SEC - EDGAR](https://www.sec.gov/edgar.shtml)
5. [International White and Yellow Pages](http://www.wayp.com)
6. [UK Companies](https://www.gov.uk/get-information-about-a-company)
7. [Global Resource Directory](https://globaledge.msu.edu/global-resources)
8. [Ripoff Report](https://www.ripoffreport.com)
9. [Google Finance](https://www.google.com/finance)
10. [State Business Records](https://opencorporates.com)
----

### Business Records
1. [Annual Reports](http://www.annualreports.com)
2. [ReportLinker](https://www.reportlinker.com)
3. [Annual Report Service](http://www.annualreportservice.com)
4. [Companies House: International](https://www.gov.uk/government/publications/overseas-registries/overseas-registries)
5. [Free Corporation Search](https://www.blackbookonline.info/USA-Corporations.aspx)
----

### Company Profiles
1. [Business Information Resources](http://www.rba.co.uk/sources)
2. [OpenCorporates](https://opencorporates.com)
3. [Corporation Wiki](https://www.corporationwiki.com)
4. [Data.com Connect](https://connect.data.com)
5. [Business Lookup](http://www.dobsearch.com/business-lookup)
6. [ZoomInfo](https://www.zoominfo.com/companies-search)
7. [Kompass](https://www.kompass.com/selectcountry)
8. [InfoBel](https://www.infobel.com)
9. [Manta](https://www.manta.com/business)
10. [The Company Database](https://www.aihitdata.com)
11. [Buzzfile](https://www.buzzfile.com/Home/Basic)
12. [LittleSis](https://littlesis.org)
13. [Companies House](https://beta.companieshouse.gov.uk)
14. [Hoovers](http://www.hoovers.com)
15. [CorporateInformation](https://corporateinformation.com)
16. [Europages](https://www.europages.co.uk)
17. [Glassdoor Company Reviews](https://www.glassdoor.com/Reviews/index.htm)
18. [Owler Inc.](https://www.owler.com)
19. [Vault](http://www.vault.com)
20. [D&B](https://www.dnb.com)
21. [CompaniesInTheUK](https://www.companiesintheuk.co.uk)
22. [UK Companies List](https://www.companieslist.co.uk)
23. [Orbis Directory](https://orbisdirectory.bvdinfo.com/version-20181213/OrbisDirectory/Companies)
24. [Crunchbase](https://www.crunchbase.com)
----

### Business Crime, Compliance, Incidents
1. [Dept Treasury Enforcement Actions Search](https://apps.occ.gov/EASearch/)
2. [EPA - Cases and Settlements | Enforcement](https://www.epa.gov/enforcement/cases-and-settlements)
3. [Aleph data search](https://data.occrp.org/)
4. [ICIJ Offshore Leaks Database String](https://www.google.com/search?q=%22XX%22+AND+site%3Ahttps%3A%2F%2Foffshoreleaks.icij.org&oq=%22XX%22+AND+site%3Ahttps%3A%2F%2Foffshoreleaks.icij.org&aqs=chrome..69i57.13022j0j7&sourceid=chrome&ie=UTF-8)
5. [EPA - Civil Cases and Settlements](https://cfpub.epa.gov/compliance/cases/)
6. [Export Database Searches (Restrictions List)](https://2016.export.gov/ecr/eg_main_023148.asp)
7. [FDA Debarment List (Drug Product Applications)](https://www.fda.gov/ICECI/EnforcementActions/FDADebarmentList/default.htm)
8. [FDA Recalls, Market Withdrawals, &amp; Safety Alerts](https://www.fda.gov/safety/recalls/default.htm)
9. [FDIC: Enforcement Decisions and Orders - Search Form](https://www.fdic.gov/news/news/press/2018/pr18010a.html)
10. [Federal Reserve Enforcement Actions](https://www.federalreserve.gov/apps/enforcementactions/search.aspx)
11. [FINRA Disciplinary Actions Online | FINRA.org](https://www.finra.org/industry/finra-disciplinary-actions-online)
12. [Foreign Agents Registration Search](https://efile.fara.gov/pls/apex/f?p=185:1:0::::P1_DISPLAY:)
13. [Foreign Corrupt Practices Act](https://www.justice.gov/sites/default/files/criminal-fraud/legacy/2012/11/14/fcpa-english.pdf)
14. [GCRD International | Welcome to GCRD.](https://www.gcrd.info/index.php?id=3814)
15. [HHS - Exclusions Database](https://exclusions.oig.hhs.gov/)
16. [ICIJ Offshore Leaks Database](https://offshoreleaks.icij.org/)
17. [Individuals Barred by FINRA - A | FINRA.org](https://www.finra.org/industry/individuals-barred-finra-a)
18. [Occupational Safety and Health Administration (OSHA) - Establishment Search](https://www.osha.gov/pls/imis/establishment.html)
19. [OSHA - Fatality and Catastrophe Investigation Summaries | Occupational Safety and Health Administration](https://www.osha.gov/pls/imis/accidentsearch.html)
20. [OSHA Searches](https://www.osha.gov/oshstats/index.html)
21. [Rip Off Report](https://www.ripoffreport.com/)
22. [SCAC | Securities Class Action Clearinghouse | Home](http://securities.stanford.edu/)
23. [SEC.gov  - Enforcement Actions](https://www.sec.gov/divisions/enforce/enforceactions.shtml)
24. [SEC.gov | Enforcement](https://www.sec.gov/litigation.shtml)
25. [StAR Stolen Asset Recovery Database (International)](https://star.worldbank.org/corruption-cases/?db=All)
26. [SWAMP Index](http://swamp.coalitionforintegrity.org/)
27. [TOXMAP | TRI and Superfund Environmental Maps](https://toxmap.nlm.nih.gov/toxmap/)
28. [US Nonproliferation List](https://www.state.gov/t/isn/c15231.htm)
29. [EEOC](http://EEOC Decisions .com)
----

### Cheatsheets & Flowcharts Tools
1. [Browsershots](http://browsershots.org)
2. [Browsershots (screenshot via mobile device)](http://browsershots.net/)
3. [Cheatsheet Bing search (NL)](https://drive.google.com/file/d/0B7IvVbyx3btlajl2aDJrVVJIOUE/view)
4. [Cheatsheet Duckduckgo search (NL)](https://drive.google.com/file/d/0B7IvVbyx3btldkpYYmRySndVQTA/view)
5. [Cheatsheet Yandex search(NL)](https://drive.google.com/file/d/0B7IvVbyx3btldktiNC1YelZaXzA/view)
6. [Databasic](http://databasic.io)
7. [Datawrapper](http://datawrapper.de)
8. [Digital Methods Initiative Tool database](https://wiki.digitalmethods.net/Dmi/ToolDatabase)
9. [Flowchart Domains](https://inteltechniques.com/data/Domain.png)
10. [Flowchart Location search](https://inteltechniques.com/data/location.png)
11. [Flowchart OSINT](https://inteltechniques.com/menu.links.html)
12. [Listly](https://listly.io/)
13. [MindMup](http://mindmup.com)
14. [PDFmyURL](http://pdfmyurl.com)
15. [SANS - Sec487 - Blue teaming wiki](https://github.com/sans-blue-team/sec487-wiki/blob/master/index.md)
16. [Station](https://getstation.com/)
----

### Android emulators
1. [Nox](https://www.bignox.com/)
2. [Bluestacks](https://www.bluestacks.com/nl/index.html)
3. [Genymotion](https://www.genymotion.com/)
4. [Leapdroid](https://leapdroid.en.softonic.com/)
----

### Translation/Language
1. [Cyrcillic decoder](https://2cyr.com/decode/?lang=en)
2. [Ethnologue](https://www.ethnologue.com/)
3. [DeepL Translator](https://www.deepl.com/translator)
4. [FlockWatch](https://github.com/sjacks26/FlockWatch)
5. [Google Translate](http://translate.google.com)
6. [Know Your Meme](http://knowyourmeme.com/)
7. [NewOCR](https://www.newocr.com/)
8. [TagDef](https://tagdef.com)
9. [Urban Dictionairy](https://www.urbandictionary.com/)
10. [Yamii](https://www.yamli.com/arabic-keyboard/)
----

### Tools - Virtual Machine
1. [Buscador OSINT VM](http://inteltechniques.com/buscador)
2. [Kali Linux](https://www.kali.org/)
3. [Linux Mint](https://linuxmint.com/download.php)
4. [Linux Ubuntu](https://www.ubuntu.com/download/desktop)
5. [VirtualBox (free)](https://www.virtualbox.org/wiki/Downloads)
6. [VMWare (paid)](https://www.vmware.com/)
----

### Employee Profiles
1. [RecruitEm](https://recruitin.net)
2. [LinkedIn](https://www.linkedin.com)
3. [MarketVisual Search](http://www.marketvisual.com)
4. [JobBird](https://www.jobbird.com)
5. [Indeed](https://www.indeed.com)
6. [CVgadget](https://www.cvgadget.com)
7. [LeadFerret](https://leadferret.com/search)
----

### Online Cameras
1. [Insecam](http://www.insecam.org)
2. [TrafficLand](http://www.trafficland.com)
3. [Dronestagram](http://www.dronestagr.am)
4. [EarthCam](http://www.earthcam.com)
5. [Overview – Google Earth](https://www.google.com/earth)
6. [Opentopia](http://www.opentopia.com)
7. [Thingful](https://www.thingful.net)
8. [DroneTheWorld](https://dronetheworld.com/)
9. [OpenStreetCam](http://openstreetcam.org)
10. [EarthExplorer](https://earthexplorer.usgs.gov)
----

### Miscellaneous
1. [Privoxy -Web Proxy Tool](http://www.privoxy.org)
2. [Passport Index](https://www.passportindex.org)
3. [Wolfram|Alpha](https://www.wolframalpha.com)
4. [Art Loss Register](http://www.artloss.com)
5. [Culture Crime DB](http://news.culturecrime.org/all)
6. [Lumen](https://lumendatabase.org)
7. [Timeline JS](https://timeline.knightlab.com)
8. [Online Free Hash Identification](https://www.onlinehashcrack.com/hash-identification.php)
9. [iTrace](http://itrace.conflictarm.com/Home/Login)
10. [Bitcoin Block Explorer](https://blockexplorer.com)
----

### Military Records
1. [Military Sites - BRB](https://www.brbpublications.com/freeresources/pubrecsitesSearch.aspx?Type=9)
2. [Military Records | Black Book Online](https://www.blackbookonline.info/Military.aspx)
3. [Military Service Records Online, by Mail, or by Fax | National Archives](https://www.archives.gov/veterans/military-service-records)
4. [Military Active Duty Status Report](https://scra.dmdc.osd.mil/scra/#/single-record)
----

### Pubs| Grey Lit Search
1. [Science Publications](http://www.thescipub.com)
2. [The Open Syllabus Project](http://explorer.opensyllabusproject.org/)
3. [Page Press](http://www.pagepress.org)
4. [Lazy Scholar](http://www.lazyscholar.org/)
5. [A-Z Databases](http://guides.uflib.ufl.edu/az.php)
6. [PQDT Open](http://pqdtopen.proquest.com/search.html)
7. [ScienceDirect](http://www.sciencedirect.com)
8. [Microsoft Academic](https://academic.microsoft.com/)
9. [OpenGrey](http://opengrey.eu/)
10. [UK National Archives](http://discovery.nationalarchives.gov.uk/)
11. [HathiTrust Digital Library](https://www.hathitrust.org/)
12. [JURN](http://jurn.org/#gsc.tab=0)
13. [World Digital Library](https://www.wdl.org/en/)
14. [Open Library](https://openlibrary.org)
15. [Ssrn.com](http://ssrn.com/en/)
16. [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/)
17. [Google Scholar](https://scholar.google.com)
18. [BASE](https://www.base-search.net/Search/Advanced)
19. [PubPeer](https://pubpeer.com)
20. [Westminster Library - UK](https://www.westminster.gov.uk/libraries)
----

### Registries
1. [TheKnot](https://www.theknot.com/registry/couplesearch)
2. [RegistryFinder](https://www.registryfinder.com)
3. [MyRegistry](https://www.myregistry.com)
4. [AmazonRegistry](https://www.amazon.com/gp/registry/search)
5. [BedBathAndBeyond Registry](https://www.bedbathandbeyond.com/store/giftregistry/registry_search_guest.jsp)
6. [The Bump Baby Registry](https://registry.thebump.com/babyregistrysearch)
----

### Political | Campaign Contributions
1. [Political Money Line](http://politicalmoneyline.com)
2. [Melissa Campaign Contributions](http://melissadata.com/lookups/fec.asp)
3. [Follow The Money](https://www.followthemoney.org)
4. [Disclosure Quest](https://disclosurequest.com)
5. [OpenSecrets](https://www.opensecrets.org)
----

### Voter Records
1. [Voter Records](https://voterrecords.com)
2. [Voter Registration Records](https://www.blackbookonline.info/USA-Voter-Records.aspx)
----

### Weather/time
1. [Everytimezone](http://everytimezone.com/)
2. [MoonCalc](https://www.mooncalc.org/)
3. [SunCalc](http://suncalc.net)
4. [Sun-Path map](http://andrewmarsh.com/apps/releases/sunpath-on-map.html)
5. [Ventusky (weather)](https://www.ventusky.com/)
6. [Wisuki](http://wisuki.com/)
----

### Website Scraping Crawling
1. [Scrapy](https://scrapy.org/)
2. [Data Miner](https://data-miner.io/)
3. [ZAPinfo™ (Formerly WebClipDrop) - Information Automation Tool](https://www.zapinfo.io/)
4. [Scrapinghub: Visual scraping with Portia](https://scrapinghub.com/portia)
5. [OutWit Hub - Find, grab and organize all kinds of data and media from](http://www.outwit.com/products/hub/)
6. [Web Scraper](http://webscraper.io/)
7. [ScraperWiki](https://scraperwiki.com/)
8. [80legs](http://www.80legs.com/)
9. [Mozenda](http://www.mozenda.com/)
10. [Visual Web Ripper (fee based)](http://www.visualwebripper.com/)
11. [Web scrapers](https://docs.google.com/a/mediaquest.pl/spreadsheet/ccc?key=0AjJXuorsDqYgdFpvcEJpdFUxY3JFQXZlcHhuVWNCUnc&usp=drive_web#gid=0)
12. [Free Crawl Tool, Google Sitemap Generator](http://tools.seochat.com/tools/online-crawl-google-sitemap-generator/)
13. [Create your Google Sitemap Online](https://www.xml-sitemaps.com/)
14. [New Website Downloader](https://websitedownloader.io/)
15. [Screaming Frog SEO Spider Tool & Crawler Software](https://www.screamingfrog.co.uk/seo-spider/)
16. [iMacros](https://imacros.net/)
17. [Import.io](https://www.import.io/)
18. [Webrecorder](https://webrecorder.io/)
19. [OutWit Hub - Find, grab and organize all kinds of data and media from online sources.](https://www.outwit.com/products/hub/)
20. [QuickCode](https://quickcode.io/)
21. [IRobotSoft -- Visual Web Scraping and Web Automation](http://irobotsoft.com/)
22. [FAW - Forensics Acquisition of Websites | The first forensic browser for the acquisition of web pages](https://en.fawproject.com/)
----

### SSL Certificate Search
1. [CertDB — SSL certificates search engine](http://certdb.com/)
2. [crt.sh SSL Certificate Search](https://crt.sh/)
----

### Fee Based Resources
1. [Adressermittlung Einwohnermeldeamt Anfrage (GER)](http://www.adressermittlung.de/)
2. [Arachnys](https://app.arachnys.com/accounts/login/)
3. [Cedar-rose.com MENA Intel](https://www.cedar-rose.com/)
4. [China Checkup](https://www.chinacheckup.com/)
5. [ClarifiedBy MENA Intel](https://www.clarifiedby.com/)
6. [Coface: for trade](http://www.coface.de/)
7. [Corporation Wiki (D&B)](https://www.corporationwiki.com/)
8. [Creditreform  Personenauskunft (GER)](https://consumer.boniversum.com/login.jsf)
9. [Creditreform Deutschland (GER)](https://online.creditreform.de/ssoapplicationweb/jsp/anmeldung/anmeldungNormal.jsf;jsessionid=8BIIIim5c2n7FTUBTGbjOxIdCkwDnkedRR6-Jv-KCX4S0hN94R3F!1575301128?TID=8ed964e3-f6e6-447a-a40a-5c39c0f2f015)
10. [Creditriskmonitor](https://www.creditriskmonitor.com/)
11. [CRIFBÜRGEL Skyminder (GER)](https://www.buergel-online.de/netconnect-web2/)
12. [D&B  Business Credit Bureau](https://www.dnb.com/)
13. [D&B Global Reference Solution](https://solutions.dnb.com/grs/)
14. [Discoverandromeda.com UK Intel](https://discoverandromeda.com/)
15. [Dow Jones Factiva](https://signin.wsj.com/bundle/)
16. [Dow Jones Factiva Clear Cookies](https://djlogin.dowjones.com/xsiddel.asp?Domain=all)
17. [Dow Jones Risk & Compliance PEP](https://djrc.dowjones.com/login.aspx)
18. [Equifax Business Credit Bureau](https://www.equifax.com/personal/)
19. [Experian Business Credit Bureau](https://www.experian.com/access/login.html)
20. [Experian Marketing-, Daten- und Analyse](http://www.experian.de/)
21. [FCIB  National Association of Credit Management](http://fcibglobal.com/)
22. [Findyr.com Validate On-Location Information](https://www.findyr.com/)
23. [GENIOS Quick Search (GER)](https://www.genios.de/)
24. [Graydon](https://www.graydon.co.uk/)
25. [Handelsregister Deutschland (GER)](https://www.handelsregister.de/rp_web/welcome-members.do;jsessionid=BE0A60EEEE0BD1140C00402C22957467-n1.tc032n01)
26. [Handelsregister HILFE (GER)](https://www.handelsregister.de/rp_web/help.do?Thema=suchergebnismaske)
27. [ICP Credit Credit reports for emerging world markets](http://www.icpcredit.com/)
28. [Infocreditworld](https://www.infocreditworld.com/)
29. [Infogreffe (France)](https://www.infogreffe.com/)
30. [Lexis Diligence®](https://www.lexisnexis.com/dd/auth/signonform.do)
31. [Lexis World Compliance Lexis Nexis Risk](https://members.worldcompliance.com/SignIn.aspx?ReturnUrl=%252f)
32. [BvD Markus Bureau van Dijk](https://markus.bvdinfo.com/version-201727/home.serv?product=markusneo)
33. [Moneyhouse.CH](https://www.moneyhouse.ch/)
34. [Moneyhouse.de (GER)](https://www.moneyhouse.de/)
35. [BvD Orbis Bureau van Dijk](https://orbis.bvdinfo.com/version-2017224/home.serv?product=orbisneo)
36. [Pipl.com Professional](https://pipl.com/accounts/login)
37. [Sayari Analytics MENA CHINA](https://sayari.com/)
38. [SEC.gov](https://www.sec.gov/edgar.shtml)
39. [Serasa Experian Brasil](https://www.serasaexperian.com.br)
40. [Skopenow Social Media Investigations](https://www.skopenow.com/)
41. [SPARK - RF](http://www.spark-interfax.ru/en)
42. [Supercheck (GER)](http://www.supercheck.de/)
43. [Transunion Business Credit Bureau](https://www.transunion.com/)
44. [World-Check Thompson Reuters](https://www.world-check.com/frontend/login/)
45. [Worldbox](https://www.worldbox.net/)
46. [Youcontrol UKRAINE](https://youcontrol.com.ua/en/)
47. [Info-clipper.com](http://www.info-clipper.com/en/)
----

