### Docs, Videos, Blogs, Podcasts and Training
1. [Microsoft Word - OSINT_Handbook_June 2018_Final.docx](https://www.i-intelligence.eu/wp-content/uploads/2018/06/OSINT_Handbook_June-2018_Final.pdf)
2. [IntelTechniques Blog](https://inteltechniques.com/blog/category/osint/)
3. [Automating OSINT Blog](http://www.automatingosint.com/blog/)
4. [Sector035 Blog](https://sector035.nl/)
5. [Jake Creps OSINT Podcast](http://osintpodcast.com/)
6. [OSINT Curious Podcast](https://osintcurio.us/webcast/)
7. [Michael Bazzell OSINT Podcast](https://inteltechniques.com/podcast.html)
----

### Twitter & LinkedIn Tools
1. [Tinfoleak](https://tinfoleak.com/)
2. [TweetBeaver](https://tweetbeaver.com/)
3. [Twimap](https://twimap.com/)
4. [OmniSci Tweetmap](https://www.mapd.com/demos/tweetmap/)
5. [Twlets](http://twlets.com/)
6. [Follower Wonk](https://followerwonk.com/analyze)
7. [Twiangulate](http://twiangulate.com/search/)
8. [Social Bearing](https://socialbearing.com/)
9. [Foller Analytics](https://foller.me/)
10. [LinkedIn User Search by Email URL](https://www.linkedin.com/sales/gmail/profile/viewByEmail/ReplaceThis@byEmailDomain.com)
11. [Social Analyzer Plugin](https://chrome.google.com/webstore/detail/socialanalyzer-social-sen/efeikkcpimdfpdlmlbjdecnmkknjcfcp)
----

### Instagram & Facebook Tools
1. [Facebook Graph Tool Beta](http://graph.tips/beta/)
2. [Paul Myers FB Graph Links](http://researchclinic.net/facebook/facebook.html)
3. [Facebook Ad Library](https://www.facebook.com/ads/library/?active_status=all&ad_type=political_and_issue_ads&country=US&impression_search_field=has_impressions_lifetime)
4. [Facebook Matrix](https://plessas.net/facebookmatrix)
5. [Hashatit](https://www.hashatit.com/)
6. [FB Video Downloader](https://www.getfvid.com/)
7. [Instagram Video Downloader](https://instafinsta.com/)
----

### Image and Metadata Tools
1. [ExifPro Image Viewer](http://exifpro.com/)
2. [MetaData Viewer](http://exif.regex.info/exif.cgi)
3. [Forensically, free online photo forensics tools](https://29a.ch/photo-forensics/#level-sweep)
4. [Photo Forensics](http://fotoforensics.com/)
5. [Online exif data viewer](https://www.get-metadata.com/)
6. [Ghiro](http://www.getghiro.org/)
7. [The Wolfram Language Image Identification Project](https://www.imageidentify.com/)
8. [Image Background Removal](https://www.remove.bg/)
9. [TinEye Reverse Image](https://www.tineye.com/)
10. [Karma Decay Reverse Image](http://karmadecay.com/)
11. [Image Differences](https://www.diffchecker.com/image-diff)
12. [Photopea](https://www.photopea.com/)
13. [Image Enhance](https://letsenhance.io/)
14. [FindClone](https://findclone.ru/)
15. [Reverse Image Search](https://pimeyes.com/en/)
16. [PIMEYES](https://pimeyes.com/en)
17. [Image Upscaler](https://vertexshare.com/image-upscaler.html)
18. [https://findclone.ru/](http://FindClone.com)
19. [Video/Image Metadata Viewer](https://www.thexifer.net/)
20. [Youtube Data Viewer](https://citizenevidence.amnestyusa.org/)
----

### Other Social Media
1. [Reddit Advanced Search](https://www.redditsearch.io/)
2. [Reddit User Search](http://redective.com/)
3. [Tweeddeck Style Reddit](https://rdddeck.com/)
4. [Reddit Investigator - Username](https://www.redditinvestigator.com/)
5. [Reddit Downloader - Username](https://redditcommentsearch.com/)
----

### Rebate Sites
1. [Rebates International](https://rebateinternational.com/RebateInternational/tracking.do#track-rebate)
2. [Lowes Rebate Center](https://lowes-rebates.com/en-us/RebateStatus?int_cmp=RebateCenter%3AA%3ANoDivision%3ACorp%3ACheck_My_Rebate)
3. [Advance Auto Parts](https://advanceautoparts.4myrebate.com/Claim/TrackaRebate)
4. [Home Depot](https://www.homedepotrebates.com/trackonly/)
----

### Maps and Cameras
1. [OpenStreetCam](http://www.openstreetcam.org)
2. [Open Maps](https://www.openstreetmap.org)
3. [Mapillary](https://www.mapillary.com/app/)
4. [Other Mapping Tools](https://gisgeography.com/free-satellite-imagery-data-list/)
5. [Dual Maps](http://data.mashedworld.com/dualmaps/map.htm)
----

### Webpage/Domain/IP/Who.is Tools
1. [ICANN WHOIS](https://whois.icann.org/en)
2. [ViewDNS.info](http://viewdns.info/)
3. [CertDB — SSL certificates search engine](http://certdb.com/)
4. [Domain CT Log Search Tool](https://crt.sh/)
5. [IP Area Search](https://geoiptool.com/)
6. [SiteSleuth.io](https://www.sitesleuth.io/)
7. [Domain history checker](https://whoisrequest.com/history/)
8. [DNS History](https://completedns.com/dns-history/)
9. [WayBack Machine](https://archive.org/web/)
10. [Website Carbon Dating](http://carbondate.cs.odu.edu/)
11. [Cached Pages Search](http://www.cachedpages.com/)
12. [Is a Page Mobile Friendly?](https://search.google.com/test/mobile-friendly)
13. [Whoisology](https://whoisology.com/)
14. [Whois By Name Search](https://www.whoxy.com/reverse-whois/)
15. [WHOIS Historical Search](https://osint.sh/whoishistory/)
----

### Business Searches
1. [OpenCorporates](https://opencorporates.com/)
2. [LittleSis](https://littlesis.org/)
----

### Python
1. [Website Tracking/Connections](https://github.com/woj-ciech/kupa3)
2. [H8mail - Password Hash Search](https://github.com/khast3x/h8mail)
3. [InstaTool](https://github.com/sc1341/InstagramOSINT)
----

### Email Searches
1. [Analyze Email Header](https://mxtoolbox.com/emailheaders.aspx)
2. [Email Verify 1 (Hippo)](https://tools.verifyemailaddress.io/)
3. [Free Email Verify](https://verify-email.org/)
4. [Spiderfoot Launch](http://127.0.0.1:5001)
5. [Gmail ID Generator](https://tools.epieos.com/google-account.php)
----

### Misc Apps
1. [Screen Recorder Download](https://www.ezvid.com/download)
2. [JSON Viewer (Online)](http://jsonviewer.stack.hu/)
3. [CyberChef](https://gchq.github.io/CyberChef/)
4. [JSON Formatter](https://jsonformatter.curiousconcept.com/)
----

### Video Sharing Sites
1. [TikTok User Search](https://tiktokapi.ga/)
2. [TikTok Search Site](https://www.osintcombine.com/tiktok-quick-search)
3. [TikTok Search](https://tikvid.com/)
----

### Vehicle Related Searches
1. [Hertz Request a Receipt](https://www.hertz.com/rentacar/receipts/request-receipts.do)
2. [Enterprise Request Search](https://www.enterprise.com/en/reserve/receipts.html?irgwc=1&mcid=ImpactRadius%3A27795%3A333526&ir_cid=a3eb2abaN84b011eab3d642010a246c1)
3. [Alamo Rental Receipts](https://www.alamo.com/en_US/car-rental/receipts.html)
4. [VIN Decoder w/Images](https://driving-tests.org/vin-decoder/)
5. [VIN Decoder w/Images 2](https://www.vindecoderz.com/)
6. [Epicvin.com](https://epicvin.com/)
----

### OSINT Collections
1. [Ultimate OSINT Collection](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
2. [OSINT Combine](https://www.osintcombine.com/tools)
3. [IntelTechniques Homepage](https://inteltechniques.com/)
4. [OSINT Framework](https://osintframework.com/)
5. [OSINT Homepage 2](https://www.osinttechniques.com/osint-tools.html)
6. [OSINT Essentials](https://www.osintessentials.com/home)
7. [Technisette OSINT Collection](https://start.me/p/m6XQ08/osint)
8. [Bellingcat OSINT](https://start.me/p/ELXoK8/bellingcat-osint-landscape)
9. [AsINT Collection](https://start.me/p/b5Aow7/asint_collection)
10. [Great OSINT Collection](https://start.me/p/3g0aKK/sources)
11. [Github.com/jivoi/awesome-osint](https://github.com/jivoi/awesome-osint)
12. [OSINT Framework Map](https://atlas.mindmup.com/digintel/digital_intelligence_training/index.html)
13. [Unexplored List of OSINT Links](https://300m.com/osint/)
14. [Digintel Start.me](https://start.me/p/ZME8nR/osint)
15. [Random Open Source Intelligence (OSINT)](https://start.me/p/gy0NXp/open-source-intelligence-osint)
16. [Random OSINT Page 2](https://start.me/p/kxGLzd/hun-osint)
17. [Paul Myers Research Clinic](http://researchclinic.net/)
18. [IntelX.Io Tools List](https://intelx.io/tools)
19. [GoFindWho OSINT Source Tools](https://gofindwho.com/)
20. [SOOVLE Search Options - Multi Source Search](https://soovle.com/)
21. [Dating apps and hook-up sites for investigators](https://start.me/p/VRxaj5/dating-apps-and-hook-up-sites-for-investigators)
22. [Random OSINT Page](https://start.me/p/gy0NXp/open-source-intelligence-osint)
23. [Telegram OSINT](https://start.me/p/YaOYnJ/telegram-osint)
----

### Username and Phone Number Searches
1. [namecheckr.com](https://www.namecheckr.com/)
2. [Namechk](http://namechk.com/)
3. [Profilr Social](https://www.profilr.social/)
4. [Social Searcher](https://www.social-searcher.com/)
5. [Knowem Username Search](https://knowem.com/)
6. [Email Address Search via LinkedIn](https://rocketreach.co/)
7. [Phone Carrier Search](https://scammerblaster.com/carrier-lookup/)
8. [Check Usernames](https://checkusernames.com/)
9. [Username Search](https://usersearch.org/)
10. [WhatsMyName Web](https://whatsmyname.app/)
11. [Username Search](https://namecheckup.com/)
----

### OSINT Accounts
1. [DeHashed](https://dehashed.com/)
2. [OSINT.TEAM](https://osint.team/home)
3. [Censys.io Login](https://censys.io/login)
4. [Shodan](https://www.shodan.io/explore)
5. [Hunter](https://hunter.io/)
6. [Github](https://github.com/)
7. [PIPL API Demo](https://pipl.com/api/demo)
8. [SEO Profiler](https://www.seoprofiler.com/account/login)
9. [Censys IP Address](https://censys.io/)
10. [Free Visualization Tool](https://vis.occrp.org/)
11. [SpiderFoot HX](https://sf-9fd3624.hx.spiderfoot.net/)
----

### Hacks/Leaks
1. [DeHashed](https://dehashed.com/)
2. [Have I Been Pwned: Email](https://haveibeenpwned.com/)
3. [Have I Been Pwned: Passwords](https://haveibeenpwned.com/Passwords)
4. [PasteBin Search](https://pastebin.com/7YaKZ3HD)
5. [Defacer.ID |Cyber Vandalism](https://defacer.id/)
6. [data.occrp.org](https://data.occrp.org/)
7. [Leak Check](https://leakcheck.net/en)
8. [Leaks.SH Search](https://leaks.sh/)
9. [Breach Data](https://breachdirectory.org/)
----

### Dorks/Operators
1. [Common Dorks List](https://medium.com/@polihenko.o/google-dorks-3cbc0e2de2dc)
2. [Dorks List 2019](https://ahrefs.com/blog/google-advanced-search-operators/)
3. [Google Dork List 1](https://www.exploit-db.com/google-hacking-database)
4. [Google Dork Database](https://github.com/redduxi/Google-Dorks-Resources/commit/4172e687e12591645ab98c3412a52b38401e91ca)
5. [Google Dork Searchable Database](https://geekwire.eu/ghdb)
----

### Free People Searches
1. [White Pages Suppression Link](https://www.whitepages.com/suppression-requests)
2. [Thats Them](https://thatsthem.com/)
----

