### VOIP
1. [@Google Voice in conjunction with VPN](https://www.google.com/voice)
2. [Poivy.com (PC & App)](https://www.poivy.com)
3. [Signup](https://voice.google.com/u/0/signup)
4. [Tutorial](https://www.youtube.com/watch?v=angdlrgzoeI)
5. [Voice Features](https://voice.google.com/about)
----

### WiFi
1. [@Free WiFi passwords from airports worldwide](https://www.hackread.com/airports-free-wifi-passwords-map/)
2. [WiGLE: Wireless Network Mapping](https://wigle.net/)
----

### Spoof Location Data  D
1. [Google Maps Address Spoof](https://support.google.com/maps/answer/3093979?co=GENIE.Platform%253DDesktop&hl=de)
----

### Caller ID Spoofing
1. [@Spooftel](https://www.spooftel.com/)
2. [Caller ID Spoofing, Voice Changing & C...](https://www.spoofcard.com/)
3. [prankdial.com/](https://www.prankdial.com/)
----

### Spoof SMS
1. [Smsgang.com Spoof SMS (€)](http://www.smsgang.com)
2. [Spoof SMS Text Messages (Kali Linux)](http://www.spoofmytextmessage.com/)
----

### Spoof Email
1. [Anonymailer](http://www.anonymailer.net/)
2. [Anonymailer Spoof E-Mail](http://www.anonymailer.net/)
3. [Anonymouse (Premium 59 $/Y)](https://anonymousemail.me)
4. [Anonymousemail](https://anonymousemail.me)
5. [EMAIL SPOOFING](http://www.anonymailer.net/)
6. [Emkei's Fake Mailer](https://emkei.cz/)
7. [xBlog: Email Spoofing: Explained (and How to Protect Yourself)](http://www.huffingtonpost.com/jason-p-stadtlander/email-spoofing-explained-_1_b_6477672.html)
8. [xBlog: How Spammers Spoof Your Email Address (and How to Protect Yourself)](https://lifehacker.com/how-spammers-spoof-your-email-address-and-how-to-prote-1579478914)
----

### IP Logger Tools | Identify Target IP |
1. [@ReadNotify](http://www.readnotify.com/)
2. [Blasze IP Logger](http://blasze.tk/)
3. [Find someone's ip address » Tracking and logging IP adresses](http://iplogger.org/)
4. [Get Notify Email Tracking Service](https://www.getnotify.com/)
5. [Google URL Shortener](https://goo.gl/)
6. [Grabify](https://grabify.link/)
7. [Honeybox Home - secXtreme GmbH](https://www.honeybox.de/home.html)
8. [Monitor folders on Windows with Folder Monitor - gHacks Tech News](https://www.ghacks.net/2018/08/07/monitor-folders-on-windows-with-folder-monitor/)
9. [Webresolver](https://webresolver.nl/)
----

### Canary Tokens
1. [Canary Resources](https://canary.tools/resources)
2. [Canary Token | Know.  Before it matters](https://canarytokens.org/generate)
3. [Thinkst Canary — Know. When it matters.](https://canary.tools/help/canarytokens)
4. [Track a Target Using Canary Token Tracking Links [Tutorial] - YouTube](https://www.youtube.com/watch?v=FNiBNdM7srE)
5. [xBlog: How to Track a Target Using Canary Token Tracking Links « Null Byte :: WonderHowTo](https://null-byte.wonderhowto.com/how-to/track-target-using-canary-token-tracking-links-0192830/)
6. [xPDF: Bring back the honeypots](https://thinkst.com/stuff/bh2015/thinkst_BH_2015_notes.pdf)
----

### Create Pixel
1. [Ever needed a transparent 1x1 PNG pixel? Grab one here!](http://www.1x1px.me/)
2. [html Colorcodes.com/](https://htmlcolorcodes.com/)
3. [Png-pixel.com](https://png-pixel.com/)
4. [xVid: Supertracker - Create your own tracking Pixel](http://supertracker.delian.io/)
5. [x_Blog: What are Tracking Pixels and How Do They Work?](https://en.ryte.com/wiki/Tracking_Pixel)
----

### Investigative Identity | Autocreate
1. [@Data Fake Generator](http://www.datafakegenerator.com/)
2. [@Fake Name Generator](http://www.fakenamegenerator.com/)
3. [@Secure Fake Name Creator | fakena.me](https://fakena.me/)
4. [Fake Identity ID Random Name Generator - ElfQrin](https://elfqrin.com/fakeid.php)
5. [How Many of Me](http://howmanyofme.com/)
6. [Just Delete Me](http://backgroundchecks.org/justdeleteme/fake-identity-generator/)
7. [Name Generator](https://name-generator.org.uk)
8. [Random Name Generator](https://randomwordgenerator.com/name.php)
9. [Random Name Generator - Behind the Name](https://behindthename.com/random)
10. [Random User Generator](https://randomuser.me)
11. [Randomly Generate Fake Names](https://uinames.com)
12. [Omatsuri.app](https://omatsuri.app/)
13. [Buy Social Media Accounts👌🏻| Cheap Accounts in Bulk⚡️⚡️⚡️](https://accfarm.com/)
14. [Fameswap.com](https://fameswap.com/)
15. [https://www.similarweb.com/de/website/fameswap.com/competitors/](https://www.similarweb.com/de/website/fameswap.com/competitors/)
----

### Investigative Identity | Name
1. [@Name Generator](https://www.name-generator.org.uk/)
2. [Best 250 Cool Instagram Usernames](http://www.lifegag.com/cool-usernames-girls-pick-one-suits/)
3. [Bevölkerung](https://www.bfs.admin.ch/bfs/de/home/statistiken/bevoelkerung.html)
4. [How Many of Me](http://howmanyofme.com/)
5. [Liste bekannter Darsteller des deutschsprachigen Films – Wikipedia](https://de.wikipedia.org/wiki/Liste_bekannter_Darsteller_des_deutschsprachigen_Films)
6. [Name Generators](http://rumandmonkey.com/widgets/toys/namegen/)
7. [nicknamemaker.net](http://nicknamemaker.net/)
8. [Random Name Generator](https://randomwordgenerator.com/name.php)
9. [Random Name Generator](https://www.behindthename.com/random/)
10. [Rumandmonkey.com](http://rumandmonkey.com/widgets/toys/namegen/)
11. [Schweizer Vornamen](http://www.beliebte-vornamen.de/2607-schweizerische.htm)
12. [The Meaning of Names](https://www.names.org/)
13. [Übersicht deutsche Vornamen, Nachnamen, Strassen und Orte](http://www.namen-liste.de/)
14. [Universal Nickname Generator](http://www.robertecker.com/hp/research/nickname-generator.php?lang=de)
15. [xBlog: 101 Ways I Screwed Up Making a Fake Identity – tisiphone.net](https://tisiphone.net/2016/10/13/101-ways-i-screwed-up-making-a-fake-identity/)
16. [xBlog: Fictional Accounts](https://www.osinttechniques.com/fictional-accounts.html)
17. [xBlog: Sockpuppet Accounts - Where to Start? - AaronCTI](https://www.aaroncti.com/lets-talk-about-sockpuppet-accounts/)
----

### Investigative Identity | Profile photos & context
1. [@Faceplus Merging](https://www.faceplusplus.com/face-merging/)
2. [@morph thing](http://www.morphthing.com/)
3. [@thispersondoesnotexist.com Face AI](https://thispersondoesnotexist.com/)
4. [100,000 AI-Generated Faces – Free to use!](https://generated.photos/)
5. [Remove Background with remove.bg](https://www.remove.bg/)
6. [Resize Your Images for Social Media](https://promo.com/tools/image-resizer/)
7. [xBlog: Jake Creps on Twitter](https://twitter.com/jakecreps/status/1362171841520013316?s=20)
8. [tenor](https://tenor.com/)
----

### Investigative Identity Logistics | Automate Social Postings
1. [@ContentStudio](https://app.contentstudio.io/)
2. [DrumUp - Content Marketing Tool](http://drumup.io)
3. [Pablo by Buffer            posts in under 30 seconds](https://pablo.buffer.com/)
4. [Franz](https://meetfranz.com/)
----

### Investigative Identity | ID Creator Card
1. [IDCreator.com](https://www.idcreator.com/)
----

### Investigative Identity | Meme creation
1. [Imgflip](https://imgflip.com/)
2. [Pablo by Buffer
            posts in under 30 seconds](https://pablo.buffer.com/)
3. [This Meme Does Not Exist](https://imgflip.com/ai-meme)
4. [screechbar: this word does not exist](https://www.thisworddoesnotexist.com/)
----

### Investigative Identity | Fake Documents  | Diploma
1. [Diplom & Berufszertifikat online kaufen (GER)](https://berufszertifikate-und-diplome.teleclip.net/)
2. [Replaceyourdoc](https://www.replaceyourdoc.com/)
3. [Topdiploma123.com](https://www.topdiploma123.com/)
----

### Investigative Identity | Freelancer Forum
1. [Fiverr](https://www.fiverr.com/)
2. [Upwork Freelancer Forum](https://www.upwork.com/)
----

### OPSEC Resources
1. [xBlog: Operations security - OPSEC](https://en.wikipedia.org/wiki/Operations_security)
2. [xBlog: #1 Online Privacy Through OPSEC and Compartmentalization](https://www.ivpn.net/privacy-guides/online-privacy-through-opsec-and-compartmentalization-part-1)
3. [xBlog: #2 OPSEC failures](https://www.ivpn.net/privacy-guides/online-privacy-through-opsec-and-compartmentalization-part-2)
4. [xBlog: #3 OPSEC guides](https://www.ivpn.net/privacy-guides/online-privacy-through-opsec-and-compartmentalization-part-3)
5. [xBlog: #4 OPSEC countermeasures](https://www.ivpn.net/privacy-guides/online-privacy-through-opsec-and-compartmentalization-part-4)
6. [xVid: The Grugq - OPSEC: Because Jail is for wuftpd](https://www.youtube.com/watch?v=9XaYdCdwiWU)
7. [xBlog: Blogs of War Hacker OPSEC @thegrugq](https://blogsofwar.com/hacker-opsec-with-the-grugq/)
8. [xBlog: 73 Rules of Spycraft (Allen Dulles)](https://blog.cyberwar.nl/2016/02/some-elements-of-intelligence-work-73-rules-of-spycraft-allen-dulles-1960s/)
9. [xBlog: OPSEC online |The United States Army](https://www.army.mil/article/137404/tips_help_maintain_opsec_online)
10. [xBlog: Threat Modeling](https://www.netmeister.org/blog/threat-model-101.html)
11. [xBlog: OpSec 101](https://www.netmeister.org/blog/opsec101.html)
12. [xBlog: Intelpage Operations Security](https://www.intelpage.info/manuales/contra/Capitulo02.htm)
13. [xPDF: Operations Security | US JCS 2012](https://info.publicintelligence.net/JCS-OPSEC.pdf)
14. [thaddeus e. grugq (@thegrugq)](https://twitter.com/thegrugq)
15. [xBlog: Hacker OPSEC @thegrugq](https://grugq.github.io/blog/2014/02/10/a-fistful-of-surveillance/)
16. [xBlog: OPSEC for hackers @thegrugq](https://www.slideshare.net/grugq/OPSEC-for-hackers)
17. [xPDF: OPSEC for security researchers](https://www.virusbulletin.com/uploads/pdf/conference/vb2014/VB2014-DiazCreus.pdf)
18. [xBlog: Tradecraft Tuesday: Operational Contacts (PART ONE) - Black Swan Outdoors](https://blackswanoutdoors.blogspot.com/2020/07/tradecraft-tuesday-operational-contacts.html)
19. [xBlog: Tradecraft Tuesday: Operational Contacts (PART TWO) - Black Swan Outdoors](https://blackswanoutdoors.blogspot.com/2020/08/tradecraft-tuesday-operational-contacts.html)
----

### Surveillance
1. [xBlog: How to Determine When You Are Under Physical Surveillance](https://escapethewolf.com/2296/when-you-under-physical-surveillance/)
2. [xBlog: Detecting Terrorist SurveillanceEmail](https://worldview.stratfor.com/article/detecting-terrorist-surveillance)
3. [xBlog: A Very Brief Introduction To Surveillance Detection](https://protectioncircle.org/2014/01/03/a-very-brief-introduction-to-surveillance-detection/)
----

### Other  Webmail
1. [@Free Email Providers Guide](http://www.fepg.net/providers.html)
2. [Create your Google Account](https://accounts.google.com/signup)
3. [GMX: E-Mail-Adresse](https://www.gmx.net/)
4. [Hushmail](https://www.hushmail.com/signup/)
5. [iCloud Mail email address](https://support.apple.com/kb/ph2620?locale=en_US)
6. [Outlook.com Microsoft.](https://www.microsoft.com/en-us/outlook-com/)
7. [Web.de](https://web.de/)
8. [Zoho Email](https://www.zoho.com/mail/)
----

### Disposable Email
1. [@Mailbox.org (GER)](https://mailbox.org/)
2. [eyepaste -> disposable email](http://www.eyepaste.com/)
3. [Mailinator Disposable Email](https://www.mailinator.com/)
4. [Nada](https://getnada.com/)
5. [Spoofmail Wegwerf-eMail (GER)](http://spoofmail.de/)
6. [Wegwerfemailadresse (GER)](http://www.wegwerfemailadresse.com/)
7. [xBlog: Anonyme E-Mail Adresse: Alle Möglichkeiten](http://www.emailtester.de/anonyme-email-adresse.php)
----

### E-Mail Forwarding / Remailer
1. [@SimpleLogin](https://simplelogin.io)
2. [33Mail](http://www.33mail.com/)
3. [Abine Blur](https://www.abine.com/)
4. [AnonAddy](https://anonaddy.com)
5. [Eliminate Spam with Cocoon](https://getcocoon.com/features/nospam)
6. [Fake Email](http://email-fake.com)
7. [inboxbear - temporary email](https://inboxbear.com/)
8. [Paranoici Remailer](http://remailer.paranoici.org/index.php)
9. [Quicksilvermail (Win Client)](https://www.quicksilvermail.net/)
10. [Sneakemail](https://sneakemail.com/)
----

### IMAP POP email client
1. [MailMate](https://freron.com/)
2. [Postbox](https://postbox-inc.com/)
3. [Thunderbird](https://thunderbird.net/de/)
----

### Hardware
1. [EDEC Digital Privacy. Evidence Integrity.](https://www.edecdf.com/)
2. [Silent Pocket](https://silent-pocket.com/)
3. [Calslock Portable Door & Travel Lock - Tools Products - Amazon.com](https://www.amazon.com/Calslock-Portable-Door-Travel-Lock/dp/B00GMPFCNC/ref=as_li_ss_tl?keywords=portable+door+lock&qid=1562448253&s=gateway&sr=8-7&linkCode=sl1&tag=01010101-20&linkId=1490b8b51bc0875206502044291b9b51&language=en_US)
----

### Adress Schutz (GER)
1. [Adress-schutz.de](https://www.adress-schutz.de/)
----

### Payment
1. [Best Bitcoin Wallet Armory - Desktop Wallet](https://www.bitcoinarmory.com/)
2. [Buy Gift Cards Online (USA)](https://www.americanexpress.com/gift-cards/)
3. [Coinbase Bitcoin Wallet - Online Wallet](https://www.coinbase.com/)
4. [Entropay.com](https://entropay.com/)
5. [Privacy - Secure Online Card Payments](https://privacy.com/)
----

### Payment (GER)
1. [ReiseBank AG: Die Experten für Bargeld und Edelmetalle](https://www.reisebank.de/)
----

### Bundesnetzagentur
1. [Bundesnetzagentur  - 
Ping-Anruf](https://www.bundesnetzagentur.de/_tools/RumitelStart/Form04PingAnruf/node.html)
----

