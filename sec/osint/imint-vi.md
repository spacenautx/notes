### Image Search | Bilder Suche |
1. [Google Images OSIT](http://images.google.com/)
2. [Bing](http://www.bing.com/images)
3. [Yandex.Images OSIT](http://images.yandex.com)
4. [Yahoo Image Search](https://images.search.yahoo.com/)
5. [Pic Search Pictures](http://www.picsearch.com/)
6. [Baidu Images OSIT](http://stu.baidu.com)
7. [Flickr OSIT](http://www.flickr.com/)
8. [xBlog: Natalia Antonova 🇺🇸🇺🇦 - Natalia Explains The Apocalypse](https://nataliaantonova.substack.com/people/2332977-natalia-antonova-)
----

### Reverse Image & Facial Recognition Search
1. [Google Images](https://images.google.com)
2. [Google Reverse Image Search for Mobile](https://ctrlq.org/google/images/)
3. [Imagewiki](http://image.wiki/#/)
4. [picwiser.com](http://picwiser.com/)
5. [EZ GIF Reverse Image Search](https://ezgif.com/reverse)
6. [Berify Reverse Image Search](https://berify.com/)
7. [IntelTechniques Reverse Image Search](https://inteltechniques.com/osint/menu.reverse.image.html)
8. [Instant Logo Reverse Image Search](http://instantlogosearch.com/)
9. [PicQuery Reverse Image Search](https://www.picquery.com/)
10. [RIS Reverse Image Search](http://www.reverse-image-search.org/)
11. [RootAbout Reverse Image Search](http://rootabout.com/)
12. [SmallSEOTools Reverse Image Search](https://smallseotools.com/reverse-image-search)
13. [SocialCatfish Reverse Image Search](https://socialcatfish.com/reverse-image-search)
14. [TinEye Reverse Image Search](https://www.tineye.com/)
15. [BetaFaceAPI Image Recognition](https://www.betafaceapi.com/demo.html)
16. [EagleEye Image Recognition](https://github.com/ThoughtfulDev/EagleEye)
17. [FaceDetection Image Recognition](http://www.facedetection.com/)
18. [FaceRec Image Recognition](http://www.face-rec.org/)
19. [FaceSaerch Image Recognition](http://www.facesaerch.com/)
20. [FindFace Image Recognition](https://findface.ru/login)
21. [How Old Image Recognition](https://www.how-old.net/#)
22. [Pictriev Image Recognition](http://Pictriev.com)
23. [PimEyes Image Recognition](https://pimeyes.com/en/)
24. [Wolfram Image ID Image Recognition](https://www.imageidentify.com/)
25. [Findclone VK](https://findclone.ru/)
26. [BLOG: A Brief Comparison of Reverse Image Searching Platforms](https://www.domaintools.com/resources/blog/a-brief-comparison-of-reverse-image-searching-platforms)
----

### Photo Image  forensics
1. [5 Tools to Detect Digitally Altered Images](https://medium.com/@raebaker/5-tools-to-detect-digitally-altered-images-719db4015b5d)
2. [Digital Image Forensic Analyzer](http://www.imageforensic.org/)
3. [Enlarge Image With Details Restored - AI Image Enlarger](http://imglarger.com/Home/Software)
4. [ExifTool by Phil Harvey](http://www.sno.phy.queensu.ca/~phil/exiftool/)
5. [Extract camera information from any Jpeg (Jpg) image file](https://camerasummary.com/)
6. [Find exif data](http://www.findexif.com/)
7. [Fotoforensics.com](http://fotoforensics.com)
8. [gbimg.org](http://gbimg.org/)
9. [Ghiro](http://www.getghiro.org/)
10. [Image Forensics](http://www.errorlevelanalysis.com/)
11. [Image Verification Assistant](http://reveal-mklab.iti.gr/reveal/index.html)
12. [InVID project Image Verification plus Chrome Extension](http://www.invid-project.eu/)
13. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
14. [JPEGsnoop](https://www.impulseadventure.com/photo/jpeg-snoop.html)
15. [Metadataanalysis (Win)](https://www.metadataanalysis.com/)
16. [Paliscope](https://www.paliscope.com/)
17. [PhotoScissors Background Removal Tool](https://online.photoscissors.com/)
18. [pngmeta Home Page](http://www.libpng.org/pub/png/apps/pngmeta.html)
19. [PngUtils](http://gnuwin32.sourceforge.net/packages/pngutils.htm)
20. [redaelli/imago-forensics](https://github.com/redaelli/imago-forensics)
21. [remove.bg Remove Image Background](https://www.remove.bg/)
22. [Reveal - Image Verification Assistant](http://reveal-mklab.iti.gr/reveal/)
23. [Sherloq - An Open-Source Digital Image Forensic Toolset](https://www.kitploit.com/2020/04/sherloq-open-source-digital-image.html)
24. [SmartDeblur](http://smartdeblur.net/)
25. [Snapseed – Apps bei Google Play](https://play.google.com/store/apps/details?id=com.niksoftware.snapseed&hl=de)
26. [Remini Professional](https://app.remini.ai/)
----

### Identify Image Content
1. [The Wolfram Language Image Identification Project](https://www.imageidentify.com/)
2. [karmadecay Reddit Image OSIT](http://karmadecay.com)
----

### Identify images (Plagiarism)
1. [Image Raider](https://infringement.report/api/raider-reverse-image-search/)
2. [Plaghunter](https://www.plaghunter.com/)
----

### Stock Photos
1. [Bigstockphoto](http://www.bigstockphoto.com/)
2. [CorbisImages](http://www.corbisimages.com/)
3. [De.fotolia.com](https://de.fotolia.com)
4. [Flaticon](https://www.flaticon.com/)
5. [Gettyimages](http://www.gettyimages.de/)
6. [Iconfinder](https://www.iconfinder.com/)
7. [iStockphoto](http://deutsch.istockphoto.com/)
8. [Librestock Free Photos](https://librestock.com/)
9. [Pikwizard.com](https://pikwizard.com/)
10. [Pixabay](https://pixabay.com/)
11. [Shutterstock](http://www.shutterstock.com/)
12. [Undesign Free design Tools Collection](https://undesign.learn.uno/)
13. [Unsplash](https://unsplash.com/)
14. [Free Images](https://www.freeimages.com)
----

### Photo Sharing
1. [Picasa Web Albums](http://picasaweb.google.com)
2. [Flickr](http://www.flickr.com)
3. [Photobucket](http://photobucket.com)
----

### Photo Library
1. [Photo Bucket](http://photobucket.com/)
2. [Pureref.com](https://www.pureref.com/)
----

### Video Search
1. [Google Videos](https://www.google.com/videohp?gws_rd=ssl)
2. [YouTube Geofind](https://mattwright324.github.io/youtube-geofind/location)
3. [Youtube Related Videos Visualization YASIV](https://yasiv.com/youtube#?q=osint)
4. [tools.digitalmethods.net/netvizz/youtube](https://tools.digitalmethods.net/netvizz/youtube/)
5. [Bing Video OSIT](http://videos.bing.com)
6. [Youtube](https://www.youtube.com/)
7. [Metacafe](http://www.metacafe.com/)
8. [Vimeo](https://vimeo.com/)
9. [Dailymotion](http://www.dailymotion.com/)
10. [Community Video](http://archive.org/details/opensource_movies)
11. [Suchen auf Vimeo](https://vimeo.com/search)
12. [Perisearch](https://www.perisearch.xyz/)
13. [Search for Videos by Location!!! Geo Locate You Tube Video](http://www.geosearchtool.com/)
14. [Geo Search Tool for YouTube](http://youtube.github.io/geo-search-tool/search.html)
15. [Location Search for YouTube Videos](https://mattw.io/youtube-geofind/location)
16. [Peteyvid.com (Privacy )](https://www.peteyvid.com/)
----

### YouTube
1. [Sign in](https://plus.google.com/)
2. [Google Plus Search - Search Google Plus (Google+) public contents...](http://googleplussearch.chromefans.org/)
3. [Google Groups](https://groups.google.com/forum/#!homeredir)
4. [Google Videos](https://www.google.com/videohp?gws_rd=ssl)
5. [YouTube Data Viewer Extract Meta Data](https://www.amnestyusa.org/citizenevidence/)
6. [Getting Youtube Subtitles.](http://mo.dbxdb.com/)
----

### Download & Convert Video
1. [KeepVid](http://keepvid.com/)
2. [Kostenloser Download von Rapidshare, FileFactory. Kostenfreier Download von Videos von YouTube, Google, Metacafe](http://de.savefrom.net/)
3. [DreDown](https://www.dredown.com/youtube)
4. [YouTube Konverter](http://www.clipconverter.cc/de/)
5. [deturl.com](http://deturl.com/www.youtube.com/watch?v=uqnqLrakxY8)
6. [Download Facebook Videos](http://www.downfacebook.com/)
7. [Scopedown Download Periscope Video](http://downloadperiscopevideos.com/)
8. [ClipGrab](https://clipgrab.org/)
9. [DownloadHelper](https://www.downloadhelper.net/)
----

### Video Stream
1. [Livestream](https://livestream.com/watch)
2. [Ustream](http://www.ustream.tv/)
----

### Camera trace | Kamera Seriennummer |
1. [stolen camera finder](http://www.stolencamerafinder.com/)
----

### Depixelize and Remove Objects Cleanup Images
1. [Remove Objects from Photos with Inpaint!](https://theinpaint.com/)
2. [GitHub - tg-bomze/Face-Depixelizer: Face Depixelizer based on "PULSE: Self-Supervised Photo Upsampling via Latent Space Exploration of Generative Models" repository.](https://github.com/tg-bomze/Face-Depixelizer)
3. [Cleanup.pictures](https://cleanup.pictures/)
----

### YouTube
1. [10 Youtube URL Tricks You Should Know About](http://www.makeuseof.com/tag/10-youtube-url-tricks-you-should-know-about/)
2. [Play multiple instances of VLC Wiki.videolan.org](https://wiki.videolan.org/VLC_HowTo/Play_multiple_instances/)
3. [GitHub: yttool  A tool for extracting info from youtube](https://github.com/nlitsme/youtube_tool)
4. [tools.digitalmethods.net/netvizz/youtube](https://tools.digitalmethods.net/netvizz/youtube/)
5. [Getvideobot.com Download Twitter video](https://getvideobot.com/)
----

### Periscope
1. [Aggregated LIVE Periscope Streams](http://onperiscope.com/)
2. [www.recperiscope.com/](http://www.recperiscope.com/)
3. [Premium Domain Names at already Discounted Prices](http://periviewer.com/?country=us)
4. [Periscope TV Watch LIVE](https://www.pscp.tv/)
----

### Image Resizer
1. [Free Image Resizer | Resize Your Images for Social Media | Promo.com](https://promo.com/tools/image-resizer/)
2. [Pablo by Buffer
            posts in under 30 seconds](https://pablo.buffer.com/)
3. [Pixlr Photo Editor](https://pixlr.com/x/)
----

### Identify Weapons
1. [Calibre Obscura](https://www.calibreobscura.com/)
2. [Sketchfab](https://sketchfab.com/)
3. [Small Arms Survey - The Weapons ID Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
----

### Visual Forensics
1. [VFrame](https://ahprojects.com/vframe/)
----

### Vehicle (car) identification
1. [Carnet.ai](https://carnet.ai/)
----

