### GENERAL
1. [Google Maps](https://www.google.com/maps)
2. [Bing Maps](https://www.bing.com/maps)
3. [Yandex.Maps](https://yandex.com/maps/)
4. [Generate a panorama](https://www.udeuschle.de/panoramas/makepanoramas_en.htm)
5. [Mapillary](https://www.mapillary.com/app/)
6. [Geonarra.com](https://www.geonarra.com/)
7. [百度地图](https://map.baidu.com/)
8. [DigitalGlobe](https://discover.digitalglobe.com/)
9. [MapQuest](https://www.mapquest.com/)
10. [OpenStreetMap](https://www.openstreetmap.org/)
11. [Maps.descarteslabs.com](https://maps.descarteslabs.com/)
12. [Living Atlas of the World | ArcGIS](https://livingatlas.arcgis.com/en/browse/)
13. [Satellites.pro](https://satellites.pro/)
14. [Wikimapia](http://wikimapia.org/)
15. [Radio Garden](http://radio.garden/search)
16. [Windy](https://www.windy.com/-Webcams/webcams)
17. [Gpx File Editor](https://gpx.studio/)
18. [OALLEY](https://www.oalley.net/app/)
19. [fgdc_gp_demos's public fiddles](https://jsfiddle.net/user/fgdc_gp_demos/fiddles/)
20. [KartaView](https://kartaview.org/map/)
21. [Google Map Search Engine](https://cse.google.com/cse?cx=013991603413798772546:mofb1uoaebi)
----

### STRANGE STUFF
1. [The one million tweet map](https://onemilliontweetmap.com/)
2. [Geoseer.net](https://www.geoseer.net/)
3. [GeoIP Tracker tool](https://shadowcrypt.net/tools/geoip)
4. [Earth Engine Dataset](https://developers.google.com/earth-engine/datasets/)
5. [GeoPlatform Portal](https://www.geoplatform.gov/)
6. [Inspire-geoportal.eu](http://www.inspire-geoportal.eu/)
7. [FAO Map Catalog](https://data.apps.fao.org/map/catalog/srv/eng/catalog.search#/home)
8. [Datacore-gn.unepgrid.ch](https://datacore-gn.unepgrid.ch/)
9. [ISRIC Data Hub](https://data.isric.org/geonetwork/srv/eng/catalog.search#/home)
10. [geocreepy](http://www.geocreepy.com/)
11. [GeoINT Search](https://cse.google.com/cse?cx=015328649639895072395:sbv3zyxzmji#gsc.tab=0)
12. [Active Agency Map (Ring)](https://www.google.com/maps/d/viewer?mid=1eYVDPh5itXq5acDT9b0BVeQwmESBa4cB)
13. [PARLER USER'S GEOCODES](https://www.google.com/maps/d/viewer?mid=1h7l3FiSySGdCp7Gk27wEV1skKnQbtZok)
----

### CALL FOR SERVICE
1. [Atlanta Crime Map](https://www.arcgis.com/apps/dashboards/c962574350f1441aa3f4e09953b3e5d4)
2. [Minneapolis PD Crime Map](https://www.arcgis.com/apps/webappviewer/index.html?id=576634548ffc4304bf9df0fb2b802f8d)
3. [Philadelphia CFS](https://www.arcgis.com/apps/webappviewer/index.html?id=011a93f6131649acaaf91ccc2edabb1a)
4. [Philadelphia Real-Time Crime App](https://www.arcgis.com/apps/webappviewer/index.html?id=0d1a95740c584a7fbdba0a1eddcf044d)
5. [SAPD CFS](https://www.arcgis.com/apps/dashboards/c91c1679c11648f1b7dcdfdd389deb55)
6. [SCPD CFS](https://www.arcgis.com/apps/webappviewer/index.html?id=792c21da72eb41bbb6475e5e7f222f8d)
7. [Boulder PD CFS](https://www.arcgis.com/apps/dashboards/e0d0983901b441e5bd3786be88b1a6aa)
8. [Wauwatosa Citizen Crime Map](https://www.arcgis.com/apps/dashboards/66b5e3b343d24182981b532d12088d5d)
9. [Halifax CFS](https://www.arcgis.com/apps/webappviewer/index.html?id=cd5b990f2132430bb2bda1da366f175c)
10. [Bangor 311](https://www.arcgis.com/apps/dashboards/05bb8c0586ef45ed810ab55cf1b96d85)
11. [Lubbock CFS](https://www.arcgis.com/apps/webappviewer/index.html?id=d72017d8d36143949cc921a02bd24755)
12. [Middletown OH CFS](https://www.arcgis.com/apps/webappviewer/index.html?id=8aba772306b441a797968e3b9089f922)
13. [Elgin IL CFS](https://www.arcgis.com/apps/dashboards/6dbab5eed57e4a31b26de787cef0176c)
14. [Prince William County Police CFS](https://www.arcgis.com/apps/webappviewer/index.html?id=35e7e79e08754fda8d551e0b974aad6a)
15. [Fitchburg CFS](https://www.arcgis.com/apps/dashboards/4aa39c8d1d6b42b8a268ac2bb4fbe3f8)
16. [tpscalls.live](https://www.tpscalls.live/)
17. [Toronto CAD Activity Map](https://tfscad.victorbiro.com/map/map.php)
18. [TPS Crime App](https://www.arcgis.com/apps/webappviewer/index.html?id=5f74f910b2ea4e85a370014921cdecbd)
19. [Peel Police Crime Mapping](https://www.arcgis.com/apps/webappviewer/index.html?id=2abe72cc102540b5a134fadea0576e0c)
20. [Toronto Police and Fire Active Events](https://gtaupdate.com/gta/)
21. [TPS C4S](https://c4s.torontopolice.on.ca/)
22. [Newport Beach PD CFS](http://www.nbpd.org/crime/calls/map.asp)
23. [Town of Clive CFS](https://www.arcgis.com/apps/webappviewer/index.html?id=f310dfb3bef44e649ca7d707f5d90646)
24. [City of Celina CFS](https://www.arcgis.com/apps/webappviewer/index.html?id=8055e88cc24642b0b8ee69834b65d69f)
25. [Pearland Emergency Incident](https://www.arcgis.com/apps/dashboards/67027d2f847b453e96338d7f1545b8a9)
----

### CONFLICT
1. [Map of Syrian Civil War](https://syria.liveuamap.com/)
2. [Ukraine Interactive map](https://liveuamap.com/)
3. [Military bases around the world. - uMap](https://umap.openstreetmap.fr/en/map/military-bases-around-the-world_510207)
4. [US Crisis Monitor](https://acleddata.com/special-projects/us-crisis-monitor/)
----

### THREAT INTEL
1. [Cyberthreat Real-Time Map](https://cybermap.kaspersky.com/)
2. [Threatbutt](https://threatbutt.com/map/)
3. [NETSCOUT Cyber Threat Horizon](https://horizon.netscout.com/)
4. [Fortinet Threat Map](https://threatmap.fortiguard.com/)
5. [Bitdefender Threat Map](https://threatmap.bitdefender.com/)
6. [THREAT MAP by LookingGlass](https://map.lookingglasscyber.com/)
7. [Cyber Threat Map](https://www.fireeye.com/cyber-map/threat-map.html)
8. [Digital Attack Map](https://www.digitalattackmap.com/)
----

### WILDFIRE / DISASTER
1. [earth :: a global map of wind, weather, and ocean conditions](https://earth.nullschool.net/#current)
2. [Tornado Map Live](https://www.arcgis.com/apps/webappviewer/index.html?id=f92d7c0b47674e7dbbe6769cec15b12c)
3. [MODIS Wildfire](https://www.arcgis.com/apps/dashboards/bf5df3a49a624521844a2e5e1ec7df05)
4. [Earthquake Watch](https://www.arcgis.com/apps/dashboards/c8af9c5411814584b460cc87cb7c3780)
5. [US Wildfire Activity Web Map](https://www.arcgis.com/home/webmap/viewer.html?webmap=df8bcc10430f48878b01c96e907a1fc3#!)
6. [InciWeb](https://inciweb.nwcg.gov/)
7. [FIRMS](https://firms.modaps.eosdis.nasa.gov/map/)
8. [Residential Fire Indiana](https://www.arcgis.com/apps/dashboards/4d1289ab105145a1b1f80c1ad3cf19fb)
9. [BC Wildfire](https://www.arcgis.com/apps/dashboards/f0ac328d88c74d07aa2ee385abe2a41b)
10. [Nepal Earthquake Live](https://www.arcgis.com/apps/webappviewer/index.html?id=b9f9da798f364cd6a6e68fc20f5475eb)
11. [MRA Mission Map Live](https://www.arcgis.com/apps/webappviewer/index.html?id=f5c90bc912e9494b895718bbc7bf7bb6)
12. [NZ Live Public Map](https://www.arcgis.com/apps/webappviewer/index.html?id=d4e237b6208740da81eb7a66d98315ab)
13. [Iowa Live Flood Map](https://www.arcgis.com/apps/dashboards/39cfe72519c04a22a6a28654c4545739)
----

### COVID-19
1. [COVID-19 Map](https://coronavirus.jhu.edu/map.html)
2. [Illinois COVID-19 Awareness](https://www.arcgis.com/apps/dashboards/8ef833c7ed254fd48fe07266f96e0f70)
3. [COVID-19 Mutual Aid Map](https://map.reach4help.org/)
4. [Mutual Aid Wiki](https://mutualaid.wiki/map)
----

### FLIGHT
1. [ADS-B Exchange](https://globe.adsbexchange.com/)
2. [RadarBox](https://www.radarbox.com/)
3. [Icarus.flights](https://icarus.flights/)
4. [FlightAware](https://flightaware.com/)
5. [Flightradar24](https://www.flightradar24.com/)
6. [Live Air Traffic Control](https://www.liveatc.net/)
7. [ICAO Civil and Military airspace SUASHP](https://www.arcgis.com/apps/webappviewer/index.html?id=5db021bcb221498f81be055b3e238bf8)
8. [Real Time Drone Tracking Map](https://www.arcgis.com/apps/webappviewer/index.html?id=ac2ca25489514f8ba38b8e67512eef62)
----

### MARITIME
1. [Marine Traffic](https://www.marinetraffic.com/)
2. [VesselFinder](https://www.vesselfinder.com/)
----

### RADIO
1. [MLS - Map](https://location.services.mozilla.com/map)
2. [WiFi Map](https://www.wifimap.io/)
3. [WiGLE: Wireless Network Mapping](https://wigle.net/)
4. [Cell2gps.com](http://www.cell2gps.com/)
5. [Find GSM base stations](https://cellidfinder.com/)
6. [Scanmap (Radio Scanner Map)](https://scanmap.mobi/)
----

### TRAFFIC / INFRA
1. [Waze](https://www.waze.com/live-map)
2. [TO LIVE](https://apps.esri.ca/torontolive/)
3. [Totransit.ca](https://totransit.ca/)
4. [PlowTO](https://www.toronto.ca/services-payments/streets-parking-transportation/road-maintenance/winter-maintenance/plowto/)
5. [Texas Stream Team Water Quality](https://www.arcgis.com/apps/dashboards/0dea3b21787e446e8ede35bd0977f00f)
6. [Atlanta CommuteATL](https://www.arcgis.com/apps/webappviewer/index.html?id=911f6c71018441ae904edd6c3a457a17)
7. [Kentucky Real-time Traffic](https://kytc.maps.arcgis.com/apps/webappviewer/index.html?id=327a38decc8c4e5cb882dc6cd0f9d45d)
8. [Toronto Outage map](https://www.torontohydro.com/outage-map)
9. [Calgary Traffic Information](https://www.arcgis.com/apps/webappviewer/index.html?id=63408ae30f19495d85662af99bc7bb5a)
10. [Power Plants in the United States](https://www.arcgis.com/apps/dashboards/201fc98c0d74482d8b3acb0c4cc47f16)
11. [Walmart Store Status](https://www.arcgis.com/apps/dashboards/4e573c79e1224081805165d25b4f33c7)
12. [Honolulu JTMC](https://www.arcgis.com/apps/dashboards/4baf2c63cac447c0a636c1fd6de0d121)
13. [Virginia Traffic Volume](https://www.arcgis.com/apps/webappviewer/index.html?id=35e4c06de0f84a9c9f3fe18e67cd2c92)
14. [City of Miami Beach Road Closures](https://www.arcgis.com/apps/webappviewer/index.html?id=1f0c6af5a563457d91f0e17959a50c04)
15. [Capital Project Detours](https://www.arcgis.com/apps/webappviewer/index.html?id=b929195726c64607af3bb1839e536c72)
16. [FDOT D1 Congestion & Congestion Head](https://www.arcgis.com/apps/webappviewer/index.html?id=59adbb525c8f48c39cc6ed7688814c0d)
17. [Penang Traffic Info](https://www.arcgis.com/apps/webappviewer/index.html?id=1fe3c24ab9614947bb3eb23b4e2051e4)
18. [Marlborough CDEM](https://www.arcgis.com/apps/webappviewer/index.html?id=7de480b8bfa541e0a73719bfc893c41d)
19. [SMH Explorer](https://www.arcgis.com/apps/webappviewer/index.html?id=a93eb1fcbd8e47abb7792f92374d4908)
20. [UK Onshore Oil and Gas Activity](https://www.arcgis.com/apps/webappviewer/index.html?id=29c31fa4b00248418e545d222e57ddaa)
21. [NZ Alerts](https://www.arcgis.com/apps/dashboards/20fcae26f03f41a4a0beef09f26c4f3a)
----

### CAMERAS
1. [Lake County Fire Camera](https://www.arcgis.com/apps/webappviewer/index.html?id=0f7aa08cc4b74fc6a0c4308d4eace6b3)
2. [OpenStreetCam](https://www.openstreetcam.org/map)
3. [VA Live Traffic (Waze & VDOT(ITERIS))](https://www.arcgis.com/apps/dashboards/a0d3fb34cda44f5b8b10be1b245f24a3)
4. [Traffic Data in Hong Kong](https://www.arcgis.com/apps/dashboards/47be6372a0434beaba99ae9c9f1d598d)
5. [camera toronto test](https://www.arcgis.com/apps/webappviewer/index.html?id=b70877d02a6949348479c84cd13a550b)
6. [Toronto Live Traffic Cameras](https://gtaupdate.com/traffic/)
7. [Hawaii Traffic Cam](http://goakamai.org/home)
8. [Honolulu Traffic Cam Map](http://www2.honolulu.gov/honolulumyway/?trafficcam)
9. [Iowa DOT Plow Cameras](https://www.arcgis.com/apps/dashboards/c357e82d79b5498aadb16dbab9863fbe)
10. [Lubbock Live Cam Map](https://www.arcgis.com/apps/webappviewer/index.html?id=affe50ac55824c7a8c757d3980787e31)
11. [Shreveport Crime Cam](https://www.arcgis.com/apps/webappviewer/index.html?id=180d092562bf43a88866f4c57f7965d1)
12. [GPPD Cam Location](https://www.arcgis.com/apps/webappviewer/index.html?id=4d0bc50a893048149ae54b926deb6487)
13. [GTA-map Toronto Live Traffic Map](http://www.celinmeteo.com/wdisplay/wxgtamaps.php)
14. [Baton Rouge Traffic Cam](https://www.arcgis.com/apps/webappviewer/index.html?id=0ec05ffb0d2d4735a969e8f31f820a7b)
15. [South Jackson New Camera Location](https://www.arcgis.com/apps/webappviewer/index.html?id=7dbc644939de4113a1345d1530f1905d)
16. [SMPD Camera Pods](https://www.arcgis.com/apps/webappviewer/index.html?id=58ec62d3e63447d4af3991d5736e04a3)
17. [LPR & OPTICAL CAMERA](https://www.arcgis.com/apps/webappviewer/index.html?id=6eed4a3345144204b54241290e68e253)
18. [Newfoundland Automatic Data Retrieval System](https://gnl.maps.arcgis.com/apps/webappviewer/index.html?id=f71672a8bb534c3eb8842f3d671ae13f)
19. [UDOT Fiber Needs and Liveview Camera](https://www.arcgis.com/apps/webappviewer/index.html?id=6bd7768931774baab020e591663565f3)
20. [Greenville Camera](https://www.arcgis.com/apps/webappviewer/index.html?id=6091d808bb6a428e94a7e14e2706c3f7)
21. [Airport Accuware Video Tracking](https://www.arcgis.com/apps/webappviewer/index.html?id=89b1632dfa0b49419f5101781b2c1305)
22. [Fire Camera Viewsheds](https://www.arcgis.com/apps/webappviewer/index.html?id=b54016fbe16244aab82c0dc16c4584e8)
----

