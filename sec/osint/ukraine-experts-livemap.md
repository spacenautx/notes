### DASHBOARD Source Links
1. [#ukraine expert list on Twitter](https://twitter.com/i/lists/1497103507547209729)
2. [Ukraine Interactive map](https://liveuamap.com/)
3. [Cyberthreat Real-Time Map](https://cybermap.kaspersky.com/)
----

### Sourcing du Sourcing
1. [Mr Loup  : "comptes Twitter sûrs #UkraineCrisis il y en a d'autres...](https://twitter.com/Loup_43/status/1496853872182243336)
2. [Deb. des Etoiles sur Twitter : "ℹ️ Comptes Twitter fiables à suivre pour s’informer sur le déro...](https://twitter.com/debunkeretoiles/status/1496816897014341635?s=12)
3. [DSI Mag speciale Russie (Gratuit)](https://twitter.com/dsi_magazine/status/1497570112618041345?s=12)
4. [war in Ukraine | Nieman Journalism Lab](https://www.niemanlab.org/2022/02/follow-war-ukraine)
----

### ### in’box (news to sort)
1. [Des banques russes migrent sur le système chinois de cartes de paiement | Zone bourse](https://www.zonebourse.com/amp/cours/action/PJSC-SBERBANK-6494829/actualite/Des-banques-russes-migrent-sur-le-systeme-chinois-de-cartes-de-paiement-39679332)
2. [Guerre en Ukraine – Une russophobie générale devient «acceptable» en Suisse - Le Matin](https://www.lematin.ch/story/une-russophobie-generale-devient-acceptable-en-suisse-504498025539)
3. [https://www.lesechos.fr/monde/europe/sylvie-bermann-nous-sommes-dans-une-nouvelle-guerre-froide-1391801#utm_source=le%3Alec0f&utm_medium=click&utm_campaign=share-links_twitter](https://www.lesechos.fr/monde/euro)
4. [https://twitter.com/gchampeau/status/1501087778687307776](https://twitter.com/gchampeau/status/1501087778687307776?s=20&t=edvYtom_FLgSt2Cn9toO4Q)
5. [Guerre en Ukraine : opérations de manipulation en cours dans la sphère de la « dark information » f...](https://larevuedesmedias.ina.fr/manipulation-desinformation-guerre-ukraine-russie-dark-information-livre-noir-silvano-trotta-booba)
6. [2021 : Se déconnecter d’internet, les tests de la Russie - Editions Oreilly](https://www.editions-oreilly.fr/actualites/se-deconnecter-dinternet-les-tests-de-la-russie/)
----

### #Ukraine
1. [DeepL Traduction – DeepL Translate : le meilleur traducteur au monde](https://www.deepl.com/translator)
2. [Salomé  - ne cédez pas à la tentation ...](https://twitter.com/salomelagresle/status/1496782822190817282)
----

### militaryland.net(live map) thx HB
1. [Deployment map of military units in Ukraine - MilitaryLand.net](https://militaryland.net/ukraine/deployment-map/)
2. [Ukrainian Casualties - Interactive Map - MilitaryLand.net](https://militaryland.net/ukraine/casualties-map/)
3. [Border Map - MilitaryLand.net](https://militaryland.net/ukraine/border-map/)
4. [Military Vehicles of Ukrainian Forces - MilitaryLand.net](https://militaryland.net/ukraine/vehicles/)
----

### CARTO - MAP (non integrable mais interesante)
1. [3-6 Suivi guerre en Ukraine | Le Devoir](https://www.ledevoir.com/monde/europe/678927/blogue-en-direct-dernieres-nouvelles-conflit-ukraine-russie)
2. [2022-03-1  Russia-Ukraine Monitor Map by Cen4infoRes (map+Sources)](https://maphub.net/Cen4infoRes/russian-ukraine-monitor)
----

### Médias "Sérieux"
1. [Study of War : daily syntheti c& key events+Sources  (Map & Paper)](https://www.understandingwar.org/user/3100/track)
2. [BNO News sur Twitter : "MAP: Countries closing its airspace to Russian planes https://t.co/sjZWaECd...](https://mobile.twitter.com/BNONews/status/1498089843586441222?ref_src=twsrc%5Etfw%7Ctwcamp%5Eembeddedtimeline%7Ctwterm%5Elist%3Abizcom%3A1497103507547209729%7Ctwgr%5Ee30%3D&ref_url=about%3Asrcdoc)
3. [50 Frenchs Journs #UKRAINE (Twitter list)](https://twitter.com/i/lists/106245865)
4. [franceinfo "Guerre en Ukraine : attention aux fausses images qui circulent sur les ré...](https://twitter.com/franceinfo/status/1496845939818078222)
----

### Articles Gratuit sur revue payante
1. [03-02 La Vigie n° 187 : L’Est de « l’Ouest » | Guerre en Ukraine](https://www.usine-digitale.fr/article/l-anssi-livre-ses-recommandations-de-cyberprotection-dans-le-cadre-du-conflit-ukrainien.N1790092)
----

### CyberWar
1. [Volunteer Hackers Converge on Ukraine Conflict With No One in Charge - The New York Times](https://www.nytimes.com/2022/03/04/technology/ukraine-russia-hackers.html)
2. [03-4 des milliers d'internautes privés d’internet après une probable cyber-attaque](https://www.capital.fr/economie-politique/en-europe-des-milliers-dinternautes-prives-dinternet-apres-une-probable-cyber-attaque-1430252)
3. [3-3 cybercriminelles visant l’OTAN et le peuple Ukrainien fuyant les zones de conflits | UnderNews](https://www.undernews.fr/hacking-hacktivisme/decouverte-dactivites-cybercriminelles-visant-lotan-et-le-peuple-ukrainien-fuyant-les-zones-de-conflits.html)
4. [3-2 L'Anssi livre ses recommandations de cyberprotection dans le cadre du conflit ukrainien](https://www.usine-digitale.fr/article/l-anssi-livre-ses-recommandations-de-cyberprotection-dans-le-cadre-du-conflit-ukrainien.N1790092)
5. [Elon Musk a activé Starlink au-dessus de l’Ukraine en guerre](https://twitter.com/laVeilleTechno/status/1497831511508361219?s=20&t=Yh4WLRrxfq_Rx0JXXGzHtQ)
6. [Russia-Ukraine War: Phishing, Malware and Hacker Groups Taking Sides](https://thehackernews.com/2022/02/russia-ukraine-war-phishing-malware-and.html)
7. [Une coupure d'Internet par la Russie : le cauchemar de l’Europe](https://www.latribune.fr/economie/international/une-coupure-d-internet-par-la-russie-le-cauchemar-de-l-europe-904912.html)
8. [03-01 : Le groupe de rançongiciels a paye cher son soutien à Vladimir P.](https://www.linformaticien.com/magazine/cybersecurite/59408-un-groupe-de-rancongiciels-a-paye-le-prix-fort-pour-avoir-soutenu-la-russie-2.html?utm_source=newsletter_207&utm_medium=email&utm_campaign=conti-paie-cher-son-soutien-a-v-poutine-sputnik-et-rt-bloques-vectra-ai-gratuit-sous-conditions-mwc22-strategie-cisco-5g-rakuten-symphony-reprend-robin-io)
9. [DDoS Attempts Hit Russia as Ukraine Conflict Intensifies | WIRED](https://www.wired.com/story/russia-ukraine-ddos-nft-nsa-security-news/?mbid=social_twitter&utm_brand=wired&utm_medium=social&utm_social-type=owned&utm_source=twitter)
10. [Ukraine: les cyberattaques venant de Russie, l'autre menace qui inquiète l'Occident | Le HuffPost](https://m.huffingtonpost.fr/entry/ukraine-cyberattaques-de-russie-tautre-menace-occident_fr_6215fe56e4b0ef74d72844ed)
----

### Comprendre/ Vulgariser /
1. [6 1eres minutes de la video (Via Niels)](https://twitter.com/nielsack/status/1496826632052609031?s=12)
2. [Qu'est-ce la guerre électronique ?](https://iatranshumanisme.com/2022/02/20/quest-ce-que-la-guerre-electronique/)
3. [La premier jour de la guerre en Ukraine, entre attaques russes, sanctions occidentales et réactions...](https://www.lemonde.fr/international/live/2022/02/24/guerre-en-ukraine-von-der-leyen-annonce-des-sanctions-europeennes-massives-dans-la-banque-l-energie-l-aerien-et-les-technologies-de-pointe_6115008_3210.html)
----

### Nuclear
1. [03-5 $`TIR sur la centrale nucléaire de Zaporijjia "peu de Risque »](https://www.la-croix.com/Monde/Guerre-Ukraine-A-centrale-nucleaire-Zaporijjia-reacteurs-sont-proteges-enceinte-beton-2022-03-04-1201203280?utm_medium=Social&utm_source=Twitter#Echobox=1646407618-1)
2. [3-4 Histoire(s) de Tchernobyl 🇺🇦 on Twitter](https://twitter.com/tchernobylfr/status/1499634556420374529?s=12)
3. [3-4 International Atomic Energy Agency](https://twitter.com/iaeaorg)
----

### ARMES & Forces en presences
1. [3-5 Moscou veut placer les centrales nucléaires ukrainiennes sous sa protection](https://www.courrierinternational.com/article/vu-de-russie-moscou-veut-placer-les-centrales-nucleaires-ukrainiennes-sous-sa-protection)
2. [Armes nucléaires ? Combien en ont-ils ? #infographie](https://twitter.com/visactu/status/1497960194361409542?s=12)
3. [INFOGRAPHIES. Guerre Russie-Ukraine : budget, troupes, arsenal... Comparez la puissance militaire des deux pays](https://www.francetvinfo.fr/monde/europe/manifestations-en-ukraine/infographies-guerre-russie-ukraine-budget-troupes-arsenal-comparez-la-puissance-militaire-des-deux-pays_4979367.html)
----

### HELP UKRAINE
1. [Swiss with Ukraine 🇨🇭❤️ 🇺🇦](https://swisshelpua.notion.site/Swiss-with-Ukraine-c0169d25e18649d4b83a4ef75a6f4ca5)
2. [Ukraine: comment je peux aider?  - rts.ch - Portail Audio](https://www.rts.ch/audio-podcast/2022/audio/ukraine-comment-je-peux-aider-25805455.html)
----

### WWIII
1. [L'Irlande neutre militaire envisgae d'investir massivement dans sa défense - Zone Militaire](http://www.opex360.com/2022/03/02/lirlande-envisage-de-revoir-sa-tradition-de-neutralite-militaire-et-dinvestir-massivement-dans-sa-defense/)
----

### Guerre Hybride
1. [Sur le front de la cyberguerre - Nouvelles technologies](https://www.rfi.fr/fr/podcasts/nouvelles-technologies/20220305-sur-le-front-de-la-cyberguerre)
2. [Conflit en Ukraine: Visa et Mastercard suspendent leurs opérations en Russie](https://www.rfi.fr/fr/%C3%A9conomie/20220306-conflit-en-ukraine-visa-et-mastercard-suspendent-leurs-op%C3%A9rations-en-russie?ref=tw)
3. [Le gouvernement invite les groupes français à ne pas quitter la Russie dans la précipitation](https://www.lefigaro.fr/societes/pourquoi-les-entreprises-francaises-restent-en-russie-20220304)
4. [3-5 Quels réseaux sociaux sont les plus utilisés en Russie ?](https://twitter.com/fcazals/status/1500353905405931520?s=12)
5. [3-5 HermeticWiper, un redoutable virus informatique détecté en France, après s'être...](https://france3-regions.francetvinfo.fr/grand-est/meurthe-et-moselle/nancy/guerre-en-ukraine-hermeticwiper-un-redoutable-virus-informatique-detecte-en-france-apres-s-etre-attaque-aux-administrations-ukrainiennes-2479702.html)
6. [3-5 Putin says sanctions over Ukraine are like a declaration of war - BBC News](https://www.bbc.com/news/world-europe-60633482)
7. [3-3 TikTok Was Designed for War | WIREDMenuStory SavedCloseChevronStory SavedCloseSearchFacebookTwitterEmailSave StoryFacebookTwitterEmailSave StoryFacebookTwitterPinterestYouTubeInstagramTiktok](https://www.wired.com/story/ukraine-russia-war-tiktok/)
8. [03-2 Apple suspend ses ventes en Russie](https://twitter.com/EchosTechMedias/status/1498920661032607745?s=20&t=FBTGwr3HRgWZ6R4YQX9AiA)
9. [03-2 oligarques russes échappent aux sanctions en Suisse - rts.ch - Suisse](https://www.rts.ch/info/suisse/12906898-comment-des-oligarques-russes-echappent-aux-sanctions-en-suisse.html)
10. [03-1 Twitter : 1 label s pour les tweets des médias proche Kremlin](https://www.20minutes.fr/high-tech/3243923-20220301-twitter-label-specifique-tweets-reprenant-medias-proches-kremlin)
11. [03-1 VIASAT (Down) Internet FR Down by Russia](https://www.lefigaro.fr/secteur/high-tech/les-telecoms-victimes-de-cyberattaques-russes-20220228)
12. [03-01 influenceuses ukrainiennes expliquent comment démarrer 1 "VAB"](https://twitter.com/maxfreenews/status/1498548336118439937?s=20&t=2qwRtIUrGPpw4hnRc9BnOg)
13. [03-1 Comment des internautes informent les Russes grâce aux avis Google Maps](https://www.bfmtv.com/tech/guerre-en-ukraine-comment-des-internautes-tentent-d-informer-les-russes-grace-aux-avis-google-maps_AN-202203010508.html)
14. [02-28 : TikTok et Facebook suspenden RT et Sputnik en Europe](https://www.lemonde.fr/pixels/article/2022/02/28/tiktok-suspend-les-comptes-des-medias-d-etat-rt-et-sputnik-en-europe_6115606_4408996.html)
15. [2019 Poutine veut couper la Russie d’Internet](https://fr.businessam.be/poutine-veut-fermer-la-russie-a-partir-dinternet/)
16. [02-28 Poutine coupe internet en Russie](https://twitter.com/LPetiniaud/status/1498272486428921862?s=20&t=2qwRtIUrGPpw4hnRc9BnOg)
17. [02-28 Google Maps éteint le trafic en temps réel en Ukraine - Numerama](https://www.numerama.com/tech/868061-google-maps-eteint-le-trafic-en-temps-reel-en-ukraine.html)
18. [02-28 ARMAIII (JV) utiliser pour FakeNews](https://twitter.com/ipcyb_fr/status/1498257931258802180?s=12)
19. [02-27 1 numéro vert pour que les soldats russe  prisonniers](https://twitter.com/nielsack/status/1497833090605010945?s=20&t=HZwGFYMaWoeCyhGnW4Wyig)
20. [02-27 les soldats russes ont bombardé les femmes ukrainiennesr #Tinder](https://twitter.com/terryzim/status/1497826660611203075?s=12)
21. [02-27 Google aurait interdit RT sur le territoire ukrainien](https://twitter.com/souverainetech/status/1497876292653436930?s=12)
22. [02-26 #Russia government websites DOWN](https://twitter.com/netblocks/status/1497594515233951744?s=12)
23. [02-26 monitoring of all sanctions against Russia](https://correctiv.org/en/latest-stories/2022/03/01/sanctions-tracker-live-monitoring-of-all-sanctions-against-russia/)
24. [http://—— A TRIER — ——.com](http://—— A TRIER — ——.com)
----

### mesures
1. [5* Carto des Réactions à l’invasion de l’Ukraine - Le Grand Continent](https://legrandcontinent.eu/fr/2022/02/24/cartographier-les-reactions-a-linvasion-de-lukraine)
2. ["La Tc, Let et Ro ferment leurs espaces Aérien.](https://mobile.twitter.com/RestitutorOrien/status/1497565225674940420?ref_src=twsrc%5Etfw%7Ctwcamp%5Eembeddedtimeline%7Ctwterm%5Elist%3Abizcom%3A1497103507547209729%7Ctwgr%5Ee30%3D&ref_url=about%3Asrcdoc)
----

### FAKE NEWS
1. [EX : propagande et de la désinformation](https://twitter.com/harry_boone/status/1497589192221609989?s=12)
----

### Desinfo - Debunking
1. [OSINT : tweets par date et localisation, exemple de l’invasion russe en Ukr...](https://predictalab.medium.com/tutoriel-osint-rechercher-des-tweets-par-date-et-localisation-exemple-de-linvasion-russe-en-fbdb40850b94)
2. [éviter de partager une image manipulée. #GuerreEnUkraine](https://twitter.com/julienpain/status/1499337678273265664?s=12)
3. [2022-03-1 EX de Fake news : Debunker des Etoiles](https://twitter.com/debunkeretoiles/status/1498722533163552770?s=12)
4. [02-23 Démasquer la désinformation via les métadonnées](https://predictalab.medium.com/tutoriel-osint-comment-d%C3%A9masquer-la-d%C3%A9sinformation-gr%C3%A2ce-aux-m%C3%A9tadonn%C3%A9es-846d103e3a94)
5. [Dubious & debunked claims - Google Sheets](https://docs.google.com/spreadsheets/d/1bZ5uItSGLfYlCKT2kl0MEGYIF0gEhPaY68KVduwPw8M/edit#gid=0)
6. [BBC Monitoring (@BBCMonitoring) / Twitter](https://twitter.com/BBCMonitoring)
7. [BotSlayer track manipulations on Twitter](https://osome.iu.edu/tools/botslayer)
8. [Conseil du Founder de HoaxBuster](https://twitter.com/guillaume_hb/status/1496876472375853058?s=12)
----

### Process - FORK ME
1. [How to bulk upload a list of Twitter ID's (CSV) to a List on Twitter - Quora](https://www.quora.com/How-can-you-bulk-upload-a-list-of-Twitter-IDs-CSV-to-a-List-on-Twitter)
----

### TOOLS - META TOOLS -FORK DIS
1. [Iframe Scaffolder TOP](https://pirhoo.github.io/iframe-scaffolder/#/)
2. [Iframely Embeds API FeaturesIframelyLogo of IframelyLogo of TikTokLogo of FacebookLogo of GitHubLogo of YouTubeLogo of TwitterLogo of PinterestLogo of RedditLogo of SpotifyLogo of InstagramLogo of Apple MusicLogo of TwitchLogo of TableauLogo of GiphyLogo of Coub](https://iframely.com/features)
----

### Reddit Watch
1. [Reddit post notifier – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/en-US/firefox/addon/reddit-post-notifier/)
2. [Redditcomber.com](https://redditcomber.com/)
3. [Notifierforreddit.com](https://notifierforreddit.com)
----

