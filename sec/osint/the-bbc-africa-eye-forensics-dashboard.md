### Advanced Google Search & Translation
1. [Google Search Operators List](https://booleanstrings.com/2018/03/08/the-full-list-of-google-advanced-search-operators/)
2. [Google Translate](https://translate.google.com/)
3. [Babelfish.com](https://www.babelfish.com/)
----

### Aircraft Tracking
1. [FlightRadar24](https://www.flightradar24.com/)
2. [Radar Box](https://www.radarbox24.com/)
3. [Plane Finder](https://planefinder.net/)
4. [Flight Aware - Flightpath KML file with 3 month trial](https://flightaware.com/)
5. [ADS-B Exchange Real Time](https://global.adsbexchange.com/VirtualRadar/desktop.html)
6. [ADS-B Exchange - Unfiltered Flight Data](https://flight-data.adsbexchange.com/)
7. [Airportia](https://www.airportia.com/)
8. [Jet Photos](https://www.jetphotos.com/)
9. [Air Team Images](https://www.airteamimages.com/)
----

### Archiving
1. [Wayback Machine](https://archive.org/web/)
2. [Archive.ph](https://archive.ph/)
3. [Hunchly](https://www.hunch.ly/)
4. [Visual Ping - Emailed when a website changes](https://visualping.io/)
----

### Chronolocation -  Time
1. [SunCalc](http://www.suncalc.org/)
2. [Time and Date](https://www.timeanddate.com/)
3. [Shadow Calculator](http://shadowcalculator.eu/#/lat/50.08/lng/19.899999999999977)
4. [Wayback Machine](https://archive.org/web/)
5. [Sun Earth Tools - Annual Sun Paths](https://www.sunearthtools.com/)
6. [TIMEBIE](http://www.timebie.com/index.php)
----

### Coding Resources (Free)
1. [Code Academy](https://www.codecademy.com/)
2. [Free Code Camp](https://www.freecodecamp.org/)
3. [GitHub](https://github.com/)
4. [Code Beautify - HTML Decoder](https://codebeautify.org/html-decode-string)
5. [How to Use Twint](https://null-byte.wonderhowto.com/how-to/mine-twitter-for-targeted-information-with-twint-0193853/)
6. [Choco - Windows Package Manger](https://chocolatey.org/)
----

### Companies
1. [Open Corporates](https://opencorporates.com/)
2. [Aleph Search - OCCRP](https://aleph.occrp.org/)
3. [Catalogue of Research Databases](https://investigativedashboard.org/databases/)
4. [List of legal entity types by country](https://en.wikipedia.org/wiki/List_of_legal_entity_types_by_country)
5. [Global Incorporation Guide](https://www.lowtax.net/g/jurisdictions/)
----

### Cyber Security
1. [Signal](https://signal.org/)
2. [Wickr](https://wickr.com/)
3. [Wire](https://app.wire.com/auth/)
4. [Mega - E2E Cloud Storage](https://mega.nz/)
5. [ProtonEmail - Encrypted Email](https://mail.protonmail.com/inbox?welcome=true)
6. [TAILS](https://tails.boum.org/)
7. [Virus Total - URL/File Scan](https://www.virustotal.com/)
8. [Haveibeenpwned? - Breach Checker](https://haveibeenpwned.com/)
9. [DeHashed - Breach Checker](https://www.dehashed.com/)
10. [Last Pass](https://www.lastpass.com/solutions/business-password-manager)
11. [1password](https://1password.com/)
12. [Dashlane](https://www.dashlane.com/)
13. [Website Scan for Virus](https://sitecheck.sucuri.net/)
14. [GIJN Digital Security](https://gijn.org/digital-security/)
15. [Scam Survivors - Scam Help Forum](https://www.scamsurvivors.com/blackmail/#/)
16. [Install SecureDrop — SecureDrop 0.12.0 documentation](https://docs.securedrop.org/en/release-0.12.0/install.html)
----

### Emails
1. [Intelligence X - Email Searcher](https://intelx.io/)
2. [Hunter - Email Searcher](https://hunter.io/find/bbc.co.uk/Manye%20Shaniece)
3. [Norbert - Email Searcher](https://app.voilanorbert.com/#!/prospecting/manual)
4. [Free Email Provider Domains](https://gist.github.com/tbrianjones/5992856/)
----

### Geospatial Data
1. [Geonames](https://www.geonames.org/)
2. [Peakvisor.com](https://peakvisor.com/)
3. [Free GIS Data](https://freegisdata.rtwilson.com/)
4. [Co-ordinates Distance](http://www.synnatschke.de/geo-tools/distance-calculator.php)
5. [GADM](https://gadm.org/index.html)
----

### Militaries/Conflicts Resources
1. [Acled - Data on Crises](https://www.acleddata.com/)
2. [Camopedia - Camo Types](https://camopedia.org/index.php?title=Main_Page)
3. [WhoWasInCommand](https://whowasincommand.com/en/)
4. [Global Militarisation Index - BICC](https://gmi.bicc.de/)
5. [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
6. [Global Security.org - Africa](https://www.globalsecurity.org/military/world/africa.htm)
7. [National Armed Forces](https://armedforces.eu/)
8. [Airwars Conflict Tracking](https://airwars.org/f)
9. [Global Conflict Tracker](https://www.cfr.org/interactive/global-conflict-tracker?category=us)
10. [Nigeria Security Tracker](https://www.cfr.org/nigeria/nigeria-security-tracker/p29483)
----

### Image/Video Forensics
1. [InVid](https://www.invid-project.eu/)
2. [Yandex](https://yandex.com/)
3. [Google Image Search w/ keywords](https://www.google.com/search?q=google+image+search&rlz=1C1CHBF_en-GBGB842GB842&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjH9-ay35nnAhVEjqQKHSv5BoIQ_AUoAXoECBMQAw&biw=1920&bih=969)
4. [Bing Sectional Image Search](https://www.bing.com/images/trending?FORM=ILPTRD)
5. [TinEye](https://tineye.com/)
6. [Jeffrey's Metadata Viewer](http://exif.regex.info/exif.cgi)
7. [Metadata2go](https://www.get-metadata.com/)
8. [DiffCheck - Image Comparison](https://www.diffchecker.com/image-diff)
9. [Forensically](https://29a.ch/photo-forensics/#forensic-magnifier)
10. [YouTube Dataviewer - Amnesty](https://citizenevidence.amnestyusa.org/)
----

### Leaks, Dumps & Pastes
1. [Distributed Denial of Secrets](https://ddosecrets.com/)
2. [WikiLeaks](https://wikileaks.org/)
3. [Global Leaks - Annonymous Whistleblowing](https://www.globaleaks.org/)
4. [Offshore Leaks - ICIJ](https://offshoreleaks.icij.org/)
5. [LeakScraper](https://github.com/Acceis/leakScraper/wiki/leakScraper)
----

### Mapping Resources
1. [Google Maps](https://www.google.com/maps)
2. [Bing Maps](https://www.bing.com/maps)
3. [Yandex Maps](https://yandex.com/maps/)
4. [OpenStreetMap](https://www.openstreetmap.org/?edit_help=1#map=7/45.178/1.758&layers=H)
5. [Moriwo](https://satellites.pro/Nigeria_map)
6. [Wikimapia](http://wikimapia.org/#lang=en&lat=52.850000&lon=-2.716700&z=3&m=w)
7. [Mapillary](https://www.mapillary.com/)
8. [F4 Map Demo - 3D Buildings](https://demo.f4map.com/#lat=19.2584418&lon=42.4267751&camera.theta=0.9)
9. [Dual Maps](http://data.mashedworld.com/dualmaps/map.htm)
10. [Earth Explorer](https://earthexplorer.usgs.gov/)
11. [MapCarta](https://mapcarta.com/27675410)
12. [Qwant - Shops/Places Tags](https://www.qwant.com/maps/)
----

### Miscellaneous
1. [Bitly](https://app.bitly.com/Bjbl7DMN1Wm/bitlinks/)
2. [Falling Rain - Weather](http://www.fallingrain.com/index.html)
3. [FIRMS Fire Map](https://firms.modaps.eosdis.nasa.gov/map/)
4. [Data Tables](https://datatables.net/)
5. [Web Scraper Plugin](https://webscraper.io/)
6. [Google Factcheck](https://toolbox.google.com/factcheck/explorer)
7. [Help A Reporter](https://www.helpareporter.com/)
----

### Monitoring
1. [Tweetdeck](https://www.tweetdeck.twitter.com)
2. [Google Alerts](https://www.google.co.uk/alerts)
3. [Nuzzel](https://nuzzel.com/)
4. [Trendsmap](https://www.trendsmap.com/)
5. [Trends24](https://trends24.in/)
6. [Buzzsumo](https://buzzsumo.com/)
7. [Google Analytics](https://analytics.google.com/analytics/web/)
8. [Followerwonk](https://followerwonk.com/)
----

### Network Analysis
1. [Maltego](https://www.Maltego.com)
2. [VIS Desktop](https://docs.alephdata.org/guide/vis-desktop)
3. [Gephi](https://gephi.org/)
----

### Open Access & Academic Search
1. [Google Scholar](https://scholar.google.com)
2. [Core](https://core.ac.uk/)
3. [Talk to Books](https://books.google.com/talktobooks/)
4. [Text Analyzer Beta, from JSTOR Labs](https://www.jstor.org/analyze/)
5. [CORE](https://core.ac.uk/)
6. [The Library of Congress](https://www.loc.gov/)
7. [Open Knowledge Maps](https://openknowledgemaps.org/)
8. [GtR](https://gtr.ukri.org/)
9. [Directory of Open Access Journals](https://doaj.org/)
10. [BASE (Bielefeld Academic Search Engine): Basic Search](https://www.base-search.net/)
11. [Open Knowledge Maps](https://openknowledgemaps.org/)
12. [NATO Lib Guides](http://www.natolibguides.info/az.php)
13. [Project MUSE](https://muse.jhu.edu/)
14. [World Bank - Open data](https://data.worldbank.org/)
15. [Google Datasets](https://datasetsearch.research.google.com/)
----

### OSINT Tutorials/Resources
1. [OS Skills List](https://github.com/imuledx/OSINT_sources)
2. [Bellingcat's Online Investigation Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
3. [Sector035 - Week in OSINT](https://medium.com/@sector035)
4. [OSINT At Home - BenDoBrown](https://www.youtube.com/playlist?list=PLrFPX1Vfqk3ehZKSFeb9pVIHqxqrNW8Sy)
5. [OSINTcurio.us](https://osintcurio.us/events/b)
6. [Investigative Journalism Tools](https://www.mandalka.name/investigative_journalism/)
7. [OSINT Essentials](https://www.osintessentials.com/)
8. [Automate the Boring Stuff - Python](https://automatetheboringstuff.com/)
9. [E Investigator Tools](https://www.einvestigator.com/open-source-intelligence-tools/)
10. [Disinfo Tool List](https://www.disinfo.eu/2019/10/22/tools-to-monitor-disinformation/)
11. [Technopedia](https://www.techopedia.com/)
12. [OSINT Tutorials](https://start.me/p/m6XQ08/osint)
13. [Digital Journalism | OSINT Essentials](https://www.osintessentials.com/)
14. [WITNESS Media Lab | OSINT Digital Forensics - WITNESS Media Lab](https://lab.witness.org/projects/osint-digital-forensics/)
15. [Tor + VPN](https://www.vpnmentor.com/blog/tor-browser-work-relate-using-vpn/)
16. [Cloning a Git Repoistory](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)
17. [Git show&#39;s file path - Stack Overflow](https://stackoverflow.com/questions/30711854/git-shows-file-path)
18. [Intel Techniques - Course](https://inteltechniques.com/training/course-progress/)
19. [Fact Checks Archive | Snopes.com](https://www.snopes.com/fact-check/)
20. [GNU Free Software](https://www.gnu.org/gnu/about-gnu.html)
21. [Terrorism/Radicalisation Dashboard](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
22. [FirstDraft Online Verification Guide](https://firstdraftnews.org/wp-content/uploads/2019/10/Verifying_Online_Information_Digital_AW.pdf?x88639)
23. [FisrtDraft - Closed Networks Guide](https://firstdraftnews.org/wp-content/uploads/2019/11/Messaging_Apps_Digital_AW-1.pdf?x41819)
----

### Streetview
1. [Google Street view](https://www.google.com/maps)
2. [Mapillary - Street view](https://www.mapillary.com/app/)
3. [Instant Street View](https://www.instantstreetview.com/)
4. [Open Street Cam - A few streetviews](https://openstreetcam.org/map/@0.3426340661950777,32.38494873046876,10z)
----

### People Search
1. [Intelligence X](https://intelx.io/)
2. [SpiderFoot](https://www.spiderfoot.net/)
3. [Go FindWho](https://gofindwho.com/)
4. [Strava Global Heatmap](https://www.strava.com/heatmap#5.63/-111.45279/40.53696/hot/all)
5. [SnapMap](https://map.snapchat.com/@51.515400,-0.092461,12.00z)
6. [Namecheck - See if Person Present in Other Social Networks (checks if username available or not)](https://namechk.com/)
7. [Number Way - Local Phone Number Checker](https://www.numberway.com/)
----

### Satellite Imagery
1. [Google Earth Pro](https://www.google.com/intl/en_uk/earth/versions/)
2. [Sentinel Hub](https://www.sentinel-hub.com/)
3. [QGIS](https://qgis.org/en/site/)
4. [Landviewer](https://eos.com/lv/)
5. [Zoom Maps](https://zoom.earth/)
6. [Planet Explorer](https://www.planet.com/explorer/#/mosaic/global_monthly_2019_04_mosaic)
7. [Image Hunter by Apollo Mapping](https://imagehunter.apollomapping.com/)
8. [Remote Pixel | Home](https://remotepixel.ca/)
9. [Elevation Map.net - Placenames](https://elevationmap.net/)
10. [Satellites.pro](https://Satellites.pro)
11. [Wayback Imagery](https://livingatlas.arcgis.com/wayback/)
----

### Social Media Search
1. [--------------------Twitter----------------------](http://Twitter.com)
2. [Twitter Operands List](https://developer.twitter.com/en/docs/tweets/rules-and-filtering/overview/standard-operators)
3. [Twitter List Copy](http://projects.noahliebman.net/listcopy/index.php)
4. [Twint](https://github.com/twintproject/twint)
5. [Advanced Twitter Search Queries](https://github.com/igorbrigadir/twitter-advanced-search)
6. [Original Tweet Bookmarklet](https://twitter.com/wongmjane/status/1202293089395568640?s=19)
7. [Tweetbeaver ID](https://tweetbeaver.com/)
8. [Twitual - Analyse follows and Follow backs](http://twitual.com/)
9. [Twitur - Twtiur Graph Search](https://www.twitur.com/)
10. [All My Tweets - View all your tweets on one page](https://www.allmytweets.net/connect/)
11. [OriginVideo - Extension](https://github.com/briceleborgne/OriginVideo)
12. [TweetDelete](https://tweetdelete.net/)
13. [Twitter Fall](https://twitterfall.com/)
14. [Linkedin Sales - find the account to which the mail is attached, replace example@example.com](https://www.linkedin.com/sales/gmail?redirect=%2Fsales%2Fgmail%2Fprofile%2FviewByEmail%2Fexample%40example.com)
15. [Trends Map](https://www.trendsmap.com/)
16. [Followerwonk Analyse - User Analysis](https://followerwonk.com/analyze)
17. [Social Bearing](https://socialbearing.com/)
18. [Thread Reader](https://threadreaderapp.com/)
19. [--------------------Facebook----------------------](http://--------------------Facebook----------------------.com)
20. [Facebook Graph.Tips](https://graph.tips/beta/)
21. [Facebook Graph Searcher - Intelligence X](https://intelx.io/tools?tab=facebook)
22. [WhoPostedWhat- FB Keyword Search](https://whopostedwhat.com/)
23. [Facebook Transparency Ads](https://www.facebook.com/ads/library/report/)
24. [Facebook Scanner](https://stalkscan.com/)
25. [Find my Facebook ID | Find Facebook Group ID | Find Facebook Page ID](https://lookup-id.com/)
26. [--------------------Telegram----------------------](http://--------------------Telegram----------------------.com)
27. [Telegram Scraper Git](https://github.com/th3unkn0n/TeleGram-Scraper)
28. [Howtofind Telegram Channel - HowToFind: Unsorted - Telegram Directory](https://ru.tgchannels.org/channel/howtofind/25)
29. [Aware Online - Telegram group location search](https://www.aware-online.com/en/search-for-telegram-groups-based-on-location/)
30. [Search for Telegram groups based on location - Aware Online Academy](https://www.aware-online.com/en/search-for-telegram-groups-based-on-location/)
31. [--------------------Linkedin----------------------](http://--------------------Linkedin----------------------.com)
32. [Linkedin - Non-Login Linkedin Search (non-chrome)](http://www.linkedin/people/search.com)
33. [BAYT - Middle East&#39;s Linkedin](https://www.bayt.com/)
34. [LinkedIn - Search by email](https://www.linkedin.com/sales/gmail/profile/viewByEmail/ReplaceThis@byEmailDomain.com)
35. [--------------------Instagram----------------------](http://--------------------Instagram----------------------.com)
36. [Pictame - Insta Analyser](https://www.pictame.com/)
37. [Instagram-Scraper](https://github.com/rarcega/instagram-scraper)
38. [Instagram - InstagramOSINT](https://github.com/sc1341/InstagramOSINT)
39. [Picpanzee - Insta Viewer](https://picpanzee.com/)
40. [--------------------WhatsApp----------------------](http://--------------------WhatsApp----------------------.com)
41. [WhatsApp Group Links](https://whatsgrouplink.com/)
42. [--------------------Reddit----------------------](http://Twitter.com)
43. [Reddit - PushShift](http://files.pushshift.io/reddit/)
44. [Reddit - Reditr](http://reditr.com/)
45. [Reddit Investigator](https://www.redditinvestigator.com/)
46. [--------------------Other SOCINT----------------------](http://--------------------Other SOCINT----------------------.com)
47. [Boardreader - Forum Search Engine](http://boardreader.com/)
48. [Foller.me](https://foller.me/)
49. [Snapchat - Grab Snaps](https://github.com/CaliAlec/snap-map-private-api)
----

### Vessel/Ship Tracking
1. [Maritime Traffic](https://www.marinetraffic.com/)
2. [Vessel Finder](https://www.vesselfinder.com/)
3. [My Ship Tracking](https://www.myshiptracking.com/)
4. [Ship Company Details](https://www.maritime-database.com/)
5. [Maritime Connector](http://maritime-connector.com/ships/)
6. [Ports.com - Ports info](http://ports.com/)
7. [Fleetmon - Vessel Photo Database](https://www.fleetmon.com/community/photos)
8. [Unsplash - Vessels](https://unsplash.com/s/photos/vessel)
9. [Marine Traffic - Photos of Ships](https://www.marinetraffic.com/en/photos/of/ships)
10. [Military Ship Tracking](https://www.marinevesseltraffic.com/2013/02/military-ship-track.html)
----

### Weapons Tracing
1. [Weapons ID Database/Filter - SAS](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
2. [ID and Support Card - SAS](http://www.smallarmssurvey.org/fileadmin/docs/M-files/SmallArmsSurvey_ID_PlayingCards.pdf)
3. [SALW Visual Handbook - SAS](http://www.smallarmssurvey.org/resources/publications/by-type/handbooks/weapons-id-handbook.html)
4. [Small Weapons Filter - SAS](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
5. [SALW - Bonn International Centre for Conversion (BICC)](https://salw-guide.bicc.de/en)
6. [Africa Arms - BICC](https://www.bicc.de/research-clusters/project/project/capacity-development-and-advice-on-salw-control-in-africa-144/)
7. [Countries SALW - BICC](https://salw-guide.bicc.de/en/country)
8. [Munitions - BICC](https://salw-guide.bicc.de/en/ammunition/list)
9. [iTrace Arms - Conflict Armament Research (CAR) (EU)](https://itrace.conflictarm.com/)
10. [Arms Trade Treaty](http://www.att-assistance.org/documents)
11. [Top Gun Gallery](https://topgungallery.com/guns-database/)
12. [UN Small Arms Tracing](https://www.un.org/disarmament/convarms/small-arms-tracing/)
13. [Silah Report](http://silahreport.com/)
14. [Armament Reserch Services](http://armamentresearch.com/)
15. [International Arms Transfers](https://www.sipri.org/databases/armstransfers)
16. [Arms Industry Production](https://www.sipri.org/databases/armsindustry)
17. [Arms Embargoes](https://www.sipri.org/databases/embargoes)
18. [Arms Exports](https://www.sipri.org/databases/national-reports)
19. [Millitary Expenditure](https://www.sipri.org/databases/milex)
20. [Multilateral Peace Operations](https://www.sipri.org/databases/pko)
21. [SIPRI - Arms/Militaries Databases](https://www.sipri.org/databases)
22. [International Arms Transfers Database](http://armstrade.sipri.org/armstrade/html/tiv/index.php)
----

### Website Fingerprints
1. [Intelligence X](https://intelx.io/)
2. [ViewDNS.info](https://viewdns.info/)
3. [Whois - Domain lookup](https://who.is/)
4. [Arin - Web page registry](https://www.arin.net/)
5. [Domain Tools - Whois Lookup, Domain Availability &amp; IP Search](http://whois.domaintools.com/)
6. [Domain Big Data Domain Lookup](https://domainbigdata.com/)
7. [Shodan](https://www.shodan.io/)
8. [Historic Domain Registrar - Paid](https://www.domaintools.com/)
----

### Weather
1. [WolframAlpha - Weather](https://www.wolframalpha.com/)
2. [Zoom Earth](https://zoom.earth/)
----

