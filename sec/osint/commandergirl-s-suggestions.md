### People Search
1. [People search](https://radaris.com/)
2. [zaba search](https://zabasearch.com/)
3. [https://www.social-searcher.com/](https://www.social-searcher.com/)
4. [https://lullar-com-3.appspot.com/en](https://lullar-com-3.appspot.com/en)
5. [https://haveibeenpwned.com/](https://haveibeenpwned.com/)
6. [https://www.zlookup.com/](https://www.zlookup.com/)
7. [spydialer.com](https://spydialer.com)
8. [validate if VoIP number](https://freecarrierlookup.com/)
9. [fastpeoplesearch](https://www.fastpeoplesearch.com/)
10. [http://itools.com/search/people-search](http://itools.com/search/people-search)
11. [https://clustrmaps.com/](https://clustrmaps.com/)
12. [https://xlek.com](https://xlek.com)
13. [leakpeek.com](https://leakpeek.com)
14. [https://ufind.name/](https://ufind.name/)
15. [App site Universal](https://lampyre.io)
----

### People
1. [WebMii](https://webmii.com/)
2. [https://www.ussearch.com/](https://www.ussearch.com/)
3. [https://www.usatrace.com/](https://www.usatrace.com/)
4. [melissa](https://melissa.com)
----

### Alerts & Monitoring
1. [Google Alerts](https://Alerts.google.com)
2. [Pastebin.com - Login Page](https://pastebin.com/alerts)
3. [Find out if you’ve been part of a data breach](https://monitor.firefox.com/)
4. [https://haveibeenpwned.com/](https://haveibeenpwned.com/)
5. [https://xposedornot.com/](https://xposedornot.com/)
6. [https://wiki.harvard.edu/confluence/display/hmschommanual/Web+Archiving+with+Archive-It](https://wiki.harvard.edu/confluence/display/hmschommanual/Web+Archiving+with+Archive-It)
7. [https://viz.greynoise.io/alerts](https://viz.greynoise.io/alerts)
8. [https://exchange.xforce.ibmcloud.com/settings/watchlist](https://exchange.xforce.ibmcloud.com/settings/watchlist)
9. [https://intelx.io/account?tab=alerts](https://intelx.io/account?tab=alerts)
10. [Follow That Page](https://followthatpage.com/)
11. [Visualping](https://visualping.io/)
12. [Wachete](https://www.wachete.com/)
13. [Account Watchdog — DeHashed](https://dehashed.com/monitor)
14. [F5bot.com](https://f5bot.com/)
----

### Username Checks
1. [Namechk](https://namechk.com/)
2. [Check Usernames](https://checkusernames.com/)
3. [WhatsMyName Web](https://whatsmyname.app/)
4. [Namevine](https://namevine.com/)
5. [Lullar Com](https://lullar-com-3.appspot.com/en)
6. [Tracr.co](https://tracr.co/)
7. [https://www.snapdex.com/](https://www.snapdex.com/)
8. [Username Search](https://usersearch.org/)
9. [Namechk](https://namechk.com/)
10. [Namecheck.bloggingehow.com](https://NameCheck.BloggingEhow.com)
11. [https://searchusers.com/](https://searchusers.com/)
12. [https://biznar.com/](https://biznar.com/)
13. [Lullar Com](https://com.lullar.com/)
14. [https://analyzeid.com](https://analyzeid.com)
15. [Namevine](https://namevine.com/)
16. [IDCrawl](https://www.idcrawl.com)
17. [SearchPOF.com](https://searchpof.com/)
18. [KnowEm](https://knowem.com/)
19. [App site Universal](https://lampyre.io/)
----

### Phone number lookup
1. [Phonebook.cz](https://phonebook.cz/)
2. [Free Carrier Lookup](https://freecarrierlookup.com/)
3. [www.spydialer.com](https://www.spydialer.com)
4. [Phone Finder](https://tools.epieos.com/phone.php)
5. [https://www.telephonedirectories.us](https://www.telephonedirectories.us)
6. [Phonebook.cz](https://phonebook.cz/])
7. [https://callername.com/](https://callername.com/)
8. [SYNC](https://sync.me/)
9. [Who Called Me](https://www.whocalledme.com)
10. [Reverse Phone Lookup](https://callername.com/search)
----

### DATALEAK/BREACH ACCESS/CHECKER
1. [DeHashed — #FreeThePassword](https://www.dehashed.com)
2. [Thats them](https://thatsthem.com)
3. [ViewDNS.info](https://viewdns.info/)
4. [FireHOL IP Lists](https://iplists.firehol.org/#)
5. [ghostproject](https://ghostproject.fr/)
6. [https://centralops.net/](https://centralops.net/)
7. [https://leakix.net/](https://leakix.net/)
8. [App site Universal](https://lampyre.io/)
9. [Pastebin dump collection](https://psbdmp.cc/)
10. [Leak-Lookup](https://leak-lookup.com/databases)
11. [Weleakinfo.to](https://weleakinfo.to/)
12. [https://amibreached.com/](https://amibreached.com/)
13. [Personal Data Leak Checker: Your Email & Data](https://cybernews.com/personal-data-leak-check/)
14. [Leaked.site](https://leaked.site/)
15. [Leakhispano.net](https://leakhispano.net/)
----

### email/cloud/storage
1. [Яндекс](http://yandex.ru)
2. [Mega.nz](http://mega.nz)
3. [https://www.mediafire.com/](https://www.mediafire.com/)
----

### e-mail infoz
1. [https://emailrep.io/](https://emailrep.io/)
2. [Tools Epieos](https://tools.epieos.com)
3. [https://emailrep.io/login](https://emailrep.io/login)
4. [Welcome to nginx!](https://grep.app/)
5. [Emailcrawlr.com](https://emailcrawlr.com/)
6. [Emailrep.io](https://emailrep.io/)
7. [dnslytics.com/email-test](https://dnslytics.com/email-test)
----

### Discoverability/Search
1. [Shodan](https://www.shodan.io/)
2. [Insecam](https://www.insecam.org/)
3. [WiGLE: Wireless Network Mapping](https://wigle.net/)
4. [Publiccom](https://publicwww.com/)
----

### Analyze Text in images (OCR) opitcal character recognition
1. [Free Online OCR](https://www.newocr.com/)
2. [removes background on photos](https://www.remove.bg/)
3. [Visual Geometry Group - University of Oxford](https://www.robots.ox.ac.uk/~vgg/software/)
----

### HashTag "translator"
1. [Tagdef - the Social Media Dictionary](https://tagdef.com/en/)
----

### Text Compare
1. [Text Compare!](https://text-compare.com/)
2. [Translatetheweb.com](https://www.translatetheweb.com/)
----

### visuals
1. [Visualsitemapper.com](http://www.visualsitemapper.com)
2. [https://app.diagrams.net/](https://app.diagrams.net/)
3. [https://app.datawrapper.de/chart/YHeYF/upload](https://app.datawrapper.de/chart/YHeYF/upload)
4. [https://datavizcatalogue.com/](https://datavizcatalogue.com/)
5. [https://app.rawgraphs.io/](https://app.rawgraphs.io/)
6. [MindMup](https://www.mindmup.com/)
7. [See it, search it](https://www.bing.com/visualsearch)
8. [http://web-based reading.com](https://voyant-tools.org/)
----

### Search/Visualize Data Gathering
1. [Maltego](https://www.maltego.com/)
2. [IVRE](https://ivre.rocks/#)
3. [SpiderFoot](https://www.spiderfoot.net/)
----

### Reverse Image Search
1. [Pictures](https://www.picsearch.com/)
2. [Yandex](https://yandex.com)
----

### Geo
1. [geocreepy](https://www.geocreepy.com/)
----

### Maps
1. [Dual Maps v7: Google Maps, Street View and Bing Maps in an embeddable control](https://www.mapchannels.com/DualMaps.aspx)
----

### Crime Database/Statistics
1. [https://crime-data-explorer.fr.cloud.gov/pages/explorer/crime/crime-trend](https://crime-data-explorer.fr.cloud.gov/pages/explorer/crime/crime-trend)
2. [SHERLOC](https://sherloc.unodc.org/cld/v3/sherloc/index.jspx?tmpl=cybrepo)
3. [Information for Victims in Large Cases](https://www.justice.gov/largecases)
4. [for illinois https://icjia.illinois.gov/researchhub/](for illinois https://icjia.illinois.gov/researchhub/)
5. [Database of Legislation](https://sherloc.unodc.org/cld/v3/sherloc/legdb/index.html?lng=en)
----

### Google AdSense
1. [Reverse Google AdSense Lookup - MoonSearch](https://moonsearch.com/adsense/)
----

### People Info MISC (leaks, socials, drives, files, etc,.)
1. [Gofindwho.com](https://gofindwho.com/)
2. [Socialbearing](https://socialbearing.com/)
----

### Markets n such (DarkWeb)
1. [mlyusr6htlxsyc7t2f4z53wdxh3win7q3qpxcrbam6jf3dmua7tnzuyd.onion/captcha](http://mlyusr6htlxsyc7t2f4z53wdxh3win7q3qpxcrbam6jf3dmua7tnzuyd.onion/captcha)
2. [http://enxx3byspwsdo446jujc52ucy2pf5urdbhqw3kbsfhlfjwmbpj5smdad.onion/dnm/](http://enxx3byspwsdo446jujc52ucy2pf5urdbhqw3kbsfhlfjwmbpj5smdad.onion/dnm/)
3. [RAPTOR.life - darknet market directory](https://raptora2y6r3bxmjcd3xglr3tcakc6ezq3omyzbnvwahhpi27l3w4yad.onion.ws/)
----

### TORRENT/IP Data
1. [https://iknowwhatyoudownload.com/](https://iknowwhatyoudownload.com/)
----

### Guides
1. [EldritchData](https://eldritchdata.neocities.org/)
----

### IMEI Info
1. [IMEI-номера :: xinit.ru](https://xinit.ru/imei/)
----

### OSINT/Investigating/Forensic Guides/Note-taking
1. [HUNCHLY- OSINT Tools: Capturing Evidence & Notetaking - Forensic Notes](https://www.forensicnotes.com/osint-tools/#why_fn)
----

### Honeypot Data & Statistics
1. [Projecthoneypot.org](https://projecthoneypot.org/)
2. [HoneyDB](https://riskdiscovery.com/honeydb/)
----

### IP LOGGING
1. [Canarytokens](https://canarytokens.org/)
2. [CanaryTokens](https://www.stationx.net/canarytokens/)
3. [Canary Tokens - Hosted by Station X](https://whiteclouddrive.com/generate)
----

### IP-Domain-DNS-Network-URL
1. [FireHOL IP Lists](https://iplists.firehol.org/)
2. [ViewDNS.info](https://viewdns.info/)
3. [https://dnsdumpster.com/](https://dnsdumpster.com/)
4. [https://viz.greynoise.io/](https://viz.greynoise.io/)
5. [https://shadowcrypt.net/](https://shadowcrypt.net/)
6. [Wheredoesthislinkgo.com](http://wheredoesthislinkgo.com/)
7. [https://spyse.com/](https://spyse.com/)
8. [https://www.nmmapper.com/](https://www.nmmapper.com/)
9. [https://spyonweb.com/](https://spyonweb.com/)
10. [https://viewdns.info/](https://viewdns.info/)
11. [https://www.domainiq.com/tools](https://www.domainiq.com/tools)
12. [https://domainbigdata.com/](https://domainbigdata.com/)
13. [domainIQ](https://www.domainiq.com/tools)
14. [http://dnshistory.org/](http://dnshistory.org/)
15. [https://www.xml-sitemaps.com/](https://www.xml-sitemaps.com/)
16. [https://spyonweb.com/](https://spyonweb.com/)
17. [https://sitecheck.sucuri.net/](https://sitecheck.sucuri.net/)
18. [https://www.bulkseotools.com](https://www.bulkseotools.com)
19. [https://centralops.net/co/DomainDossier.aspx](https://centralops.net/co/DomainDossier.aspx)
20. [https://www.spyfu.com](https://www.spyfu.com)
21. [https://www.nmmapper.com/](https://www.nmmapper.com/)
22. [https://shadowcrypt.net](https://shadowcrypt.net)
23. [domainbigdata](https://domainbigdata.com/)
24. [NS Lookup](https://dnschecker.org/ns-lookup.php)
25. [DNS History](https://completedns.com/dns-history/)
26. [Whois History](https://osint.sh/whoishistory/)
27. [ICANN Lookup](https://lookup.icann.org/en/lookup)
28. [Whoisology](https://whoisology.com/)
29. [Whois.evtottav.com](https://whois.evtottav.com/)
30. [dnsdumpster](https://dnsdumpster.com/)
----

### Threat Intelligence
1. [Censys](https://censys.io/)
2. [www.virustotal.com](https://www.virustotal.com)
3. [packettotal.com](https://packettotal.com)
4. [urlscan.io](https://urlscan.io)
5. [shodan.io](https://shodan.io)
6. [Threat Crowd](https://www.threatcrowd.org/)
7. [Whtop.com](https://www.whtop.com/)
8. [IBM X-Force Exchange](https://exchange.xforce.ibmcloud.com/)
9. [https://checkphish.ai/](https://checkphish.ai/)
10. [https://phishcheck.me/](https://phishcheck.me/)
11. [https://app.any.run/](https://app.any.run/)
12. [https://talosintelligence.com/](https://talosintelligence.com/)
13. [Threat Crowd](https://www.threatcrowd.org/)
14. [https://www.threatminer.org/](https://www.threatminer.org/)
15. [https://sitereport.netcraft.com/](https://sitereport.netcraft.com/)
16. [https://pulsedive.com/](https://pulsedive.com/)
17. [https://www.hybrid-analysis.com/](https://www.hybrid-analysis.com/)
18. [https://www.urlvoid.com](https://www.urlvoid.com)
19. [piracema.io](https://www.piracema.io/about)
20. [Opswat.](https://metadefender.opswat.com/?lang=en)
21. [AbuseIPDB](https://www.abuseipdb.com/)
22. [Cisco Talos Intelligence Group](https://talosintelligence.com/)
23. [ONYPHE](https://www.onyphe.io/)
----

### Threat Hunting
1. [IBM X-Force Exchange](https://exchange.xforce.ibmcloud.com/)
2. [GreyNoise Visualizer](https://viz.greynoise.io/)
3. [RiskIQ Community](https://community.riskiq.com/)
4. [VirusTotal](https://www.virustotal.com/)
5. [Search - urlscan.io](https://urlscan.io/search/#*)
6. [Joe Sandbox](https://www.joesandbox.com/)
7. [Interactive Online Malware Analysis Sandbox](https://app.any.run/)
8. [AlienVault Open Threat Exchange](https://otx.alienvault.com/)
9. [PacketTotal](https://packettotal.com/)
10. [Censys](https://censys.io/)
11. [Spyse.com](https://spyse.com/)
12. [site check sucuri](https://sitecheck.sucuri.net/)
13. [https://pulsedive.com/](https://pulsedive.com/)
14. [VX Vault](http://vxvault.net/ViriList.php)
15. [Cybercrime-tracker.net](http://cybercrime-tracker.net/)
16. [Software ||  Cisco Talos Intelligence Group](https://talosintelligence.com/software)
----

### AWS/Buckets
1. [Public buckets by grayhatwarfare](https://buckets.grayhatwarfare.com/)
2. [Cloud Object Storage](https://aws.amazon.com/s3/)
----

### Image Hosting
1. [Wampi.ru](https://wampi.ru/)
2. [BigMIND](https://intelli1.zoolz.com/)
3. [Image.yandex.com](https://image.yandex.com)
4. [Tiny PiX](https://tinypix.top/)
5. [Mga.nz](https://mga.nz)
----

### Plain Text Hosting
1. [Pastebin](https://www.pastebin.com)
2. [https://bin.privacytools.io/](https://bin.privacytools.io/)
3. [https://paste.ubuntu.com/](https://paste.ubuntu.com/)
4. [Paste-bin.xyz](https://paste-bin.xyz/)
5. [https://snippet.host/](https://snippet.host/)
6. [https://tio.run/](https://tio.run/)
7. [https://ghostbin.co/](https://ghostbin.co/)
8. [https://hastebin.com/](https://hastebin.com/)
9. [ghostbin](https://ghostbin.com/)
10. [http://controlc.com/](http://controlc.com/)
11. [Pastefs.com](https://pastefs.com/)
12. [https://intelx.io/](https://intelx.io/?did=40e88ea8-0c32-4694-bfda-f66fd0f15245)
13. [Skidbin.net](https://skidbin.net/)
----

### Leak Tests (Clear Net)
1. [AmIUnique](https://amiunique.org/fp)
2. [Cover Your Tracks](https://coveryourtracks.eff.org/)
3. [https://www.whonix.org/wiki/Dev/Leak_Tests](https://www.whonix.org/wiki/Dev/Leak_Tests)
4. [http://check2ip.com/](http://check2ip.com/)
5. [https://www.cloudflare.com/ssl/encrypted-sni/](https://www.cloudflare.com/ssl/encrypted-sni/)
6. [https://www.doileak.com/](https://www.doileak.com/)
7. [https://www.whatismybrowser.com/](https://www.whatismybrowser.com/)
8. [https://www.dnsleaktest.com/](https://www.dnsleaktest.com/)
9. [https://ipleak.net/](https://ipleak.net/)
10. [https://privacy.net/analyzer/](https://privacy.net/analyzer/)
11. [https://www.stupidproxy.com/ip-address-user-agent/](https://www.stupidproxy.com/ip-address-user-agent/)
12. [https://browserleaks.com/](https://browserleaks.com/)
----

### Leak Tests (Dark Net)
1. [elfq2qefxx6dv3vy.onion/binfo_check_anonymity.php](http://elfq2qefxx6dv3vy.onion/binfo_check_anonymity.php)
2. [donionsixbjtiohce24abfgsffo2l4tk26qx464zylumgejukfq2vead.onion/test.php](http://donionsixbjtiohce24abfgsffo2l4tk26qx464zylumgejukfq2vead.onion/test.php)
----

### MAC adress lookup/Network
1. [MAC Address to Vendor Lookup - Stefan Frei](https://techzoom.net/lab/mac-address-lookup/)
2. [https://www.macvendorlookup.com/](https://www.macvendorlookup.com/)
3. [WiGLE: Wireless Network Mapping](https://wigle.net/)
----

### Reverse Analytics Google
1. [domainIQ](https://www.domainiq.com/reverse_analytics)
2. [Reverse Google Analytics Lookup](http://moonsearch.com/analytics/)
3. [Reverse Analytics Search](https://hackertarget.com/reverse-analytics-search/)
4. [SpyOnWeb](https://spyonweb.com/)
5. [Reverse Analytics Search](https://hackertarget.com/reverse-analytics-search/)
----

### Vehicle Identification (license plates)
1. [Platesmania.com](http://platesmania.com/)
2. [License Plates of the World](http://www.worldlicenseplates.com/)
3. [Intelligence X](https://intelx.io)
----

### Forums(clearnet)
1. [Fake-scam.info](https://www.fake-scam.info)
2. [HBH: Learn how hackers break in, and how to keep them out.](https://hbh.sh/home)
3. [Xss.is](https://xss.is)
4. [Data Security: Breach and Leak Search](https://www.keepds.com/breach/search)
5. [RaidForums -- Databases](https://raidforums.com/Forum-Databases)
6. [sINFUL SITE](https://sinfulsite.com/index.html)
----

### Non Profit / Charity INFO
1. [Charity Navigator](https://www.charitynavigator.org/)
2. [https://apps.irs.gov/app/eos/](https://apps.irs.gov/app/eos/)
3. [https://www.irs.gov/statistics/soi-tax-stats-annual-extract-of-tax-exempt-organization-financial-data](https://www.irs.gov/statistics/soi-tax-stats-annual-extract-of-tax-exempt-organization-financial-data)
4. [https://www.irs.gov/charities-non-profits/exempt-organizations-business-master-file-extract-eo-bmf](https://www.irs.gov/charities-non-profits/exempt-organizations-business-master-file-extract-eo-bmf)
5. [https://projects.propublica.org/nonprofits/](https://projects.propublica.org/nonprofits/)
----

### :D Some fun links
1. [How Many of Me](http://howmanyofme.com)
2. [Fake Person Generator](https://www.fakepersongenerator.com/)
----

### Fun things
1. [http://archived GIFS :D.com](http://archived GIFS :D.com)
----

### Fake WhatsApp Message Screenshot Generator
1. [Fake WhatsApp Chat Generator](http://www.fakewhats.com/generator)
----

### Leaked/Breached Databases (CLEARNET)
1. [All pages](https://ddosecrets.com/wiki/Special:AllPages)
2. [Search WikiLeaks](https://search.wikileaks.org/?query=)
3. [Cryptome.wikileaks.org](https://cryptome.wikileaks.org/?q=#results)
4. [ICIJ Offshore Leaks Database](https://offshoreleaks.icij.org/)
5. [LeakIX - Reports](https://leakix.net/reports)
6. [Leakhispano.net](https://leakhispano.net/)
7. [Discord | DDoSecrets Search](https://search.ddosecrets.com/data/discord/)
8. [breachdirectory.com/breaches](https://breachdirectory.com/breaches?lang=en)
9. [Leak - Lookup | Breaches](https://leak-lookup.com/breaches)
10. [Onion.Live](https://onion.live/?q=breached+data)
----

### MISC Social Media analytics
1. [Socialblade](https://socialblade.com/)
----

### Social Engineering/Osint Onion Forum
1. ["social ingenering" :')](http://commudazrdyhbullltfdy222krfjhoqzizks5ejmocpft3ijtxq5khqd.onion/forums/social_ingenering/)
----

### Independent Information about Russian Organizations
1. [E-ecolog.ru](https://e-ecolog.ru/)
2. [Поиск организаций и товарных знаков](https://companies.rbc.ru/)
3. [rusprofile](https://www.rusprofile.ru)
4. [Портал "Чекко" – Проверь любую компанию или ИП](https://checko.ru)
5. [inJust.pro](https://injust.pro/)
6. [Предоставление сведений из ЕГРЮЛ/ЕГРИП в электронном виде](https://egrul.nalog.ru/index.html)
7. [Федеральная налоговая служба](https://www.nalog.gov.ru/)
----

### Website info
1. [BuiltWith Technology Lookup](https://builtwith.com)
2. [http://carbondate.cs.odu.edu](http://carbondate.cs.odu.edu)
3. [MoonSearch](http://moonsearch.com/)
4. [Analyzeid](https://analyzeid.com/)
5. [Login to your Whoxy Account](https://www.whoxy.com/account/history.php)
6. [Publiccom- google tags](https://publicwww.com/)
----

### Extract Website Data Extension
1. [Data Miner](https://data-miner.io/)
----

### URL extract
1. [ConvertCSV](https://www.convertcsv.com)
2. [Data Miner](https://data-miner.io/)
3. [unfurl](https://dfir.blog/unfurl/)
----

### URL info
1. [https://www.bulkseotools.com/](https://www.bulkseotools.com/)
2. [https://unshorten.it/](https://unshorten.it/)
3. [https://www.getlinkinfo.com/](https://www.getlinkinfo.com/)
4. [URLhaus](https://urlhaus.abuse.ch/browse/)
5. [URL and website scanner](https://urlscan.io/)
6. [WebPageTest](https://www.webpagetest.org/)
7. [PhishTank](https://www.phishtank.com/)
8. [Extract URLs](https://www.convertcsv.com/url-extractor.htm)
9. [Analyzeid](https://analyzeid.com)
----

### Notetaking/Documentation
1. [Pocket](https://app.getpocket.com/)
2. [https://disk.yandex.com/notes/](https://disk.yandex.com/notes/)
3. [ShareX](https://getsharex.com/)
4. [Lightshot](https://prnt.sc/)
5. [Google Keep](https://keep.google.com)
6. [Hunchly](https://www.hunch.ly/)
7. [https://joplinapp.org/](https://joplinapp.org/)
8. [https://turtlapp.com/](https://turtlapp.com/)
9. [Note Taking App](https://www.zoho.com/notebook/)
----

### API Discovery
1. [apis.io/](https://apis.io/)
2. [https://www.whoxy.com/](https://www.whoxy.com/)
3. [https://api.spyonweb.com/](https://api.spyonweb.com/)
4. [Api.c99.nl](https://api.c99.nl/)
----

### Fake ID (Darknet)
1. [elfq2qefxx6dv3vy.onion/fakeid.php](http://elfq2qefxx6dv3vy.onion/fakeid.php)
2. [https://fakena.me/fake-name/](https://fakena.me/fake-name/)
----

### Validation/Fact Checking\
1. [https://hoaxy.osome.iu.edu/faq.php#](https://hoaxy.osome.iu.edu/faq.php#)
2. [https://reveal-mklab.iti.gr/reveal/index.html](https://reveal-mklab.iti.gr/reveal/index.html)
3. [https://toolbox.google.com/factcheck/explorer](https://toolbox.google.com/factcheck/explorer)
4. [https://www.snopes.com/](https://www.snopes.com/)
----

### Trending/Impact/Information/Story Detection
1. [Invid.weblyzard.com](https://invid.weblyzard.com/)
2. [https://trends.google.com/trends/](https://trends.google.com/trends/)
----

### AI FACES people and horses
1. [Gallery of AI Generated Faces](https://generated.photos/faces)
2. [This Person Does Not Exist](https://thispersondoesnotexist.com)
3. [www.thishorsedoesnotexist.com](https://thishorsedoesnotexist.com)
4. [https://generated.photos/faces](https://generated.photos/faces)
5. [Faceapp.com](https://www.faceapp.com/)
----

### Virtual Numbers
1. [TextNow](https://textnow.com)
2. [Voice](https://voice.google.com)
----

### Disposable Accounts
1. [https://www.guerrillamail.com/](https://www.guerrillamail.com/)
----

### Opt-Out First Steps
1. [Just Delete Me](https://backgroundchecks.org/justdeleteme/)
----

### Deleting/Sanitizing
1. [Darik's Boot And Nuke](https://www.dban.org)
2. [CCleaner Professional](https://www.ccleaner.com/ccleaner)
3. [https://www.bleachbit.org/](https://www.bleachbit.org/)
----

### Privacy
1. [privacytools.io](https://www.privacytools.io/)
2. [Privacy Test & Analyzer: See what information websites know about youPrivacy Check Tool](https://privacy.net/analyzer/)
3. [GRC | Security Now! Episode Archive](https://www.grc.com/securitynow.htm)
----

### OPT OUT data/marketing/advertising etc
1. [Privacy - About the DataChevron RightArrow RightPlayPlus](https://www.acxiom.com/about-us/privacy/atd/)
----

### Opt Out ADS/Data
1. [LexisNexis Information Suppression Request](https://optout.lexisnexis.com/)
2. [https://www.dmachoice.org/](https://www.dmachoice.org/)
3. [https://www.acxiom.com/data-privacy-ethics/](https://www.acxiom.com/data-privacy-ethics/)
4. [https://youradchoices.com/](https://youradchoices.com/)
5. [https://www.networkadvertising.org/](https://www.networkadvertising.org/)
----

### Secure email
1. [Riseup](https://www.riseup.net)
2. [ProtonMail](https://protonmail.com)
3. [Yandex](https://yandex.com/)
4. [https://www.openpgp.org/](https://www.openpgp.org/)
5. [https://www.enigmail.net/index.php/en/](https://www.enigmail.net/index.php/en/)
6. [https://www.openpgp.org/](https://www.openpgp.org/)
----

### Secure Communication +
1. [WhatsApp](https://www.whatsapp.com/)
2. [Telegram](https://telegram.me)
3. [https://signal.org/](https://signal.org/)
4. [XMPP](https://xmpp.org/)
5. [jabber.org](https://www.jabber.org/)
6. [Pidgin](https://www.pidgin.im/)
7. [https://otr.cypherpunks.ca/](https://otr.cypherpunks.ca/)
8. [The most secure collaboration platform · Wire](https://wire.com/en/)
9. [ChatSecure (iphone)](https://chatsecure.org/)
10. [Prosody.im](https://prosody.im/)
11. [https://www.xabber.com/web/](https://www.xabber.com/web/)
12. [Keybase](https://keybase.io/)
13. [https://element.io/](https://element.io/)
----

### Other (opt-out)
1. [My.telegram.org](https://my.telegram.org/)
----

### DarkWeb Linkz
1. [Qrtitjevs5nxq6jvrnrjyz5dasi3nbzx24mzmfxnuk2dnzhpphcmgoyd.onion](http://qrtitjevs5nxq6jvrnrjyz5dasi3nbzx24mzmfxnuk2dnzhpphcmgoyd.onion/)
----

### Russian Social Media/Search ENgines/PhoneNumber Lookup
1. [OK.ru](https://ok.ru)
2. [Поиск Mail.Ru](https://go.mail.ru)
3. [Sociuminfo.com](https://sociuminfo.com)
4. [Портал "Чекко" – Проверь любую компанию или ИП](https://checko.ru/)
5. [https://www.roum.ru//](https://www.roum.ru//)
6. [Яндекс](https://yandex.ru)
7. [RUSSIAN TELEGRAM ANALYTICS https://tgstat.ru/en](RUSSIAN TELEGRAM ANALYTICS https://tgstat.ru/en)
8. [ВКонтакте](https://m.vk.com/)
9. [Rutube](https://rutube.ru/)
10. [Career.habr.com](https://career.habr.com/)
11. [Нигма.РФ](http://nigma.ru/)
12. [Рамблер](http://www.rambler.ru/)
13. [Gismeteo](https://gismeteo.ru)
14. [Государственные услуги](https://gosuslugi.ru)
15. [SubscribeRu](https://subscribe.ru/)
16. [Referat.ru](https://www.referat.ru/)
17. [Whitepages.com.ru](https://whitepages.com.ru/)
18. [Russian-personals.com](https://russian-personals.com/)
19. [Anon Forum -- Russian](http://commudazrdyhbullltfdy222krfjhoqzizks5ejmocpft3ijtxq5khqd.onion/)
20. [Socialmediaholding.ru](https://socialmediaholding.ru/)
21. [rusprofile](https://www.rusprofile.ru/)
22. [Рамблер](https://www.rambler.ru/)
23. [rusprofile](https://www.rusprofile.ru/)
24. [Phone Book of Russia.com  +7   by Phonebook of the World.com](https://phonebookoftheworld.com/russia/)
----

### Russian Forums / links CompanyInfo
1. [http://www.ved.gov.ru/eng/companies/](http://www.ved.gov.ru/eng/companies/)
2. [PEP: PUBLIC DATABASE OF DOMESTIC POLITICALLY EXPOSED PERSONS OF RUSSIA AND BELARUS](https://rupep.org/en/)
----

### Deep Links DIR
1. [deep search](http://xjypo5vzgmo7jca6b322dnqbsdnp3amd24ybx26x5nxbusccjkm4pwid.onion/)
2. [Tor2Web: Tor Hidden Services Gateway](https://eu.onionsearchengine.com/)
3. [Tor2web.onionsearchengine.com](https://tor2web.onionsearchengine.com/)
4. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion/)
5. [Deep Links Dump - Uncensored Deep Web Link Directory](http://deepqelxz6iddqi5obzla2bbwh5ssyqqobxin27uzkr624wtubhto3ad.onion/)
6. ["The Safe Onion Links"](https://safeonu4c4xme2kzhuaer4ucm7enrjv23oznzwkz4d4f2wm7cc4ucuid.onion)
7. [DeepLink Onion Directory](http://mega7hxbfrjay3qkavy6jw2hw6lvkanaojxuottd45m4i4qr5wdenyad.onion/)
8. [Tasty Onions - Deep Web Link Directory](http://22tojepqmpah32fkeuurutki7o5bmb45uhmgzdg4l2tk34fkdafgt7id.onion/)
9. [DarkDir](http://l7vh56hxm3t4tzy75nxzducszppgi45fyx2wy6chujxb2rhy7o5r62ad.onion/)
10. [Onion Link Directory](http://torlinkuqd6ntr57y35m53pitu3qzfdbmhpkqmzn7yunzuwzrxjpmmyd.onion/link-list.html)
11. [Dark-Catalog](http://catalogxlotjjnu6vyevngf675vu6beefzxw6gd6a3fwgmgznksrqmqd.onion/forums.html)
12. [Link list v3.2.](http://torv5dp5n46htkqu42r3ywtt75ll2zu24a3yhcsgm6aslp43woclc2id.onion/)
13. [Ableonion Links](http://notbumpz34bgbz4yfdigxvd6vzwtxc3zpt5imukgl6bvip2nikdmdaad.onion/links/)
14. [MISC](http://omensx7ucu4gossi2nwn3l227ka73vsob36yytr3oe32u63ftgi4t4qd.onion/Links/links.html)
----

### Onion Search Engines / Links
1. [Venusoseaqnafjvzfmrcpcq6g47rhd7sa6nmzvaa4bj5rp6nm5jl7gad.onion](https://venusoseaqnafjvzfmrcpcq6g47rhd7sa6nmzvaa4bj5rp6nm5jl7gad.onion)
2. [OURREALM - search beyond censorship](http://orealmvxooetglfeguv2vp65a3rig2baq2ljc7jxxs4hsqsrcemkxcad.onion/)
3. [Phobos](http://phobosxilamwcg75xt22id7aywkzol6q6rfl2flipcqoc4e4ahima5id.onion/)
4. [dark.fail (limited)](http://darkfailenbsdla5mal2mxn2uz66od5vtzd5qozslagrfzachha3f3id.onion/)
5. [Ahmia](http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/)
6. [The Hidden Wiki](http://hiddenwwiqg2jb5s3wyvzxeipcl5ese2pa2cqnu6myi3d5bcmhmdagqd.onion/index.php/Main_Page)
7. [Dark Eye - profiles on the darknet, pgp keys, reviews](http://darkeyepxw7cuu2cppnjlgqaav6j42gyt43clcn4vjjf7llfyly5cxid.onion/)
8. [darkweblink.com onion version](http://dwltorbltw3tdjskxn23j2mwz2f4q25j4ninl5bdvttiy4xb6cqzikid.onion/)
9. [Digdeep4orxw6psc33yxa2dgmuycj74zi6334xhxjlgppw6odvkzkiad.onion](http://digdeep4orxw6psc33yxa2dgmuycj74zi6334xhxjlgppw6odvkzkiad.onion/)
10. [http://ankexpn6vk3qc5ooyyj7ufi6nmyt44vxbjtbxxkq4bxo7xzghai7kiqd.onion/ANK%20Stash%20-%20%E2%9A%A1hacking%E2%9A%A1%20-%20leaked-data%20[861360279076864090].html](http://ankexpn6vk3qc5ooyyj7ufi6nmyt44vxbjtbxxkq4bxo7xzghai7kiqd.onion/ANK%20Stash%20-%20%E2%9A%A1hacking%E2%9A%A1%20-%20leaked-data%20[861360279076864090].html)
11. [TorBot Search](http://torbotzotnpygayi724oewxnynjp4pwumgmpiy3hljwuou3enxiyq3qd.onion/)
12. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion/)
13. [TOR66](http://tor66sewebgixwhcqfnp5inzp5x5uohhdy3kvtnyfxc2e5mxiuh34iid.onion/)
14. [Deep Search - Search Engine](http://search7tdrcvri22rieiwgi5g46qnwsesvnubqav2xakhezv4hjzkkad.onion/)
15. [Torch: The Original Tor Search Engine](http://torchdeedp3i2jigzjdmfpn5ttjhthh5wbmda2rr3jvqjg5p77c54dqd.onion/search?query=forum)
16. [SearchDemon](http://srcdemonm74icqjvejew6fprssuolyoc2usjdwflevbdpqoetw4x3ead.onion/)
17. [Torgle - The Dark Web Search Engine](http://iy3544gmoeclh5de6gez2256v6pjh4omhpqdh2wpeeppjtvqmjhkfwad.onion/torgle/)
18. [ThirdEye Search Engine](http://3666eyedb2c6dlvtys4nkjfxnyfjjielks2fijvmdivqp2noku2fzqqd.onion/)
19. [EXCAVATOR search engine](http://2fd6cemt4gmccflhm6imvdfvli3nf7zn6rfrwpsy7uhxrgbypvwf5fad.onion/)
20. [Kraken Search Engine](http://krakenai2gmgwwqyo7bcklv2lzcvhe7cxzzva2xpygyax5f33oqnxpad.onion/)
21. [Tor SE](http://torse2ghyb4ytifb2yb7wxbttembxzhpvvoybgkrjyvwl2hutephs3yd.onion/)
22. [SENTOR search engine](http://e27slbec2ykiyo26gfuovaehuzsydffbit5nlxid53kigw3pvz6uosqd.onion/)
23. [OSS Onion Search Server](http://3fzh7yuupdfyjhwt3ugzqqof6ulbcl27ecev33knxe3u7goi3vfn2qqd.onion/oss/)
24. [Tor2Web: Tor Hidden Services Gateway](https://tor2web.onionsearchengine.com/)
25. [MetaGer -German Search Engine](http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion/)
26. [FindTor](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/)
27. [Dark Search Enginer](http://l4rsciqnpzdndt2llgjx3luvnxip7vbyj6k6nmdy4xs77tx6gkd24ead.onion/)
28. [abiko](http://abikoifawyrftqivkhfxiwdjcdzybumpqrbowtudtwhrhpnykfonyzid.onion/)
29. [GDARK Search Engine](http://zb2jtkhnbvhkya3d46twv3g7lkobi4s62tjffqmafjibixk6pmq75did.onion/gdark/search.php)
30. [haystak](http://haystak5njsmn2hqkewecpaxetahtwhsbsa64jom2k22z5afxhnpxfid.onion/)
31. [KILOS](http://mlyusr6htlxsyc7t2f4z53wdxh3win7q3qpxcrbam6jf3dmua7tnzuyd.onion/search)
----

### Onion Pastes/ Notes
1. [Cryptornetzamrhytcxhr3ekth6vom4ewns7pqxtywfvn5eezxgcqgqd.onion](http://cryptornetzamrhytcxhr3ekth6vom4ewns7pqxtywfvn5eezxgcqgqd.onion/)
2. [Black Cloud - Image Hosting](http://bcloudwenjxgcxjh6uheyt72a5isimzgg4kv5u74jb2s22y3hzpwh6id.onion/)
3. [BlackHost](https://blackhost7pws76u6vohksdahnm6adf7riukgcmahrwt43wv2drvyxid.onion/?id=pst)
----

### ClearNet Links to DarkWeb
1. [DarkWebWiki.org – Darknet Markets Links](https://darkwebwiki.org/)
2. [150+ Working Dark Web Links – Onion v3 | Darkweb-links.com](https://darkweb-links.com/150-dark-web-links-2021/#Search_Engines)
3. [Eu.onionsearchengine.com](https://eu.onionsearchengine.com/)
----

### Russian Phone numberlookup
1. [English.spravkaru.net](http://english.spravkaru.net)
2. [Russia Reverse Lookup 7, Phone Number Search](https://www.searchyellowdirectory.com/reverse-phone/7/)
3. [Russian Companies Directory](http://www.ved.gov.ru/eng/companies/)
4. [CIS White Pages: People Search for Free in Russia, Ukraine, Belarus, Kazahstan, Latvia, Moldova](http://english.spravkaru.net/?nj)
5. [International Calling Cards](https://www.comfi.com)
6. [Поиск по номеру телефона](https://mysmsbox.ru/phone-search)
7. [Ru-98.ru](https://ru-98.ru/)
8. [https://кто-звонит.рф](https://кто-звонит.рф)
9. [Определить оператора и регион по номеру телефона](https://phonenum.info/)
10. [Mysmsbox.ru](https://mysmsbox.ru)
11. [https://xn----dtbofgvdd5ah.xn--p1ai/](https://xn----dtbofgvdd5ah.xn--p1ai/)
12. [8call.ru](https://8call.ru)
13. [Apertonet.ru](https://apertonet.ru)
14. [Znep.ru](https://znep.ru/)
15. [Apk-shatura.ru](https://apk-shatura.ru)
16. [Allnum.ru](http://allnum.ru)
17. [Interweb.spb.ru](http://interweb.spb.ru)
18. [Определить оператора, регион и страну по номеру телефона](https://phonenum.info/phone/)
19. [Phone Number Track](https://www.phonenumbertrack.com)
----

### telegrambots
1. [Telegram: Contact @GetCont_bot](http://t.me/GetCont_bot)
2. [Telegram: Contact @mailsearcher_bot](https://t.me/mailsearcher_bot)
3. [Telegram: Contact @getfb_bot](http://t.me/getfb_bot)
4. [Telegram: Contact @Tpoisk_Bot](http://t.me/Tpoisk_Bot)
5. [Telegram: Contact @Tpoisk_Bot](http://t.me/Tpoisk_Bot)
6. [Telegram: Contact @bmi_np_bot](https://t.me/bmi_np_bot)
7. [Telegram: Contact @numberPhoneBot](https://t.me/numberPhoneBot)
8. [Ssb_russian_probiv_bot.com](http://@ssb_russian_probiv_bot.com)
9. [Whoisdombot.com](http://@whoisdombot.com)
10. [Vkfindface_bot.com](http://@vkfindface_bot.com)
11. [Приложение TELEGRAM AVinfoBot](http://@avinfobot.com)
12. [Phone_avito_bot.com](http://@phone_avito_bot.com)
13. [Clerkinfobot.com](http://@clerkinfobot.com)
14. [Telegram: Contact @howtofindbot](https://t.me/howtofindbot)
----

### Guides: RU
1. [Telegram боты для пробива инфы | Planworld](https://planworld.ru/ru/telegram/telegram-boty-dlya-probiva-infy.html)
2. [iPhones.ru](https://www.iphones.ru/)
----

### Software - Privacy (onion)
1. [Whonix](http://www.dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/)
2. [QUBES OS](http://qubesosfasa4zl44o4tws22di6kepyzfeqv3tg4e3ztknltfxqrymdad.onion/)
3. [OnionShare - OpenSource tool for secure and anonymous sharing of files. Hosting & chatting through Tor](http://lldan5gahapx5k7iafb3s4ikijc4ni7gx5iywdflkba5y2ezyg6sjgyd.onion/)
4. [correcthorse.pw (clearnet version) http://correct2qlofpg4tjz5m7zh73lxtl7xrt2eqj27m6vzoyoqyw4d4pgyd.onion/](http://correct2qlofpg4tjz5m7zh73lxtl7xrt2eqj27m6vzoyoqyw4d4pgyd.onion/)
5. [Secure Password Generator](http://password2fofn6xamqgltp6il66zg3smsr2426qlmvucjufkpbbazmad.onion/cgi-bin/generator.cgi)
----

### Sourcing
1. [Boolean Strings](https://booleanstrings.com/)
----

### INDEXING/CRAWLING WEB
1. [https://www.webcrawler.com/](https://www.webcrawler.com/)
2. [https://robhammond.co](https://robhammond.co)
3. [https://robhammond.co/tools/](https://robhammond.co/tools/)
----

### MetaSearch Engines
1. [about - ToS;DR Search](https://search.tosdr.org)
2. [Google Scholar](https://scholar.google.com)
3. [Gigablast](https://gigablast.com/)
4. [Private.sh](https://private.sh/)
5. [Ecosia](https://www.ecosia.org)
6. [Dogpile](https://www.dogpile.com/)
7. [Excite](https://www.excite.com/)
8. [Social Searcher](https://www.social-searcher.com)
9. [Search engine and human edited web directory KartOO](http://www.kartoo.com/)
10. [Lookahead.surfwax.com](http://lookahead.surfwax.com/)
11. [info.com](https://info.com)
12. [peekier](https://peekier.com)
13. [carrot2](https://search.carrot2.org/#/search/web)
----

### Search Engines
1. [https://www.dogpile.com/](https://www.dogpile.com/)
2. [https://ahmia.fi/](https://ahmia.fi/)
3. [https://eu.onionsearchengine.com/](https://eu.onionsearchengine.com/)
4. [Comparethesearchengines.com](https://comparethesearchengines.com/)
5. [Custom Search  |  Google Developers](https://developers.google.com/custom-search)
6. [CC Search](https://search.creativecommons.org/)
7. [Anonymous Proxy Search Engine](https://gibiru.com/)
8. [OneSearch](https://www.onesearch.com/)
9. [Startpage](https://STARTPAGE.COM)
10. [Wiki.com](https://www.wiki.com/)
11. [Boardreader](https://boardreader.com/)
12. [Neeva](https://neeva.com/)
13. [Wolfram|Alpha](https://www.wolframalpha.com/)
----

### Academic/Scholarly Search Engines
1. [Google Scholar](https://www.scholar.google.com)
2. [Scinapse.io](https://scinapse.io/)
3. [Msearch.io](https://msearch.io/)
4. [ResearchGate](https://www.researchgate.net/)
5. [Directory of Open Access Journals](https://doaj.org/)
6. [https://www.copyscape.com/](https://www.copyscape.com/)
7. [https://education.iseek.com/iseek/home.page](https://education.iseek.com/iseek/home.page)
8. [https://www.refseek.com/](https://www.refseek.com/)
9. [Microsoft Academic](https://academic.microsoft.com/home)
10. [https://www.jstor.org/](https://www.jstor.org/)
11. [Www.base-search.net](https://www.base-search.net/\)
12. [https://www.doaj.org/](https://www.doaj.org/)
13. [ERIC](https://eric.ed.gov/)
14. [Www.wolframalpha.com](https://www.wolframalpha.com/)
15. [https://community.libguides.com/](https://community.libguides.com/)
16. [lumendatabase.org](https://lumendatabase.org/)
17. [Sci-hub.ru](https://sci-hub.ru/)
----

### Advanced Searching
1. [Darksearch.io (](https://darksearch.io/)
2. [Secret Search Engine Labs](http://www.secretsearchenginelabs.com/)
3. [Eu.onionsearchengine.com](https://eu.onionsearchengine.com/)
4. [Centralops](https://centralops.net/)
5. [ahmia.fi](http://ahmia.fi)
6. [https://epieos.com/](https://epieos.com/)
7. [https://www.yougetsignal.com/](https://www.yougetsignal.com/)
8. [data.occrp.org](https://data.occrp.org/)
9. [As.onionsearchengine.com](https://as.onionsearchengine.com/)
10. [ONYPHE](https://www.onyphe.io/)
11. [ExoneraTor – Tor Metrics](https://metrics.torproject.org/exonerator.html)
12. [Riddler.io](https://riddler.io/)
13. [Ahmia —
      Search Tor Hidden Services](https://ahmia.fi/)
----

### Google Dorking/Hacking
1. [Offensive Security’s Exploit Database Archive](https://www.exploit-db.com/google-hacking-database)
2. [Google Advanced Search Operators ~ Supple](https://supple.com.au/tools/google-advanced-search-operators/)
3. [Google Hacking Diggity Project Resources](https://resources.bishopfox.com/resources/tools/google-hacking-diggity/)
4. [DorkSearch](https://dorksearch.com/)
5. [find public files in google drive***](https://www.dedigger.com/#gsc.tab=0)
----

### Global FTP Search Engines
1. [Global FTP Search Engine: Global File Search Engine](https://globalfilesearch.com/)
2. [https://snowfl.com/](https://snowfl.com/)
3. [Mamont. Largest FTP Search Engine](https://www.mmnt.ru/int/)
----

### Advanced Search/Multipurpose
1. [Darksearch.io](https://darksearch.io/)
2. [https://technisette.com](https://technisette.com)
3. [https://intelx.io/](https://intelx.io/)
4. [Darkschn4iw2hxvpv2vy2uoxwkvs2padb56t3h4wqztre6upoc5qwgid.onion](https://darkschn4iw2hxvpv2vy2uoxwkvs2padb56t3h4wqztre6upoc5qwgid.onion)
5. [Ahmia —
      Search Tor Hidden Services](https://ahmia.fi/)
----

### Photo/Face REVERSE Search Engines
1. [TinEye](https://tineye.com/)
2. [Google Images](https://images.google.com/)
3. [Pictriev](https://pictriev.com)
4. [iLookLikeYou.com](https://ilooklikeyou.com)
5. [Facesearch.com](https://facesearch.com)
6. [Face search • PimEyes](https://pimeyes.com/en/)
7. [Findclone](https://findclone.ru/)
----

### Data Set Research
1. [Datasetsearch.research.google.com](https://datasetsearch.research.google.com)
2. [SpyOnWeb](https://spyonweb.com/)
3. [ONYPHE](https://www.onyphe.io/)
4. [Search WikiLeaks](https://search.wikileaks.org/)
----

### Digital Forensics (DFIR)
1. [Home Page](https://www.dfir.training/)
----

### Open Source Investigating/Data Gathering/Rea
1. [Research Clinic](http://researchclinic.net/index.htm)
2. [Bellingcat's Online Investigation Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
3. [https://inteltechniques.com/](https://inteltechniques.com/)
----

### Photo Forensics
1. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
2. [https://29a.ch/photo-forensics/#forensic-magnifier](https://29a.ch/photo-forensics/#forensic-magnifier)
3. [FotoForensics](https://fotoforensics.com/)
4. [Exifinfo.org](https://exifinfo.org/)
5. [https://exifcleaner.com/](https://exifcleaner.com/)
6. [Metadata Viewer](https://www.extractmetadata.com/)
7. [Pic2Map Photo Location Viewer](https://www.pic2map.com/)
8. [https://www.freemaptools.com/](https://www.freemaptools.com/)
9. [https://29a.ch/photo-forensics/#forensic-magnifier](https://29a.ch/photo-forensics/#forensic-magnifier)
10. [https://intelx.io/tools?tab=filetool](https://intelx.io/tools?tab=filetool)
11. [Shows and removes EXIF DATA](http://www.verexif.com/en/)
12. [theXifer.net](https://www.thexifer.net/)
13. [ExifTool by Phil Harvey](https://exiftool.org/index.html)
14. [Image info online](http://linkstore.ru/exif/)
----

### File Forensics
1. [https://29a.ch/photo-forensics/#level-sweep](https://29a.ch/photo-forensics/#level-sweep)
2. [https://quicksand.io/](https://quicksand.io/)
3. [https://malware.sekoia.fr/new](https://malware.sekoia.fr/new)
----

### Internet of Things Searches- loTData
1. [thingful](https://www.thingful.net/)
2. [zoomeye](https://zoomeye.org)
3. [Shodan](https://shodan.io)
4. [Insecam](http://www.insecam.org/)
----

### Find Alternative Site
1. [https://alternativeto.net/software/leakbase/](https://alternativeto.net/software/leakbase/)
----

### DarkNetemail
1. [Hxuzjtocnzvv5g2rtg2bhwkcbupmk7rclb6lly3fo4tvqkk5oyrv3nid.onion](http://hxuzjtocnzvv5g2rtg2bhwkcbupmk7rclb6lly3fo4tvqkk5oyrv3nid.onion/)
2. [cock.li](http://rurcblzhmdk22kttfkel2zduhyu3r6to7knyc7wiorzrx5gw4c3lftad.onion/)
----

### Onion links/diurectories cont
1. [Rznvg5sjacavz5kpshrq4urm75xzruha6iiyuggidnioo5ztvwdfroyd.onion](http://rznvg5sjacavz5kpshrq4urm75xzruha6iiyuggidnioo5ztvwdfroyd.onion/)
2. [http://lnbpcgk4mem5vvvwilqsk7yb5i2bgtjphnvq37kajdycu56i5omt4cqd.onion/cat/7](http://lnbpcgk4mem5vvvwilqsk7yb5i2bgtjphnvq37kajdycu56i5omt4cqd.onion/cat/7)
3. [Jaz45aabn5vkemy4jkg4mi4syheisqn2wn2n4fsuitpccdackjwxplad.onion](http://jaz45aabn5vkemy4jkg4mi4syheisqn2wn2n4fsuitpccdackjwxplad.onion/)
4. [Qrtitjevs5nxq6jvrnrjyz5dasi3nbzx24mzmfxnuk2dnzhpphcmgoyd.onion](http://qrtitjevs5nxq6jvrnrjyz5dasi3nbzx24mzmfxnuk2dnzhpphcmgoyd.onion/)
5. [Torlinkuqd6ntr57y35m53pitu3qzfdbmhpkqmzn7yunzuwzrxjpmmyd.onion](http://torlinkuqd6ntr57y35m53pitu3qzfdbmhpkqmzn7yunzuwzrxjpmmyd.onion/)
6. [http://zaejy2lskzkbxo5vlc3qxs4mpwg25lgoadwtwxquvhkkq5eid43oqryd.onion/wiki/index.php](http://zaejy2lskzkbxo5vlc3qxs4mpwg25lgoadwtwxquvhkkq5eid43oqryd.onion/wiki/index.php)
7. [Wclekwrf2aclunlmuikf2bopusjfv66jlhwtgbiycy5nw524r6ngioid.onion](http://wclekwrf2aclunlmuikf2bopusjfv66jlhwtgbiycy5nw524r6ngioid.onion/)
8. [http://wclekwrf2aclunlmuikf2bopusjfv66jlhwtgbiycy5nw524r6ngioid.onion/hlink-category/catalogue/](http://wclekwrf2aclunlmuikf2bopusjfv66jlhwtgbiycy5nw524r6ngioid.onion/hlink-category/catalogue/)
9. [Pvhwsb7a3d2oq73xtr3pzvmrruvswyqgkyahcb7dmbbfftr4qtsmvjid.onion](http://pvhwsb7a3d2oq73xtr3pzvmrruvswyqgkyahcb7dmbbfftr4qtsmvjid.onion/)
10. [Gtl3bpvdnzijqmna2sd25ktixabzt7pval6fpkd2ur6vpov52zzv74yd.onion](http://gtl3bpvdnzijqmna2sd25ktixabzt7pval6fpkd2ur6vpov52zzv74yd.onion/)
11. [Ryf3kesi2lx6ujbg66bqqoo2grsgcavcqpvovp2ovwi73s3yqinrhhad.onion](http://ryf3kesi2lx6ujbg66bqqoo2grsgcavcqpvovp2ovwi73s3yqinrhhad.onion/)
12. [Rqagvqs4qx3t5masyja42io7ukaffohowgauvymi2xgcp26chrirf5yd.onion](http://rqagvqs4qx3t5masyja42io7ukaffohowgauvymi2xgcp26chrirf5yd.onion/)
13. [http://gtl3bpvdnzijqmna2sd25ktixabzt7pval6fpkd2ur6vpov52zzv74yd.onion/index.html#search-engines](http://gtl3bpvdnzijqmna2sd25ktixabzt7pval6fpkd2ur6vpov52zzv74yd.onion/index.html#search-engines)
14. [Deepxfmkjlils3vd7bru5rrxy3x3lchjgmv3wsgycjfynktmuwgm64qd.onion](http://deepxfmkjlils3vd7bru5rrxy3x3lchjgmv3wsgycjfynktmuwgm64qd.onion/)
15. [Bwravzt2xdkc3r3ha6ivyizbtxw4bplohjaswqfe6zlh5tt22eb5iaid.onion](http://bwravzt2xdkc3r3ha6ivyizbtxw4bplohjaswqfe6zlh5tt22eb5iaid.onion/)
16. [Onionu2dme6j7myzoyotcpzzmo6vmozl2vmflucwogr2euc2nucqc7yd.onion](http://onionu2dme6j7myzoyotcpzzmo6vmozl2vmflucwogr2euc2nucqc7yd.onion/)
17. [Linkssfpaeihh6nkzf373e45avva6x7kjkiqnceomh275motaxoc3ead.onion](http://linkssfpaeihh6nkzf373e45avva6x7kjkiqnceomh275motaxoc3ead.onion/)
18. [2ja7yz5b3owazz76uvs3zjgcobf4ui63afscjc4azlu67khfuy3iq5yd.onion](http://2ja7yz5b3owazz76uvs3zjgcobf4ui63afscjc4azlu67khfuy3iq5yd.onion/)
19. [Russianvii3zs27lht5jvwuppli77k47567bbv5o3bsos4qxxbu67sid.onion](http://russianvii3zs27lht5jvwuppli77k47567bbv5o3bsos4qxxbu67sid.onion/)
20. [UnderDir](http://underdiriled6lvdfgiw4e5urfofuslnz7ewictzf76h4qb73fxbsxad.onion/)
21. [donionsixbjtiohce24abfgsffo2l4tk26qx464zylumgejukfq2vead.onion/onions.php](http://donionsixbjtiohce24abfgsffo2l4tk26qx464zylumgejukfq2vead.onion/onions.php)
----

### darkweb directory links (clearnet)
1. [Dark Web Links](https://darkweblinks.com/)
2. [Энтузиаст создал самый полный и самый бесполезный список сайтов даркнета — «Хакер»](https://xakep.ru/2017/05/11/darkweb-index/)
3. [OnionLand Search](https://onionlandsearchengine.com/)
----

### Onion Links for checking breached/leaked data
1. [Search Instagram Users Leaked Password Database](http://breachdbsztfykg2fdaq2gnqnxfsbj5d35byz3yzj73hazydk4vq72qd.onion/)
2. [Deep Search](http://xjypo5vzgmo7jca6b322dnqbsdnp3amd24ybx26x5nxbusccjkm4pwid.onion/)
3. [http://ddosxlvzzow7scc7egy75gpke54hgbg2frahxzaw6qq5osnzm7wistid.onion/wiki/Distributed_Denial_of_Secrets](http://ddosxlvzzow7scc7egy75gpke54hgbg2frahxzaw6qq5osnzm7wistid.onion/wiki/Distributed_Denial_of_Secrets)
4. [Instagram - Leaked Password Database (Limited)](http://breachdbsztfykg2fdaq2gnqnxfsbj5d35byz3yzj73hazydk4vq72qd.onion/LeakedPass)
5. [FreedomHQ](http://freedomzw5x5tzeit4jgc3gvic3bmecje53hwcoc3nnwe2c3gsukdfid.onion/databases?page=2)
6. [Trashchan public DB dump](http://trashbakket2sfmaqwmvv57dfnmacugvuhwxtxaehcma6ladugfe2cyd.onion/)
----

### onion forums
1. [http://darkzzx4avcsuofgfez5zq75cqc4mprjvfqywo45dfcaxrwqg6qrlfid.onion/onions/](http://darkzzx4avcsuofgfez5zq75cqc4mprjvfqywo45dfcaxrwqg6qrlfid.onion/onions/)
2. [DIR of Links for Search Engines, Catalogs, Forums, News, ETC](http://reycdxyc24gf7jrnwutzdn3smmweizedy7uojsa7ols6shttp://reycdxyc24gf7jrnwutzdn3smmweizedy7uojsa7ols6sflwu25ijoyd.onion/tags/flwu25ijoyd.onion/2019/11/14/onionlinks/#Forums)
3. [$$$ DARK0DE - market, databases, etc](http://darkodemard3wjoe63ld6zebog73ncy77zb2iwjtdjam4xwvpjmjitid.onion/search/Database/all/1)
4. [Helium Forum & Marketplace](http://7qiouusobthfxjw47albrqffk4lbqwauqlu6dpwh4kosddqt2x26ocad.onion/#)
5. [$$$ DARKNETFORUM](http://7qiouusobthfxjw47albrqffk4lbqwauqlu6dpwh4kosddqt2x26ocad.onion/#)
6. [$$$ HiddenForum](http://hiddenuip5qlthdkbeqrpcfja4k5qr5urordvm4sm3gnz6wcy7yo5qqd.onion/forum)
7. [Dread - Forum :)](http://dreadytofatroptsdj6io7l3xptbet6onoyno2yv7jicoxknyazubrad.onion/)
8. [XSS.IS forum on Darkenet](http://xssforolrt4zlzqsvinl5tg3wgx33nc2abrvmrreauvafy4jxqseomad.onion/)
9. [Suprbaydvdcaynfo4dgdzgxb4zuso7rftlil5yg5kqjefnw4wq4ulcad.onion](http://suprbaydvdcaynfo4dgdzgxb4zuso7rftlil5yg5kqjefnw4wq4ulcad.onion/)
10. [http://suprbaydvdcaynfo4dgdzgxb4zuso7rftlil5yg5kqjefnw4wq4ulcad.onion/search.php](http://suprbaydvdcaynfo4dgdzgxb4zuso7rftlil5yg5kqjefnw4wq4ulcad.onion/search.php)
11. [8chan](http://4usoivrpy52lmc4mgn2h34cmfiltslesthr56yttv2pxudd3dapqciyd.onion/)
----

### Hacking Forums on the Clearnet
1. [Sinisterly](https://sinister.ly/)
2. [Demon Forums - Advanced Hacking](https://demonforums.net/Forum-Advanced-Hacking)
3. [Cracked](https://cracked.to/)
4. [hackforums](https://hackforums.net/)
5. [Nulled](https://www.nulled.to/)
6. [RaidForums](https://raidforums.com/)
7. [0x00sec](https://0x00sec.org/)
8. [BlackHatWorld](https://www.blackhatworld.com/)
9. [HACKCRAZE - The Hacking & Security Forum](https://hackcraze.com/)
10. [Nulledbb.com](https://nulledbb.com/)
11. [Cracking.org](https://cracking.org/)
12. [Crackia.com](https://crackia.com/)
13. [Cracking Portal | Cracking Begins | Best Cracking Forum](https://crackingportal.com/)
----

### BlackHat Sources (darkWeb)
1. [Blkhatjxlrvc5aevqzz5t6kxldayog6jlx5h7glnu44euzongl4fh5ad.onion](http://blkhatjxlrvc5aevqzz5t6kxldayog6jlx5h7glnu44euzongl4fh5ad.onion/)
----

### Archived/Cached Pages 1/2
1. [archive.fo](https://archive.today)
2. [Chacedviews.com](https://chacedviews.com)
3. [CachedPages](https://cachedpages.com)
4. [Websitation.com](https://websitation.com)
5. [Internet Archive: Wayback Machine](https://archive.org/web/)
6. [https://megalodon.jp](https://megalodon.jp)
7. [https://archive.vn/](https://archive.vn/)
8. [https://swap.stanford.edu/](https://swap.stanford.edu/)
9. [Perma.cc](https://perma.cc/)
10. [https://oldweb.today](https://oldweb.today)
11. [https://webrecorder.net/tools](https://webrecorder.net/tools)
12. [https://replayweb.page/](https://replayweb.page/)
----

### Archive/Cache 2/2
1. [https://archive.org/web/](https://archive.org/web/)
2. [Internet Archive: Wayback Machine](https://archive.org/web/)
3. [https://archive.today/](https://archive.today/)
4. [archive.is](https://archive.is/)
5. [archive.li](https://archive.li/)
6. [https://www.nationalarchives.gov.uk/](https://www.nationalarchives.gov.uk/)
7. [http://www.archivesportaleurope.net/](http://www.archivesportaleurope.net/)
8. [https://www.webcitation.org](https://www.webcitation.org)
9. [https://webarchive.jira.com/wiki/home](https://webarchive.jira.com/wiki/home)
10. [http://webverse.archive.org/](http://webverse.archive.org/)
11. [https://cachedview.nl/](https://cachedview.nl/)
12. [Conifer](https://conifer.rhizome.org/)
13. [Video Vault 0.2](https://www.bravenewtech.org/)
----

### Social Media ( reddit, skype, twitter)
1. [reddit search](https://redditsearch.io/)
2. [Skypli.com](https://www.skypli.com/)
3. [Skypeipresolver.net](http://www.skypeipresolver.net/)
4. [http://spoonbill.io/](http://spoonbill.io/)
5. [Spponbill.io](https://spponbill.io)
6. [Karma Decay](http://karmadecay.com/)
7. [copy twitter lists](http://projects.noahliebman.net/listcopy/connect.php)
8. [https://shadowban.eu/](https://shadowban.eu/)
9. [https://unionmetrics.com/free-tools/twitter-snapshot-report/](https://unionmetrics.com/free-tools/twitter-snapshot-report/)
10. [Botometer by OSoMe](https://botometer.osome.iu.edu/)
11. [Reddit-user-analyser.netlify.app](https://reddit-user-analyser.netlify.app/)
12. [RedditMetis](https://redditmetis.com/)
13. [Removeddit](https://www.removeddit.com/)
----

### Steam Profile's
1. [https://steamid.io/](https://steamid.io/)
2. [Steamid.uk](https://steamid.uk/)
3. [Findsteamid.com](https://findsteamid.com)
4. [Steamid.xyz](https://steamid.xyz/)
5. [Steam Discovery](https://steamdiscovery.com)
6. [steamspy](https://steamspy.com/)
7. [Steam ID Finder](https://steamidfinder.com)
8. [Steam Database](https://steamdb.info/)
9. [Giveaways for Steam Games](https://www.steamgifts.com/)
10. [Rep.TF](https://rep.tf/)
11. [SteamRep » Home](https://steamrep.com/)
12. [Bazaar.tf](http://bazaar.t.com)
13. [rep.tf](http://f.com)
14. [F-o-g.trade](https://f-o-g.trade/)
15. [CSGO Exchange](http://csgo.exchange/)
16. [Steamid.uk](https://steamid.uk/)
17. [CSGOlounge](http://csgolounge.com/)
18. [Steamladder.com](https://steamladder.com/)
19. [backpack.tf](https://backpack.tf/)
20. [import.tax](https://steamid.io/)
21. [Steamid.eu.cutestat.com](https://steamid.eu.cutestat.com/)
22. [Steam Gauge](https://www.mysteamgauge.com/)
23. [Steamid.uk](https://steamid.uk/)
----

### Snapchat Tools
1. [Snap Map](https://map.snapchat.com)
2. [Story.snapchat.com](https://story.snapchat.com/)
3. [AddMeSnaps.com](https://www.addmesnaps.com/)
4. [Ghostdex.app](https://ghostdex.app/)
5. [snapsr.com/](https://snapsr.com/)
6. [Ghostcodes.com](https://www.ghostcodes.com/)
----

### Chrome Security Extensions
1. [WebRTC Network Limiter](https://chrome.google.com/webstore/detail/webrtc-network-limiter/npeicpdbkakmehahjeeohfdhnlpdklia)
2. [HTTPS Everywhere](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp)
3. [NoScript](https://chrome.google.com/webstore/detail/noscript/doojmbjmlfjjnbmnoijecmcbfeoakpjm)
4. [Privacy Badger](https://chrome.google.com/webstore/detail/privacy-badger/pkehgijcmpdhfbdbbnkijodmdjhbjlgp)
5. [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en-GB)
6. [Unshorten.link - Chrome Web Store](https://chrome.google.com/webstore/detail/unshortenlink/gbobdaaeaihkghbokihkofcbndhmbdpd?hl=en)
7. [DuckDuckGo Privacy Essentials](https://chrome.google.com/webstore/detail/duckduckgo-privacy-essent/bkdgflcldnnnapblkhphbgpggdiikppg?hl=en)
8. [Click&Clean](https://chrome.google.com/webstore/detail/clickclean/ghgabhipcejejjmhhchfonmamedcbeod?hl=en)
----

### Instagram (GitHub)
1. [https://github.com/althonos/InstaLooter](https://github.com/althonos/InstaLooter)
2. [https://github.com/akurtovic/InstaRaider](https://github.com/akurtovic/InstaRaider)
----

### Youtube (tools) / VIDEO
1. [Extract Meta Data](https://citizenevidence.amnestyusa.org/)
2. [https://unlistedvideos.com/](https://unlistedvideos.com/)
3. [https://citizenevidence.amnestyusa.org/](https://citizenevidence.amnestyusa.org/)
4. [watch frame by frame](http://www.watchframebyframe.com/)
5. [TubeOffline](https://www.tubeoffline.com/)
6. [https://keepvid.com/](https://keepvid.com/)
7. [Geo Search Tool](http://youtube.github.io/geo-search-tool/search.html)
----

### TikTok Tools
1. [TikTok Quick Search](https://www.osintcombine.com/tiktok-quick-search)
2. [TikTok Search Engine: Search and Watch TikToks](https://www.searchtiktoks.com)
3. [https://ttdownloader.com/](https://ttdownloader.com/)
4. [https://exportcomments.com/](https://exportcomments.com/)
----

### Twitter (tools)
1. [Twitter Advanced Search](https://twitter.com/search-advanced)
2. [https://tweetdeck.twitter.com/](https://tweetdeck.twitter.com/)
3. [TweeterID](https://tweeterid.com/)
4. [https://botometer.osome.iu.edu/](https://botometer.osome.iu.edu/)
5. [https://hoaxy.osome.iu.edu/](https://hoaxy.osome.iu.edu/)
6. [https://truthy.indiana.edu/tools/trends/](https://truthy.indiana.edu/tools/trends/)
7. [https://truthy.indiana.edu/tools/networks/](https://truthy.indiana.edu/tools/networks/)
8. [Socialbearing](https://socialbearing.com/)
9. [tinfoleak](https://tinfoleak.com/)
10. [Socialbearing](https://socialbearing.com/)
----

### Discord (tools)
1. [Search Discord Servers](https://disboard.org/search)
2. [Discord.ID](https://discord.id/)
3. [https://www.leaked.site/index.php?resolver%2Fdiscord.0%2F=](https://www.leaked.site/index.php?resolver%2Fdiscord.0%2F=)
4. [Serverse](https://extraction.team/serverse.html)
----

### Facebook (tools)
1. [StalkFace](https://stalkface.com/en/)
2. [Find my Facebook ID](https://findmyfbid.com/)
3. [https://whopostedwhat.com/](https://whopostedwhat.com/)
4. [https://exportcomments.com/](https://exportcomments.com/)
5. [Facebook Search](https://graph.tips/beta/)
----

### Instagram
1. [Searchusers.com](https://searchusers.com/)
2. [https://downloadgram.com/](https://downloadgram.com/)
3. [https://www.storysaver.net/](https://www.storysaver.net/)
4. [https://gramsave.com/](https://gramsave.com/)
5. [https://exportcomments.com/](https://exportcomments.com/)
----

### Twitter (GitHub)
1. [twintproject/twint](https://github.com/twintproject/twint)
----

### Facebook +
1. [Who posted what?](https://whopostedwhat.com/)
2. [Find my Facebook ID](https://findmyfbid.com/)
3. [Facebook Search](https://graph.tips/beta/)
4. [searchisback](https://searchisback.com/)
5. [Find my Facebook ID](https://findmyfbid.com/)
6. [What does Facebook publish about you and your friends?](http://zesty.ca/facebook/)
7. [https://exportcomments.com/](https://exportcomments.com/)
8. [https://exportcomments.com/](https://exportcomments.com/)
----

### Mozilla Firefox Osint/Sec extensions//verification
1. [RevEye Reverse Image Search – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/en-US/firefox/addon/reveye-ris/)
----

### Chrome Osint/Sec Verify extensions
1. [https://chrome.google.com/webstore/detail/exif-quickview/kjihpkahhpobojbdnknpelpgmcihnepj/related?hl=en-US](https://chrome.google.com/webstore/detail/exif-quickview/kjihpkahhpobojbdnknpelpgmcihnepj/related?hl=en-US)
2. [https://www.stupidproxy.com/ip-address-user-agent/](https://www.stupidproxy.com/ip-address-user-agent/)
----

### DataBases for Legal Professionals
1. [Martindale](https://www.martindale.com/)
2. [http://expertwitness.com/en/categories/all/All](http://expertwitness.com/en/categories/all/All)
----

### Telegram
1. [TelegramDB.org](https://telegramdb.org/)
2. [Search.buzz.im](https://search.buzz.im/)
3. [Telegram Search. Search for posts](https://tgstat.ru/en/search)
4. [Lyzem Blog](https://lyzem.com/)
----

### Other ....
1. [Clubhouse Database](https://clubhousedb.com/)
----

### Videos (onion)
1. [INVIDEIOUS](http://c7hqkpkpemu6e7emz5b4vyz7idjgdvgaaa3dyimmeojqbgpea3xqjoid.onion/feed/popular)
----

### torrent (onion)
1. [Piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion](http://piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion/)
----

### DarkNetRandom Chat
1. [http://notbumpz34bgbz4yfdigxvd6vzwtxc3zpt5imukgl6bvip2nikdmdaad.onion/rchat/](http://notbumpz34bgbz4yfdigxvd6vzwtxc3zpt5imukgl6bvip2nikdmdaad.onion/rchat/)
2. [http://pfpmd7dd5ijt4add2sfi4djsaij4u3ebvnwvyvuj6aeipe2f5llptkid.onion/ui3/](http://pfpmd7dd5ijt4add2sfi4djsaij4u3ebvnwvyvuj6aeipe2f5llptkid.onion/ui3/)
3. [jitjatj3qbb42jvik4udcehxpkoidppz3gojslh7jcatuo4hx4xwayid.onion/login.php](http://jitjatj3qbb42jvik4udcehxpkoidppz3gojslh7jcatuo4hx4xwayid.onion/login.php)
----

### bookmarks misc onion
1. [menu](http://cgjzkysxa4ru5rhrtr6rafckhexbisbtxwg2fg743cjumioysmirhdad.onion/bookmarks.html)
2. [http://skhz5s6z5dpjz3flxudesptuc53lsfykjjzk6mwxabssrgqwz6k7cyid.onion/all-tor-sites-in-hidden-wiki.html](http://skhz5s6z5dpjz3flxudesptuc53lsfykjjzk6mwxabssrgqwz6k7cyid.onion/all-tor-sites-in-hidden-wiki.html)
3. [Wiki Tor List](http://ub6bby4x2djuerj4i2cbxnzmef566zgb2f7x6iqnu6dzwznxw2tscmqd.onion/)
----

