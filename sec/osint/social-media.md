### General Searches
1. [European Union - Social networks](https://europa.eu/european-union/contact/social-networks_en)
2. [Socialfinder.app](https://socialfinder.app/)
3. [SMAT - Social Media Analysis Toolkit](https://www.smat-app.com/)
4. [Qresear.ch](https://qresear.ch/)
5. [Gofindwho.com](https://gofindwho.com/)
6. [Skylens](https://app.skylens.io/)
----

### Facebook
1. [Facebook Mutual Friends](https://www.facebook.com/browse/mutual_friends/?uid=USER_ID&node=USER_ID)
2. [Facebook Video Downloader Online](https://fbdown.net/)
3. [Ad Library](https://www.facebook.com/ads/library/)
4. [Facebook Groups, list of active Facebook groups in 2020](https://www.neargroups.com/facebook)
5. [Who posted what?](https://www.whopostedwhat.com/)
----

### LinkedIn
1. [XRAY Search](https://recruitin.net/)
----

### Skype
1. [Skype API Avatar](https://avatar.skype.com/v1/avatars/(userHere)/public?returnDefaultImage=false&size=l)
2. [CyberHub](https://cyber-hub.net/oldip.php)
3. [Skypeusernames](https://www.skypeusernames.com/)
4. [Find skype usernames free, find skype friends](https://www.findonlinecontacts.com/skype-usernames)
5. [Skypli.com](https://www.skypli.com/)
----

### Twitch
1. [SocialBlade](https://socialblade.com/)
2. [Twitch User Information](https://twitch-tools.rootonline.de/followerlist_viewer.php)
3. [Modlookup.3v.fi](https://modlookup.3v.fi/)
----

### WhatsApp
1. [Neargroups](https://www.neargroups.com/)
----

### Discord
1. [Guild Lookup | DiscordLookup](https://discordlookup.com/guild)
2. [Lookup Server | Discord Toolbox](https://distools.app/lookup/guild)
3. [Lookup User | Discord Toolbox](https://distools.app/lookup/user)
4. [Discord ID to Username](https://discord.id/)
5. [Discord Creation Date Check](https://hugo.moe/discord/discord-id-creation-date.html)
6. [DiscordHub User Search](https://discordhub.com/user/search)
7. [Discord Snowflake to Timestamp Converter](https://pixelatomy.com/snow-stamp/)
8. [Discord.name](https://discord.name/)
----

### Videos
1. [Save Subs - Subtitles](https://savesubs.com/)
----

### Forgot Password Pages
1. [Facebook - Forgot Password](https://www.facebook.com/login/identify/)
2. [Twitter - Forgot Password](https://twitter.com/account/begin_password_reset)
3. [Instagram - Forgot Password](https://www.instagram.com/accounts/password/reset/)
4. [Ebay - Forgot Password](https://www.ebay.com/signin/?otpFyp=1)
5. [PayPal - Forgot Password](https://www.paypal.com/authflow/password-recovery/)
6. [Apple - Recover Your Apple ID](https://iforgot.apple.com)
7. [Steam - Forgot Password](https://help.steampowered.com/en/wizard/HelpWithLoginInfo?issueid=406)
----

### Twitter
1. [Twitter Advanced Search](https://twitter.com/search-advanced)
2. [SocialBlade](https://socialblade.com/)
3. [Tweetdeck](https://tweetdeck.twitter.com/)
4. [TweeterID](https://tweeterid.com/)
5. [SoclaiBearing](https://socialbearing.com/)
6. [Followerwonk](https://followerwonk.com/)
7. [All My Tweets](https://www.allmytweets.net/connect/)
8. [Twitter Search](https://twitter.com/search/?q=twitter.com/(userHere))
9. [Tweet Topic Explorer](http://tweettopicexplorer.neoformix.com/#n=(userHere))
10. [FirstFollower](https://socialrank.com/firstfollower)
11. [The one million tweet map](https://onemilliontweetmap.com/)
12. [Trendsmap](https://www.trendsmap.com/#)
13. [Twitter Accounts Details](http://www.twitteraccountsdetails.com/)
14. [Tweet Sentiment Visualization App](https://www.csc2.ncsu.edu/faculty/healey/tweet_viz/tweet_app/)
15. [Foller.me](https://foller.me/)
16. [Tagboard](https://tagboard.com/)
17. [OmniSci Tweetmap](https://scl2-04-gpu03.mapd.com/)
18. [Spoonbill](https://spoonbill.io/)
19. [whotwi](https://en.whotwi.com/)
20. [twdown](https://twdown.net/)
21. [Tweet Mapper](https://keitharm.me/projects/tweet/)
22. [Export Twitter Data to PDF etc...](https://www.twdocs.com/)
23. [burrrd.](https://burrrd.com/)
24. [Botometer](https://botometer.iuni.iu.edu/#!/)
25. [Twitter Shadowban Test](https://shadowban.eu/)
26. [getdaytrends](https://getdaytrends.com/)
27. [whotwi](https://en.whotwi.com/)
----

### Flickr
1. [loc.alize.us](https://loc.alize.us)
----

### Tinder
1. [Tinder Profile](http://gotinder.com/@(usernameHere))
----

### 4chan
1. [4Archive - the biggest archive of 4chan discussions](https://4archive.org/)
2. [4chan search](https://4chansearch.com/)
3. [4chansearch.org](https://4chansearch.org/)
----

### TikTok
1. [TikTok Tag Search](https://www.tiktok.com/tag/(hashtagHere))
2. [TikTok Browser](https://tiktokapi.ga/)
3. [Playtik.com | Tik tok musically viewer on web](https://playtik.com/)
4. [tiktok-quick-search](https://www.osintcombine.com/tiktok-quick-search)
5. [TikTok Profile Analytics Tool - CloutMeter](https://cloutmeter.com/)
6. [download tiktok video](https://ttdown.org/)
7. [Download TikTok Videos Without Watermark For Free](https://snaptik.app/)
8. [Tiktok online viewer](https://urlebird.com/)
----

### Instagram
1. [SocialBlade](https://socialblade.com/)
2. [HypeAuditor](https://hypeauditor.com/)
3. [Instagram Locations](https://www.instagram.com/explore/locations/)
4. [Instagram Hashtag Map](https://displaypurposes.com/map)
5. [Instagram Hashtag Graph](https://displaypurposes.com/graph)
6. [Instagram editor and viewer](https://www.picuki.com/)
----

### Snapchat
1. [Snapchat Map](https://map.snapchat.com/)
2. [Snapchat Stories](https://story.snapchat.com/s/(userHere))
3. [Snapcode](https://app.snapchat.com/web/deeplink/snapcode?username=(userHere)&type=SVG&size=240)
4. [Snapdex](https://www.snapdex.com/)
5. [Search Snapchat Usernames](https://somesnapcode.com/)
6. [SoVIP](https://sovip.io/?pa=1&q=nameHere)
7. [AddMeSnaps](https://www.addmesnaps.com/)
8. [snapsr](http://snapsr.com/)
9. [Snapchat Multi-Viewer](https://www.osintcombine.com/snapchat-multi-viewer)
10. [Ghostcodes.com](https://www.ghostcodes.com/)
11. [Political Ads Library](https://www.snap.com/en-GB/political-ads)
----

### Telegram
1. [Telegram Lists](https://telegramchannels.me/)
2. [Telegram-group.com](https://www.telegram-group.com/en/)
3. [Telegram Channels List](https://tlgrm.eu/channels)
4. [Telegram - Google Custom Search](https://cse.google.com/cse?q=+&cx=006368593537057042503:efxu7xprihg)
5. [Telegram groups, list of active Telegram groups in 2020](https://www.neargroups.com/telegram)
6. [Telegram Search. Search for posts](https://tgstat.ru/en/search)
----

### YouTube
1. [Comment History](https://www.youtube.com/feed/history/comment_history)
2. [Unlisted Videos](http://unlistedvideos.com/)
3. [DigitalMethods](https://tools.digitalmethods.net/netvizz/youtube/)
4. [ddownr](https://ddownr.com/)
5. [SocialBlade](https://socialblade.com/)
6. [The YouTube Channel Crawler](http://channelcrawler.com/)
7. [YouTube Comment Scraper](http://ytcomments.klostermann.ca/)
8. [Youtube Related Videos Visualization](http://www.yasiv.com/youtube)
9. [downsub](http://downsub.com/)
10. [Geo Search Tool](http://youtube.github.io/geo-search-tool/search.html)
11. [YouTube Region Restriction Checker](http://polsy.org.uk/stuff/ytrestrict.cgi?ytid=)
12. [Amnestyusa](https://citizenevidence.amnestyusa.org/)
13. [HypeAuditor](https://hypeauditor.com/)
14. [watch frame by frame](http://www.watchframebyframe.com/)
15. [Intelx YouTube Metadata](https://intelx.io/tools?tab=youtube)
16. [YouTube Comment Finder](https://ytcomment.kmcat.uk/)
17. [Video Subtitles](http://video.google.com/timedtext?lang=en&v={video_id})
18. [tools.digitalmethods.net/netvizz/youtube](https://tools.digitalmethods.net/netvizz/youtube/)
19. [Youtube channel ID](https://commentpicker.com/youtube-channel-id.php)
20. [YouTube Metadata Bulk](https://mattw.io/youtube-metadata/bulk)
21. [YouTube Channel Geofind](https://mattw.io/youtube-geofind/)
22. [Hadzy.com](https://hadzy.com/)
----

### Reddit
1. [Reddit Investigator](http://www.redditinvestigator.com/)
2. [Reddit Domain Search](https://www.reddit.com/domain/(domainHere))
3. [Search Reddit Comments by User](https://redditcommentsearch.com/)
4. [Reddit User Analyser](https://atomiks.github.io/reddit-user-analyser/)
5. [Redective](https://www.redective.com/)
6. [RedditGifts](https://www.redditgifts.com/profiles/view/(userHere))
7. [Pushshift Reddit Search](https://redditsearch.io/)
8. [SnoopSnoo](https://snoopsnoo.com/)
9. [Related subreddits based on your comments](https://anvaka.github.io/sayit/?query=)
10. [Map of Reddit](https://anvaka.github.io/map-of-reddit/)
----

