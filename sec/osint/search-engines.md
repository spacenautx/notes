### Search engines - Archived pages
1. [Archive.is](http://archive.is)
2. [Archive.org](http://archive.org)
3. [Archive.org (downloader)](https://github.com/hartator/wayback-machine-downloader)
4. [AwareOnline - Web archive search tool](https://www.aware-online.com/osint-tools/web-archive-search-tool/)
5. [CachedView](https://cachedview.nl/)
6. [Cachedview (Google)](http://cachedview.com/)
7. [Cached Pages](http://www.cachedpages.com/)
8. [Screenshots.com](http://www.screenshots.com/)
----

### Search engines - Blogs
1. [Blog Search](https://www.blog-search.com/)
2. [Blogs Botw](https://blogs.botw.org/)
3. [Bloggernity.com](http://www.bloggernity.com/)
4. [Blogsearchengine.org](http://www.blogsearchengine.org/)
5. [Food Blog Search](http://foodblogsearch.com/)
6. [Searchblogspot.com](https://www.searchblogspot.com/)
7. [Topix](http://www.topix.com/search/article?q=)
8. [Twingly](https://www.twingly.com/search)
9. [WordPress.com Search](https://en.search.wordpress.com/)
----

### Search engines - Cryptocurrencies
1. [Addresswatcher.com](https://addresswatcher.com/)
2. [Aware Online - Cryptocurrency tools](https://www.aware-online.com/en/osint-tools/cryptocurrency-tools/)
3. [Bitcoin Address Lookup Checker and Alerts - BitcoinWhosWho](http://bitcoinwhoswho.com/)
4. [Blockchain.info](https://blockchain.info/)
5. [BitcoinWhoSWho.com](http://bitcoinwhoswho.com/)
6. [Crypto Scam Checker](https://fried.com/crypto-scam-checker)
7. [FSMA (BE) Black list of fraudulent currency traders](https://www.fsma.be/en/warnings/companies-operating-unlawfully-in-belgium?field_type_of_fraude_tid_i18n=10595&submit=Apply)
8. [LiveCoinWatch](https://www.livecoinwatch.com/)
9. [OXT.me](https://oxt.me/)
10. [WalletExplorer](https://www.walletexplorer.com/)
----

### Search engines - Darkweb
1. [Ahmia.fi](https://ahmia.fi/)
2. [Darkwebnews](https://darkwebnews.com/)
3. [D﻿ark.fai﻿l](https://dark.fail/)
4. [Deepweb - Best search engines for Darkweb](https://www.deepweb-sites.com/deep-web-search-engines/)
5. [Dreaddit (visit via Tor)](http://dreadditevelidot.onion/)
6. [FreshOnions (visit via Tor)](http://vps7nsnlz3n4ckiie5evi5oz2znes7p57gmrvundbmgat22luzd4z2id.onion/)
7. [Onion.link](http://onion.link/faq.html)
8. [OnionlandSearchengine](https://onionlandsearchengine.com/)
9. [Onions.se](http://onions.es/)
10. [Tor66 (visit via Tor)](http://tor66sezptuu2nta.onion/)
11. [Torscan](http://www.torscan.io/)
----

### Search engines - Documents
1. [FindPdfDoc](http://www.findpdfdoc.com/)
2. [FindPdfDoc](http://www.findpdfdoc.com/)
3. [FindPdfDoc](http://www.findpdfdoc.com/)
----

### Search egnines - Exploits/vulnerabilities
1. [Sploitus](https://sploitus.com)
2. [Vulmon](http://vulmon.com)
----

### Search engines - General
1. [100searchengines.com](https://www.100searchengines.com/)
2. [2Lingual](http://2lingual.com/)
3. [Bing](https://bing.start.me/?a=gsb_startme_00_00_ssg01)
4. [Bing vs. Google](http://bvsg.org)
5. [DuckDuckGo](http://duckduckgo.com)
6. [eTools.ch - multiple search engines at once](http://www.etools.ch/)
7. [Google.com](https://google.com)
8. [Google Scholar](https://scholar.google.com)
9. [Google Trends](http://trengs.google.ccom)
10. [I Search From](http://isearchfrom.com/)
11. [Inteltechniques multiple search engine](https://inteltechniques.com/osint/menu.search.html)
12. [IntelX.io](https://intelx.io/)
13. [Oscobo](http://oscobo.co.uk/)
14. [Qwant](http://qwant.com)
15. [SearchEngineColossus](http://searchenginecolossus.com/)
16. [Searx.me - multiple search engines at once](https://searx.me/)
17. [Startpage](https://www.startpage.com/)
18. [Swisscows](https://swisscows.com)
19. [Yamii (Arabic)](https://www.yamli.com/arabic-keyboard/)
20. [Yandex (RU)](http://yandex.ru)
----

### Search engines - Google Custom Search
1. [CSE Utopia](https://start.me/p/EL84Km/cse-utopia)
2. [GEOSINTsearch](https://cse.google.com/cse?cx=015328649639895072395:sbv3zyxzmji)
3. [Pasted tekst](https://cse.google.com/cse/publicurl?cx=013991603413798772546:nxs552dhq8k)
4. [Reddit](https://cse.google.com/cse/publicurl?cx=017261104271573007538:bbzhlah6n4o)
5. [Sprp77 Custom search engines](https://start.me/p/b5ynOQ/sprp77-search-engines)
----

### Search engines - Image search
1. [Baidu images search](http://image.baidu.com/)
2. [Bing Image Search](https://www.bing.com/?scope=images&nr=1&FORM=NOFORM)
3. [European Union Trademark search](https://euipo.europa.eu/eSearch/#advanced/trademarks)
4. [FindFace (RU)](https://findface.ru/)
5. [Flickr](https://secure.flickr.com/)
6. [Fotoforensics](http://fotoforensics.com/)
7. [Google Images](https://www.google.com/imghp)
8. [Icon finder](https://besticon.herokuapp.com/)
9. [Image Identify Project](https://www.imageidentify.com/)
10. [Imgops](http://imgops.com/)
11. [IntelTechniques - Image](https://inteltechniques.com/osint/reverse.image.html)
12. [Jeffreys Exif search engine](http://exif.regex.info/exif.cgi)
13. [PicSearch](http://www.picsearch.com/)
14. [PimEyes](https://pimeyes.com/en/)
15. [Reverse-Image-Search](http://www.reverse-image-search.org/)
16. [Rootabout](http://rootabout.com/)
17. [StolenCameraFinder](http://www.stolencamerafinder.co.uk/)
18. [ThoughtfulDev/EagleEye](https://github.com/ThoughtfulDev/EagleEye)
19. [TinEye Reverse Image Search](https://tineye.com)
20. [Trademarkts (USA)](https://trademarks.justia.com/search?q=)
21. [Yandex Image Search](https://yandex.com/images/)
----

### Search engines - IoT, cameras, 3Dprinting
1. [123cam](http://www.123webcam.com/)
2. [Airport webcams](http://airportwebcams.net/)
3. [Earthcam](http://www.earthcam.com/)
4. [Insecam](http://www.insecam.org/)
5. [Lookr](https://www.lookr.com/)
6. [NextBase](https://www.nextbase.co.uk/video-map/)
7. [Openstreetcam](https://www.openstreetcam.org/map/)
8. [Opentopia](http://www.opentopia.com/)
9. [Pictimo](https://www.pictimo.com/)
10. [Reolink - list of unsecured cameras](https://reolink.com/unsecured-ip-camera-list/)
11. [Shodan.com](https://shodan.io)
12. [The webcam network](http://www.the-webcam-network.com/)
13. [Thingful](https://www.thingful.net/)
14. [Thingiverse](https://www.thingiverse.com/)
15. [Webcam.nl (NL)](https://webcam.nl/live_streaming/)
16. [Webcams.travel](https://www.webcams.travel/)
17. [Worldcam](https://worldcam.eu/)
----

### Search engines - IRC
1. [General - Kiwiirc](https://kiwiirc.com/search)
2. [General - Mibbit](http://mibbit.com)
3. [General - Netsplit](http://irc.netsplit.de/channels/)
4. [ICQ - People search](http://icq.com/people)
5. [Skype - Websolver](https://webresolver.nl/)
----

### Search engines - Maps
1. [Aerophotostock (NL)](https://www.aerophotostock.com/)
2. [Airmap.io](https://app.airmap.io/)
3. [Antennebureau (NL)](http://www.antenneregister.nl/Html5Viewer_Antenneregister/Index.html?viewer=antenneregister)
4. [APlaceBetween.Us](http://a.placebetween.us/)
5. [Bing Maps](https://www.bing.com/maps)
6. [CtrlQ - Find postal addresses](https://ctrlq.org/maps/address/)
7. [DigitalGlobe (paid)](https://www.digitalglobe.com/)
8. [Dualmaps.com](http://dualmaps.com/)
9. [Echosec (paid)](https://www.echosec.net)
10. [Europe live map refugees/terrorism](http://europe.liveuamap.com)
11. [Facebook Live Map](http://facebook.com/live)
12. [Geograph (photos)](https://www.geograph.org/)
13. [GeoGratis (CAN)](http://geogratis.cgdi.gc.ca/)
14. [GeoLocateThis](http://geolocatethis.site/)
15. [Geonames](http://www.geonames.org/)
16. [GEOSINTsearch](https://cse.google.com/cse?cx=015328649639895072395:sbv3zyxzmji)
17. [Google Earth (download link)](https://www.google.com/earth/download/gep/agree.html)
18. [Google Maps](https://maps.google.com)
19. [GPSies](https://www.gpsies.com/trackList.do)
20. [Hate Map](https://www.splcenter.org/hate-map)
21. [Hectometerpaal (NL)](http://www.hmpaal.nl/)
22. [Hoogspanningsnet (NL) (power pylons)](https://webkaart.hoogspanningsnet.com/index2.php#7/52.000/5.000)
23. [Imgeris (NL)](http://www.imergis.nl/asp/44.asp)
24. [Instant Google Street View](https://www.instantstreetview.com/)
25. [Islamic State map](http://umap.openstreetmap.fr/en/map/islamic-state-claimed-provinces-map_29647#7/33.409/42.902)
26. [Kadaster - Bagviewer (NL)](https://bagviewer.kadaster.nl/lvbag/bag-viewer/index.html)
27. [Livingatlas - Wayback](https://livingatlas.arcgis.com/wayback/)
28. [Mapchecking](https://www.mapchecking.com)
29. [Mappillary](https://www.mapillary.com)
30. [NewspaperMap](https://newspapermap.com/)
31. [OpenSeaMap](http://www.openseamap.org/index.php?id=openseamap&L=1)
32. [Snapchat Map](https://map.snapchat.com/)
33. [Strava Heatmap](https://labs.strava.com/heatmap)
34. [Strava search engine](https://www.doogal.co.uk/strava.php)
35. [Universal Postal Union](http://www.upu.int/en/the-upu/member-countries.html)
36. [WarWire (monitor wars/crissis (paid))](https://www.warwire.net/)
37. [Whats3words](https://what3words.com/)
38. [WhatsHalfway](http://www.whatshalfway.com/)
39. [Wikimapia](https://www.google.com/earth/download/gep/agree.html)
40. [World Aeronautical Database](http://worldaerodata.com)
41. [Yandex maps](https://n.maps.yandex.ru/)
42. [Zoom.earth](https://zoom.earth)
----

### Search engines - Online marketplaces
1. [2ehands.be (BE)](http://2ehands.be)
2. [AdvertentieZoeker (NL)](http://www.advertentiezoeker.nl/)
3. [Craigslist](https://www.craigslist.org/about/sites)
4. [Craigslist - AllCraigsListSearch](https://allcraigslistsearch.com/)
5. [Craigslist - Craigslist Search Engine](https://craigs-list-search.com/)
6. [Craigslist - Zoomthelist](http://zoomthelist.com/)
7. [eBay](https://www.ebay.com/)
8. [Gumtree](https://www.gumtree.com/)
9. [Kijiji (CAN)](https://www.kijiji.ca/)
10. [Markt.de (DE)](https://www.markt.de/)
11. [Marktplaats (NL)](http://marktplaats.nl)
12. [Speurders (NL)](http://speurders.nl)
13. [Quoka.de (DE)](http://quoka.de)
----

### Search engines - Pastebin
1. [PasteLert](https://andrewmohawk.com/pasteLert/)
2. [Psbdmp](https://psbdmp.ws/)
----

### Search engines - Presentations/Slides
1. [Slide Search Engine](http://www.slidesearchengine.com/)
----

### Search engines - Realestate
1. [Blockshopper (USA)](https://blockshopper.com/)
2. [Funda (NL](http://funda.nl)
3. [Homemetry (USA)](https://homemetry.com/)
4. [iLocate (NL)](https://www.ilocate.nl/)
5. [Jaap (NL)](http://jaap.nl)
6. [Realtor (USA)](https://www.realtor.com/)
7. [Rehold (USA)](https://rehold.com/)
8. [Zoopla (UK)](http://zoopla.co.uk)
----

### Search engines - Similiar sites
1. [Similarsites.com](https://similarsites.com)
2. [SimilarSiteSearch.com](http://www.similarsitesearch.com/)
3. [TopSimilarSites](http://www.topsimilarsites.com/)
----

### Search engines - Social media
1. [Facebook - A to Z](https://www.facebook.com/directory/people/A)
2. [Facebook - Compare profiles](https://pitoolbox.com.au/new-facebook-tool/)
3. [Facebook - Facebook tool](http://pitoolbox.com.au/facebook-tool/)
4. [Facebook - FBNinja](http://fbninja.byethost13.com/?i=2)
5. [Facebook - Look up ID](http://lookup-id.com/)
6. [Facebook - Netbootcamp](http://netbootcamp.org/facebook.html)
7. [Facebook - OSINT Combine](https://www.osintcombine.com/facebook-intersect-search-tool)
8. [Facebook - PeopleFind](http://peoplfindthor.dk)
9. [Facebook - Plessas Graph Search](https://docs.google.com/spreadsheets/d/15dD0qSCDYDwVtWKC9zfZk1j8RzmvZARXTu9hrM34DnU/edit#gid=0)
10. [Facebook - Profileengine.com](http://profileengine.com/)
11. [Facebook - ResearchClinic](http://www.researchclinic.net/facebook/)
12. [Facebook - Search is back](https://searchisback.com/)
13. [Facebook - Stalkscan](http://stalkscan.com/)
14. [Facebook - WhoPostedWhat](http://whopostedwhat.com)
15. [General - Community search](https://inteltechniques.com/osint/communities.html)
16. [General - Boardreader (fora)](http://boardreader.com/)
17. [General - Content posted by the European Union](https://europa.eu/european-union/contact/social-networks_en)
18. [General - Hashatit.com](https://www.hashatit.com/)
19. [General - Social-searcher](https://www.social-searcher.com)
20. [Google - Groups](https://groups.google.com/forum/#!overview)
21. [Instagram - Explore locations](https://www.instagram.com/explore/locations/)
22. [Instagram - Gramrix](https://gramrix.com)
23. [Instagram - Gramvideos](http://gramvideos.com/)
24. [Instagram - Instidy](http://instidy.com)
25. [Instagram - Picbear](http://picbear.com)
26. [Instagram - Pictame](http://www.pictame.com)
27. [Instagram - Search old photos tagged to locations](https://www.osintcombine.com/instagram-explorer)
28. [Instagram - Seachtmybio](https://www.searchmy.bio/)
29. [Instagram - Someture](http://someture.com/)
30. [Instagram - StatFlux](http://statflux.com/)
31. [Instagram - Vibbi](http://vibbi.com/viewer)
32. [Instagram - Yooying](http://yooying.com)
33. [Keybase.io](http://keybase.io)
34. [LinkedIn - Findera](https://findera.com/)
35. [LinkedIn - Skimleads](https://skimleads.com/all/emails/leads.html)
36. [Pinterest - PinGroupie](http://pingroupie.com/)
37. [Reddit - Archive](http://www.redditarchive.com/)
38. [Reddit - Ceddit](http://ceddit.com)
39. [Reddit - GCS](https://cse.google.com/cse/publicurl?cx=017261104271573007538:bbzhlah6n4o)
40. [Reddit - Metareddit](http://metareddit.com/)
41. [Reddit - Karma Decay (image search)](http://karmadecay.com/)
42. [Reddit - Metareddit](http://metareddit.com/)
43. [Reddit  - Metrics](http://redditmetrics.com/)
44. [Reddit - Mostly Harmless](http://kerrick.github.io/Mostly-Harmless/#features)
45. [Reddit - RedditInsight](http://www.redditinsight.com/)
46. [Reddit - Redditinvestigator.com](https://redditinvestigator.com)
47. [Reddit - Redditsearch.io](http://redditsearch.io/)
48. [Reddit - Redective.com](http://Redective.com/)
49. [Reddit - Resavr](https://www.resavr.com/)
50. [Reddit - Snoopsnoo](https://snoopsnoo.com/)
51. [Snapchat - FindMySnap](http://findmysnap.com)
52. [Snapchat  - Map](https://map.snapchat.com/)
53. [Snapchat - Snapdex](https://www.snapdex.com)
54. [Snapchat - Somesnaps](http://somesnaps.com/)
55. [Telegram - CSE](https://cse.google.com/cse?&cx=006368593537057042503:efxu7xprihg)
56. [Telegram - Tchannel](https://tchannels.me/)
57. [Telegram - Telegago](https://cse.google.com/cse?&cx=006368593537057042503:efxu7xprihg)
58. [Telegram - TSearch](http://tsear.ch)
59. [Tumblr - Tumblr originals](https://studiomoh.com/fun/tumblr_originals/)
60. [Twitter - Followerwonk](https://followerwonk.com/bio/)
61. [Twitter - Gigatweeter](http://gigatweeter.com/)
62. [Twitter - Search](https://twitter.com/search-home)
63. [Yahoo - Groups](https://groups.yahoo.com/neo/search?)
----

### Search engines - Source code
1. [Grepcode](http://grepcode.com/)
2. [NerdyData](https://nerdydata.com/search)
3. [PublicWWW](https://publicwww.com/)
----

### Search engines - Vacancies/jobs/recruitment
1. [General - Collection of search tools by Stefanie Proto](https://docs.google.com/spreadsheets/d/1cAeOLwsM65pDJus5xIaZf-yfSEh1g6-59yzc00TyR9Y/edit?usp=sharing)
----

### Search engines - WiFi & internet connectivity
1. [CheckMacAddresses](http://techzoom.net/tools/CheckMacAddress)
2. [Find Wi-Fi | Mylnikov GEO](https://find-wifi.mylnikov.org/)
3. [Hotspotsvinden (NL)](http://www.hotspotsvinden.nl)
4. [Macvendorloopup](https://www.macvendorlookup.com/)
5. [Wigle](https://wigle.net/)
----

### Search engines - YouTube/Periscope/videos
1. [Bing video search](https://www.bing.com/?scope=video&nr=1)
2. [Periscope - PeriSearch](https://www.perisearch.xyz/)
3. [Periscope - OnPeriscope](http://onperiscope.com/)
4. [YouTube  - Channel Crawler](http://channelcrawler.com/)
5. [YouTube - Geo Search Tool](http://youtube.github.io/geo-search-tool/search.html)
6. [YouTube - Related YouTube videos](http://yasiv.com/youtube)
7. [YouTube - WordHunt](https://www.wordhunt.xyz/)
8. [YouTube - YouTube Dataviewer](https://citizenevidence.amnestyusa.org/)
----

