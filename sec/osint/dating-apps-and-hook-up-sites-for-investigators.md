### Connect
1. [Frenchdetective.com](http://www.frenchdetective.com)
2. [https://www.linkedin.com/in/frenchinvestigator/](https://www.linkedin.com/in/frenchinvestigator/)
3. [https://twitter.com/frenchPI](https://twitter.com/frenchPI)
----

### LGBTQ+
1. [Blued (Largest gay app in the world. China but now International)](https://www.blued.com/en/index.html)
2. [Chappy (choice of casual or relationship) FB needed.](https://chappyapp.com/)
3. [Gaydar](https://www.gaydar.net/register)
4. [Grindr.com (brutal meat market. By far the most popular gay app)](https://www.grindr.com)
5. [GROWLr: The Gay Bear Social Network](http://www.growlrapp.com/)
6. [Her (mostly lesbian)](https://weareher.com/)
7. [Hornet, the Premier Gay Social Network.](https://hornetapp.com/login)
8. [Jack'd (mostly people of color looking for other POC)](https://www.jackd.mobi/)
9. [SCRUFF - Gay Guys Worldwide - SCRUFF](https://www.scruff.com/)
10. [Lex](https://thisislex.app/)
----

### Poly / Swolly / Swingers
1. [#Open (Queer, non-binary, poly)](https://hashtagopen.com/)
2. [3Fun](https://play.google.com/store/apps/details?id=com.threesome.swingers.app.threefun&hl=en_US)
3. [Adult FriendFinder - The World's Largest Adult Dating and Hookup Site](https://adultfriendfinder.com/go/page/cover?pid=p634078.subkey000_adult_friend_finder_EM_12&ip=auto&no_click=1&alpo_redirect=1&nats=s0ebzCrOO--pcrid--209799256943--pmt--e--pkw--adult%20friend%20finder--id--0ebzCrOO--)
4. [Bender - The gay dating app with video messaging](http://www.benderapp.com/)
5. [Feeld: Alternative Dating app for couples & singles #threesome](https://feeld.co/)
6. [Local Dating /poly groups Meetups - Meetup](https://www.meetup.com/topics/dating-and-relationships/)
7. [Polyamory](https://www.facebook.com/pages/Polyamory/109440212409219)
8. [polymatchmaker.com](http://www.polymatchmaker.com/)
9. [Reddit Polyamory r4r](https://www.reddit.com/r/polyamoryR4R)
10. [Swindr for SLS Couples & SLS Singles](https://www.swindr.net/)
11. [Swingers Lifestyle Community](http://www.swinglifestyle.com)
----

### BDSM / Fetish / Kinky
1. [ALT: Erotic BDSM, Bondage & Fetish Sex Dating for Masters, Mistresses](https://alt.com/)
2. [BDSM](http://bdsm.com)
3. [BDSM Singles Looking For Fun - BDSMSINGLES.com](https://www.bdsmsingles.com)
4. [Collarspace.com](https://www.collarspace.com/)
5. [Facebook.com/groups/1628468610499173](https://www.facebook.com/groups/1628468610499173/)
6. [Fetish.com](https://www.fetish.com/)
7. [Fetlife: BDSM, Fetish & Kinky](https://fetlife.com/)
8. [Fetster.com](https://www.fetster.com/)
9. [Findafemdom.com](https://findafemdom.com)
10. [Fuck.com](https://www.fuck.com/)
11. [‎KinkD](https://www.kinkdapp.com/)
12. [Knkiapp.com](http://www.knkiapp.com/)
13. [Whiplr.com](https://whiplr.com/)
----

### Extra marital
1. [Ashley Madison](https://www.ashleymadison.com/?lang=en_US&c=1&utm_source=google&utm_medium=cpc&utm_term=%252Bhook%20%252Bup%20%252Bapp&utm_content=b&utm_campaign=Ashley+Madison+-+US+-+Casual+Sex&gclid=Cj0KCQjw09zOBRCqARIsAH8XF1b3BpRCzi82zIuWl8muIh960YvK_jWuCP4lfupFxtywR8FeVrx9ZYgaAv_dEALw_wcB)
2. [No Strings Attached: Discreet Sex, Married Affairs & Dating](https://nostringsattached.com/)
3. [AffairD](https://www.affaird.com/)
----

### Arrangement dating / Sugar Daddy search
1. [Seeking Arrangement (most popular SD site by far)](https://www.seekingarrangement.com/)
2. [SecretBenefits.com: Sugar Daddy Dating (accepts Bitcoins)](https://www.secretbenefits.com/)
3. [Sugar Daddy Meet](https://www.sugardaddymeet.com/)
4. [Sugar D: with beauty-verification service.](http://sugard.co/)
5. [Sudy](https://www.sudyapp.com/#/)
6. [Sudy Cougar (Android) Sugar Momma Dating & Hookup](https://sudyapp.com/blog/sudy-cougar-steadily-climbs-app-store-social-networking-app-rankings/)
7. [Sudy Gay: Gay Sugar Daddy Dating & Hookup](https://sudyapp.com/blog/sudy-gay-app/)
8. [Les Sudy: Sugar Mommy dating and hook up for lesbians (Android)](https://play.google.com/store/apps/details?id=com.sudy.les)
9. [Arrangement.com](http://www.arrangement.com/)
10. [whatsyourprice.com (bid on per date price)](http://whatsyourprice.com)
11. [Miss Travel (sponsored trips in exchange for sex)](https://www.misstravel.com/)
12. [Oh La La - paid on-demand escort/ dating app in NYC](https://www.ohlala.com/en)
----

### Escorting
1. [cowboys 4 angels: Male escorts for women](https://www.cowboys4angels.com)
2. [Eros.Com: Eros Guide Escort directory](https://www.eros.com/)
3. [Escort Ads](https://www.escort-ads.com/)
4. [Escort Index: Repository by region](https://escortindex.com/)
5. [Escortfish.ch: Organized by region](https://escortfish.ch/)
6. [Rendevu - Uber for escorts](https://rd.vu/)
7. [Skip the games](http://skipthegames.com/)
8. [Slixa](https://www.slixa.com)
9. [Southern GFE](https://www.southerngfe.com/)
10. [Suger Nights](http://sugarnights.com/)
----

### Reddit Dating Subs
1. [/r/Affairs](https://www.reddit.com/r/Affairs/)
2. [/r/cuddlebuddies/](https://www.reddit.com/r/cuddlebuddies/)
3. [/r/ForeverAloneDating/](https://www.reddit.com/r/ForeverAloneDating/)
4. [/r/hardshipmates/](https://www.reddit.com/r/hardshipmates/)
5. [/r/hookup/](https://www.reddit.com/r/hookup/)
6. [/r/InternetFriends/](https://www.reddit.com/r/InternetFriends/)
7. [/r/Kikpals/](https://www.reddit.com/r/Kikpals/)
8. [/r/LetsChat/](https://www.reddit.com/r/LetsChat/)
9. [/r/MakeNewFriendsHere/](https://www.reddit.com/r/MakeNewFriendsHere/)
10. [/r/NSFWskype/](https://www.reddit.com/r/NSFWskype/)
11. [/r/nycr4r/ New York City reddit personals](https://www.reddit.com/r/nycr4r/)
12. [/r/olderwomen Who do you check out at the mall](https://www.reddit.com/r/olderwomen/)
13. [/r/penpals/](https://www.reddit.com/r/penpals/)
14. [/r/polyamoryR4R/](https://www.reddit.com/r/polyamoryR4R/)
15. [/r/r4r/ Reddit Personals - 300,000+ - Activity Partners, Groups, Dating, Hanging Out, Soulmates, FWBs](https://www.reddit.com/r/r4r/)
16. [/r/R4R30Plus/ For Redditors over 30](https://www.reddit.com/r/R4R30Plus/)
17. [/r/Random_Acts_Of_Sex/](https://www.reddit.com/r/Random_Acts_Of_Sex/)
18. [/r/RandomActsOfBlowJob/](https://www.reddit.com/r/RandomActsOfBlowJob/)
19. [/r/RandomActsOfMakingOut/](https://www.reddit.com/r/RandomActsOfMakingOut/)
20. [/r/RandomActsOfMuffDive/](https://www.reddit.com/r/RandomActsOfMuffDive/)
21. [/r/SnapchatSingles/](https://www.reddit.com/r/SnapchatSingles/)
22. [ISO kinksters?](https://www.reddit.com/r/BDSMpersonals/)
23. [r/GamerPals](https://www.reddit.com/r/GamerPals/)
24. [r/naughtyfromneglect](https://www.reddit.com/r/naughtyfromneglect/)
25. [r/NeedAFriend](https://www.reddit.com/r/Needafriend/)
26. [r/OnlineAffairs](https://www.reddit.com/r/OnlineAffairs/)
27. [r/RedditRoommates](https://www.reddit.com/r/redditroommates/)
28. [r/Sex](https://www.reddit.com/r/sex/)
29. [TOOL: Tricks to search Reddit](https://www.makeuseof.com/tag/right-way-search-reddit/)
----

### OK Cupid
1. [OkCupid Dating on the App Store](https://itunes.apple.com/app/apple-store/id338701294?mt=8)
2. [OkCupid Dating - Android Apps on Google Play](https://play.google.com/store/apps/details?id=com.okcupid.okcupid&referrer=utm_source%253Ddesktophome%2526utm_medium%253Dweb)
3. [OkCupid](https://www.okcupid.com/)
4. [OkCupid Reddit](https://www.reddit.com/r/OkCupid/)
5. [Using OkCupid's Search Filters - YouTube](https://www.youtube.com/watch?v=Qboq-I4PvGc)
6. [OKCupid Scams: All About Catfish Scams on OKCupid.com](https://socialcatfish.com/blog/okcupid-scams-everything-about-being-catfished-scams-on-okcupid-com/)
----

### POF (Plenty Of FIsh)
1. [POF Dating on the App Store](https://itunes.apple.com/app/apple-store/id389638243?mt=8)
2. [POF Free Dating App - Android Apps on Google Play](https://play.google.com/store/apps/details?id=com.pof.android&feature=search_result&hl=en&referrer=utm_source%253Dpofweb_link_1%2526utm_medium%253DCampaignID=0)
3. [Spokeo (searches POF, Match.com... by e-mail address)](https://www.spokeo.com/)
4. [POF advanced search](http://www.pof.com/advancedsearch.aspx)
5. [POF.com username search](http://www.pof.com/basicusersearch.aspx)
6. [Reddit Plenty Of Fish](https://www.reddit.com/r/POF/)
7. [A POF search string that you can examine and customize](http://www.pof.com/advancedsearch.aspx?iama=f&seekinga=m&minage=47&maxage=49&searchtype=&interests=giants&country=1&state=40&City=West+New+York&miles=5&cmdSearch=Begin+Interest+Search&Profession=)
----

### Tinder
1. [fbessez (fabien bessez) · GitHub Tinder API](https://github.com/fbessez)
2. [I asked Tinder for my data. It sent me 800 pages of my deepest, darkest secrets](https://www.theguardian.com/technology/2017/sep/26/tinder-personal-data-dating-app-messages-hacked-sold)
3. [IntelTechniques | Covertly Investigating Tinder Profiles (scrubbed archived version)](http://archive.is/oJzq7)
4. [Investigating Mobile Dating Applications in the Tinder Age - Magnet Forensics Inc.](https://www.magnetforensics.com/mobile-forensics/investigating-mobile-dating-applications-with-ief/)
5. [Investigating sexual crime in the Tinder age](https://www.youtube.com/watch?v=m05Btz-hsQk)
6. [Itunes.apple.com/us/app/tinder/id547702041](https://itunes.apple.com/us/app/tinder/id547702041?mt=8)
7. [Look by username - replace @username (in this case, Mark)](https://www.gotinder.com/@username)
8. [OSINT: Advanced Tinder capture (by @baywolf88)](https://www.learnallthethings.net/osmosis)
9. [Reddit Tinder](https://www.reddit.com/r/Tinder/)
10. [Search within POF by username with Yandex,com](https://yandex.com/search/?text=site%3Apof.com%20ginger&lr=21411)
11. [Swipehelper.com](https://www.swipehelper.com/)
12. [Techcrunch coverage of Tinder / Match Group](https://techcrunch.com/tag/match-group/)
13. [Tinder (Android) APK (old versions)](https://tinder.en.uptodown.com/android/old)
14. [Tinder Desktop](https://www.gotinder.com)
15. [Tinder for Android](https://play.google.com/store/apps/details?id=com.tinder&hl=en)
16. [Tinder Scams: Know if You’re Being Catfished on Tinder](https://socialcatfish.com/blog/catfished-tinder-need-know-tinder-scams/)
17. [Tinder Scraper: A Tinder Bot for Data Scientist's built on Node.js](http://cruzwelborn.com/tinder-scraper/)
18. [Tinder Select dating app for celebs and VIPs: REVIEW, WALKTHROUGH - Business Insider](http://www.businessinsider.com/tinder-select-for-celebs-and-other-vip-users-review-screenshots-walkthrough-2017-6)
19. [Track Down a Tinder Profile on desktop](https://null-byte.wonderhowto.com/how-to/track-down-tinder-profile-with-location-spoofing-google-chrome-0182905/)
----

### Grindr
1. [Grindr - Gay chat on the App Store](https://itunes.apple.com/us/app/grindr-gay-chat/id319881193?mt=8)
2. [Grindr - Gay chat, meet & date - Android Apps on Google Play](https://play.google.com/store/apps/details?id=com.grindrapp.android&hl=en)
3. [Grindr Reddit](https://www.reddit.com/r/grindr/)
4. [tomlandia (Thomas Schneider) Tinder API research / Fuckr](https://github.com/tomlandia)
5. [Gaymoji: A New Language for That Search](https://www.nytimes.com/2017/03/14/fashion/grindr-gay-emoji-gaymoji-digital.html?_r=0)
6. [Finding a user's location on Grindr the simple way](http://vervainglobal.com/articles/triangulation.html)
7. [/r/AlienExchange/](https://www.reddit.com/r/AlienExchange/)
----

### Happn
1. [happn — Dating app on the App Store](https://itunes.apple.com/us/app/happn-dating-app/id489185828?mt=8)
2. [happn – Local dating app - Android Apps on Google Play](https://play.google.com/store/apps/details?id=com.ftw_and_co.happn&hl=en)
3. [Happn.com](https://www.happn.com)
4. [Hacking Happn](https://www.sourcecon.com/hacking-will-happn-sourcing-online-proximity-dating-apps-by-ngsesq/)
5. [https://www.reddit.com/r/happn/](https://www.reddit.com/r/happn/)
----

### Hinge (now without Facebook login requirement)
1. [Hinge](https://hinge.co/)
2. [Hinge's Most Eligible](http://mosteligible.hinge.co/)
3. [Tech Crunch coverage of Hinge](https://techcrunch.com/tag/hinge/)
4. [Reddit HingeApp](https://www.reddit.com/r/hingeapp/)
----

### Companion apps to dating apps ANDROID
1. [Packet Capture - Android Apps on Google Play](https://play.google.com/store/apps/details?id=app.greyshirts.sslcapture&hl=en)
2. [GPS Emulator for Android](https://play.google.com/store/apps/details?id=com.rosteam.gpsemulator&hl=en_US&gl=US)
----

### Dating sites resources
1. [Comparison of online dating websites - Wikipedia](https://en.wikipedia.org/wiki/Comparison_of_online_dating_websites)
2. [Dating News: Apps and sites](https://www.datingnews.com/apps-and-sites/)
3. [Dating Scout](https://www.datingscout.com/)
4. [Online Dating | Pew Research Center](http://www.pewresearch.org/topics/online-dating/)
5. [Online Dating reviewed by Consumer Report Dec. 2016: Match Me If You Can](https://www.consumerreports.org/dating-relationships/online-dating-guide-match-me-if-you-can/)
6. [Reddit Dating Advice & apps reviews](https://www.reddit.com/r/dating_advice/)
7. [Reddit Online Dating](https://www.reddit.com/r/OnlineDating/)
8. [Romance Scams: a website dedicated to protecting online daters from scammers,](http://RomanceScams.org)
9. [Statistics: Online dating](https://www.statista.com/topics/2158/online-dating/)
10. [Techcrunch Dating Apps news](https://techcrunch.com/tag/dating/)
11. [Vice MOtherboard - Online Dating](https://motherboard.vice.com/en_us/topic/online-dating)
12. [Most Popular dating & networking apps (APPlyzer data)](https://www.applyzer.com/?mmenu=worldcharts)
----

