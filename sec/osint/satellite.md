### Satellite services
1. [Sentinel Hub EO Browser](https://apps.sentinel-hub.com/eo-browser/?zoom=10&lat=41.57025&lng=12.0623&themeId=DEFAULT-THEME)
2. [Sentinel Playground | Sentinel Hub](https://apps.sentinel-hub.com/sentinel-playground/?source=S2&lat=40.4&lng=-3.73&zoom=12&preset=1-NATURAL-COLOR&layers=B01,B02,B03&maxcc=20&gain=1.0&gamma=1.0&time=2020-02-01%7C2020-08-19&atmFilter=&showDates=false)
3. [Planet Explorer](https://www.planet.com/explorer/#)
4. [DigitalGlobe](https://discover.digitalglobe.com/)
5. [Google Earth](https://earth.google.com/intl/earth/download/ge/agree.html)
6. [Google Earth (3D)](https://earth.google.com/web/@0,0,0a,22251752.77375655d,35y,0h,0t,0r)
7. [old Terraserver (paid)](https://www.precisionhawk.com/satellite)
----

### Search & monitor satellite imagery
1. [Image Hunter (search & monitor)](https://imagehunter.apollomapping.com/)
2. [Soar (search)](https://soar.earth/satellites/landsat?)
3. [EarthExplorer (search)](https://earthexplorer.usgs.gov/)
4. [Zoom Earth (search)](https://zoom.earth/)
5. [EOS (search)](https://eos.com/landviewer/?lat=37.15290&lng=36.06550&z=15&id=S2A_tile_20190521_37SBB_1&b=Red,Green,Blue&anti)
6. [OpenAerialMap (search)](https://openaerialmap.org/)
7. [🗺Compare Satellite Imagery 🛰](https://mc.bbbike.org/mc/#)
8. [Geoseer.net](https://www.geoseer.net/)
----

### Livemaps
1. [Myanmar.liveuamap.com](https://myanmar.liveuamap.com/)
2. [Snap Map](https://map.snapchat.com/@21.493878,38.705267,5.72z)
3. [The one million tweet map](https://onemilliontweetmap.com/)
----

### Map Services
1. [Map Switcher](https://chrome.google.com/webstore/detail/map-switcher/fanpjcbgdinjeknjikpfnldfpnnpkelb)
2. [Google Maps](https://www.google.de/maps/@50.9251688,6.9604035,15z?hl=de)
3. [Wikimapia](http://wikimapia.org/)
4. [Mapillary](https://www.mapillary.com/)
5. [Geofind (Youtube)](https://mattw.io/youtube-geofind/location)
6. [KartaView (OpenStreetCam)](https://kartaview.org/map/@52.52712376381652,13.330353626769352,19z)
7. [OpenStreetMap](http://www.openstreetmap.org/#map=6/51.330/10.453)
8. [Dual Maps (Road, Aerial, Street)](http://data.mashedworld.com/dualmaps/map.htm)
9. [List regional maps](https://en.wikipedia.org/wiki/List_of_street_view_services)
10. [Yandex.Maps](https://yandex.com/maps/)
11. [Bing.Maps](https://www.bing.com/maps)
12. [Bing SAT - Bird´s eye (nesw)](https://www.bing.com/maps?&cp=50.941157~6.958324&lvl=20&sty=o&w=100%&dir=0)
13. [Baidu (China)](https://map.baidu.com/)
14. [Transform coordinates (baidu)](https://tool.lu/coordinate/)
15. [Tencent.Maps (China)](https://map.qq.com/)
16. [Map service (China)](https://map.sogou.com/)
17. [Amap (China, street view pics)](https://www.amap.com/)
18. [Turkey.Maps](https://sehirharitasi.ibb.gov.tr/)
----

### Environmental / Earth data
1. [Earthdata Search](https://search.earthdata.nasa.gov/search)
2. [Worldview - Layers (Flood, Fire...)](https://worldview.earthdata.nasa.gov/?lg=false&t=2021-07-23-T11%3A47%3A54Z)
3. [Fire Map - NASA](https://firms.modaps.eosdis.nasa.gov/map/#d:2021-07-22..2021-07-23;l:country-outline;@83.7,22.1,5z)
----

### Conflict Data
1. [Syria Admin Google Earth](https://maps.mapaction.org/dataset/217-3496)
2. [MapAction](https://maps.mapaction.org/)
3. [Countrie Maps_UNITAR](https://www.unitar.org/maps/countries)
----

### Analyses
1. [Geomatica FreeView - Free download and software reviews - CNET Download](https://download.cnet.com/Geomatica-FreeView/3000-2383_4-10066985.html)
----

### Training/Reading
1. [High-Resolution Satellite Imagery Ordering and Analysis Handbook | American Association for the Advancement of Science](https://www.aaas.org/resources/high-resolution-satellite-imagery-ordering-and-analysis-handbook)
----

### Commercial
1. [Oneatlas.airbus.com](https://oneatlas.airbus.com/)
2. [SecureWatch - Maxar](https://explore.maxar.com/securewatch-demo)
----

### Special sites
1. [Rail Cab Rides](https://railcabrides.com/en/)
----

### Storytelling
1. [Zerstörung von Mariupol](https://www.sueddeutsche.de/projekte/artikel/politik/krieg-in-der-ukraine-zerstoerung-von-mariupol-e008044/?reduced=true)
----

### Researcher
1. [Maps.greenpeace.org](https://maps.greenpeace.org/)
----

### Tracking
1. [Search: #GEOINT OR #Satellite OR "Satellite AND OSINT"](https://twitter.com/search?q=%23GEOINT%20OR%20%23Satellite%20OR%20%22Satellite%20AND%20OSINT%22&src=typed_query&f=top)
----

### Tutorial
1. [Visualizing The Invisible: Stranded Syrians in the Desert | by Christoph Koettl | Lemming Cliff | Medium](https://medium.com/lemming-cliff/visualizing-the-invisible-stranded-syrians-in-the-desert-3feccd9707f3)
2. [Using object-based image analysis for automated displacement shelter identification and count | by Christoph Koettl | Lemming Cliff | Medium](https://medium.com/lemming-cliff/using-object-based-image-analysis-for-automated-displacement-shelter-identification-and-count-5eedfc28040d)
3. [How to...](https://towardsdatascience.com/how-to-use-open-source-satellite-data-for-your-investigative-reporting-d662cb1f9f90)
----

### Tools
1. [Google Timelapse](https://earthengine.google.com/timelapse/)
2. [Carto (visualize)](https://carto.com/)
3. [Arcgis (map)](https://www.arcgis.com/index.html#)
4. [Juxtapose (slider)](https://juxtapose.knightlab.com/)
5. [Flourish](https://public.flourish.studio/visualisation/1544556/?utm_source=showcase&utm_campaign=visualisation/1544556)
----

### SoMe geolocation
1. [Youtube (filter:360+long/4K)](https://www.youtube.com/results?search_query=berlin&sp=EgQYAngB)
2. [Youtube Geo Find](https://mattw.io/youtube-geofind/location)
3. [Facebook Search](https://sowdust.github.io/fb-search/)
4. [Google - I search from ?](http://isearchfrom.com/)
----

### extra services
1. [Directory of towns (e.g. Myanmar)](http://www.fallingrain.com/world/index.html)
2. [GeoNames](http://www.geonames.org/)
3. [PeakFinder (mountains)](https://www.peakfinder.org/)
4. [PeakVisor](http://peakvisor.com/panorama.html?lat=45.9420159&lng=7.8699421&alt=4598)
5. [3D map of the world by PeakVisor](https://peakvisor.com/panorama.html?lat=45.9420159&lng=7.8699421&alt=4598)
6. [MapChecking - People on a square](https://www.mapchecking.com/#1;48.8629656,2.2869780,18)
7. [WeGo (Traffic/Construction)](https://wego.here.com/)
8. [OpenRailwayMap](http://www.openrailwaymap.org/)
9. [EarthExplorer (science US)](https://earthexplorer.usgs.gov/)
10. [Satellites.pro](https://satellites.pro/)
11. [Latitude/Longitude Coordinates](https://www.latlong.net/)
12. [Convert Latitude/Longitude to Decimal](https://andrew.hedges.name/experiments/convert_lat_long/)
13. [Radius, Distance, Area calculator](https://www.freemaptools.com/)
----

