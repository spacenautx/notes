### Geopolitics
1. [Stratfor](https://www.stratfor.com/)
2. [Lawfare](https://www.lawfareblog.com/)
3. [Brookings](https://www.brookings.edu/)
4. [War on the Rocks](https://warontherocks.com)
5. [cimsec.org](https://cimsec.org)
6. [Army War College Publications](https://publications.armywarcollege.edu)
7. [U.S. Naval Institute](https://www.usni.org)
8. [RAND Corporation](https://www.rand.org)
9. [Center for a New American Security](https://www.cnas.org)
10. [Home](https://www.cna.org)
11. [RUSI](https://rusi.org/)
12. [Center for Strategic and International Studies |](https://www.csis.org/)
13. [Chatham House](https://www.chathamhouse.org/)
14. [Wilson Center](https://www.wilsoncenter.org/)
15. [Cyber War Map](https://embed.kumu.io/0b023bf1a971ba32510e86e8f1a38c38#apt-index/)
16. [IISS](https://www.iiss.org/)
17. [Council on Foreign Relations](https://www.cfr.org/)
18. [Home, Above Feeds, Annoucement](https://smallwarsjournal.com/)
19. [Scowcroft Center for Strategy and Security - Atlantic Council](https://www.atlanticcouncil.org/programs/scowcroft-center-for-strategy-and-security/)
20. [Rjgallagher.co.uk](https://www.rjgallagher.co.uk/)
21. [The Cipher Brief](https://www.thecipherbrief.com/)
22. [Center for European Policy Analysis](https://www.cepa.org/)
----

### Web Tools
1. [Threat Encyclopedia - Trend Micro USA](https://www.trendmicro.com/vinfo/us/threat-encyclopedia/)
2. [Deepsight.symantec.com/PortalNextgen](https://deepsight.symantec.com/PortalNextgen)
3. [Moveit.jmn.disa.mil](https://moveit.jmn.disa.mil/)
4. [MITRE ATT&CK™](https://attack.mitre.org/)
5. [Unit42](https://unit42.paloaltonetworks.com/)
6. [Login | Falcon](https://falcon.crowdstrike.com/login/)
7. [Forensicswiki.org](https://www.forensicswiki.org)
8. [Interactive Online Malware Analysis Sandbox](https://app.any.run)
9. [PacketTotal](https://packettotal.com/)
10. [RiskIQ Community Edition](https://community.riskiq.com/login)
11. [Censys](https://censys.io)
12. [Shodan.com](https://shodan.com)
13. [CRT](https://crt.sh)
14. [certstream.calidog.io](https://certstream.calidog.io/)
15. [Symantec Sitereview](https://sitereview.bluecoat.com/#/)
16. [Ems.dc3on.gov](https://ems.dc3on.gov/#!/auth/login)
17. [Apt.threattracking.com](https://apt.threattracking.com)
18. [VirusTotal](https://www.virustotal.com/)
19. [CyberChef](https://gchq.github.io/CyberChef/)
20. [URL and website scanner](https://urlscan.io/)
21. [Collective Intelligence Framework — CSIRT Gadgets, LLC](https://csirtgadgets.com/collective-intelligence-framework)
22. [DataSploit](https://datasploit.readthedocs.io/en/latest/)
23. [Tweettioc.com](https://tweettioc.com/)
24. [Network Tools: DNS,IP,Email](https://mxtoolbox.com/SuperTool.aspx?action=spf%3abp.com&run=toolpage)
25. [archive.ph](https://archive.ph/)
----

### Education & Training
1. [Learn Git Branching](https://learngitbranching.js.org/)
2. [platform.avatao.com/discover/paths](https://platform.avatao.com/discover/paths)
3. [Fedvte.usalearning.gov/coursecat_external.php?group=ALL](https://fedvte.usalearning.gov/coursecat_external.php?group=ALL)
4. [Cybrary](https://www.cybrary.it/)
5. [Hunting with Splunk: The Basics](https://www.splunk.com/blog/2017/07/06/hunting-with-splunk-the-basics.html)
6. [GRC | SSL TLS HTTPS Web Server Certificate Fingerprints](https://www.grc.com/fingerprints.htm)
7. [www.dfir.training/resources/downloads/cheatsheets-infographics](https://www.dfir.training/resources/downloads/cheatsheets-infographics)
8. [GitHub - aptnotes/data: APTnotes data](https://github.com/aptnotes/data)
9. [Cybrary](https://www.cybrary.it)
10. [Skillset](https://www.skillset.com)
11. [Www.cccure.education/register](https://www.cccure.education/register)
12. [Professor Messer IT Certification Training Courses](https://www.professormesser.com/)
13. [explainshell.com](https://explainshell.com/)
14. [www.virusbulletin.com/virusbulletin/2019/11/vb2019-paper-fantastic-information-and-where-find-it-guidebook-open-source-ot-reconnaissance/](https://www.virusbulletin.com/virusbulletin/2019/11/vb2019-paper-fantastic-information-and-where-find-it-guidebook-open-source-ot-reconnaissance/)
15. [IppSec
 - YouTube](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA/featured)
16. [A penetration tester’s guide to subdomain enumeration](https://blog.appsecco.com/a-penetration-testers-guide-to-sub-domain-enumeration-7d842d5570f6)
17. [VirusTotal Intelligence - Your malware research telescope](https://www.virustotal.com/intelligence/help/malware-hunting/)
18. [iamciso.files.wordpress.com/2019/04/virustotal-for-investigators.pdf](https://iamciso.files.wordpress.com/2019/04/virustotal-for-investigators.pdf)
19. [Threat Intelligence and the Limits of Malware Analysis | Dragos](https://dragos.com/resource/threat-intelligence-and-the-limits-of-malware-analysis/)
20. [Teach Yourself Computer Science](https://teachyourselfcs.com/)
21. [Iron Geek](https://www.irongeek.com/)
22. [https://tryhackme.com/](https://tryhackme.com/)
----

### Analytical Tools
1. [Cognitive Bias Codex](https://upload.wikimedia.org/wikipedia/commons/6/65/Cognitive_bias_codex_en.svg)
----

### OSINT Frameworks & Resources
1. [bellingcat](https://www.bellingcat.com/)
2. [Bellingcat's Digital Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
3. [Boolean Strings Irina Shamaeva](https://booleanstrings.com/)
4. [DFIR Cheat sheets and Infographics](https://www.dfir.training/resources/downloads/cheatsheets-infographics)
5. [DiRT Directory](https://dirtdirectory.org/)
6. [Duplichecker.com Free Tools](https://www.duplichecker.com/free-tools.php)
7. [Factcheckingday.com](https://factcheckingday.com/)
8. [GIJC2017 handout - Margot Williams.docx](https://drive.google.com/file/d/1pLjKuNjFyHGDiOAW7sLmX7L9o2j5kdbF/view)
9. [GIJN  Support : / Help Desk](https://helpdesk.gijn.org/support/home?utm_source=GIJN+Mailing+List&utm_campaign=ace65367e8-EMAIL_CAMPAIGN_2019_10_17_12_57&utm_medium=email&utm_term=0_eae0e8c5a9-ace65367e8-121295345&mc_cid=ace65367e8&mc_eid=a51bc8c883)
10. [i-intelligence Chris Pallaris](https://www.i-intelligence.eu/osint-tools-and-resources-handbook-2018/)
11. [ilove OSINT on Github](https://github.com/ilovecode2018/awesome-osint)
12. [IntelTechniques.com BUSCADOR OSINT VM](https://inteltechniques.com/links.html)
13. [IVMachiavelli/OSINT_Team_Links](https://github.com/IVMachiavelli/OSINT_Team_Links)
14. [Jivoi Awesome-osint](https://github.com/jivoi/awesome-osint)
15. [Netbootcamp OSINT Tools](http://netbootcamp.org/osinttools/)
16. [OSINT 🔎 STASH](https://t.co/If3wrURiyT?amp=1)
17. [OSINT Arno Reuser](http://rr.reuser.biz/)
18. [OSINT Framework Justin Nordine](http://osintframework.com/)
19. [OSINT Technisette (The Great OSINT index)](https://start.me/p/m6XQ08/osint)
20. [OSINT Tools (ENG)](https://www.aware-online.com/en/osint-tools/)
21. [OSINT Tools Aware Online (Dutch)](https://www.aware-online.com/osint-tools/)
22. [OSINT YOGA Created by WebBreacher Hoffman](https://yoga.osint.ninja/)
23. [OSINTessentials - Eoghan Sweeneyt](https://www.osintessentials.com/)
24. [Osintpost](https://osintpost.com/open-source-investigation-guides/)
25. [Qwarie UK OSINT Neil Smith](http://www.uk-osint.net/favorites.html)
26. [Reddit OSINT Page](https://old.reddit.com/r/OSINT/)
27. [Research Clinic Paul Meyers](http://researchclinic.net/)
28. [Search Phil Bradley](http://www.philb.com/)
29. [Sprp77's OSINT Resources](https://drive.google.com/drive/folders/1CBcemFdorkAqJ-Sthsh67OVHgH4FQF05)
30. [Toddington-Free Resources Knowledge Base](https://www.toddington.com/resources/tii-free-resources-knowledge-base/)
31. [OSINT Essentials](https://www.osintessentials.com/)
----

### Host Based tools
1. [Download Elastic Products | Elastic](https://www.elastic.co/downloads/)
2. [snort rule infographic final nobleed](https://snort-org-site.s3.amazonaws.com/production/document_files/files/000/000/116/original/Snort_rule_infographic.pdf)
3. [Microsoft Safety Scanner Download - Windows security | Microsoft Docs](https://docs.microsoft.com/en-us/windows/security/threat-protection/intelligence/safety-scanner-download)
----

### Incident Response
1. [https://zeltser.com/media/docs/security-incident-questionnaire-cheat-sheet.pdf](https://zeltser.com/media/docs/security-incident-questionnaire-cheat-sheet.pdf)
----

### PenTesting
1. [Null Byte
 - YouTube](https://www.youtube.com/channel/UCgTNupxATBfWmfehv21ym-g)
2. [Full Ethical Hacking Course - Network Penetration Testing for Beginners (2019) - YouTube](https://www.youtube.com/watch?v=3Kq1MIfTWCE)
3. [HackerSploit
 - YouTube](https://www.youtube.com/channel/UC0ZTPkdxlAKf-V33tqXwi3Q)
4. [Bienvenue                                        
[Root Me](https://www.root-me.org)
----

### Cheat Sheets
1. [The Ultimate List of SANS Cheat Sheets | SANS Institute](https://www.sans.org/blog/the-ultimate-list-of-sans-cheat-sheets/)
2. [Cybersec Cheat Sheets in all Flavors! (Huge List Inside)](https://www.reddit.com/r/cybersecurity/comments/iu17uu/cybersec_cheat_sheets_in_all_flavors_huge_list/)
----

### Firefox AddOn | Security & Productivity
1. [Copy Plaintext](https://addons.mozilla.org/en-US/firefox/addon/copy-plaintext)
2. [Download Firefox from a trusted source](https://www.firefox.com/)
3. [Exif Viewer](https://addons.mozilla.org/en-US/firefox/addon/exif-viewer/?)
4. [Google Translate for Firefox](https://addons.mozilla.org/en-US/firefox/addon/google-translator-for-firefox/)
5. [HTTPS Everywhere](https://addons.mozilla.org/de/firefox/addon/https-everywhere/)
6. [Image Search Options](https://addons.mozilla.org/en-US/firefox/addon/image-search-options/?)
7. [Multi Account Container](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/?)
8. [uBlock Origin – Add-ons for Firefox (2)](https://addons.mozilla.org/firefox/addon/ublock-origin/)
9. [unMHT Save Page as MHTML](https://addons.mozilla.org/de/firefox/addon/save-page-we/?src=search)
10. [Video Download Helper](https://addons.mozilla.org/en-US/firefox/addon/video-downloadhelper/?)
----

### PowerShell
1. [www.sans.org/reading-room/whitepapers/incident/disrupting-empire-identifying-powershell-empire-command-control-activity-38315](https://www.sans.org/reading-room/whitepapers/incident/disrupting-empire-identifying-powershell-empire-command-control-activity-38315)
2. [github.com/Neo23x0/signature-base/blob/master/yara/gen_powershell_empire.yar](https://github.com/Neo23x0/signature-base/blob/master/yara/gen_powershell_empire.yar)
3. [Www.slideshare.net/Hackerhurricane/you-can-detect-powershell-attacks](https://www.slideshare.net/Hackerhurricane/you-can-detect-powershell-attacks)
4. [Learn Windows PowerShell in a Month of Lunches](https://www.youtube.com/playlist?list=PL6D474E721138865A)
----

### Languages & OS
1. [RegExr](https://regexr.com/)
2. [Regex Tutorial—From Regex 101 to Advanced Regex](https://www.rexegg.com/)
3. [BRAND NEW: Index of Tips and Tricks](https://blog.commandlinekungfu.com/p/index-of-tips-and-tricks.html)
4. [tutoriaLinux
 - YouTube](https://www.youtube.com/channel/UCvA_wgsX6eFAOXI8Rbg_WiQ)
----

### Tech
1. [Wired](https://www.wired.com)
2. [MIT Technology Review](https://www.technologyreview.com/)
----

### Science
1. [IFLScience](https://www.iflscience.com)
----

### News
1. [National Public Radio (NPR)](https://www.npr.org)
----

