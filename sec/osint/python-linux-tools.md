### Corporate Searches
1. [GitHub - skickar/OpenCorporatesCLI: Business search, fun with rate limiting](https://github.com/skickar/OpenCorporatesCLI)
----

### Domain, IP & Web Searches
1. [GitHub - Yashvendra/Recon-X: Advanced Reconnaissance tool to enumerate attacking surface of the target.](https://github.com/Yashvendra/Recon-X.git)
2. [abdulgaphy/r3con1z3r](https://github.com/abdulgaphy/r3con1z3r)
3. [thewhiteh4t/FinalRecon](https://github.com/thewhiteh4t/FinalRecon)
4. [https://github.com/s0md3v/Photon](https://github.com/s0md3v/Photon)
5. [harleo/knockknock](https://github.com/harleo/knockknock)
----

### Distros
1. [Tails - Download and install Tails](https://tails.boum.org/install/)
2. [Kali Linux](https://www.kali.org/)
3. [Buscador OSINT VM](https://inteltechniques.com/buscador/)
4. [Tsurugi Linux](https://tsurugi-linux.org/index.php)
5. [axlshear/dora-osint-vm](https://github.com/axlshear/dora-osint-vm)
6. [Osintux.org](http://www.osintux.org/)
7. [OSINT VM](https://www.tracelabs.org/initiatives/osint-vm)
----

### Email Recon
1. [sham00n/buster](https://github.com/sham00n/buster)
2. [https://github.com/woj-ciech/Danger-zone](https://github.com/woj-ciech/Danger-zone)
3. [https://github.com/marztinvigo/email2phonenumber](https://github.com/marztinvigo/email2phonenumber)
4. [Github.com](https://github.com/m411ok/infoga.git)
5. [iojw/socialscan](https://github.com/iojw/socialscan)
6. [https://github.com/laramies/theHarvester.git](https://github.com/laramies/theHarvester.git)
7. [https://github.com/khast3x/h8mail](https://github.com/khast3x/h8mail)
8. [GitHub - WhiteHatInspector/emailGuesser: This is an open source project which helps users "guess" their target's email address based on multiple inputs and preferences.](https://github.com/WhiteHatInspector/emailGuesser)
9. [GitHub - holehe](https://github.com/megadose/holehe)
----

### Facebook
1. [harismuneer/Ultimate-Facebook-Scraper](https://github.com/harismuneer/Ultimate-Facebook-Scraper)
----

### Document Search
1. [opsdisk/metagoofil](https://github.com/opsdisk/metagoofil)
----

### Kamerka
1. [Hunting with ꓘamerka 2.0 aka FIST (Flickr, Instagram, Shodan, Twitter)](https://hackernoon.com/hunting-with-%EA%93%98amerka-2-0-aka-fist-flickr-instagram-shodan-twitter-ca363f12562a)
2. [woj-ciech/kamerka](https://github.com/woj-ciech/kamerka)
----

### LinkedIn
1. [GitHub - m8r0wn/CrossLinked: LinkedIn enumeration tool to extract valid employee names from an organization through search engine scraping. Names can be formatted in a defined naming convention for further security testing.](https://github.com/m8r0wn/CrossLinked)
2. [GitHub - 0xZDH/BridgeKeeper: Scrape employee names from search engine LinkedIn profiles. Convert employee names to a specified username format.](https://github.com/0xZDH/BridgeKeeper)
3. [https://github.com/x-0117/Tarantula-For-LinkedIn#:~:text=Tarantula%20is%20a%20crawler%20or%20spider%2C%20currently%20designed,filtering%20profiles%20by%20searching%20for%20keywords%20in%20them](https://github.com/x-0117/Tarantula-For-LinkedIn#:~:text=Tarantula%20is%20a%20crawler%20or%20spider%2C%20currently%20designed,filtering%20profiles%20by%20searching%20for%20keywords%20in%20them)
4. [initstring/linkedin2username](https://github.com/initstring/linkedin2username)
----

### Google Analytic Recon
1. [jakecreps/swamp](https://github.com/jakecreps/swamp)
----

### Instagram
1. [GitHub - bernsteining/InstaLocTrack: A SOCMINT/OSINT project which goal is to fetch all the locations related to an Instagram profile in order to plot them on a map.](https://github.com/bernsteining/InstaLocTrack)
2. [https://github.com/blackhatethicalhacking/InstagramOSINT](https://github.com/blackhatethicalhacking/InstagramOSINT)
3. [https://github.com/sc1341/InstagramOSINT](https://github.com/sc1341/InstagramOSINT)
4. [rarcega/instagram-scraper](https://github.com/rarcega/instagram-scraper)
5. [https://github.com/instaloader/instaloader](https://github.com/instaloader/instaloader)
6. [GitHub](https://github.com/novitae/sterraxcyl)
----

### Pastebin Scraping
1. [GitHub - sandwichi/pastebin-scraper: Scrapes pastebin.com API for pastes containing supplied keywords.](https://github.com/sandwichi/pastebin-scraper)
----

### People Recon
1. [xillwillx/skiptracer](https://github.com/xillwillx/skiptracer)
2. [HA71/Namechk](https://github.com/HA71/Namechk)
3. [https://github.com/sherlock-project/sherlock.git](https://github.com/sherlock-project/sherlock.git)
4. [GitHub - 0xZDH/BridgeKeeper: Scrape employee names from search engine LinkedIn profiles. Convert employee names to a specified username format.](https://github.com/0xZDH/BridgeKeeper)
5. [GitHub - 1d8/username.py: US name osint searches](https://github.com/1d8/username.py)
----

### Phone Number Recon
1. [GitHub - sundowndev/PhoneInfoga: Advanced information gathering & OSINT tool for phone numbers](https://github.com/sundowndev/PhoneInfoga.git)
2. [https://github.com/martinvigo/email2phonenumber](https://github.com/martinvigo/email2phonenumber)
3. [GitHub - phomber](https://github.com/s41r4j/phomber)
----

### Recon-NG
1. [Recon-ng V5 - YouTube](https://www.youtube.com/playlist?list=PLBf0hzazHTGOg9taK90uFjdcb8UgGfRKZ)
2. [Recon-ng Tutorial – Part 1 Install and Setup | Secure Network Management](http://securenetworkmanagement.com/recon-ng-tutorial-part-1/)
----

### Shodan
1. [Shodan Help Center](https://help.shodan.io/)
2. [Shodan Search Engine](https://beta.shodan.io/search/examples)
3. [Shodan Pentesting Guide](https://community.turgensec.com/shodan-pentesting-guide/)
----

### SnapChat
1. [sc1341/SnapMap-OSINT](https://github.com/sc1341/SnapMap-OSINT)
----

### Sn0int
1. [kpcyrd/sn0int](https://github.com/kpcyrd/sn0int)
----

### Spiderfoot
1. [DNS recon with SpiderFoot - asciinema](https://asciinema.org/a/295912)
2. [https://github.com/smicallef/spiderfoot.git](https://github.com/smicallef/spiderfoot.git)
----

### Telegram
1. [GitHub - th3unkn0n/TeleGram-Scraper: telegram group scraper tool. fetch all information about group members](https://github.com/th3unkn0n/TeleGram-Scraper)
----

### TikTok
1. [sc1341/TikTokOSINT](https://github.com/sc1341/TikTokOSINT)
----

### Twitter
1. [vaguileradiaz/tinfoleak](https://github.com/vaguileradiaz/tinfoleak)
2. [https://github.com/twintproject/twint](https://github.com/twintproject/twint)
3. [https://github.com/MazenElzanaty/TwLocation](https://github.com/MazenElzanaty/TwLocation)
----

### Username Recon
1. [iojw/socialscan](https://github.com/iojw/socialscan)
2. [https://github.com/sherlock-project/sherlock.git](https://github.com/sherlock-project/sherlock.git)
3. [https://github.com/0x8b30cc/UserHawk](https://github.com/0x8b30cc/UserHawk)
4. [GitHub - 7rillionaire/Search4: A tool to search a particular username on almost every social platform and tell , whether the user with that username exists on that site or not.](https://github.com/7rillionaire/Search4)
5. [GitHub - 1d8/username.py: made for osint searches](https://github.com/1d8/username.py)
6. [GitHub - ITermSec/USearch: USearch (User name Search social network)](https://github.com/ITermSec/USearch)
----

### Wayback Machine
1. [GitHub](https://github.com/lorenzoromani1983/wayback-keyword-search)
----

