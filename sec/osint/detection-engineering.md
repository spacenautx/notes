### Domain Analysis
1. [urlscan](http://urlscan.io/)
2. [joe sandbox](https://www.joesandbox.com/)
3. [www.hybrid-analysis.com/](https://www.hybrid-analysis.com)
4. [Services.normshield.com/](http://services.normshield.com/)
5. [Securitytrails](http://securitytrails.com/)
6. [Ring of Saturn Internetworking](http://networking.ringofsaturn.com/)
7. [Blocklist Removal Center - The Spamhaus Project](http://www.spamhaus.org/lookup/)
8. [SpamCop.net](http://www.spamcop.net/bl.shtml)
9. [desenmascara.me/](http://desenmascara.me/)
10. [cymon](http://cymon.io/)
11. [PhishStats](http://phishstats.info/)
12. [talos intelligence](http://talosintelligence.com/)
13. [elceef/dnstwist](http://github.com/elceef/dnstwist)
14. [GitHub - FGRibreau/mailchecker: Cross-language temporary (disposable/throwaway) email detection library. Covers 33 078 fake email providers.](http://github.com/FGRibreau/mailchecker)
15. [GitHub - HurricaneLabs/machinae: Machinae Security Intelligence Collector](http://github.com/hurricanelabs/machinae)
16. [GitHub - michael-yip/MaltegoVT: A set of Maltego transforms for VirusTotal Public API v2.0. This set has the added functionality of caching queries on a daily basis to speed up resolutions.](http://github.com/michael-yip/MaltegoVT)
17. [GitHub - EmersonElectricCo/boomerang: A tool designed for consistent and safe capture of off network web resources.](http://github.com/EmersonElectricCo/boomerang)
18. [Free Online Tools for Looking up Potentially Malicious Websites](http://zeltser.com/lookup-malicious-websites/)
19. [badips.com | an IP based abuse tracker](http://www.badips.com/)
20. [Zscaler Zulu URL Risk Analyzer](http://zulu.zscaler.com/)
21. [GitHub - hiddenillusion/IPinfo: Searches various online resources to try and get as much info about an IP/domain as possible.](http://github.com/hiddenillusion/IPinfo)
22. [TekDefense](http://www.tekdefense.com/automater/)
23. [Whois. DomainTools](http://whois.domaintools.com/)
24. [MultiRBL.valli.org](http://multirbl.valli.org/)
25. [site check sucuri](http://sitecheck.sucuri.net/)
26. [urlquery.net/](http://urlquery.net/)
27. [Intezer Analyze](https://analyze.intezer.com/#/)
----

### MALWARE: Detection and Classification
1. [chkrootkit -- locally checks for signs of a rootkit](http://www.chkrootkit.org/)
2. [ClamAV](http://www.clamav.net/)
3. [ExifTool by Phil Harvey](http://sno.phy.queensu.ca/~phil/exiftool/)
4. [#totalhash | Malware Analysis Database](http://totalhash.cymru.com/)
5. [PE-bear | hasherezade's 1001 nights](http://hshrzd.wordpress.com/pe-bear/)
6. [Witryna nie jest dostępna](http://exeinfo.pe.hu/)
7. [The Rootkit Hunter project](http://rkhunter.sourceforge.net/)
8. [Bitbucket](http://bitbucket.org/cse-assemblyline/assemblyline)
9. [ssdeep - Fuzzy hashing program](http://ssdeep-project.github.io/ssdeep/)
10. [KoreLogicSecurity/mastiff](http://github.com/KoreLogicSecurity/mastiff)
11. [GitHub - airbnb/binaryalert: BinaryAlert: Serverless, Real-time & Retroactive Malware Detection. Good news, we are hiring!!!!!!!! https://careers.airbnb.com/positions/213964/](http://github.com/airbnb/binaryalert)
12. [GitHub - jessek/hashdeep](http://github.com/jessek/hashdeep)
13. [GitHub - EmersonElectricCo/fsf: File Scanning Framework](http://github.com/EmersonElectricCo/fsf)
14. [GitHub - gurnec/HashCheck: HashCheck Shell Extension for Windows with added SHA2, SHA3, and multithreading; originally from code.kliu.org](http://github.com/gurnec/HashCheck)
15. [GitHub - JusticeRage/Manalyze: A static analyzer for PE executables.](http://github.com/JusticeRage/Manalyze)
16. [GitHub - hiddenillusion/AnalyzePE: Wraps around various tools and provides some additional checks/information to produce a centralized report of a PE file.](http://github.com/hiddenillusion/AnalyzePE)
17. [GitHub - Dynetics/Malfunction: Malware Analysis Tool using Function Level Fuzzy Hashing](http://github.com/Dynetics/Malfunction)
18. [GitHub - rjhansen/nsrllookup](http://github.com/rjhansen/nsrllookup)
19. [GitHub - uppusaikiran/generic-parser: A Single Library Parser to extract meta information,static analysis and detect macros within the files.](http://github.com/uppusaikiran/generic-parser)
20. [GitHub - horsicq/Detect-It-Easy: Program for determining types of files for Windows, Linux and MacOS.](http://github.com/horsicq/Detect-It-Easy)
21. [GitHub - mitre/multiscanner: Modular file scanning/analysis framework](http://github.com/mitre/multiscanner)
22. [Github.com/uppusaikiran/virustotal-falsepositive-detector](http://github.com/uppusaikiran/virustotal-falsepositive-detector)
23. [Neo23x0/Loki](http://github.com/Neo23x0/Loki)
24. [Python script to interface with totalhash.com · GitHub](http://gist.github.com/gleblanc1783/3c8e6b379fa9d646d401b96ab5c7877f)
25. [GitHub - uppusaikiran/yara-finder: Simple tool to find the yara matches on a file](http://github.com/uppusaikiran/yara-finder)
26. [GitHub - Neo23x0/yarGen: yarGen is a generator for YARA rules](http://github.com/Neo23x0/yarGen)
27. [pev - the PE file analysis toolkit](http://pev.sourceforge.net/)
28. [Page Redirection](http://plusvic.github.io/yara/)
29. [handlers.sans.org/jclausing/packerid.py](http://handlers.sans.org/jclausing/packerid.py)
30. [Marco Pontello's Home](http://mark0.net/soft-trid-e.html)
----

### Active Directory
1. [Active Directory Security – Active Directory & Enterprise Security, Methods to Secure Active Directory, Attack Methods & Effective Defenses, PowerShell, Tech Notes, & Geek Trivia…](https://adsecurity.org/)
2. [Lay of the Land with BloodHound](http://threat.tevora.com/lay-of-the-land-with-bloodhound/)
3. [harmj0y](http://www.harmj0y.net)
4. [Active Directory Security: The Good, the Bad, & the UGLY – Active Directory Security](https://adsecurity.org/?p=3622)
5. [SharpHound: Evolution of the BloodHound Ingestor](https://blog.cptjesus.com/posts/newbloodhoundingestor)
6. [Mimikatz DCSync Usage, Exploitation, and Detection – Active Directory Security](https://adsecurity.org/?p=1729)
7. [Targeted Kerberoasting](http://www.harmj0y.net/blog/activedirectory/targeted-kerberoasting/)
8. [Detecting Kerberoasting Activity Part 2 – Creating a Kerberoast Service Account Honeypot – Active Directory Security](https://adsecurity.org/?p=3513)
9. [Detecting the Elusive-ActiveDirectory ThreatHunting](https://adsecurity.org/wp-content/uploads/2017/04/2017-BSidesCharm-DetectingtheElusive-ActiveDirectoryThreatHunting-Final.pdf)
10. [Active Directory as a C2 (Command & Control)](https://akijosberryblog.wordpress.com/2018/03/17/active-directory-as-a-c2-command-control/)
----

### Malware
1. [Malpedia (Fraunhofer FKIE)](https://malpedia.caad.fkie.fraunhofer.de/)
2. [Malware - Threat Encyclopedia - Trend Micro NO](https://www.trendmicro.com/vinfo/no/threat-encyclopedia/malware)
----

### Detection Engineering Resources
1. [Uncoder.io](https://uncoder.io/)
2. [Azure Resource Manager template reference](https://docs.microsoft.com/en-us/azure/templates/)
3. [GitHub - splunk/security_content: Splunk Security Content](https://github.com/splunk/security_content)
4. [Technical documentation, API, and code examples](https://docs.microsoft.com/en-us/)
5. [SOC Prime Threat Detection Marketplace (TDM) - Join for Free](https://tdm.socprime.com/login/#)
6. [Azure resource provider operations | Microsoft Docs](https://docs.microsoft.com/en-us/azure/role-based-access-control/resource-provider-operations)
7. [Monitoring what matters – Windows Event Forwarding for everyone (even if you already have a SIEM.)](https://blogs.technet.microsoft.com/jepayne/2015/11/23/monitoring-what-matters-windows-event-forwarding-for-everyone-even-if-you-already-have-a-siem/)
8. [Sysmon Events Table](https://rawsec.lu/blog/posts/2017/Sep/19/sysmon-events-table/)
9. [Windows Event Forwarding - Centralized logging for everyone! (Even if you already have centralized logging!)](https://channel9.msdn.com/Events/Ignite/Australia-2015/INF327)
10. [Basics of Windows Incident Response](https://jordanpotti.com/2017/01/20/basics-of-windows-incident-response/)
11. [Use Windows Event Forwarding to help with intrusion detection (Windows 10)](https://docs.microsoft.com/en-us/windows/security/threat-protection/use-windows-event-forwarding-to-assist-in-intrusion-detection)
12. [Windows Event Forwarding for Network Defense – Palantir – Medium](https://medium.com/@palantir/windows-event-forwarding-for-network-defense-cb208d5ff86f)
13. [Cheat-Sheets](https://www.malwarearchaeology.com/cheat-sheets)
14. [Tales of a Threat Hunter 1](https://www.eideon.com/2017-09-09-THL01-Mimikatz/)
15. [RDP hijacking — how to hijack RDS and RemoteApp sessions transparently to move through an…](https://medium.com/@networksecurity/rdp-hijacking-how-to-hijack-rds-and-remoteapp-sessions-transparently-to-move-through-an-da2a1e73a5f6)
----

### Places for Ideas
1. [www.tenforums.com/tutorials](https://www.tenforums.com/tutorials/)
2. [Adam the Automator](https://adamtheautomator.com/)
3. [Windows Archives - e-Methods Technologies](https://e-methodstechnologies.com/category/windows/)
4. [Woshub.com](http://woshub.com/)
5. [Event Types | Okta Developer](https://developer.okta.com/docs/reference/api/event-types/)
6. [https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#-strong-api-overview-strong-](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#-strong-api-overview-strong-)
----

### Detection
1. [DNSlogging - Google Drive](https://drive.google.com/drive/folders/0B5BuM3k0_mF3LXpnYVUtU091Vjg)
2. [Attack Infrastructure Logging – Part 1: Logging Server Setup – VIVI](https://thevivi.net/2018/03/23/attack-infrastructure-logging-part-1-logging-server-setup/)
3. [threathunting-spl/Detecting_Similarity.md at master · inodee/threathunting-spl · GitHub](https://github.com/inodee/threathunting-spl/blob/master/hunt-queries/Detecting_Similarity.md)
4. [threathunting-spl/spl_tips_tricks.md at master · inodee/threathunting-spl · GitHub](https://github.com/inodee/threathunting-spl/blob/master/spl_tips_tricks.md)
5. [DFIR and Threat Hunting: Patterns of Behavior](http://findingbad.blogspot.nl/2017/02/patterns-of-behavior.html?m=1)
6. [DFIR and Threat Hunting: A Few of My Favorite Things - Continued](http://findingbad.blogspot.nl/2017/12/a-few-of-my-favorite-things-continued.html?m=1)
7. [Tales of a Threat Hunter 2](https://www.eideon.com/2018-03-02-THL03-WMIBackdoors/#wmi-persistence-detection)
8. [Troopers_MSFT_Defence_at_scale.pdf - Google Drive](https://drive.google.com/file/d/1QXjmlPRvfiRBnqzNpsTQo5bn0xKQoNc4/view)
9. [GitHub - beahunt3r/Windows-Hunting](https://github.com/beahunt3r/Windows-Hunting)
10. [GitHub - botherder/snoopdigg: Simple utility to ease the process of collecting evidence to find infections](https://github.com/botherder/snoopdigg)
11. [Home | Happy Threat Hunting](https://www.happythreathunting.com/)
12. [GitHub - socprime/SigmaRulesIntegration](https://github.com/socprime/SigmaRulesIntegration)
13. [Analyzing Attacker&#39;s &quot;Soft&quot; TTP&#39;s with Splunk](https://nullsecure.org/analyzing-soft-ttps-in-splunk/)
14. [DerbyCon 2017, Ryan Nolette&#39;s &#39;How to Hunt for Lateral Movement on Your Network&#39; - Security Boulevard](https://securityboulevard.com/2017/11/derbycon-2017-ryan-nolettes-how-to-hunt-for-lateral-movement-on-your-network/)
15. [https://static1.squarespace.com/static/552092d5e4b0661088167e5c/t/59c1814829f18782e24f1fe2/1505853768977/Windows+PowerShell+Logging+Cheat+Sheet+ver+Sept+2017+v2.1.pdf](https://static1.squarespace.com/static/552092d5e4b0661088167e5c/t/59c1814829f18782e24f1fe2/1505853768977/Windows+PowerShell+Logging+Cheat+Sheet+ver+Sept+2017+v2.1.pdf)
16. [JPCERT/CC Blog: Investigate Unauthorised Logon Attempts using LogonTracer](http://blog.jpcert.or.jp/2018/01/investigate-unauthorised-logon-attempts-using-logontracer.html)
17. [JPCERT/CC Blog: Research Report Released: Detecting Lateral Movement through Tracking Event Logs (Version 2)](http://blog.jpcert.or.jp/2017/12/research-report-released-detecting-lateral-movement-through-tracking-event-logs-version-2.html)
18. [Tool Analysis Result Sheet](https://jpcertcc.github.io/ToolAnalysisResultSheet/)
19. [Introducing the Adversary Resilience Methodology — Part One](https://posts.specterops.io/introducing-the-adversary-resilience-methodology-part-one-e38e06ffd604)
20. [Introducing the Adversary Resilience Methodology — Part Two](https://posts.specterops.io/introducing-the-adversary-resilience-methodology-part-two-279a1ed7863d)
21. [Blue Team Fundamentals. – SecurityBytes](https://securitybytes.io/blue-team-fundamentals-4ee226368b7b)
22. [Blue Team fundamentals Part Two: Windows Processes.](https://securitybytes.io/blue-team-fundamentals-part-two-windows-processes-759fe15965e2)
----

### MITRE Attack
1. [Coverage Comparison | MITRE Cyber Analytics Repository](https://car.mitre.org/coverage/)
----

### Defense/Blue Team
1. [Blue Team Training Toolkit](https://www.bt3.no/)
2. [Github: DetectionLab](https://github.com/clong/DetectionLab)
3. [Github: Great List of Resources to Build an Enterprise Grade Home Lab](https://github.com/aboutsecurity/blueteam_homelabs)
4. [GitHub: leonidas - Automated Attack Simulation](https://github.com/fsecurelabs/leonidas)
5. [Github: SecLists](https://github.com/danielmiessler/SecLists)
6. [GitHub: tsunami-security-scanner-plugins](https://github.com/google/tsunami-security-scanner-plugins)
----

### Malware Analysis
1. [redcode-labs/Coldfire](https://github.com/redcode-labs/Coldfire)
2. [nbeede/BoomBox](https://github.com/nbeede/BoomBox)
3. [GitHub - francisck/DanderSpritz_lab: A fully functional DanderSpritz lab in 2 commands](https://github.com/francisck/DanderSpritz_lab)
4. [clong/DetectionLab](https://github.com/clong/DetectionLab)
5. [Reverse Engineering 101](https://malwareunicorn.org/workshops/re101.html#0)
----

### NETWORK: Sniffer
1. [Addons.mozilla.org/de/firefox/addon/live-http-headers/](http://addons.mozilla.org/de/firefox/addon/live-http-headers/)
2. [netsniff-ng toolkit](http://netsniff-ng.org/)
3. [Wireshark · Go Deep.](http://www.wireshark.org/)
----

### Processes: Windows
1. [User mode and kernel mode - Windows drivers | Microsoft Docs](https://docs.microsoft.com/en-us/windows-hardware/drivers/gettingstarted/user-mode-and-kernel-mode)
2. [Blue Team fundamentals Part Two: Windows Processes.](https://securitybytes.io/blue-team-fundamentals-part-two-windows-processes-759fe15965e2)
3. [Code & Process Injection - Red Teaming Experiments](https://www.ired.team/offensive-security/code-injection-process-injection)
4. [Process Security and Access Rights - Win32 apps | Microsoft Docs](https://docs.microsoft.com/en-us/windows/win32/procthread/process-security-and-access-rights)
5. [Process Injection Techniques used by Malware | by Angelystor | CSG @ GovTech | Medium](https://medium.com/csg-govtech/process-injection-techniques-used-by-malware-1a34c078612c)
6. [FUNDAMENTAL WINDOWS PROCESSES. Hello friends. In this blog post I… | by Alparslan Akyıldız academy | Medium](https://alparslanakyildiz.medium.com/fundamental-windows-processes-6341696cf4f0)
7. [Understanding And Detecting Dll 1nj3ct0n & Process Hollowing | by Alparslan Akyıldız academy | Medium](https://alparslanakyildiz.medium.com/understanding-and-detecting-dll-1nj3ct0n-process-hollowing-fcd87676d36b)
8. [Process Injection Techniques. This article contains an overview of… | by Ozan Unal | Medium](https://medium.com/@ozan.unal/process-injection-techniques-bc6396929740)
----

### Detection Engineering for AWS
1. [Detectioninthe.cloud](http://detectioninthe.cloud/)
----

### Other SIEM Detection Rules
1. [MITRE ATT&CK Analytics — Alert Rules latest documentation](https://docs.logpoint.com/docs/alert-rules/en/latest/MITRE.html)
2. [Splunk Security Essentials DocsSSE Content :: Splunk Security Essentials Docs](https://docs.splunksecurityessentials.com/content-detail/)
3. [GitHub - vadim-hunter/Detection-Ideas-Rules: Detection Ideas & Rules repository.](https://github.com/vadim-hunter/Detection-Ideas-Rules)
4. [CyberDrain](https://www.cyberdrain.com/)
5. [KB - AventisTech](https://aventistech.com/kb/)
----

### Security Company Blogs
1. [Blog - Sophos Labs - Sophos Community](https://community.sophos.com/sophos-labs/b/blog)
----

### Snort and Suricata Rules
1. [3CORESec](https://blacklist.3coresec.net/)
----

### SIGMA/OSSEM
1. [OSSEM Detection Model: Leveraging Data Relationships to Generate Windows Event XPath Queries](https://blog.openthreatresearch.com/ossem_generation_xpath_queries)
----

### LogRhythm
1. [Download Kibana Free • Get Started Now | Elastic](https://www.elastic.co/downloads/kibana)
2. [Console - Kibana](http://127.0.0.1:5601/app/kibana#/dev_tools/console?load_from=https:%2F%2Fwww.elastic.co%2Fguide%2Fen%2Felasticsearch%2Freference%2Fcurrent%2Fsnippets%2Fwindows%2F1.json&_g=())
3. [LogRhythm-Labs · GitHub](https://github.com/LogRhythm-Labs)
4. [PowerShell Command Line Logging | LogRhythm](https://logrhythm.com/blog/powershell-command-line-logging/?utm_campaign=Oktopost-Q2+2019+%7C+Global+Social+%7C+Blogs&utm_content=Oktopost-linkedin-&utm_social=TRUE&utm_source=linkedin)
5. [An Overview to Threat Hunting: 7 Common Hunts to Get Started | LogRhythm](https://logrhythm.com/webcasts/an-overview-to-threat-hunting-7-common-hunts-to-help-get-started/)
6. [Custom MPE Rules using Regular Expression Training... - LogRhythm Community](https://community.logrhythm.com/t5/SIEM-articles/Custom-MPE-Rules-using-Regular-Expression-Training-Materials/ta-p/40465)
7. [Regular Expressions (Regex) Cheat Sheet by Dave Ch... - LogRhythm Community](https://community.logrhythm.com/t5/SIEM-articles/Regular-Expressions-Regex-Cheat-Sheet-by-Dave-Child-of/ta-p/40378)
8. [LogRhythm UEBA | UEBA Security | LogRhythm](https://logrhythm.com/products/logrhythm-ueba/?utm_campaign=Oktopost-Q1+2019+%7C+NA+Social+%7C+Use+Case&utm_content=Oktopost-twitter-&utm_social=TRUE&utm_source=twitter)
----

### Splunk
1. [secops4thewin](https://github.com/secops4thewin?tab=repositories)
2. [Box Plots: Making Custom Visualizations](https://www.splunk.com/blog/2016/05/12/box-plots-via-custom-visualizations.html)
3. [dstaulcu](https://github.com/dstaulcu?tab=repositories)
4. [inodee/threathunting-spl](https://github.com/inodee/threathunting-spl/blob/master/spl_tips_tricks.md)
5. [stricaud/TA-misp](https://github.com/stricaud/TA-misp)
6. [Splunk Security Essentials](https://splunkbase.splunk.com/app/3435/)
7. [Splunk ES Content Update](https://splunkbase.splunk.com/app/3449/)
8. [rkovar/splunk-hunting-helpers](https://github.com/rkovar/splunk-hunting-helpers)
9. [inodee/threathunting-spl](https://github.com/inodee/threathunting-spl/blob/master/hunt-queries/Detecting_Beaconing.md)
----

### Sysmon
1. [MHaggis/sysmon-dfir](https://github.com/MHaggis/sysmon-dfir)
2. [olafhartong/sysmon-modular](https://github.com/olafhartong/sysmon-modular)
3. [SwiftOnSecurity/sysmon-config](https://github.com/swiftOnSecurity/sysmon-config)
4. [nshalabi/SysmonTools](https://github.com/nshalabi/SysmonTools)
5. [Sysinternals New Tool Sysmon (System Monitor)](https://www.darkoperator.com/blog/2014/8/8/sysinternals-sysmon)
6. [First.org/resources/papers/conf2017/Advanced-Incident-Detection-and-Threat-Hunting-using-Sysmon-and-Splunk.pdf](https://www.first.org/resources/papers/conf2017/Advanced-Incident-Detection-and-Threat-Hunting-using-Sysmon-and-Splunk.pdf)
7. [darkoperator/Posh-Sysmon](https://github.com/darkoperator/Posh-Sysmon)
8. [SysmonView and SysmonShell](https://nosecurecode.blog)
9. [Sysmon and Neo4j](https://www.malwaresoup.com/sysmon-and-neo4j/)
10. [Detect the undetectable with Sysinternals Sysmon and Powershell logs](https://securitylogsdotorg.files.wordpress.com/2017/06/bsides-athens-sysmon-final.pdf)
----

### NETWORK: Honey Pot
1. [Automated Malware Analysis](http://www.cuckoosandbox.org/)
2. [Bifrozt download | SourceForge.net](http://sourceforge.net/projects/bifrozt/)
3. [Conpot](http://conpot.org/)
4. [foospidy/HoneyPy](http://github.com/foospidy/HoneyPy)
5. [GitHub - desaster/kippo: Kippo - SSH Honeypot](http://github.com/desaster/kippo)
6. [GitHub - dtag-dev-sec/t-pot-autoinstall: Deprecated - Please use T-Pot Universal Installer](http://github.com/dtag-dev-sec/t-pot-autoinstall)
7. [GitHub - paralax/awesome-honeypots: an awesome list of honeypot resources](http://github.com/paralax/awesome-honeypots)
8. [GitHub - tnich/honssh: HonSSH is designed to log all SSH communications between a client and server.](http://github.com/tnich/honssh)
9. [GitHub - zeroq/amun: Amun Honeypot](http://github.com/zeroq/amun)
10. [HoneyDrive honeypot bundle distro](http://bruteforce.gr/honeydrive)
11. [Kojoney - A honeypot for the SSH Service](http://kojoney.sourceforge.net/)
12. [MushMush](http://glastopf.org/)
13. [T-Pot 17.10 - Multi-Honeypot Platform rEvolution](http://dtag-dev-sec.github.io/mediator/feature/2017/11/07/t-pot-17.10.html)
14. [Www.edgis-security.org/honeypot/dionaea/](http://www.edgis-security.org/honeypot/dionaea/)
15. https://github.com/telekom-security/tpotce
----

