### Whois Domain and IP
1. [Censys Search](https://search.censys.io/)
2. [IP and Domain Reputation Center || Cisco Talos Intelligence Group - Comprehensive Threat Intelligence](https://talosintelligence.com/reputation_center)
3. [Comprehensive IP address data, IP geolocation API and database](https://ipinfo.io/)
4. [Whois API | Whois Lookup API | Domain Whois API](https://www.whoxy.com/)
5. [DomainBigData.com - Online investigation tools](https://domainbigdata.com/)
6. [Whois Search - Free Domain Name Lookup - Verisign](https://webwhois.verisign.com/webwhois-ui/index.jsp)
7. [https://www.whois.net/](https://whois.net)
8. [ICANN Lookup](https://lookup.icann.org/)
9. [WHOIS Search, Domain Name, Website, and IP Tools](https://who.is/)
10. [Free online network tools - traceroute, nslookup, dig, whois lookup, ping - IPv6](https://centralops.net/co/)
11. [Whois lookup](https://dnslytics.com/whois-lookup)
12. [Free WHOIS - Domain Name Lookup](https://www.ip2whois.com)
13. [Who actually owns that domain name find out more info](https://www.iptrackeronline.com/whois.php)
14. [Domain / IP Whois](https://viewdns.info/whois/)
15. [Whois](https://www.godaddy.com/whois)
16. [Whois Lookup, Domain Availability & IP Search - DomainTools](https://whois.domaintools.com/)
17. [Whois.com - Domain Names & Identity for Everyone](https://www.whois.com/)
18. [Threat Intelligence Platform](https://threatintelligenceplatform.com/)
----

### IP Geo Location
1. [Comprehensive IP address data, IP geolocation API and database](https://ipinfo.io/)
2. [ipstack - Free IP Geolocation API](https://ipstack.com/)
3. [IP Geolocator - Trace an IP address to city, country, ISP - Geody](https://www.geody.com/geoip.php)
4. [IP Tracker & Tracer ⇒ Find, Track, Trace IP Address Location](https://www.ip-tracker.org/)
5. [GeoIP2 Web Service Demo | MaxMind](https://www.maxmind.com/en/geoip2-precision-demo)
6. [Free IP Geolocation API and Accurate IP Geolocation Database](https://ipgeolocation.io/)
7. [IP Address Locator](https://geobytes.com/iplocator/)
8. [IP Address Geolocation Lookup Demo](https://www.ip2location.com/demo)
9. [Locate IP Address Lookup Show on Map City of the IP 3.250.2.129](https://infosniper.net)
10. [Map IPs - Plot upto 500,000 IPs on a map - IPinfo.io](https://ipinfo.io/tools/map)
----

### Scans
1. [Pentest tools from nmap online to subdomain finder, theHarvester, wappalyzer. Discover dns records of domains, detect cms using cmseek & whatweb](https://www.nmmapper.com/)
2. [MalShare](https://malshare.com/)
3. [MalwareBazaar - Malware sample exchange](https://bazaar.abuse.ch/)
4. [MetaDefender Cloud | Advanced threat prevention and detection](https://metadefender.opswat.com/)
5. [Talos File Reputation Lookup || Cisco Talos Intelligence Group - Comprehensive Threat Intelligence](https://talosintelligence.com/talos_file_reputation)
6. [Wannabrowser](https://www.wannabrowser.net/)
7. [VirusTotal](https://www.virustotal.com/gui/home/upload)
8. [URL2PNG - Screenshots as a Service](https://www.url2png.com/)
9. [Jotti's malware scan](https://virusscan.jotti.org)
10. [gary  Online Virus Scanner](https://www.garyshood.com/virus/)
11. [Kaspersky Threat Intelligence Portal](https://opentip.kaspersky.com/)
12. [Phishcheck 2.0 beta - Home](https://phishcheck.me/)
13. [https://checkphish.ai/](https://checkphish.ai/)
14. [Capture a website screenshot online](https://pikwy.com/)
15. [VirusTotal](https://www.virustotal.com/gui/home/url)
16. [isitPhishing - Anti phishing tools and information](https://isitphishing.org/)
17. [URL and website scanner](https://urlscan.io/)
18. [https://www.hybrid-analysis.com/](https://www.hybrid-analysis.com/)
19. [Visualping](https://visualping.io/)
20. [Analyse your HTTP response headers](https://securityheaders.com/)
----

### Digital Certificate Search
1. [Censys](https://search.censys.io/certificates?q=)
2. [SSL Security Test | Scan Web and Email Server SSL TLS STARTTLS Encryption](https://www.immuniweb.com/ssl/)
3. [crt.sh | Certificate Search](https://crt.sh/)
4. [Qualys SSL Labs - Projects](https://www.ssllabs.com/projects/index.html)
5. [badssl.com](https://badssl.com/)
6. [SSL Certificate Checker - Diagnostic Tool | DigiCert.com](https://www.digicert.com/help/)
7. [Entrust Certificate Search](https://ui.ctsearch.entrust.com/ui/ctsearchui)
----

### Explain
1. [Regexper](https://regexper.com/)
2. [Regex for playground](https://ihateregex.io/playground/)
3. [explainshell.com](https://explainshell.com/)
4. [Codeexplainer.org](https://codeexplainer.org/)
5. [pythontutor.com/visualize.html](https://pythontutor.com/visualize.html#mode=edit)
----

### DFIR
1. [Autopsy](https://www.autopsy.com/)
----

### URL Redirect
1. [URL Redirect Checker](https://wheregoes.com/)
2. [301 Redirect Checker](https://www.sureoak.com/seo-tools/301-redirect-checker)
3. [Redirect Detective - Discover where those redirects really go to](https://redirectdetective.com/)
4. [Bulk URL HTTP Status Code, Header & Redirect Checker](https://httpstatus.io/)
5. [Redirect Checker | Check your Statuscode 301 vs 302](https://www.redirect-checker.org/)
6. [Lookyloo](https://lookyloo.circl.lu/)
----

### URL Expander
1. [Expand Shortened URLs - ExpandURL](https://www.expandurl.net/)
2. [URL Expander](https://urlexpander.net/)
3. [URL Expander — Unshorten Any Short URL To A Long URL](https://urlex.org/)
4. [CheckShortURL - Your shortened URL expander](http://checkshorturl.com/)
5. [Link Expander & Decrypter - Unshorten url in one click!](https://linkexpander.com/)
6. [unfurl](https://dfir.blog/unfurl/)
----

### Email Header Analyzer
1. [Email Header Analyzer](https://www.whatismyip.com/email-header-analyzer/)
2. [Analyze my mail header](https://mailheader.org/)
3. [E-Mail Header Analyzer - Analyze e-mail header lines](https://gaijin.at/en/tools/e-mail-header-analyzer)
4. [Messageheader](https://toolbox.googleapps.com/apps/messageheader/)
5. [Message Header Analyzer](https://mha.azurewebsites.net/)
6. [Complete email header analysis. Analyse, track ip here](https://www.iptrackeronline.com/email-header-analysis.php)
7. [Email Header Analyzer, RFC822 Parser](https://mxtoolbox.com/EmailHeaders.aspx)
----

### Data
1. [Just Get My Data - A directory of direct links for you to obtain your data from web services.](https://justgetmydata.com/)
2. [Just Delete Me | A directory of direct links to delete your account from web services.](https://justdeleteme.xyz/)
----

### CVEs & Exploits Search
1. [Activity Feed](https://attackerkb.com/)
2. [VULNERABILITY LAB - SECURITY VULNERABILITY RESEARCH LABORATORY - Best Independent Bug Bounty Programs, Responsible Disclosure & Vulnerability Coordination Platform - INDEX](https://www.vulnerability-lab.com/)
3. [Exploit Database - Site 1](https://cxsecurity.com/exploit/)
4. [NVD - Search](https://nvd.nist.gov/search)
5. [CVE - CVE](https://cve.mitre.org/)
6. [Shodan Exploits](https://exploits.shodan.io/welcome)
7. [Vulmon - Vulnerability Intelligence Search Engine](https://vulmon.com/)
8. [CVE security vulnerability database. Security vulnerabilities, exploits, references and more](https://www.cvedetails.com/)
9. [Vulners - Vulnerability Data Base](https://vulners.com/search)
10. [Offensive Security’s Exploit Database Archive](https://www.exploit-db.com/)
11. [https://sploitus.com/](https://sploitus.com/)
----

### New widget
1. [de4js | JavaScript Deobfuscator and Unpacker](https://lelinhtinh.github.io/de4js/)
----

### Dummy Data
1. [Test or Dummy dataset generator](https://smalldev.tools/test-data-generator-online)
2. [generatedata.com](https://generatedata.com/)
----

### Web Archiving
1. [See cached version of website](https://stored.website/)
2. [Google Cached Pages of Any Website - CachedView](https://cachedview.com/)
3. [Internet Archive: Digital Library of Free & Borrowable Books, Movies, Music & Wayback Machine](https://archive.org/)
4. [archive.ph](https://archive.today/)
----

### Website Value
1. [Website worth calculator](https://www.webuka.com)
2. [Website value calculator and web information](https://websiteoutlook.com/)
3. [https://www.godaddy.com/domain-value-appraisal/appraisal/](https://www.godaddy.com/domain-value-appraisal/appraisal/)
4. [Calculate Website Traffic Worth, Revenue and Pageviews with SiteWorthTraffic](https://www.siteworthtraffic.com/)
5. [https://www.siteprice.org/](https://www.siteprice.org/)
6. [https://www.worthofweb.com/](https://www.worthofweb.com/)
----

### People Search
1. [Odls](https://locator.ice.gov/odls)
2. [Inmate Locator](https://www.bop.gov/inmateloc/)
3. [The Dru Sjodin National Sex Offender Public Website](https://www.nsopw.gov/)
4. [People Search | People Finder | Skipease](https://www.skipease.com/)
5. [Lullar Com - Search People Profile by Email or Username](https://com.lullar.com/)
6. [Home Page v1](https://start.umd.edu/gtd/)
7. [Free Public Records Search - judyrecords](https://www.judyrecords.com/)
8. [PeekYou - People Search Made Easy](https://peekyou.com/)
9. [Public Records Search | Veripages](https://veripages.com/)
10. [Private Eye - Find a person by name](https://www.privateeye.com/)
11. [https://www.beenverified.com/](https://www.beenverified.com/)
12. [PublicRecordsNOW - Find a person by name](https://www.publicrecordsnow.com/)
13. [Spokeo - People Search | White Pages | Reverse Phone Lookup](https://www.spokeo.com/)
14. [Whitepages](https://www.whitepages.com/)
----

### Data Leak
1. [BrowserLeaks - Web Browser Fingerprinting - Browsing Privacy](https://browserleaks.com/)
2. [tinfoleak | Free dossier of a twitter user](https://tinfoleak.com/)
3. [Firefox Monitor](https://monitor.firefox.com/)
4. [Ashley Madison hacked email checker](https://ashley.cynic.al/)
5. [BreachAlarm has been discontinued](https://breachalarm.com/)
6. [https://leakhispano.net](https://leakhispano.net)
7. [https://haveibeenzucked.com/](https://haveibeenzucked.com/)
8. [Cryptome](https://cryptome.org/)
9. [https://leakpeek.com/](https://leakpeek.com/)
10. [Intelligence X](https://intelx.io/)
11. [Scattered Secrets: check if your password has been compromised in a data breach](https://scatteredsecrets.com/)
12. [WikiLeaks](https://wikileaks.org/)
13. [https://leakedsource.ru/](https://leakedsource.ru/)
14. [Have I Been Sold?](https://haveibeensold.app/)
15. [Have I Been Pwned: Check if your email has been compromised in a data breach](https://haveibeenpwned.com/)
16. [Leakcheck.net](https://leakcheck.net/)
----

### Remote Jobs
1. [Hired](https://hired.com/)
2. [FlexJobs](https://www.flexjobs.com/)
3. [Jobspresso](https://jobspresso.co/)
4. [remoteok.com](https://remoteok.com/)
5. [JustRemote](https://justremote.co/)
6. [Jsremotely.com](https://jsremotely.com/)
7. [Daily Remote](https://dailyremote.com/)
8. [Remote Leaf](https://remoteleaf.com/)
9. [Remote Work Hub](https://remoteworkhub.com/)
10. [RemoteLeads](https://remoteleads.io/)
11. [Remote Jobs](https://www.workingnomads.com/jobs)
----

### Outages
1. [PowerOutage.US](https://poweroutage.us)
2. [Is it Down - Check website status from this USA based server, you can confirm if a website is down or have any error](https://isitdown.us/)
3. [Is it down? Check at Down for Everyone or Just Me](https://downforeveryoneorjustme.com/)
4. [Status overview](https://downdetector.com/)
5. [Helps you find whether the website you are trying to browse is down or not. Check if the website is down just for you or everyone around the globe.](https://www.isitdownrightnow.com/)
6. [Internet Outages Map](https://www.thousandeyes.com/outages/)
----

### Phone Lookup
1. [Phone Lookup - Reverse Phone Lookups @ PhoneLookup.com](https://www.phonelookup.com/)
2. [Internet 800 Directory - Directory of free toll free (tollfree), 800,866,877 and 888 listings of businesses by company, number and type of industry, regardless of long distance carrier.](https://inter800.com)
3. [Reverse Phone Number Lookup | Phone Number Search | ZLOOKUP](https://www.zlookup.com/)
4. [https://nuwber.com/](https://nuwber.com/)
5. [https://www.whocalledme.com/](https://www.whocalledme.com/)
6. [Reverse Phone Lookup | Phone Number Search | ThatsThem](https://thatsthem.com/reverse-phone-lookup)
7. [Phone Validator ✔️ | Is it a Cell or is it a Landline?](https://www.phonevalidator.com)
8. [Welcome to FreeCarrierLookup.com](https://freecarrierlookup.com/)
9. [Who called me from this phone number? You May find out with a Reverse Phone Lookup. Enter a number to discover a name, address, and more details about a phone number owner.](https://www.reversephonelookup.com/)
10. [Main Page | shouldianswer.net](https://www.shouldianswer.net/)
11. [https://www.numberguru.com/](https://www.numberguru.com/)
----

### Metadata
1. [FotoForensics](https://fotoforensics.com/)
2. [View Exif data online, remove Exif online](http://www.verexif.com/en/)
3. [online metadata and exif viewer](http://metapicz.com/#landing)
4. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
5. [Online exif data viewer](https://www.metadata2go.com/)
----

### Proxy
1. [ProxySite.Site | Free Web Proxy Site to Unblock Blocked Sites](https://proxysite.site/)
2. [The Fastest Free Proxy](https://hide.me/en/proxy)
3. [ProxySite.com - Free Web Proxy Site](https://www.proxysite.com/)
4. [KPROXY - Free Anonymous Web Proxy - Anonymous Proxy](https://kproxy.com/)
----

### Career
1. [Cybersecurity Career Pathway](https://www.cyberseek.org/pathway.html)
2. [CYBERSECURITY JOB HUNTING GUIDE](https://cyberhuntingguide.weebly.com/)
----

### Capture The Flag
1. [Hacktoria - OSINT, OPSEC and INFOSEC. CTF Events & Information Security.](https://hacktoria.com/)
2. [iCTF - Where Intel is Everything](https://ictf.io/)
3. [GeoGuessr - Let's explore the world!](https://www.geoguessr.com/)
4. [CyberSoc | Cyber Detective CTF](https://ctf.cybersoc.wales/)
5. [HTB Academy : Cyber Security Training](https://academy.hackthebox.eu/)
6. [Capture the Flag](https://ctf.hackchallengesforkids.com/)
7. [CyberDefenders: Blue Team CTF Challenges](https://cyberdefenders.org/)
8. [Vulnerable By Design ~ VulnHub](https://www.vulnhub.com/)
9. [Hacking Training For The Best](https://www.hackthebox.eu/)
10. [https://hack.me/](https://hack.me/)
11. [TryHackMe | Cyber Security Training](https://tryhackme.com/)
12. [ACI CTF](https://acictf.com/)
13. [picoCTF - CMU Cybersecurity Competition](https://picoctf.org/)
----

### Web Search Engines
1. [Yandex](https://yandex.com/)
2. [Infospace](https://infospace.com/)
3. [Web Search - Exalead](https://www.exalead.com/search/web/)
4. [Gigablast - An Alternative Web Search Engine](https://gigablast.com)
5. [Entireweb - Web Search Engine](https://entireweb.com/)
6. [DuckDuckGo — Privacy, simplified.](https://duckduckgo.com)
7. [百度一下，你就知道](https://baidu.com)
----

### Blog Search
1. [Blog Search Engine](https://www.blogsearchengine.com/)
2. [Blog Search](https://www.blog-search.com/)
3. [Bloghub.com - Search at blog directory Bloghub using our blog search engine tool. - blog directory & blog search engine](http://www.bloghub.com/)
4. [Free Business Listing - Business Directory](https://blogs.botw.org/)
5. [Blogging Fusion - Blog Directory - Article Directory - Web Directory](https://www.bloggingfusion.com/)
6. [Blog Search Engine](http://www.blogsearchengine.org/)
7. [Blogspot Blog Search](https://www.searchblogspot.com/)
----

### Plagiarism Checker
1. [Plagiarism Checker Free | Accurate with Percentage](https://plagiarismdetector.net/)
2. [Plagiarism Checker - 100% Free Online Plagiarism Detector](https://smallseotools.com/plagiarism-checker/)
3. [Plagiarism Checker by Grammarly](https://www.grammarly.com/plagiarism-checker)
4. [Original Writing, Made Easy | Quetext](https://www.quetext.com/)
5. [Free Online Plagiarism Checker - Check Plagiarism](https://www.check-plagiarism.com/)
6. [Copyscape Plagiarism Checker - Duplicate Content Detection Software](https://www.copyscape.com/)
----

### Emails
1. [Tools Epieos](https://tools.epieos.com/)
2. [Hunter](https://hunter.io/)
3. [Trace Email Address Source](https://whatismyipaddress.com/trace-email)
4. [Verify Email Address Online](https://verify-email.org/)
5. [Email Health Check](https://mxtoolbox.com/emailhealth)
6. [Free Email Address Search](https://email.addresssearch.com/)
7. [Whoisology.com](https://whoisology.com/email/)
8. [People search by email](https://www.cyberbackgroundchecks.com/email)
9. [Leads search](https://omail.io/leads)
10. [Lead Crawler](https://leadcrawler.io/)
11. [Email Dossier](https://centralops.net/co/EmailDossier.aspx)
12. [#1 FREE Email Validation & Verification Tool](https://b2bsprouts.com/email-validation-tool/)
13. [Didtheyreadit.com](http://www.didtheyreadit.com/)
14. [Email Address Verifier](https://tools.emailhippo.com/)
15. [Email Blacklist Check](https://mxtoolbox.com/blacklists.aspx)
16. [Lullar Com](https://com.lullar.com/)
17. [Email Sherlock](https://www.emailsherlock.com/)
18. [FREE Email Search](https://www.spytox.com/email-search)
19. [Find People for Free Using an Email Address](https://thatsthem.com/reverse-email-lookup)
20. [Find emails addresses and social profiles in seconds](https://www.orbitly.io/)
21. [Spokeo](https://www.spokeo.com/email-search)
22. [Intel X Email Tool](https://intelx.io/tools?tab=email)
23. [Peepmail](http://samy.pl/peepmail/)
24. [Find Emails](https://www.findemails.com/)
25. [email checker: discover social networks from an email address](https://www.manycontacts.com/en/mail-check)
26. [Namecombiner.com](https://namecombiner.com/)
27. [EMAIL-FINDER](http://publicemailrecords.com/)
28. [Scattered Secrets](https://scatteredsecrets.com/)
29. [WhiteHatInspector/emailGuesser](https://github.com/WhiteHatInspector/emailGuesser)
30. [Vedbex: Email to Skype](https://www.vedbex.com/email2skype)
31. [A list of all email provider domains (free, paid, blacklist etc). Some of these are probably not around anymore. I've combined a dozen lists from around the web. Current "major providers" should all be in here as of the date this is created.](https://gist.github.com/ammarshah/f5c2624d767f91a7cbdc4e54db8dd0bf)
32. [Inteltechniques Email Tool](https://inteltechniques.com/tools/Email.html)
33. [Leakpeek.com](https://leakpeek.com/)
34. [Synapsint](https://synapsint.com/#mail)
35. [Gravatar - Globally Recognized Avatars](http://en.gravatar.com/site/check)
36. [Epieos - Empowering the human factor in cybersecurity](https://tools.epieos.com/email.php)
37. [https://emailrep.io/](https://emailrep.io/)
----

### OSINT Analysis Tools
1. [Free online timeline maker](https://time.graphics/)
----

### You want more?
1. [cipher387](https://github.com/cipher387)
2. [OSINT INCEPTION 🔍](https://start.me/p/Pwy0X4/osint-inception)
3. [All of the Best Links and Resources on Cyber Security.](https://s0cm0nkey.gitbook.io/s0cm0nkeys-security-reference-guide/)
4. [OH SHINT! Welcome Aboard](https://ohshint.gitbook.io/oh-shint-its-a-blog/)
5. [HackTricks](https://book.hacktricks.xyz/welcome/readme)
----

