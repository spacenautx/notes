### Frequently Used
1. [Negative News String](https://www.google.com/search?q=%22ENTITYNAME%22+AND+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&oq=%22ENTITYNAME%22+AND+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&aqs=chrome..69i57.113385j0j7&sourceid=chrome&ie=UTF-8)
2. [LexisNexis AML Insight](https://amlinsight.lexisnexis.com/)
3. [Google Translate](https://translate.google.com/)
4. [Country Code List](https://www.nationsonline.org/oneworld/country_code_list.htm)
5. [SWIFT Code Lookup](https://www2.swift.com/bsl/facelets/bicsearch.faces)
6. [Wayback Machine](https://archive.org/web/)
7. [SearchOpener](http://www.searchopener.com/)
----

### Arms Trade
1. [Small Arms Survey Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
2. [SIPRI Trade Register](http://armstrade.sipri.org/armstrade/page/trade_register.php)
3. [Arms Embargoes](https://www.sipri.org/databases/embargoes)
4. [Military Ranks Wiki](https://en.wikipedia.org/wiki/List_of_comparative_military_ranks#United_States)
----

### Banking/Finance
1. [LexisNexis AMLInsight](https://amlinsight.lexisnexis.com/)
2. [Negative News String](https://www.google.com/search?q=%22ENTITYNAME%22+AND+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&oq=%22ENTITYNAME%22+AND+~arrest+OR+bankruptcy+OR+BSA+OR+~conviction+OR+~criminal+OR+fraud+OR+~lawsuit+OR+money%2Blaunder+OR+OFAC+OR+Ponzi+OR+~terrorist+OR+~violation+OR+~%22Honorary+Consul%22+OR+%22consul%22+OR+%22Panama+Papers%22&aqs=chrome..69i57.113385j0j7&sourceid=chrome&ie=UTF-8)
3. [SWIFT Code Lookup](https://www2.swift.com/bsl/facelets/bicsearch.faces)
4. [SWIFT Message Standards](https://www2.swift.com/knowledgecentre/publications/usgf_20180720/3.0?topic=mt202cov-5-field-52a.htm)
5. [Country Codes List](https://www.nationsonline.org/oneworld/country_code_list.htm)
6. [Investopedia Search String](https://www.google.com/search?ei=SNqfW4D8NYWzjwSZ7aToBw&q=%22XX%22+AND+site%3Ahttps%3A%2F%2Fwww.investopedia.com&oq=%22XX%22+AND+site%3Ahttps%3A%2F%2Fwww.investopedia.com&gs_l=psy-ab.3...3002.11305..12640...0.0..0.84.629.8......0....1j2..gws-wiz.......0i71.DQD05M7P3vA)
7. [ABA Routing Number Search](https://routingnumber.aba.com/Search1.aspx)
8. [FFIEC Records](https://cdr.ffiec.gov/public/ManageFacsimiles.aspx)
9. [Know Your Country](https://www.knowyourcountry.com/copy-of-country-reports)
10. [Bank Lists](https://en.wikipedia.org/wiki/Lists_of_banks)
11. [FDIC BankFind](https://research.fdic.gov/bankfind/)
12. [Fraud Red Flags](https://www.osc.state.ny.us/localgov/pubs/red_flags_fraud.pdf)
13. [Currency Converter (by Date)](https://www.oanda.com/currency/converter/)
14. [Binlist](https://binlist.net/)
15. [Currencies List](https://en.wikipedia.org/wiki/List_of_circulating_currencies)
16. [Financial Secrecy Index](https://www.financialsecrecyindex.com/introduction/fsi-2018-results)
----

### Bribery/Corruption
1. [Corruption Perceptions Index](https://www.transparency.org/news/feature/corruption_perceptions_index_2017)
2. [Transparency International](https://www.transparency.org/)
3. [Kleptocracy Initiative](http://kleptocracyinitiative.org/)
4. [Global Witness](https://www.globalwitness.org/en/)
5. [The Sentry](https://thesentry.org)
6. [Lava Jota (Brazilian "Car Wash")](http://www.jota.info/lavajota/)
7. [OCED Country Resources](https://www.oecd.org/corruption/bycountry/)
8. [FARA Search](https://efile.fara.gov/pls/apex/f?p=185:1:::::P1_DISPLAY)
9. [Foreign Corrupt Practices Act](https://www.justice.gov/sites/default/files/criminal-fraud/legacy/2012/11/14/fcpa-english.pdf)
10. [Lobby View](https://www.lobbyview.org/query#!/)
11. [Stolen Asset Recovery Initiative](https://star.worldbank.org/corruption-cases/?db=All)
12. [SWAMP Index](http://swamp.coalitionforintegrity.org/)
13. [Facebook Ad Archive](https://www.facebook.com/ads/archive/?active_status=all&ad_type=political_and_issue_ads&country=US)
14. [OCCRP](https://www.occrp.org/en/)
15. [CIA World Leaders List](https://www.cia.gov/library/publications/resources/world-leaders-1/index.html)
16. [Trace Bribery Risk Matrix](https://www.traceinternational.org/trace-matrix#189)
17. [DS Giving](http://dsgiving.com/)
18. [Resources for Development Consulting](http://www.res4dev.com/)
----

### Charities
1. [IRS Charities Search](https://apps.irs.gov/app/eos/)
2. [Charity Navigator](https://www.charitynavigator.org/)
3. [GuideStar](www.guidestar.org/search)
----

### Investment
1. [SEC EDGAR](https://www.sec.gov/edgar/searchedgar/companysearch.html)
2. [SEC Investment Advisor Lookup](https://adviserinfo.sec.gov/)
3. [FINRA Broker Check](https://brokercheck.finra.org/)
4. [Yahoo! Finance](https://finance.yahoo.com/quote/%5EDJI?p=^DJI&.tsrc=fin-srch)
5. [SEC Enforcement](https://www.sec.gov/page/litigation)
6. [FINRA Disciplinary Action Search](https://www.finra.org/rules-guidance/oversight-enforcement/finra-disciplinary-actions-online)
----

### Companies
1. [OpenCorporates](https://opencorporates.com/)
2. [List-Org (Russian Companies)](https://www.list-org.com/)
3. [Qichacha (Chinese Companies)](https://www.qichacha.com/)
4. [Companies House (UK Companies)](https://beta.companieshouse.gov.uk/)
5. [EU National Registries](https://e-justice.europa.eu/content_business_registers_in_member_states-106-de-en.do?member=1)
6. [ICIJ Offshore Leaks Database String](https://www.google.com/search?q=%22XX%22+AND+site%3Ahttps%3A%2F%2Foffshoreleaks.icij.org&oq=%22XX%22+AND+site%3Ahttps%3A%2F%2Foffshoreleaks.icij.org&aqs=chrome..69i57.13022j0j7&sourceid=chrome&ie=UTF-8)
7. [Offshore Jurisdictions](https://ioserv.com/en/jurisdictions/jurisdlist/)
8. [Global Incorporation Guide](https://www.lowtax.net/g/jurisdictions/)
9. [Legal Entity Types by Country](https://en.wikipedia.org/wiki/List_of_legal_entity_types_by_country)
10. [FinCEN MSB Registrant Search](https://www.fincen.gov/msb-registrant-search)
11. [SEC Company Search](https://www.sec.gov/edgar/searchedgar/companysearch.html)
12. [NAICS Code Search](https://www.census.gov/cgi-bin/sssd/naics/naicsrch)
13. [Fortune Global 500](http://fortune.com/fortune500/)
14. [Ripoff Report](https://www.ripoffreport.com/)
----

### Country Risk
1. [BBC Country Profiles](http://news.bbc.co.uk/2/hi/country_profiles/default.stm)
2. [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
3. [ACLED (Conflict Data)](https://www.acleddata.com/)
4. [Travel Risk Map | US State Department](https://travelmaps.state.gov/TSGMap/)
5. [LiveUA Map](https://liveuamap.com/)
6. [Control Risks Maps](https://www.controlrisks.com/riskmap-2018/maps)
----

### Cryptocurrency
1. [Coinmonks: Blockchain Tracing](https://medium.com/coinmonks/tracing-an-offshore-bank-and-a-dark-web-service-using-the-blockchain-an-osint-investigation-a1000251c3ec)
2. [Top 100 Bitcoin Wallets](https://bitinfocharts.com/top-100-richest-bitcoin-addresses.html)
3. [Blockchain Explorer](https://www.blockchain.com/explorer)
4. [WalletExplorer](https://www.walletexplorer.com/)
5. [Bitcoin Who's Who](https://bitcoinwhoswho.com/)
----

### Datasets
1. [Google Dataset Search](https://toolbox.google.com/datasetsearch)
2. [World Bank Open Data](https://data.worldbank.org)
3. [State Court Records](https://docs.google.com/document/d/1DHHv7GS6mycat97RTzlZDkokPZcm3QpoJgQ0W_QqaiY/edit#)
4. [The Law Pages](https://www.thelawpages.com/criminal-offence/#)
5. [Financial Crime Stats](https://utica.libguides.com/c.php?g=203170&p=3771160)
6. [GSA Contractor List](https://www.gsaelibrary.gsa.gov/ElibMain/contractorList.do?contractorListFor=A)
7. [OCCRP Data](https://data.occrp.org)
8. [OCCRP Investigative Dashboard](https://investigativedashboard.org/databases/global/)
----

### Drug Trade
1. [International Narcotics Control Strategy Report](https://www.state.gov/j/inl/rls/nrcrpt/2017/)
2. [DEA Drug Slang Terms](https://ndews.umd.edu/sites/ndews.umd.edu/files/dea-drug-slang-terms-and-code-words-july2018.pdf)
3. [NIH Commonly Abused Drugs](https://www.drugabuse.gov/drugs-abuse/commonly-abused-drugs-charts#pcp)
4. [UN International Drug Prices](https://data.unodc.org/)
5. [InSight Crime](https://www.insightcrime.org/)
6. [US Gangs List](https://en.wikipedia.org/wiki/List_of_gangs_in_the_United_States)
7. [Illicit Drugs World Factbook](https://www.cia.gov/library/publications/the-world-factbook/fields/2086.html#rs)
8. [DEA Wanted Fugitives](https://www.dea.gov/fugitives/all)
9. [DEA Drug Images](https://www.dea.gov/media-gallery/drug-Images)
----

### Flight/Maritime
1. [FAA Registry](https://registry.faa.gov/aircraftinquiry/)
2. [Passenger Airlines Wiki](https://en.wikipedia.org/wiki/List_of_passenger_airlines#List_of_passenger_airlines)
3. [Flight Tracker](https://global.adsbexchange.com/VirtualRadar/desktop.html)
4. [AirNav RadarBox](https://www.radarbox24.com/flight/AA2097#)
5. [Marine Traffic](https://www.marinetraffic.com/en/ais/home/centerx:4.1/centery:40.2/zoom:7)
6. [BoatInfoWorld.com - Search, View &amp; Download Vessel Information](https://www.boatinfoworld.com/)
7. [IOC Live Piracy Map](https://www.icc-ccs.org/piracy-reporting-centre/live-piracy-map)
8. [Flags of Convenience](https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/List_of_flags_of_convenience.html)
----

### Human Trafficking
1. [Trafficking in Persons Report](https://www.state.gov/j/tip/rls/tiprpt/index.htm)
2. [Polaris Project](https://polarisproject.org/sites/default/files/A%20Roadmap%20for%20Systems%20and%20Industries%20to%20Prevent%20and%20Disrupt%20Human%20Trafficking%20-%20Financial%20Industry.pdf)
3. [Mixed Migration Centre](http://www.mixedmigration.org/)
4. [Migration Policy Data](https://www.migrationpolicy.org/programs/migration-data-hub)
5. [Financial Flows from Human Trafficking](https://www.fatf-gafi.org/media/fatf/content/images/Human-Trafficking-2018.pdf)
6. [Global Modern Slavery Directory](http://www.globalmodernslavery.org/)
7. [Human Trafficking Flow Map](http://dataviz.du.edu/projects/htc/flow/)
----

### Map/GEOINT
1. [Google Maps](https://www.google.com/maps)
2. [OpenStreetMap](https://www.openstreetmap.org/#map=5/38.007/-95.844)
3. [Bing Maps](https://www.bing.com/maps?FORM=Z9LH2)
4. [Wikimapia](http://wikimapia.org/#lang=en&lat=39.977120&lon=140.822754&z=5&m=w)
5. [Sentinel Hub](https://www.sentinel-hub.com/)
6. [OpenStreetCam](https://www.openstreetcam.org/map/)
7. [2GIS (Dubai Businesses)](https://2gis.ae/dubai)
8. [Google Maps Dev. Guide](https://developers.google.com/maps/documentation/urls/guide)
9. [Strava Heatmap](https://www.strava.com/heatmap)
10. [CIA Maps](https://www.cia.gov/library/publications/the-world-factbook/docs/refmaps.html)
11. [Asia Map](https://www.cia.gov/library/publications/the-world-factbook/graphics/ref_maps/political/pdf/asia.pdf)
12. [China Administrative Map](https://www.cia.gov/library/publications/resources/cia-maps-publications/map-downloads/China_Admin.pdf)
13. [Japan Prefectures](https://upload.wikimedia.org/wikipedia/commons/b/bc/Regions_and_Prefectures_of_Japan_2.svg)
14. [Japanese Addresses](https://resources.realestate.co.jp/living/how-to-read-a-japanese-address/)
15. [Tri-Border Area](https://apps.cndls.georgetown.edu/projects/borders/archive/files/80516cb7cf546c84e7385944978ab4cd.jpg)
16. [Central America/Caribbean Map](https://www.cia.gov/library/publications/the-world-factbook/graphics/ref_maps/political/pdf/central_america.pdf)
----

### NGO Links
1. [C4ADS](https://c4ads.org)
2. [Centre for Financial Crime & Security Studies](https://rusi.org/expertise/research/centre-financial-crime-and-security-studies)
3. [FDD Center on Sanctions and Illicit Finance](http://www.defenddemocracy.org/csif)
4. [ICIJ](https://www.icij.org/)
5. [Project Follow](http://www.projectfollow.org)
6. [ICLG](https://iclg.com/practice-areas/anti-money-laundering-laws-and-regulations)
----

### OSINT
1. [Bellingcat Digital Toolkit](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
2. [Reuser OSINT Tools](http://rr.reuser.biz/#company%20informationInternational)
3. [Bellingcat Thread](https://twitter.com/trbrtc/status/895734898647945220?lang=en)
4. [Awesome OSINT](https://github.com/Ph055a/awesome_osint)
5. [The OSINT Podcast](https://www.buzzsprout.com/209598)
6. [Bellingcat Resources](https://www.bellingcat.com/category/resources/)
7. [Creps Social Media Intel](https://start.me/p/m6MbeM/social-media-intelligence-dashboard)
8. [Terrorism & Radicalization](https://start.me/p/OmExgb/terrorism-radicalisation-research-dashboard)
9. [Social Media Intelligence](https://start.me/p/m6MbeM/social-media-intelligence-dashboard)
10. [Bruno Mortier > Sources](https://start.me/p/3g0aKK/sources)
11. [Technisette > Tools](https://start.me/p/wMdQMQ/tools)
12. [TDiDdy > OSINT](https://start.me/p/b56xX8/osint)
13. [Jk Priv > AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
14. [TI](https://start.me/p/rxRbpo/ti)
----

### Terrorism
1. [UMD Start](https://www.start.umd.edu/gtd/)
2. [Islamism Map](https://islamism-map.com/#/)
3. [GWU Project on Extremism](https://extremism.gwu.edu/)
4. [Mapping Militant Organizations](http://web.stanford.edu/group/mappingmilitants/cgi-bin/)
5. [Crime Terror Nexus](https://crimeterrornexus.com/)
6. [SPLC Hate Map](https://www.splcenter.org/hate-map)
----

### Press
1. [BBC Media Profiles](https://www.google.com/search?ei=KhTKW5qcD6zHjwTC_aUQ&q=intitle%3A%22COUNTRYNAME%22+AND+intitle%3A%22profile+-+Media+-+BBC+News%22&oq=intitle%3A%22COUNTRYNAME%22+AND+intitle%3A%22profile+-+Media+-+BBC+News%22&gs_l=psy-ab.3...6293.8181..14020...0.0..0.86.896.11......0....1..gws-wiz.oHO_CeGB42g)
2. [Google Alerts](https://www.google.com/alerts)
3. [Press Reader (Global Newspapers)](https://www.pressreader.com/catalog)
4. [vTuner Radio](http://vtuner.com/setupapp/guide/asp/BrowseStations/StartPage.asp?sBrowseType=Location)
----

### Regulatory Links
1. [OFAC Sanctions Search](https://sanctionssearch.ofac.treas.gov/)
2. [FFIEC Manual](https://bsaaml.ffiec.gov/docs/manual/BSA_AML_Man_2014_v2_CDDBO.pdf)
3. [MSB Examination Manual](https://www.fincen.gov/sites/default/files/shared/MSB_Exam_Manual.pdf)
4. [Bank Secrecy Act](https://www.fincen.gov/resources/statutes-regulations/fincens-mandate-congress)
5. [USA PATRIOT Act](https://www.aclu.org/sites/all/libraries/pdf.js/web/viewer.html?file=https%3A%2F%2Fwww.aclu.org%2Fsites%2Fdefault%2Ffiles%2Ffield_document%2Fpatriot_text.pdf)
6. [FinCEN 311 List](https://www.fincen.gov/resources/statutes-and-regulations/311-special-measures)
7. [Real Estate GTO](https://www.fincen.gov/sites/default/files/shared/Real%20Estate%20GTO%20Order%20-%208.22.17%20Final%20for%20execution%20-%20Generic.pdf)
8. [Financial Action Task Force](http://www.fatf-gafi.org/)
9. [UK Serious Fraud Office](https://www.sfo.gov.uk/)
10. [FinCEN Enforcement Actions](https://www.fincen.gov/news-room/enforcement-actions)
11. [NY DFS Enforcement Actions](https://www.dfs.ny.gov/industry_guidance/enforcement_actions_lfs)
----

### Search/Internet
1. [Wayback Machine](https://archive.org/web/)
2. [DuckDuckGo (Private Search Engine)](https://duckduckgo.com/)
3. [Whois (IP Information)](https://www.whois.net/)
4. [PDF My URL](http://pdfmyurl.com)
5. [Alexa Top 500](https://www.alexa.com/topsites)
6. [Google Trends](https://trends.google.com/trends/?geo=US)
7. [Google Search Operators](https://ahrefs.com/blog/google-advanced-search-operators/)
8. [Mastering Google Operators](https://moz.com/blog/mastering-google-search-operators-in-67-steps)
9. [Fake Name Generator](https://www.fakenamegenerator.com/)
10. [Is This Website Safe](https://safeweb.norton.com)
11. [Numberway](https://www.numberway.com)
12. [De-Hashed](https://www.dehashed.com/)
13. [Have I been pwned?](https://haveibeenpwned.com/)
14. [Email Address Verification](https://tools.verifyemailaddress.io/)
15. [PenTest Tools](https://pentest-tools.com/home)
16. [Site Map Crawler](https://www.xml-sitemaps.com/)
17. [ccTLD List](https://icannwiki.org/Country_code_top-level_domain#Current_ccTLDs)
18. [Constant Contact Dork](https://www.google.com/search?ei=tpuuW83xJaqUjwTB_6zoAw&q=%22XX%22+AND+site%3A*.constantcontact.com&oq=%22XX%22+AND+site%3A*.constantcontact.com&gs_l=psy-ab.3...3118.13595..16539...1.0..0.105.1567.18j1......0....1..gws-wiz.wZK5_0lx1Oo)
19. [IP Lookup](https://whatismyipaddress.com/ip-lookup)
20. [SearchOpener](http://www.searchopener.com/)
21. [Donotlink.it](https://donotlink.it/)
22. [CuteStat](https://www.cutestat.com/)
23. [Yandex Image Search](https://yandex.com/images/)
----

### Social Media
1. [Twitter Advanced Search](https://twitter.com/search-advanced?lang=en)
2. [TweetDeck](https://tweetdeck.twitter.com/)
3. [Node XL](https://www.smrfoundation.org/nodexl/)
4. [Spoonbill](http://spoonbill.io/)
----

### Financial Crimes News
1. [ACAMS Free Webinars](https://www.acams.org/aml-resources/training/)
2. [ACFE Fraud Talk (Podcast)](http://www.acfe.com/podcast.aspx#paging:number=25|paging:currentPage=0|date-filter:path=default|sort:path~type~order=.date~datetime~desc)
3. [AML Conversations (Podcast)](https://soundcloud.com/user-45942002/tracks)
4. [Bribe, Swindle or Steal (Podcast)](http://www.traceinternational.org/bribe_swindle_or_steal)
5. [FT Banking Weekly (Podcast)](https://www.ft.com/banking-weekly-podcast)
6. [SanctionLaw Blog](https://sanctionlaw.com/blog/)
7. [WSJ Risk and Compliance Journal](https://www.wsj.com/news/risk-compliance-journal)
8. [Taxcast](http://www.tackletaxhavens.com/taxcast/)
----

### Trade
1. [Download Trade Finance Guide](http://build.export.gov/build/idcplg?IdcService=DOWNLOAD_PUBLIC_FILE&RevisionSelectionMethod=Latest&dDocName=eg_main_043219)
2. [Export Documents](https://2016.export.gov/logistics/eg_main_018121.asp)
3. [Commodities Prices](https://www.bloomberg.com/markets/commodities)
4. [Remittance Map](http://www.pewglobal.org/interactives/remittance-map/)
5. [Container Tracking](https://www.track-trace.com/container)
6. [HS Code Lookup](https://www.freightos.com/freight-resources/harmonized-system-code-finder-hs-code-lookup/)
7. [Seaport Codes](https://www.worldnetlogistics.com/seaport-codes/)
8. [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
9. [Logistics Glossary](https://www.google.com/search?rlz=1C1GCEU_enUS821US821&ei=z4IuXN3pMYKMjwT6y5aYCg&q=inurl%3ASEARCHTERM+AND+site%3Ahttps%3A%2F%2Fwww.logisticsglossary.com%2F&oq=inurl%3ASEARCHTERM+AND+site%3Ahttps%3A%2F%2Fwww.logisticsglossary.com%2F&gs_l=psy-ab.12...1771.10363..12313...3.0..0.98.1419.16......0....1..gws-wiz.9Oz_2c3oAv0)
10. [Observatory of Economic Complexity](https://atlas.media.mit.edu/en/rankings/country/eci/)
11. [Country Commercial Guides](https://www.export.gov/ccg)
----

### Translation
1. [Google Translate](https://translate.google.com/)
2. [Ethnologue](https://www.ethnologue.com/)
3. [Regional Naming Conventions](https://www.bankersonline.com/sites/default/files/tools/namingconventions_0.pdf)
----

### Visualization
1. [Draw.io](https://www.draw.io/)
2. [Datawrapper](https://www.datawrapper.de/)
3. [Kibana](https://www.elastic.co/products/kibana)
4. [Data Viz Catalogue](https://datavizcatalogue.com/)
5. [RAWGraphs](http://app.rawgraphs.io/)
6. [Visual Investigative Scenarios](https://vis.occrp.org/)
----

