### Toolsammlungen und Tools
1. [IntelTechniques OSINT Online Search Tool](https://inteltechniques.com/tools/)
2. [OSINTtechniques Toolsammlung](https://www.osinttechniques.com/osint-tools.html)
3. [OSINTcombine Toolsammlung](https://www.osintcombine.com/tools)
4. [Intelligence X Toolsammlung](https://intelx.io/tools)
5. [OSINTgeek | Tools](https://osintgeek.de/tools)
6. [Technisette Toolsammlung](https://www.technisette.com/p/tools)
7. [Bellingcat's Online Investigation Toolkit [bit.ly/bcattools]](https://bit.ly/bcattools)
8. [NirSoft](https://www.nirsoft.net/)
9. [OSIRT](http://osirtbrowser.com/)
10. [KitPloit](https://www.kitploit.com/)
11. [OsintDojo Collection of tools and methods](https://github.com/sinwindie/OSINT)
12. [List of OSINT Tools and Developers](https://twitter.com/cyb_detective/status/1436239412959096833)
13. [TCM Open-Source-Intelligence-Resources](https://github.com/TCM-Course-Resources/Open-Source-Intellingence-Resources)
14. [A collection of several hundred online tools for OSINT](https://github.com/cipher387/osint_stuff_tool_collection)
15. [Start,me OSINT Toolsammlungen](https://google.de/?q=site%3Astart.me+OSINT)
16. [OSINT Framework](https://osintframework.com/)
17. [Random Comment Picker](https://commentpicker.com/)
----

### Infoseiten
1. [We are OSINTCurio.us](https://osintcurio.us/)
2. [OSINT Techniques](https://www.osinttechniques.com/)
3. [Sector035](https://sector035.nl/)
4. [NixIntel](https://nixintel.info/)
5. [Key Findings](https://keyfindings.blog/)
6. [So geht Onlinerecherche 2.0: Nützliche Tools und Strategien](https://rechercheseminar.de/tools/#1556993933126-77711e7c-b985)
7. [LORÁND BODÓ](https://www.lorandbodo.com/)
8. [osintme.com](https://www.osintme.com/)
9. [OSINT secjuice.com](https://www.secjuice.com/tag/osint/)
10. [German Open Source Intelligence Conference](https://gosintcon.de/)
11. [OSINT Framework -- Mindmap+Links](https://osintframework.com)
12. [osint.tools – @uk_onlineops](https://osint.tools/)
13. [SearchReSearch](http://searchresearch1.blogspot.com/)
14. [Week in OSINT – Medium](https://medium.com/week-in-osint)
15. [Recommended OSINT Tools and Processes](https://os2int.com/toolbox/)
16. [The Ultimate OSINT Collection](https://start.me/p/DPYPMz/the-ultimate-osint-collection)
17. [The OSINT Library – Blockint](https://www.blockint.nl/the-osint-library/)
18. [OSINT Tutorials](https://www.aware-online.com/en/osint-tutorials/)
19. [OSINT Map](https://map.malfrats.industries/)
20. [Osintnewsletter.com](https://osintnewsletter.com)
21. [Online-Recherche Newsletter Archiv](https://ornarchiv.wordpress.com/)
22. [Verification Handbook](https://datajournalism-com.translate.goog/read/handbook/verification-1?_x_tr_sl=auto&_x_tr_tl=de&_x_tr_hl=de)
----

### OSINT Youtube Kanäle
1. [Bashinho - OSINT für Einsteiger](https://www.youtube.com/playlist?list=PL1GvaRufndL17faftlKLsa641ejjjWC1o)
2. [10 Minute (or less!) OSINT Tips](https://www.youtube.com/playlist?list=PL423I_gHbWUUOs09899rex4t2l5py9YIk)
3. [German Open Source Intelligence Conference](https://www.youtube.com/channel/UCLOAQxy9yvISdRfdLZzUOAA)
4. [conINT](https://www.youtube.com/channel/UCBtSOceclpKcvunVNw82tFQ)
5. [OSINT Dojo - YouTube](https://www.youtube.com/channel/UChbp7r-Lezl1CBNQWBDYGeQ)
6. [Bendobrown](https://www.youtube.com/channel/UCW2WOgSiMr216a27KWG_aqg)
7. [OSINTGeek](https://www.youtube.com/channel/UC3_tqpI-67B5gRWyGd5y47w)
8. [Layer 8 Conference](https://www.youtube.com/channel/UCynWOUeHAOflEQtJnrZpkNA)
9. [SANS Blue Team Ops](https://www.youtube.com/c/SANSBlueTeamOps)
----

### Bashinho - Youtube-Playlists
1. [Bashinho - start.me](https://start.me/p/wM2QPX/bashinho)
2. [Internetgrundlagen für OSINT - YouTube](https://www.youtube.com/playlist?list=PL1GvaRufndL0CP2DR7YoiOxFGqPNW7fWS)
3. [OSINT für Einsteiger - YouTube](https://www.youtube.com/playlist?list=PL1GvaRufndL17faftlKLsa641ejjjWC1o)
4. [OSINT in der Bash - YouTube](https://www.youtube.com/playlist?list=PL1GvaRufndL1iEoWi9Ia4bVC6diGhTaWX)
5. [Bash-Grundlagen - YouTube](https://www.youtube.com/playlist?list=PL1GvaRufndL0zKVa2ehfvLa42KkfcYoUO)
6. [Datenaufbereitung in der Bash - YouTube](https://www.youtube.com/playlist?list=PL1GvaRufndL3S4hC74dzu1sO2nb94260k)
7. [AWK, der Alleskönner - YouTube](https://www.youtube.com/playlist?list=PL1GvaRufndL03kefgNJH76m0KoKPSk-QH)
----

### OSINT Communities
1. [Searchlight](https://discord.gg/Aj79c5Wp)
2. [Join Trace Labs on Discord](https://discord.com/invite/tracelabs)
3. [The OSINT Curious Project](https://discord.com/invite/osintcurious)
4. [Help users identify a location](https://www.reddit.com/r/whereisthis/)
----

### Eigensicherung
1. [Virtual Box: so verwandeln Sie Ihren PC in einen Surf- oder Testrechner](https://www.pcwelt.de/ratgeber/Virtuellen-PC-mit-altem-Windows-einrichten-OS-Recycling-8923087.html)
2. [Oracle VM VirtualBox](https://www.virtualbox.org/)
3. [Linux Mint Download](https://www.linuxmint.com/download.php)
4. [Leitfaden für die Installation von Linux Mint — Linux Mint Installation Guide  Dokumentation](https://linuxmint-installation-guide.readthedocs.io/de/latest/)
5. [LMU](https://www.linuxmintusers.de/)
6. [Startseite › Wiki › ubuntuusers.de](https://wiki.ubuntuusers.de/Startseite/)
7. [All Training Materials | tuxcademy](https://www.tuxcademy.org/media/all/)
8. [Linux-Kurs – Vom Einsteiger zum Linux-Profi](http://linuxkurs.ch)
9. [Kurs: Linux (Mint) für Einsteiger (OpenRUB)](https://moodle.ruhr-uni-bochum.de/m/course/view.php?id=19351&section=0)
10. [OSINT VM](https://www.tracelabs.org/initiatives/osint-vm)
11. [Tsurugi Linux](https://tsurugi-linux.org/)
12. [Kali](https://www.kali.org/)
13. [Kostenlose offizielle Windows Test-VM für 90 Tage Laufzeit](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/)
14. [Lizenzbedingungen Windows VM (für Evaluation, Test..)](https://az792536.vo.msecnd.net/vms/release_notes_license_terms_8_1_15.pdf)
----

### Browser Eigensicherung und Security Addons
1. [Cover Your Tracks](https://coveryourtracks.eff.org/)
2. [Exploiting custom protocol handlers for cross-browser tracking](https://schemeflood.com)
3. [ZENDAS Was Ihr Browser über Sie verrät (Datenschutz in der Hochschule)](https://www.zendas.de/service/browserdaten.html)
4. [So geht's - Referrer in Mozilla Firefox deaktivieren | NETZWELT](https://www.netzwelt.de/galerie/19255-so-geht-s-referrer-mozilla-firefox-deaktivieren.html)
5. [Canvas-Fingerprinting – Webtracking ohne Cookies - IONOS](https://www.ionos.de/digitalguide/online-marketing/web-analyse/canvas-fingerprinting-webtracking-ohne-cookies/)
6. [The Ultimate Firefox Privacy & Security Guide](https://proprivacy.com/privacy-service/guides/firefox-privacy-security-guide)
7. [User-Agent Switcher - Firefox](https://addons.mozilla.org/de/firefox/addon/user-agent-switcher-revived/)
8. [User-Agent Switcher and Manager](https://addons.mozilla.org/en-US/firefox/addon/user-agent-string-switcher/)
9. [User-Agent Switcher and Manager](https://chrome.google.com/webstore/detail/user-agent-switcher-and-m/bhchdcejhohfmigjafbampogmaanbfkg?hl=de)
10. [uBlock Origin - Firefox](https://addons.mozilla.org/de/firefox/addon/ublock-origin/)
11. [uBlock Origin - Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=de)
12. [Canvas Defender](https://addons.mozilla.org/en-US/firefox/addon/no-canvas-fingerprinting/)
13. [Canvas Fingerprint Defender - Chrome Web Store](https://chrome.google.com/webstore/detail/canvas-fingerprint-defend/lanfdkkpgfjfdikkncbnojekcppdebfp?hl=de)
14. [Location Guard](https://addons.mozilla.org/en-US/firefox/addon/location-guard/)
15. [Location Guard - Chrome Web Store](https://chrome.google.com/webstore/detail/location-guard/cfohepagpmnodfdmjliccbbigdkfcgia?hl=de)
16. [I don't care about cookies](https://addons.mozilla.org/de/firefox/addon/i-dont-care-about-cookies/)
17. [I don't care about cookies - Chrome Web Store](https://chrome.google.com/webstore/detail/i-dont-care-about-cookies/fihnjjcciajhdojfnbdddfaoknhalnja?hl=de)
18. [Stealthy](https://addons.mozilla.org/de/firefox/addon/stealthy/)
19. [Stealthy](https://chrome.google.com/webstore/detail/stealthy/ieaebnkibonmpbhdaanjkmedikadnoje?hl=de)
20. [Privacy Handbuch - Noscript Anleitung](https://www.privacy-handbuch.de/handbuch_21c1.htm)
21. [NoScript](https://addons.mozilla.org/de/firefox/addon/noscript/)
22. [NoScript - Chrome Web Store](https://chrome.google.com/webstore/detail/noscript/doojmbjmlfjjnbmnoijecmcbfeoakpjm?hl=de)
23. [Wodurch unterscheidet sich Facebook Container von Firefox Multi-Account Containers? | Hilfe zu Firefox](https://support.mozilla.org/de/kb/facebook-container-oder-multi-account-containers)
24. [Firefox Multi-Account Containers](https://addons.mozilla.org/de/firefox/addon/multi-account-containers/)
25. [Temporary Containers](https://addons.mozilla.org/de/firefox/addon/temporary-containers/)
26. [Tuhinshubhra/ExtAnalysis](https://github.com/Tuhinshubhra/ExtAnalysis)
27. [Mozilla Firefox, Portable Edition](https://portableapps.com/de/apps/internet/firefox_portable)
----

### Eigensicherungs Webseiten
1. [VirusTotal URL Checker](https://www.virustotal.com/gui/home/url)
2. [Blacklight – The Markup](https://themarkup.org/blacklight)
3. [SynapsInt](https://synapsint.com)
----

### ausgewählte Start.me Sammlungen
1. [OSINTframework.de](https://start.me/p/ZME8nR/osint)
2. [Nixintel's OSINT Resource List](https://start.me/p/rx6Qj8/nixintel-s-osint-resource-list)
3. [FAROS OSINT Resources](https://start.me/p/1kvvxN/faros-osint-resources)
4. [First Draft News - Basic Toolkit](https://start.me/p/vjv80b/first-draft-basic-toolkit)
5. [V3nari Bookmarks](https://start.me/p/1kxyw9/v3nari-bookmarks)
6. [commandergirl's suggestions](https://start.me/p/1kJKR9/commandergirl-s-suggestions)
7. [Verification Toolset](https://start.me/p/ZGAzN7/verification-toolset?utm_content=buffer84b1d&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
----

### Übersetzung / Synonyme
1. [DeepL Translator](http://deepl.com)
2. [Webseiten im Browser automatisch übersetzen: So geht´s | NETZWELT](https://www.netzwelt.de/anleitung/178580-webseiten-browser-automatisch-uebersetzen-so-gehts.html)
3. [Google Translate Web](http://itools.com/tool/google-translate-web-page-translator)
4. [To Google Translate](https://addons.mozilla.org/de/firefox/addon/to-google-translate)
5. [Webseiten übersetzen](https://addons.mozilla.org/de/firefox/addon/traduzir-paginas-web/)
6. [Synonyme, Slang und Umgangssprache](https://www.openthesaurus.de/)
7. [Keyword Tool](https://keywordtool.io/)
8. [Offline Translator – Übersetzer-Apps die auch ohne Internet funktionieren | EHLION](https://ehlion.com/de/magazine/offline-uebersetzer/)
----

### Empfohlene Twitterkonten
1. [Verif!cation Quiz Bot (@quiztime)](https://twitter.com/quiztime)
2. [We Are OSINTCurious (@OsintCurious)](https://twitter.com/OsintCurious)
3. [OSINT Dojo (@OSINTDojo)](https://twitter.com/OSINTDojo)
4. [IntelOsint](https://twitter.com/IntelOsint)
5. [OSINT Research (@OSINT_Research)](https://twitter.com/OSINT_Research)
6. [Usersearch_web](https://twitter.com/Usersearch_web)
7. [OH SHINT! (@ohshint_)](https://twitter.com/ohshint_)
8. [Cyber Detective (@cyb_detective)](https://twitter.com/cyb_detective)
9. [kirbstr (@kirbstr)](https://twitter.com/kirbstr)
10. [OliverKlein](https://twitter.com/OliverKlein)
11. [OSINT Techniques (@OSINTtechniques)](https://twitter.com/OSINTtechniques)
12. [Loránd Bodó (@LorandBodo)](https://twitter.com/LorandBodo)
13. [Benjamin Strick (@BenDoBrown)](https://twitter.com/BenDoBrown)
14. [nixintel (@nixintel)](https://twitter.com/nixintel)
15. [technisette (@technisette)](https://twitter.com/technisette)
16. [𝕊𝕖𝕔𝕥𝕠𝕣𝟘𝟛𝟝 (@Sector035)](https://twitter.com/Sector035)
17. [Dutch OsintGuy (@dutch_osintguy)](https://twitter.com/dutch_osintguy)
18. [Micah (@WebBreacher)](https://twitter.com/WebBreacher)
19. [osint.support (@OsintSupport)](https://twitter.com/OsintSupport)
20. [ʜᴇɴᴋ ᴠᴀɴ ᴇss (@henkvaness)](https://twitter.com/henkvaness)
----

### Trainings (kostenlos)
1. [OSINT Quiz by sector035](https://quiz.sector035.nl/)
2. [OSINT Kurs mit Zertifikat](https://baselgovernance.org/news/new-free-elearning-course-open-source-intelligence-osint)
3. [Verif!cation Quiz Bot (@quiztime)](https://twitter.com/quiztime)
----

### Trainings (kostenpflichtig)
1. [OSINTgeek (deutsch)](https://osintgeek.de/#kurse)
2. [Open-Source Intelligence (OSINT) Gathering Training](https://www.sans.org/cyber-security-courses/open-source-intelligence-gathering/)
3. [Advanced Open-Source Intelligence (OSINT) Gathering and Analysis](https://www.sans.org/cyber-security-courses/advanced-open-source-intelligence-gathering-analysis/)
4. [My OSINT Training Home Page](https://www.myosint.training/collections)
----

### Bookmarklets
1. [Bookmarklets](http://7is7.com/software/bookmarklets/)
----

### Whois, Geolocation, MAC, DNS, etc
1. [Whois and other free online network tools](https://centralops.net/co/)
2. [Whoxy - historische Whois Daten](https://www.whoxy.com/)
3. [Flagfox](https://addons.mozilla.org/de/firefox/addon/flagfox)
4. [GeoIP2 Database Demo](https://www.maxmind.com/en/geoip-demo)
5. [IP Address Geolocation](https://resolve.rs/ip/geolocation.html)
6. [Location of an IP Address](https://osintcurio.us/2022/02/14/location-of-an-ip-address/)
7. [heise online MAC](https://www.heise.de/netze/tools/mac)
8. [dnsdumpster](https://dnsdumpster.com)
9. [Robtex](https://www.robtex.com/)
10. [Spyse.com](https://spyse.com/)
11. [How to find and check my IP address](https://whoer.net/)
12. [IPNetInfo: Retrieve IP Address Information from WHOIS servers](https://www.nirsoft.net/utils/ipnetinfo.html)
13. [Quad9 | Ein öffentlicher und kostenloser DNS-Dienst für mehr Sicherheit und Datenschutz](https://www.quad9.net/de/)
14. [domainbigdata](https://domainbigdata.com/)
15. [Netcraft](http://netcraft.com)
16. [CRT](https://crt.sh/)
17. [Sensys.io](http://sensys.io)
18. [Reverse Analytics ID](https://dnslytics.com/reverse-analytics)
19. [Reverse Adsense ID](https://dnslytics.com/reverse-adsense)
20. [Reverse IP Lookup](https://reverseip.domaintools.com/)
21. [Twelve99 Looking Glass](https://lg.twelve99.net)
22. [Ping.pe](http://ping.pe/)
23. [ViewDNS.info](https://viewdns.info/)
24. [SecurityTrails](https://securitytrails.com/)
25. [My IP Address](https://myip.ms/)
----

### Dokumentation
1. [Zotero](http://www.zotero.org)
----

### Webseite sichern
1. [Eine Webseite speichern | Hilfe zu Firefox](https://support.mozilla.org/de/kb/Eine%20Webseite%20speichern)
2. [Firefox Screenshots | Hilfe zu Firefox](https://support.mozilla.org/de/kb/firefox-screenshots)
3. [MAGNET Web Page Saver - Magnet Forensics](https://www.magnetforensics.com/resources/web-page-saver/)
4. [eDocPrintPro PDF Drucker](https://www.pdfprinter.at/)
5. [HTTrack Website Copier](https://www.httrack.com/page/2/)
6. [Cyotek WebCopy](https://www.cyotek.com/cyotek-webcopy/downloads)
7. [Instant Data Scraper](https://chrome.google.com/webstore/detail/instant-data-scraper/ofaokhiedipichpaobibbnahnkdoiiah)
8. [Link Gopher](https://sites.google.com/site/linkgopher/)
----

### Webseite überwachen / Nachricht bei Änderung
1. [Visualping](https://visualping.io/)
2. [Follow That Page](https://followthatpage.com)
3. [Versionista](https://versionista.com/)
4. [Web Monitor](https://distill.io/apps/web-monitor)
5. [Webseite auf Änderungen überwachen – bqdp.de](https://blog.bqdp.de/2017/06/23/webseite-auf-aenderungen-ueberwachen/)
6. [huginn/huginn](https://github.com/huginn/huginn)
----

### Vergangenheit im Netz
1. [Web Archives](https://addons.mozilla.org/de/firefox/addon/view-page-archive/)
2. [Web Archives - Chrome Web Store](https://chrome.google.com/webstore/detail/web-archives/hkligngkgcpcolhcnkgccglchdafcnao?hl=de)
3. [Wayback Machine](https://addons.mozilla.org/de/firefox/addon/wayback-machine_new/)
4. [Wayback Machine](https://chrome.google.com/webstore/detail/wayback-machine/fpnmgdkabkmnadcjpehmlllkndpkmiak?hl=de)
5. [Internet Archive](https://archive.org)
6. [Internet Archive: Advanced Search Engine](https://archive.org/advancedsearch.php)
7. [Using Archive.org for OSINT Investigations](https://osintcurio.us/2021/03/03/using-archive-org-for-osint-investigations/)
8. [View cache of all sites 🗄 Cache.pw](https://cache.pw/de/)
9. [CachedPages](http://www.cachedpages.com/)
10. [Wayback Keyword Search- GitHub](https://github.com/lorenzoromani1983/wayback-keyword-search)
11. [archive.ph](https://archive.ph/)
----

### PGPKey
1. [Kleopatra - OpenPGP](https://www.openpgp.org/software/kleopatra/)
2. [Keybase](http://keybase.io)
3. [Keyserver](https://www.heise.de/security/dienste/Keyserver-474468.html)
----

### Zeitzonen
1. [Zeitzonen der Welt: Alle Zeitzonen-Abkürzungen](https://www.timeanddate.de/zeitzonen/weltweit/)
2. [The Time Zone Converter](https://www.thetimezoneconverter.com/)
3. [timebie](http://www.timebie.com/)
----

### Barcodes
1. [Free online Barcode Reader](https://online-barcode-reader.inliteresearch.com/)
----

### Cams
1. [Webcams | OSINT Essentials](https://www.osintessentials.com/webcams)
2. [EarthCam](https://www.earthcam.com)
3. [Pictimo](https://www.pictimo.com/)
----

### Smartphone-Ortung
1. [Mit der App "Wo ist?" ein verlorenes oder gestohlenes Gerät finden - Apple Support](https://support.apple.com/de-de/HT210515)
2. [Gerät in „Mein iPhone suchen“ auf iCloud.com orten - Apple Support](https://support.apple.com/de-de/guide/icloud/mmfc0f2442/icloud)
3. [Android Lokalisation](https://www.google.com/android/find)
4. [Stolen Camera Finder](https://stolencamerafinder.com)
----

### Bitcoin und Kryptowährungen
1. [Bitcoinwert zeitgenau bestimmen](https://www.tradingview.com/chart/)
2. [Das Bitcoin Whitepaper von Satoshi Nakamoto](https://www.bitcoin.de/de/bitcoin-whitepaper-deutsch)
3. [Coin Market Cap](https://coinmarketcap.com/)
4. [Paper Wallet](https://blockchainwelt.de/paper-wallet-erstellen-sichern-aufbewahren/)
5. [electrum seed wordlist](https://github.com/spesmilo/electrum/tree/master/electrum/wordlist)
6. [Blockchain Demo](https://andersbrownworth.com/blockchain/coinbase)
7. [Bitcoin Transaktionsgebühr](https://bitinfocharts.com/de/comparison/bitcoin-transactionfees.html)
8. [WalletExplorer](https://www.walletexplorer.com)
9. [Bitcoin Lookup](https://intelx.io/tools?tab=bitcoin)
10. [The Ethereum  Block Explorer](http://etherscan.io)
11. [Unstoppable Domains](https://unstoppabledomains.com/nft-collection)
12. [Bitcoin Abuse Database](http://bitcoinabuse.com/)
13. [bitcoin whos who](https://Bitcoinwhoswho.com)
14. [OXT](https://oxt.me)
15. [Bitcoinpaperwallet.com | Is it a Scam? Case Study 2022](https://privacypros.io/wallets/paper-scam-warning/)
16. [Bitcoin Geldautomaten](https://coinatmradar.com/country/80/bitcoin-atm-germany/)
----

### Passwörter
1. [Openwall](https://www.openwall.com/)
2. [Intelligence X](http://intelx.io/)
3. [WinRar Passwort umgehen in 4 min - YouTube](https://www.youtube.com/watch?v=gpTYKVMtfhI)
4. [Word- und Excel-Passwörter knacken - PC Magazin](https://www.pc-magazin.de/ratgeber/word-excel-passwoerter-knacken-3196662-15330.html)
5. [How to Break or Crack PDF Password Protected File](https://www.iseepassword.com/how-to-crack-pdf-password.html)
6. [Die drei wichtigsten PDF-Passwort-Cracker, die Sie ausprobieren sollten - iStarApps](https://www.istarapps.com/de/pdf-password-cracker.html)
7. [CrackStation](https://crackstation.net/)
8. [Passwort erraten. erste 3 Zeichen eingeben](https://passwords.igg.cloud/)
----

### Darknet
1. [Tor2web: Browse the Tor Onion Services](https://www.tor2web.org/)
2. [SEARCH THE DARK WEB – IACA Dark Web Investigation Support](https://iaca-darkweb-tools.com/search-darkweb/)
3. [Unhardened Web Servers in Tor Have No Anonymity – We are OSINTCurio.us](https://osintcurio.us/2019/03/05/apache-mod_status-in-tor-hidden-services-destroy-anonymity/)
4. [Nameserver Lookup + Tor Exit Node](https://www.gaijin.at/de/tools/nameserver-lookup#result)
5. [Tor2Web Proxy Liste](https://twitter.com/ohshint_/status/1482190623759212544)
6. [Tails](https://tails.boum.org/)
7. [A Beginners Guide to the Dark Web – We are OSINTCurio.us](https://osintcurio.us/2022/03/14/a-beginners-guide-to-the-dark-web/)
8. [Awesome Onion Links](https://github.com/5ky1ar/Awesome-Onion-Links)
9. [Darkweb OSINT investigations resources for 2022 – osintme.com](https://www.osintme.com/index.php/2022/02/12/darkweb-osint-investigations-resources-for-2022/)
10. [tor.taxi](https://tor.taxi/)
----

### Datentransformation
1. [CyberChef](https://gchq.github.io/CyberChef)
----

### Shodan
1. [Cheat Sheet](https://cheatsheet-maker.herokuapp.com/sheet/61df0d1a9ee9010015bc5ebe)
----

### Gesperrte Inhalte sichtbar machen
1. [Mobile-Friendly Test](https://search.google.com/test/mobile-friendly)
2. [Best Online HTML Viewer, HTML Beautifier, HTML Formatter, Beautify, Minify, Test Output](https://codebeautify.org/htmlviewer)
----

### Scam
1. [ScamSearch](https://scamsearch.io/)
----

### Link Shortener
1. [Short links verification cheatsheet](https://github.com/seintpl/osint/blob/main/short-links-verification-cheatsheet.md)
2. [CheckShortURL](https://checkshorturl.com/)
3. [Unshorten that URL!](https://unshorten.it/)
4. [URL Expander — Unshorten Any Short URL To A Long URL](https://urlex.org/)
5. [Investigate Shortened URLs - Aware Online](https://www.aware-online.com/en/investigate-shortened-urls/)
----

### Audio
1. [Audio in Schrift umwandeln](https://twitter.com/henkvaness/status/1492178152935890949)
2. [How To Get Transcripts For Any Podcast · Mark Allen](https://markallen.io/podcast-transcripts/)
3. [google-brings-search-to-podcasts-through-automatic-transcription](https://searchengineland.com/google-brings-search-to-podcasts-through-automatic-transcription-314798)
4. [How can I search over audio?](http://searchresearch1.blogspot.com/2022/02/searchresearch-challenge-21622-how-can.html?m=1)
5. [translate live video from Ukrainian YouTube channels or IPTV](https://twitter.com/henkvaness/status/1496794581110870017)
6. [Go-transcribe.com](https://go-transcribe.com/)
----

### Suchen & Finden
1. [Suchmaschine auswählen](https://start.me/w/dNlAjM)
2. [Suchmaschinen - Linksammlung](https://start.me/w/Bqp5eL)
3. [Spezialsuchmaschinen - Linksammlung](https://start.me/w/9qz5Xl)
4. [Personensuchmaschinen - Linksammlung](https://start.me/w/1vwmJL)
5. [Mailadressen prüfen und finden - Linksammlung](https://start.me/w/gmdz1K)
6. [Nicknames - Linksammlung](https://start.me/w/la5ngD)
7. [Bildersuche - Linksammlung](https://start.me/w/gqmRAn)
8. [Bildbearbeitung - Linksammlung](https://start.me/w/7wqOOR)
9. [Bildersuche Anleitungen - Linksammlung](https://start.me/w/QGK2qA)
10. [Bilder Chronolocation - Linksammlung](https://start.me/w/LNnp0M)
11. [Fotoforensik - Linksammlung](https://start.me/w/wwDlpX)
12. [Karten - Linksammlung](https://start.me/w/1vwoM9)
13. [Satelliten Aufnahmen - Linksammlung](https://start.me/w/EN1pOQ)
14. [Gaming - Linksammlung](https://start.me/w/MlylkP)
15. [Dronen-Aufnahmen - Linksammlung](https://start.me/w/GObY68)
16. [Telefonnummern - Linksammlung](https://start.me/w/mqlqkv)
----

### Suchmaschinen
1. [Author Talk: The Joy of Search by Daniel M. Russell | The MIT PressSearchclosecloseBackcloseBackclosePDFPDFBackcloseBackclosefacebooktwitterlinkedinpinterestglyph-logo_May2016BackSearchcloseSharecloseSharecloseSmall ArrowSmall ArrowSmall ArrowSmall Arrowfacebooktwitterlinkedinpinterestglyph-logo_May2016](https://mitpress.mit.edu/blog/author-talk-joy-search-daniel-m-russell)
2. [Google](https://google.de)
3. [Google – Suchanpassung](https://www.google.com/history/optout?hl=de)
4. [Yandex](https://yandex.com/)
5. [Bing](https://bing.de)
6. [Qwant](https://www.qwant.com/?l=de)
7. [Baidu](https://baidu.com)
8. [you.com](https://you.com)
9. [Google Advanced Search Operators](https://docs.google.com/document/d/1ydVaJJeL1EYbWtlfj9TPfBTE5IBADkQfZrQaBZxqXGs/edit)
10. [Yandex Search Operators: A List Of 25+ Operators [2021]](https://seosly.com/yandex-search-operators/)
11. [Guide to Advanced Search Operators for Bing and Google](https://www.bruceclay.com/blog/bing-google-advanced-search-operators/)
12. [How to Find (Almost) Anything on Google - NetCredit Blog](https://www.netcredit.com/blog/how-to-find-anything-on-google/)
13. [Google Suchparameter](https://www.onlinemarketing-praxis.de/uploads/pdf/suchparameter-google-uebersicht.pdf)
14. [Google Search Presentation by David M. Russell](https://drive.google.com/file/d/0BxlpTzK9iG-2dy1jYnpVeWdBV1k/view)
15. [Suchmaschinen Datenbank](https://www.suchmaschinen-datenbank.de/)
16. [Using the Google custom search engine for OSINT – osintme.com](https://www.osintme.com/index.php/2020/09/28/using-the-google-custom-search-engine-for-osint/)
17. [Google Alerts – Interessanten neuen Inhalten im Web folgen](https://google.de/alerts)
18. [Talkwalker Alerts, die beste kostenlose und einfache Alternative zu Google Alerts - Talkwalker](https://talkwalker.com/de/alerts)
19. [DorkSearch](https://www.dorksearch.com/)
20. [So You Think You Can Google?](https://www.youtube.com/watch?v=uyqXS5lL-mc)
21. [Isearchfrom-com.translate.goog](https://isearchfrom-com.translate.goog/?_x_tr_sch=http&_x_tr_sl=auto&_x_tr_tl=de&_x_tr_hl=de)
----

### Spezialsuchmaschinen
1. [Intelligence X](http://intelx.io)
2. [northdata.de](https://www.northdata.de/)
3. [SeekLogo](https://seeklogo.com/)
4. [Listen Notes](https://www.listennotes.com)
5. [Similar Sites](https://www.similarsites.com)
6. [Briz Speech Database - find a text in large video and audio archives in a moment! The media asset management with full-text search.](https://www.brizsoft.com/speech-database/)
7. [Shodan](https://www.shodan.io/explore)
8. [OpenCorporates](https://opencorporates.com/)
9. [File-Extensions](https://www.file-extensions.org/)
10. [Fagan Finder](https://www.faganfinder.com/)
11. [Alt-Tech Social Search](https://www.osintcombine.com/alt-tech-social-search)
----

### Personensuchmaschinen
1. [Yasni](http://www.yasni.de)
2. [International-Infobel-Telefonverzeichnis](https://www.infobel.com/de/world)
3. [Personensuche.dastelefonbuch.de](https://personensuche.dastelefonbuch.de/)
4. [Osint Me Tricky Thursday #6 – Person Search – osintme.com](https://www.osintme.com/index.php/2020/06/25/osint-me-tricky-thursday-6-person-search/)
5. [NAMINT](https://seintpl.github.io/NAMINT/)
6. [WebMii](https://webmii.com/)
----

### Mailadressen prüfen und finden
1. [Have I been pwned?](https://haveibeenpwned.com/)
2. [Intelligence X](http://intelx.io)
3. [Vortimo Skylight](https://skylight.vortimo.com/)
4. [Email Lookup](https://tools.epieos.com/email.php)
5. [Email Address Verifier](https://tools.emailhippo.com/)
6. [Email Test to check the delivery to every mail server](https://dnslytics.com/email-test)
7. [Email Reputation](https://emailrep.io/)
8. [MXtoolbox](https://mxtoolbox.com/)
9. [Find the Person Behind an Email Address - Digital Inspiration](https://www.labnol.org/internet/find-person-by-email-address/13913/)
10. [E-Mail-Header lesen und verstehen | th-h.de](https://th-h.de/net/usenet/faqs/headerfaq/)
11. [Email Permutator](http://metricsparrow.com/toolkit/email-permutator/)
12. [Email Extraction Tool - MxToolBox](https://mxtoolbox.com/EmailExtraction.aspx)
13. [Hunter (finde professionelle E-Mail-Adressen)](https://hunter.io/)
14. [Connect](https://connect.clearbit.com/)
----

### Nicknames
1. [How to trace social media users across multiple platforms](https://osintcurio.us/2020/03/02/how-to-trace-social-media-users-across-multiple-platforms/)
2. [Username-Lookup](https://intelx.io/tools?tab=username)
3. [WhatsMyName Web](https://whatsmyname.app)
4. [KnowEm](https://knowem.com/)
5. [analyzeid.com/username](https://analyzeid.com/username/)
6. [Namechk](https://namechk.com/)
7. [Username Search](https://usersearch.org/index.php)
8. [thewhiteh4t/nexfil](https://github.com/thewhiteh4t/nexfil)
----

### Bildersuche
1. [Reverse Image Search Multi-Tool](https://intelx.io/tools?tab=image)
2. [Yandex.Images: search for images on the internet, search by image](https://yandex.com/images/)
3. [TinEye](https://tineye.com/)
4. [Google Bilder](https://www.google.de/imghp?hl=de&ogbl)
5. [Google Advanced Image Search](https://www.google.com/advanced_image_search)
6. [Bing Bildersuche](https://www.bing.com/images?FORM=Z9LH)
7. [Pixsy](https://www.pixsy.com/)
8. [PimEyes: Face Recognition Search Engine and Reverse Image Search |](https://pimeyes.com/en)
9. [Jeffrey Friedl's Image Metadata Viewer](http://exif.regex.info/exif.cgi)
10. [GeoTips - Typisches für Länder](https://geotips.net/europe/)
11. [License Plates of the World](http://worldlicenseplates.com/)
12. [Trip Advisor - Sehenswürdigkeiten](https://www.tripadvisor.de/)
13. [SkyscraperPage.com](https://skyscraperpage.com/)
14. [Phorio Wolkenkratzer Datenbank](https://en.phorio.com/)
15. [Gesichtserkennung | Microsoft Azure](https://azure.microsoft.com/de-de/services/cognitive-services/face/#demo)
16. [Structurae - Internationale Datenbank für Ingenieurbauwerke](https://structurae.net/de/)
17. [Autos identifizieren](https://carnet.ai/)
----

### Bildersuche Addons
1. [Search by Image - Firefox](https://addons.mozilla.org/en-GB/firefox/addon/search_by_image/)
2. [Search by Image - Chrome Web Store](https://chrome.google.com/webstore/detail/search-by-image/cnojnbdhbhnkbcieeekonklommdnndci?hl=de)
3. [Reverse Image Search](https://addons.mozilla.org/en-US/firefox/addon/image-reverse-search/)
4. [Exif Viewer - Firefox](https://addons.mozilla.org/de/firefox/addon/exif-viewer)
5. [Exif Viewer - Chrome Web Store](https://chrome.google.com/webstore/detail/exif-viewer/kbnpbnmjmgabkfemdehelbgdppngihhg?hl=de)
6. [Copyfish](https://addons.mozilla.org/en-US/firefox/addon/copyfish-ocr-software/)
7. [Copyfish 🐟 Free OCR Software - Chrome Web Store](https://chrome.google.com/webstore/detail/copyfish-%F0%9F%90%9F-free-ocr-soft/eenjdnjldapjajjofmldgmkjaienebbj?hl=de)
8. [Lupe für den Browser](https://addons.mozilla.org/de/firefox/addon/magnifying-glass-hover-zoom/)
9. [Lupe für Google Chrome ™ - Chrome Web Store](https://chrome.google.com/webstore/detail/magnifying-glass-for-goog/haccmlbamnmoglclgipjldbdpdlfefki?hl=de)
10. [Search on Google Lens](https://addons.mozilla.org/de/firefox/addon/search-on-google-lens/)
11. [Fake news debunker by InVID & WeVerify](https://chrome.google.com/webstore/detail/fake-news-debunker-by-inv/mhccpoafgdgbhnjfhkcmgknndkeenfhe)
----

### Bildbearbeitung
1. [schwarzweise Bilder farbig machen.](https://www.myheritage.de/incolor)
2. [Bilder zuschneiden](https://www.iloveimg.com/crop-image)
3. [Bildinhalte löschen cleanup.pictures](https://cleanup.pictures/)
4. [PhotoScissors Background Removal Tool](https://photoscissors.com/)
----

### Bildersuche Anleitungen
1. [Guide To Using Reverse Image Search For Investigations](https://www.bellingcat.com/resources/how-tos/2019/12/26/guide-to-using-reverse-image-search-for-investigations/)
2. [20 Questions – Quiztime 25th November 2019 – NixIntel](https://nixintel.info/osint/20-questions-quiztime-25th-november-2019/)
3. [Chronolocation of Media](https://sector035.nl/articles/chronolocation-of-media)
4. [Using the Sun and the Shadows for Geolocation - bellingcatUsing the Sun and the Shadows for Geolocation - bellingcat](https://www.bellingcat.com/resources/2020/12/03/using-the-sun-and-the-shadows-for-geolocation/)
5. [When there is no Google Earth or Street View, what can you do? – We are OSINTCurio.us](https://osintcurio.us/2020/09/28/when-there-is-no-google-earth-or-street-view-what-can-you-do/)
6. [How to use of Pinterest image search - GainChanger](https://www.gainchanger.com/pinterest-image-search/)
7. [Using Gap Analysis For Smarter OSINT – Quiztime 4th March 2020 – NixIntel](https://nixintel.info/osint/using-gap-analysis-for-smarter-osint-quiztime-4th-march-2020/)
8. [GeoGuessr- The Top Tips, Tricks and Techniques](https://somerandomstuff1.wordpress.com/2019/02/08/geoguessr-the-top-tips-tricks-and-techniques/)
9. [“Search your screen, with Google Lens” to the Chrome Desktop Beta Channel](https://support.google.com/chrome/thread/123640918/introducing-%E2%80%9Csearch-your-screen-with-google-lens%E2%80%9D-to-the-chrome-desktop-beta-channel?hl=en)
10. [Chrome 94 for desktop tests Google Lens Image Search](https://techdows.com/2021/09/chrome-94-desktop-google-lens-search.html)
11. [Geolocation mit Bergen](https://www.linkedin.com/posts/eyran-millis_mountain-geolocating-tutorial-activity-6941996317795565568-JYvf)
----

### Bilder Chronolocation
1. [Pl@ntNet identify](https://identify.plantnet.org/de)
2. [Berechnung der Position der Sonne in den Himmel für jeden Ort auf der Erde zu jeder Zeit des Tages](https://www.sunearthtools.com/dp/tools/pos_sun.php?lang=de)
3. [SunCalc sun position- und sun phases calculator](https://suncalc.org)
4. [Shadowcalculator.eu](http://shadowcalculator.eu/)
5. [Historische Wetterdaten](https://www.wetteronline.de/wetterdaten/)
----

### Fotoforensik
1. [FotoForensics](https://fotoforensics.com)
2. [MeVer - Image Verification Assistant](https://mever.iti.gr/forensics/)
3. [Digital Image Forensic Analyzer](https://www.imageforensic.org/)
4. [online photo forensics tools - 29a.ch](https://29a.ch/photo-forensics/#forensic-magnifier)
5. [JPEGsnoop](https://www.impulseadventure.com/photo/jpeg-snoop.html)
6. [Anleitung: Wie man ein Fake-Bild erkennen kann | Tutonaut.de](https://www.tutonaut.de/wie-man-fake-bilder-im-netz-erkennen-kann/)
7. [Bildmanipulation erkennen](http://www.bilderlernen.at/wp-content/uploads/2017/09/BildmanipulationErkennenGavino.pdf)
8. [Handbook of Digital Face Manipulation and Detection | SpringerLink](https://link.springer.com/book/10.1007/978-3-030-87664-7)
9. [BSI  -  Deepfakes - Gefahren und Gegenmaßnahmen](https://www.rnd.de/digital/deepfakes-erkennen-auf-welche-hinweise-sie-bei-audio-und-videomanipulationen-achten-koennen-4GCHCB6RM3SDXJZ7WFCPDX37P4.html)
10. [Deepfakes - Gefahren und Gegenmaßnahmen](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Kuenstliche-Intelligenz/Deepfakes/deepfakes_node.html)
----

### Karten
1. [Google Maps](https://www.google.com/maps)
2. [Yandex.Maps — detailed map of the world](https://yandex.com/maps/)
3. [Bing Maps](https://www.bing.com/maps)
4. [Mapillary](https://www.mapillary.com/)
5. [Apple Maps (thanks DuckDuckGo)](https://duckduckgo.com/?q=germanyl&t=h_&ia=web&iaxm=maps)
6. [KartaView](https://kartaview.org)
7. [what3words /// The simplest way to talk about location](https://what3words.com/kinosaal.lernen.druck)
8. [Free Map Tools](https://www.freemaptools.com/)
9. [Snap Map](https://map.snapchat.com/)
10. [The one million tweet map](https://onemilliontweetmap.com)
11. [Twitter Trending Hashtags and Topics](https://www.trendsmap.com/map)
12. [Dual maps](http://dualmaps.com)
13. [MapChecking](https://mapchecking.com)
14. [MapHub](https://maphub.net/)
15. [Overpass-turbo.eu](https://overpass-turbo.eu/)
16. [WiGLE: Wireless Network Mapping](https://wigle.net)
17. [OSINT — Free Tools for better Satellite Imagery | by Aijaz Khan | Jan, 2022 | Medium](https://medium.com/@aijaz.khan9872/osint-free-tools-for-better-satellite-imagery-2738e9df4012)
18. [Showmystreet](https://showmystreet.com)
19. [Wikimapia](https://wikimapia.org/#lang=de&lat=51.267300&lon=8.235100&z=12&m=w)
20. [Maps for Mobility | geOps](https://mobility.portal.geops.io/world.geops.transit?baselayers=world.geops.travic,ch.sbb.netzkarte,ch.sbb.netzkarte.dark,world.geops.aerial&lang=en&layers=paerke,strassennamen,haltekanten,haltestellen,pois&x=1008277.4&y=6244730.73&z=10.71)
21. [Whatiswhere.com](https://www.whatiswhere.com/)
----

### Satelliten Aufnahmen
1. [Soar | Discover your Earth](https://t.co/XBfTH5555Q)
2. [Satellites.pro](https://satellites.pro)
3. [Spectator](https://spectator.earth/)
4. [World Imagery Wayback](https://livingatlas.arcgis.com/wayback/#active=10312&ext=-115.36416,36.03322,-115.23284,36.09477)
----

### Gaming
1. [PSN Profile](https://psnprofiles.com)
2. [Steam Community :: Search Users](https://steamcommunity.com/search/users)
3. [Search Xbox Gamertags](https://xboxgamertag.com)
4. [Xboxclips.co](https://xboxclips.co)
5. [Xboxreplay.net](https://xboxreplay.net)
----

### Dronen-Aufnahmen
1. [TRAVEL with DRONE](https://travelwithdrone.com/)
2. [Free Drone Videos & Aerial footage](https://mixkit.co/free-drone-footage/)
3. [Dronestagram](https://www.dronestagr.am/)
4. [Best Drone Videos Archives](https://www.dronezon.com/category/best-drone-videos/)
5. [Airvuz.com](https://www.airvuz.com/)
----

### Telefonnummern
1. [PhoneInfoga](https://demo.phoneinfoga.crvx.fr/#/)
----

### Soziale Netzwerke
1. [Social Media - Linksammlung](https://start.me/w/k9aNAk)
2. [Fake Account - Linksammlung](https://start.me/w/8qK42B)
3. [Facebook - Linksammlung](https://start.me/w/m5QGgv)
4. [Twitter - Linksammlung](https://start.me/w/P4lNnA)
5. [TikTok - Linksammlung](https://start.me/w/Aq1yEk)
6. [Instagram - Linksammlung](https://start.me/w/Jwm6n0)
7. [Whats App - Linksammlung](https://start.me/w/yDLNgD)
8. [Skype - Linksammlung](https://start.me/w/NaB057)
9. [Youtube - Linksammlung](https://start.me/w/NaB0b7)
10. [Telegram - Linksammlung](https://start.me/w/aAjgr6)
11. [Snapchat - Linksammlung](https://start.me/w/RAPAGe)
12. [Linkedin - Linksammlung](https://start.me/w/7qn0ox)
13. [Google - Linksammlung](https://start.me/w/Xz2G5k)
14. [Reddit - Linksammlung](https://start.me/w/RLxRLm)
15. [Weitere SN - Linksammlung](https://start.me/w/4bvJ5q)
----

### Social Media
1. [A Beginner's Guide to Social Media Verification - bellingcatA Beginner's Guide to Social Media Verification - bellingcat](https://www.bellingcat.com/resources/2021/11/01/a-beginners-guide-to-social-media-verification/)
2. [User Lookups: TikTok, Instagram, Snapchat, Tinder, OnlyFans & Badoo](https://usersearch.org/updates/2021/11/19/user-lookups-tiktok-instagram-snapchat-tinder-onlyfans-badoo/?utm_campaign=02-12-22+-+User+Lookups%3A+T&utm_source=Message&utm_medium=Twitter)
3. [Social Scanner API Documentation (hailbytes-hailbytes-default) | RapidAPI](https://rapidapi.com/hailbytes-hailbytes-default/api/social-scanner/)
----

### Fake Account
1. [The OSINT Puppeteer](https://osintcurio.us/2018/12/27/the-puppeteer/)
2. [Fictional Accounts](https://www.osinttechniques.com/fictional-accounts.html)
3. [My process for setting up anonymous sockpuppet accounts. : OSINT](https://www.reddit.com/r/OSINT/comments/dp70jr/my_process_for_setting_up_anonymous_sockpuppet/)
4. [Whichfaceisreal.com](https://www.whichfaceisreal.com/)
5. [How to recognize fake AI-generated images](https://kcimc.medium.com/how-to-recognize-fake-ai-generated-images-4d1f6f9a2842)
6. [This Person Does Not Exist](https://www.thispersondoesnotexist.com/)
7. [Generated Photos](https://generated.photos/)
8. [Biometrie im Reisepass: Peng! Kollektiv schmuggelt Fotomontage in Ausweis - DER SPIEGEL](https://www.spiegel.de/netzwelt/netzpolitik/biometrie-im-reisepass-peng-kollektiv-schmuggelt-fotomontage-in-ausweis-a-1229418.html)
9. [morph thing](https://www.morphthing.com/)
10. [Face Merging](https://www.faceplusplus.com/face-merging/)
11. [Generate look-a-like photos to protect your identity](https://generated.photos/anonymizer)
12. [Remove Background from Image – remove.bg](https://www.remove.bg/de)
13. [theXifer.net](https://www.thexifer.net/)
14. [Fake Name Generator](https://www.fakenamegenerator.com/)
15. [Random User Generator](https://randomuser.me/)
16. [Random Name Generator](https://www.behindthename.com/random/)
17. [Unsplash (Freie Stockphotos)](https://unsplash.com/)
18. [Pexels (Freie Stockfotos)](https://pexels.com)
19. [Mozilla Firefox, Portable Edition](https://portableapps.com/de/apps/internet/firefox_portable)
20. [sipgate](https://www.satellite.me/)
21. [Privacy (Virtuelle verschleierte Kreditkarten)](https://privacy.com)
22. [Fake Profile Detector (Deepfake, GAN)](https://chrome.google.com/webstore/detail/fake-profile-detector-dee/jbpcgcnnhmjmajjkgdaogpgefbnokpcc?hl=en-US)
23. [Ganbreeder](https://www.artbreeder.com/)
24. [Fake Information Generator](https://fakeinfo.net/)
25. [Am I Real?](https://seintpl.github.io/AmIReal/)
----

### Facebook
1. [Jens Farley's Facebook Bookmarklets](http://com.hemiola.com/bookmarklet/)
2. [What to do when a Facebook profile is private?](https://osintcurio.us/2020/10/19/what-to-do-when-a-facebook-profile-is-private/)
3. [Get Facebook ID](https://addons.mozilla.org/de/firefox/addon/facebook-id)
4. [DumpItBlue+](https://chrome.google.com/webstore/detail/dumpitblue%2B/igmgknoioooacbcpcfgjigbaajpelbfe)
5. [Facebook Matrix — Plessas Experts Network](https://plessas.net/facebookmatrix)
6. [The new Facebook Graph Search – part 1](https://osintcurio.us/2019/08/22/the-new-facebook-graph-search-part-1/)
7. [The new Facebook Graph Search – part 2](https://osintcurio.us/2019/08/22/the-new-facebook-graph-search-part-2/)
8. [Facebook Search](https://sowdust.github.io/fb-search/)
9. [Facebook Search](https://graph.tips/beta/)
10. [Facebook Scanner | Stalk Scan Facebook](https://www.spoofbox.com/de/tool/stalk-scan)
11. [Facebook: Classic vs New – Which is Better for OSINT?](https://osintcurio.us/2020/08/19/facebook-classic-vs-new-which-is-better-for-osint/)
12. [Intelx Facebook Searcher](https://intelx.io/tools?tab=facebook)
13. [How to find someone on Facebook? - News & Article](https://usersearch.org/updates/2022/02/04/how-to-find-someone-on-facebook/?utm_campaign=02-10-22+-+How+to+find+som&utm_source=Message&utm_medium=Twitter)
14. [Find my Facebook ID](https://lookup-id.com/)
----

### Twitter
1. [Twitter Advanced Search](https://twitter.com/search-advanced)
2. [How to Use Twitter's Advanced Search Parameters to Uncover Relevant Mentions | Social Media Today](https://www.socialmediatoday.com/social-networks/how-use-twitters-advanced-search-parameters-uncover-relevant-mentions)
3. [Advanced Twitter Search Cheat Sheet](https://twitter.com/OsintStash/status/1075397222710284288)
4. [Twitter Tools](https://intelx.io/tools?tab=twitter)
5. [Muting the Twitter algorithm and using basic search operators for better OSINT research](https://osintcurio.us/2019/08/01/muting-the-twitter-algorithm-and-using-basic-search-operators-for-better-osint-research/)
6. [Sleeping Time](http://sleepingtime.org)
7. [Tweetdeck](https://tweetdeck.twitter.com/)
8. [Twitter Analytics by Foller.me](https://foller.me/)
9. [Socialbearing](https://socialbearing.com/)
10. [Allegedly](https://makeadverbsgreatagain.org/allegedly/)
11. [TweetBeaver](https://tweetbeaver.com)
12. [Twitonomy](https://twitonomy.com/)
13. [Spoonbill - Account Information Changes](https://spoonbill.io/)
14. [tinfoleak](https://tinfoleak.com)
15. [whotwi](https://en.whotwi.com/)
16. [Thread Reader App / Unroll an Thread](https://twitter.com/threadreaderapp)
17. [5 Best Ways to Save Twitter Threads in 2021 (Guide) | Beebom](https://beebom.com/save-twitter-threads/)
18. [unroll compile and save twitter threads](https://twitter.com/cyb_detective/status/1480299146145939460)
19. [Search operators](https://developer.twitter.com/en/docs/twitter-api/v1/rules-and-filtering/search-operators)
20. [Twitter Profile Investigations - what can you find? - News & Article](https://usersearch.org/updates/2021/11/07/twitter-profile-investigations-what-can-you-find/?utm_campaign=02-13-22+-+Twitter+Profile&utm_source=Message&utm_medium=Twitter)
21. [Twitter Video Downloader Online](https://twdown.net/)
22. [TweepDiff](https://tweepdiff.com)
23. [Treeverse – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/en-US/firefox/addon/treeverse/)
24. [Followerwonk](https://followerwonk.com/)
25. [Twitterfall](https://twitterfall.com/)
26. [TweeterID](https://tweeterid.com/)
----

### TikTok
1. [Investigate TikTok Like A Pro!](https://www.bellingcat.com/resources/2020/05/25/investigate-tiktok-like-a-pro/)
2. [sinwindie TikTok Investigation](https://github.com/sinwindie/OSINT/blob/master/TikTok/OSINT%20Investigations%20on%20TikTok.pdf)
3. [TikTok Quick Search](https://www.osintcombine.com/tiktok-quick-search)
----

### Instagram
1. [Searching Instagram](https://osintcurio.us/2019/07/16/searching-instagram/)
2. [Searchmy.bio](https://www.searchmy.bio/)
3. [Instagram Explorer](https://www.osintcombine.com/instagram-explorer)
4. [Find Instagram User ID](https://www.instafollowers.co/find-instagram-user-id)
5. [Find My Instagram User ID](https://commentpicker.com/instagram-user-id.php)
6. [InstaZoom - Instagram Profilbild vergrößern - Schneller pfp zoom!](https://instazoomer.de/de)
7. [Instagram Profilbilder vergrößern](https://izoomyou.com)
8. [Instagram story viewer and stalker online](https://dumpor.com/)
9. [Instagram  viewer + cached pages](https://www.picuki.com/)
10. [megadose/toutatis](https://github.com/megadose/toutatis)
11. [Datalux/Osintgram](https://github.com/Datalux/Osintgram)
12. [How to find a user on Instagram? - News & Article](https://usersearch.org/updates/2022/01/17/how-to-find-a-user-on-instagram/)
13. [InstaHunt](https://instahunt.co/)
14. [How to Search Multiple Hashtags on Instagram | Social ProsHow to Search Multiple Hashtags on Instagram | Social Pros](https://socialpros.co/search-multiple-hashtags-on-instagram/)
15. [Instagram Downloader (Photo/ Video/ Story/ Bulk) – Holen Sie sich diese Erweiterung für 🦊 Firefox (de)](https://addons.mozilla.org/de/firefox/addon/instagram_download/)
16. [Instagram Downloader, Download Video, Photo, Reels, IGTV online - SnapInsta](https://snapinsta.app/)
17. [Downloader for Instagram - Chrome Web Store](https://chrome.google.com/webstore/detail/downloader-for-instagram/efjchbcjcajmmjkoaneomgkphanidfan)
18. [Instagram search tool](https://www.aware-online.com/en/osint-tools/instagram-search-tool/)
19. [Instagram analyzer and viewer](https://gramhir.com/)
20. [Pixwox: Instagram person search, viewer and downloader](https://www.pixwox.com)
----

### Whats App
1. [WhatsApp Dorks And Tricks – NixIntel](https://nixintel.info/osint/whatsapp-dorks-and-tricks/)
2. [Fake WhatsApp Chat Generator](https://www.fakewhats.com/generator)
----

### Skype
1. [Skypli.com](https://www.skypli.com/)
2. [Skype – A hidden OSINT goldmine](https://whitehatinspector.blogspot.com/2021/03/skype-hidden-osint-goldmine.html)
----

### Youtube
1. [YouTube Metadata](https://mattw.io/youtube-metadata/)
2. [Extract Meta Data](https://citizenevidence.amnestyusa.org/)
3. [12 Youtube erweiterte Suche Tricks](https://www.searchtricks.net/youtube-erweiterte-suche/)
4. [Grabbing Videos For OSINT – How To Use YouTube-DL – NixIntel](https://nixintel.info/linux/grabbing-videos-for-osint-how-to-use-youtube-dl/)
5. [youtube-dlg](https://mrs0m30n3.github.io/youtube-dl-gui/)
6. [yt-dlp/yt-dlp](https://github.com/yt-dlp/yt-dlp)
7. [Yout.com - Youtube Video Download](https://yout.com/video/)
8. [Online Video Downloader](https://de.savefrom.net/)
9. [KeepVid.works](https://keepvid.works/)
10. [Keepvid](https://keepvid.com/)
11. [Convert2MP3 - YouTube to MP3 und MP4 converter](https://convert2mp3.tv/de/)
12. [Altersverifikation bei Youtube umgehen](https://www.privacytutor.de/vpn/streaming/youtube-vpn/youtube-altersbeschraenkung-umgehen/#:~:text=YouTube%20Altersbeschr%C3%A4nkung%20mit%20VPN%20%26%20Google%2DAccount%20umgehen&text=Hier%20reicht%20es%20aus%2C%20wenn,Standort%20anhand%20deiner%20IP%2DAdresse.)
13. [JDownloader.org](https://jdownloader.org/jdownloader2)
14. [Open Broadcaster Software](https://obsproject.com/)
15. [Bildschirm aufnehmen mit Windows 10](https://www.heise.de/tipps-tricks/Bildschirm-aufnehmen-mit-Windows-10-4226449.html)
16. [AdBlocker for YouTube™](https://addons.mozilla.org/en-US/firefox/addon/adblock-for-youtube/)
17. [Addons für Video Download](https://addons.mozilla.org/de/firefox/search/?q=youtube%20download)
18. [YCS - YouTube Comment Search](https://addons.mozilla.org/de/firefox/addon/ycs/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)
19. [Youtube Comment Search](https://chrome.google.com/webstore/detail/ycs-youtube-comment-searc/pmfhcilikeembgbiadjiojgfgcfbcoaa)
20. [Adblock für Youtube™ - Chrome Web Store](https://chrome.google.com/webstore/detail/adblock-for-youtube/cmedhionkhpnakcndndgjdbohmhepckk?hl=de)
21. [Video Downloader Chrome](https://chrome.google.com/webstore/search/download%20video?hl=de)
22. [YouTube Timestamp Comments - Chrome Web Store](https://chrome.google.com/webstore/detail/youtube-timestamp-comment/khngjoedfeicfbjlcfmiigbokbnlibei/)
23. [Youtube™ Actual Top Comments - Chrome Web Store](https://chrome.google.com/webstore/detail/youtube-actual-top-commen/hbdmelobmfcompinikjdaiphhonbgfpn/)
24. [Mattw.io](https://mattw.io)
25. [Find geotagged Videos on Map](https://mattw.io/youtube-geofind/)
26. [Freetubeapp.io](https://freetubeapp.io/)
27. [Bitdownloader.io](https://bitdownloader.io/)
----

### Telegram
1. [Telegram Search. Search for posts](https://tgstat.ru/en/search)
2. [Suche in offenen Telegram Kanälen](https://search.buzz.im/)
3. [Telegram Chats exportieren: Verlauf und Nachrichten sichern](https://noobguides.de/telegram-chats-exportieren-verlauf-und-nachrichten-sichern/)
4. [Tools](https://intelx.io/tools?tab=telegram)
5. [TLGRM.eu](https://tlgrm.eu)
6. [Awesome List-Telegram-OSINT](https://github.com/ItIsMeCall911/Awesome-Telegram-OSINT)
7. [Bellingcat: Open source investigations and Telegram - Google Docs](https://docs.google.com/document/d/1Xao1ELH2lDojAqrn4737rCk8Nr4bQENLVonZZ7cQSJM/edit)
8. [Telegram Fundamentals – OSINT, OPSEC, Privacy & Hostile Profiling](https://www.cqcore.uk/telegram-fundamentals/)
9. [Tgstat.com](https://tgstat.com/)
10. [Telegram video Download](https://pastedownload.com/t)
11. [How to Archive Telegram Content to Document Russia's Invasion of Ukraine - bellingcatHow to Archive Telegram Content to Document Russia's Invasion of Ukraine - bellingcat](https://www.bellingcat.com/resources/how-tos/2022/03/08/how-to-archive-telegram-content-to-document-russias-invasion-of-ukraine/)
12. [Telemetr.io Telegram Channels](http://telemetr.io)
----

### Snapchat
1. [Using Snapchat For OSINT  – 10 Minute Tip – We are OSINTCurio.us](https://osintcurio.us/2020/04/13/using-snapchat-for-osint-10-minute-tip/)
2. [Snapchat Multi-Viewer](https://www.osintcombine.com/snapchat-multi-viewer)
3. [Snapdex](https://snapdex.com)
4. [Using Snapchat for OSINT – part 2](https://osintcurio.us/2021/02/26/using-snapchat-for-osint-part-2/)
----

### Linkedin
1. [Linkedin Search](https://intelx.io/tools?tab=linkedin)
2. [Getting your sock puppet connections on LinkedIn](https://osintcurio.us/2021/05/20/getting-your-sock-puppet-connections-on-linkedin/)
----

### Google
1. [Getting a Grasp on GoogleID’s](https://medium.com/week-in-osint/getting-a-grasp-on-googleids-77a8ab707e43)
2. [Email Lookup](https://tools.epieos.com/email.php)
3. [mxrch/GHunt](https://github.com/mxrch/GHunt)
4. [Gmail Account to People Chip](https://twitter.com/DailyOsint/status/1499033979399376904)
----

### Reddit
1. [Search Reddit](https://bmai.dev/reddit/)
2. [Reddit Search](https://camas.unddit.com/)
----

### Weitere SN
1. [FuPa (Amateur Fußballer Deutschland)](https://FuPa.net)
2. [Strava (Renn- / Radlerstrecken Community)](https://www.strava.com/?hl=de)
3. [Garmin Connect](https://connect.garmin.com)
4. [You Can Run, But You Can’t Hide – We are OSINTCurio.us](https://osintcurio.us/2022/01/24/you-can-run-but-you-cant-hide/)
5. [OSINT: How to find someone on OnlyFans? - News & Article](https://usersearch.org/updates/2022/02/11/osint-how-to-find-someone-on-onlyfans/?utm_campaign=02-14-22+-+OSINT%3A+How+to+f&utm_source=Message&utm_medium=Twitter)
6. [SMAT - Social Media Analysis Toolkit](https://www.smat-app.com/)
7. [VK Hunt öffentliche VK Profile durchsuchen](https://twitter.com/TRADECRAFT14/status/1554850536821751813)
----

