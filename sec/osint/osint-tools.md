### TOOLS - Website/Domain Records/IoT
1. [Who Is](https://www.whois.net/)
2. [Domain Search](https://hunter.io/search)
3. [Wayback Machine](http://archive.org/web/web.php)
4. [Wayback Machine](http://archive.org/web/web.php)
5. [Website checker, website change detection, monitoring and alerts](https://visualping.io/cdlp.html?utm_source=cd&utm_medium=redirtocdlp&utm_campaign=mig1)
6. [Cached Pages](http://www.cachedpages.com/)
7. [Shodan](https://www.shodan.io/)
8. [My IP Neighbors](http://www.my-ip-neighbors.com/)
9. [WhatIsMyIPAddress.com](https://whatismyipaddress.com)
10. [Investigator](https://abhijithb200.github.io/investigator/?utm_campaign=The%20OSINT%20Newsletter&utm_medium=email&utm_source=Revue%20newsletter)
----

### TOOLS - Data Visualization
1. [Draw.io](https://www.draw.io/)
2. [Datawrapper](https://www.datawrapper.de/)
3. [Kibana](https://www.elastic.co/products/kibana)
4. [Data Viz Catalogue](https://datavizcatalogue.com/)
5. [RAWGraphs](http://app.rawgraphs.io/)
6. [Visual Investigative Scenarios](https://vis.occrp.org/)
7. [Text2MindMap](https://tobloef.com/text2mindmap/)
----

### Tools - Abuse
1. [Cyberbullying.org/report](https://cyberbullying.org/report)
2. [Datacentrumgids (Benelux)](https://www.datacentrumgids.nl/)
3. [Gethuman (USA)](http://gethuman.com)
4. [Meldknop.nl - report abuse (NL)](https://meldknop.nl)
5. [Veiliginternetten.nl (NL)](https://veiliginternette.nl)
----

### Tools - Android emulators
1. [Bluestacks](https://www.bluestacks.com/nl/index.html)
2. [Genymotion](https://www.genymotion.com/)
3. [Leapdroid](https://leapdroid.en.softonic.com/)
4. [Nox](https://www.bignox.com/)
----

### Tools - Darkweb
1. [Deepdotweb - List of most popular darkmarkets](https://www.deepdotweb.com/2013/10/28/updated-llist-of-hidden-marketplaces-tor-i2p/)
2. [DeepWeb - Deeplinks](https://www.deepweb-sites.com/deep-web-links-2015/)
3. [DeepWeb - Top 50 onion-sites](https://www.deepweb-sites.com/top-50-dark-web-onion-domains-pagerank/)
4. [Hunchly's Darkweb report](https://www.dropbox.com/s/dvm23v5dhoe1cz4/HiddenServices.xlsx?dl=0&utm_content=buffer7f07c&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
5. [i2p (download)](https://geti2p.net/en/)
6. [Ichidan (visit via Tor)](http://ichidanv34wrx7m7.onion)
7. [Onionscan](https://onionscan.org/)
8. [OnionShare](https://blog.torproject.org/tor-heart-onionshare)
9. [Tor (download)](https://www.torproject.org/index.html.en)
10. [TorNodes](https://www.dan.me.uk/tornodes)
11. [Zeronet (download)](https://zeronet.io/)
----

### Tools - Email
1. [Email-format](https://email-format.com/)
2. [Gmail](https://gmail.com)
3. [Guerillamail](http://www.guerrillamail.com/)
4. [Hunter.io](http://hunter.io)
5. [Hushmail](http://hushmail.com)
6. [Inboxbear - Passwordless email addresses](http://inboxbear.com)
7. [IntelTechniques - Email](https://inteltechniques.com/osint/email.search.html)
8. [Mail.com - free email creations](https://mail.com)
9. [Mailinator](https://www.mailinator.com/)
10. [Manycontacts](https://www.manycontacts.com/)
11. [Protonmail](http://protonmail.com)
12. [Verify an email address - Hunter.io](https://hunter.io/email-verifier)
13. [Verify an email address - Mailhippo](https://tools.verifyemailaddress.io/)
14. [Verify an email address - Mailtester](http://www.mailtester.com)
15. [Verify an email address - Verifalia](http://verifalia.com/validate-email)
----

### Tools - Github
1. [Zen](https://github.com/s0md3v/Zen)
2. [CSE for search 48 pastebin sites](https://cipher387.github.io/pastebinsearchengines/)
----

### Tools - Hash
1. [Online Password Hash Crack](http://www.onlinehashcrack.com)
----

### Tools  - Images and Documents
1. [Exif viewer tool](http://owl.phy.queensu.ca/~phil/exiftool/)
2. [File Extensions](https://www.file-extensions.org)
3. [Foca](https://www.elevenpaths.com/labstools/foca/index.html)
4. [Forensically](https://29a.ch/photo-forensics/#level-sweep)
5. [Get-metadata.com](https://www.get-metadata.com/)
6. [Ghiro](http://www.getghiro.org/)
7. [ImageIdentify](https://www.imageidentify.com)
8. [InVid - Verify fake videos](http://www.invid-project.eu/tools-and-services/invid-verification-plugin/)
9. [Izitru.com](http://izitru.com)
10. [Online convert](https://www.online-convert.com/)
11. [PDF Redact Tools](https://github.com/firstlookmedia/pdf-redact-tools)
12. [Remove background](https://www.remove.bg/)
13. [Spiderpig - metadata on documents](https://github.com/hatlord/Spiderpig)
14. [SunCallculation](http://suncalc.net/)
15. [Text-compare](https://text-compare.com/)
16. [ThoughtfulDev/EagleEye](https://github.com/ThoughtfulDev/EagleEye)
17. [MoonCalc moon position- and moon phases calculator](https://www.mooncalc.org/?msclkid=14379814d13311eca31a14b46e2008ff#/49.495,11.073,3/2022.05.11/10:03/1/3)
----

### Tools - Leaks
1. [LeakScraper](https://github.com/Acceis/leakScraper/wiki/leakScraper)
----

### Tools - Maps
1. [Follow Your World](https://followyourworld.appspot.com/)
2. [FoodnDrink](https://github.com/WebBreacher/foodndrink)
3. [Microsoft/USBuildingFootprints](https://github.com/Microsoft/USBuildingFootprints/)
4. [RoboSat](https://github.com/mapbox/robosat)
5. [KartaView](https://kartaview.org/map/@-6.407518221863476,107.04770438649952,8z)
----

### Tools - OSINT
1. [101+ OSINT resources](https://i-sight.com/resources/101-osint-resources-for-investigators/)
2. [AnswerThePublic](https://answerthepublic.com/)
3. [Bellingcat - OSINT Links](https://docs.google.com/document/d/1BfLPJpRtyq4RFtHJoNpvWQjmGnyVkfE2HYoICKOGguA/edit)
4. [Browsershots](http://browsershots.org)
5. [Browsershots (screenshot via mobile device)](http://browsershots.net/)
6. [Cheatsheet Bing search (NL)](https://drive.google.com/file/d/0B7IvVbyx3btlajl2aDJrVVJIOUE/view)
7. [Cheatsheet Duckduckgo search (NL)](https://drive.google.com/file/d/0B7IvVbyx3btldkpYYmRySndVQTA/view)
8. [Cheatsheet Yandex search(NL)](https://drive.google.com/file/d/0B7IvVbyx3btldktiNC1YelZaXzA/view)
9. [CloudFail - find IPs behind Cloudflare](https://github.com/m0rtem/CloudFail)
10. [Cyberchef](https://gchq.github.io/CyberChef/)
11. [Databasic](http://databasic.io)
12. [Datawrapper](http://datawrapper.de)
13. [DFIR Training - Various tools](https://www.dfir.training/tools/osint/)
14. [Digital Methods Initiative Tool database](https://wiki.digitalmethods.net/Dmi/ToolDatabase)
15. [Facepluspus (morphing)](https://www.faceplusplus.com/face-merging/)
16. [Fakenamegenerator](http://www.fakenamegenerator.com)
17. [FakeSpot - identify fake reviews](https://www.fakespot.com/)
18. [Flowchart Domains](https://inteltechniques.com/data/Domain.png)
19. [Flowchart Location search](https://inteltechniques.com/data/location.png)
20. [Flowchart OSINT](https://inteltechniques.com/menu.links.html)
21. [Followthatpage.com](https://followthatpage.com/)
22. [Hunchly](http://hunch.ly)
23. [Keep Google - Google notes](https://keep.google.com)
24. [Listly](https://listly.io/)
25. [Maltego - CaseFile](https://www.paterva.com/web7/buy/maltego-clients/casefile.php)
26. [Microsoft Steps Recorder](https://support.microsoft.com/en-us/help/22878/windows-10-record-steps)
27. [MidaSearch OSINT websites](https://midasearch.org/osint-websites/)
28. [Mindmap OSINT Digital Intelligence](https://atlas.mindmup.com/digintel/digital_intelligence_training/index.html)
29. [MindMup](http://mindmup.com)
30. [Morphing photos](http://www.morphthing.com/)
31. [OSINTframework](https://osintframework.com/)
32. [OSINTframework by @CryptoCypher](https://pastebin.com/raw/4WFNefEi)
33. [PDFmyURL](http://pdfmyurl.com)
34. [Pentest-tools](https://pentest-tools.com/home)
35. [ResearchClinic](http://www.researchclinic.net/)
36. [SANS - Sec487 - Blue teaming wiki](https://github.com/sans-blue-team/sec487-wiki/blob/master/index.md)
37. [Sken.io](https://www.sken.io/)
38. [Spiderfoot](http://spiderfoot.net)
39. [Station](https://getstation.com/)
40. [UK OSINT - helpfull tools](http://www.uk-osint.net/)
41. [Visualize your investigation](https://vis.occrp.org/)
42. [Xray OSINT Gathering tool](https://n0where.net/network-osint-gathering-tool-xray)
----

### Tools - Phone numbers
1. [Receive SMS (free)](http://receive-sms-online.com/)
2. [Receive-SMS (free)](https://receive-sms.com/)
3. [Receive SMS (free)](http://sms.sellaite.com/)
4. [Receive SMS Online](https://www.receivesmsonline.net/)
----

### Tools - Privacy
1. [BrowserLeaks.com](https://browserleaks.com/)
2. [Downforeveryoneorjustme](http://downforeveryoneorjustme.com/)
3. [ipleak.net](https://ipleak.net/)
4. [Mijnonlineidentiteit (NL)](https://www.mijnonlineidentiteit.nl/social-media-privacy-instellingen/)
5. [OptOut Doc by @OsintNinja](https://docs.google.com/spreadsheets/d/1UY9U2CJ8Rnz0CBrNu2iGV3yoG0nLR8mLINcnz44XESI/edit#gid=1864750866)
6. [Pribot (AI-powered Privacy/Policies)](https://pribot.org/)
7. [Privacy Tools](https://epic.org/privacy/tools.html)
8. [Private Internet Access](https://www.privateinternetaccess.com)
9. [ProtonVPN](https://protonvpn.com/)
10. [Proxysnel](http://proxysnel.nl)
11. [Safeweb.norton.com](http://safeweb.norton.com)
12. [Should I Trust](https://github.com/ericalexanderorg/should-i-trust)
13. [Sucuri Security](http://sitecheck.sucuri.net)
14. [Terms of Service; Didn't Read](https://tosdr.org/)
15. [TwoFactorAuthentication](https://twofactorauth.org/)
16. [Verexif - remove exif data from pictures](http://www.verexif.com/en/)
17. [What's my User Agent?](http://www.whatsmyua.info/)
----

### Tools - Start.me-links
1. [Asint collection](https://start.me/p/b5Aow7/asint_collection)
2. [Bruno Mortier (DEU)](https://start.me/p/3g0aKK/sources)
3. [Country sorted OSINT links](https://start.me/p/W2kwBd/sources-cnty)
4. [Verification set for datingsites](https://start.me/p/VRxaj5/dating-apps-and-sites-for-investigators)
5. [Verification Toolset](https://start.me/p/ZGAzN7/verification-toolset)
----

### Tools - Social Media
1. [Facebook - FindMyFBid](https://findmyfbid.com/)
2. [Facebook - ExtractFace](http://le-tools.com/ExtractFace.html#download)
3. [Facebook - PeopleYouMayKnow](https://gizmodo.com/keep-track-of-who-facebook-thinks-you-know-with-this-ni-1819422352)
4. [Facebook - Video Downloader](https://fbdown.net/)
5. [General - Meetfranz](https://meetfranz.com/)
6. [Instagram - Filtergram](https://filtergram.app)
7. [Instagram - GramSpy](http://gramspy.com/)
8. [Instagram - Instagram downloader](https://downloadgram.com/)
9. [Instagram - Instadp.site](https://instadp.site/)
10. [Instagram - InstagramOSINT](https://github.com/sc1341/InstagramOSINT)
11. [Instagram - Instaloader](https://github.com/instaloader/instaloader)
12. [Instagram - Instalooter](https://github.com/althonos/InstaLooter)
13. [Instagram - InstaSave](https://github.com/alshf89/InstaSave)
14. [Instagram - izuum](http://izuum.com)
15. [Instagram - SaveIG](https://saveig.com/)
16. [Instagram - Stalker Stories](https://stalker-stories.com/)
17. [Instagram - StoriesIG](https://storiesig.com/)
18. [LinkedIn - LinkedIn2Username](https://github.com/initstring/linkedin2username)
19. [LinkedIn - Socilab search](http://www.socilab.com)
20. [LinkedIn - Search by email](https://www.linkedin.com/sales/gmail/profile/viewByEmail/ReplaceThis@byEmailDomain.com)
21. [Periscope - Download periscope videos](http://downloadperiscopevideos.com)
22. [Reddit - PushShift](http://files.pushshift.io/reddit/)
23. [Reddit - Reditr](http://reditr.com/)
24. [Snapchat - Grab Snaps](https://github.com/CaliAlec/snap-map-private-api)
25. [Telegram - Save history](https://github.com/pigpagnet/save-telegram-chat-history)
26. [Tumblr - Show original post](http://studiomoh.com/fun/tumblr_originals/)
27. [Twitter - Allmytweets](https://www.allmytweets.net/connect/)
28. [Twitter - Bio changed of following](http://spoonbill.io/)
29. [Twitter - Deadbird](https://keitharm.me/project/deadbird)
30. [Twitter - Download Twitter Video](http://www.downloadtwittervideo.com/)
31. [Twitter - Email Test](https://pdevesian.eu/tet)
32. [Twitter - Fakers](https://fakers.statuspeople.com)
33. [Twitter - Geotweets](http://geosocialfootprint.com/)
34. [Twitter - How far did your tweet travel?](https://tweetreach.com/)
35. [Twitter - OneMillionTweetMap](http://onemilliontweetmap.com)
36. [Twitter - Snapbird twitter history](https://snapbird.org/)
37. [Twitter - Socialbearing](https://socialbearing.com/)
38. [Twitter - Spoonbill](http://spoonbill.io/)
39. [Twitter - Tinfoleak](https://tinfoleak.com/)
40. [Twitter - Trendmap](https://www.trendsmap.com/)
41. [Twitter - Tweet Mapper](https://keitharm.me/project/tweet-mapper)
42. [Twitter - Tweetanalyzer](https://github.com/x0rz/tweets_analyzer)
43. [Twitter - Tweetbeaver](http://tweetbeaver.com/)
44. [Twitter - Tweetmap](https://www.mapd.com/demos/tweetmap/)
45. [Twitter - Tweets to Excel](http://twlets.com/)
46. [Twitter - Twiangulate](http://twiangulate.com/search/)
47. [Twitter - TwiMap](https://twimap.com/)
48. [Twitter - Twitonomy](http://www.twitonomy.com)
49. [Vkontakte - FindFace](https://findface.ru/)
50. [Vkontakte - Retreive metadata](https://gist.github.com/cryptolok/8a023875b47e20bc5e64ba8e27294261)
51. [WhatsApp - Retreive data](https://github.com/LoranKloeze/WhatsAllApp)
52. [WhatsApp - Create fake WhatsApp messages](http://www.fakewhats.com/generator)
----

### TOOLS - DEFAULT CREDENTIALS
1. [Default Passwords](https://cirt.net/passwords)
2. [Default passwords list](https://default-password.info/)
3. [Default Password Lookup Utility](https://www.fortypoundhead.com/tools_dpw.asp)
4. [Router Passwords](https://www.routerpasswords.com/)
5. [5342 Default Passwords from Open Sez Me!](https://open-sez.me/)
----

### Tools - Translation/Language
1. [Cyrcillic decoder](https://2cyr.com/decode/?lang=en)
2. [Ethnologue](https://www.ethnologue.com/)
3. [DeepL Translator](https://www.deepl.com/translator)
4. [FlockWatch](https://github.com/sjacks26/FlockWatch)
5. [Google Translate](http://translate.google.com)
6. [Know Your Meme](http://knowyourmeme.com/)
7. [NewOCR](https://www.newocr.com/)
8. [TagDef](https://tagdef.com)
9. [Urban Dictionairy](https://www.urbandictionary.com/)
10. [Yamii](https://www.yamli.com/arabic-keyboard/)
----

### Tools - URL Shortners
1. [Bit.do](https://bit.do)
2. [Bit.ly](http://bit.ly)
3. [CheckShortUrl](http://checkshorturl.com/)
4. [Grabify IP Logger](https://grabify.link/)
5. [Unfurlr](https://unfurlr.com/)
6. [Unshorten.it](http://unshorten.it)
7. [Unshorten.link](https://unshorten.link/)
----

### Tools - Verification
1. [Research Wikipedia edits](https://www.engadget.com/2018/05/06/facebook-friend-suggestions-helped-connect-extremists/?guccounter=1)
2. [Reviews - Fakespot](http://fakespot.com)
----

### Tools - Video
1. [Hugin.Sourceforge.net](http://hugin.sourceforge.net/)
2. [TubeOffline](https://www.tubeoffline.com/)
----

### Tools - Virtual Machine
1. [Buscador OSINT VM](http://inteltechniques.com/buscador)
2. [Kali Linux](https://www.kali.org/)
3. [Linux Mint](https://linuxmint.com/download.php)
4. [Linux Ubuntu](https://www.ubuntu.com/download/desktop)
5. [VirtualBox (free)](https://www.virtualbox.org/wiki/Downloads)
6. [VMWare (paid)](https://www.vmware.com/)
----

### Tools- Who.is/domain information
1. [Subdomains - CTFR (Pyhton)](https://securityonline.info/ctfr-get-the-subdomains-from-a-https-website-in-a-few-seconds/)
----

### Tools - YouTube
1. [HookTube](https://hooktube.com/)
2. [Keepvid](https://keepvid.com/)
3. [WatchFrameByFrame](http://www.watchframebyframe.com/)
4. [Yasiv](https://yasiv.com/youtube)
5. [Yout.com](https://yout.com)
6. [Youtube-DL](https://rg3.github.io/youtube-dl/)
7. [YTcomments](http://ytcomments.klostermann.ca/)
----

### Tools - WebScraping
1. [Common Crawl](https://commoncrawl.org/)
2. [GitHub](https://github.com/laramies/theHarvester)
3. [80legs – Customizable Web Scraping](https://80legs.com/)
4. [Agenty®](https://agenty.com/)
5. [Anthracite](http://freshmeat.sourceforge.net/projects/anthracite)
6. [Apify](https://apify.com/)
7. [artoo.js · The client-side scraping companion.](https://medialab.github.io/artoo/)
8. [Scrapy](https://scrapy.org/)
9. [Web Data Extraction Software](https://datatoolbar.com/)
10. [Deixto.com](https://deixto.com/)
11. [DataSift](https://datasift.com/)
12. [Email Extractor](https://chrome.google.com/webstore/detail/email-extractor/jdianbbpnakhcmfkcckaboohfgnngfcc)
----

