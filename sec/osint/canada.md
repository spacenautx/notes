### People Search
1. [411.ca](https://411.ca/)
2. [New Canada 411](https://www.canada411.ca/)
3. [People Search in Canada](https://recordsfinder.com/canada/)
4. [ARELLO Licensee Verification Database](https://www.arello.com/)
5. [411directoryassistance.ca](https://www.411directoryassistance.ca/)
6. [Provincial Archives of New Brunswick](https://archives.gnb.ca/Search/FEDS/Default.aspx?culture=en-CA)
7. [Canadian Securities Administrators](https://www.securities-administrators.ca/nrs/nrsearchprep.aspx)
8. [Dentist Registrant Lookup](https://www.cdsbc.org/home/registrant-lookup#/lookup/dentists)
9. [Search the Directory - UBC Faculty & Administrative Directory](https://www.directory.ubc.ca/index.cfm)
----

### Business Search
1. [411.ca](https://411.ca/)
2. [New Canada 411](https://www.canada411.ca/)
3. [MRAS Business Registry Search](https://beta.canadasbusinessregistries.ca/search)
4. [City of Richmond BC - Search the Business Directory](https://www.richmond.ca/busdev/directory/search/bdsearch.aspx)
----

### Address
1. [Find a Postal Code / Address](https://www.canadapost.ca/info/mc/personal/postalcode/fpc.jsf)
2. [Permits - City of Vancouver - Application / Permit Search By Address](https://plposweb.vancouver.ca/Public/Default.aspx?PossePresentation=PermitSearchByAddress)
3. [BC Assessment](https://www.bcassessment.ca/)
----

### Court Records
1. [CSO](https://justice.gov.bc.ca/cso/index.do)
2. [Canadian Legal Information Institute](https://www.canlii.org/en/)
3. [Federal Court](https://decisions.fct-cf.gc.ca/fc-cf/en/d/s/index.do?col=54)
4. [NCRA Sourcebook](https://portal.ncra.org/Sourcebook)
5. [Manitoba Court Records](https://web43.gov.mb.ca/Registry/NameSearch)
6. [Supreme Court](https://scc-csc.lexum.com/scc-csc/fr/nav.do)
7. [NB Courts - Public Self-Serve Website](https://www1.gnb.ca/nota/Default.aspx?lang=en-US)
8. [The Courts of British Columbia](https://www.bccourts.ca/search_judgments.aspx)
9. [The Courts of British Columbia - Home](https://www.bccourts.ca/index.aspx)
----

### Maps
1. [VicMap - Victoria](http://vicmap.victoria.ca/Html5Viewer/index.html?viewer=Public)
2. [Recreation Sites and Trails BC](http://www.sitesandtrailsbc.ca/search/search-location.aspx)
----

### Aviation
1. [Authorized Person - Flight Crew Licensing Search](https://wwwapps.tc.gc.ca/saf-sec-sur/2/CAS-SAC/apfcls.aspx)
2. [CCARCS-RIACC - Advanced Search](https://wwwapps.tc.gc.ca/Saf-Sec-Sur/2/CCARCS-RIACC/RchAvc.aspx)
3. [CCARCS-RIACC - Historical Search](https://wwwapps.tc.gc.ca/Saf-Sec-Sur/2/CCARCS-RIACC/RchHs.aspx)
----

### Public Records
1. [Royal BC Museum Archives](http://search-collections.royalbcmuseum.bc.ca/Genealogy)
2. [OPENGOVCA: Open Government Data in Canada](https://opengovca.com/)
3. [Canada Public Records Free Search | SearchSystems.net](https://publicrecords.searchsystems.net/Canada_Free_Public_Records/)
4. [Municipal Assessment Agency - Appeals](https://www.maa.ca/appeals/)
5. [Unclaimed Dividends Search -  Bankruptcy - Industry Canada](https://www.ic.gc.ca/app/scr/osb-bsf/ufd/search.html)
6. [Canadian Police Information Centre](https://www.cpic-cipc.ca/index-eng.htm)
7. [Home - Royal BC Museum and Archives Library](https://rbcm.catalogue.libraries.coop/eg/opac/home?physical_loc=488&locg=)
8. [Search Bibliothèque et Archives Canada / Library and Archives Canada](https://bac-lac.on.worldcat.org/discovery?lang=en)
9. [ArchivesCanada](https://archivescanada.accesstomemory.ca/actor/browse)
10. [New Westminster Archives Advanced Search Page](http://archives.newwestcity.ca/search.aspx)
11. [Ontario Securities Warning List](https://www.osc.gov.on.ca/en/Investors_warning-list_index.htm)
12. [IIROC Search](https://www.iiroc.ca/IIROCSearch/Pages/Search_Results_Disciplinary_Cases.aspx#k=)
13. [Brantford Public Library](https://www.brantfordlibrary.ca/Modules/History/BMD/Search.aspx)
14. [City of Richmond BC - Advanced Search All Records](http://archives.richmond.ca/archives/descriptions/advanced-search.aspx)
----

### Vessels
1. [Vessel Registration Query System](https://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/vrqs-srib/eng/vessel-registrations)
2. [Vessel database query](https://www.nauticapedia.ca/dbase/Query/dbsubmit_Vessel.php)
----

### Deaths
1. [Cemeteries :: City of Edmonton](http://coewebapps.edmonton.ca/cemeteries/default.aspx)
----

