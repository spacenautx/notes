### AOCs (Agencies, Organizations, and Companies)
1. [Aseanmp](https://aseanmp.org/publications-2/)
2. [ASIS](https://www.asisonline.org/publications--resources/security-topics/active-shooter/)
3. [Combating Terrorism Center at West Point](https://ctc.usma.edu/reports/?type=major-reports)
4. [Constellis](https://constellis.com/press-releases)
5. [DIA](https://www.dia.mil/Military-Power-Publications/)
6. [DGAP](https://dgap.org/en)
7. [Federation Of American Scientists](https://fas.org/publications/)
8. [Global Coalition to Protect Education from Attack](http://www.protectingeducation.org/resources)
9. [IISS](https://www.iiss.org/)
10. [ICCT](https://icct.nl/)
11. [Economicsandpeace](http://economicsandpeace.org/)
12. [ISD](https://www.isdglobal.org/)
13. [International SOS](https://www.internationalsos.com/)
14. [ICSR](https://icsr.info/publications/)
15. [Journal for Deradicalization](http://journals.sfu.ca/jd/index.php/jd)
16. [Research Publications](https://www.migrationpolicy.org/research)
17. [migrationpolicy](https://www.minderoo.com.au/walk-free/?utm_medium=301&utm_source=www.walkfreefoundation.org)
18. [DNI](https://www.dni.gov/index.php/nctc-newsroom)
19. [Public Safety Canada](https://www.publicsafety.gc.ca/cnt/rsrcs/pblctns/index-en.aspx)
20. [RAN](https://ec.europa.eu/home-affairs/e-library/multimedia_en?qt-multimedia=2#qt-multimedia)
21. [Risk Advisory](https://www.riskadvisory.com/products/sias/)
22. [RUSI](https://rusi.org/)
23. [The Lancet](https://www.thelancet.com/journals/lancet/home)
24. [UN Human Rights Commission](https://data2.unhcr.org/en/search?type%5B%5D=document&doc_type%5B%5D=7&maps=1)
25. [UNODC](https://www.unodc.org/unodc/en/publications.html?ref=menutop)
26. [World Resource Institute](https://www.wri.org/our-work/project/world-resources-report/publications)
----

### Popular
1. [IBM News Dashboard](https://news-explorer.mybluemix.net/?query=Current%20Events&type=unconstrained)
2. [The Counterterrorism group](https://www.counterterrorismgroup.com/)
3. [LinkedIn](https://www.linkedin.com/company/35635890)
4. [Google](https://google.com)
5. [Bing](https://bing.start.me/?a=gsb_startme_00_00_ssg01)
6. [Yahoo! Search](https://yahoo.start.me/?a=wsp_startme_00_00_ssg02)
7. [Youtube](http://www.youtube.com)
8. [Al Jazeera](https://www.aljazeera.com/)
9. [Sky News](https://news.sky.com/#)
10. [Crisis24 Dashboard](https://www.garda.com/crisis24)
11. [National Public Radio (NPR)](https://www.npr.org/)
12. [https://www.liveleak.com/c/liveleakers](https://www.liveleak.com/c/liveleakers)
13. [Research | Wilson Center](https://www.wilsoncenter.org/research?topic=193)
14. [The Wall Street Journal](https://www.wsj.com/)
15. [News | Military Times](https://www.militarytimes.com/news/)
16. [Council on Foreign Relations](https://www.cfr.org/)
17. [Terrorist Attacks](https://storymaps.esri.com/stories/terrorist-attacks/)
18. [Google Earth](https://www.google.com/earth/)
19. [UN News](https://news.un.org/en/)
20. [Snap Map](https://map.snapchat.com/@23.601559,-51.403050,2.00z)
21. [Newspapers Online and Worldwide](https://www.thebigproject.co.uk/news/index.htm)
22. [Worldpress](https://www.worldpress.org/)
23. [refdesk.com](https://www.refdesk.com/paper.html)
24. [Reuters](https://www.reuters.com/)
25. [Public Radio International](https://www.pri.org/)
26. [Today's Front Pages](https://www.newseum.org/todaysfrontpages/)
27. [VOA](https://www.voanews.com/)
28. [Foreign Affairs](https://www.foreignaffairs.com/?cid=int-gna)
29. [GlobalSecurity.org](https://www.globalsecurity.org/index.html)
30. [Periscope](https://www.pscp.tv/)
----

### News
1. [The Washington Post](https://www.washingtonpost.com)
2. [The White House](https://www.whitehouse.gov)
3. [PBS](https://www.pbs.org)
4. [The Wall Street Journal](https://www.wsj.com)
5. [National Public Radio (NPR)](https://www.npr.org)
----

### Weather
1. [The Weather Channel](https://www.weather.com)
2. [National Weather Service](https://forecast.weather.gov)
3. [National Hurricane Center](https://nhc.noaa.gov)
----

### BIAS / FACT Chek
1. [Ground News](https://ground.news/)
----

### Intelligence & Analysis
1. [The Red (Team) Analysis Society](https://www.redanalysis.org/)
2. [AFIO](https://www.afio.com/)
3. [Intelligence 101](http://www.intelligence101.com/category/intelligence-101/)
4. [IntelCenter](https://intelcenter.com/)
5. [Jiox](https://jiox.blogspot.com/)
6. [U.S. Senate Select Committee on Intelligence](https://www.intelligence.senate.gov/)
----

### Homeland Security / National Security
1. [House of Homeland Security](https://homeland.house.gov/)
2. [HSAJ](https://www.hsaj.org/articles/category/volume-xiv)
3. [WSIN](https://www.riss.net/centers/wsin/)
4. [Inpublicsafety](https://inpublicsafety.com/)
5. [Homeland Security Digital Library at NPS](https://www.hsdl.org/c/)
6. [Lawfare](https://www.lawfareblog.com/)
7. [Carnegie Endowment for International Peace](https://carnegieendowment.org/)
8. [RAND Corporation](https://www.rand.org/)
9. [Tufts](https://fletcher.tufts.edu/research-action/centers-chairs-programs)
10. [Wilson Center](https://www.wilsoncenter.org/)
----

### IC & Parnters
1. [NATO Map](https://www.nato.int/nato-on-the-map/#lat=51.72673918960763&lon=4.849117014409103&zoom=0&layer-5)
2. [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
3. [U.S. Department of State](https://www.state.gov/)
4. [Office of the director of National Intelligence](https://www.dni.gov/)
5. [Combating Terrorism Center at West Point](https://ctc.usma.edu/)
6. [https://www.un.org/sc/ctc/](https://www.un.org/sc/ctc/)
7. [USAGM](https://www.usagm.gov/news-and-information/threats-to-press/)
8. [Office of Foreign Assets Control - Sanctions Programs and InformationUntitled 1Untitled 1Untitled 1Untitled 1Untitled 1](https://www.treasury.gov/resource-center/sanctions/pages/default.aspx)
----

### World T.V.
1. [wwiTV](https://wwitv.com/portal.htm)
2. [En.satexpat.com](http://en.satexpat.com/)
3. [Periscope](https://www.pscp.tv/)
----

### Databases
1. [UCDP](https://www.ucdp.uu.se/)
2. [Global Modern Slavery Directory](https://www.globalmodernslavery.org/)
3. [FSI](https://cisac.fsi.stanford.edu/mappingmilitants)
----

