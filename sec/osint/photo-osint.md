### SEARCH PEOPLES BY PHOTO:
1. [Search4faces (face identification)](https://search4faces.com/)
2. [Vk.watch (face identification)](https://vk.watch/)
3. [Eye of God (face identification)](https://eyeofgod.global/)
4. [Findclone (face identification)](https://findclone.ru/)
5. [Beyne.ai (face identification)](https://beyne.ai/)
6. [PimEyes (face identification)](https://pimeyes.com/en/)
7. [Smart_SearchBot (face identification)](https://t.me/ssb_russian_probiv_bot)
8. [Quick OSINT (face identification)](https://t.me/Quick_OSINT_bot)
9. [Findfacerobot (face identification)](https://t.me/findfacerobot)
10. [PinFaceBot (face identification)](https://t.me/PinFaceBot)
11. [Face Finder Engine (face identification)](https://www.robots.ox.ac.uk/~vgg/software/vff/)
12. [Photo Sherlock (image search)](https://photosherlock.com/)
13. [Yandex Pictures (image search)](https://yandex.ru/images/)
14. [Google Images (image search)](https://images.google.com/)
15. [Mail.ru (image search)](https://go.mail.ru/search_images)
16. [Image Search Engine (image search)](https://www.robots.ox.ac.uk/~vgg/software/vise/)
17. [Search by Image (Chrome Ext)](https://chrome.google.com/webstore/detail/search-by-image/cnojnbdhbhnkbcieeekonklommdnndci)
18. [TinEye Reverse (Chrome Ext)](https://chrome.google.com/webstore/detail/tineye-reverse-image-sear/haebnnbpedcbhciplfhjjkbafijpncjl)
19. [face_recognition (open source)](https://github.com/ageitgey/face_recognition)
----

### DETERMINING AGE AND RACE:
1. [RACE TYPE](https://en.vonvon.me/quiz/9447)
2. [GENDER AND AGE](http://www.pictriev.com/)
3. [OBJECT RECOGNITION](https://www.imageidentify.com/)
4. [DEEPFAKE DETECT](https://platform.sensity.ai/login?redirect=%2Fdeepfake-detection)
5. [Deepfake GAN (deepfake detection)](https://chrome.google.com/webstore/detail/fake-profile-detector-dee/jbpcgcnnhmjmajjkgdaogpgefbnokpcc)
6. [DFSpot (deepfake detection)](https://github.com/chinmaynehate/DFSpot-Deepfake-Recognition)
7. [Weishi (deepfake detection)](http://weishi.baidu.com/product/deepfake)
8. [DeepReal (deepfake detection)](http://deepfakes.real-ai.cn)
----

### COMPARISON OF TWO FACES:
1. [Betaface free online](https://www.betafaceapi.com/demo.html)
2. [Facial Recognition](https://azure.microsoft.com/en-us/services/cognitive-services/face/)
3. [Amazon Rekognition](https://aws.amazon.com/ru/rekognition/)
4. [Image-diff (Compare Images)](https://www.diffchecker.com/image-diff/)
----

### EXTRACTING PHOTO METADATA:
1. [ExifTool](https://exiftool.org/)
2. [EXIF Data Viewer](https://www.exifdata.com/)
3. [exif-viewer](http://exif-viewer.com/)
4. [Image Metadata Viewer](http://exif.regex.info/exif.cgi)
5. [EXIF Viewer (Chrome Ext)](https://chrome.google.com/webstore/detail/exif-viewer-pro/mmbhfeiddhndihdjeganjggkmjapkffm)
6. [IPTC (Facebook metadata)](https://getpmd.iptc.org/)
7. [metapicz](http://metapicz.com/#landing)
8. [jimpl](https://jimpl.com/)
9. [Exiv2 (open source)](https://github.com/Exiv2/exiv2)
10. [Camerasummary](https://camerasummary.com/)
11. [Pic2Map (geolocation by map)](https://www.pic2map.com/)
12. [Metadata2go (geolocation by map)](https://www.metadata2go.com/)
13. [GeoImgr (geolocation by map)](https://www.geoimgr.com/)
----

### PHOTOFORENSIC TOOLS:
1. [Sherloq (open source)](https://github.com/GuidoBartoli/sherloq)
2. [imago-forensics (open source)](https://github.com/redaelli/imago-forensics)
3. [image-forensics (open source)](https://github.com/MKLab-ITI/image-forensics)
4. [Ghiro (forensic examination)](https://www.getghiro.org/)
5. [FotoForensics](http://fotoforensics.com/)
6. [Forensically (presence of edits)](https://29a.ch/photo-forensics/#error-level-analysis)
7. [Image Verification (presence of edits)](https://mever.iti.gr/forensics/)
8. [Image Edited? (presence of edits)](http://imageedited.com/)
9. [JPEGsnoop (open source)](https://github.com/ImpulseAdventure/JPEGsnoop)
10. [SEARCH BY CAMERA](http://www.stolencamerafinder.com/)
11. [SEARCH BY CAMERA](http://www.cameratrace.com/)
12. [Camera Ballistics](https://www.mobiledit.com/camera-ballistics)
----

### FACT-CHECKING TOOLS:
1. [NUMBER OF PEOPLE IN THE PHOTO](https://www.mapchecking.com/)
2. [FAKE NEWS DEBUNKER (Chrome Ext)](https://chrome.google.com/webstore/detail/fake-news-debunker-by-inv/mhccpoafgdgbhnjfhkcmgknndkeenfhe?hl=en)
3. [BIRD IDENTIFICATION](https://merlin.allaboutbirds.org/)
4. [DOG IDENTIFICATION](https://github.com/j05t/identify-dog-breeds-pro)
5. [CAR IDENTIFICATION](https://carnet.ai)
6. [PLANTS IDENTIFICATION](https://identify.plantnet.org/)
7. [MOUNTAIN IDENTIFICATION](https://www.peakfinder.org/)
8. [EARTH LINE IDENTIFICATION](https://search.descarteslabs.com/)
9. [SUN CALCULATOR](http://suncalc.net/)
10. [SHADOWMAP](https://app.shadowmap.org)
11. [LOCATION BY OBJECTS IN THE PHOTO](https://labs.tib.eu/geoestimation/)
----

### PHOTO QUALITY IMPROVEMENT:
1. [MyHeritage](https://www.myheritage.nl/photo-enhancer)
2. [Improvephoto](https://improvephoto.net/)
3. [Pinkmirror](https://pinkmirror.com/)
4. [Photoshop](https://online-fotoshop.ru/fotoredaktor-online/)
5. [waifu2x](http://waifu2x.udp.jp/)
6. [Pichance](https://pichance.com/)
7. [AI Image Upscaler](https://icons8.com/upscaler)
8. [SmartDeblur (open source)](https://github.com/Y-Vladimir/SmartDeblur)
9. [Remini Professional](http://app.remini.ai)
----

### CLEAR PHOTO BACKGROUND:
1. [Remove Background](https://www.remove.bg/ru)
2. [FocoClipping](https://www.fococlipping.com)
3. [BackgroundRemover](https://backgroundremover.app)
----

### CLEAR WATERMARKS ON PHOTO:
1. [Remove Objects](https://theinpaint.com/)
2. [Cleanup.pictures](https://cleanup.pictures/)
----

### DECRYPTION OF SIMPLE STEGANOGRAPHY:
1. [Image Steganography](https://incoherency.co.uk/image-steganography/#unhide)
2. [Steganography Online](https://stylesuxx.github.io/steganography/)
3. [iCoder](https://tools.icoder.uz/steganographic-decoder.php)
----

### RECOVERY OF TEXTS PER PIXEL:
1. [Depix (open source)](https://github.com/beurtschipper/Depix)
2. [Unredacter (open source)](https://github.com/bishopfox/unredacter)
----

### FACE MODELING:
1. [IDENTIKIT](http://foto.hotdrv.ru/fotorobot)
2. [3D Face Reconstruction (open source)](https://github.com/AaronJackson/vrn-docker/)
----

### DETECT OBJECTS ON CCTV:
1. [Watsor (open source)](https://github.com/asmirnou/watsor)
2. [Frigate (open source)](https://github.com/blakeblackshear/frigate)
----

