### Standards & Methodologies
1. [The Penetration Testing Execution Standard](http://www.pentest-standard.org/index.php/Main_Page)
2. [CybOX CTI Wiki](https://wiki.oasis-open.org/cti/cybox)
3. [MITRE ATT&CK™](https://attack.mitre.org/)
4. [STIX - Structured Threat Information Expression](https://oasis-open.github.io/cti-documentation/stix/intro)
5. [TAXII - Trusted Automated Exchange of Intelligence Information](https://oasis-open.github.io/cti-documentation/taxii/intro)
----

### APT Actors & Operations
1. [Airtable: APT Groups & Operations](https://airtable.com/shr3Po3DsZUQZY4we/tbljpA5wI1IaLI4Gv/viwGFVFtuu0l88e7u)
2. [FireEye: Advanced Persistent Threat Groups](https://www.fireeye.com/current-threats/apt-groups.html)
3. [CFR: Cyber Operations](https://www.cfr.org/interactive/cyber-operations)
4. [Malpedia: Threat Actors](https://malpedia.caad.fkie.fraunhofer.de/actors)
5. [Malpedia: Malware Families](https://malpedia.caad.fkie.fraunhofer.de/families)
----

### Translation
1. [Google Translate](https://translate.google.com/)
2. [Ethnologue](https://www.ethnologue.com/)
3. [DeepL Translator](https://www.deepl.com/translator)
----

### Recent Events
1. [CrowdStrike: Research & Threat Intel](https://www.crowdstrike.com/blog/category/threat-intel-research/)
2. [U.S. CERT Alerts](https://www.us-cert.gov/ncas/alerts)
3. [InfoSec Handlers Diary Blog](https://isc.sans.edu/diary.html)
4. [U.S. CERT Bulletins](https://www.us-cert.gov/ncas/bulletins)
5. [Kaspersky Lab: Securelist](https://securelist.com/)
6. [U.S. CERT Current Activity](https://www.us-cert.gov/ncas/current-activity)
7. [Malwarebytes Labs Blog](https://blog.malwarebytes.com/)
8. [Silobreaker Threat Reports](http://silobreaker.com/threat-reports)
9. [McAfee Labs](https://securingtomorrow.mcafee.com/category/other-blogs/mcafee-labs/)
10. [Palo Alto - Unit 42 Blogs](https://unit42.paloaltonetworks.com/category/unit42/)
11. [Talos Blog |Cisco Talos Intelligence Group](https://blog.talosintelligence.com/)
12. [Twitter Trending Topics & Hashtags](https://www.trendsmap.com/)
13. [Silobreaker: Threat Reports](https://www.silobreaker.com/threat-reports/)
----

### Threat Sharing
1. [AlienVault Open Threat Exchange](https://otx.alienvault.com/)
2. [Apiary - Community-Driven Threat Intelligence](http://apiary.gtri.gatech.edu/)
3. [CIRCL: MISP - Malware Information Sharing Platform](http://www.circl.lu/services/misp-malware-information-sharing-platform/)
4. [Facebook ThreatExchange Overview](https://developers.facebook.com/programs/threatexchange/)
5. [hashdd](https://hashdd.com/)
6. [IBM X-Force Exchange](https://www.ibm.com/us-en/marketplace/ibm-xforce-exchange)
7. [Maltiverse](https://www.maltiverse.com/dashboards/newioc)
8. [Pulsedive - IOC Sharing](https://pulsedive.com/)
9. [ThreatConnect | TC Open](https://threatconnect.com/free/)
10. [ThreatList - MatthewRoberts.io](https://www.matthewroberts.io/api/threatlist/latest)
----

### OSINT Resources
1. [Taxonomy of OSINT Sources](https://www.i-intelligence.eu/resources/taxonomy/)
2. [101+ OSINT Resources for Investigators [2019] | i-Sight](https://i-sight.com/resources/101-osint-resources-for-investigators/)
3. [Automating OSINT – Course Center](https://learn.automatingosint.com/)
4. [IntelTechniques: OSINT Search Tool](https://inteltechniques.com/menu.html)
5. [Domain Workflow: IntelTechniques](https://inteltechniques.com/data/Domain.png)
6. [Email Workflow: IntelTechniques](https://inteltechniques.com/data/Email.png)
7. [Location Workflow: IntelTechnique](https://inteltechniques.com/data/Location.png)
8. [Real Name Workflow: IntelTechniques](https://inteltechniques.com/data/Real%20Name.png)
9. [Telephone Workflow: IntelTechniques](https://inteltechniques.com/data/Telephone.png)
10. [Username Workflow: IntelTechniques](https://inteltechniques.com/data/Username.png)
11. [DataSploit application](https://github.com/DataSploit/datasploit)
12. [DFIR Tools - OSINT](https://www.dfir.training/tools/osint/)
13. [Download Star (Firefox Add-on)](https://addons.mozilla.org/en-US/firefox/addon/download-star/)
14. [GeoSocial Footprint](http://geosocialfootprint.com/)
15. [Hunchly](https://www.hunch.ly/)
16. [Omnibus: IOC Enrichment & Management](https://github.com/InQuest/Omnibus)
17. [OSINT Framework](https://osintframework.com/)
18. [OSINT Mindmap - WebBreacher](https://github.com/WebBreacher/osinttools)
19. [OSINT YOGA](https://yoga.osint.ninja/)
20. [Paterva Maltego](https://www.paterva.com/web7/)
21. [python-iocextract - Advanced IOC extraction](https://github.com/InQuest/python-iocextract)
22. [SpiderFoot](http://spiderfoot.net)
----

### Virtual Machines
1. [Buscador OSINT VM](http://inteltechniques.com/buscador)
2. [BlackArch Linux](https://blackarch.org)
3. [Kali Linux - Penetration Testing](https://www.kali.org/)
4. [REMnux: A free Linux Toolkit for Reverse-Engineering and Analyzing Malware](https://remnux.org)
5. [FLARE VM: The Windows Malware Analysis Distribution You’ve Always Needed! « FLARE VM: The Windows Malware Analysis Distribution You’ve Always Needed!](https://www.fireeye.com/blog/threat-research/2017/07/flare-vm-the-windows-malware.html)
6. [OALabs Malware Analysis Virtual Machine](https://oalabs.openanalysis.net/2018/07/16/oalabs_malware_analysis_virtual_machine/)
7. [Oracle VM VirtualBox](https://www.virtualbox.org)
----

### Malware Sandboxes
1. [Intezer](https://analyze.intezer.com/)
2. [Any.run](https://any.run/)
3. [Amaaas (Android sandbox)](https://amaaas.com/)
4. [App.sndbox.com](https://app.sndbox.com)
5. [Binaryguard](http://www.binaryguard.com/)
6. [CAPE Sandbox](https://cape.contextis.com/)
7. [CIRCL » Dynamic Malware Analysis Platform](http://www.circl.lu/services/dynamic-malware-analysis/)
8. [Detux (Linux Sandbox)](http://detux.org/)
9. [Hybrid Analysis](http://hybrid-analysis.com)
10. [Joe Sandbox](https://www.joesandbox.com/)
11. [SecondWrite](https://webportal.secondwrite.com/register/)
12. [Valkyrie.comodo.com](https://valkyrie.comodo.com/)
13. [Vicheck (Document Sandbox)](https://www.vicheck.ca/)
14. [python-sandboxapi](https://github.com/InQuest/python-sandboxapi)
----

### Malware Sources
1. [Contagio Dump](https://contagiodump.blogspot.com/)
2. [Das Malwerk](http://dasmalwerk.eu/)
3. [KernelMode.info Forum](http://www.kernelmode.info/forum/viewforum.php?f=16)
4. [MalShare](http://malshare.com/)
5. [Malware.lu's AVCaesar](https://avcaesar.malware.lu/)
6. [Malware-Traffic-Analysis.net](http://www.malware-traffic-analysis.net)
7. [Malwr (Down for now)](https://malwr.com/)
8. [The Zoo](http://ytisf.github.io/theZoo/)
9. [URLHaus](https://urlhaus.abuse.ch/browse/)
10. [VirusTotal Intelligence](https://www.virustotal.com/intelligence/search/)
11. [VirusBay](https://beta.virusbay.io/)
12. [VirusShare.com](https://virusshare.com/)
13. [Virusign (Down for Maintenance)](http://www.virusign.com/)
----

### Signature Detection
1. [ClamAV](https://www.clamav.net/)
2. [Using YARA rules in ClamAV](https://www.clamav.net/documents/using-yara-rules-in-clamav)
3. [InQuest: awesome-yara list](https://github.com/InQuest/awesome-yara)
4. [Loki: YARA based IOC scanner](https://github.com/Neo23x0/Loki)
5. [Sigma: Generic signature format for SIEMs](https://github.com/Neo23x0/sigma)
6. [Snort IDS and Rules](https://www.snort.org/downloads)
7. [Suricata Rules](https://suricata.readthedocs.io/en/suricata-4.1.2/rules/)
8. [YARA Documentation](http://virustotal.github.io/yara/)
9. [YARA: GitHub](https://github.com/VirusTotal/yara)
10. [YARA-Rules Project: GitHub](https://github.com/Yara-Rules/rules)
11. [Neo23x0 YARA signatures repository](https://github.com/Neo23x0/signature-base)
12. [yarGen: Automated YARA signature creation](https://github.com/Neo23x0/yarGen)
13. [Deadbits/yara-rules: Collection of YARA signatures from individual research](http://github.com/deadbits/yara-rules)
----

### Useful Search Engines
1. [Ahmia - Search Tor Hidden Services](https://ahmia.fi/)
2. [ONYPHE - Search for Open-source and CTI data](https://www.onyphe.io/)
3. [Shodan - Search engine for Internet-connected devices](httsp://www.shodan.io)
4. [Searx.me - Privacy Respecting Engine](https://searx.me/)
5. [Sploitus - Vulnerability Search Engine](https://sploitus.com/)
----

### Host Information
1. [BuiltWith](https://builtwith.com/)
2. [Censys.io](http://censys.io)
3. [CertDB (SSL certificates)](http://certdb.com)
4. [Cloudflare - Crimeflare](http://crimeflare.biz/)
5. [Crt.sh (SSL certificates)](https://crt.sh/)
6. [DNSdumpster](https://dnsdumpster.com/)
7. [DNSlytics.com](https://dnslytics.com)
8. [DomainBigData.com - Online investigation tools](http://domainbigdata.com)
9. [Domain History](http://domainhistory.net/)
10. [DomainIQ](https://www.domainiq.com)
11. [DomainTools - Research](https://research.domaintools.com)
12. [FindSubdomains.com](https://findsubdomains.com/)
13. [IPVoid](http://www.ipvoid.com/)
14. [PassiveDNS - Mnemonic](https://passivedns.mnemonic.no/search)
15. [Robtex](http://robtex.com/)
16. [Securitytrails.com](http://securitytrails.com/)
17. [Shodan Maps](https://maps.shodan.io/)
18. [SpyOnWeb](http://spyonweb.com)
19. [URLScan.io](https://urlscan.io)
20. [ViewDNS.info](http://viewdns.info)
21. [WHOIS Search](https://who.is)
22. [WHOIS Search (ICANN)](http://whois.icann.org/en)
----

### Reputation Databases
1. [Free Website Malware Scanner Online](http://app.webinspector.com/)
2. [Open Threat Intelligence](https://cymon.io/)
3. [Deepviz](https://search.deepviz.com/)
4. [Web Filter Lookup](http://www.fortiguard.com/ip_rep/)
5. [HASHDD](https://hashdd.com/)
6. [Automated Malware Analysis - Joe Sandbox Cloud Basic](https://www.url-analyzer.net/)
7. [Is it Hacked?](http://www.isithacked.com/)
8. [isitPhishing](http://isitphishing.org/)
9. [PhishTank](http://www.phishtank.com/)
10. [Malware Domain List](http://www.malwaredomainlist.com/mdl.php)
11. [MalwareURL](http://www.malwareurl.com/listing-urls.php)
12. [Siteadvisor](http://www.siteadvisor.com/)
13. [McAfee  - Check Single URL](http://www.trustedsource.org/)
14. [Email Blacklist Check](http://mxtoolbox.com/blacklists.aspx)
15. [Quttera - Website Anti-Malware Scanner](http://quttera.com/)
16. [ReputationAuthority](http://www.reputationauthority.org/)
17. [Sucuri Security](https://sitecheck.sucuri.net/)
18. [Trend Micro Site Safety Center](http://reclassify.wrs.trendmicro.com/)
19. [URLQuery](http://urlquery.net/)
20. [URLScan.io](https://urlscan.io/)
21. [URLVoid.com](http://urlvoid.com/)
22. [IPVoid](http://www.ipvoid.com/)
23. [VirusTotal](https://www.virustotal.com/)
24. [ThreatMiner.org](https://www.threatminer.org/)
25. [Zscaler URL Risk Analyzer](http://zulu.zscaler.com/)
----

