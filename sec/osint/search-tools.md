### Search engines
1. [Bing](https://www.bing.com/?toHttps=1&redig=E5D9D6F7D2904A0FB25FB4A22C8D9BC7)
2. [Yandex Russian, English version](https://www.yandex.com/)
3. [OSINTgeek | Open Source Intelligence Tools](https://osintgeek.de/tools)
4. [Oscobo](https://www.oscobo.com/)
5. [intelx.io/ European search machine](https://intelx.io/)
6. [Intelligence X](https://intelx.io/tools?tab=general)
7. [OsintDojo.github.io](https://www.osintdojo.com/resources/)
8. [Datasetsearch.research.google.com](https://datasetsearch.research.google.com/)
9. [Carrot2 Clustering Engine](https://search.carrot2.org/#/web)
10. [People search](https://webmii.com/)
11. [The Wolfram Language Image Identification Project](https://www.imageidentify.com/)
12. [different search engines](http://www.etools.ch/)
13. [Qwant](https://www.qwant.com/)
14. [Internationa search index](http://searchenginecolossus.com/)
15. [StartPage safe search engine](https://www.startpage.com/)
16. [Allerlei zoekmachines](https://inteltechniques.com/osint/menu.search.html)
17. [NerdyData.com](https://nerdydata.com/search)
18. [Aware-online.com/academy/osint-tools](https://aware-online.com/academy/osint-tools/)
19. [DuckDuckGo](https://duckduckgo.com/)
20. [License Plates of the World](http://www.worldlicenseplates.com/)
21. [MetaGer: Privacy Protected Search & Find](https://metager.org/)
22. [Keyword tool](https://keywordtool.io/google)
23. [Emojipedia](https://emojipedia.org/)
----

### Google
1. [advanced search](https://www.google.nl/advanced_search)
2. [picture search](https://www.google.nl/advanced_image_search?hl=nl&biw=2021&bih=941&q=geavanceerd+zoeken+afbeeldingen&tbm=isch)
3. [Fact Check Tools](https://toolbox.google.com/factcheck/explorer)
4. [15 handige Google Operators - Effectief zoeken - Aware Online Academy](https://www.aware-online.com/15-handige-google-operators/)
5. [Google Trends for locations.](http://mashups.appb.in/google-trend-mashup/)
6. [Google Translate](https://translate.google.com/)
7. [DocJax - Find any document on the web](https://www.docjax.com/)
8. [google.com](http://google.com/ncr)
9. [Startpage save browsing google](https://www.startpage.com/)
10. [Wireless Passwords From Airports And Lounges Around The World - Google My Maps](https://www.google.com/maps/d/u/0/viewer?mid=1Z1dI8hoBZSJNWFx2xr_MMxSxSxY&ll=-3.81666561775622e-14%2C43.707413350000024&z=1)
11. [i search from](http://isearchfrom.com/)
12. [Google Finder gmail](https://tools.epieos.com/google-account.php)
----

### Search in Fora
1. [Boardreader](http://boardreader.com/)
2. [Blog Search Engine](http://www.blogsearchengine.org/)
3. [Google Discussiegroepen](https://groups.google.com/forum/#!overview)
4. [Omgili](http://omgili.com/)
----

### Discord
1. [DiscordBee - Public Discord Server List](https://discordbee.com/servers?q=dutch)
2. [Browse Public Dutch Discord Servers](https://discordservers.com/search/dutch)
3. [Discord History search (script)](https://dht.chylex.com/)
4. [DiscordHub](https://discordhub.com/user/search)
5. [disboard.org/nl/search](https://disboard.org/nl/search)
6. [Extract usefull url's](https://dfir.blog/unfurl/)
----

### Addons
1. [RevEye Chrome](https://chrome.google.com/webstore/detail/reveye-reverse-image-sear/keaaclcjhehbbapnphnmpiklalfhelgf)
2. [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?utm_source=chrome-ntp-icon)
3. [Video DownloadHelper Chrome](https://chrome.google.com/webstore/detail/video-downloadhelper/lmjnegcaeklhafolokijcfjliaokphfk?hl=nl)
4. [Country Flags & IP Whois Chrome](https://chrome.google.com/webstore/detail/country-flags-ip-whois/bffjckjhidlcnenenacdahhpbacpgapo)
5. [TransOver - Chrome Web Store](https://chrome.google.com/webstore/detail/transover/aggiiclaiamajehmlfpkjmlbadmkledi?utm_source=chrome-ntp-icon)
6. [RevEye Firefox](https://addons.mozilla.org/nl/firefox/addon/reveye-ris/)
7. [uBlock Origin – Deze extensie downloaden voor 🦊 Firefox (nl)](https://addons.mozilla.org/nl/firefox/addon/ublock-origin/)
8. [Video downloadhelper Firefox](https://addons.mozilla.org/nl/firefox/addon/video-downloadhelper/)
9. [Country Flag Firefox](https://addons.mozilla.org/nl/firefox/addon/country-flags-ip-whois/)
10. [Transover Firefox](https://addons.mozilla.org/nl/firefox/addon/transover-ff/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)
----

### Pictures
1. [Reverse Image Search. - HD-internet](https://hdinternet.nl/reverse-image-search/)
2. [CC Search](https://search.creativecommons.org/)
3. [Exif data](http://exif.regex.info/exif.cgi)
4. [TinEye Reverse Image Search](https://www.tineye.com/)
5. [GeocachingToolbox.com.](https://www.geocachingtoolbox.com/index.php?lang=nl&page=imageInformation)
6. [extract text out of picture](https://www.newocr.com/)
7. [Remove exifdata](http://www.verexif.com/en/)
8. [100k_models - Google Drive](https://drive.google.com/drive/folders/1wSy4TVjSvtXeRQ6Zr8W98YbSuZXrZrgY)
9. [This Person Does Not Exist](https://thispersondoesnotexist.com/)
10. [Pexels](https://www.pexels.com/)
11. [Sensity](https://sensity.ai/)
----

### Handy sites
1. [Cybercrimebibliotheek](https://www.cybercrimeinfo.nl/vormen)
2. [Verkorte URL origineel achterhalen](http://checkshorturl.com/)
3. [Unshorten link](https://unshorten.link/)
4. [HDinternet](https://hdinternet.nl)
5. [open camera's](http://www.insecam.org/en/mapcity/)
6. [Google Search Operators: The Complete List (42 Advanced Operators)](https://ahrefs.com/blog/google-advanced-search-operators/)
7. [German advertisement poles](https://www.crossvertise.com/)
8. [Time converter](https://www.worldtimebuddy.com/)
9. [extract text from pdf of pictures](https://www.sodapdf.com/ocr-pdf/)
10. [Free Online OCR](https://onlineocr.net/)
11. [Time and Date](https://www.timeanddate.com/)
12. [Top Sites in Netherlands](https://www.alexa.com/topsites/countries/NL)
13. [Top Grossing Apps and Download Statistics iOS Store | App Annie](https://www.appannie.com/en/apps/ios/top/)
14. [Advanced Google Search Methods (IRE 2017) class.pdf](https://drive.google.com/file/d/0BxlpTzK9iG-2MG4zWGxpSVRRaWc/view)
15. [Google Search Operators: The Complete List (42 Advanced Operators)](https://ahrefs.com/blog/google-advanced-search-operators/)
16. [eTools.ch - Mobile Metasearch](https://www.etools.ch/mobileSearch.do)
17. [Www.scampolicegroup.com](https://www.scampolicegroup.com/)
18. [InstaScams](https://instascams.blogspot.com/)
19. [ScamHaters United Ltd](https://www.scamhatersunited.com/)
20. [Terrorism incidents](https://www.start.umd.edu/gtd/search/BrowseBy.aspx?category=country)
21. [WhatsMyName Web](https://whatsmyname.app/)
22. [Manifestations  map](https://manif.app/?lat=53.25418368537724&long=6.829891204833985&zoom=13&lang=nl)
----

### Tools
1. [Tools Epieos](https://tools.epieos.com/)
2. [Skypli.com](https://www.skypli.com/)
3. [Search.buzz.im](https://search.buzz.im/)
4. [Hate on Display™ Hate Symbols Database](https://www.adl.org/hate-symbols)
5. [SunCalc sun position- und sun phases calculator](https://www.suncalc.org/#/40.1789,-3.5156,3/2021.05.19/11:45/1/3)
6. [Checktips – Nieuwscheckers](https://nieuwscheckers.nl/categorie/checktips/)
----

