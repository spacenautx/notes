### Case Studies
1. [AML Toolbox](https://start.me/p/rxeRqr/aml-toolbox?embed=1)
2. [Financial Crimes OSINT Tools: Companies – Travis Birch – Medium](https://medium.com/@travb/financial-crimes-osint-tools-companies-b9ebc4ca1ace)
3. [Financial Crimes OSINT Tools: Banking – Travis Birch – Medium](https://medium.com/@travb/financial-crimes-osint-tools-banking-5ede7edbc14f)
----

### Company/Charity Information
1. [Search the charity register](https://www.gov.uk/find-charity-information)
----

### Black Market
1. [Havoscope](https://www.havocscope.com/)
2. [How Facebook Made It Easier Than Ever to Traffic Middle Eastern Antiquities](https://www.worldpoliticsreview.com/articles/25532/how-facebook-made-it-easier-than-ever-to-traffic-middle-eastern-antiquities)
3. [antiquities trafficking in the digital age](https://conflictantiquities.wordpress.com/2018/08/15/egypt-looting-online-trafficking/#more-12648)
4. [How Much Money is ISIS Making from Antiquities Looting?](https://gatesofnineveh.wordpress.com/2016/01/12/how-much-money-is-isis-making-from-antiquities-looting/)
----

### Bitcoin
1. [Bitcoin Block Explorer](https://blockchain.info/)
2. [SharadKumar97/OSINT-SPY](https://github.com/SharadKumar97/OSINT-SPY)
3. [Blockchain](https://www.blockchain.com/)
4. [OSINT-SPY | Search using OSINT](https://osint-spy.com/ip-logger)
5. [BTC (Bitcoin API)](https://osint-spy.com/btc)
6. [Neutrino - Solutions for monitoring, analyzing and tracking cryptocurrency flows](https://neutrino.nu/)
7. [Elliptic | Preventing and detecting criminal activity in cryptocurrencies](https://www.elliptic.co/)
8. [Chainalysis](https://www.chainalysis.com/)
9. [Best Bitcoin Trading Bots – Top Cryptocurrency Earning Software?](https://bitcoinexchangeguide.com/best-bitcoin-trading-bots/)
10. [Bitcoin Address Lookup, Checker and Alerts](https://bitcoinwhoswho.com/)
11. [WalletExplorer.com: smart Bitcoin block explorer](https://www.walletexplorer.com/)
12. [Bitcoin Abuse Database](https://www.bitcoinabuse.com/)
----

### Trade Data
1. [Free access to detailed global trade data](https://comtrade.un.org/)
2. [World Bank Open Data](https://data.worldbank.org/)
3. [Container tracking](http://www.track-trace.com/container)
4. [Transport - Freight transport - OECD Data](https://data.oecd.org/transport/freight-transport.htm)
----

### Credit Card Information
1. [Free BIN/IIN Lookup Web Service](https://binlist.net/)
2. [Validate, Verify &amp; Check Credit Card or Debit Card Number](https://www.bincodes.com/creditcard-checker/)
3. [Pastebin](https://pastebin.com/)
4. [kahunalu/pwnbin](https://github.com/kahunalu/pwnbin)
----

### Cryptocurrencies
1. [Jameson Lopp :: Bitcoin Resources](https://lopp.net/bitcoin.html)
2. [Cryptocurrency Facts](https://cryptocurrencyfacts.com/)
3. [Bitcoin and the Alt-Right](https://www.splcenter.org/bitcoin-and-alt-right)
4. [How Tech Supports Hate](https://www.splcenter.org/hate-and-tech)
5. [Neonazi Bitcoin Tracker - Twitter](https://twitter.com/neonaziwallets?lang=en)
6. [Tracing Jihadi Cells - Case Study](https://medium.com/@benjaminstrick/tracing-syrian-cell-kidnappers-scammers-finances-through-blockchain-e9c52fb6127d)
----

### Automating FOSINT
1. [Follow the Bitcoin With Python, BlockExplorer and Webhose.io | Automating OSINT Blog](http://www.automatingosint.com/blog/2017/09/follow-the-bitcoin-with-python-blockexplorer-and-webhose-io/)
2. [Follow the Money with Python | Automating OSINT Blog](http://www.automatingosint.com/blog/2015/09/follow-the-money-with-python/)
----

### Maps & Satellites
1. [Instant Google Street View](https://www.instantstreetview.com/)
2. [Using World Imagery Wayback](https://www.esri.com/arcgis-blog/products/arcgis-living-atlas/mapping/using-world-imagery-wayback/)
3. [Google Maps](http://maps.google.com)
4. [Google Earth](https://google.com/earth)
5. [Bing Maps](https://www.bing.com/maps)
6. [Apple Maps](https://mapsconnect.apple.com)
7. [DualMaps](http://www.dualmaps.com/)
8. [Animated Route Map by www.mapchannels.com](http://data.mapchannels.com/routemaps2/routemap200.htm?saddr=17706%20Seattle%20Ferry%20Terminal,%20Seattle,%20WA%2098104,%20USA&daddr=400%20Broad%20St,%20Seattle,%20WA%2098109,%20USA&maptype=2&units=2&z=15&fi=50&fs=1&refresh=3&timeout=300&draggable=0&sw=240&svc=0&svz=2&atw=160&fgc=000000&bgc=CCCCCC&rc=FF0000&rw=3&ro=0.7)
9. [OpenStreetMap](https://www.openstreetmap.org/#map=6/54.910/-3.432)
10. [Dashcam Map - Nextbase](https://www.nextbase.co.uk/video-map/)
11. [Midasearch - webcam viewers](https://midasearch.org/midasearch-webcam-viewers/)
12. [Mapillary](https://www.mapillary.com/)
13. [Maps.me](https://maps.me)
14. [Wikimapia](http://wikimapia.org)
15. [Openrailwaymap](https://www.openrailwaymap.org)
16. [Terra Server](https://www.terraserver.com/)
17. [GeoNames](http://www.geonames.org/)
18. [Free GIS Data](https://freegisdata.rtwilson.com/)
19. [Zoom.earth](https://zoom.earth)
20. [Harris](http://www.mapmart.com/)
21. [Planet](https://www.planet.com/)
22. [Radiant Earth](https://www.radiant.earth/)
23. [Damage Assessment Maps: Syria](http://www.unitar.org/unosat/maps/SYR)
24. [Find an Address, Business, or Location](https://www.mapquest.com/search)
25. [Geology](https://geology.com/)
26. [TRAVEL with DRONE](https://travelwithdrone.com/)
27. [Satellite map of the World : Google™ — share any place, address search; cities, countries, regions](https://satellites.pro/)
28. [Maps and Data](https://unitar.org/unosat/maps)
----

### Aviation Movements
1. [ADS-B Exchange – World&#39;s largest co-op of unfiltered flight data](https://www.adsbexchange.com/)
2. [Flightrader24](https://www.flightradar24.com/)
3. [Live Flight Tracking](https://uk.flightaware.com/live/)
4. [Listen to Live Air Traffic Control](https://www.liveatc.net/)
5. [Historical Flight Viewer](https://flight-data.adsbexchange.com/)
6. [GVA Dictator Alert](https://github.com/lefranz/geneva-dictators)
7. [Planespotters](https://www.planespotters.net/)
8. [Military Aircraft Photos/Search](http://www.airfighters.com/)
9. [FlightConnections](https://www.flightconnections.com/)
----

### Train Movements
1. [Raildar](https://raildar.co.uk/radar.html)
----

### Maps
1. [Conflict Live Map](https://liveuamap.com/)
2. [Hatemap](https://www.splcenter.org/hate-map)
3. [Regional Neo-Nazi groups in England, Scotland and Wales](https://mapsontheweb.zoom-maps.com/post/180237048133/regional-neo-nazi-groups-in-england-scotland-and)
4. [Riskmap.com](https://riskmap.com)
5. [Global Incident Map Displaying Events, Incidents And Other Suspicious Activity](http://amberalerts.globalincidentmap.com/home.php)
6. [Terrorist Attacks](https://storymaps.esri.com/stories/terrorist-attacks/)
7. [How to - identifying burnt villages](https://www.bellingcat.com/resources/how-tos/2018/09/04/identify-burnt-villages-satellite-imagery%e2%80%8a-case-studies-california-nigeria-myanmar/)
8. [Far-right Map](https://farright.liveuamap.com/)
9. [Far-left Map](https://farleft.liveuamap.com/)
10. [Every Disputed Territory in the World [Interactive Map]](http://metrocosm.com/disputed-territories-map.html)
11. [European Xenophobia](https://www.economist.com/graphic-detail/2018/07/25/european-xenophobia-reflects-racial-diversity-not-asylum-applications)
12. [EU Sanctions Map](https://sanctionsmap.eu/#/main)
----

### Time Zone Converter
1. [Worldtime Buddy](https://www.worldtimebuddy.com/)
----

### Country Profile
1. [BBC NEWS](http://news.bbc.co.uk/1/hi/country_profiles/default.stm)
2. [The World Factbook](https://www.cia.gov/library/publications/the-world-factbook/docs/profileguide.html)
3. [Foreign travel advice](https://www.gov.uk/foreign-travel-advice)
4. [European Country of Origin Information Network](https://www.ecoi.net/)
5. [Country Profiles](https://www.ecoi.net/en/countries/congo/country-profiles/)
----

### Country Profiles & Briefings
1. [Crisis Group](https://www.crisisgroup.org/)
----

### Delete Online Footprints
1. [Quickly Delete Years of Facebook Posts With This Chrome Extension](https://lifehacker.com/quickly-delete-years-of-facebook-posts-with-this-chrome-1828148008)
2. [How to delete old tweets before they come back to haunt you.](https://www.nbcnews.com/news/amp/ncna896546)
3. [Tweetdeleter – Delete tweets with a few clicks | Premium](https://www.tweetdeleter.com/en/offers)
4. [TweetEraser - Delete All Your Tweets And Favorites](https://www.tweeteraser.com/)
5. [Automatically delete your old tweets with TweetDelete.net](https://www.tweetdelete.net/)
6. [Social Book Post Manager](https://chrome.google.com/webstore/detail/social-book-post-manager/ljfidlkcmdmmibngdfikhffffdmphjae)
7. [How to Delete Your Old Tweets So Your Enemies Can&#39;t Get to Them](https://lifehacker.com/how-to-delete-your-old-tweets-and-favs-before-your-enem-1821062277)
----

### Tools - Virtual Machine
1. [Buscador OSINT VM](http://inteltechniques.com/buscador)
2. [Kali Linux](https://www.kali.org/)
3. [Linux Mint](https://linuxmint.com/download.php)
4. [Linux Ubuntu](https://www.ubuntu.com/download/desktop)
5. [VirtualBox (free)](https://www.virtualbox.org/wiki/Downloads)
6. [VMWare (paid)](https://www.vmware.com/)
----

### Free/mium OSINT Software
1. [Hunchly - Better Online Investigations](https://hunch.ly/)
2. [OSINT Browser for Law Enforcement](http://osirtbrowser.com/)
3. [People Tracker](https://trape.co/)
4. [Twitter OSINT Tool Tinfoleak](https://n0where.net/twitter-osint-tool-tinfoleak)
5. [LaNMaSteR53 / Recon-ng   — Bitbucket](https://bitbucket.org/LaNMaSteR53/recon-ng)
6. [laramies/theHarvester](https://github.com/laramies/theHarvester)
7. [hslatman/awesome-threat-intelligence](https://github.com/hslatman/awesome-threat-intelligence)
8. [Paliscope](https://www.paliscope.com/)
9. [Maltego](https://www.paterva.com/web7/downloads.php)
10. [Video: How to Use Maltego to Research &amp; Mine Data Like an Analyst](https://null-byte.wonderhowto.com/how-to/video-use-maltego-research-mine-data-like-analyst-0180985/)
11. [Story Maps](https://storymaps.arcgis.com/en/)
----

### Tools - Android emulators
1. [Andy - Android Emulator for PC &amp; Mac](https://andyroid.net)
2. [Bluestacks](https://www.bluestacks.com/nl/index.html)
3. [Genymotion](https://www.genymotion.com/)
4. [Leapdroid](https://leapdroid.en.softonic.com/)
5. [Nox](https://www.bignox.com/)
----

### Scrape/Download Data
1. [Data Scraper](https://chrome.google.com/webstore/detail/data-scraper-easy-web-scr/nndknepjnldbdbepjfgmncbggmopgden)
2. [Web Scraper](http://webscraper.io/)
3. [Downlodo.com](http://www.downlodo.com/)
4. [Agenty](https://chrome.google.com/webstore/detail/agenty-advanced-web-scrap/gpolcofcjjiooogejfbaamdgmgfehgff/related)
----

### Phone Number Lookup
1. [FREE to Lookup Unknown Callers - ReversePhoneLookup.com](http://www.reversephonelookup.com/)
2. [Free Reverse Phone Number Lookup](https://www.spydialer.com/default.aspx)
3. [Reverse Phone Lookup | Cell Phone Search | BeenVerified](https://www.beenverified.com/reverse-phone/)
4. [Who is calling you? Do a confidential reverse phone lookup to find out!](https://www.intelius.com/reverse-phone-lookup)
----

### IoT - Search Engines
1. [Shodan.com](https://shodan.io)
2. [Thingful](https://www.thingful.net/)
3. [Worldcam](https://worldcam.eu/)
4. [Earthcam](http://www.earthcam.com/)
5. [Openstreetcam](https://www.openstreetcam.org/map/)
6. [Webcams.travel](https://www.webcams.travel/)
7. [Airport webcams](http://airportwebcams.net/)
8. [123cam](http://123cam.com/)
9. [Insecam](http://www.insecam.org/)
10. [Opentopia](http://www.opentopia.com/)
11. [The webcam network](http://www.the-webcam-network.com/)
----

### Deep Fakes
1. [Deepfakes Club](https://www.deepfakes.club/)
2. [Project Voco](https://www.bbc.co.uk/news/technology-37899902)
3. [Audio Fakes](https://motherboard.vice.com/en_us/article/3k7mgn/baidu-deep-voice-software-can-clone-anyones-voice-with-just-37-seconds-of-audio)
----

### Newspapers
1. [Newspaperarchive.com](https://newspaperarchive.com/)
2. [newspaper map](https://newspapermap.com/)
----

### People Search
1. [PEP Checks and Sanction Screening](https://namescan.io/)
2. [Everypolitician.org](http://everypolitician.org/)
3. [CVgadget.com](http://www.cvgadget.com/)
4. [CV Searcher - Google Custom Search](https://cse.google.com/cse/publicurl?cx=009462381166450434430:0aq_5piun68)
5. [Certificated Bailiff Register – Justice UK](https://certificatedbailiffs.justice.gov.uk/)
6. [Inteltechniques - People Search Tools](https://inteltechniques.com/menu/pages/person.tool.html)
7. [Spokeo](https://www.spokeo.com/)
8. [Find People for Free](https://thatsthem.com/)
9. [Identify the owner of any phone number!](http://www.peoplebyname.com/)
10. [Public Records Directory](https://publicrecords.directory/)
11. [No.1 Free People Search](http://www.yasni.com/)
12. [Free People Search Engine](https://www.zabasearch.com/)
13. [Peekyou](https://www.peekyou.com/)
14. [How Many of Me](http://howmanyofme.com/search/)
15. [Find A Grave](https://www.findagrave.com/)
16. [Pipl](https://pipl.com/)
17. [PeekYou.com](https://www.peekyou.com/)
18. [People search - Find people over the internet for free - White Pages](http://www.ppfinder.com/)
19. [Yasni](http://www.yasni.co.uk/)
20. [Free Family History and Genealogy Records — FamilySearch.org](https://www.familysearch.org/)
21. [Find People, Phone Numbers, Addresses &amp; More](https://www.whitepages.com/)
22. [Find People, Phone Numbers, Addresses &amp; More | Whitepages](https://www.411.com/)
23. [192.com](http://www.192.com/)
24. [RecruitEm](https://recruitin.net/)
25. [Over 50,000 Genealogy Links for US, Canada, UK, Ireland, Australia, NZ - created &amp; maintained by Ian Mold](http://www.genealogylinks.net/)
26. [Ancestry® | Genealogy, Family Trees &amp; Family History Records](https://www.ancestry.co.uk/?geo_a=t&geo_s=us&geo_t=uk&geo_v=2.0.0&o_iid=41013&o_lid=41013&o_sch=Web+Property)
27. [Alumni.NET](http://www.alumni.net/)
28. [100% Free Genealogy - FamilyTreeNow.com](https://www.familytreenow.com/)
----

### Phone numbers
1. [Receive SMS (free)](http://receive-sms-online.com/)
2. [Receive-SMS (free)](https://receive-sms.com/)
3. [Receive SMS (free)](http://sms.sellaite.com/)
4. [Receive SMS Online](https://www.receivesmsonline.net/)
----

### Pastebins
1. [Pastebins (Google Custom Search)](https://inteltechniques.com/osint/menu.pastebins.html)
----

### Stolen Property
1. [Art and Culture crime database](http://news.culturecrime.org/all)
2. [Artloss](http://www.artloss.com/en)
3. [Hotgunz (USA)](http://www.hotgunz.com/search.php)
4. [NMPR - The National Mobile Property Register](https://thenmpr.com/home)
----

### Facial Recognition Search Engines
1. [Baidu Image Search](https://image.baidu.com/)
2. [Betaface | Advanced face recognition](https://www.betaface.com/wpa/)
3. [Face detection &amp; recognition](http://facedetection.kuznech.com/)
4. [Google Images](https://www.google.com/imghp)
5. [Innovative graphic search engine Picwiser - photos fast matching, image matching, image recognition, photos, picture, search engine](http://picwiser.com/)
6. [pictriev, Gesichts-Suchmaschine](http://www.pictriev.com/)
7. [Twins or Not?](http://www.twinsornot.com/)
8. [Yandex Image Search](https://yandex.com/images/)
----

### Translation
1. [Google Translate](https://translate.google.co.uk/)
2. [Yandex.Translate – dictionary and online translation between English and over 90 other languages.](https://translate.yandex.com/)
3. [Baidu Translate](https://fanyi.baidu.com/)
4. [DeepL Translator](https://www.deepl.com/translator)
5. [Translate by Babylon - Free Online Translation](http://translation.babylon-software.com/)
6. [Free Online Translation](http://imtranslator.net/translation/)
----

### Company Information
1. [The Investigator’s Handbook: A Guide to Using OpenCorporates](https://blog.opencorporates.com/2017/10/31/the-investigators-handbook-a-guide-to-using-opencorporates/)
2. [Databases (Worldwide) - Investigative Dashboard](https://investigativedashboard.org/databases/)
3. [OpenCorporates :: The Open Database Of The Corporate World](https://opencorporates.com/)
4. [Aleph data search](https://data.occrp.org/)
5. [Overseas registries](https://www.gov.uk/government/publications/overseas-registries/overseas-registries)
6. [GCRD International | Welcome to GCRD.](https://www.gcrd.info/index.php?id=3814)
7. [ICIJ Offshore Leaks Database](https://offshoreleaks.icij.org/)
8. [Companies House UK](https://www.gov.uk/get-information-about-a-company)
9. [Company Check Orbis](https://orbisdirectory.bvdinfo.com/version-20171213/OrbisDirectory/Companies)
10. [Company Check Zauba](https://www.zaubacorp.com/)
11. [Company Check](https://companycheck.co.uk/)
12. [Financial Conduct Authority](https://register.fca.org.uk/)
13. [Victims of Offshore: A Video Introduction to the Panama Papers - ICIJ](https://www.icij.org/investigations/panama-papers/video/)
14. [The KYC Registry](https://www.swift.com/our-solutions/compliance-and-shared-services/financial-crime-compliance/our-kyc-solutions/the-kyc-registry#topic-tabs-menu)
----

### Regional and Global CT Actors / Initatives.
1. [Interpol&#39;s Global CT Strategy - Summary](https://bit.ly/2xwBZoZ)
2. [Countering Terrorist Narratives](https://icct.nl/wp-content/uploads/2017/11/Countering-Terrorist-Narratives-Reed-Whittaker-Haroro-European-Parliament.pdf)
3. [UN Counter Terrorism Committee](https://www.un.org/sc/ctc/)
4. [OSCE Counter Terrorism](https://www.osce.org/secretariat/terrorism)
5. [Global Counter Terrorism Forum](https://www.thegctf.org/)
6. [Centre of Excellence Defence Against Terrorism](http://www.coedat.nato.int/)
7. [Global Engagement Center](https://www.state.gov/r/gec/)
8. [Hedayah.ae](http://www.hedayah.ae/)
9. [The Global Coalition Against Daesh | Home](http://theglobalcoalition.org/en/home/)
10. [Sawab Center](http://80.227.220.174/)
11. [Southeast Asia Regional Center for Counter Terrorism](http://www.searcct.gov.my/)
12. [The Islamic Military Counter Terrorism Coalition](https://imctc.org/English)
----

### Translation/Language
1. [Learn Languages](http://mylanguages.org/)
2. [Lexilogos](https://www.lexilogos.com/)
3. [Cyrcillic decoder](https://2cyr.com/decode/?lang=en)
4. [Google Translate](http://translate.google.com)
5. [Know Your Meme](http://knowyourmeme.com/)
6. [TagDef](https://tagdef.com)
7. [Urban Dictionairy](https://www.urbandictionary.com/)
8. [Yamii](https://www.yamli.com/arabic-keyboard/)
9. [DeepL](https://www.deepl.com/translator)
----

### Monitoring News/Conflicts/Web
1. [Liveuamap.com](https://liveuamap.com)
2. [NewsBrief](http://emm.newsbrief.eu/NewsBrief/clusteredition/en/latest.html)
3. [Google Alerts](https://www.google.co.uk/alerts)
4. [Google Alerts alternative. The best and free alerts service with Twitter results](https://www.talkwalker.com/alerts)
5. [IBM News Explorer](http://news-explorer.mybluemix.net/?query=IRAQ&type=unconstrained)
6. [IRIS](https://iris.wcoomd.org/?locale=en)
7. [Live Piracy Map](https://www.icc-ccs.org/piracy-reporting-centre/live-piracy-map)
8. [NewsNow: World News news](https://www.newsnow.co.uk/h/World+News)
----

### Open Access Journals
1. [Perspectives on Terrorism](https://www.jstor.org/journal/persponterr)
2. [Journal for Deradicalization](http://journals.sfu.ca/jd/index.php/jd)
3. [CTC Sentinel](https://ctc.usma.edu/ctc-sentinel/)
4. [Publications | ICCT](https://icct.nl/publications/?categories=13)
5. [HOMELAND SECURITY AFFAIRS](https://www.hsaj.org/)
6. [http://scholarcommons.usf.edu/jss/](http://scholarcommons.usf.edu/jss/)
7. [Directory of Open Access Journals](https://doaj.org/)
8. [GtR](https://gtr.ukri.org/)
----

### Maritime Movements
1. [LocalizaTodo HTML5](http://www.localizatodo.com/html5/)
2. [Marine Traffic](https://www.marinetraffic.com/)
3. [ShipmentLink](https://www.shipmentlink.com/servlet/TDB1_CargoTracking.do)
4. [Vessel Tracker](https://www.vesseltracker.com/)
5. [Vessel Finder](https://www.vesselfinder.com/)
6. [Shipfinder](http://www.shipfinder.com/)
7. [Shipfinder](http://shipfinder.co/)
8. [Ship Spotting](http://www.shipspotting.com/)
9. [ShipAIS](http://www.shipais.com/)
10. [Ais Decoder](http://arundale.com/docs/ais/ais_decoder.html)
11. [TankerTrackers](http://tankertrackers.com/#/)
12. [Fleetmon](https://www.fleetmon.com/services/live-tracking/fleetmon-explorer/)
13. [Equasis](http://www.equasis.org/EquasisWeb/public/HomePage)
----

### Other
1. [Visual Tools for Investigators.](https://vis.occrp.org/)
2. [Islamism World Map](https://islamism-map.com/#/)
3. [West Africa Leaks - ICIJ](https://www.icij.org/investigations/west-africa-leaks/)
4. [ThoughtfulDev/EagleEye](https://github.com/ThoughtfulDev/EagleEye)
----

### Email Search/Checker
1. [Custom Search Tool by Michael Bazzell](https://inteltechniques.com/osint/menu.email.html)
2. [Online Email Validator &amp; Email Verification Services | Byteplant](https://www.email-validator.net/)
3. [Email Format](https://email-format.com/)
4. [Hunter](https://hunter.io/)
5. [Email Permutator](http://metricsparrow.com/toolkit/email-permutator/)
6. [Have I Been Pwned: Check if your email has been compromised in a data breach](https://haveibeenpwned.com/)
----

### Digital Forensics
1. [InVid](http://www.invid-project.eu/)
2. [Get-metadata](https://www.get-metadata.com/)
3. [Jeffrey Friedl&#39;s Image Metadata Viewer](http://exif.regex.info/exif.cgi)
4. [List of digital forensics tools](https://en.wikipedia.org/wiki/List_of_digital_forensics_tools)
5. [Forensically](https://29a.ch/photo-forensics/#forensic-magnifier)
6. [Fotoforensics](https://fotoforensics.com/)
7. [IRFANVIEW](http://www.irfanview.com/)
8. [hatlord/Spiderpig](https://github.com/hatlord/Spiderpig)
9. [Metapicz](http://metapicz.com/#landing)
10. [Exifdata](http://exifdata.com/)
11. [ExifTool](https://www.sno.phy.queensu.ca/~phil/exiftool/)
----

### Useful Addons
1. [Addons](https://start.me/p/nRQNRb/addons)
----

### Verification
1. [Verification Toolset](https://start.me/p/ZGAzN7/verification-toolset?utm_content=buffer84b1d&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
2. [First Draft News - Verification Dashboard](https://start.me/p/Wrrzk0/tools)
----

### Other OSINT lists
1. [AsINT_Collection](https://start.me/p/b5Aow7/asint_collection)
2. [OSINT Framework](http://osintframework.com/)
3. [OSINT](https://start.me/p/m6XQ08/osint)
4. [Ph055a/awesome_osint](https://github.com/Ph055a/awesome_osint)
5. [IVMachiavelli/OSINT_Team_Links](https://github.com/IVMachiavelli/OSINT_Team_Links)
6. [jivoi/awesome-osint](https://github.com/jivoi/awesome-osint)
7. [Tools](https://start.me/p/Wrrzk0/tools)
8. [MidaSearch](https://midasearch.org/)
9. [Uk-osint.net](https://www.uk-osint.net)
10. [OSINTessentials](https://www.osintessentials.com/)
11. [https://www.osintessentials.com](https://www.osintessentials.com)
12. [Advanced Digital Toolkit - OSINT - Andy Black Associates](https://www.andyblackassociates.co.uk/resources-andy-black-associates/advanced-digital-toolkit/)
----

### Training & Tutorials
1. [OSINT Handbook 2018](https://www.i-intelligence.eu/wp-content/uploads/2018/06/OSINT_Handbook_June-2018_Final.pdf)
2. [CaseFile – Visualization of Information](https://corma.de/en/casefile-visualization-of-information-investigation/)
3. [Exposing the Invisible](https://exposingtheinvisible.org/)
4. [Research Clinic](http://www.researchclinic.net/)
5. [CNS New Tools Courseware Portfolio – Learn. Investigate. Discover.](https://learn-new-tools.org/)
6. [Dark Web Tools](https://iaca-darkweb-tools.com/)
7. [GIJN - Toolbox](https://gijn.org/2018/11/07/gijn-toolbox/)
8. [European Country of Origin Information Network](https://www.ecoi.net/en/coi-resources/training/)
----

### Safeguarding Anonymity & Privacy
1. [One click and you&#39;re out: UK makes it an offence to view terrorist propaganda even once • The Register](https://www.theregister.co.uk/AMP/2019/02/13/uk_counter_terror_act_royal_assent/?__twitter_impression=true)
2. [Is your VPN secure?](https://theconversation.com/is-your-vpn-secure-109130?utm_source=twitter&utm_medium=twitterbutton)
3. [Basic Opsec](https://medium.com/@JMartinMcFly/basic-opsec-for-the-uninitiated-77d0839f7bce)
4. [Browser Uniqueness &amp; Fingerprinting](https://medium.com/@ravielakshmanan/web-browser-uniqueness-and-fingerprinting-7eac3c381805)
5. [Am I unique?](https://amiunique.org/)
6. [Privacy Tools](https://amiunique.org/tools)
7. [Digital Security Resources](https://medium.com/@mshelton/current-digital-security-resources-5c88ba40ce5c)
8. [Identity Protection - CIFAS](https://www.cifas.org.uk/services/identity-protection)
9. [Web Tracking: What You Should Know About Your Privacy Online](https://medium.freecodecamp.org/what-you-should-know-about-web-tracking-and-how-it-affects-your-online-privacy-42935355525)
10. [25 Best Chrome Extensions For Penetration Testing Can Keep You Out of Trouble](http://techblogcorner.com/2017/10/27/best-chrome-extensions-for-penetration-testing/)
11. [Why Do Some Websites Block VPNs?](https://www.howtogeek.com/403771/why-do-some-websites-block-vpns/)
12. [Your IP Address: 107.20.28.255](http://mybrowserinfo.com/)
13. [BrowserLeaks.com](https://browserleaks.com/)
14. [Online Guide to Privacy](https://epic.org/privacy/tools.html)
15. [What is a VPN - NORD VPN](https://nordvpn.com/what-is-a-vpn/)
----

### Domain Searches
1. [Hexillion: Whois API and Internet tools for cybersecurity investigations, troubleshooting, competitive intelligence](https://hexillion.com/)
2. [Domain Search Tool](https://inteltechniques.com/osint/domain.search.html)
3. [IP Address Search Tool](https://inteltechniques.com/osint/menu.ip.html)
4. [Whois Lookup &amp; IP](https://www.whois.net/)
5. [Instant Domain Search](https://instantdomainsearch.com/)
6. [Recon-ng (Advanced)](https://bitbucket.org/LaNMaSteR53/recon-ng)
7. [DataSploit (Advanced)](https://github.com/DataSploit/datasploit)
----

### Academic Search Engines
1. [scholar.google.co](https://scholar.google.com)
2. [Talk to Books](https://books.google.com/talktobooks/)
3. [Text Analyzer Beta, from JSTOR Labs](https://www.jstor.org/analyze/)
4. [CORE](https://core.ac.uk/)
5. [The Library of Congress](https://www.loc.gov/)
6. [Open Knowledge Maps](https://openknowledgemaps.org/)
7. [BASE (Bielefeld Academic Search Engine): Basic Search](https://www.base-search.net/)
----

### Datasets & Databases
1. [Who are Britain’s jihadists?](https://www.bbc.co.uk/news/uk-32026985)
2. [Jihadology.net](http://jihadology.net)
3. [Searching state courts](https://docs.google.com/document/d/1DHHv7GS6mycat97RTzlZDkokPZcm3QpoJgQ0W_QqaiY/edit#bookmark=id.kskj5vz9fvql)
4. [Terror Cases - GWUPOE](https://twitter.com/TerrorCases?lang=en)
5. [TheLawPages.com - Terrorism (UK)](https://www.thelawpages.com/criminal-offence/#)
6. [Crisis- and conflict-related data sets (Africa, Middle East, South &amp; South-East Asia)](https://www.acleddata.com/data/)
7. [Acled Dashboard](https://www.acleddata.com/dashboard/)
8. [GIRDS: Database on Terrorism in Germany: Right-Wing Extremism and Jihadism](http://girds.org/projects/database-on-terrorism-in-germany-right-wing-extremism)
9. [Global Terrorism Database](https://www.start.umd.edu/gtd/)
10. [GWUPOE - Cases](https://extremism.gwu.edu/cases)
11. [GWUPOE - Extremism Tracker](https://extremism.gwu.edu/gw-extremism-tracker)
12. [GWUPOE - Telegram (ISIS) Tracker](https://extremism.gwu.edu/isis-online-infographics)
13. [Investigative Project - Court Cases](https://www.investigativeproject.org/cases.php)
14. [RAND](http://smapp.rand.org/rwtid/search_form.php)
15. [The Guantánamo Docket](https://www.nytimes.com/interactive/projects/guantanamo)
16. [ICD - Foreign Terrorist Fighters (Cases)](http://www.internationalcrimesdatabase.org/foreignfighters?utm_content=buffer73dc1&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
17. [ICD - Terrorism Cases](http://www.internationalcrimesdatabase.org/Cases/ByCategory/Terrorism)
18. [IVEO Knowledge Matrix](http://www.start.umd.edu/data-tools/iveo-knowledge-matrix)
19. [Profiles of Individual Radicalization in the United States (PIRUS)](http://www.start.umd.edu/data-tools/profiles-individual-radicalization-united-states-pirus)
20. [START - Compiled list of data and tools](http://www.start.umd.edu/data-and-tools/start-datasets)
21. [TEVUS Portal](http://www.start.umd.edu/tevus-portal)
22. [Nuclear Facilities Attack Database (NuFAD)](https://www.start.umd.edu/nuclear-facilities-attack-database-nufad)
23. [NACJD](https://www.icpsr.umich.edu/icpsrweb/NACJD/discover-data.jsp)
24. [Harmony Documents Archive – Combating Terrorism Center at West Point](https://ctc.usma.edu/harmony-program/)
25. [CTC - Militant Imagery Project](https://ctc.usma.edu/militant-imagery-project/)
26. [RUSI | Armed Drones](https://drones.rusi.org/)
27. [Dataset on Right-wing terrorism](https://www.sv.uio.no/c-rex/english/news-and-events/news/2016/rtv-dataset.html)
28. [Suicide Attack Database](http://cpostdata.uchicago.edu/search_new.php)
29. [Islamophobia | Haas Institute](https://haasinstitute.berkeley.edu/global-justice/islamophobia)
30. [Individuals referred to and supported through the Prevent Programme statistics](https://www.gov.uk/government/collections/individuals-referred-to-and-supported-through-the-prevent-programme-statistics)
31. [CVE Interventions Database](http://www.impact.itti.com.pl/index#/home)
32. [UK Hate Crime Statistics](https://www.gov.uk/government/collections/hate-crime-statistics)
33. [South Asia Terrorism Updates](http://www.satp.org/)
34. [Syrian War Daily](https://syrianwardaily.com/)
35. [UK Counter Terrorism Statistics](https://www.gov.uk/government/collections/counter-terrorism-statistics)
36. [ADL Hate Crime Map](https://www.adl.org/adl-hate-crime-map)
37. [Digital Terrorism and Hate Project](http://digitalhate.net/index.php)
38. [EU Terrorism Situation &amp; Trend Report (Te-Sat)](https://www.europol.europa.eu/activities-services/main-reports/eu-terrorism-situation-and-trend-report)
39. [UCDP (14) datasets](http://ucdp.uu.se/downloads/)
40. [Transnational Terrorism Data - Harvard Dataverse](https://dataverse.harvard.edu/dataverse.xhtml?alias=transnatlterrorism)
41. [Syrian Archive](https://syrianarchive.org/en/database)
42. [Global Counter-IED Map](https://aoav.org.uk/category/ieds-and-suicide-bombings/addressing-the-threat/global-counter-ied-map/)
43. [28+ million Gab posts](https://twitter.com/jasonbaumgartne/status/1057496300348940290)
44. [Gun Violence Archive](https://www.gunviolencearchive.org/)
----

### Digital Methods for Research
1. [ToolDatabase &lt; Dmi &lt; Foswiki](https://wiki.digitalmethods.net/Dmi/ToolDatabase)
2. [Spatial Analysis Tools | GISPopSci](http://gispopsci.org/software/)
----

### Arms & Ammunition
1. [SIPRI Arms Transfers Database](https://www.sipri.org/databases/armstransfers)
2. [The Weapons ID Database](http://www.smallarmssurvey.org/weapons-and-markets/tools/weapons-id-database.html)
3. [Search 3d models - sketchfab](https://sketchfab.com/search?sort_by=-pertinence&type=models)
4. [Arms embargoes](https://www.sipri.org/databases/embargoes)
5. [Identification and Support Cards](http://www.smallarmssurvey.org/weapons-and-markets/tools/id-cards.html)
6. [Ammunition-Tracing-Kit.pdf](http://www.smallarmssurvey.org/fileadmin/docs/D-Book-series/book-06-ATK/SAS-Ammunition-Tracing-Kit.pdf)
7. [Loading Mapping Arms Data (MAD) 1992-2014. Please wait as application loads.
                    Note: The graphics used in this visualization are not supported by older versions of 			Internet Explorer. It is best viewed using an up-to-date versi...](http://nisatapps.prio.org/armsglobe/index.php)
8. [Global Firearms Holdings](http://www.smallarmssurvey.org/weapons-and-markets/tools/global-firearms-holdings.html)
9. [UEMS tools](http://www.smallarmssurvey.org/weapons-and-markets/stockpiles/unplanned-explosions-at-munitions-sites/uems-tools.html)
10. [HotGunz Stolen Gun Search Results](https://www.hotgunz.com/search.php)
----

### Transnational Weekly
1. [George C. Marshall European Center for Security Studies - Transnational Weekly](http://www.marshallcenter.org/MCPUBLICWEB/en/nav-submenu-pubs-transnational-weekly-en.html)
----

### Global Terrorism Index
1. [Visionofhumanity.org](http://visionofhumanity.org/app/uploads/2018/12/Global-Terrorism-Index-2018-1.pdf)
----

### Other Resources
1. [Resources for Finding and Using Satellite Images - Global Investigative Journalism Network](https://gijn.org/resources-for-finding-and-using-satellite-images/)
----

### Internet Archives
1. [Archive.today](https://archive.today)
2. [Wayback Machine](https://web.archive.org/)
3. [Freemium solution](https://www.apowersoft.com/video-download-capture.html)
4. [How to Archive Open Source Materials - bellingcat](https://www.bellingcat.com/resources/how-tos/2018/02/22/archive-open-source-materials/)
----

### Dark Web Search Engines
1. [TOR Search Engines](https://www.notion.so/TOR-Search-Engines-7b6a20b5ddf342c183f9c654fc7b6c25)
2. [Daily Dark Web Monitoring Reports from Hunchly](https://darkweb.hunch.ly/)
3. [Ahmia.fi](https://ahmia.fi/)
4. [Torscan](http://www.torscan.io/)
5. [Onion.link](http://onion.link/faq.html)
----

### Combatant Affiliation/Weapons
1. [How to Digitally Verify Combatant Affiliation in Middle East Conflicts - bellingcat](https://www.bellingcat.com/resources/how-tos/2018/07/09/digitally-verify-middle-east-conflicts/)
2. [Camopedia](http://camopedia.org/index.php?title=Main_Page)
3. [Camouflageindex.camouflagesociety.org/index-2.html](http://camouflageindex.camouflagesociety.org/index-2.html)
4. [Uniforminsignia.org](http://www.uniforminsignia.org/)
5. [List of comparative military ranks](https://en.wikipedia.org/wiki/List_of_comparative_military_ranks)
6. [All Badges site for collectors](http://allbadges.net/en)
----

### Arabic names
1. [The Names of Jihad - Majalla Magazine](http://eng.majalla.com/2017/07/article55254103/the-names-of-jihad)
2. [Behind the Name: Arabic Names](https://www.behindthename.com/names/usage/arabic)
3. [Arabic name structure - Wikipedia](https://en.wikipedia.org/wiki/Arabic_name)
----

### Blogging, forum & other communities
1. [Strava](https://labs.strava.com/heatmap/)
2. [Tumblr](https://www.tumblr.com/)
3. [LiveJournal](https://www.livejournal.com/)
4. [Classmates](http://www.classmates.com/)
5. [Wordpress](https://wordpress.org)
6. [Blogger](https://www.blogger.com/)
7. [Wix](https://wix.com)
8. [Medium](https://medium.com)
9. [ProBoards](https://www.proboards.com/)
10. [SquareSpace](https://www.squarespace.com/?)
11. [Joomla](https://www.joomla.org/)
12. [Ghost](https://ghost.org/)
13. [Weebly](https://www.weebly.com/?lang=en)
----

### Facebook
1. [OSINT Search Tool by IntelTechniques](https://inteltechniques.com/menu.html)
2. [Help Center](https://www.facebook.com/help/460711197281324/)
3. [Facebook Matrix](https://docs.google.com/spreadsheets/d/15dD0qSCDYDwVtWKC9zfZk1j8RzmvZARXTu9hrM34DnU/edit#gid=0)
4. [AFS](https://chrome.google.com/webstore/detail/afs/gckmgjhcejhnfenfbippohhnfjkeaapj?hl=en)
5. [Who posted what?](https://whopostedwhat.com/)
6. [Pitoolbox.com.au/facebook-tool](http://pitoolbox.com.au/facebook-tool/)
7. [Stalkscan.com](https://www.stalkscan.com/)
8. [Facebook Search Tool by Bob Brasich @NetBootCamp](http://netbootcamp.org/facebook.html)
9. [Find my Facebook ID](https://findmyfbid.com/)
10. [Bulk Facebook ID Finder | Seo Tool Station](https://seotoolstation.com/bulk-facebook-id-finder)
11. [Facebook Live Map](https://www.facebook.com/livemap)
12. [Facebook Page Analytics](https://barometer.agorapulse.com/)
13. [Search is Back!](https://searchisback.com/)
14. [Facebook Graph Search Generator by Henk van Ess and Torment Gerhardsen](http://graph.tips/beta/)
15. [Facebook Video Downloader Online](https://www.fbdown.net/index.php)
----

### Telegram
1. [Save Telegram Chat History](https://chrome.google.com/webstore/detail/save-telegram-chat-histor/kgldnbjoeinkkgpphaaljlfhfdbndfjc?hl=en)
2. [Telegram API for OSINT – Part 1 – Users](https://fabledowlblog.wordpress.com/2017/07/10/telegram-api-for-osint-part-1-users/)
3. [Telegram API for OSINT – Part 2 – Messages](https://fabledowlblog.wordpress.com/2017/09/09/telegram-api-for-osint-part-2-messages/)
4. [nickoala/telepot](https://github.com/nickoala/telepot)
5. [TChannels.me](https://tchannels.me/)
6. [Github.com/Skarlso/rscrap](https://github.com/Skarlso/rscrap)
----

### Copy of Telegram
1. [Skarlso/rscrap](https://github.com/Skarlso/rscrap)
2. [Largest catalog of Telegram channels. Statistics, analytics, TOP chart. Telegram Analytics.](https://tgstat.com/)
3. [https://pypi.org/project/telegram-export/](https://pypi.org/project/telegram-export/)
4. [Far-Right on Telegram](https://eeradicalization.com/far-right-extremism-on-telegram-a-brief-overview/)
5. [Jihadi Telegram Overview](https://eeradicalization.com/exploring-the-jihadi-telegram-world-a-brief-overview/)
6. [Channels Overview](https://eeradicalization.com/jihadi-telegram-tutorial-channels-the-lone-wolfs-lair/)
----

### How to Telegram
1. [Exploring The Jihadi Telegram World: A Brief Overview – European Eye on Radicalization](https://eeradicalization.com/exploring-the-jihadi-telegram-world-a-brief-overview/)
2. [Jihadi Telegram Tutorial Channels: The Lone Wolf’s Lair – European Eye on Radicalization](https://eeradicalization.com/jihadi-telegram-tutorial-channels-the-lone-wolfs-lair/)
----

### Gab
1. [lyndsysimon/gab](https://github.com/lyndsysimon/gab)
----

### VKontakte
1. [FindFace](https://findface.ru/)
2. [small tool to retreive vk.com (vkontakte) users hidden metadata (state, access, dates, counts, etc) anonymously (without login)](https://gist.github.com/cryptolok/8a023875b47e20bc5e64ba8e27294261)
3. [Поиск сообществ](https://vk.com/communities)
4. [Поиск людей](https://vk.com/people)
5. [ВКонтакте RSS](https://vk-to-rss.appspot.com/)
6. [Фонарик. Таргетинг Вконтакте. Аналог Церебро-таргет.](http://spotlight.svezet.ru/)
7. [ТаргетоLOG — уникальный инструмент таргетирования](http://targetolog.com/)
8. [Шпион ВКонтакте](http://vk5.city4me.com/)
----

### LinkedIn
1. [Searching on LinkedIn | LinkedIn Help](https://www.linkedin.com/help/linkedin/answer/302/searching-on-linkedin?lang=en)
2. [Socilab - LinkedIn Social Network Visualization, Analysis, and Education](http://www.socilab.com/#home)
3. [initstring/linkedin2username](https://github.com/initstring/linkedin2username)
4. [LinkedIn Profiles by Email (Michael Bazzell Blog)](https://inteltechniques.com/blog/2018/04/25/linkedin-profiles-by-email/)
5. [Lusha](https://chrome.google.com/webstore/detail/lusha/mcebeofpilippmndlpcghpmghcljajna/related?hl=en)
6. [Michael Bazzell&#39;s Blog  » Blog Archive   » LinkedIn Profiles by Email](https://inteltechniques.com/blog/2018/04/25/linkedin-profiles-by-email/)
----

### Tumblr
1. [Tumblr Originals: shows original posts and hides reblogs for any given tumblr blog.](http://www.studiomoh.com/fun/tumblr_originals/)
2. [Trending](https://www.tumblr.com/explore/trending)
----

### IS Media System
1. [IS Media System](https://www.ict.org.il/Article/2235/The_Media_System_of_the_Islamic_State-On_the_Road_to_Recovery#gsc.tab=0)
----

### Violent Online Political Extremism
1. [Library - VOX - Pol](https://www.voxpol.eu/library/)
2. [Blog Archives - VOX - Pol](https://www.voxpol.eu/category/blog/)
3. [VOX-Pol Publications - VOX - Pol](https://www.voxpol.eu/publications/)
4. [War of keywords - how extremists are exploiting the internet and what to do about it](https://institute.global/sites/default/files/inline-files/IGC_War%20of%20Keywords_23.08.17_0.pdf)
5. [Third-Party Research - Tech Against Terrorism](https://www.techagainstterrorism.org/news/global/)
----

### YouTube
1. [Geo Search Tool](https://youtube.github.io/geo-search-tool/search.html)
2. [Savevideo.me](https://savevideo.me/)
3. [YouTube Data Tools](https://tools.digitalmethods.net/netvizz/youtube/)
4. [HookTube](https://hooktube.com/)
5. [Keepvid](https://keepvid.com/)
6. [WatchFrameByFrame](http://www.watchframebyframe.com/)
7. [Yout.com](https://yout.com)
8. [Youtube-DL](https://rg3.github.io/youtube-dl/)
9. [YTcomments](http://ytcomments.klostermann.ca/)
10. [Fram by frame](http://www.watchframebyframe.com/)
11. [YouTube](https://www.youtube.com/watch?v=Bb0HnaYNUx4&t=117s)
----

### Flickr
1. [Flickr: Explore everyone&#39;s photos on a Map](https://www.flickr.com/map/)
2. [idGettr — Find your Flickr ID](https://www.webpagefx.com/tools/idgettr/)
3. [Free Username Search - Lookup any username on UserSherlock.com](http://usersherlock.com/)
----

### Instagram
1. [Search Instagram Hashtags &amp; Users -&gt; Generate Popular Tags (FREE)](http://keywordtool.io/instagram/)
2. [Instagram Stories and Stories Highlights Download - storiesig](https://storiesig.com/)
3. [picodash](https://www.picodash.com/)
4. [Tofo.me](https://tofo.me/)
5. [Bearpanther.com/instamap](http://bearpanther.com/instamap/)
6. [Web.stagram.com](https://web.stagram.com/)
7. [propublica/qis](https://github.com/propublica/qis/)
8. [http://yooying.com/search/instadp.net/](http://yooying.com/search/instadp.net/)
9. [hehpollon/Instagram-Crawler](https://github.com/hehpollon/Instagram-crawler/)
10. [rarcega/instagram-scraper](https://github.com/rarcega/instagram-scraper)
11. [http://mixagram.com/](http://mixagram.com/)
12. [Worldcam](http://worldc.am/)
13. [Search Instagram Users &amp; Hashtag &amp; Posts • Yooying](https://www.yooying.com/search)
14. [http://instaloader.github.io/picdeer.com/](https://instaloader.github.io/picdeer.com/)
15. [alshf89/InstaSave](https://github.com/alshf89/InstaSave/)
16. [Instagram Online Viewer | Picbear](http://picbear.club/)
17. [Custom Instagram Tools by Inteltechniques.com](https://inteltechniques.com/osint/instagram.html)
18. [DownloadGram - Instagram photo and video downloader online](https://downloadgram.com/)
19. [Search people](http://www.mundimago.com/profile/search)
20. [althonos/InstaLooter](https://github.com/althonos/InstaLooter)
21. [Instagram Search](https://web.stagram.com/search)
22. [Pictame](http://www.pictame.com/)
23. [althonos/InstaLooter](https://github.com/althonos/InstaLooter/)
24. [Instagram Search Engine](https://mulpix.com/)
25. [Picodash](https://www.picodash.com/?error=login)
----

### Twitter
1. [Twitter.com/search-advanced](https://twitter.com/search-advanced?lang=en-gb)
2. [TweetDeck](https://tweetdeck.twitter.com/)
3. [The who, what, where, when how of social media - tools](https://osome.iuni.iu.edu/tools/)
4. [Followerwonk: Tools for Twitter Analytics, Bio Search and More](https://moz.com/followerwonk/)
5. [Twitter Analytics by Foller.me](https://foller.me/)
6. [Real-time Twitter Trending Hashtags and Topics](https://www.trendsmap.com/)
7. [vaguileradiaz/tinfoleak](https://github.com/vaguileradiaz/tinfoleak)
8. [TweetBeaver - Home of Really Useful Twitter Tools](https://tweetbeaver.com/)
9. [Twlets](http://twlets.com/)
10. [Mapd.com/demos/tweetmap](https://www.mapd.com/demos/tweetmap/)
11. [Tweet Mapper](https://keitharm.me/project/tweet-mapper)
12. [Snap Bird - search twitter&#39;s history](https://snapbird.org/)
13. [tinfoleak](https://tinfoleak.com/)
14. [The one million tweet map](https://onemilliontweetmap.com/?center=25.505,-0.09&zoom=2&search=&timeStep=0&timeSelector=0&hashtag1=&hashtag2=&hashtagBattle=0&timeRange=0&timeRange=25&heatmap=0&sun=0&cluster=1)
15. [GeoSocial Footprint](http://geosocialfootprint.com/)
16. [Twitter Email Test (TET): An OSINT tool](https://pdevesian.eu/tet)
17. [Download Twitter Videos to MP4 &amp; MP3! Online Easy &amp; Free](https://www.downloadtwittervideo.com/)
18. [Botcheck.me](https://botcheck.me/)
19. [Twelve Ways to Spot a Bot](https://medium.com/dfrlab/botspot-twelve-ways-to-spot-a-bot-aedc7d9c110c)
20. [How To Spot Russian Trolls](https://medium.com/dfrlab/trolltracker-how-to-spot-russian-trolls-2f6d3d287eaa)
21. [Tracking &#39;Russian bots in real-time&#39; Hamilton68](https://dashboard.securingdemocracy.org/)
22. [The Twitter Twayback Machine by staringispolite](https://staringispolite.github.io/twayback-machine/)
23. [Mentiomapp](http://mentionmapp.com/)
24. [Twitter List Finder](https://cse.google.com/cse/publicurl?cx=016621447308871563343:u4r_fupvs-e)
25. [Twiangulate: analyzing the connections between friends and followers](http://twiangulate.com/search/)
26. [Twitter Search — BackTweets](http://backtweets.com/)
27. [Scopedown](http://downloadperiscopevideos.com/)
28. [Periscope](https://periscope.tv/)
29. [OSoMe: Resources](https://osome.iuni.iu.edu/resources/)
30. [Twitter Cheat Sheet](https://thenative.com/wp-content/uploads/twitter-cheat-sheet-PDF.pdf)
31. [digitalmethodsinitiative/dmi-tcat](https://github.com/digitalmethodsinitiative/dmi-tcat/wiki)
32. [Understanding bots, botnets and trolls](https://ijnet.org/en/story/understanding-bots-botnets-and-trolls)
33. [Scraping &amp; Visualizing Twitter mentions – Dutch Osint Guy – Medium](https://medium.com/@dutchosintguy/scraping-visualizing-twitter-mentions-520a6277eb8b)
34. [Tweelpers](http://tweeplers.com/?cc=WORLD)
35. [https://spoonbill.io/](https://spoonbill.io/)
36. [Home page](http://twopcharts.com)
37. [twintproject/twint](https://github.com/twintproject/twint)
----

### Snapchat
1. [Snap Map](https://map.snapchat.com/)
2. [Snapdex: The Best Snapchat Names Index](https://www.snapdex.com/)
3. [CaliAlec/snap-map-private-api](https://github.com/CaliAlec/snap-map-private-api)
4. [Snapvip.io](https://snapvip.io/)
5. [Find My Snap -  Check if your SnapChat account was leaked on the 2013  hack.](http://findmysnap.com/)
----

### WhatsApp
1. [WhatsApp tools of all kind](http://wassame.com/tools/)
2. [Retrieve Metadata](https://github.com/LoranKloeze/WhatsAllApp)
3. [LoranKloeze/WhatsAllApp](https://github.com/LoranKloeze/WhatsAllApp)
4. [Fake WhatsApp Chat Generator](http://www.fakewhats.com/generator)
----

