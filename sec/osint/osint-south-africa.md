### Keyword Research
1. [OneLook Reverse Dictionary and Thesaurus](https://www.onelook.com/reverse-dictionary.shtml)
2. [Keyword Sheeter](https://keywordsheeter.com/)
3. [keyword discovery](https://www.keyworddiscovery.com/)
4. [Keyword Finder & SEO Difficulty Tool](https://kwfinder.com/)
5. [Keyword Tool](https://keywordtool.io/)
6. [Choose the Right Keywords with Our Research Tools](https://ads.google.com/home/tools/keyword-planner/)
7. [Semrush Keyword Magic Tool](https://www.semrush.com/lp/keyword-magic-tool-1/en/?kw=keyword%20search%20tool&cmp=Africa_SRCH_Keyword_Magic_Tool_EN&label=keyword_magic_tool&Network=g&Device=c&utm_content=509594370445&kwid=aud-781476728854:kwd-296894760171&cmpid=12618910293&agpid=116785405861&BU=Core&extid=&adpos=&gclid=CjwKCAjw7IeUBhBbEiwADhiEMZdNFXSwpYbOW6viarYuQlgS-WSVcpXeZNEa0WADSCJLCayW3O71gxoC3acQAvD_BwE)
8. [Keywords Explorer by Ahrefs: Discover Keyword Ideas and Analyze SEO Metrics](https://ahrefs.com/keywords-explorer)
----

### Search Engines
1. [Programmable Search Engine](https://cse.google.com/cse?cx=013991603413798772546:dhoqafcmphk#gsc.tab=0)
2. [Google (South Africa)](https://google.co.za)
3. [Bing.co.za](https://bing.co.za)
4. [Properties for sale and rent, Jobs, Used Cars and more](https://www.ananzi.co.za/)
5. [Welcome to Hotfrog](https://www.hotfrog.co.za/)
6. [Top Search Engine Marketing Services in South Africa - 2022 Reviews | Clutch.co](https://clutch.co/za/agencies/sem)
7. [IP Address API and Data Solutions](https://ipinfo.io/)
8. [Shodan](https://www.shodan.io/)
----

### Website Mirroring tool
1. [HTTrack Website Copier](https://www.httrack.com/)
2. [Wget](https://www.gnu.org/software/wget/)
3. [Softonic](https://blackwidow.en.softonic.com/)
----

### Tech Tools /Data
1. [Python Data Analysis Library](https://pandas.pydata.org/)
2. [Google Cloud Platform](https://console.cloud.google.com/apis/)
3. [Maltego](https://www.maltego.com/)
4. [SpiderFoot](https://www.spiderfoot.net/)
5. [IBM  Analytics](https://i2group.com/)
----

### Internet
1. [Coza.net.za](https://coza.net.za/)
2. [ZACR](https://www.registry.net.za/)
3. [.za Domain Name Authority (.ZADNA)](https://www.zadna.org.za/)
4. [Icasa.org.za](https://www.icasa.org.za/)
----

### Phone Number
1. [Truecaller](https://www.truecaller.com/search/us/515-419-8701)
2. [Advanced Background Checks](https://www.advancedbackgroundchecks.com/phone/)
3. [Best Phone Lookup](http://phonesearch.us/login.php)
4. [CarrierLookup | Find Cell Phone Carrier For Free &amp; Carrier Lookup API](https://www.carrierlookup.com/)
5. [CNAM and caller name lookup service provider - CID(name)Professional cnam service provider for asterisk, freeswitch, opensips, and all VoIP devices](http://www.cidname.com/)
6. [Data24-7 - Data on Demand.](https://www.data24-7.com/)
7. [Facebook](https://www.facebook.com/)
8. [Infobel - Phone directory](https://www.infobel.com/)
9. [Makelia - Public Records Repository](https://makelia.com/)
10. [NANPA : Area Code Map](https://www.nationalnanpa.com/area_code_maps/ac_map_static.html)
11. [NANPA Area Code Query](https://www.nationalnanpa.com/enas/npa_query.do)
12. [Numberway - Free White Pages - People Search - International Phone Books](https://www.numberway.com/)
13. [Pipl - Phone Number](https://pipl.com/)
14. [Reverse Phone Lookup - Find Name, Address &amp; More For Any Phone Number - Addresses.com](http://www.addresses.com/phone)
15. [slydial](https://www.slydial.com/)
16. [Spy Dialer](https://www.spydialer.com/default.aspx)
17. [SpyTox](https://www.spytox.com/)
18. [Superpages Online Yellow Pages, Local Business Directory](https://www.superpages.com/)
19. [Sync.ME - Caller ID &amp; Phone Number Search](https://sync.me/)
20. [TextMagic](https://www.textmagic.com/free-tools/carrier-lookup)
21. [Thats Them](https://thatsthem.com/reverse-phone-lookup)
22. [True People Search](https://www.truepeoplesearch.com/results?phoneno=(515)419-8701)
23. [US Phonebook](https://www.usphonebook.com/515-419-8701/detailed)
24. [White Pages | People Finder - AnyWho](https://www.anywho.com/reverse-lookup)
25. [Whocalld](https://whocalld.com/)
----

### New widget
1. [Brabys](https://www.brabys.com/)
2. [Sayellow.com](https://www.sayellow.com/)
3. [Best Directory](https://www.bestdirectory.co.za/)
4. [Thebusinessdirectory.co.za](https://www.thebusinessdirectory.co.za/)
5. [Yalwa.co.za](https://www.yalwa.co.za/)
6. [Eastrand.the-directory.co.za](https://eastrand.the-directory.co.za/)
7. [Freefind.co.za](https://www.freefind.co.za/)
8. [Rate it All](http://www.rateitall.co.za/index.php)
9. [Mpumalanga.online](https://www.mpumalanga.online)
----

### Geo-location
1. [Strava Global Heatmap](https://www.strava.com/heatmap#8.27/18.55841/-33.37631/hot/all)
2. [Geo Search Tool](https://youtube.github.io/geo-search-tool/search.html)
3. [Facebook Live](https://www.facebook.com/watch/live/?ref=live_delegate)
4. [RouteView](http://routeview.org/VirtualRide/)
5. [WiGLE: Wireless Network Mapping](https://wigle.net/)
6. [Radius Around a Point on a Map](https://www.freemaptools.com/radius-around-point.htm)
7. [MapQuest](https://www.mapquest.com/)
8. [Maperitive](http://maperitive.net/)
9. [South Africa Maps - Perry-Castañeda Map Collection - UT Library OnlinePerry-Castañeda Library Map Collection | The University of Texas at Austin](https://maps.lib.utexas.edu/maps/south_africa.html)
10. [South Africa AMS Topographic Maps - Perry-Castañeda Map Collection - UT Library Online](https://maps.lib.utexas.edu/maps/ams/south_africa/)
11. [Directions Map (with Animated Street View)](http://www.tripgeo.com/Directionsmap.aspx)
12. [DigitalGlobe](https://discover.digitalglobe.com/)
13. [www.bing.com/maps](https://www.bing.com/maps)
14. [🗺 BBBike Map Compare 🛰](https://mc.bbbike.org/mc/)
15. [Google Maps](https://www.google.com/maps)
16. [Geo Search Tool](https://youtube.github.io/geo-search-tool/search.html)
17. [GRASS GIS](https://grass.osgeo.org/)
18. [I Know Where Your Cat Lives](https://iknowwhereyourcatlives.com/cat/c863b469ce)
19. [IP Logger URL Shortener](https://iplogger.org/)
20. [Where Am I - Know your Location and Postal Address on Google Maps](https://www.labnol.org/maps/location/)
21. [GPS Visualizer: Quick Geocoder](https://www.gpsvisualizer.com/geocode)
----

### Email Formatting
1. [Email Format](https://email-format.com)
2. [Emailformat.de](https://emailformat.de/)
3. [ProspectLinked](https://prospectlinked.com/)
4. [Sales Navigator for Gmail](https://rapportive.com/)
5. [Samy Kamkar](https://samy.pl/peepmail)
6. [FindEmails](https://toofr.com)
7. [Anymailfinder.com](https://anymailfinder.com/)
8. [Emailformat.de](https://emailformat.de/)
9. [Email Generator](https://emailgenerator.io/)
10. [Email Permutator](https://metricsparrow.com/toolkit/email-permutator/)
11. [Fetching title...](https://docs.google.com/spreadsheet/ccc?key=0AoW7aksoVU98dGNFSUtfeXg4akpNTWM0Z2pHWjJzZUE#gid=0)
----

### Anti-trafficking / Missing
1. [Da Vinci Foundation](https://davincifoundation.org.za)
2. [Da Vinci Cybersecurity & Digital Forensics](https://davinciforensics.co.za)
3. [Da Vinci Group](https://davincigroup.co.za)
4. [Who We Are - Missing Children South Africa](https://missingchildren.org.za/who-we-are/)
5. [South Africa | Love Justice International](https://www.lovejustice.ngo/south-africa?utm_source=google_cpc&utm_medium=ad_grant&utm_campaign=awareness+%28locations%29&utm_term=human%20trafficking%20in%20south%20africa%20statistics&utm_campaign=*Awareness+(Continents)&utm_source=adwords&utm_medium=ppc&hsa_acc=4382957001&hsa_cam=15513067914&hsa_grp=133616716006&hsa_ad=589453720738&hsa_src=g&hsa_tgt=kwd-383596182710&hsa_kw=human%20trafficking%20in%20south%20africa%20statistics&hsa_mt=b&hsa_net=adwords&hsa_ver=3)
6. [Silentrights.co.za](https://www.silentrights.co.za/)
7. [Anti-Human Trafficking - The Salvation Army](https://www.salvationarmy.org.za/anti-human-trafficking/)
8. [https://www.justice.gov.za/vg/TIP.html](https://www.justice.gov.za/vg/TIP.html)
9. [South Africa launches Prevention and Combating of Trafficking in Persons National Policy Framework](https://www.unodc.org/unodc/en/human-trafficking/glo-act/south-africa-launches-prevention-and-combating-of-trafficking-in-persons-national-policy-framework.html)
10. [Stoptrafficking.org.za](https://www.stoptrafficking.org.za/)
11. [Human Trafficking in South Africa - The Borgen Project](https://borgenproject.org/human-trafficking-in-south-africa/)
12. [https://www.dominofoundation.org.za/red-light/](https://www.dominofoundation.org.za/red-light/)
13. [0800222777.org.za](https://0800222777.org.za/#:~:text=0800%20222%20777%20is%20the,fastest%20response%20to%20each%20case.)
14. [Human Trafficking | A21](https://www.a21.org/content/human-trafficking/gqe0rc?permcode=gqe0rc)
15. [Articles about human trafficking in South Africa   - Choma](https://choma.co.za/1tags/1261/human-trafficking-in-south-africa)
16. [Department of Social Development - South Africans Should Be Vigilant About Child Trafficking...](https://www.dsd.gov.za/index.php/latest-news/21-latest-news/360-south-africans-should-be-vigilant-about-child-trafficking)
17. [MissingKids](https://www.missingkids.org/)
18. [The Pink Ladies Organisation for Missing Children](http://thepinkladiesza.weebly.com/)
19. [DHT Projects — Disrupt Human Trafficking](https://www.disrupthumantrafficking.com/projects)
20. [Human Trafficking | Western Cape Government](https://www.westerncape.gov.za/general-publication/human-trafficking)
21. [toc](https://www.unodc.org/southernafrica/en/toc.html)
22. [Criminal Justice and Corruption](https://www.unodc.org/southernafrica/en/cjc/index.html)
----

### South African Police
1. [Home page of the Directorate for Priority Crime Investigation](https://www.saps.gov.za/dpci/index.php)
2. [Home page of the SAPS Internet](https://www.saps.gov.za/index.php)
3. [https://www.saps.gov.za/journal/comm_pol.php](https://www.saps.gov.za/journal/comm_pol.php)
4. [Services | SAPS (South African Police Service)](https://www.saps.gov.za/faqdetail.php?fid=6#forensic)
5. [Cold Case Missing Persons](https://www.facebook.com/groups/2768498316746790)
6. [INTERPOL](https://www.interpol.int/en)
----

### Legal
1. [Databases](http://www.saflii.org/content/databases)
2. [Golegal.co.za](https://www.golegal.co.za/)
3. [Lexisnexis.co.za](https://www.lexisnexis.co.za/)
4. [Welcome to Sabinet](https://sabinet.co.za/)
5. [South African Legal Databases - Researching South African Law Library Guide - LibGuides at University of Cape Town](https://libguides.lib.uct.ac.za/c.php?g=784962&p=5622295)
----

### Credit Bureaus
1. [Experian](https://www.experian.co.za/)
2. [TransUnion](https://www.transunion.co.za/)
3. [Understand Your Credit Profile | Sanlam Credit Solutions](https://www.sanlam.co.za/personal/financialplanning/creditprofile/Pages/default.aspx?gclsrc=ds&gclsrc=ds)
4. [Mycreditcheck.co.za](https://www.mycreditcheck.co.za/)
5. [ClearScore ZA](https://www.clearscore.com/za)
----

### Government
1. [Companies and Intellectual Property Commission, CIPC South Africa](http://www.cipc.co.za/za/)
2. [National Department of Basic Education > Home](https://www.education.gov.za/)
3. [Statssa.gov.za](https://www.statssa.gov.za/)
4. [Dha.gov.za](http://www.dha.gov.za/)
5. [South African government](https://www.gov.za/)
6. [Electoral Commission of South Africa (IEC)](https://www.elections.org.za/content/)
7. [https://www.justice.gov.za/vg/TIP.html](https://www.justice.gov.za/vg/TIP.html)
8. [Sapostalcodes.info](https://sapostalcodes.info/)
9. [Postal Codes  South Africa](https://postalcodez.co.za/)
10. [Postal Codes](https://www.postoffice.co.za/questions/postalcode.html)
11. [Parliament.gov.za](https://www.parliament.gov.za/)
12. [Ssa.gov.za](https://www.ssa.gov.za/)
13. [Cybersecurityhub.gov.za](https://www.cybersecurityhub.gov.za/)
14. [Human Trafficking | Western Cape Government](https://www.westerncape.gov.za/general-publication/human-trafficking)
15. [https://www.labour.gov.za/](https://www.labour.gov.za/DOL/)
16. [Justice/Branch Structure/COS](https://www.justice.gov.za/branches/organo/organo_COS.htm)
17. [https://www.justice.gov.za/contact/contact_list.html](https://www.justice.gov.za/contact/contact_list.html)
18. [http://www.dha.gov.za/index.php/immigration-services/south-african-ports-of-entry](http://www.dha.gov.za/index.php/immigration-services/south-african-ports-of-entry)
----

### Chamber of Commerce
1. [Southern African-German Chamber of Commerce and Industry](https://suedafrika.ahk.de/en/?q=user&cHash=8143114eb565f1098289ad63107b2b23)
2. [Capechamber.co.za](https://capechamber.co.za/)
3. [Welcome to Sacci](https://sacci.org.za/)
4. [Belgianchambersa.co.za](https://belgianchambersa.co.za/)
5. [Amcham.co.za](https://amcham.co.za/)
6. [Fsacci.com](https://www.fsacci.com/)
7. [ECSECC - Eastern Cape Socio Economic Consultative Council](https://www.ecsecc.org/page/organised-business)
----

### Identity / criminal Record checks
1. [HURU Technologies Portal](https://portal.huru.co.za/register)
2. [Criminal Checks | LexisNexis South Africa](https://www.lexisnexis.co.za/lexisrefcheck/lexis-refcheck-verifications/criminal-checks)
3. [Justice/Criminal/NRSO](https://www.justice.gov.za/vg/nrso.html)
4. [Department of Home Affairs - Track & Trace your Status](http://www.dha.gov.za/index.php/minister-ofhome-affairs/28-track-trace/78-track-traceyour-status)
5. [SA ID Number Validator](https://www.checkid.co.za/)
----

### Law Enforcement Information Requests
1. [Facebook/Instagram Law Enforcement Online Requests - Portal](https://www.facebook.com/records/login/)
2. [Facebook Law Enforcement Online Requests - Guidelines](https://www.facebook.com/safety/groups/law/guidelines/)
3. [Twitter Law Enforcement Online Requests - Portal](https://legalrequests.twitter.com/forms/landing_disclaimer)
4. [Twitter Law Enforcement Online Requests - Guidelines](https://help.twitter.com/en/rules-and-policies/twitter-law-enforcement-support)
5. [WhatsApp Law Enforcement Request - Portal](https://www.whatsapp.com/records/login/)
6. [WhatsApp Information for Law Enforcement - Guidelines](https://faq.whatsapp.com/en/android/26000050/?category=5245250)
7. [Google/Youtube Information Requests - Guidelines](https://support.google.com/transparencyreport/answer/7381738?hl=en)
8. [Google/Youtube information Requests - Portal log in](https://lers.google.com/signup_v2/landing#/requestaccount)
9. [LinkedIn Law Enforcement Data Requests](https://www.linkedin.com/help/linkedin/answer/16880/linkedin-law-enforcement-data-request-guidelines?lang=en)
10. [Pinterest Request Guidelines](https://help.pinterest.com/en/article/law-enforcement-guidelines)
11. [VK Guidelines](https://vk.com/data_protection?section=requests)
12. [Tumblr Law Enforcement Guidelines and Requests](https://www.tumblr.com/docs/en/law_enforcement)
13. [Reddit Law Enforcement Guidelines and Reports](https://www.reddit.com/wiki/law_enforcement_guidelines)
14. [Line - User info Disclosure Requests by Law Agencies](https://linecorp.com/en/security/article/35)
15. [Snap Chat - Law Enforcement Guidelines and Contact Information](https://storage.googleapis.com/snap-inc/privacy/lawenforcement.pdf)
16. [WeChat - Law Enforcement Contact Information](https://www.wechat.com/en/contact_us.html)
17. [Viber Guidelines and Contact Information](https://www.viber.com/terms/viber-privacy-policy/#disclosure)
18. [Signal - Information Request Contact Information](https://signal.org/bigbrother/)
19. [Microsoft Request Guidelines](https://blogs.microsoft.com/datalaw/our-practices/)
20. [Apple Request Portal](https://www.apple.com/sg/privacy/government-information-requests/)
21. [Yahoo Guidelines and Contact information](https://www.eff.org/document/yahoo-law-enforcement-guide)
22. [Adobe Guidelines and Contact Information](https://www.adobe.com/legal/lawenforcementrequests.html)
23. [Report Abuse to Telegram](https://t.me/isiswatch/2)
24. [Twitch Information Requests](https://www.twitch.tv/p/legal/terms-of-service/#17-requests-for-user-information)
25. [Tiktok Law Enforcement Request Guidelines](https://www.tiktok.com/legal/law-enforcement?lang=en)
26. [Tam Tam Contact Information](https://about.tamtam.chat/en/policy-eu/)
----

### Webcams
1. [Southafricawebcams.co.za](https://www.southafricawebcams.co.za/)
2. [Live Cams in South Africa](https://www.skylinewebcams.com/en/webcam/south-africa.html)
3. [Live Streaming Webcams | African Wildlife | African Game Reserves](https://www.mangolinkcam.com/webcams/mammals/african-wildlife.html)
4. [Capetown-webcam.com](http://www.capetown-webcam.com/)
5. [https://www.webcamtaxi.com/en/south-africa.html](https://www.webcamtaxi.com/en/south-africa.html)
6. [See South Africa Live Webcams & Weather Reports | SeeCam](https://www.see.cam/za)
----

### News / News Search Engines
1. [News24](https://news24.com)
2. [IOL](https://iol.co.za)
3. [City Press](https://citypress.co.za)
4. [Times Live](https://timeslive.co.za)
5. [Daily Sun](https://dailysun.co.za)
6. [Mail and Guardian](https://mg.co.za/)
7. [SABC](https://sabc.co.za)
8. [Sowetan LIVE](https://sowetanlive.co.za)
9. [Business Day](https://www.businesslive.co.za/bd/)
10. [Caxtonlocalmedia.co.za](https://caxtonlocalmedia.co.za/)
11. [1stHeadlines-Breaking News](https://www.1stheadlines.com/)
12. [Google News](https://news.google.com/topstories?hl=en-US&gl=US&ceid=US:en)
13. [Google News Archive Search](https://news.google.com/newspapers?hl=en)
14. [NewsNow: the independent news discovery platform](https://www.newsnow.co.uk/h/)
15. [NewspaperArchive®](https://newspaperarchive.com/)
16. [AllYouCanRead](https://www.allyoucanread.com/)
17. [Search](https://www.newspapers.com/search/)
18. [British Newspapers](https://search.findmypast.com/search/british-newspapers)
19. [Newspaper Directory:  DailyEarth](http://dailyearth.com/index.html)
20. [Search U.S. Newspaper Directory, 1690-Present « Chronicling America « Library of Congress](https://chroniclingamerica.loc.gov/search/titles/)
21. [newspaper map](https://newspapermap.com/)
22. [WorldNews](https://wn.com/)
23. [Paperboy Online Newspapers](https://www.thepaperboy.com/index.cfm)
24. [Africa News | ReutersGalleryVideoGalleryVideoGalleryVideoGalleryGallery](https://www.reuters.com/world/africa/)
25. [The New Humanitarian | AfricaTNH_LOGO_Primary_K_Burgundy_RGB](https://www.thenewhumanitarian.org/Africa)
26. [PressReader.com - Digital Newspaper & Magazine Subscriptions](https://www.pressreader.com/catalog/country/south-africa)
----

### Rail
1. [Rail - Department-of-Transport](https://www.transport.gov.za/rail)
2. [Rsr.org.za](https://www.rsr.org.za/)
----

### Maritime
1. [MarineTraffic: Global Ship Tracking Intelligence | AIS Marine Traffic](https://www.marinetraffic.com/en/ais/home/centerx:18.2/centery:-3.0/zoom:3)
2. [VesselFinder](https://www.vesselfinder.com/)
3. [3D Submarine cable map - InfoWorldMaps](https://infoworldmaps.com/3d-submarine-cable-map/)
4. [Office of Foreign Assets Control](https://home.treasury.gov/policy-issues/office-of-foreign-assets-control-sanctions-programs-and-information)
5. [Sea Archives - defenceWeb](https://www.defenceweb.co.za/category/sea/sea-sea/)
6. [SAMSA -](https://www.samsa.org.za/Pages/default.aspx)
7. [Search global export import ports directory, by port code, port name & country](https://www.volza.com/ports)
8. [Container BIC Code Lookup Tool](https://www.bic-code.org/bic-codes/)
9. [Prefixlist](https://www.prefixlist.com/)
10. [Ship Finder](https://shipfinder.co/)
11. [CruiseMapper](https://www.cruisemapper.com/)
12. [Container tracking](https://www.track-trace.com/container)
13. [Our Divisions
            
            
            Transnet Port Terminals](https://www.transnet.net/Divisions/Pages/TPT.aspx)
----

### Cell Phone Service Providers
1. [Vodacom](https://vodacom.co.za)
2. [WELCOME TO MTN](https://mtn.co.za)
3. [Cellphone Contracts, Prepaid & Data](https://cellc.co.za)
4. [Official Telkom website home page](https://www.telkom.co.za/today/)
5. [Rain.co.za](https://www.rain.co.za/)
----

### Air
1. [https://www.airports.co.za/utilities/live-flight-info](https://www.airports.co.za/utilities/live-flight-info)
2. [Cape Town International Aiport Flights | CPT](https://www.southafrica.to/transport/Airports/Cape-Town-International/Cape-Town-International-Airport.php5)
3. [Durban International Aiport](https://www.southafrica.to/transport/Airports/Durban-International/Durban-International-Airport.php5)
4. [Lanseria Airport (Johannesburg)](https://www.southafrica.to/transport/Airports/Lanseria-Airport/Lanseria-Airport.php5)
5. [O.R. Tambo International Airport Flight Bookings | JNB](https://www.southafrica.to/transport/Airports/O-R-Tambo-International/O-R-Tambo-International-Airport.php5)
6. [Bram Fischer International Airport - Wikipedia](http://en.wikipedia.org/wiki/Bloemfontein_airport)
7. [King Phalo Airport - Wikipedia](http://en.wikipedia.org/wiki/East_London_Airport)
8. [Eastgateairport.co.za](http://www.eastgateairport.co.za/)
----

