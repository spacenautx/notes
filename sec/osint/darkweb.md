### Darkweb links
1. [OnionLink.  We link you up with the onions.](http://onion.link/)
2. [Google Hacking Database (GHDB)](https://www.exploit-db.com/google-hacking-database/)
3. [Tor Project](https://www.torproject.org)
4. [DuckDuckGo](https://duckduckgo.com/?t=)
5. [Torscan.io - An Onion Technology Search Engine](http://www.torscan.io/)
6. [IP address used by tor?](https://exonerator.torproject.org/)
7. [TOR Darknet site listing](http://onions.es/)
8. [Onionlandsearchengine.com](https://onionlandsearchengine.com/)
9. [Ipfs-search.com](https://ipfs-search.com/)
10. [links naar TOR websites](http://hackingnewstutorials.com/huge-list-of-darknet-deep-web-hidden-websites-2017/)
----

### Hacking
1. [Norse Attack Map](http://map.norsecorp.com/#/?geo=eu)
2. [The Latest Google Dorks List](https://edgylabs.com/google-dorks-list)
3. [Google Dorks](https://gbhackers.com/latest-google-dorks-list/)
4. [Leaked Databases?](https://www.vigilante.pw/)
----

### Information material
1. [Laat Je Niet Hack Maken](https://laatjeniethackmaken.nl/)
2. [Projects Yeslab](http://yeslab.org/projects)
3. [WikiLeaks World Tomorrow](https://worldtomorrow.wikileaks.org/index.html)
4. [Tails](https://tails.boum.org/index.en.html)
5. [WikiLeaks](https://wikileaks.org/)
6. [14 days running a secret Dark Web pedophile honeypot (and why I now think Tor is the devil) - Geek Slop](http://geekslop.com/2015/catching-pedophiles-running-secret-dark-web-tor-honeypot)
7. [Hackers info](http://www.zone-h.org/?zh=2)
8. [Deep Web Links 2018](https://www.deepweb-sites.com/deep-web-links-2015/)
9. [Deep Dot Web](https://www.deepdotweb.com/)
10. [Offensive Security’s Exploit Database Archive](https://www.exploit-db.com/google-hacking-database)
----

### Interesting websites
1. [Reddit Nederlands](https://www.reddit.com/r/thenetherlands/)
2. [Reddit zoekresultaten - 4chan](https://www.reddit.com/r/4chan/search?q=4chan&restrict_sr=on)
3. [4chan](http://www.4chan.org/)
4. [tor - Pastebin.com](https://pastebin.com/search?q=tor)
5. [/v/darknetmarkets](https://voat.co/v/darknetmarkets)
6. [Deep Web • r/deepweb](https://www.reddit.com/r/deepweb/)
7. [RBI: Reddit Bureau of Investigation • r/RBI](https://www.reddit.com/r/RBI/)
8. [tornodes](https://www.dan.me.uk/tornodes)
----

### Bitcoin
1. [Bitcoin Address Lookup, Checker and Alerts - BitcoinWhosWho](http://bitcoinwhoswho.com/)
2. [WalletExplorer.com: smart Bitcoin block explorer](https://www.walletexplorer.com/)
3. [Blockchair / Bitcoin / Outputs](https://blockchair.com/bitcoin/outputs)
4. [Address Checker Bitcoins](http://addresschecker.eu/)
5. [Addresswatcher](https://addresswatcher.com/)
6. [blockchain.com/explorer](https://www.blockchain.com/explorer)
----

### Communication
1. [PGP de-encrypting](https://sela.io/pgp/)
2. [Documentation Mailvelope](https://www.mailvelope.com/en/help)
3. [Dutch PGP werking](https://www.nu.nl/internet/3784407/zo-versleutel-je-e-mail-met-pgp.html)
----

### Search in Darkweb
1. [Ahmia — Search Tor Hidden Services](https://ahmia.fi/)
2. [search-clearweb-copy and paste in TOR browser!!!](https://onionlandsearchengine.com/)
3. [CachedView](https://cachedview.nl/)
4. [Google Cached Pages of Any Website](https://cachedview.com/)
5. [d﻿ark.fai﻿l: Which darknet sites are online?](https://dark.fail/)
----

