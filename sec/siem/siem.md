# siem

-------
### security information and event management
- Various hook detection and restore (Splicing, EAT/IAT, SSDT and more)
- System component integrity check
- Hidden process detection (kernel and user level)
- Floating code detection
- Complete "hooked by" information retrieval
- Layer-by-layer inspection for multi-hooks
- Advanced process and memory dumps
- Kernel log inspection

### terminal log viewer
- https://github.com/dogoncouch/logdissect
- https://github.com/allinurl/goaccess
- https://lnav.org/features
- https://github.com/Delgan/loguru
- https://github.com/klaussinani/signale

- [MITRE ATT&CK](https://attack.mitre.org)

### Audit
- https://github.com/mzet-/linux-exploit-suggester
- https://github.com/rebootuser/LinEnum
- https://github.com/diego-treitos/linux-smart-enumeration
- https://github.com/CISOfy/lynis
- https://github.com/AlessandroZ/BeRoot
- https://github.com/future-architect/vuls
- https://github.com/ngalongc/AutoLocalPrivilegeEscalation
- https://github.com/b3rito/yodo
- https://github.com/belane/linux-soft-exploit-suggester
- https://github.com/jondonas/linux-exploit-suggester-2
- https://github.com/DominicBreuker/pspy
- https://github.com/sevagas/swap_digger
- https://github.com/NullArray/RootHelper
- https://github.com/NullArray/MIDA-Multitool
- https://github.com/initstring/dirty_sock
- https://github.com/sosdave/KeyTabExtract
- https://github.com/itsKindred/modDetective
- https://github.com/nongiach/sudo_inject
- https://github.com/kitabisa/teler
- https://github.com/panther-labs/panther

### elk stack
- [elasctic search](https://github.com/elastic/elasticsearch)
- [logstash](https://github.com/elastic/logstash)
- [kibana](https://github.com/elastic/kibana)

### resources
- https://github.com/opendatav/datav
- https://github.com/CISOfy/lynis
- https://github.com/ossec/ossec-hids
- https://github.com/bradley-evans/cfltools
- https://github.com/Neo23x0/sigma
- https://github.com/Graylog2/graylog2-server
- https://github.com/Cyb3rWard0g/HELK
- https://github.com/netdata/netdata
- https://github.com/SigmaHQ/sigma
- https://github.com/baidu/openrasp
- https://github.com/jivoi/awesome-ml-for-cybersecurity
- https://posts.specterops.io/ready-to-hunt-first-show-me-your-data-a642c6b170d6
- https://posts.specterops.io/welcome-to-helk-enabling-advanced-analytics-capabilities-f0805d0bb3e8
- https://gbhackers.com/security-information-and-event-management-siem-a-detailed-explanation/
- https://github.com/marshall/logcat-color
- https://github.com/sonatype-nexus-community/DevAudit
- https://github.com/OTRF/ThreatHunter-Playbook
- https://github.com/yzhao062/anomaly-detection-resources

### logs
- https://github.com/uber-go/zap
- https://www.honeycomb.io/blog/lies-my-parents-told-me-about-logs/
- https://sobolevn.me/2020/03/do-not-log

### anti forensics
- https://www.inversecos.com/2022/06/detecting-linux-anti-forensics-log.html

### tools
- grafana
- telegraf
- influxdb
- https://medium.com/javarevisited/6-log-management-tools-which-one-do-you-like-3635303c495
- https://github.com/matanolabs/matano

### resources
- https://gist.github.com/isaqueprofeta/d14f394d8679fce0a11d7961d514fcdd
