### browser exploitation

- https://connormcgarr.github.io/type-confusion-part-3/
- https://gist.github.com/shamb0/4ba7177d08af24490ab4a5ba9531e42c
- https://github.com/AZMagic/Slient-Url-Exploit-New-Cve-Chrome-Exploit-Html-Downloader-Put-Your-Link
- https://github.com/Escapingbug/awesome-browser-exploit
- https://github.com/cezary-sec/awesome-browser-security/
- https://github.com/emredavut/Chrome-Android-and-Windows-0day-RCE-SBX
- https://github.com/m1ghtym0/browser-pwn
- https://jhalon.github.io/chrome-browser-exploitation-1/
- https://connormcgarr.github.io/

 
- https://connormcgarr.github.io/type-confusion-part-1/
- https://learnappsec.com/blog/awesome-browser-security/

### basics
- [browser engineering](https://browser.engineering/)
- https://developer.chrome.com/blog/inside-browser-part1/
- https://developer.chrome.com/blog/inside-browser-part2/
- https://developer.chrome.com/blog/inside-browser-part3/
- https://joshondesign.com/2020/03/10/rust_minibrowser

### Headless Browsers
- https://github.com/dhamaniasad/HeadlessBrowsers

### data
- https://github.com/moonD4rk/HackBrowserData
