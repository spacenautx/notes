# fuzzing

- https://media.ccc.de/v/37c3-12235-fuzzing_the_tcp_ip_stack#t=491
- https://blog.regehr.org/
- https://blog.bricked.tech/posts/tmnf/part1/

- https://github.com/klecko/kvm-fuzz
- https://github.com/ex0dus-0x/fuzzable
- https://blog.isosceles.com/how-to-build-a-corpus-for-fuzzing/
- https://security.googleblog.com/2023/08/ai-powered-fuzzing-breaking-bug-hunting.html
- https://offsec.almond.consulting/ghostscript-cve-2023-28879.html

# fuzzing papers to read
- https://github.com/0xricksanchez/paper_collection


# AFL

- https://epi052.gitlab.io/notes-to-self/blog/2021-11-26-fuzzing-101-with-libafl-part-4/
- https://afl-1.readthedocs.io/en/latest/user_guide.html#
- https://github.com/google/fuzzing/blob/master/docs/building-fuzz-targets.md

- https://github.com/antonio-morales/EkoParty_Advanced_Fuzzing_Workshop
- https://github.com/Dor1s/libfuzzer-workshop
- https://github.com/mykter/afl-training
- https://github.com/antonio-morales/Fuzzing101

- https://fuzzing.in/codelabs/fuzzing_linux/index.html
- https://medium.com/@ayushpriya10/fuzzing-applications-with-american-fuzzy-lop-afl-54facc65d102
- https://blog.f-secure.com/super-awesome-fuzzing-part-one/
- https://foxglovesecurity.com/2016/03/15/fuzzing-workflows-a-fuzz-job-from-start-to-finish/
- https://www.lazenca.net/display/TEC/AFL+-+American+fuzzy+lop
- https://dl.acm.org/doi/10.1145/3412452.3423572
- https://research.aurainfosec.io/hunting-for-bugs-101/
- https://xz.aliyun.com/t/4314
- https://fuzzing-project.org/tutorial3.html
- https://trustfoundry.net/introduction-to-triaging-fuzzer-generated-crashes/
- https://github.com/jfoote/exploitable
- https://github.com/bnagy/crashwalk
- https://github.com/rr-debugger/rr
- https://blog.ret2.io/2018/06/19/pwn2own-2018-root-cause-analysis/
- http://9livesdata.com/2018/03/fuzzing-how-to-find-bugs-automagically-using-afl/
- https://stfpeak.github.io/2017/06/12/AFL-Cautions/
- https://animal0day.blogspot.com/2017/05/fuzzing-apache-httpd-server-with.html
- https://stfpeak.github.io/2017/06/11/Finding-bugs-using-AFL/
- https://thalium.github.io/blog/posts/rdpegfx/
- https://www.youtube.com/watch?v=2bTmB3cwhxs

- https://www.fuzzingbook.org/
- https://github.com/wcventure/FuzzingPaper
- https://airbus-seclab.github.io/AFLplusplus-blogpost/

- https://github.com/AFLplusplus/AFLplusplus/tree/stable/docs
- https://github.com/secfigo/Awesome-Fuzzing
- https://sha256.net/fuzzing-ping.html
- https://blog.trailofbits.com/2023/02/14/curl-audit-fuzzing-libcurl-command-line-interface/
- https://github.com/FuzzingLabs/thoth/blob/master/doc/symbolic_execution.md
- https://f0rm2l1n.github.io/2021-02-02-syzkaller-diving-01/
- https://sthbrx.github.io/blog/2021/06/14/fuzzing-grub-part-2-going-faster/
- https://airbus-seclab.github.io/AFLplusplus-blogpost/
- https://bushido-sec.com/index.php/2023/06/19/the-art-of-fuzzing/
- https://ricercasecurity.blogspot.com/2023/07/fuzzing-farm-4-hunting-and-exploiting-0.html
- https://aws.amazon.com/blogs/opensource/announcing-snapchange-an-open-source-kvm-backed-snapshot-fuzzing-framework/

### browser
- https://bugs.chromium.org/p/chromium/issues/list

# rust
- https://academy.fuzzinglabs.com/introduction-rust-fuzzing

### android
- https://fuzzing.science/blog/Fuzzing-Android-Native-libraries-with-libFuzzer-Qemu

# libfuzzer

Definition: a fuzz target is a function that has the following signature and does something interesting with it's arguments:

```c
extern "C" int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
  DoSomethingWithData(Data, Size);
  return 0;
}
```
- https://news.ycombinator.com/item?id=20829712
- https://www.lazenca.net/display/TEC/libFuzzer
- https://stfpeak.github.io/2017/05/24/Finding-bugs-using-libFuzzer/
- https://www.darkrelay.com/post/fuzzing-with-libfuzzer

# fuzzilli
- https://github.com/googleprojectzero/fuzzilli

# rewind
- https://github.com/quarkslab/rewind

### llvm
- https://www.aosabook.org/en/llvm.html


### resources
- https://forallsecure.com/resources
- https://h0mbre.github.io/Fuzzing-Like-A-Caveman/#
- https://0xninja.fr/bggp3/

#### videos
- https://www.youtube.com/user/gamozolabs/videos
- https://www.youtube.com/watch?v=BrDujogxYSk
- https://www.youtube.com/watch?v=SngK4W4tVc0

#### lab
- https://github.com/Dor1s/libfuzzer-workshop
- https://github.com/ouspg/libfuzzerfication
- https://github.com/ianare/exif-samples/tree/master/jpg

### silifuzz
- https://github.com/google/silifuzz

### books
- https://blog.revolutionanalytics.com/2014/01/the-fourier-transform-explained-in-one-sentence.html
