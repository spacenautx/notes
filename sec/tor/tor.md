# tor

- [whether address was used as a Tor relay](https://metrics.torproject.org/exonerator.html)
- https://github.com/N3Dx0o/Tor-exit-nodes-grabber
- https://github.com/fastfire/deepdarkCTI
- https://github.com/ruped24/toriptables2
- https://www.osintcombine.com/post/the-other-dark-nets
- https://dark.fail/
- https://github.com/D4RK-R4BB1T/Dark-Web-Archives

### tools
- https://github.com/ahmia/ahmia-site
- https://www.thc.org/segfault/
