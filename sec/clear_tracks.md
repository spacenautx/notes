# clear tracks

```bash
*Leave Bash without history*
# Tell Bash to use /dev/null instead of ~/.bash_history.
# This is the first command we execute on every shell.
# It will stop the Bash from logging your commands.

export HISTFILE=/dev/null
```

*It is good housekeeping to 'commit suicide' when exiting a shell*
```bash
alias exit='kill -9 $$'
```

*Any command starting with a " " (space) will not get logged to history either.*
```bash
 id
```
*Hide your command*
```bash
exec -a syslogd nmap -T0 10.0.2.1/24
```

*Alternatively if there is no Bash*
```bash
# In this example we execute nmap but let it appear with the name syslogd in ps alxwww process list.
cp `which nmap` syslogd
PATH=.:$PATH syslogd -T0 10.0.2.1/24
```

*Hide your arguments*
```bash
# Download zap-args.c.
# This example will execute nmap but will make it appear as 'syslogd' without any arguments in the ps alxww output.

gcc -Wall -O2 -fpic -shared -o zap-args.so zap-args.c -ldl
LD_PRELOAD=./zap-args.so exec -a syslogd nmap -T0 10.0.0.1/24

# Note: There is a gdb variant as well. Anyone?
```

*Almost invisible SSH*
```bash
ssh -o UserKnownHostsFile=/dev/null -T user@host.org "bash -i"
```

## resources
- https://hackingpassion.com/clear-your-tracks-on-linux/
- https://github.com/TheSpeedX/PROXY-List
- https://github.com/ffffffff0x/Digital-Privacy
- https://github.com/pluja/awesome-privacy
- https://github.com/Lissy93/personal-security-checklist
- https://codingheroes.io/resources/
- https://github.com/captainGeech42/ransomwatch
- [browser privacy](https://privacytests.org/)
- https://github.com/danoctavian/awesome-anti-censorship
- https://cybertoolbank.cc
- https://github.com/yaelwrites/Big-Ass-Data-Broker-Opt-Out-List
