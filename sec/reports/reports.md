# reports


- https://github.com/blacklanternsecurity/writehat
- https://github.com/ZephrFish/BugBountyTemplates/blob/master/Example.md
- https://github.com/juliocesarfort/public-pentesting-reports

* =bb report=
Issue Description
Issue Identified
Risk Breakdown
Affected URLs
Steps to Reproduce
Affected Demographic
Recommendation
References


* template
+ =Executive=
1. Business Impact
2. Customization
3. Talking to the business
4. Affect bottom line
5. Strategic Roadmap
6. Maturity model
7. Appendix with terms for risk rating

+ =Technical=
1. Identify systemic issues and technical root cause analysis
2. Maturity Model
3. Technical Findings
	Description
	Screen shots
	Ensure all PII is correctly redacted
	Request/Response captures
	PoC examples
	Ensure PoC code provides benign validation of the flaw
4. Reproducible Results
	Test Cases
	Fault triggers
5. Incident response and monitoring capabilities
	Intelligence gathering
	Reverse IDS
	Pentest Metrics
	Vuln. Analysis
	Exploitation
	Post-exploitation
	Residual effects (notifications to
	3rd parties, internally, LE, etc...)
6. Common elements

+ =Methodology=

Objective(s)
Scope
Summary of findings
Appendix with terms for risk rating

Quantifying the risk
1. Evaluate incident frequency
	probable event frequency
	estimate threat capability
	(from 3 - threat modeling)
	Estimate controls strength (6)
	Compound vulnerability (5)
	Level of skill required
	Level of access required
2. Estimate loss magnitude per incident
	Primary loss
	Secondary loss
	Identify risk root cause analysis
	Root Cause is never a patch
	Identify Failed Processes
3. Derive Risk
	Threat
	Vulnerability
	Overlap
	Deliverable

1. Preliminary results
2. Review of the report with the customer
3. Adjustments to the report
4. Final report
5. Versioning of Draft and Final Reports
6. Presentation
	Technical
	Management Level
7. Workshop / Training
	Gap Analysis (skills/training)
8. Exfiltarted evidence and any other raw (non-proprietary) datagathered.
9. Remediation Roadmap
	Triage
	Maturity Model
	Progression Roadmap
	Long-term Solutions
	Defining constraints

* resources

- http://www.pentest-standard.org/index.php/PTES_Technical_Guidelines#Reporting
- https://github.com/santosomar/public-pentesting-reports

* Footnotes

* DFIR reports
- https://github.com/statnmap/pdfreport
- https://github.com/dfirtrack/dfirtrack
- https://github.com/dastergon/postmortem-templates
- https://github.com/meirwah/awesome-incident-response
- https://github.com/certsocietegenerale/IRM/tree/main/EN

### tools
- https://hakin9.org/blackstone-project-pentesting-reporting-tool/
