# Phishing toolkit:


### Browser In The Browser (BITB) Templates
https://github.com/mrd0x/BITB

### SiteCopy
Website replication, also known as website backup. It is to save all the content on the web page through the tool. Of course, not only an html page is saved, but all the css, js and static files contained in the source code of the web page are saved, so that the entire website can be browsed locally.
Through it, you can quickly get the front-end source code of the fishing spot, modify it and convert it to PHP or JAVA. Modify the front-end request parameters and method entry to the back-end and write an api interface to receive the front-end parameters. You can also add the requester's IP and UA facilitates further utilization. Commonly used phishing points such as OA, mail service, management platform, etc., can prepare in advance the common OA, mail service, management platform and other phishing point transformation reserves on the market.
There are other equally good imitation site gadgets such as: https://smalltool.github.io/ is also a good efficiency tool to quickly pull the source code of fishing spots, or it is also a good way to use wget under Linux.
Address: https://github.com/Threezh1/SiteCopy.git

### PhishingLogin
The function of the script is mainly used to receive the account password entered in the phishing page form. The parameters to be received can be added according to your own needs. The script is rough, please improve it yourself. In addition, you can also use your own JS probe directly Find the phishing page and collect the target machine information easily. In practice, try to collect as much information as possible with the least operation.
Address: https://github.com/fuzz-security/PhishingLogin.git

### formhish
An automated website based on a phishing form. The tool can automatically detect inputs on html form based websites to create phishing pages.
Address: https://github.com/t3rm1naLdr01d/formphish

### Mail service platform
You can send phishing emails with one click
Address: https://github.com/sumerzhang/PhishingInstall.git

### Mail service probe
Mailbox probe background management system, this probe can determine whether the target has clicked on the mail.
Address: https://github.com/r00tSe7en/Mail-Probe.git

### espoofer
espoofer is an open source testing tool that can bypass SPF, DKIM and DMARC authentication in email systems. It helps mail server administrators and penetration testers to check whether target email servers and clients are vulnerable to email spoofing attacks or can be abused to send spoofed emails.
Address: https://github.com/chenjj/espoofer.git

### PhishAPI
A comprehensive web-based phishing kit with rapid deployment and real-time alerts! The API has three main functions. One allows you to easily deploy cloned login pages for credential theft, another is weaponized Word document creation, and a third is a saved email campaign template. Both attack methods are integrated into Slack for real-time alerts
Address: https://github.com/curtbraz/PhishAPI.git
Unconventional Fishing Ideas

### QRLJacking
QR code fishing
https://github.com/OWASP/QRLJacking.git

### lockphish
A tool for phishing attacks on the lock screen, designed to obtain Windows credentials, Android PIN and iPhone Passcode using https links. This point may still take a lot of time to polish and throw it directly, and the other party may not see it.
Address: https://github.com/JasonJerry/lockphish.git

### hmmcookies
Grab cookies from Firefox, Chrome, Opera using shortcut file (bypassing UAC)
https://github.com/swagkarna/hmmcookies
Address: The author has deleted the source code of the library and I have saved a copy locally. I haven't figured out how effective this fishing spot is for the time being.

### asnphishing
Advanced phishing tool with otp bypass, when the blue party enters their credentials, you need to go to the original website and use those credentials to send the real OTP to the victim. Once he enters that OTP, that OTP will also be with you and you will be allowed to log into the account before him.
Address: https://github.com/asnzodiac/asnphishing.git

### mip22
- https://github.com/makdosx/mip22

### maskphish
- https://github.com/jaykali/maskphish
