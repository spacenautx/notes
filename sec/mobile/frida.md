# frida


- https://github.com/synacktiv/frinet
- https://github.com/DERE-ad2001/Frida-Labs
- https://learnfrida.info/
- https://approov.io/blog/how-to-bypass-certificate-pinning-with-frida-on-an-android-app
- https://github.com/nshalabi/RunShell64/blob/master/Extra/INTERCEPT_API_FRIDA.md
- https://gist.github.com/incogbyte/1e0e2f38b5602e72b1380f21ba04b15e
- https://mrbypass.medium.com/unlocking-potential-exploring-frida-objection-on-non-jailbroken-devices-without-application-ed0367a84f07
- https://github.com/LabCIF-Tutorials/Tutorial-AndroidNetworkInterception
- https://www.revers0.com/posts/hookupdater/
- https://labs.cognisys.group/posts/Writing-your-first-Frida-script-for-Android/
- https://github.com/leonjza/frida-boot
- https://8ksec.io/advanced-root-detection-bypass-techniques/
- https://knifecoat.com/Posts/Coverage+guided+fuzzing+for+native+Android+libraries+(Frida+%26+Radamsa)
