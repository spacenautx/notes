- https://github.com/actuator/Android-Security-Exploits-YouTube-Curriculum
- https://github.com/krizzsk/HackersCave4StaticAndroidSec
- https://www.ragingrock.com/AndroidAppRE/
- https://github.com/NetKingJ/awesome-android-security
- https://hacklido.com/blog/416-unveiling-mobile-app-security-a-comprehensive-guide
- https://www.linkedin.com/pulse/android-static-analysis-fundamentals-smali-code-introduction
- https://bhavukjain.com/blog/2023/02/19/capturing-requests-non-proxy-aware-application
 
- https://secrary.com/android-reversing/android101/
- https://secrary.com/android-reversing/components/
- [burp](https://medium.com/inbughunters/basic-android-security-testing-lab-part-1-a2b87e667533)
- https://appsecwiki.com/#/mobilesecurity
- https://manifestsecurity.com/android-application-security/
- https://mobile-security.gitbook.io/mobile-security-testing-guide/overview/0x03-overview
- https://www.evilsocket.net/2017/04/27/Android-Applications-Reversing-101/
- https://github.com/OWASP/owasp-mstg
- https://github.com/OWASP/owasp-masvs
- https://secvibe.com/android-appsec-27855dca8531
- https://github.com/ashishb/android-security-awesome
- https://github.com/zhujun2730/Android-Learning-Resources
- https://developer.android.com/guide/topics/manifest/manifest-intro
- https://github.com/abhi-r3v0/EVABS
- https://github.com/abhi-r3v0/Adhrit
- https://github.com/vaib25vicky/awesome-mobile-security
- https://github.com/m0bilesecurity/RMS-Runtime-Mobile-Security
- https://github.com/iddoeldor/frida-snippets
- https://blog.fox-it.com/2022/06/29/flubot-the-evolution-of-a-notorious-android-banking-malware/
- https://github.com/nowsecure/secure-mobile-development
- https://github.com/Zeyad-Azima/Offensive-Resources
- https://github.com/abhi-r3v0/Adhrit
- https://github.com/mirfansulaiman/Command-Mobile-Penetration-Testing-Cheatsheet
- https://github.com/tanprathan/MobileApp-Pentest-Cheatsheet
- https://github.com/saeidshirazi/awesome-android-security
- https://github.com/B3nac/Android-Reports-and-Resources
- https://github.com/Ralireza/Android-Security-Teryaagh
- https://www.youtube.com/watch?v=CwCOGf4Uunk
- https://blog.thalium.re/posts/leveraging-android-permissions/
- https://notsosecure.com/bypassing-hardened-android-applications


### Mobile Application Pentesting :

- https://github.com/randorisec/MobileHackingCheatSheet
- https://hackersonlineclub.com/mobile-security-penetration-testing/
- https://hackersonlineclub.com/android-root-detection-bypass-by-objection-and-frida/
- https://github.com/Ignitetechnologies/Android-Penetration-Testing
- https://github.com/dn0m1n8tor/AndroidPentest101
- https://www.mobilehackinglab.com/blog/damn-exploitable-android-app-lab-setup
- https://github.com/httptoolkit/httptoolkit-android
-----
- https://medium.com/@patilpiyush/mobile-application-pentesting-part-1-596e82e56e83
- https://medium.com/@patilpiyush/mobile-application-pentesting-part-2-feda7659eb12
- https://medium.com/@patilpiyush/mobile-application-pentesting-part-3-a9acbb487e6
- https://medium.com/@patilpiyush/mobile-application-pentesting-part4-329ca80b8e4b
- https://medium.com/@patilpiyush/mobile-application-pentesting-part-5-3c83e7f4dfe7
- https://medium.com/@patilpiyush/mobile-application-pentesting-part6-542870ffc5f7
- https://medium.com/@social_62682/from-fuzzing-to-remote-code-execution-in-samsung-android-56cbdebcfeca
-----
- https://gbhackers.com/mobile-application-penetration-testing
-----
- https://mobsf.github.io/docs/#/dynamic_analyzer?id=genymotion-cloud-android-x86_64
- https://awakened1712.github.io/hacking/hacking-whatsapp-gif-rce/
-----
- https://github.com/dwisiswant0/apkleaks
- https://github.com/den4uk/andriller
- https://github.com/tanprathan/MobileApp-Pentest-Cheatsheet
- https://github.com/enty8080/r0pwn
-----
- https://www.cyclon3.com/bypass-instagram-ssl-certificate-pinning-for-ios
- https://files.nwwn.com/android/
- https://eshard.com/posts/pixel6_bootloader_3


### labs
- https://medium.com/purplebox/step-by-step-guide-to-building-an-android-pentest-lab-853b4af6945e
- https://github.com/mattboddy47/tracedCTF
- https://github.com/t0thkr1s/allsafe
- https://github.com/rewanthtammana/Damn-Vulnerable-Bank
- https://github.com/oversecured/ovaa

### tools
- https://github.com/dwisiswant0/apkleaks
- https://github.com/MobSF/Mobile-Security-Framework-MobSF
- https://github.com/charles2gan/GDA-android-reversing-Tool
- https://github.com/Impact-I/reFlutter
- https://github.com/nowsecure/r2frida
- https://github.com/AvillaDaniel/AvillaForensics
- https://github.com/RealityNet/Android-Forensics-References
- https://github.com/bytedance/appshark
- [debloater](https://github.com/0x192/universal-android-debloater)
