#+title: xxe

https://github.com/jas502n/CVE-2020-8840/
GitHub
https://github.com/FasterXML/jackson-databind

https://spaceraccoon.dev/a-tale-of-two-formats-exploiting-insecure-xml-and-zip-file-parsers-to-create-a
https://0xatul.github.io/posts/2020/02/external-xml-entity-via-file-upload-svg/
https://mahmoudsec.blogspot.com/2019/08/exploiting-out-of-band-xxe-using.html
https://mohemiv.com/all/exploiting-xxe-with-local-dtd-files/
https://honoki.net/2018/12/12/from-blind-xxe-to-root-level-file-read-access
https://www.corben.io/XSS-to-XXE-in-Prince/
https://medium.com/@zain.sabahat/an-interesting-xxe-in-sap-8b35fec6ef33
https://medium.com/@mrnikhilsri/oob-xxe-in-prizmdoc-cve-2018-15805-dfb1e474345c
https://blog.netspi.com/xxe-in-ibms-maas360-platform
https://r00thunt.com/2018/10/05/blind-xml-external-entities-out-of-band-channel-vulnerability-paypal-case-study

spaceraccoon.dev
A Tale of Two Formats: Exploiting Insecure XML and ZIP File Parsers...
XML and ZIP - A Tale as Old As Time While researching a bug bounty target, I came across a web application that processed a custom file ...

External XML Entity via File Upload (SVG)
Write-up: XXE-File-upload ( SVG )
Baby paws:
I looked for what are the functionalites available in the WEB app just to see what are the possible attack vectors. I analyzed the WAF and found what triggers it and what are blocked characters I found a upload field that explicitl...
Exploiting Out Of Band XXE using internal network and php wrappers
Hello hackers, A couple of weeks ago I tweeted about exploiting an out of band XXE vulnerability with a firewall blocking all outgoing req...

Exploiting XXE with local DTD files
This little technique can force your blind XXE to output anything you want!

From blind XXE to root-level file read access
https://isc.sans.edu/diary/26166
