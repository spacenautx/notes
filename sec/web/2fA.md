- https://rashahacks.com/bypass-2fa-mechanism/


2FA Bypass techniques:🔥

List of 15 Common 2FA Bypasses For Bug Bounty / Penetration Testing.  

1. Response Manipulation
In response if "success":false
Change it to "success":true

2. Status Code Manipulation
If Status Code is 4xx
Try to change it to 200 OK and see if it bypasses restrictions

3. 2FA Code Leakage in Response
Check the response of the 2FA Code Triggering Request to see if the code is leaked.

4.JS File Analysis
Rare but some JS Files may contain info about the 2FA Code, worth giving a shot

5.2FA Code Reusability
Same code can be reused

6.Lack of Brute-Force Protection
Possible to brute-force any length 2FA Code

7.Missing 2FA Code Integrity Validation
Code for any user account can be used to bypass the 2FA

8.CSRF on 2FA Disabling
No CSRF Protection on disabling 2FA, also there is no auth confirmation

9. Password Reset Disable 2FA
2FA gets disabled on password change/email change

10.Backup Code Abuse
Bypassing 2FA by abusing the Backup code feature
Use the above mentioned techniques to bypass Backup Code to remove/reset 2FA reset restrictions

11.Clickjacking on 2FA Disabling Page
I-framing the 2FA Disabling page and social engineering victim to disable the 2FA

12. Iframing the 2FA Disabling page and social engineering victim to disable the 2FA
If the session is already hijacked and there is a session timeout vulnerability

13.Bypass 2FA with null or 000000
Enter the code 000000 or null to bypass 2FA protection.

Steps:-
1. Enter “null” in 2FA code
2. Enter 000000 in 2FA code
3. Send empty code - Someone found this in grammarly
4. Open a new tab in the same browser and check if other API endpoints are accessible without entering 2FA

14. Google Authenticator Bypass
Steps:-
1) Set-up Google Authenticator for 2FA
2) Now, 2FA is enabled
3) Go on the password reset page and change your password
4) If your website redirects you to your dashboard then 2FA (Google Authenticator) is bypassed

15. Bypassing OTP in registration forms by repeating the form submission multiple times using repeater

Steps :-
1) Create an account with a non-existing phone number
2) Intercept the Request in Burp Suite
3) Send the request to the repeater and forward
4) Go to the Repeater tab and change the non-existent phone number to your phone number
5) If you got an OTP to your phone, try using that OTP to register that non-existent number

16. Manipulating JSON parameter

Let say we have a parameter "otp" in JSON body for verification like : 

{"otp":"123456"} --> 403

Try changing this to an array : 

{"otp":["111111","123456","<correct_otp>"]} --> 200, token
