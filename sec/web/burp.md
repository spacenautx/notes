# burp

### android
- https://github.com/Anof-cyber/Androset
 
## Labs [11/21]

## SQLi
- [X] SQL injection UNION attack, determining the number of columns returned by the query
- [ ] SQL injection UNION attack, finding a column containing text
- [ ] SQL injection UNION attack, retrieving data from other tables
- [ ] SQL injection UNION attack, retrieving multiple values in a single column
- [ ] SQL injection attack, querying the database type and version on Oracle
- [ ] SQL injection attack, querying the database type and version on MySQL and Microsoft
- [ ] SQL injection attack, listing the database contents on non-Oracle databases
- [ ] SQL injection attack, listing the database contents on Oracle
- [ ] Blind SQL injection with conditional responses
- [ ] Blind SQL injection with conditional errors
- [ ] Blind SQL injection with time delays
- [ ] Blind SQL injection with time delays and information retrieval
- [ ] Blind SQL injection with out-of-band interaction
- [ ] Blind SQL injection with out-of-band data exfiltration
- [ ] SQL injection vulnerability in WHERE clause allowing retrieval of hidden data
- [ ] SQL injection vulnerability allowing login bypass

## XSS
- [X] Reflected XSS into HTML context with nothing encoded
- [X] Reflected XSS into HTML context with most tags and attributes blocked
- [X] Reflected XSS into HTML context with all tags blocked except custom ones
- [X] Reflected XSS with event handlers and href attributes blocked
- [ ] Reflected XSS with some SVG markup allowed
- [ ] Reflected XSS into attribute with angle brackets HTML-encoded
- [ ] Stored XSS into anchor href attribute with double quotes HTML-encoded
- [ ] Reflected XSS in canonical link tag
- [ ] Reflected XSS into a JavaScript string with single quote and backslash escaped
- [ ] Reflected XSS into a JavaScript string with angle brackets HTML encoded
- [ ] Reflected XSS into a JavaScript string with angle brackets and double quotes HTML-encoded and single quotes escaped
- [ ] Reflected XSS in a JavaScript URL with some characters blocked
- [ ] Stored XSS into onclick event with angle brackets and double quotes HTML-encoded and single quotes and backslash escaped
- [ ] Reflected XSS into a template literal with angle brackets, single, double quotes, backslash and backticks Unicode-escaped
- [ ] Reflected XSS with AngularJS sandbox escape without strings
- [ ] Reflected XSS with AngularJS sandbox escape and CSP
- [ ] Stored XSS into HTML context with nothing encoded
- [ ] DOM XSS in document.write sink using source location.search
- [ ] DOM XSS in document.write sink using source location.search inside a select element
- [ ] DOM XSS in innerHTML sink using source location.search
- [ ] DOM XSS in jQuery anchor href attribute sink using location.search source
- [ ] DOM XSS in AngularJS expression with angle brackets and double quotes HTML-encoded
- [ ] Reflected DOM XSS
- [ ] Stored DOM XSS
- [ ] Exploiting cross-site scripting to steal cookies
- [ ] Exploiting cross-site scripting to capture passwords
- [ ] Exploiting XSS to perform CSRF
- [ ] Reflected XSS protected by CSP, with dangling markup attack
- [ ] Reflected XSS protected by very strict CSP, with dangling markup attack
- [ ] Reflected XSS protected by CSP, with CSP bypass

## CSRF
- [X] CSRF vulnerability with no defenses
- [X] CSRF where token validation depends on request method
- [X] CSRF where token validation depends on token being present
- [X] CSRF where token is not tied to user session
- [ ] CSRF where token is tied to non-session cookie
- [ ] CSRF where token is duplicated in cookie
- [ ] CSRF where Referer validation depends on header being present
- [ ] CSRF with broken Referer validation

## Clickjacking
- [X] Basic clickjacking with CSRF token protection
- [X] Clickjacking with form input data prefilled from a URL parameter
- [X] Clickjacking with a frame buster script
- [X] Exploiting clickjacking vulnerability to trigger DOM-based XSS
- [X] Multistep clickjacking

## DOM
- [X] DOM XSS using web messages
- [X] DOM XSS using web messages and a JavaScript URL
- [X] DOM XSS using web messages and JSON.parse
- [X] DOM-based open redirection
- [X] DOM-based cookie manipulation
- [X] Exploiting DOM clobbering to enable XSS
- [X] Clobbering DOM attributes to bypass HTML filters

## CORS
- [X] CORS vulnerability with basic origin reflection
- [X] CORS vulnerability with trusted null origin
- [X] CORS vulnerability with trusted insecure protocols
- [X] CORS vulnerability with internal network pivot attack

## XXE
- [X] Exploiting XXE using external entities to retrieve files
- [ ] Exploiting XXE to perform SSRF attacks
- [ ] Blind XXE with out-of-band interaction
- [ ] Blind XXE with out-of-band interaction via XML parameter entities
- [ ] Exploiting blind XXE to exfiltrate data using a malicious external DTD
- [ ] Exploiting blind XXE to retrieve data via error messages
- [ ] Exploiting XXE to retrieve data by repurposing a local DTD
- [ ] Exploiting XInclude to retrieve files
- [ ] Exploiting XXE via image file upload

## SSRF
- [X] Basic SSRF against the local server
- [ ] Basic SSRF against another back-end system
- [ ] SSRF with blacklist-based input filter
- [ ] SSRF with whitelist-based input filter
- [ ] SSRF with filter bypass via open redirection vulnerability
- [ ] Blind SSRF with out-of-band detection
- [ ] Blind SSRF with Shellshock exploitation

## smuggling
- [X] HTTP request smuggling, basic CL.TE vulnerability
- [ ] HTTP request smuggling, basic TE.CL vulnerability
- [ ] HTTP request smuggling, obfuscating the TE header
- [ ] HTTP request smuggling, confirming a CL.TE vulnerability via differential responses
- [ ] HTTP request smuggling, confirming a TE.CL vulnerability via differential responses
- [ ] Exploiting HTTP request smuggling to bypass front-end security controls, CL.TE vulnerability
- [ ] Exploiting HTTP request smuggling to bypass front-end security controls, TE.CL vulnerability
- [ ] Exploiting HTTP request smuggling to reveal front-end request rewriting
- [ ] Exploiting HTTP request smuggling to capture other users' requests
- [ ] Exploiting HTTP request smuggling to deliver reflected XSS
- [ ] Exploiting HTTP request smuggling to perform web cache poisoning
- [ ] Exploiting HTTP request smuggling to perform web cache deception

## command injection
- [X] OS command injection, simple case
- [X] Blind OS command injection with time delays
- [X] Blind OS command injection with output redirection
- [X] Blind OS command injection with out-of-band interaction
- [X] Blind OS command injection with out-of-band data exfiltration

## SSTI
- [X] Basic server-side template injection
- [X] Basic server-side template injection (code context)
- [X] Server-side template injection using documentation
- [X] Server-side template injection in an unknown language with a documented exploit
- [X] Server-side template injection with information disclosure via user-supplied objects
- [X] Server-side template injection in a sandboxed environment
- [X] Server-side template injection with a custom exploit

## Directory traversal
- [X] File path traversal, simple case
- [X] File path traversal, traversal sequences blocked with absolute path bypass
- [X] File path traversal, traversal sequences stripped non-recursively
- [X] File path traversal, traversal sequences stripped with superfluous URL-decode
- [X] File path traversal, validation of start of path
- [X] File path traversal, validation of file extension with null byte bypass

## Access control vulnerabilities
- [X] Unprotected admin functionality
- [X] Unprotected admin functionality with unpredictable URL
- [X] User role controlled by request parameter
- [X] User role can be modified in user profile
- [X] URL-based access control can be circumvented
- [ ] Method-based access control can be circumvented
- [X] User ID controlled by request parameter
- [X] User ID controlled by request parameter, with unpredictable user IDs
- [X] User ID controlled by request parameter with data leakage in redirect
- [X] User ID controlled by request parameter with password disclosure
- [ ] Insecure direct object references
- [ ] Multi-step process with no access control on one step
- [ ] Referer-based access control

## Authentication
- [X] Username enumeration via different responses
- [X] Username enumeration via subtly different responses
- [ ] Username enumeration via response timing
- [ ] Broken brute-force protection, IP block
- [ ] Username enumeration via account lock
- [ ] Broken brute-force protection, multiple credentials per request
- [ ] 2FA simple bypass
- [ ] 2FA broken logic
- [ ] 2FA bypass using a brute-force attack
- [ ] Brute-forcing a stay-logged-in cookie
- [ ] Offline password cracking
- [ ] Password reset broken logic
- [ ] Password reset poisoning
- [ ] Password brute-force via password change

## WebSockets
- [X] Manipulating WebSocket messages to exploit vulnerabilities
- [X] Manipulating the WebSocket handshake to exploit vulnerabilities
- [X] Cross-site WebSocket hijacking

## Web cache poisoning
- [X] Web cache poisoning with an unkeyed header
- [X] Web cache poisoning with an unkeyed cookie
- [X] Web cache poisoning with multiple headers
- [X] Targeted web cache poisoning using an unknown header
- [X] Web cache poisoning to exploit a DOM vulnerability via a cache with strict cacheability criteria
- [X] Combining web cache poisoning vulnerabilities
- [X] Web cache poisoning via an unkeyed query string
- [X] Web cache poisoning via an unkeyed query parameter
- [X] Parameter cloaking
- [X] Web cache poisoning via a fat GET request
- [X] URL normalization
- [X] Cache key injection
- [X] Internal cache poisoning

## Insecure deserialization
- [X] Modifying serialized objects
- [X] Modifying serialized data types
- [X] Using application functionality to exploit insecure deserialization
- [X] Arbitrary object injection in PHP
- [ ] Exploiting Java deserialization with Apache Commons
- [ ] Exploiting PHP deserialization with a pre-built gadget chain
- [ ] Exploiting Ruby deserialization using a documented gadget chain
- [ ] Developing a custom gadget chain for Java deserialization
- [ ] Developing a custom gadget chain for PHP deserialization
- [ ] Using PHAR deserialization to deploy a custom gadget chain

## Information disclosure
- [X] Information disclosure in error messages
- [X] Information disclosure on debug page
- [X] Source code disclosure via backup files
- [X] Authentication bypass via information disclosure
- [X] Information disclosure in version control history

## Business logic
- [X] Excessive trust in client-side controls
- [X] High-level logic vulnerability
- [X] Low-level logic flaw
- [X] Inconsistent handling of exceptional input
- [X] Inconsistent security controls
- [X] Weak isolation on dual-use endpoint
- [X] Insufficient workflow validation
- [X] Authentication bypass via flawed state machine
- [X] Flawed enforcement of business rules
- [X] Infinite money logic flaw
- [X] Authentication bypass via encryption oracle

## Host Header
- [X] Basic password reset poisoning
- [X] Password reset poisoning via dangling markup
- [X] Web cache poisoning via ambiguous requests
- [X] Host header authentication bypass
- [X] Routing-based SSRF
- [X] SSRF via flawed request parsing

## OAuth
- [X] Authentication bypass via OAuth implicit flow
- [ ] Forced OAuth profile linking
- [ ] OAuth account hijacking via redirect-uri
- [ ] Stealing OAuth access tokens via an open redirect
- [ ] Stealing OAuth access tokens via a proxy page
- [ ] SSRF via OpenID dynamic client registration

### profiles
- https://github.com/TheGetch/Burp-Suite-Pro-Scan-Profiles

## Tutorials
- [BurpSuite-For-Pentester](https://github.com/Ignitetechnologies/BurpSuite-For-Pentester)
- [The Art of Exploiting Logical Flaws in Web Applications](https://drive.google.com/file/d/1-4fJ_lKI_Lg5LytELS3f-fPoFy0Tvxk1/view)
- [Writing extension](https://allabouthack.com/writing-burp-suite-extension-in-python-part-2)
- https://github.com/frank-leitner/portswigger-websecurity-academy
